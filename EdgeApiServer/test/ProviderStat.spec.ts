import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';
import ProviderStats from '../src/ProviderStats';

describe('ProviderStat', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();

    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
    });

    it('Status', async () => {
        let result = await api.stats_doAddStats('today', 'TestCase', 'test_add_stats', 1);
        result.should.be.eql(true);
    });

    it('stat_add', async () => {

        let stats = new ProviderStats(ProviderStats.prototype);

        let result = await stats.doAddStats('today', 'test', 'something', 1) as any;
        result.should.be.eql(true);
    });

});