import { EdgeRequest } from '../src/EdgeRequest';

const should = require('should');

console.log("Making a call that takes 5 minutes.");

const request = new EdgeRequest();
request.doGetLink("http://playground.brians.com/timeout.php", {})
    .then(function (data: any) {
        console.log("DATA=", data);
        return process.exit();
    }).catch(function (e: Error) {
    console.log("Exception:", e);
    return process.exit();
});