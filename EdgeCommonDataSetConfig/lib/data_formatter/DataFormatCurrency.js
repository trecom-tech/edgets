"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for money data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const common = new DataFormatCommon_1.default();
const numeral = require('numeral');
class DataFormatCurrency extends DataFormatterType_1.default {
    constructor() {
        super();
        this.name = 'money';
        this.align = 'right';
        this.input_width = 120;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null) || (data === null) || (data === "")) {
            return "";
        }
        return numeral(common.getNumber(data)).format('$ #,###.[##]');
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return common.getNumber(data);
    }
    attachEditor(elSourceField, path) {
        super.attachEditor(elSourceField, path);
        return elSourceField.attr("onclick", "this.select();");
    }
}
exports.DataFormatCurrency = DataFormatCurrency;
exports.default = DataFormatCurrency;
//# sourceMappingURL=DataFormatCurrency.js.map