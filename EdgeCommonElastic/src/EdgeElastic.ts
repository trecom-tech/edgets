import { config } from 'edgecommonconfig';
import EdgeRequest from 'edgecommonrequest';
import EdgeApi from 'edgeapi';

const AWSElasticsearch = require('aws-es');
const Elasticsearch    = require('elasticsearch');
const LRU              = require('lru-cache');

export default class EdgeElastic {
    static singleInstance: EdgeElastic = null;
    private cache        : any;
    private log          : any;
    private base         : string;
    private client       : any;

    /**
     * @returns singleton for the elastic search
     */
    static getElastic() {
        if (EdgeElastic.singleInstance === null) {
            EdgeElastic.singleInstance = new EdgeElastic();
        }

        return EdgeElastic.singleInstance;
    }

    /**
     * Create an instance of EdgeElastic
     */
    constructor() {
        const credentials = config.getCredentials('elasticsearch');

        if (credentials.type === 'es') {
            this.client = new Elasticsearch.Client(credentials);
        } else {
            this.client = new AWSElasticsearch(credentials);
        }

        this.base = `${credentials.host}/`;
        this.log = config.getLogger('Elastic');

        // Setup Cache
        const cacheOptions = {
            max: 100,
            maxAge: 1000 * 60 * 5, // 300 seconds
        };
        this.cache = new LRU(cacheOptions);
    }

    /**
     * get mappings for index/type
     * @param params {index, type}
     */
    async doGetMapping(params: { index: string; type: string }) {
        const { index, type } = params;
        let link = this.base + index + '/_mappings/' + type;

        let request = new EdgeRequest();
        const output = await request.doGetLink(link, {});

        config.dump('output', output);
        return output;
    }

    /**
     * delete index
     * @param params {index}
     */
    async doDeleteIndex(params: { index: string }): Promise<string> {
        const output = await this.client.indices.delete({
            index: params.index,
        });
        return output;
    }

    /**
     * retrives all indices
     */
    async doGetIndexes(): Promise<any> {
        const link = this.base + '_cat/indices';
        const request = new EdgeRequest();
        const output = await request.doGetLink(link, { v: true });
        return output;
    }

    /**
     * update dataset index, collection type with id and body (doc)
     * @param options {dataset, collection, id, doc}
     */
    async doUpdate(options: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
    }) {
        const { dataset, collection, id, doc } = options;

        config.status(
            `EdgeElastic doUpdate Start on ${dataset}:${collection}/${id}`,
        );
        const result = await this.client.index({
            index: dataset,
            type: collection,
            id,
            body: doc,
        });
        config.dump('EdgeElastic doUpdate result:', result);
        return result;
    }

    /**
     * get cached dataset index, colllection type
     * @param options {dataset: index, colleciton: type, body: search options}
     */
    async doGet(options: { dataset: string; collection: string; body: any }) {
        const { dataset, collection, body } = options;

        let searchOptions: any = {
            index: dataset,
            type: collection,
        };

        for (let varName in body) {
            searchOptions[varName] = body[varName];
        }

        try {
            if (config.traceEnabled) {
                config.status(
                    `EdgeElastic doSearch dataset=${dataset} collection=${collection} starting`,
                );
                config.dump('Search Options', searchOptions);
            }

            const data = await this.client.get(searchOptions);
            config.status('EdgeElastic doSearch Done');
            return data;
        } catch (err) {
            this.log.error('doSearch Error', {
                searchOptions,
                err,
            });
            config.status('EdgeElastic doSearch Done with errors');
            console.log(err);
            throw err;
        }
    }

    /**
     * Search for collection
     * @param params {dataset, collection, query, sourceFilter, limit}
     */
    async doSearch(params: {
        dataset: string;
        collection: string;
        query: string;
        sourceFilter: any[];
        limit: number;
    }) {
        const {
            dataset,
            collection,
            query = '',
            sourceFilter = [],
            limit = 100,
        } = params;

        return new Promise((resolve, reject) => {
            let searchOptions = {
                index: dataset,
                type: collection,
                _source: sourceFilter,
                q: query,
                scroll: '10s',
                size: limit,
            };

            if (config.traceEnabled) {
                config.status(
                    `EdgeElastic doSearch dataset=${dataset} collection=${collection} starting`,
                );
                config.dump('Search Options', searchOptions);
            }

            let allResults: any = null;
            const collectResultsFunction = (err: Error, data: any) => {
                if (err != null) {
                    console.log('doSearch error:', err);
                    this.log.error('doSearch Error', {
                        searchOptions,
                        err,
                    });

                    config.status('EdgeElastic doSearch Done with errors');
                    reject(err);
                } else {
                    config.status('EdgeElastic doSearch Done');

                    if (allResults == null) {
                        allResults = data;
                    } else {
                        for (let result of Array.from(data.hits.hits)) {
                            allResults.hits.hits.push(result);
                        }
                    }

                    config.status(
                        `EdgeElastic we have ${
                            allResults.hits.hits.length
                        } of ${data.hits.total}`,
                    );

                    if (
                        data.hits.total > allResults.hits.hits.length &&
                        allResults.hits.hits.length < limit
                    ) {
                        return this.client.scroll(
                            {
                                scrollId: data._scroll_id,
                                scroll: '10s',
                            },
                            collectResultsFunction,
                        );
                    } else {
                        return resolve(allResults);
                    }
                }
            };

            this.client.search(searchOptions, collectResultsFunction);
        });
    }

    /**
     * set index dataset index with id and doc.
     * @param params {dataset, collection, id, doc}
     */
    async doIndex(params: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
    }) {
        const { dataset, collection, id, doc } = params;

        try {
            const recordHash = EdgeApi.getHash(doc);
            const recordKey = `ELASTIC${dataset}_${collection}_${id}`;

            const cacheHash = this.cache.get(recordKey);

            if (cacheHash != null && cacheHash === recordHash) {
                config.status(`EdgeElastic doIndex hash match on ${recordKey}`);
                return true;
            }

            config.status(
                `EdgeElastic doIndex Start on ${dataset}:${collection}/${id}`,
            );
            this.log.info('Submitted', {
                dataset,
                collection,
                id,
                url: `${this.base}${dataset}/${collection}/${id}`,
            });

            const result = await this.client.index({
                index: dataset,
                type: collection,
                id,
                body: doc,
            });

            this.log.info('doIndex', {
                result,
                dataset,
                collection,
                id,
            });

            this.cache.set(recordKey, recordHash);

            return result;
        } catch (err) {
            console.log(err);
            this.log.error('doIndex', {
                err,
                dataset,
                collection,
                id,
                doc,
            });
            throw err;
        }
    }

    /**
     * Returns the hash values as stored in Elastic
     * @param params {dataset, collection ,id}
     */

    async doGetDocumentHash(params: {
        dataset: string;
        collection: string;
        id: string;
    }) {
        const { dataset, collection, id } = params;
        let key = dataset + '_' + collection + '_' + id;
        const result = await this.client.get({
            index: 'dochash',
            type: 'path',
            id: key,
        });

        config.dump('getDocResult', { result });

        return result;
    }

    async setDocumentHash(params: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
        subpath: string;
    }) {
        const { dataset, collection, id, doc, subpath } = params;

        try {
            const key = dataset + '_' + collection + '_' + id;
            const hash = EdgeApi.getHash(doc);

            let hashes: any = {};
            hashes[subpath] = hash;

            const result = await this.client.index({
                index: 'dochash',
                type: 'path',
                id: key,
                // doc_as_upsert : true
                body: {
                    _lastUpdate: new Date(),
                    _updateRequired: true,
                    hashes,
                },
            });

            return result;
        } catch (err) {
            this.log.err('setDocumentHash', {
                err,
                dataset,
                collection,
                id,
                doc,
            });
            throw err;
        }
    }
}
