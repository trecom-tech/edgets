/// <reference types="request" />
import EdgeMailData from './EdgeMailData';
import EdgeCommonMailerError from './EdgeCommonMailerError';
export { default as EdgeEmail } from './EdgeEmail';
export { default as EdgeMailData } from './EdgeMailData';
export declare class EdgeCommonMailer {
    static haveCreds: boolean;
    static singleton: EdgeCommonMailer;
    private logger;
    constructor();
    /**
     * @returns singleton for the elastic search
     */
    static getMailer(forceRefresh?: boolean): EdgeCommonMailer;
    /**
     * warn if cred is not found.
     */
    static warnMissingCreds(): boolean;
    /**
     * Log error internally
     */
    internalReportError(error: EdgeCommonMailerError, message: any): boolean;
    /**
     * Log success internally
     */
    internalReportMail(message: any, result: any): boolean;
    /**
     * Send email
     * @param message [EdgeMailData | any | EdgeEmail] - message body
     */
    doSendMessage(message: EdgeMailData | any): Promise<[import("request").Response, any]>;
}
export default EdgeCommonMailer;
