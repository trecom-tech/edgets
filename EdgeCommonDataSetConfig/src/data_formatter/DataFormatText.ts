//
// class for Text datatype
//
// @extends DataFormatterType
//
import { DataFormatterType } from './DataFormatterType';

export class DataFormatText extends DataFormatterType {
    name: string        = 'text';
    input_width: number = 300;
    align: string       = 'left';

    constructor(...args: any[]) {
        super();
    }


    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {

        if ((data == null)) {
            return "";
        }

        if (typeof data === 'number') {
            return data.toString();
        }

        if (typeof data === "object") {
            if (Array.isArray(data)) {
                data = data.filter(a => a != null).join(", ");
            } else {
                const list = [];
                for (let varName in data) {
                    const value = data[varName];
                    if ((value == null)) {
                        continue;
                    }
                    list.push(`${varName}=${value}`);
                }
                data = list.join(", ");
            }
        }

        if (data.length > 300) {
            return data.slice(0, 300) + "...";
        }

        return data;
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        return data;
    }

    renderTooltip(row: any, value: any, tooltipWindow: any) {
        if ((value == null)) {
            return false;
        }

        if (typeof value === "string") {
            // @ts-ignore
            const w = textWidth(value) + 20;
            const h = ((Math.floor(w / 400) + 1) * 20) + 15;
            /*if value.length > 100 then w = 440
            if value.length > 200 then w = 640
            if value.length > 300 then h = 440*/

            tooltipWindow.setSize(w, h);
            tooltipWindow.getBodyWidget().addClass("text");
            tooltipWindow.html(value);
            return true;
        }

        // console.log "renderTooltip row=", row, "value=", value
        return false;
    }
}

export default DataFormatText;
