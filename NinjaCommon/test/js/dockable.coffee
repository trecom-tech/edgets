$ ->

    addTestButton "Top Dock Test", "Open", (e)->

        w = addHolder()
        w.setView "Docked", (view)->
            view.getFirst().html "This is a docked widget that will be 100px on the top always"
            view.getBody().setView "TestShowSize"


    addTestButton "Left Dock Test", "Open", (e)->

        w = addHolder()
        w.setView "Docked", (view)->
            view.setLocationName "left"
            view.setDockSize 160
            view.getFirst().html "This is a docked widget that will be 160px on the left always"
            view.getBody().setView "TestShowSize"


    addTestButton "Bottom Dock Test with a view", "Open", (e)->

        w = addHolder()
        w.setView "Docked", (view)->
            view.setLocationName "bottom"
            view.setDockSize 200
            view.getFirst().setView "TestShowSize"
            view.getBody().setView "TestShowSize"

    addTestButton "Docked Toolbar", "Open", ()->

        addHolder()
        .setView "TestLayout9", (view)->

    addTestButton "Docked Toolbar with autolayout", "Open", ()->

        addHolder()
        .setView "TestLayout10", (view)->

    addTestButton "Simple Popup with a dock on top", "Open", (e)->

        doPopupView "Docked", "Popup with a dock", null, 500, 500, (view)->
            view.getFirst().setView "TestShowSize"
            view.getBody().setView "TestShowSize"

    addTestButton "Simple Popup with a dock and fixed", "Open", (e)->

        doPopupView "Docked", "Popup with a dock", null, 500, 500, (view)->

            view.getFirst().setView "TestShowSize"

            view.getBody().setView "TestShowSize"
            w = view.createTopFixed 100
            w.html "TEST"

    addTestButton "Simple Popup with a dock on bottom", "Open", (e)->

        doPopupView "Docked", "Popup with a dock", null, 500, 500, (view)->
            view.setLocationName "bottom"
            view.getFirst().setView "TestShowSize"
            view.getBody().setView "TestShowSize"

    addTestButton "Popup Table with dock", "Open", (e)->

        loadZipcodes()
        .then ()->
            doPopupView "Docked", "Popup with a table and dock", null, 500, 600, (view)->
                view.setDockSize 80
                view.getFirst().setView "TestShowSize"
                view.getBody().setView "Table", (viewTable)->
                    viewTable.addTable "zipcode"

    addTestButton "Show/Hide Child of ViewDocked", "Open", (e)->

        w = addHolder()
        w.setView "Docked", (view)->
            view.setLocationName "bottom"
            view.setDockSize 200
            view.getFirst().setView "TestShowSize"
            view.getBody().setView "TestShowSize"
            first  = view.getFirst()
            second = view.getSecond()
            setTimeout () =>
                second.hide()
                true
            , 2000
            setTimeout () =>
                second.show()
                true
            , 5000
        true


    addTestButton "Dock Serialize/Deserialize", "Open", (e)->

        w = addHolder()
        w.setView "Docked", (view)->
            view.setLocationName "bottom"
            view.setDockSize 200
            view.getFirst().setView "TestShowSize"
            view.getBody().setView "TestShowSize"


            setTimeout () ->
                obj = view.serialize()
                console.log "Dock serialized: ", obj, view.getFirst().childView.serialize()
                doPopupView "Docked", "Docked-deserialize", null, 800, 600, (view) ->
                    view.deserialize obj
            , 3000

    go()
