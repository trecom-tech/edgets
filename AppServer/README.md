# AppServer

Command line

This will not update the yarn packages at startup.

    c appserver --fast

## AppLauncher

The [App Launcher](src/AppLauncher.coffee) code is meant to run as the main program on
every node of the Edge Server or Hub Server.

* AppLauncher runs npm install / npm update on all modules before running them.
* AppLauncher will immediately restart any app that crashes or ends
* AppLauncher should load always running tools such as MicroServices, Web Servers, and the Job Server.

Steps

    mkdir edge && cd edge
    git clone git@gitlab.protovate.com:Edge/Jobs.git Jobs
    cd Jobs
    yarn install
    mkdir ~/EdgeConfig
    mkdir ~/EdgeData
    mkdir ~/EdgeData/logs
    mkdir ~/EdgeData/images

    c src/AppLauncher

on your development machine

    scp <credentials file> server:EdgeConfig/
    scp <AppLauncherConfig file> server:EdgeConfig/
