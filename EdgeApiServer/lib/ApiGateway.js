"use strict";
//  A gateway can take a command and options
//  and pass it to a provider
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const ProviderOS_1 = require("./ProviderOS");
const ProviderData_1 = require("./ProviderData");
const ProviderLogs_1 = require("./ProviderLogs");
const ProviderStats_1 = require("./ProviderStats");
const ProviderWorkQueue_1 = require("./ProviderWorkQueue");
const ProviderCodeEditor_1 = require("./ProviderCodeEditor");
const ProviderUtility_1 = require("./ProviderUtility");
const ProviderAuth_1 = require("./ProviderAuth");
const ProviderTimeline_1 = require("./ProviderTimeline");
const ProviderTable_1 = require("./ProviderTable");
const ProviderTest_1 = require("./ProviderTest");
class ApiGateway {
    /**
     * Creates an instance
     */
    constructor() {
        this.apiCallList = {};
        this.providers = [];
        this.apiWantsUser = {};
        this.apiCallFunc = {};
        this.apiOwner = {};
        this.providers.push(new ProviderOS_1.default(ProviderOS_1.default.prototype));
        this.providers.push(new ProviderCodeEditor_1.default(ProviderCodeEditor_1.default.prototype));
        this.providers.push(new ProviderData_1.default(ProviderData_1.default.prototype));
        this.providers.push(new ProviderLogs_1.default(ProviderLogs_1.default.prototype));
        this.providers.push(new ProviderStats_1.default(ProviderStats_1.default.prototype));
        this.providers.push(new ProviderWorkQueue_1.default(ProviderWorkQueue_1.default.prototype));
        this.providers.push(new ProviderUtility_1.default(ProviderUtility_1.default.prototype));
        this.providers.push(new ProviderAuth_1.default(ProviderAuth_1.default.prototype));
        this.providers.push(new ProviderTimeline_1.default(ProviderTimeline_1.default.prototype));
        this.providers.push(new ProviderTable_1.default(ProviderTable_1.default.prototype));
        this.providers.push(new ProviderTest_1.default(ProviderTest_1.default.prototype));
        this.determineApiCalls();
    }
    /**
     * An API Provider is a class the implements "ApiProvider" to expose functions
     * @param apiProvider instance of class extends ApiProvider
     */
    addProvider(apiProvider) {
        try {
            // Give api privder the reference to gateway.
            apiProvider._apiGatewayRef = this;
            let name = apiProvider.getBaseName();
            edgecommonconfig_1.default.status(`Adding calls for ${name}`);
            for (let objName in apiProvider.apiCallList) {
                let objVal = apiProvider.apiCallList[objName];
                edgecommonconfig_1.default.status(`Adding ${name}_${objName}(${JSON.stringify(objVal)})`);
                if (objVal != null && objVal.length > 0 && objVal[0] === 'user') {
                    this.apiWantsUser[name + '_' + objName] = true;
                    objVal.shift();
                    this.apiCallList[name + '_' + objName] = objVal;
                }
                else {
                    this.apiWantsUser[name + '_' + objName] = false;
                    this.apiCallList[name + '_' + objName] = objVal;
                }
                this.apiOwner[name + '_' + objName] = apiProvider;
                this.apiCallFunc[name + '_' + objName] = apiProvider[objName];
            }
            return true;
        }
        catch (e) {
            edgecommonconfig_1.default.status('Error loading API Provider:', e);
            return false;
        }
    }
    /**
     * Look at the functions exposed by the API and create an
     * array to reference those functions so that the socket can
     * RPC call them.
     */
    determineApiCalls() {
        try {
            this.apiCallList = {};
            this.apiCallFunc = {};
            return Array.from(this.providers).map(api => this.addProvider(api));
        }
        catch (e) {
            console.log(`Exception in determineApiCalls: ${e}`);
            return edgecommonconfig_1.default.reportError('Exception in determineApiCalls', e);
        }
    }
    MakeDynamicCall(callName, callData, user) {
        let args = [];
        if (this.apiWantsUser[callName]) {
            args.push(user);
        }
        for (let varName of Array.from(this.apiCallList[callName])) {
            args.push(callData[varName]);
        }
        let result = this.apiCallFunc[callName].apply(this.apiOwner[callName], args);
        return result;
    }
    hasApiFunc(callName) {
        return this.apiCallFunc.hasOwnProperty(callName);
    }
    /**
     * Handle a call to the API
     * @param callName - [string] function name provider_method
     * @param callData - [EdgeSocketRequestData]
     * @param callerSession - [ApiSocketUser]
     */
    doProcessApiCall(callName, callData, callerSession) {
        if (this.apiCallList[callName] != null) {
            let result = this.MakeDynamicCall(callName, callData, callerSession);
            return result;
        }
        return new Promise((resolve) => {
            resolve({
                error: true,
                message: `Invalid api call: ${callName}`,
            });
        });
    }
}
exports.default = ApiGateway;
;
//# sourceMappingURL=ApiGateway.js.map