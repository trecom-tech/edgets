import EdgeApi       from 'edgeapi';

describe('Test Api', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();

    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
    });

    // TEST API
    it('Test Api test', async () => {
        try {
            let result = await api.test_doTestCommand(5, 10);
            result.should.be.eql('X5Y10');

            result = await api.test_doTestCommand2(10, 20);
            result.should.be.eql(123);

            result = await api.test_doTestCommand3();
            result.should.be.eql('Test3 Result Here');
        } catch (err) {
            console.error(err);
        }
    });
});