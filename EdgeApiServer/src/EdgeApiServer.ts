import config             from 'edgecommonconfig';
import ApiSocketServer    from './ApiSocketServer';
import ApiWebServer       from './ApiWebServer';
import { addServiceUser } from './utility/AddServiceUser';

// The EdgeApiServer exposes the information stored at the edge using
// multiple API methods.   Those include

// WebSockets => Using Socket.io
// REST API   => Using KOA
// MQ         => Using Rabbit MQ (AQMP 0.9)

// TODO:
// 1- Add ZeroMQ Support again
// 2- Add SOAP Support and WSDL generation

// One instance of this server maintains a Mongo connection and
// incoming connections.  This can be run on many hosts

export function startServer(isBasicAuthEnabled: boolean = false, strRealm: string = ''): ApiSocketServer {
    // Add default service user while starting server
    addServiceUser()
        .then((res) => {
            console.log('Finished adding service user!!!');
        })
        .catch((err) => {
            console.log(err);
        });

    config.status(`Web server enabled on ${ config.WebserverPort }`);
    let apiWebServer = new ApiWebServer();

    if (isBasicAuthEnabled) {
        apiWebServer.enableBasicAuth(strRealm);
    }

    config.status('Creating ApiSocketServer, Socket.io listener enabled');
    let apiSocketServer = new ApiSocketServer(apiWebServer);

    config.setTitle('Edge API Server');
    return apiSocketServer;
}

export { default as ApiProvider } from './ApiProvider';
export { default as ApiSocketUser } from './ApiSocketUser';
export { EdgeError } from './EdgeError';
