## 
## This view shows a calendar in mobile mode
##
class ViewCalendarWide extends View

	onShowScreen: () => 
		# @title.html "Test title"
		@momentObj = moment()
		@today     = moment()

		## Options
		defaultOptions = 
			weekStart    : 0
			expanded     : false

		@options = defaultOptions
		@calcDateOptions @momentObj

		@momentLocaleData = moment().localeData()
		@monthLabels = @momentLocaleData.months()
		@weekDayLabels = @momentLocaleData.weekdaysShort()

		@touchX = -1
		@touchY = -1

		@touchEventWidth = 30
		@touchDistance = 0

		## Event handlers
		@expandHandler.on "click", (e) =>
			@toggleExpanded()
			true

		## Show contents
		@renderCalendar()

		@setupEvents()
		true

	setDate: (date) =>
		momentObj = moment(date)
		if !moment.isMoment(momentObj)
			console.log "Invalid date in setting date: ", date
			return null

		@momentObj = momentObj
		@today     = @momentObj.clone()

		@calcDateOptions @momentObj
		@momentObj

	calcDateOptions: (momentObj = @momentObj) =>
		@options.currentWeek        = momentObj.week()
		@options.currentMonth       = momentObj.month()
		@options.currentYear        = momentObj.year()
		@options.startWeek          = momentObj.clone().startOf("month").week()
		@options.endWeek            = momentObj.clone().endOf("month").week()
		if @options.endWeek < @options.startWeek
			@options.endWeek += momentObj.weeksInYear()
		@options.weeksInMonth       = @options.endWeek - @options.startWeek + 1
		@options.daysInCurrentMonth = momentObj.daysInMonth()

		@options

	toggleExpanded: () =>
		@options.expanded = !@options.expanded
		@renderCalendar()
		true

	renderCalendar: () =>
		@setTitle()
		@setWeekDayLabels()
		@showWeeks()
		@showWeekdays()
		@setExpandHandlerContent(@options.expanded)
		true

	setTitle: () =>
		month = @monthLabels[@options.currentMonth]
		year = @options.currentYear
		@title.html "#{month} #{year}"
		true

	showWeeks: () =>
		startWeek   = @options.startWeek
		currentWeek = @options.currentWeek

		if @options.expanded
			for index in [0..5]
				this["weekWrapper#{index + 1}"].show()
		else
			for index in [0..5]
				if index is (currentWeek - startWeek)
					this["weekWrapper#{index + 1}"].show()
				else
					this["weekWrapper#{index + 1}"].hide()

		true

	showWeekdays: () =>
		startWeek          = @options.startWeek
		weeksInMonth       = @options.weeksInMonth
		currentWeek        = @options.currentWeek

		if @options.expanded
			currDate = @momentObj.clone().startOf("month").startOf("week")
			for idxWeek in [0..5]
				if idxWeek < weeksInMonth
					for idxDate in [0..6]
						@renderDate currDate, idxWeek, idxDate
						currDate.add(1, "days")
				else
					this["weekWrapper#{idxWeek + 1}"].hide()
				
		else
			startOfWeek = @momentObj.clone().startOf("week").date()
			currDate = @momentObj.clone().startOf("week")
			for idxDate in [0..6]
				@renderDate currDate, currentWeek - startWeek, idxDate
				currDate.add(1, 'days')
		true 

	renderDate: (momentObj, week, day) =>
		if !momentObj? or !moment.isMoment(momentObj)
			console.log "Invalid moment object for date: ", momentObj
			return false
		if !week? or !day?
			console.log "Invalid week and/or day values for date: ", week, day
			return false

		date = momentObj.date()
		if momentObj.year() is @today.year() and momentObj.month() is @today.month() and momentObj.date() is @today.date() 
			this["weekDay#{week + 1}#{day + 1}"].css "line-height", "#{@cellSize / 2}px"
			this["weekDay#{week + 1}#{day + 1}"].html("<div class='today'>#{date}</div>")
		else
			if this["weekDay#{week + 1}#{day + 1}"].css("line-height") isnt "#{@cellSize}px"
				this["weekDay#{week + 1}#{day + 1}"].css "line-height", "#{@cellSize}px"
			this["weekDay#{week + 1}#{day + 1}"].html date

		if momentObj.month() isnt @options.currentMonth
			this["weekDay#{week + 1}#{day + 1}"].addClass "inactive"
		else
			this["weekDay#{week + 1}#{day + 1}"].removeClass "inactive"

		true

	setExpandHandlerContent: (expanded) =>
		if expanded
			@expandHandler.html "<i class='far fa-angle-up'></i>"
		else
			@expandHandler.html "<i class='far fa-angle-down'></i>"
		true

	setWeekDayLabels: () =>
		for index in [0..6]
			this["dayLabel#{index + 1}"].html @weekDayLabels[(index + @options.weekStart) % 7]
		true

	setDayCellSize: (cellSize) =>
		if !cellSize or typeof cellSize isnt "number"
			console.log "ViewCalendarWide invalid cellWidth: ", cellSize
			return false

		@cellSize = cellSize
		@dayLabelsWrapper.children().width cellSize
		for index in [1..6]
			this["weekWrapper#{index}"].children().width cellSize
			this["weekWrapper#{index}"].children().height cellSize
			this["weekWrapper#{index}"].children().css "line-height",  "#{cellSize}px"

		@renderCalendar()
		true

	setupEvents: () =>
		@prevBtn.on "click", (e) =>
			if !@timerMoveDate
				@timerMoveDate = setTimeout () =>
					@moveDate(-1)
				, 300

		@nextBtn.on "click", (e) =>
			if !@timerMoveDate
				@timerMoveDate = setTimeout () =>
					@moveDate(1)
				, 300

		@daysWrapper.on "mousedown touchstart", (e) =>
			@isTouching = true
			true

		@daysWrapper.on "mouseup touchend", (e) =>

			if Math.abs(@touchDistance) > @touchEventWidth
				
				if !@timerMoveDate
					# console.log "Touch ended: ", @touchDistance, @touchEventWidth
					@timerMoveDate = setTimeout () =>
						@moveDate(-1 * Math.sign(@touchDistance))
						@touchDistance = 0
					, 300

			@touchX = -1
			@touchY = -1
			# @touchDistance = 0
			@isTouching = false
			true

		@daysWrapper.on "mousemove touchmove", (e) =>
			if !@isTouching then return
			touchX = e.pageX or e.changedTouches[0].clientX
			touchY = e.pageY or e.changedTouches[0].clientY
			if @touchX >= 0 and @touchY >= 0
				deltaX = touchX - @touchX
				deltaY = touchY - @touchY
				@touchDistance += deltaX

			@touchX = touchX
			@touchY = touchY
			true

	moveDate: (interval) =>
		if !interval then return false
		if @options.expanded
			@momentObj.month(@options.currentMonth + interval)
		else
			@momentObj.week(@options.currentWeek + interval)
		@calcDateOptions()
		@renderCalendar()
		clearTimeout @timerMoveDate
		@timerMoveDate = null
		true

	onResize: (w, h) =>
		@setDayCellSize w / 7
		@touchEventWidth = 0.4 * w
		true

	serialize: () =>
		return
			date: @momentObj.toString()

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options?
				console.log "ViewCalendarWide: invalid options to deserialize, "
				reject "invalid data passed"

			if options.date?
				@setDate options.date

			resolve true
