import EdgeImportExportGoogleDoc    from '../src/EdgeImportExportGoogleDoc';
import config from 'edgecommonconfig';

const should = require('should');
const sheetId = '15t03XmgP8Wv1eSg2LlD4qqI6JMitZknh65LHj05j7Qg';

config.setCredentials('GoogleDocs1', {
    'oauth2': {
        'client_id'     : '36621392230-di6ehq4n7fmv8a9b186o8dkroragdpfc.apps.googleusercontent.com',
        'client_secret' : 'CMWK6kdFXgWDDpXC4LwLAv6u',
        'refresh_token' : '1/lgmk_4QJBe7V_4O9E5kQ8TSSni4hT3cfKwi8ZKugWug'
    }
});

describe('Reading Test', () => {

    const doc = new EdgeImportExportGoogleDoc('GoogleDocs1', sheetId, 'test.json', null);
    
    describe('Reading sheets', () => {
        
        let sheet: any   = null;
        const today      = new Date();

        it('Test invalid range in reading doc', () => {
            return doc.doReadDoc('test')
            .catch((err: any) => {
                should.exist(err);
            });
        });

        it('Test invalid range in reading sheet', () => {
            return doc.doReadSheets('test')
            .catch((err: any) => {
                should.exist(err);
            });
        });

        it('Should read the test documnet', async () => {
            let result: any = await doc.doReadSheets('A1:F6');
            should.exist(result);
            result.should.have.property('Sheet1');

            result = await doc.doReadSheets();
            should.exist(result);
            result.should.have.property('Sheet1');
            sheet = result['Sheet1'];
        });

        it('Should be able to get a text cell', () => {
            let cell = sheet.get(0, 0);
            cell.value().should.equal('Something Here that is text');
        });

        it('Should be able to get a number cell', () => {
            const cell = sheet.get(5, 1);
            cell.value().should.equal(52.43231451);
        });

        it('Should save a cell', async () => {
            let cell = sheet.get(4, 5);
            cell.setValue(today.getTime());

            cell = sheet.get(4, 6);
            cell.setValue(`Testing ${today.getSeconds()}`);
            cell.setBackgroundColor(0.7, 0.6, 0.5);

            cell = sheet.get(3, 6);
            cell.setHyperlink(`http://www.yahoo.com?${today.getTime()}`, 'Yahoo Link');

            const result: any = await sheet.doSaveChanges();
            should.exist(result);
            result.should.have.property('length', 3);
        });

    });

    describe('Reading cells', () => {

        it('doReadDocFormulas', async () => {
            const testRange = 'A1:F6';
            const result: any = await doc.doReadDocFormulas(testRange);
            should.exist(result);
            result.length.should.equal(6);
            result[0].length.should.equal(1); // Test Text in A1
            result[1].length.should.equal(0); // Blank Row
            result[2].length.should.equal(6); // Row 3, column headers
        });

        it('Should correct return a single cell', async () => {
            const testRange = 'A1';
            const result: any = await doc.doReadDoc(testRange);
            should.exist(result);
            result.length.should.equal(1);
            result[0].length.should.equal(1); 
            result[0][0].should.equal('Something Here that is text');
        });

        it('doReadDoc', async () => {
            const testRange = 'A1:F6';
            const result: any = await doc.doReadDoc(testRange);
            should.exist(result);
            result.length.should.equal(6);
            result[0].length.should.equal(1); // Test Text in A1
            result[1].length.should.equal(0); // Blank Row
            result[2].length.should.equal(6); // Row 3, column headers
        });

    });

});