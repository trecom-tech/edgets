import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatImageList extends DataFormatterType {
    name: string;
    width: number;
    align: string;
    option: any[];
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    format(currentValue: any, options: any[], path: any): string;
    unformat(currentValue: any, path: any): any;
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
}
export default DataFormatImageList;
