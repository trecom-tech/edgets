# EdgeCommonMessageQueue
> A general interface to the Edge message queue system.  Allows writing objects to the queue and opening a queue or pub/sub and consuming. Currently supporting AMQP(RabbitMQ).

    npm install --save ssh+git://git@gitlab.protovate.com:Edge/EdgeCommonMessageQueue
    import { EdgeAMQPController } from 'edgecommonmessagequeue'; // For AMQP Controller

This wrapper provides the general functions needed to quickly read or write with a message queue.   In the current state the project
is using AQMP 0.9 which is supported by many servers but we are testing and using RabbitMQ in Production.

## How to setup credentials
Please add `mqHost` variable to credentials for AMQP.

## AMQP Tutorial
### Opening a message queue by name

To open a message queue given a name, simply call the doOpenMessageQueue function and
this will return a wrapper (EdgeMessageQueue class) around an open MQ with read/write.

    const mq = await EdgeAMQPController.doOpenMessageQueue("test");
    // Now you have a connected and ready to use mq
    await mq.doSubmitBuffer(someObject);

To open a fan-out message queue by name use doOpenSubscription, for example:

    await EdgeAMQPController.doOpenSubscription(config.mqExchangeStatusUpdates, (message)=> {
        console.log("STATUS:", message);
    });

### Deploying RabbitMQ for testing

To test locally you will need to install RabbitMQ.  On a mac this is done with:

    brew install rabbitmq
    brew services start rabbitmq

Once you have it installed it will be running locally without passwords.   There is also a
[web based management](http://localhost:15672) installed by default.

To verify it's running, using the command

    /usr/local/sbin/rabbitmqctl status

THere will not be any message queues defined when you first start, that is valid.  This
module uses the EdgeCommonConfig library's getCredentials for "mqHost" which is set
by default to the local server for testing.

Once you run the tests with *npm test* you'll have queues created and you can use this
command to see the message queue and number of messages inside waiting

    $ /usr/local/sbin/rabbitmqctl list_queues
    Listing queues ...
    test	10

You may want to clear out messages from a queue for testing using this command

    $ /usr/local/sbin/rabbitmqctl purge_queue test

### Install rabbitmq in ubuntu

Please follow the following steps to install rabbitmq in ubuntu
https://gist.github.com/fernandoaleman/72f0ad39e11915c0077d544b50096b50

### Run tests locally with local rabbitmq

    npm install
    npm run test

### Testing using docker

    docker-compose build
    docker-compose up
