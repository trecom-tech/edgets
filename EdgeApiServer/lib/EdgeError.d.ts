export declare class EdgeError extends Error {
    message: string;
    code: string;
    status: number;
    options: any;
    constructor(message: string, code: string, status?: number, options?: any);
}
export default EdgeError;
