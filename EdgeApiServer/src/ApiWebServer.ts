import * as koa        from 'koa';
import * as fs         from 'fs';
import * as os         from 'os';
import * as formidable from 'formidable';
import * as swagger    from 'swagger2';
import { ui }          from 'swagger2-koa';
import config          from 'edgecommonconfig';
import ApiGateway      from './ApiGateway';
import ApiWebUser      from './ApiWebUser';
import * as stream     from 'stream';

const mime      = require('mime-types');
const session   = require('koa-session');
const compress  = require('koa-compress');
const glob      = require('glob-all');
const jsontoxml = require('jsontoxml');
const xml2js    = require('xml2js');
const builder   = new xml2js.Builder();

import WebServerHelper from './WebServerHelper';
import EdgeError       from './EdgeError';

const WebPath         = __dirname + '/../Web/';
const REDIRECTJSON    = 'redirects.json';
let globalLogger: any = null;
let banLogger: any    = null;
let globalValidAppNames : any = {};

interface CustomRoutes {
    pattern: string;
    callback: Function
};

/**
 * Escapse and parse host name to get app name
 * Convert to lowercase
 * Remove extension such as .com and .net
 * Remove www. from the front
 * Replace any remaining dots with _, for example, www.admin.website.com becomes "admin_website"
 * Filter the name to remove ^a-zA-Z0-9_ such that we have a safe and ascii printable name.
 * @param host - [string] host of browser
 */
export const safeEscapeAppName = function (host: string) {
    try {
        let appName = host.toLowerCase();

        appName = appName.replace(/\.[a-zA-Z_]*$/, '');
        appName = appName.replace(new RegExp(`^www\\.`), '');
        
        appName = appName.split('.').join('_');
        
        appName = appName.replace(/[^a-zA-Z0-9_]/g, '');
        return appName;
    } catch(err) {
        
        return null;
    }
}

/**
 * Convert a single path to a list of paths.
 * @param app - koa application
 * @param folder - string | string[] path(s) to find static files
 * @param url - url to match
 */
const staticPathMiddleware = function(app: any, folders: string | string[], url: string): void {

    if (typeof folders === 'string') {
        folders = [folders];
    }

    if (folders == null) {
        config.status(`Error in mapping ${ url } to invalid folder`);
        console.log(`Error in mapping ${ url } to invalid folder`);
        return;
    }

    config.status(`Mapped ${ folders } to url ${ url }`);

    const rePath = new RegExp(`^${ url }/([^\?]+)`, 'i');
    return app.use(async (ctx: koa.Context, next: Function) => {

        try {

            let dyFolders = folders;
            if (ctx.session.appName) {
                const pathList = [
                    `./Web/${ ctx.session.appName }/`, 
                    `../Web/${ ctx.session.appName }/`, 
                    `../../Web/${ ctx.session.appName }/`
                ];
                dyFolders = Array.from(folders).concat(pathList);
            }

            config.status(`staticPathMiddleware checking url=${ ctx.url } path=${ ctx.path } in `, dyFolders);

            const m = ctx.url.match(rePath);
            if (m != null) {

                for (let path of Array.from(dyFolders)) {

                    const filename = path + m[1];

                    try {
                        // console.log "staticPathMiddleware checking #{filename}"
                        const stats = fs.statSync(filename);
                        // console.log "staticPathMiddleware stat:", stats

                        if (stats != null) {
                            config.status(`staticPathMiddleware url=${ url } path=${ ctx.path } sending`);
                            ctx.response.type = mime.lookup(filename);
                            ctx.body          = fs.createReadStream(filename);
                            return true;
                        } else {
                            config.status('staticPathMiddleware no stats');
                        }
                    } catch (error) {
                        config.status('staticPathMiddleware error loading path=', path, 'm=', m, ' filename=', filename);
                        config.status('staticPathMiddleware exception:', error);
                    }
                }

                // console.log "Unable to find #{path}/#{m[1]}"

                return false;
            }
        } catch (error) {
            console.log('Static file error: ', error);
        }

        return next();
    });
};

const compileScreen = (screenName: string, appName: string) => new Promise(async (resolve) => {

    //
    // Remove trailing slash and anything params after it
    screenName = screenName.replace(/\/.*/, '');

    config.status(`compileScreen Screen=${ screenName }, appName=${ appName }`);

    let pathList = ['./Web/screens/', '../Web/screens', '../../Web/screens'];
    if (appName != null) {
        pathList = [`../Web/${ appName }/screens/`, `./Web/${ appName }/screens/`, `../../Web/${ appName }/screens/`, './Web/screens/'];
    }

    // require.main.paths is an array of paths to node_modules folders.
    for (let p of Array.from(require.main.paths)) {
        pathList.push(`${ p }/NinjaCommon/screens/views`);
    }

    let filenameStylus = await WebServerHelper.doFindFileInPath(`screen_${ screenName.toLowerCase() }.styl`, pathList);
    let filenameJade   = await WebServerHelper.doFindFileInPath(`screen_${ screenName.toLowerCase() }.pug`, pathList);
    let filenameCoffee = await WebServerHelper.doFindFileInPath(`screen_${ screenName.toLowerCase() }.coffee`, pathList);

    let html = await WebServerHelper.doCompilePugFile(filenameJade);
    html     = `<div id='Screen${ screenName }' class='screen contentNoPadding overflow-hidden'>` + html + '</div>';
    let js   = await WebServerHelper.doCompileCoffeeFile(filenameCoffee);

    let str = '';
    str += js;
    str += '\n';
    str += `Screen${ screenName }.prototype.screenContent = '`;
    str += escape(html);
    str += '\';';

    //  CSS File
    let css = await WebServerHelper.doCompileStylusFile(filenameStylus);
    if (css) {
        css = escape(css);
        str += `Screen${ screenName }.prototype.css = unescape('${ css }');`;
    }

    resolve(str);
});

const compileView = function(viewName: string, appName: string) {

    config.status(`compileView viewName=${ viewName }, appName=${ appName }`);

    return new Promise(async (resolve) => {

        let pathList = ['./Web/views/', '../Web/views', '../../Web/views', '../CoffeeNinjaCommon/ninja/views', '../../CoffeeNinjaCommon/ninja/views', '../../../CoffeeNinjaCommon/ninja/views'];
        if (appName != null) {
            pathList.push(`./Web/${ appName }/views/`);
            pathList.push(`../Web/${ appName }/views/`);
            pathList.push(`../../Web/${ appName }/views/`);
        }

        // require.main.paths is an array of paths to node_modules folders.
        for (let p of Array.from(require.main.paths)) {
            pathList.push(`${ p }/NinjaCommon/ninja/views`);
        }
        let filenameCss  = await WebServerHelper.doFindFileInPath(`${ viewName }.styl`, pathList);
        let filenameHtml = await WebServerHelper.doFindFileInPath(`${ viewName }.pug`, pathList);
        let filenameJs   = await WebServerHelper.doFindFileInPath(`${ viewName }.coffee`, pathList);

        let css  = await WebServerHelper.doCompileStylusFile(filenameCss);
        let html = await WebServerHelper.doCompilePugFile(filenameHtml);
        let js   = await WebServerHelper.doCompileCoffeeFile(filenameJs);

        html = escape(html);
        css  = escape(css);

        js += '\n';
        js += `${ viewName }.prototype.template = unescape('${ html }');\n`;
        js += `${ viewName }.prototype.css = unescape('${ css }');`;
        js += '\n';

        resolve(js);
    });
};

export default class ApiWebServer {
    app          : koa            = new koa();
    fileTimer    : any            = [];
    fileWatch    : any            = [];
    staticContent: any            = [];
    addedRoutes  : CustomRoutes[] = [];
    gateway      : ApiGateway     = new ApiGateway();
    globalUnique : number         = 0;
    isBasicAuthEnabled: boolean   = false;
    strRealm     : string;

    constructor() {
        this.setupMiddlewares();
        console.log(`Listening on port ${ config.WebserverPort }`);
    }

    public enableBasicAuth(strRealm: string) {
        this.isBasicAuthEnabled = true;
        this.strRealm = strRealm;
    }

    setupMiddlewares() {
        let pathHelper      = require('path');
        let localCommonPath = pathHelper.dirname(require.main.filename);

        this.loadLessFiles('/css/baseedgeweb.css', WebPath + 'css/*less');
        this.loadStylusFile('/css/edgeweb.css', WebPath + 'css/*styl');
        this.loadCoffeeFiles('/js/combined.js', [WebPath + 'js/*coffee', '../Web/common/*.coffee', localCommonPath + '/Web/common/*.coffee', localCommonPath + '/../Web/common/*.coffee']);

        this.app.keys = ['apiWebServerKey123'];
        this.app.use(session(this.app));
        this.app.use(compress({
            filter: function (contentType: string) {
                return /javascript|css/i.test(contentType);
            },
            threshold: 2048,
            flush: require('zlib').Z_SYNC_FLUSH
        }));

        this.swaggerMiddleware();
        this.loggingMiddleware();
        this.statusMiddleware();
        this.findAppNameMiddleware();

        let localVendorPath = this.findStaticFilePath('vendor/ace', 'ace.js');
        if (localVendorPath == null) {
            throw new Error('Unable to find vendor path');
        }
        localVendorPath = localVendorPath.replace('/ace/', '/');
        config.status(`Local vendor path: ${ localVendorPath }`);

        staticPathMiddleware(this.app, localVendorPath + 'ace/', '/ace');
        staticPathMiddleware(this.app, localVendorPath + 'closure/', '/closure');
        staticPathMiddleware(this.app, localVendorPath + 'closure/', '/closure-library/closure');

        let localNinjaPath = this.findStaticFilePath('ninja', 'ninja.js');
        config.status('Local ninja path:', localNinjaPath);

        if (localNinjaPath == null) {
            console.log('WARNING: Unable to find localNinjaPath for ninja.js');
            localNinjaPath = './node_modules/NinjaCommon/ninja';
        }

        staticPathMiddleware(this.app, localNinjaPath, '/ninja');


        let localPathFonts = this.findStaticFilePath('fonts', 'usesf.css');
        config.status('Local fonts path:', localPathFonts);
        staticPathMiddleware(this.app, [localPathFonts, localPathFonts + "/../Web/fonts/", localNinjaPath + '/fonts/'], '/fonts');

        let localPathImages = this.findStaticFilePath('images', 'checkbox.png');
        config.status('Local images path:', localPathImages);
        staticPathMiddleware(this.app, [localPathImages, localNinjaPath + '/images/', config.imagePath], '/images');

        let localPathVendor = this.findStaticFilePath('vendor', 'co.js');
        config.status('Local vendor path:', localPathVendor);
        staticPathMiddleware(this.app, [localPathVendor, localNinjaPath + '/vendor/', localCommonPath + '/Web/vendor/', localCommonPath + '/../Web/vendor/'], '/vendor');

        this.setupStatic();
        this.screenMiddleware();

        this.dataPathMiddleware();
        this.apiPathMiddleware();
        
        this.addRouteMiddleware();
        this.banHackerMiddleWare();
        this.hostRedirectMiddleware();
        this.finalMiddleware();

        this.createConfigFilesIfNotExist()
    }

    /**
     * Get the component required for starting Socket.io
     */
    getHttpServer() {
        return require('http').Server(this.app.callback());
    }

    /**
     * Get HTTPS server required for starting Socket.io with SSL support
     */
    getHttpsServer() {
        try {
            const dirs      = ['.', '..', os.homedir() + '/EdgeConfig/'];
            const host      = os.hostname();
            const privkey   = config.FindFileInPath(`privkey-${ host }.pem`, dirs);
            const fullchain = config.FindFileInPath(`fullchain-${ host }.pem`, dirs);

            if (privkey != null && fullchain != null) {
                let options = {
                    key: fs.readFileSync(privkey),
                    cert: fs.readFileSync(fullchain),
                };

                return require('https').Server(options, this.app.callback());
            } else {
                config.status('SSL certificate not found');
                return null;
            }
        } catch (e) {
            config.status(`Error: ${ e }`);
            return null;
        }
    }

    /**
     * Set a timer to watch a file for changes
     * @param f file to watch
     * @param callback callback after changed
     */
    setWatchTimer(f: string, callback: Function) {
        if (this.fileWatch[f] == null) {
            this.fileWatch[f] = fs.watch(f, {}, (event, filename) => {
                if (this.fileTimer[f]) {
                    clearTimeout(this.fileTimer[f]);
                }
                return this.fileTimer[f] = setTimeout(callback, 1000);
            });
        }

        return true;
    }

    //
    //  Initialize the static content holder and make a callback for each
    /**
     * Process a path or glob to a set of files
     * Initialize the static content holder and make a callback for each
     * @param url
     * @param pathList
     * @param contentType
     * @param callbackEachFile
     */
    async processPath(url: string, pathList: string[] | string, contentType: string, callbackEachFile: Function) {
        if (!Array.isArray(pathList)) {
            pathList = [pathList];
        }

        this.staticContent[url] = {
            mime: contentType,
            content: '',
        };

        for (let path of Array.from(pathList)) {

            config.status(`Processing ${ path } (${ contentType }) for ${ url }`);
            let files = glob.sync(path);

            for (let f of files) {

                config.status(`processPath ${ contentType }: Reading file: ${ f } for ${ url }`);
                let content = await callbackEachFile(f);
                this.staticContent[url].content += content;

                //  Watch for changes
                this.setWatchTimer(f, () => {
                    this.fileTimer[f] = null;
                    config.status(`processPath Reloading url=${ url } path=${ path }`);
                    return this.processPath(url, pathList, contentType, callbackEachFile);
                });
            }
        }

        return true;
    }

    loadLessFiles(url: string, path: string | string[]) {

        let files = [WebPath + 'css/variables.less', WebPath + 'css/mixins.less'];
        this.processPath(url, path, 'text/css', (filename: string) => {
            if (!/mixins/.test(filename) && !/variables/.test(filename)) {
                files.push(filename);
            }

            return Promise.resolve('');
        });

        return setTimeout(async () => {
            const css                       = await WebServerHelper.doCompileLessFiles(files);
            this.staticContent[url].content = css.css;
        }, 200);
    }

    loadStylusFile(url: string, path: string | string[]) {

        this.processPath(url, path, 'text/css', WebServerHelper.doCompileStylusFile);
        return true;
    }

    loadJadeFile(url: string, path: string | string[]) {
        this.processPath(url, path, 'text/html', WebServerHelper.doCompilePugFile);
        return true;
    }

    loadCoffeeFiles(url: string, path: string | string[]) {

        this.processPath(url, path, 'text/javascript', WebServerHelper.doCompileCoffeeFile);
        return true;
    }

    setupStatic() {

        return this.app.use(async (ctx, next) => {

            config.status(`setupStatic checking path ${ ctx.path }`);

            if (this.staticContent[ctx.path] != null && this.staticContent[ctx.path].mime != null) {
                ctx.response.type = this.staticContent[ctx.path].mime;
                ctx.body          = this.staticContent[ctx.path].content;
                return config.status(`setupStatic Static Match path=${ ctx.path } [`, ctx.response.type, ']');
            } else {
                return next();
            }
        });
    }

    /**
     * Validates that an app name is valid.  For example app "test" must have Web/test/ as a folder.
     * @param appName The name of app folder
     * 
     * TODO: Remove invalid file path chars from appName just in case.
     */
    doValidateAppname(appName : string) :Promise<boolean>
    {
        return new Promise(async (resolve) => {

            if (globalValidAppNames[appName])
            {
                resolve(true);
                return;
            }

            let localPath = this.findStaticFilePath('templates/', 'index.pug');
            console.log("doValidateAppname localPath=", localPath);
            fs.stat(localPath + "../" + appName, (err, stats)=>{
                if (err)
                {
                    config.status("doValidateApp failed for", appName, " in path", localPath + "../" + appName);
                    resolve(false);
                } else {
                    globalValidAppNames[appName] = true;
                    resolve(true);
                }
            });
        });
    }

    //  Load the template (html file from pug source)
    //  given a path and the actual app name.
    doLoadAppTemplate(requestPath: string, appName: string) {

        config.status(`doLoadAppTemplate for path=${ requestPath }, appName=${ appName }`);
        return new Promise(async (resolve) => {

            let localPath = this.findStaticFilePath(`${ appName }/templates/`, 'index.pug');
            config.status(`doLoadAppTemplate found localPath=${ localPath }`);

            if (localPath == null) {
                localPath = this.findStaticFilePath('templates/', 'index.pug');
                config.status(`doLoadAppTemplate found using no appName localPath=${ localPath }`);
            }

            if (localPath == null) {
                config.status('Error finding local path for appName');
                return null;
            }

            config.status(`Saving to [${ requestPath }] localPath=${ localPath }`);
            await this.processPath(requestPath, [localPath + '/index.pug'], 'text/html', WebServerHelper.doCompilePugFile);
            resolve(true);
        });
    }

    swaggerMiddleware() {
        const document = swagger.loadDocumentSync(__dirname + '/../docs/api/swagger/swagger.yaml');
        this.app.use(ui(document, '/swagger'));
    }

    //  Log some request details
    loggingMiddleware() {

        return this.app.use(async (ctx, next) => {

            if (globalLogger === null) {
                globalLogger = config.getLogger('access_log');
            }

            if (globalLogger != null) {
                globalLogger.info('Request', {
                    stamp: new Date(),
                    url: ctx.url,
                    path: ctx.path,
                    headers: ctx.headers,
                    ip: ctx.ip,
                    protocol: ctx.protocol,
                    originalUrl: ctx.originalUrl,
                    query: ctx.query,
                });
            }

            return next();
        });
    }

    /**
     * Bans hacker middleware
     * Return 403 permission error when request url is matched with a pattern
     */
    banHackerMiddleWare() {
        return this.app.use(async (ctx, next) => {

            if (banLogger === null) {
                banLogger = config.getLogger('ban_log');
            }

            if (banLogger != null) {
                const reBan= /\.php|\.aspx/i;
                const m = ctx.url.match(reBan);

                if (m != null) {
                    banLogger.info('Request', {
                        stamp: new Date(),
                        url: ctx.url,
                        path: ctx.path,
                        headers: ctx.headers,
                        ip: ctx.ip,
                        protocol: ctx.protocol,
                        originalUrl: ctx.originalUrl,
                        query: ctx.query,
                    });
                    ctx.throw(403, "Permission error");
                    return true;
                }
            }

            return next();
        });
    }

    //  If there is an app name, pull it out of the URL
    findAppNameMiddleware() {
        let reApp = new RegExp('^/([a-zA-Z]+)[/?]*$');

        return this.app.use(async (ctx, next) => {

            const m = ctx.url.match(reApp);
            if (m != null) {
                config.status('findAppNameMiddleware Found app ', m[1], ' in ', ctx.url);
                let requestPath = `/${ m[1] }/`;

                let validApp = await this.doValidateAppname(m[1]);
                if (!validApp)
                {
                    return next();
                }

                let result = await this.doLoadAppTemplate(requestPath, m[1]);
                if (result != null) {

                    console.log('SETTING APPNAME:', m[1], '[', ctx.session.appName, ']');
                    ctx.session.appName = m[1];
                    if (this.staticContent[requestPath] == null || this.staticContent[requestPath].mime == null) {
                        ctx.body = `Invalid call, staticContent[${ requestPath }] not set`;
                    } else {
                        ctx.response.type = this.staticContent[requestPath].mime;
                        ctx.body          = this.staticContent[requestPath].content;
                    }

                    return true;
                } else {

                    ctx.body = `Invalid application <b>${ m[1] }</b>`;
                    return true;
                }
            }

            return next();
        });
    }

    screenMiddleware() {

        let reScreen = new RegExp('^/screens/(.*).js', 'i');
        let reView   = new RegExp('^/views/(.*).js', 'i');

        return this.app.use(async (ctx, next) => {

            let m = ctx.url.match(reScreen);
            if (m != null) {
                config.status('screenMiddleware screen javascript', m);
                ctx.body          = await compileScreen(m[1], ctx.session.appName);
                ctx.response.type = 'application/javascript';
                return true;
            }

            m = ctx.url.match(reView);
            if (m != null) {
                config.status('screenMiddleware view javascript', m);
                ctx.body          = await compileView(m[1], ctx.session.appName);
                ctx.response.type = 'application/javascript';
                return true;
            }
            return next();
        });
    }

    //  Try to find a given filename in various places and return the path
    findStaticFilePath(folderName: string, filename: string) {

        let pathList = [];

        let pathHelper = require('path');
        let appDir     = pathHelper.dirname(require.main.filename);

        pathList.push(`./Web/${ folderName }/`);
        pathList.push(`../Web/${ folderName }/`);
        pathList.push(`../../Web/${ folderName }/`);

        pathList.push(process.cwd() + `/Web/${ folderName }/`);
        pathList.push(process.cwd() + `/../Web/${ folderName }/`);
        pathList.push(process.cwd() + `/../../Web/${ folderName }/`);

        pathList.push(appDir + `/Web/${ folderName }/`);
        pathList.push(appDir + `/../Web/${ folderName }/`);
        pathList.push(appDir + `/../../Web/${ folderName }/`);
        pathList.push(appDir + `/../../../Web/${ folderName }/`);

        //  Web path relative to the ApiWebServer folder
        pathList.push(__dirname + `/Web/${ folderName }/`);
        pathList.push(__dirname + `/../Web/${ folderName }/`);
        pathList.push(__dirname + `/../../Web/${ folderName }/`);
        pathList.push(__dirname + `/../../../Web/${ folderName }/`);

        pathList.push(appDir + `/node_modules/${ folderName }/`);
        pathList.push(appDir + `/../node_modules/${ folderName }/`);
        pathList.push(appDir + `/../../node_modules/${ folderName }/`);
        pathList.push(appDir + `/../../../node_modules/${ folderName }/`);
        pathList.push(appDir + `/../../../../node_modules/${ folderName }/`);

        // Paths for the module "CoffeeNinjaCommon"
        pathList.push(__dirname + `/CoffeNinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../CoffeNinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../../CoffeNinjaCommon/${ folderName }/`);

        pathList.push(appDir + `/node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(appDir + `/../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(appDir + `/../../node_modules/NinjaCommon/${ folderName }/`);

        pathList.push(process.cwd() + `/node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(process.cwd() + `/../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(process.cwd() + `/../../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(process.cwd() + `/../../../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(process.cwd() + `/../../../../node_modules/NinjaCommon/${ folderName }/`);

        pathList.push(__dirname + `/node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../../../node_modules/NinjaCommon/${ folderName }/`);
        pathList.push(__dirname + `/../../../../node_modules/NinjaCommon/${ folderName }/`);

        let path = config.FindFileInPath(filename, pathList, true);

        if (path == null) {
            config.status(`Warning: unable to find ${ filename } in `, pathList);
            return null;
        }

        return path;
    }

    addRouteMiddleware() {

        return this.app.use(async (ctx, next) => {

            config.status('addRouteMiddleware this=', ctx.path);
            
            let parseForm = (request: koa.Request): Promise<[any, any]> => new Promise((resolve, reject) => {
                let form = new formidable.IncomingForm();
                return form.parse(request.req, function(err: Error, fields: any, files: any) {
                    if (err != null) {
                        reject(err);
                    }
                    return resolve([fields, files]);
                });
            });

            for (let i = 0; i < this.addedRoutes.length; i++) {
                const route = this.addedRoutes[i];
                
                let rePattern = new RegExp(route.pattern, 'i');
                
                let m = ctx.url.match(rePattern);
                if (m != null) {
                    let resultParse = await parseForm(ctx.request);
                    let result      = route.callback(m[0], resultParse[0], resultParse[1], m, ctx.response, ctx.request);

                    if (result != null && result.then != null && typeof result.then === 'function') {
                        result = await result;
                    }

                    ctx.body = result;
                    if (typeof result === 'object' && !(result instanceof stream.Readable)) {
                        ctx.response.type = 'application/javascript';
                    }

                    return true;
                }

            }

            return next();

        });
    }

    /**
     * Add a custom URL or Path
     * @param pathPattern {string|regex} The pattern to match
     * @param callback {function} (url, fields, files, matches)
     */
    addRoute(pathPattern: string, callback: Function) {

        this.addedRoutes.push({
            pattern: pathPattern,
            callback: callback
        });
    }
    
    /**
     * Create a ApiWebUser which will be saved in session.
     */
    async initializeWebUserSession(ctx: koa.Context) {
        let u = new ApiWebUser(++this.globalUnique, ctx.request.ip, ctx.request);
        await u.doInitUser();
        return u;
    }
    
    /**
     * handles data/datset/collection.(json|xml)
     */
    async dataPathMiddleware() {
        const reDataPath      = /^.data\/(\w+)\/(\w+)(.*).(json|xml)/i;
        return this.app.use(async (ctx: koa.Context, next: any) => {
            let path = unescape(ctx.url);
        
            let m = path.match(reDataPath);
            if (m != null && m[1] != null) {

                // if (!ctx.session.webUser) {
                ctx.session.webUser = await this.initializeWebUserSession(ctx);

                try {
                    config.dump('dataPathMiddleware Match', m);
        
                    let full      = m.shift();
                    let dataset   = m.shift();
                    let extension = m.pop();
                    path          = `/${ m.join('/') }`;
                    path          = path.replace('//', '/');
        
                    let fields: any = null;
                    if (ctx.query != null && ctx.query.fields != null) {
                        fields = {};
                        for (let a of ctx.query.fields.split(',')) {
                            fields[a] = 1;
                        }
                    }
        
                    config.dump('searching', {
                        dataset,
                        path,
                        fields,
                    });
        
                    //  TODO:  Make sure the dataset is valid first
                    //  TODO:  Check permissions
                    const data: any = {
                        dataSet  : dataset, 
                        path     : path, 
                        fieldList: fields, 
                        sortOrder: null, 
                        offset   : 0, 
                        limit    : 1000
                    };
                    
                    const result = this.gateway.doProcessApiCall('data_doGetItems', data, ctx.session.webUser);
                    let finalResult: any;

                    if (result != null && result.then != null && typeof result.then === 'function') {
        
                        finalResult = await result;
        
                        // this.doSendFlatResponse(user, uuid, newResult);
                        // this.total_api_calls_done++;
                        // return apiLog.save(this.log, newResult);
                    } else {
                        finalResult = result;
                    }

                    config.status(`dataPathMiddleware returning data for url=${ path }`);
                    if (extension === 'xml') {
                        ctx.response.type = 'application/vnd.api+xml';
                        finalResult = JSON.parse(JSON.stringify(finalResult));
                        
                        let xml = '<root>';
                        finalResult.map((item: any) => {
                            xml += `<item>${jsontoxml(item)}</item>`;
                        })
                        xml += '</root>';

                        ctx.body          = xml;
                    } else {
                        ctx.response.type = 'application/vnd.api+json';
                        ctx.body          = JSON.stringify(finalResult);
                    }
                } catch (e) {
                    config.reportException('dataPathMiddleware internal error', e);
                    config.status('dataPathMiddleware internal error', e);
                    ctx.body = `Internal error:${ e }`;
                }
                return true;
            }
        
            return next();
        });
    };
    
    /**
     * handles dynamic api call
     */
    async apiPathMiddleware() {
        const reApiPath       = /^.api\/(\w+)\/(\w+)(.*).(json|xml)/i;

        return this.app.use(async (ctx: koa.Context, next) => {
            let path = unescape(ctx.url);
            
            let m = path.match(reApiPath);
            if (m != null && m[1] != null) {

                // brian - Session not working.
                // sessions are making ctx.session.webUser an object but that object is not of a 
                // class IApiUser and those API functions not there.
                //
                //  (!ctx.session.webUser) {
                ctx.session.webUser = await this.initializeWebUserSession(ctx);

                // Check if BasicAuth is enabled.
                // If so, all calls to the server require authorization using the http "Basic Authorization" protocol.
                // That is, any request missing the Authorization header would return 403 access error.
                if (this.isBasicAuthEnabled) {
                    const authHeader = ctx.headers.authorization;
                    // parse login and password from headers
                    const b64auth = (authHeader || '').split(' ')[1] || '';
                    const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':');

                    try {
                        await this.gateway.doProcessApiCall(
                            'auth_doAuthUser',
                            [username,  password] as any,
                            ctx.session.webUser
                        );
                    } catch (err) {
                        config.status('Basic Authentication Error', err);
                        ctx.body = 'Failed to authenticate';
                    }
                }

                let key = m[1] + '_' + m[2];
                if (this.gateway.hasApiFunc(key) == null) {
                    key = m[1] + '_' + 'do' + m[2];
                }
        
                if (this.gateway.hasApiFunc(key) == null) {
                    const err = new Error(`Api path for ${key} not found.`);
                    config.reportException('apiPathMiddleware internal error', err);
                    config.status('apiPathMiddleware internal error', err);
                    ctx.body = `Internal error:${ err }`;
                } else {
        
                    try {
                        let params          = m[3].replace('/', '').split('/');
                        const callData: any = {};
                        const args          = Array.from(this.gateway.apiCallList[key]);
                        
                        for (const index in args) {
                            if ((Number(index) === args.length - 1) && key.includes('data_')) {
                                callData[args[index]] = params.join('/');
                            } else {
                                callData[args[index]] = params.shift();
                            }
                        }

                        const result = this.gateway.doProcessApiCall(key, callData, ctx.session.webUser);
                        let finalResult: any = null;

                        if (result != null && result.then != null && typeof result.then === 'function') {
                            finalResult = await result;
                        } else {
                            finalResult = result;
                        }

                        if (finalResult != null) {
                            if (finalResult.length != null) {
                                config.status(`apiPathMiddleware call Result Returned ${ finalResult.length } bytes.`);
                            } else {
                                config.status('apiPathMiddleware call Result returned unknown bytes.');
                            }
                        } else {
                            config.status('apiPathMiddleware call result null');
                        }
        
                        if (m[4] === 'xml') {
                            ctx.response.type = 'application/vnd.api+xml';
                            ctx.body          = builder.buildObject(finalResult);
                        } else {
                            ctx.response.type = 'application/vnd.api+json';
                            ctx.body          = JSON.stringify(finalResult);
                        }
                        
                    } catch (e) {
                        config.reportException('apiPathMiddleware internal error', e);
                        config.status('apiPathMiddleware internal error', e);

                        if (e instanceof EdgeError) {
                            // TODO put custom logic here for handling errors based on error code
                            const finalResult = { error: e.message, e: e.toString() };

                            if (m[4] === 'xml') {
                                ctx.response.type = 'application/vnd.api+xml';
                                ctx.body          = builder.buildObject(finalResult);
                            } else {
                                ctx.response.type = 'application/vnd.api+json';
                                ctx.body          = JSON.stringify(finalResult);
                            }

                            return;
                        }

                        ctx.body = `Internal error:${ e }`;
                    }
                }
        
                return true;
            }
        
            return next();
        });
    };
    
    /**
     * If nothing matched so far and no url exists then, redirect url to
     * ${protocal}://${hostname}/${escaped appName from hostname}?
     * So that in next redirection, findappNameMiddleware will match that appName to load
     * Web/${appName}/index.pug
     */
    hostRedirectMiddleware() {

        return this.app.use(async (ctx, next) => {
            let needRedirection = false;
            
            if (ctx.path === '/') {
                needRedirection = true;
            }

            if (needRedirection) {
                const appName: string = safeEscapeAppName(ctx.hostname);
                                
                ctx.redirect(`${ctx.origin}/${appName}`);
                return true;
            }

            return next();
        });
    }

    createConfigFilesIfNotExist() {
        // Check redirects.json for any rediredcts
        const redirectsPath = config.getDataPath(REDIRECTJSON);
        if (!fs.existsSync(redirectsPath)) {
            fs.writeFileSync(redirectsPath, JSON.stringify({}));
        }

        return true;
    }

    /**
     * Final Middleware
     */
    finalMiddleware() {

        return this.app.use(async (ctx: koa.Context, next: any) => {

            if (ctx.path === "/bayer1")
            {
                ctx.redirect("/express#StartSurvey/bayer1en_full");
                return true;
            }

            try {
                // Check redirects.json for any rediredcts
                const redirectsPath = config.getDataPath(REDIRECTJSON);

                const redirects = JSON.parse(fs.readFileSync(redirectsPath).toString());
                if (redirects[ctx.path]) {
                    ctx.redirect(redirects[ctx.path]);
                    return true;
                }
            } catch (err) {
                console.log(err);
            }

            config.status('finalMiddleware this=', ctx.path);
            ctx.body = `Unknown call to ${ ctx.path }`;
            return true;
        });
    }

    /**
     * Simple response for /status
     */
    statusMiddleware() {

        this.app.use(async (ctx: koa.Context, next: any) => {
            if (ctx.url === '/status') {
                ctx.body = 'EdgeApiServer Running';
                return true;
            }

            return next();
        });

        return true;
    }
};
