# EdgeCommonJobQueueSubmit
> Provides a small wrapper around "Job types" that enable Edge users to quickly submit a job in the
> correct JSON format to the message queue.

    npm install --save "ssh+git://git@gitlab.protovate.com:Edge/EdgeCommonJobQueueSubmit"
    import JobQueueSubmit from 'edgecommonjobqueuesubmit';

## Common Usage

Generally you will allocate a JobQueueSubmit object and call doSubmitData over and over as needed.  This will
handle connecting to the message queue and then sending the data.

    const jobQueue = new JobQueueSubmit('High');
    await jobQueue.doSubmitData("dataSet", "/collection/id/[field]", obj);

`jobQueue.doSubmitData` will resolve only when the pushing data to queue is success

The above doSubmitData returns a promise that will return true once the message has been submitted to the
job queue.  This will handle all the drain and waits required for networking, compression, and putting the
obj into the right format for EdgeServer to understand as a *"savedata"* job.

## Additional functions

* doSubmitInsert(dataSet, path, data) - Insert data to the given path, does not check for changes.

* doDeletePath(dataSet, path) - Delete one or more paths matching the pattern

* doSubmitAppend(dataSet, path, newData) - Append something to an array [] within a given path.
