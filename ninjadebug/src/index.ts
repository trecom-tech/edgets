import chalk from "chalk";
import * as moment from "moment";
import * as numeral from "numeral";

/*
* Ninja main class which is exported as a general purpose set of debugging tools
* */

let globalNinjaInstance = null;
let slice               = [].slice;

export class Ninja {
    // Format to use to display numbers, set to null for no output
    static debugNumberFormat: string = "#,###.[####]";
    // Format to use to display dates, set to null for no output
    static debugDateFormat: string   = "dddd, MMMM Do YYYY, h:mm:ss a";

    static dumpBoolean(value: any): string {
        return chalk.green(value);
    }

    static dumpNull(): string {
        return chalk.gray("<null>");
    }

    static dumpString(value: any): string {
        return "'" + chalk.magenta(value) + "'";
    };

    /*
    * Dump attempts to display a variable as if it were an assignment that you
    * can just copy/paste back into your code if needed.
    * */
    static dump(title: any, items?: any) {
        if (!items) {
            items = title;
            title = "";
        }
        if (!title) {
            title = "";
        }
        let str = chalk.white(title) + " = ";
        for (const item in items) {
            str += Ninja.dumpVar(item, "");
        }
        console.log(str);
    };

    static dumpDate(value: any): string {
        if (Ninja.debugDateFormat) {
            return chalk.yellow(moment(value).format(Ninja.debugDateFormat));
        }
        return chalk.yellow(value);
    };

    static dumpNumber(value: any): string {
        if (isNaN(value)) {
            return chalk.gray("<isNaN>");
        }
        if (Ninja.debugNumberFormat != null) {
            if (!value) {
                return Ninja.dumpNull();
            }
            return chalk.cyan(numeral(value).format(Ninja.debugNumberFormat));
        }
        return chalk.cyan(value);
    }

    static dumpObject(items: any, indent: any): string {
        let maxLen = 0;
        let str    = "";

        for (const varName in items) {
            if (varName.length > maxLen) {
                maxLen = varName.length;
            }
        }

        for (const varName in items) {
            const value = items[varName];
            let name    = varName;
            while (name.length < maxLen) {
                name = name + " ";
            }
            str += indent + name + " : ";
            if (varName === "_id") {
                str += value.toString();
            } else {
                str += Ninja.dumpVar(value, indent + "    ");
            }
            str += "\n";
        }
        return str.replace(/\n$/, "");
    };

    /*
    * Returns a text formatted value for a decimal that is a percent
    * */
    static dumpPercent(value?: any): string {
        if (!value) {
            value = 0;
        }
        return numeral(value).format("#,### %");
    }

    static dumpVar(value: any, indent: any): string {
        let all;
        let prototype;
        let str = "";
        let type;

        type = typeof value;
        if (value == null) {
            return Ninja.dumpNull();
        }

        if (type === "string") {
            return Ninja.dumpString(value);
        }

        if (type === "number") {
            return Ninja.dumpNumber(value);
        }

        if (type === "boolean") {
            return Ninja.dumpBoolean(value);
        }

        if (value.getTime != null) {
            return Ninja.dumpDate(value);
        }

        prototype = value.constructor.toString();
        if (/function Object/.test(prototype)) {
            str = "\n";
            str += Ninja.dumpObject(value, indent + "    ");
            return str;
        }

        if (/function Array/.test(prototype)) {
            all = [];
            for (let subItem in value) {
                all.push(Ninja.dumpVar(subItem, indent));
            }
            str = chalk.bold("[") + all.join(", ") + chalk.bold("]");
            return str;
        }

        if (type === "function") {
            return "Function()";
        }

        str += Ninja.dumpObject(value, indent + "    ");

        return str;
    }

    /*
    *  Log will take any number of items and output them similar to console.log
    *  except that color is added automatically and objects are dummped nicely.
    * */
    static log(items: any) {
        let level = 0;

        for (const item in items) {
            if (typeof item === "string" && level === 0) {
                level++;
                process.stdout.write(item);
            } else {
                let str = Ninja.dumpVar(item, "");
                process.stdout.write(str);
                level--;
            }
        }
        process.stdout.write("\n");
    };

    /*
    * Output a string from a number padded right to a given string length
    *
    * */
    static pad(value: any, maxSpace: number, colorFunction?: any, customFormat?: string): string {
        let str;
        if (!value) {
            value = "<null>";
        }
        if (typeof value === "number" && isNaN(value)) {
            value = "<isNaN>";
        }
        if (typeof value === "number") {
            if (customFormat) {
                str = numeral(value).format(customFormat);
            } else {
                str = numeral(value).format(Ninja.debugNumberFormat);
            }
            while (str.length < maxSpace) {
                str = " " + str;
            }
            if (colorFunction) {
                return colorFunction(str);
            }
            return chalk.cyan(str);
        }
        str = value.toString();
        while (str.length < maxSpace) {
            str = str + " ";
        }
        if (colorFunction) {
            return colorFunction(str);
        }
        return str;
    }

}

export default Ninja;
