"use strict";
// class for array data type
// @extends [DataFormatterType]
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatArray extends DataFormatterType_1.default {
    constructor() {
        super();
        this.name = 'array';
        this.align = 'left';
        this.clickable = true;
    }
    /**
     *
     * funtion to format the currently passed data
     * @param data [Object] data to be formatted
     * @param options [Object] additonal options defined for the datatype
     * @param path [String] path where the value is being edited
     * @return [Object] formatted data
     */
    format(data, options, path) {
        if ((data != null) && Array.isArray(data)) {
            if (data.length === 0) {
                return "";
            }
            const len = data.filter(a => a != null).length;
            return `View ${len}`;
        }
        else {
            return "";
        }
    }
    /**
     * funtion to unformat the currently formatted data
     * @param data [Object] data to be unformatted
     * @param path [String] path where the value is being edited
     */
    unformat(data, path) {
        return data;
    }
    /**
     *
     * @param row
     * @param value
     * @param tooltipWindow
     */
    renderTooltip(row, value, tooltipWindow) {
        if ((value == null)) {
            return false;
        }
        let width = 0;
        let height = 20;
        let str = "<table>";
        for (let index = 0; index < value.length; index++) {
            const val = value[index];
            str += "<tr><td>";
            str += (index + 1);
            str += "</td><td>";
            let temp = '';
            if (Array.isArray(val)) {
                temp = val.length + ' Records';
            }
            else if (typeof val === 'object') {
                temp = Object.keys(val).length + ' Properties';
            }
            else {
                temp = val;
            }
            str += temp;
            str += "</tr>";
            // @ts-ignore
            const tempWidth = textWidth(index + temp) + 40;
            width = width > tempWidth ? width : tempWidth;
            height += 20;
        }
        str += "</table>";
        tooltipWindow.html(str);
        tooltipWindow.setSize(width, height);
        return true;
    }
    /**
     *
     * @param e
     * @param col
     * @param data
     */
    onFocus(e, col, data) {
        // @ts-ignore
        const path = DataMap.getDataMap().engine.parsePath(e.path);
        // @ts-ignore
        removePopupView(`${path.collection} - ${col}`);
        // @ts-ignore
        return doPopupViewOnce("Table", `${path.collection} - ${col}`, `${path.collection} - ${col}`, 800, 400, `Row ${e.rn + 1}`, function (view) {
            // @ts-ignore
            DataMap.refreshTempTable(`arrayTable_${col}`, data[col], false);
            return view.loadTable(`arrayTable_${col}`);
        });
    }
}
exports.DataFormatArray = DataFormatArray;
exports.default = DataFormatArray;
//# sourceMappingURL=DataFormatArray.js.map