class ViewWysEditor extends View

    getDependencyList: ()=>
        return [ "/vendor/summernote/summernote.min.js", "/vendor/summernote/summernote.css" ]

    showEditor: ()=>
        ##|
        ##|  Placeholder, no longer needed

    ## -------------------------------------------------------------------------------------------------------------
    ## constructor to create new editor
    ##
    onShowScreen: () =>
        @wysWidget = @addDiv "wys-editor", "wys-editor-" + GlobalValueManager.NextGlobalID()
        @wysEditor = new WysEditor @wysWidget.getTag()

    ## -------------------------------------------------------------------------------------------------------------
    ## function to get the created editor instance
    ##
    ## @return [WysEditor] wysEditor
    ##
    getEditorInstance: =>
        return @wysEditor

    ##|
    ##|  get the content of the view
    getContent: ()=>
        instance = @getEditorInstance()
        if !instance?
            return null

        return instance.getContent()

    ##|
    ##|  set the content of the view
    setContent: (newContent)=>
        instance = @getEditorInstance()
        if !instance?
            setTimeout @setContent, 100, newContent
            return true

        instance.setContent(newContent)
        @content = newContent
        true

    ##
    ## set airMode of the view's editor
    setAirMode: (bool) =>
        instance = @getEditorInstance()
        if !instance?
            setTimeout @setAirMode, 100, bool
            return true

        instance.setAirMode bool
        @airMode = bool
        true

    ##
    ## Set toolbar items
    setToolbarItems: (items) =>
        instance = @getEditorInstance()
        if !instance?
            setTimeout @setToolbarItems, 100, items
            return true

        instance.setToolbarItems items
        @toolbaritems = items
        true

    ##
    ## create a new button
    createButton: (btnObj) =>
        title    = btnObj.title
        tooltip  = btnObj.tooltip
        callback = btnObj.callback
        icon     = btnObj.icon

        instance = @getEditorInstance()
        if !instance?
            setTimeout @createButton, 100, title, tooltip, callback, icon
            return true

        instance.createButton title, tooltip, callback, icon
        @buttonobj = btnObj
        true

    ##
    ## Set hotkey for summernote: e.g. "Cmd/Ctrl" + "S" to save 
    setHotkey: (keyboard, callback) =>
        instance = @getEditorInstance()
        if !instance?
            setTimeout @setHotkey, 100, keyboard, callback
            return true
        return instance.setHotkey keyboard, callback

    ##
    ## Set focus in the Editor 
    setFocus: () =>
        instance = @getEditorInstance()
        if !instance?
            setTimeout @setFocus, 100
            return true 
        return instance.setFocus()

    onResize: (w, h) =>
        if @wysEditor?
            @wysEditor.onResize w, h

    ##
    ## function being called when this view gets removed/destroyed
    removeExtraThings: () => 
        true

    serialize: () =>
        return
            content: @content
            airMode: @airMode
            button: Object.assign({}, @buttonobj)
            toolbaritems: Object.assign({}, @toolbaritems)

    ##|
    ##|  Options to setup in getLayout functions
    deserialize: (options)=>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewWysEditor: invalid data passed"

            if options.content?
                @setContent options.content

            if options.airMode? and typeof options.airMode is "boolean"
                @setAirMode options.airMode

            if options.toolbaritems and Array.isArray(options.toolbaritems) is true 
                @setToolbarItems options.toolbaritems

            if options.button? and typeof options.button is "object"
                @createButton options.button 

            resolve true
