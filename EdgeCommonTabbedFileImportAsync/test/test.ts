import TabbedFileImprotAsync from '../src/TabbedFileImportAsync';
import * as path from 'path';
import 'mocha';

const mock = require('mock-fs');
const should = require('should');

describe('Set format for a normal CSV file with a header row', () => {
    mock({
        'test.csv': 'a,b,c,d\n1,2,3,4\n'
    });
    const tfi = new TabbedFileImprotAsync('test.csv');

    it('SetFormatCSVWithHeaders', () => {
        tfi.setFormatCSVWithHeaders();
        tfi.should.have.property('hasHeader', true);
        tfi.should.have.property('seperator', 44);
        tfi.should.have.property('handleEscaped', true);
        tfi.should.have.property('skipCount', 0);
    });

    it('Validate callback function', async () => {
        let result = await tfi.readLine(null);
        result.should.equal(false);
        result = await tfi.readLine((data: any) => {/**/});
        result.should.equal(true);
        mock.restore();
    });
});

describe('Read CSV files', () => {

    it('Read a short CSV file', () => {
        const tfi       = new TabbedFileImprotAsync(path.resolve(__dirname, 'SampleCSV_11kb.csv'));
        const expected  = '81,Luxo Professional Combination Clamp-On Lamps,Beth Paige,8995,737.94,102.3,21.26,Northwest Territories,Office Furnishings,0.59';
        const startTime = new Date();
        let countRows   = 0;
        let actual: any = null;

        return tfi.readLine((data:any) => {
            countRows ++;
            if (countRows === 81) {
                actual = data[0];
            }
        }).then(() => {
            const endTime = new Date();
            const duration = (endTime.getTime() - startTime.getTime()) / 1000;
            console.log('Duration: ', duration);
            actual.should.equal(expected);
            // actual count is 101 because we return null at the end
            countRows.should.equal(101);
        });
    });

    it('Read a long CSV file', () => {
        const tfi       = new TabbedFileImprotAsync(path.resolve(__dirname, 'SampleCSV_6150kb.csv'));
        const expected  = 'Luxo Professional Combination Clamp-On Lamps,Beth Paige,8995,737.94,102.3,21.26,Northwest Territories,Office Furnishings,0.59';
        const startTime = new Date();
        let countRows   = 0;
        let actual: any = null;

        return tfi.readLine((data:any) => {
            countRows ++;
            if (countRows === 81) {
                actual = data[0];
            }
        }).then(() => {
            const endTime = new Date();
            const duration = (endTime.getTime() - startTime.getTime()) / 1000;
            console.log('Duration: ', duration);
            actual.should.equal(expected);
            // actual count is 59508 because we return null at the end
            countRows.should.equal(59508);
        });
    });

    it('Read a TSV file', () => {
        const tfi       = new TabbedFileImprotAsync(path.resolve(__dirname, 'SampleTSV.tsv'));
        const expected  = '15';
        const startTime = new Date();
        let countRows   = 0;
        let actual: any = null;

        return tfi.readLine((data:any) => {
            countRows ++;
            if (countRows === 48) {
                actual = data[0];
            }
        }).then(() => {
            const endTime = new Date();
            const duration = (endTime.getTime() - startTime.getTime()) / 1000;
            console.log('Duration: ', duration);
            actual.should.equal(expected);
            // actual count is 62 because we return null at the end
            countRows.should.equal(62);
        });
    });
});