/**
 * This is the base class for a table that is stored on a remote
 * storage technology. Right now this is coded against the Edge API for loading / saving records.
 */
export default class TableBase {
    private api;
    private isConnected;
    private marks;
    id: string;
    data: any;
    dataSet: string;
    tableName: string;
    constructor(dataSet: string, tableName: string, id: string);
    isset(path: string): boolean;
    /**
     * Returns a value give a path such as /test/data/name or test.name
     */
    safeString(path: string): any;
    safeNumber(path: string): number;
    safeBool(path: string): boolean;
    /**
     * Called after a connection is established for the first time.
     * Should be used to check indexes and such.
     */
    onVerifyModel(): Promise<boolean>;
    /**
     * You can overwrite this function to see what changed.
     */
    onSaved(totalChanges: number, newData: any): Promise<boolean>;
    /**
     * Potentially subject changes to this record.
     */
    doSave(): Promise<number | true>;
    doVerifyLoaded(): Promise<any>;
    /**
     * Execute an aggregate
     */
    doAggregate(aggList: any): Promise<any>;
    /**
     * Load a single record
     */
    doFind(condition: any): Promise<TableBase | null>;
    doRemove(condition: any): Promise<boolean>;
    doFindAll(condition?: any, sortCondition?: any, offset?: number, limit?: number): Promise<TableBase[]>;
    doVerifyIndex(keyName: string, indexType: any, isUnique: boolean): Promise<any>;
    /**
     * Called when an object is loaded to create hash values from the top level attributes
     * this way we can avoid sending them to save if they don't change
     */
    private markDataValues;
    private doConnect;
}
