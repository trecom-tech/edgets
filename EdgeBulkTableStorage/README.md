# EdgeBulkTableStorage
> The Edge interface to store common records into a bulk
> log or database.   Currently tuned for Azure Table Storage.

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeBulkTableStorage.git
    import BulkTableStorage from 'edgebulktablestorage';

## Creating an instance

    const ts = new BulkTableStorage("azureTest", "table1");

### Parameters

* configName: Pass in the config name as defined in the credential
* tableName: Indicate a azure table

## General helper functions

### Insert a record

    Return true/false value upon success

    const record = {
        name    : "Brian Pollack",
        age     : 42,
        birthday: new Date("1975-10-02 10: 04: 05"),
        income  : 17.64
    };

    const result = await ts.doInsertLogRecord("log1", record);

### Create a query
    
    Return a query object

    const query = ts.createQuery("name", "Brian Pollack", 1);

### Execute a query
    
    Return a query result

    const result:any = await ts.doQuery(query);

## Run tests locally

    npm install
    npm run test


