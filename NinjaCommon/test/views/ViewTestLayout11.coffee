class ViewTestLayout11 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        name: "viewSplitter"
        percent: 30
        top: "TestShowSize"
        bottom:
            view: "Grid"
            name: "viewGrids"
            grids: [
                view: "TestShowSize"
                name: "view1"
                title: "Grid 1"
            ,
                view: "TestShowSize"
                name: "view2"
                title: "Grid 2"
            ,
                view: "Grid"
                name: "view3"
                title: "Grid 3"
                width:  6
                height: 4                
                grids: [
                    view: "TestShowSize"
                    name: "view31"
                    title: "Grid 3-1"
                ]
            ]

    onShowScreen: ()=>
        console.log "ViewTestLayout7 onShowScreen viewSplitter=", @viewSplitter
        console.log "ViewTestLayout7 onShowScreen view1=", @view1
        console.log "ViewTestLayout7 onShowScreen view2=", @view2
        console.log "ViewTestLayout7 onShowScreen viewTGrids=", @viewGrids
