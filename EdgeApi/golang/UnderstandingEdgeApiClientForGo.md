## Description

I produced package called "apiclient" which plays role of EdgeApi Client.

It needs to be imported this module, "gosocketio", like this one before using module.

	go get github.com/graarh/golang-socketio


## Important Notes

This is client side ony and needs to be launched with EdgeApi Server.

## Using module

If we import this package, it will be created the new instance for this one and we can use it as "apiclient".

After creating instance, call setServer() to connect the server. And we will use init() and connect() to check and confirm if server is available and try to connect server.

### connect to server.

	ec, _ := apiclient.SetServer("localhost", 8001)
	ec.Init()
	ec.Connect()

## Api 

* GetApiList() : Get the list of available apis from server.
* MakeCall(callName) : Call to server by callName (string) and Get the result.
