class ScreenViewHolder extends Screen

    windowTitle      : "View Holder"
    windowSubTitle   : ""

    #
    # This must function the same as PopupWindow
    addToolbar: (buttonList)=>

        @toolbarHeight = 42

        gid = "pnav" + GlobalValueManager.NextGlobalID()
        @navBar = $ "<div />",
            id    : gid
            class : 'popupNavBar'

        @navBar.css
            height   : @toolbarHeight
            width    : "100%"

        @ViewHolderToolbar.append @navBar
        @ViewHolderToolbar.height(@toolbarHeight)
        @ViewHolderToolbar.width("100%");

        @toolbar = new DynamicNav("#" + gid)
        for button in buttonList
            @toolbar.addElement button
        @toolbar.render()
        true

    onSetupButtons: ()=>
        #
        # Emit events as required by popup controller
        GlobalClassTools.addEventManager(this)

    onShowScreen: (args)=>
        console.log "ViewHolder Loaded, args=", args
        $("#MainTitle").html args.args
        @windowTitle = args.args