import MongoGateway from './MongoGateway';
import { Collection, FilterQuery, Cursor } from 'mongodb';
export declare class MongoModel {
    name: string;
    gateway: MongoGateway;
    theCollection: Collection;
    internalCollectionPromise: Promise<Collection>;
    /**
     *
     * @param name [string] collectionName
     * @param gateway MongoGateway object
     */
    constructor(name: string, gateway: MongoGateway);
    /**
     * Connects to the database and gets a reference to the table
     * which is a collection in mongo terms
     *
     */
    doInternalGetCollection(): Promise<Collection<any>>;
    doAggregate(aggList: any): Promise<any[]>;
    /**
     * Given a specific field, return the unique values and counts.
     * @param fieldName
     * @param matchCondition
     */
    doGetUniqueValues(fieldName: any, matchCondition?: any): Promise<any[]>;
    doUpdateOne(searchCondition: any, setCondition: any, isMulti?: boolean): Promise<import("mongodb").UpdateWriteOpResult>;
    doInsertMany(newDocuments: any): Promise<import("mongodb").InsertWriteOpResult>;
    doInsertOne(newDocument: any): Promise<import("mongodb").InsertOneWriteOpResult>;
    doRemove(condition: any): Promise<import("mongodb").WriteOpResult>;
    /**
     * Insert or update a document with a given ID
     * @param value
     * @param newDataObj
     */
    doUpsertOne(value: any, newDataObj: any): Promise<boolean>;
    doAppend(condition: any, subPath: any, newData: any): Promise<false | import("mongodb").WriteOpResult>;
    doFindAndInsert(condition: any, newData: any, attemptCount?: number): Promise<any>;
    doInsertOrUpdate(condition: any, newData: any): Promise<import("mongodb").WriteOpResult>;
    doFindByID(value: any, createIfNeeded?: boolean, optionalPath?: string[], returnFullDetail?: boolean): Promise<any>;
    /**
     * Count the results of a search before searching
     * @param condition
     */
    doCount(condition: any): Promise<number>;
    doFindNearbyIds(conditions: any, location: any, miles: any, _offset: number, limit: number, fieldList?: any): Promise<any[]>;
    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param conditions [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles [number] the number of miles to search around
     * @param offset [number] Optional starting point in the result list (default 0)
     * @param limit [number] Optional the maximum number of results (default 1000)
     */
    doFindNearby(conditions: any, location: any, miles: any, offset: number, limit: number): Promise<{}>;
    /**
     * Do find on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [object] condition to apply find
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doFind(condition: any, fieldList?: any, sortCondition?: any, offset?: number, limit?: number): Promise<any[]>;
    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetCursor(condition: FilterQuery<any>, fieldList: any, sortCondition: any, offset: number, limit: number): Promise<Cursor<any>>;
    doStream(condition: any, fieldList: any, dateFieldName: any, _lastModified: any, limit: number, callback: Function): Promise<{}>;
    doTail(condition: any, fieldList: any, offset: number, limit: number, callback: Function, sortCondition?: any): Promise<{}>;
    doCreateIndex(indexName: any, indexOptions: any): Promise<void>;
}
export default MongoModel;
