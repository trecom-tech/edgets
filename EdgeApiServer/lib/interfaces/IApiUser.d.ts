import { IUser } from './IUser';
import { EdgeApiProvider } from 'edgeapi';
import ApiProvider from '../ApiProvider';
export declare abstract class IApiUser extends EdgeApiProvider {
    myConnectionID: number;
    myName: number;
    ipAddress: string;
    rec: IUser;
    authAttempts: number;
    timezoneOffset: number;
    system: string;
    /**
     * Run one-time initial code after the user object is created.
     */
    doInitUser(): Promise<void>;
    addRoles(roles?: string[], system?: string): boolean;
    removeRoles(roles?: string[], system?: string): boolean;
    hasRole(roleName: string, targetSystem?: string): boolean;
    doChangePassword(newPassword: string): boolean;
    doAddTimeline(provider: ApiProvider, timelineData: any): Promise<import("edgeapi").EdgeSocketResponseData>;
    abstract addSubscription(data: any): boolean;
}
export default IApiUser;
