import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatSimpleObject extends DataFormatterType {
    name: string;
    clickable: boolean;
    align: string;
    constructor(...args: any[]);
    format(data: any, options: any, path: any): string;
    unformat(data: any, path: any): any;
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
    onFocus(e: any, col: any, data: any): any;
}
export default DataFormatSimpleObject;
