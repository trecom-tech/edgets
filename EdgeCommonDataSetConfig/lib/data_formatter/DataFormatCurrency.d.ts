import DataFormatterType from './DataFormatterType';
export declare class DataFormatCurrency extends DataFormatterType {
    name: string;
    align: string;
    input_width: number;
    constructor();
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    attachEditor(elSourceField: any, path: any): any;
}
export default DataFormatCurrency;
