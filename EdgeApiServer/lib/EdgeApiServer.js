"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const ApiSocketServer_1 = require("./ApiSocketServer");
const ApiWebServer_1 = require("./ApiWebServer");
const AddServiceUser_1 = require("./utility/AddServiceUser");
// The EdgeApiServer exposes the information stored at the edge using
// multiple API methods.   Those include
// WebSockets => Using Socket.io
// REST API   => Using KOA
// MQ         => Using Rabbit MQ (AQMP 0.9)
// TODO:
// 1- Add ZeroMQ Support again
// 2- Add SOAP Support and WSDL generation
// One instance of this server maintains a Mongo connection and
// incoming connections.  This can be run on many hosts
function startServer(isBasicAuthEnabled = false, strRealm = '') {
    // Add default service user while starting server
    AddServiceUser_1.addServiceUser()
        .then((res) => {
        console.log('Finished adding service user!!!');
    })
        .catch((err) => {
        console.log(err);
    });
    edgecommonconfig_1.default.status(`Web server enabled on ${edgecommonconfig_1.default.WebserverPort}`);
    let apiWebServer = new ApiWebServer_1.default();
    if (isBasicAuthEnabled) {
        apiWebServer.enableBasicAuth(strRealm);
    }
    edgecommonconfig_1.default.status('Creating ApiSocketServer, Socket.io listener enabled');
    let apiSocketServer = new ApiSocketServer_1.default(apiWebServer);
    edgecommonconfig_1.default.setTitle('Edge API Server');
    return apiSocketServer;
}
exports.startServer = startServer;
var ApiProvider_1 = require("./ApiProvider");
exports.ApiProvider = ApiProvider_1.default;
var ApiSocketUser_1 = require("./ApiSocketUser");
exports.ApiSocketUser = ApiSocketUser_1.default;
var EdgeError_1 = require("./EdgeError");
exports.EdgeError = EdgeError_1.EdgeError;
//# sourceMappingURL=EdgeApiServer.js.map