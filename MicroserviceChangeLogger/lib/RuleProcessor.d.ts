export default class RuleProcessor {
    rule: any;
    test_condition: any;
    log: any;
    active: boolean;
    readonly limit: number;
    constructor(rule: any);
    /**
     * Report an error and deactive the rule
     */
    setError(txt: string): void;
    /**
     * Check to see if a change matches any rules
     */
    check(job: any): boolean;
    onExecute(job: any): void;
    onInit(): void;
}
