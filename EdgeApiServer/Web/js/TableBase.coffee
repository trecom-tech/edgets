#
# This is the base class for a table that is stored on a remote
# storage technology.
#

window.globalTableBaseCache = {}

class TableBase

    #
    #---- Events you can overwrite in the subclass ----
    #

    #
    # Called after a connection is established for the first time.
    # Should be used to check indexes and such
    onVerifyModel : ()=>
        return true

    constructor: (@dataSet, @tableName, @useDataMap, @useCache) ->

        if !@useDataMap?
            @useDataMap = false

        if !@useCache?
            @useCache = true

        if @useCache
            if !window.globalTableBaseCache[@dataSet]?
                window.globalTableBaseCache[@dataSet] = {}

            if !window.globalTableBaseCache[@dataSet][@tableName]?
                window.globalTableBaseCache[@dataSet][@tableName] = new LocallyCachedTable(@dataSet, @tableName)

            @cacheTimeout = 90


    #
    # Potentially subject changes to this record
    #
    doSave: ()=>

        newPromise ()=>

            if !@id then throw Error "Invalid call to doSave on #{@tableName}, no id"
            if !@data? then throw Error "Invalid call to doSave on #{@tableName}, id=#{@id}, not loaded"

            yield sock.data_doUpdatePath @dataSet, "/#{@tableName}/#{@id}", @data
            true

    #
    # Quickly make sure the item is loaded for this record
    #
    doVerifyLoaded: ()=>

        if @verifyLoadedPromise?
            return @verifyLoadedPromise

        if !@id?
            throw new Error "Can't load record, id is not set"

        @verifyLoadedPromise = newPromise ()=>
            @data = yield sock.data_doGetItem @dataSet, "/#{@tableName}/#{@id}"
            return @data

    #
    # Load a single record
    #
    doFind: (@id) ->
        return @doVerifyLoaded()

    doRemove: (condition) ->

        newPromise ()=>

            yield sock.data_doRemove @dataSet, @tableName, condition
            return true

    #
    # Find all the records in a given table with a condition 
    # matching a specific search conditions
    #
    doFindAll: (condition, sortCondition, offset, limit) ->

        className =  @constructor.name

        if @useDataMap
            DataMap.removeTableData @tableName

        newPromise ()=>

            if !condition?
                condition = ""

            if typeof condition == "object"
                str = ""
                for keyName, keyVal of condition
                    str = str + keyName + ":" + keyVal
                condition = str

            if !sortCondition?
                sortCondition = { }

            if !offset?
                offset = 0

            if !limit?
                limit = 1000

            if @useCache

                searchResults = yield sock.data_doGetItems @dataSet, "/#{@tableName}/#{condition}", { _id: 1, id: 1, _lastModified: 1 }, sortCondition, offset, limit

                idlist = []
                idlist.push item.id for item in searchResults

                yield window.globalTableBaseCache[@dataSet][@tableName].doGetMany "Loading #{@tableName} data from server", idlist, @cacheTimeout

                all = []
                for id in idlist
                    all.push window.globalTableBaseCache[@dataSet][@tableName].data[id]

            else

                globalBusyDialog.showBusy "Downloading records"
                all = yield sock.data_doGetItems @dataSet, "/#{@tableName}/#{condition}", null, sortCondition, offset, limit

            results = []
            counter = 0
            for obj in all

                if !obj? or !obj.id?
                    console.log "Warning: invalid obj in results from /#{@tableName}/#{condition}"
                    continue

                oneItem            = {}
                oneItem.tableName  = @tableName
                oneItem.dataSet    = @dataSet
                oneItem.useDataMap = @useDataMap
                oneItem.data       = obj

                # oneItem = new @constructor(obj.id)
                # oneItem.tableName = @tableName
                # oneItem.dataSet = @dataSet
                # oneItem.useDataMap = @useDataMap
                # oneItem.data = obj

                if @useDataMap
                    # console.log "TableBase doFindAll #{@tableName} Adding:", obj.id, obj
                    DataMap.addDataUpdateTable @tableName, obj.id, obj
                    # console.log JSON.stringify(obj)

                results.push oneItem

            globalBusyDialog.finished()
            return results

    #
    # Create/Verify an index
    doVerifyIndex: (keyName, indexType, isUnique) =>

        newPromise ()=>

            result = yield sock.data_doVerifySimpleIndex(@dataSet, @tableName, keyName, indexType, isUnique)
            return result

    #
    #---------------------------------------------------------------
    #Functions related to IndexDB storage in the browser
    #---------------------------------------------------------------
    #
    doFindAllWithCache: (condition, sortCondition, offset, limit, expireTime)=>

        newPromise ()=>

            server = yield @doOpenLocalDatabase()

            cacheKey = JSON.stringify
                ds: @dataSet
                t: @tableName
                c: condition
                s: sortCondition
                o: offset
                l: limit

            cacheKey = cacheKey.replace /[{}\"]/g, ""

            # console.log "Searching for:", cacheKey
            results = yield server.cache.query('_key').only(cacheKey).execute()
            if results? and results.length? and results.length > 0
                cachedResults = []
                for result in results

                    # console.log "CACHE for #{cacheKey}: ", result

                    oneItem = new @constructor(result.data.id)
                    oneItem.data = result.data
                    oneItem.verifyLoadedPromise = @dummyPromise
                    oneItem.tableName = @tableName
                    oneItem.dataSet = @dataSet
                    oneItem.useDataMap = @useDataMap
                    cachedResults.push oneItem

                return cachedResults

            results = yield @doFindAll(condition, sortCondition, offset, limit)
            for result in results
                server.cache.add
                    _key: cacheKey
                    data: result.data

            console.log "SERVER RESULS:", results
            return results

    #
    # Open a local database to store samples and fields
    #
    doOpenLocalDatabase: ()=>

        if @localDatabasePromise? then return @localDatabasePromise
        @localDatabasePromise = new Promise (resolve, reject)=>

            serverName = "rrv3tc_cache"

            # @doDeleteLocalDatabase()

            db.open
                server: serverName
                version: 1
                schema:
                    cache:
                        key: {keyPath: '_local_id', autoIncrement:  true}
                        indexes:
                            id: { }
                            _key: { }

            .then (server)=>
                resolve(server)

    #
    # Delete the local storage database for this table
    #
    @doDeleteLocalDatabase: ()->

        new Promise (resolve, reject) ->

            serverName = "rrv3tc_cache"

            db.delete serverName
            .then (ev)=>
                console.log "DELETE RESULT [#{@serverName}]:", ev
                resolve(ev)

            .catch (err)=>
                console.log "CATCH DELETE  [#{@serverName}]:", err
                reject(err)

