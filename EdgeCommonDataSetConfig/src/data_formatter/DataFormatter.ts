import { DataFormatCommon }       from './DataFormatCommon';
import { DataFormatText }         from './DataFormatText';
import { DataFormatInt }          from './DataFormatInt';
import { DataFormatNumber }       from './DataFormatNumber';
import { DataFormatCurrency }     from './DataFormatCurrency';
import { DataFormatDate }         from './DataFormatDate';
import { DataFormatDateTime }     from './DataFormatDateTime';
import { DataFormatFloat }        from './DataFormatFloat';
import { DataFormatDateAge }      from './DataFormatDateAge';
import { DataFormatEnum }         from './DataFormatEnum';
import { DataFormatDistance }     from './DataFormatDistance';
import { DataFormatBoolean }      from './DataFormatBoolean';
import { DataFormatPercent }      from './DataFormatPercent';
import { DataFormatTimeAgo }      from './DataFormatTimeAgo';
import { DataFormatSimpleObject } from './DataFormatSimpleObject';
import { DataFormatSourceCode }   from './DataFormatSourceCode';
import { DataFormatTags }         from './DataFormatTags';
import { DataFormatMultiselect }  from './DataFormatMultiselect';
import { DataFormatDuration }     from './DataFormatDuration';
import { DataFormatLink }         from './DataFormatLink';
import { DataFormatImageList }    from './DataFormatImageList';
import { DataFormatMemo }         from './DataFormatMemo';
import { DataFormatNumberSystem } from './DataFormatNumberSystem';
import { DataFormatArray }        from './DataFormatArray';
import { DataFormatPassword }     from './DataFormatPassword';
import { DataFormatIcon }         from './DataFormatIcon';

const formatType: any = {
    DataFormatText,
    DataFormatInt,
    DataFormatNumber,
    DataFormatCurrency,
    DataFormatDate,
    DataFormatDateTime,
    DataFormatFloat,
    DataFormatDateAge,
    DataFormatEnum,
    DataFormatDistance,
    DataFormatBoolean,
    DataFormatPercent,
    DataFormatTimeAgo,
    DataFormatSimpleObject,
    DataFormatSourceCode,
    DataFormatTags,
    DataFormatMultiselect,
    DataFormatDuration,
    DataFormatLink,
    DataFormatImageList,
    DataFormatMemo,
    DataFormatNumberSystem,
    DataFormatArray,
    DataFormatPassword,
    DataFormatIcon
};

if (typeof window !== 'undefined' && window !== null) {
    // #|
    // #|  Only if loaded in a browser
    // @ts-ignore
    window['textWidth'] = (text: any, font: any) => {
        // @ts-ignore
        if (!textWidth.fakeEl) {
            // @ts-ignore
            textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            // @ts-ignore
            textWidth.fakeEl.css('font-size', '14px');
            // @ts-ignore
            textWidth.fakeEl.css('white-space', 'nowrap');
            // @ts-ignore
            textWidth.fakeEl.css('overflow-x', 'auto');
            // @ts-ignore
            textWidth.fakeEl.css('position', 'absolute');
        }
        // @ts-ignore
        textWidth.fakeEl.text(text);
        // @ts-ignore
        return textWidth.fakeEl.width();
    };
}

export class DataFormatter {
    formats: any = {};
    common: any;

    getNumber(data: any) {
        return this.common.getNumber(data);
    }

    getMoment(data: any, format: any) {
        return this.common.getMoment(data, format);
    }

    // ###
    // ### function to register the data formatter class
    // ###
    // ### @param [DataFormatterType] formattingClass
    // ### @return null
    // ###
    register(formattingClass: any) {
        return this.formats[formattingClass.name] = formattingClass;
    }

    // ###
    // ### function to get the data formatter from registered formatter
    // ###
    // ### @param [String] dataTpe data type name of the desired formatter class
    // ### @note fires error if formatter not found
    // ### @return DataFormatterType
    // ###
    getFormatter(dataType: any) {
        if (!this.formats[dataType]) {
            console.log("Registered types:", this.formats);
            throw new Error(`Invalid type: ${dataType}`);
        }

        return this.formats[dataType];
    }

    // ###
    // ### Format some data based on the type and
    // ### return just the formatted value, not the style or other details.
    // ###
    // ### @param [String] dataType data type name for which formatter is running
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additional options to apply formatting
    // ### @param [String] path the current path at which the formatter is running
    // ### @return [Object] value formatted data using data type formatter
    // ###
    formatData(dataType: any, data: any, options?: any, path?: any) {
        let value;
        if ((this.formats[dataType] == null)) {
            console.log("Registered types:", this.formats);
            return `Invalid type [${dataType}]`;
        }

        return value = this.formats[dataType].format(data, options, path);
    }

    // ###
    // ### UnFormat some data based on the type and
    // ### return just the unformatted value, not the style or other details.
    // ###
    // ### @param [String] dataType data type name for which formatter is running
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additional options to apply formatting
    // ### @param [String] path the current path at which the formatter is running
    // ### @return [Object] value unformatted data using data type formatter
    // ###
    unformatData(dataType: any, data: any, options?: any, path?: any) {
        let value;
        if ((this.formats[dataType] == null)) {
            return `Invalid type [${dataType}]`;
        }
        return value = this.formats[dataType].unformat(data, options, path);
    }

    // ###
    // ### constructor
    // ###
    constructor() {
        this.common = new DataFormatCommon();

        for (const key in formatType) {
            const type = formatType[key];
            this.register((new type));
        }
    }
}

export default DataFormatter;