# EdgeApiServer /Code

This folder is a web module that allows a developer to review dynamic code blocks, edit them, test them,
watch for errors, and manage them.
