import { config } from '../src/Config';

const fs = require('fs');

//
//  Simple test case should print an exception within this file

const log = config.getLogger("test2");
log.error("Internal error");

const str1: any = "Hello";
const number1   = 123;

const testFunction = () =>
    new Promise((resolve, reject) => {
        str1.callFunctionThatDoesntExist();
        resolve(false);
    })
;

testFunction()
    .then((res) => {
        console.log(res)
    })
    .catch((err) => {
        console.log(err);
    })
const number2 = 123;
const number3 = 123;