import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatDistance extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    constructor(...args: any[]);
    format(data: any, options: any, path: any): string | 0;
    unformat(data: any, path: any): any;
}
export default DataFormatDistance;
