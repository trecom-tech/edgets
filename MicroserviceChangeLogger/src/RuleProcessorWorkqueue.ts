import EdgeApi                  from 'edgeapi';
import { config }               from 'edgecommonconfig';
import RuleProcessor            from './RuleProcessor';

export default class RuleProcessorWorkqueue extends RuleProcessor {
    
    api         : any;
    lastRecord  : any;

    onInit() {
        this.lastRecord = null;

        if (!this.rule.wq_name) {
            this.setError('Invalid Workqueue or missing "wq_name"');
        }

        EdgeApi.doGetApi()
        .then((api) => {
            this.api = api;
        })
        .catch((err) => {
            console.log(err);
        });
    }

    onExecute(job: any, count: number = 0) {
        config.status('WorkQueue Match:', job);

        if (!this.api) {
            config.status('WorkQueue Match, job is being postponed until api is available');

            if (count === this.limit) {
                throw new Error('RuleProcessorWorkqueue onEexecute, api not defined');
            } 

            // No API defined, postpone this rule
            setTimeout(() => {
                this.onExecute(job);
            }, 1000);

            return;
        }

        // Don't resend the last request
        if (this.lastRecord === job.record_id) {
            return;
        }

        this.lastRecord = job.record_id;
        this.api.workqueue_doAddRecord(this.rule.wq_name, job.record_id);
    }
}