/// <reference types="node" />
export declare class MultipartParser {
    x: number;
    y: number;
    nlcount: number;
    max_len: number;
    mode: string;
    current: string;
    start: number;
    items: any[];
    currentItem: any;
    boundary: string;
    bodyBuffer: Buffer;
    constructor(boundary: string, bodyBuffer: Buffer);
    nextChar(): number;
    moveToEndOfLine(): string;
    parseHeaders(): boolean;
    getBody(): boolean;
    findBoundary(): boolean;
    doParse(): number[];
}
export default MultipartParser;
