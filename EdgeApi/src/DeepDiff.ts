const arrayClone = function (a: any[]) {
    let result: any[] = [];
    for (let item of a) {
        result.push(item);
    }
    return result;
};

const deepDiffArray = function (source: any[], dest: any[], basePath?: any[]) {
    // Dest has a list of elements, see if each element is in the source
    // and if so, see if it's new or deleted or changed

    let diffs              = [];
    let foundKeys: any     = {};
    let foundKeysDest: any = {};

    for (let otherIdx in source) {
        let otherObj = source[otherIdx];
        if (typeof otherObj === 'function') {
            continue;
        }

        let strKey        = JSON.stringify(otherObj);
        foundKeys[strKey] = otherIdx;
    }

    for (let idx in dest) {
        let obj = dest[idx];
        if (typeof obj === 'function') {
            continue;
        }

        let strDestRecord = JSON.stringify(obj);
        if (foundKeys[strDestRecord] == null) {
            diffs.push(strDestRecord);
        }

        foundKeysDest[strDestRecord] = 1;
    }

    return diffs;
};

const deepDiff = function (source: any, dest: any, basePath: any[]) {
    let d;
    let keyVal;
    let tempList;
    let diffs: any[] = [];

    const appendDiff = function (
        kind: string,
        lhs: string,
        rhs: string,
        path: any[],
    ) {
        // if kind == "D" and (lhs == '' and rhs == null) then return
        if (path.length === 1 && path[0] === 'id') {
            return null;
        }
        if (kind === 'N' && (rhs === null || rhs === '')) {
            return null;
        }

        return diffs.push({
            kind,
            lhs,
            rhs,
            path: arrayClone(path),
        });
    };

    if (basePath == null) {
        basePath = [];
    }

    if (typeof basePath !== 'object') {
        basePath = [basePath];
    }

    if (source == null) {
        source = {};
    }
    if (dest == null) {
        dest = {};
    }

    if (basePath.length > 6) {
        return [];
    }

    for (const keyName in dest) {
        keyVal = dest[keyName];
        if (typeof keyVal === 'function') {
            continue;
        }
        if (keyName === '_id') {
            continue;
        }
        if (keyName === '_lastModified') {
            continue;
        }

        basePath.push(keyName);

        if (Array.isArray(keyVal)) {
            if (
                source[keyName] == null ||
                typeof source[keyName] !== 'object'
            ) {
                tempList = deepDiffArray([], keyVal, basePath);
            } else {
                tempList = deepDiffArray(source[keyName], keyVal, basePath);
            }

            if (tempList != null && tempList.length > 0) {
                diffs.push({
                    kind: 'NE',
                    rhs : tempList,
                    path: arrayClone(basePath),
                });
            }
        } else if (keyVal instanceof Date) {
            if (source[keyName] == null || source[keyName].getTime == null) {
                appendDiff('N', null, keyVal.toString(), basePath);
            } else {
                if (source[keyName].getTime() !== keyVal.getTime()) {
                    appendDiff(
                        'E',
                        source[keyName].toString(),
                        keyVal.toString(),
                        basePath,
                    );
                }
            }
        } else if (typeof keyVal === 'object') {
            if (keyVal === null) {
                // delete by forced null set
                appendDiff('D', source[keyName], null, basePath);
            } else {
                // Dig deeper Add
                if (source[keyName] == null) {
                    tempList = deepDiff({}, keyVal, basePath);
                } else {
                    tempList = deepDiff(source[keyName], keyVal, basePath);
                }

                for (d of tempList) {
                    diffs.push(d);
                }
            }

            // if a key is new then add it
        } else if (source[keyName] == null) {
            appendDiff('N', null, keyVal, basePath);
        } else {
            let strCompare1 = keyVal;
            let strCompare2 = source[keyName];

            if (
                typeof strCompare1 === 'object' &&
                typeof strCompare2 === 'object'
            ) {
                strCompare1 = keyVal.toString();
                strCompare2 = source[keyName].toString();
            }

            if (strCompare1 !== strCompare2) {
                appendDiff('E', source[keyName], keyVal, basePath);
            }
        }

        basePath.pop();
    }

    for (const keyName in source) {
        keyVal = source[keyName];
        if (typeof keyVal === 'function') {
            continue;
        }
        if (keyName === '_id') {
            continue;
        }
        if (keyName === '_lastModified') {
            continue;
        }

        basePath.push(keyName);

        if (typeof keyVal === 'object') {
            if (Array.isArray(keyVal)) {
                // Skip arrays if only in the source
            } else if (dest[keyName] == null) {
                // Instead of deleteing just the sub items, delete the entire tree
                // tempList = deepDiff keyVal, {}, basePath
                // console.log "Append Diff 1:, #{keyVal}"
                // appendDiff "D", keyVal, null, basePath
            } else {
                tempList = deepDiff(keyVal, dest[keyName], basePath);
                for (d of tempList) {
                    diffs.push(d);
                }
            }
        }

        // else if !dest[keyName]?
        // Don't delete in this case
        // console.log "Here no dest[#{keyName}]"
        // appendDiff "D", keyVal, null, basePath

        basePath.pop();
    }

    return diffs;
};

export default deepDiff;
