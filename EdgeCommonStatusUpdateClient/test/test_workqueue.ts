import * as os from 'os';
import StatusUpdateClient from '../src/StatusUpdateClient';

/*
Test using Workqueue update messages
see ProviderWorkqueue
*/

const statusUpdateClient = new StatusUpdateClient("testing", "detaultType");
statusUpdateClient.doOpenChangeMessageQueue()
.then(()=> {

    console.log("Status Update Client Connected");
    return setInterval(()=> {

        const data: any = {};
        data.name       = "test";
        data.host       = os.hostname();
        data.type       = "workqueue";
        data.total_in   = 10;
        data.total_out  = 20;
        data.total_pend = 30;
        data.total_hits = 40;
        data.stamp      = new Date();
        statusUpdateClient.sendStatus(data);
        console.log("Sending:", data);
    }

    , 1000);
});