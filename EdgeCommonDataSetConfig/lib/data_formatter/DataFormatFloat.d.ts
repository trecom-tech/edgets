import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatFloat extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    constructor(...args: any[]);
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    attachEditor(elSourceInput: any, path: any): any;
    allowKey(keyCode: number): boolean;
}
export default DataFormatFloat;
