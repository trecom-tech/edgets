import fs = require('fs');

const brandsFile = fs.readFileSync('ninja/vendor/fontawesome-pro-5.0.0/web-fonts-with-css/webfonts/fa-brands-400.svg');
let reIcon = /(glyph\-name\=\")(.*)\"/;
const brands = brandsFile.toString();
let brandsList = [];
for (const line of brands.split('\n')) {
    let mat = line.match(reIcon);
    if (mat != null && mat[2] != null) {
        brandsList.push(mat[2]);
    }
}

const dataFile = fs.readFileSync('ninja/vendor/fontawesome-pro-5.0.0/web-fonts-with-css/less/_variables.less');
reIcon = /(\@fa\-var\-)(.*)(\:.*)/;
const data = dataFile.toString();
let all = [];
for (const line of data.split('\n')) {
    let m = line.match(reIcon);
    if (m != null && m[2] != null) {
        if (brandsList.indexOf(m[2]) === -1) {
            all.push(`fa-${ m[2] }`);
        } else {
            console.log(m[2]);
        }
    }
}

let list = all.sort();

fs.writeFileSync('ninja/vendor/icons.json', JSON.stringify(list));
