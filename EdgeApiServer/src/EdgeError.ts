
export class EdgeError extends Error {
    public message: string;
    public code: string;
    public status: number;
    public options: any;

    constructor(message: string, code: string, status: number = 400, options: any = {}) {

        super(message);

        this.message = message;
        this.code = code;
        this.status = status;
        this.options = options;

        Error.captureStackTrace(this, this.constructor);
    }
}

export default EdgeError;
