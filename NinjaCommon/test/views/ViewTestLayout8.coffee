class ViewTestLayout8 extends View

    getLayout: ()=>
        view: "Docked"
        name: "viewDocked"
        docked:
            view: "TestShowSize"
            name: "viewFixedSize"
        main:
            view: "DynamicTabs"
            tabs: [
                title: "Top tab 1"
                name: "view1"
                icon: "fa-circle"
                view: "DynamicTabs"
                tabs: [
                    title: "Apple"
                    view: "TestShowSize"
                ,
                    title: "Ball"
                    location: "right"
                    size: 300
                    view: "Docked"
                    dock: "TestShowSize"
                    main:
                        name: "viewBall"
                        view: "TestShowSize"
                ]
            ,
                title: "Top tab 2"
                name: "view2"
                icon: "fa-arrow-right"
                view: "DynamicTabs",
            ,
                title: "Top tab 3"
                name: "view3"
                view: "DynamicTabs"
            ]
        location: "bottom"
        size: 300

    onShowScreen: ()=>
        console.log "ViewTestLayout8 onShowScreen viewFixedSize=", @viewFixedSize
        console.log "ViewTestLayout8 onShowScreen view1=", @view1
        console.log "ViewTestLayout8 onShowScreen view2=", @view2
        console.log "ViewTestLayout8 onShowScreen viewTabs=", @viewTabs
        console.log "ViewTestLayout8 onShowScreen viewBall=", @viewBall

