"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const passwordUtil = require("./Password");
const edgedatasetmanager_1 = require("edgedatasetmanager");
exports.DEFAULT_SERVICE_USER = {
    apiUser: 'edgeApi',
    apiPassword: 'Hr^YQ5yh!2eM;QbV'
};
function addServiceUser(args = exports.DEFAULT_SERVICE_USER) {
    return __awaiter(this, void 0, void 0, function* () {
        const serviceGroup = {
            id: 'EdgeApiGroup',
            name: 'EdgeApiGroup',
            description: 'Test Group for Authenticating serviceUsers',
            roles: {
                test: [
                    'serviceUser'
                ],
                global: [
                    'serviceUser'
                ],
            },
            members: [
                args.apiUser,
            ],
        };
        const serviceUser = {
            id: args.apiUser,
            password: passwordUtil.generatePasswordHash(args.apiPassword),
            name: 'Service User',
            first_modified: new Date(),
            last_modified: new Date(),
            active: true,
            email: 'serviceuser@protovate.com',
            roles: {
                test: [
                    'serviceUser'
                ],
                global: [
                    'serviceUser'
                ],
            }
        };
        const dsLookup = new edgedatasetmanager_1.DataSetManager('lookup');
        const groupCollection = yield dsLookup.doGetCollection("groups");
        const existingGroup = yield groupCollection.doFindByID(serviceGroup.id);
        if (existingGroup) {
            // Dont add if group already exists, justlog that user already exists;
            console.log(`Group(${serviceGroup.id}) already exists`);
        }
        else {
            yield groupCollection.doUpsertOne(serviceGroup.id, serviceGroup);
        }
        const dsMaster = new edgedatasetmanager_1.DataSetManager('master');
        const userCollection = yield dsMaster.doGetCollection("user");
        const existingUser = yield userCollection.doFindByID(args.apiUser);
        if (existingUser) {
            // Dont add if user already exists, justlog that user already exists;
            console.log(`User(${args.apiUser}) already exists`);
        }
        else {
            yield userCollection.doUpsertOne(args.apiUser, serviceUser);
        }
        return true;
    });
}
exports.addServiceUser = addServiceUser;
//# sourceMappingURL=AddServiceUser.js.map