const stylus       = require('stylus');
const nib          = require('nib');
const pug          = require('pug');
const chalk        = require('chalk');
const version      = require('project-version');
const coffeeScript = require('coffeescript');
const less         = require('less');

import *      as   os from 'os';
import *      as   fs from 'fs';
import config         from 'edgecommonconfig';

//  Set to true when you want to know when requsts are not found.
let globalDebugMissingFiles = false;

//  Helper Class
export default class WebServerHelper {

    /**
     * Given a filename and path list, resolve with the full path that exists.
     * @param filename
     * @param pathList
     * @param fake
     */
    static async doFindFileInPath(filename: string, pathList: string[] | string) {
        let result = config.FindFileInPath(filename, pathList, false);
        if (result == null) {
            if (globalDebugMissingFiles) {
                console.log(`File '${ filename }' ${ filename.length } not found in :`, JSON.stringify(pathList));
            }
            return null;
        }

        return result;
    }

    static doCompileLessFiles(filenameList: string[] | string): Promise<{ css: any }> {

        return new Promise(function(resolve, reject) {

            try {

                let strContent = '';
                for (let filename of filenameList) {
                    strContent += fs.readFileSync(filename);
                }

                return less.render(strContent, { compress: false }, (err: Error, output: any) => {

                    if (err != null) {
                        console.log('LESS Compile Error:', err);
                    }

                    resolve(output);
                });
            } catch (e) {

                console.log(`doCompileLessFile, file not found ${ filenameList }`);
                resolve({ css: '' });
            }
        });
    }

    static doCompileStylusFile(filename: string): Promise<string> {

        return new Promise(function(resolve, reject) {

            if (filename == null) {
                resolve('');
            }

            return fs.readFile(filename, 'utf8', function(_err, content) {
                
                try {
                    return stylus(content).set('filename', filename).set('compress', false).use(nib()).render((err: Error, css: any) => {
                        if (err != null) {
                            console.log(chalk.yellow('Error in Stylus file: ') + filename);
                            console.log(err);
                            return resolve('');
                        } else {
                            return resolve(css);
                        }
                    });
                } catch (ex) {
                    if (content == null) {
                        console.log(`No such file: ${ filename }`);
                    } else {
                        console.log('Inner exception from Stylus: ', chalk.yellow(ex));
                    }

                    return resolve('');
                }
            });
        });
    }

    static doCompilePugFile(filename: string): Promise<string> {

        return new Promise(function(resolve, reject) {

            if (filename == null) {
                resolve('');
            }

            try {

                fs.readFile(filename, 'utf8', function(err, content) {

                    if (err != null) {
                        resolve('');
                    }

                    let html = pug.render(content, {
                        filename,
                        pretty: false,
                        debug: false,
                        buildnum: version,
                        ioserver: os.hostname() + ':' + config.WebsocketPort,
                    });

                    resolve(html);
                });
            } catch (e) {

                console.log('Unable to compile jade: ', filename);
                resolve('');
            }
        });
    }

    static doCompileCoffeeFile(filename: string): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                config.status('doCompileCoffeeFile Reading File:', filename);
                if (filename == null) {
                    config.status('doCompileCoffeeFile bad call, missing filename');
                    resolve('');
                }

                fs.readFile(filename, 'utf8', function(_err, content) {

                    try {

                        let compiled = coffeeScript.compile(content, { bare: true });

                        resolve(compiled);
                    } catch (ex) {

                        console.log('Content=', content);
                        console.log('Inner exception from CoffeeScript: ', chalk.yellow(ex));
                        resolve();
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }
};
