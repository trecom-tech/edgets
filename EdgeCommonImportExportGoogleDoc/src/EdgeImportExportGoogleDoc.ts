import * as jsonfile    from 'jsonfile';
import { config }       from 'edgecommonconfig';

const google            = require('googleapis');
const googleAuth        = require('google-auth-library');

const reDateFormat1     = new RegExp('([0-9]{1,2}).([0-9]{1,2}).([0-9][0-9][0-9][0-9])');

class EdgeGoogleCell {
    
    sheet   : any;
    row     : number;
    col     : number;
    cell    : any;
    update  : any;

    constructor(sheet: any, row: number, col: number, cell: any) {
        this.sheet  = sheet;
        this.row    = row;
        this.col    = col;
        this.cell   = cell;
        this.update = null;
    }

    value() {
        if (!this.cell.data) {
            return null;
        }

        if (!this.cell.data.userEnteredValue) {
            return null;
        }

        if (this.cell.data.userEnteredValue.stringValue) {
            return this.cell.data.userEnteredValue.stringValue;
        }

        if (this.cell.data.userEnteredValue.numberValue) {
            return  this.cell.data.userEnteredValue.numberValue;
        }

        if (this.cell.data.effectiveValue) {
            if (this.cell.data.effectiveValue.numberValue) {
                return this.cell.data.effectiveValue.numberValue;
            }

            if (this.cell.data.effectiveValue.stringValue) {
                return this.cell.data.effectiveValue.stringValue;
            }

            if (this.cell.data.effectiveValue.errorValue) {
                return 'SpreadsheetError';
            }

            return this.cell.data.effectiveValue;
        }

        return null;
    }

    isFormula() {
        if (this.cell.data && 
            this.cell.data.userEnteredValue && 
            this.cell.data.userEnteredValue.formulaValue) {
            return true;
        }

        return false;
    }
    
    /**
     * See https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets#RowData
     */
    setValue(newValue: any) {
        let oldValue = this.value();
        if (oldValue === newValue) {
            return true;
        }

        this.initUpdate('userEnteredValue');
        this.cell.data = this.cell.data || {};

        if (typeof newValue === 'string') {
            this.update.rows[0].values[0].userEnteredValue = {stringValue: newValue};
            this.cell.data.userEnteredValue = {stringValue: newValue};
        } else if (typeof newValue === 'number') {
            this.update.rows[0].values[0].userEnteredValue = {numberValue: newValue};
            this.cell.data.userEnteredValue = { numberValue: newValue };
        } else if (typeof newValue === 'boolean') {
            let newValue2 = 'Yes';
            if (!newValue) { 
                newValue2 = 'No';
            }
            this.update.rows[0].values[0].userEnteredValue =  { stringValue: newValue2 };
            this.cell.data.userEnteredValue = { stringValue: newValue2 };
        } else {
            console.log("Warning, don't know how to setValue for type ", typeof newValue);
        }
    }

    setFormula(newFormula: any) {
        this.cell.data = this.cell.data || {};
        this.cell.data.userEnteredValue = this.cell.data['userEnteredValue'] || {};

        if (this.cell.data && this.cell.data.userEnteredValue.formulaValue === newFormula) {
            return true;
        }

        this.initUpdate('userEnteredValue');
        this.update.rows[0].values[0].userEnteredValue = {formulaValue: newFormula};
        this.cell.data.userEnteredValue = {formulaValue: newFormula};

        return true;
    }

    setHyperlink(newLink: string, newValue: string) {
        let formula = `=HYPERLINK('${newLink}','${newValue}')`;
        this.setFormula(formula);
        this.cell.data.userEnteredValue.stringValue = newValue;

        return true;
    }

    setBackgroundColor(newRed: number, newGreen: number, newBlue: number) {
        this.initUpdate('userEnteredFormat.backgroundColor');
        
        this.update.rows[0].values[0].userEnteredFormat = 
            this.update.rows[0].values[0].userEnteredFormat || {};

        this.update.rows[0].values[0].userEnteredFormat.backgroundColor = {
            red     : newRed,
            green   : newGreen,
            blue    : newBlue
        };

        return true;
    }

    private initUpdate(name: string) {
        if (!this.update) {
            this.update = {
                start   : {
                    sheetId     : this.sheet.properties.sheetId,
                    rowIndex    : this.row,
                    columnIndex : this.col
                },
                rows    : [{values: [{}]}],
                fields  : ''
            }
        }

        for (let n of this.update.fields.split(',')) {
            if (n === name) {
                return true;
            }
        }

        if (this.update.fields.length > 0) {
            this.update.fields += ',';
        }
        this.update.fields += name

        return true;
    }

}

class EdgeGoogleSheet {

    googleExport    : any;
    spreadsheetId   : string;
    docProperties   : any;
    properties      : any;
    data            : any;
    cells           : any;
    maxRowCount     : number;
    maxColCount     : number;
    rowCount        : number;

    constructor(googleExport: any, spreadsheetId: string, docProperties: any, sheet: any) {
        this.googleExport  = googleExport;
        this.spreadsheetId = spreadsheetId;
        this.docProperties = docProperties;
        this.properties    = sheet.properties;

        // Data contains rowData, rowMetadata, columnMetadata
        this.data          = sheet.data[0];
        this.cells         = {};

        // Maximum values only, not that they all have data
        this.maxRowCount   = sheet.properties.gridProperties.rowCount;
        this.maxColCount   = sheet.properties.gridProperties.columnCount;

        this.rowCount      = this.data.rowData.length;
    }

    doSaveChanges() {
        return new Promise((resolve, reject) => {
            // See what changes are pending in each cell
            let requests = [];

            for (let rowNum in this.cells) {
                let colList = this.cells[rowNum];
                for (let colNum in colList) {
                    let cell = colList[colNum];
                    if (cell.update) {
                        requests.push({updateCells: cell.update});
                    }
                }
            }

            if (requests.length > 0) {
                config.status(`EdgeGoogleSheet doSave ${requests.length} changes`);
                const batchUpdateRequest = {requests: requests};

                const log = config.getLogger('sheets');
                log.info('Sending batch update', {
                    spreadsheetId   : this.spreadsheetId,
                    resource        : batchUpdateRequest
                });

                const sheets = google.sheets('v4');
                sheets.spreadsheets.batchUpdate({
                    auth            : this.googleExport.getAuth(),
                    spreadsheetId   : this.spreadsheetId,
                    resource        : batchUpdateRequest
                }, (err: any, result: any) => {
                    if (err) {
                        log.error('Sending batch update', {
                            spreadsheetId: this.spreadsheetId,
                            resource:      batchUpdateRequest,
                            err:           err,
                            result:        result
                        });

                        console.log('error in batch', {
                            spreadsheetId: this.spreadsheetId,
                            resource:      batchUpdateRequest,
                            err:           err,
                            result:        result
                        });

                        resolve(false);
                    } else {
                        // Remove pending update
                        for (let rowNum in this.cells) {
                            let colList = this.cells[rowNum];
                            for (let colNum in colList) {
                                let cell = colList[colNum];
                                delete(cell.update);
                            }
                        }

                        resolve(result.replies);
                    }
                });
            } else {
                resolve([{}]);
            }
        });
    }

    get(rowNum: number, colNum: number) {
        if (this.cells[rowNum] && this.cells[rowNum][colNum]) {
            return this.cells[rowNum][colNum];
        }

        let result: any = {
            data: null
        };

        if (this.data.rowData[rowNum] && this.data.rowData[rowNum].values) {
            if (this.data.rowData[rowNum].values[colNum]) {
                result.data = this.data.rowData[rowNum].values[colNum];
            }
        }

        result.rowMeta = this.data.rowMetadata[rowNum] || null;
        result.colMeta = this.data.columnMetadata[colNum] || null;

        /*
        Example of what the result might look like at this point:
        { data:
            { userEnteredValue: { stringValue: 'Something Here that is text' },
              effectiveValue: { stringValue: 'Something Here that is text' },
              formattedValue: 'Something Here that is text',
              effectiveFormat:
                { backgroundColor: [Object],
                padding: [Object],
                horizontalAlignment: 'LEFT',
                verticalAlignment: 'BOTTOM',
                wrapStrategy: 'OVERFLOW_CELL',
                textFormat: [Object],
                hyperlinkDisplayType: 'PLAIN_TEXT' } },
        rowMeta: { pixelSize: 21 },
        colMeta: { pixelSize: 100 } }
        */

        this.cells[rowNum] = this.cells[rowNum] || {};
        this.cells[rowNum][colNum] = new EdgeGoogleCell(this, rowNum, colNum, result);

        return this.cells[rowNum][colNum];
    }
}

export default class EdgeImportExportGoogleDoc {
    
    sheetId     : string;
    cacheFile   : any;
    maxCacheAge : any;
    credentials : any;
    
    constructor(credentialKey: string, sheetId: string, cacheFile?: any, maxCacheAge?: any) {
        this.sheetId     = sheetId;
        this.cacheFile   = cacheFile;
        this.maxCacheAge = maxCacheAge;
        this.credentials = config.getCredentials(credentialKey);
        
        if (!this.credentials) {
            config.status(`EdgeImportExportGoogleDoc Warning: Missing credentials for key '${credentialKey}`);
            return;
        }

        if (!this.credentials.oauth2) {
            config.status(`EdgeImportExportGoogleDoc Warning: Missing credentials oauth2 for key '${credentialKey}'`);
            return;
        }

        if (!this.credentials.oauth2.client_id) {
            config.status(`EdgeImportExportGoogleDoc Warning: Missing credentials oauth2 client_id for key '${credentialKey}'`);
        }
    }

    /**
     * Batch updates document
     * https://developers.google.com/sheets/api/guides/batchupdate
     */

    doReadSheets(ranges?: string) {
        return new Promise((resolve, reject) => {
            // Try the cache first
            if (this.cacheFile) {
                try {
                    config.status(`doReadSheets, Reading cache file ${this.cacheFile}`);
                    const rawData = jsonfile.readFileSync(this.cacheFile);

                    if (rawData && rawData.cacheTime) {
                        const today = new Date();
                        this.maxCacheAge = this.maxCacheAge || 300 * 1000;
                        
                        if (today.getTime() - rawData.cacheTime < this.maxCacheAge) {
                            let sheets: any = {};

                            for(let sheet of rawData.data.sheets) {
                                const s = new EdgeGoogleSheet(this, rawData.data.spreadsheetId, rawData.data.properties, sheet);
                                sheets[sheet.properties.title] = s;
                            }

                            resolve(sheets);
                            return;
                        }
                    }
                } catch (e) {
                    config.status('doReadSheets, Cache file error', e);
                }
            }

            let options: any = {
                auth            : this.getAuth(),
                spreadsheetId   : this.sheetId,
                includeGridData : true
            }

            if (ranges) {
                options.ranges = ranges;
            }

            config.status('doReadSheets, Reading sheets v4 with options:', options);
            
            const sheets = google.sheets('v4');
            sheets.spreadsheets.get(options, (err: any, response: any) => {
                if (err) {
                    reject(err);
                    return;
                }

                let sheets: any = {};
                for(let sheet of response.sheets) {
                    config.status('doReadSheets, processing sheet id:', response.spreadsheetId, ' as ', sheet.properties.title);
                    const s = new EdgeGoogleSheet(this, response.spreadsheetId, response.properties, sheet);
                    sheets[sheet.properties.title] = s;
                }

                if (this.cacheFile) {
                    config.status('doReadSheets, writing cache file:', this.cacheFile);
                    jsonfile.writeFileSync(this.cacheFile, {
                        cacheTime   : new Date().getTime(),
                        data        : response
                    });
                }

                config.status('doReadSheets, finished.');
                resolve(sheets);
            });
        });
    }

    doReadDoc(range: string, dataFormat: string = 'UNFORMATTED_VALUE') {
        return new Promise((resolve, reject) => {
            const sheets = google.sheets('v4');
            sheets.spreadsheets.values.get({
                auth                    : this.getAuth(),
                spreadsheetId           : this.sheetId,
                range                   : range,
                valueRenderOption       : dataFormat,
                dateTimeRenderOption    : 'FORMATTED_STRING'
            }, (err: any, response: any) => {
                if (err) {
                    config.status(`EdgeImportExportGoogleDoc doReadDoc Range=${range} error`, err);
                    reject(err);
                    return;
                }

                let allRows = [];
                for(let row of response.values) {
                    let thisRow = [];
                    for(let cell of row) {
                        if (typeof cell === 'string' && reDateFormat1.test(cell)) {
                            cell = new Date(cell);
                        }

                        thisRow.push(cell);
                    }

                    allRows.push(thisRow);
                }

                resolve(allRows);
            });
        });

        /*
        JSON When you request a single cell as a range:
        {"range":"Sheet1!A1","majorDimension":"ROWS","values":[["Something Here that is text"]]}
        
        JSON When you request a range of cells
        {"range":"Sheet1!A1:F6","majorDimension":"ROWS","values":[["Something Here that is text"],[],["Name","Magic Number","BirthDay","Formula","Another Formula","Formatted Column"],["Brian","23","10/2/1975","41.35","2300","Something Else"],["John","15","1/2/1989","28.08","1500"],["Mike","52.4","3/14/2012","4.87","5240","Another Thing"]]}
        */
    }

    /**
     * Read a part of the document using the range passed in
     * Range examples: "A1" - Default sheet, cell A1 only.
     */
    doReadDocFormulas(range: string) {
        return this.doReadDoc(range, 'FORMULA');
    }

    /**
     * Get the OAuth2 object based on the stored credentials
     * See the util/README file for information on how to generate the values.
     */
    private getAuth() {
        const clientId           = this.credentials.oauth2.client_id;
        const clientSecret       = this.credentials.oauth2.client_secret;
        const redirectURL        = '';

        const auth               = new googleAuth();
        const oauth2Client       = new auth.OAuth2(clientId, clientSecret, redirectURL);
        oauth2Client.credentials = this.credentials.oauth2;

        return oauth2Client;
    }
}