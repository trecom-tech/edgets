import DataSetPath from '../src/DataSetPath';
import { ObjectID } from 'mongodb';


describe("DataSetPath", () => {

    it("Should handle a strange subpath situation", () => {
        let path = new DataSetPath("/item/Name: Brian/123");
        path.should.have.property("value", null);
        path.should.have.property("collection", "item");
        path.should.have.property("rawPath", "/item/Name: Brian/123");
        path.should.have.property("canSave", false);
        path.condition.should.have.property("Name", " Brian");
        return path.subPath.length.should.be.eql(1);
    });

    it("Should have Simple single item search, non numeric", () => {
        let path = new DataSetPath("/item/apple");
        path.should.have.property("value", "apple");
        path.should.have.property("collection", "item");
        path.should.have.property("rawPath", "/item/apple");
        path.should.have.property("canSave", true);
        path.condition.should.have.property("id", "apple");
        return path.subPath.length.should.be.eql(0);
    });

    it("Should have search multiple items seperated by comma", () => {
        let path = new DataSetPath("/item/1,2,3");
        path.should.have.property("collection", "item");
        path.condition.should.have.property("$and");
        path.condition['$and'].length.should.be.eql(3);
        path.should.have.property("canSave", false);
        path.should.have.property("rawPath", "/item/1,2,3");
        return path.subPath.length.should.be.eql(0);
    });

    it("Should have search a range of values", () => {
        let path = new DataSetPath("/item/2-8");
        path.should.have.property("collection", "item");
        path.should.have.property("rawPath", "/item/2-8");
        path.should.have.property("canSave", false);
        path.condition.should.have.property("id");
        path.condition.id.should.have.property("$gt", 1);
        path.condition.id.should.have.property("$lt", 9);
        return path.subPath.length.should.be.eql(0);
    });

    it("Should have An object _id", () => {
        let path = new DataSetPath("/item/5745d5bd7be83a7fa64f0d07");
        path.should.have.property("collection", "item");
        path.should.have.property("canSave", true);
        path.condition.should.have.property("_id", ObjectID.createFromHexString("5745d5bd7be83a7fa64f0d07"));
        return path.subPath.length.should.be.eql(0);
    });

    it("Should have Simple single item search, with sub path", () => {
        let path = new DataSetPath("/item/1/person/name");
        path.should.have.property("collection", "item");
        path.should.have.property("canSave", true);
        path.condition.should.have.property("id", 1);
        path.subPath.length.should.be.eql(2);
        path.subPath[0].should.be.eql("person");
        return path.subPath[1].should.be.eql("name");
    });

    // ##|
    // ##|  TODO--- Convert these to Mocha
    // ##|
    it("Should have a single item search, with sub path and trailing slash", () => {
        let path = new DataSetPath("/item/1/person/name/");
        path.should.have.property("collection", "item");
        path.should.have.property("canSave", true);
        path.should.have.property("rawPath", "/item/1/person/name/");
        path.condition.should.have.property("id", 1);
        path.value.should.be.eql(1);
        path.subPath.length.should.be.eql(2);
        path.subPath[0].should.be.eql("person");
        return path.subPath[1].should.be.eql("name");
    });

    it("Should have All items in a table", () => {
        let path = new DataSetPath("/item/");
        path.should.have.property("collection", "item");
        path.should.have.property("canSave", false);
        return path.should.have.property("rawPath", "/item/");
    });

    it("Should have All items in a table", () => {
        let path = new DataSetPath("/item");
        path.should.have.property("collection", "item");
        path.should.have.property("canSave", false);
        return path.should.have.property("rawPath", "/item");
    });

    it("Should have All elments with a sub key that matches", () => {
        let path = new DataSetPath("/item/*/person/name:Brian/");
        path.subPath.length.should.be.eql(2);
        path.subPath[0].should.be.eql("person");
        path.subPath[1].should.be.eql("name");
        return path.condition['person.name'].should.be.eql("Brian");
    });

    it("Should have Logical or in search", () => {
        let path = new DataSetPath("/item/or:1,2,4,90,23");
        path.subPath.length.should.be.eql(0);
        path.condition.should.have.property("$or");
        path.condition['$or'].length.should.be.eql(5);
        path.condition['$or'][0].should.have.property("id", 1);
        path.condition['$or'][1].should.have.property("id", 2);
        path.condition['$or'][2].should.have.property("id", 4);
        path.condition['$or'][3].should.have.property("id", 90);
        return path.condition['$or'][4].should.have.property("id", 23);
    });

    it("Should have Logical in search", () => {
        let path = new DataSetPath("/item/in:1,2,4,90,23");
        path.subPath.length.should.be.eql(0);
        path.condition.should.have.property("id");
        path.condition.id.should.have.property("$in");
        path.condition.id['$in'].length.should.be.eql(5);
        path.condition.id['$in'][0].should.be.eql(1);
        path.condition.id['$in'][1].should.be.eql(2);
        path.condition.id['$in'][2].should.be.eql(4);
        path.condition.id['$in'][3].should.be.eql(90);
        return path.condition.id['$in'][4].should.be.eql(23);
    });

    it("Should have Logical in search", () => {
        let path = new DataSetPath("/item/firstname:in:Brian,John,Scott");
        path.subPath.length.should.be.eql(0);
        path.condition.should.have.property("firstname");
        path.condition.firstname.should.have.property("$in");
        path.condition.firstname['$in'].length.should.be.eql(3);
        path.condition.firstname['$in'][0].should.be.eql("Brian");
        path.condition.firstname['$in'][1].should.be.eql("John");
        return path.condition.firstname['$in'][2].should.be.eql("Scott");
    });
    it("Should have Field Exists", () => {
        let path = new DataSetPath("/item/firstname:+exists");
        path.subPath.length.should.be.eql(0);
        path.condition.should.have.property("firstname");
        return path.condition.firstname['$exists'].should.be.eql(true);
    });

    it("Should have Field does not exist", () => {
        let path = new DataSetPath("/item/firstname:-exists");
        path.subPath.length.should.be.eql(0);
        path.condition.should.have.property("firstname");
        return path.condition.firstname['$exists'].should.be.eql(false);
    });

    it("Should have Complex field path", () => {
        let path = new DataSetPath("/item/user.firstname:Brian");
        path.subPath.length.should.be.eql(0);
        return path.condition['user.firstname'].should.be.eql('Brian');
    });

});