import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
import DataSetManager from 'edgedatasetmanager';
export declare class ProviderUtility extends ApiProvider {
    redisLock: any;
    dmSequential: DataSetManager;
    sequentialDatasetName: string;
    sequentialCollectionName: string;
    constructor(prototype: ApiProvider);
    onInit(): void;
    getBaseName(): string;
    doGetGuid(): string;
    doGetServerStamp(user?: ApiSocketUser): {
        timestamp: number;
        user: number;
    };
    doGetNextSequential(user: ApiSocketUser, slug: string): Promise<any>;
    doGetNextNewObject(user: ApiSocketUser, datasetName: string, collectionName: string): Promise<any>;
    doInternalGetSequentialDataSet(): DataSetManager;
    doInternalReserveSequential(dm: DataSetManager, slug: string, nextSequential: number): Promise<void>;
}
export default ProviderUtility;
