class ViewTestShowMessage1 extends View
    
    getLayout: ()=>
        view:     "Splittable"
        percent:  40
        callback: "onSplitterSetup"
        top:      "TestShowSize"
        bottom:   
            view: "Table"    
            collection: "zipcode"
            name: "viewTable"

    onShowScreen: ()=>
        @el.css "border", "1px dashed blue"

        @attachGlobalEvent "Table", "test1", ()->
            console.log "ViewTestShowMessage1 on test1 message"
            true
        @attachGlobalEvent "Table", "test2", ()->
            console.log "ViewTestShowMessage1 on test2 message"
            true            
        @attachGlobalEvent "Keyboard", "test3", ()->
            console.log "ViewTestShowMessage1 on test3 message"
            true    
        console.log @viewTable.table 
        @viewTable.table.attachGlobalEvent "Table", "test_table_zipcode", () =>
            console.log "ViewTestShowMessage1.viewTable on test_table_zipcode message"
            true
        # globalTableEvents.on "test1", ()->
        #     console.log "ViewTestShowMessage1 on test1 message"
        #     true

        true

    onResize: (w, h)=>
        super(w, h)

        # str = ""
        # str += "<table class='infoTable'>"
        # str += "<tr><td> onResize w=</td><td> #{w} </td> <td align=right> h= </td> <td> #{h} </td></tr>"
        # str += "<tr><td> internal w=</td><td> #{@width()} </td> <td align=right> h= </td> <td> #{@height()} </td></tr>"
        # str += "<tr><td> outer w=</td><td> #{@outerWidth()} </td> <td align=right> h= </td> <td> #{@outerHeight()} </td></tr>"
        # str += "</table>"

        # @txtBody.html str


