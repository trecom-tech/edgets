import RateCapture from 'edgecommonratecapture';
export declare class ClusterWorker {
    jobsRunning: number;
    statsKey: any;
    currentTimestamp: Date;
    monitorChannel: any;
    logDataset: any;
    logCollection: any;
    loggingCollection: any;
    api: any;
    bar: any;
    log: any;
    hostname: string;
    workerList: any;
    constructor();
    onInit(): Promise<boolean>;
    reportText(...messages: any[]): boolean;
    onBeforeUpdate(vars: any): void;
    fixupJson(obj: any): boolean;
    setupCounterUpdateLoop(queueName: string, mqName: string, monitorChannel: any, callbackGetTotal: any): (vars: any) => any;
    setupLogging(): boolean;
    processMessageQueue(logName: string, queueName: string, consumerCount: number, onMessageCallback: any): {
        totalProcessed: number;
        totalPending: number;
    };
    setupMonitorProgressWorkQueue(queueName: string): RateCapture;
    startRequest(slot: any, id: any, queueName: string): {
        id: string;
        record_id: any;
        start_time: [number, number];
        server: string;
        name: string;
    };
    endRequest(reqData: any, result: any): boolean;
    verifyLoadAverage(): boolean;
    internalProcessWorkQueueWorker(slot: number, queueName: string, onMessageCallback: any): boolean;
    processWorkQueue(logName: string, queueName: string, consumerCount: number, onMessageCallback: any): Promise<RateCapture>;
}
export default ClusterWorker;
