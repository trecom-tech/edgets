##  To help with automated testing
globalEpochChartCounter = 0

class ViewChartEpoch extends View

	getDependencyList: ()=>
		return [ "/vendor/d3.min.js", "/vendor/epoch.min.js", "/vendor/epoch.min.css" ]

	onShowScreen: () =>
		@epochOptions = {}
		@epochOptions.type = 'time.line'
		@gid = globalEpochChartCounter++
		id = "chartEpoch" + @gid
		@elEpochHolder = @add "div", "epoch category10", id
		@elEpochHolder.setAbsolute()
		obj = 
			type: @epochOptions.type
			axes: ['left', 'bottom', 'right']
			windowSize: 60
			width: @elEpochHolder.width()
			height: @elEpochHolder.height()
		obj.range = {}
		@epochChart = @elEpochHolder.el.epoch obj
		true

	setOptions: (dataObj) =>
		if !dataObj?
			return

		if !@epochOptions?
			return

		if !@epochOptions.data?
			@epochOptions.data = []

		if !@epochOptions.range?
			@epochOptions.range = 
				left: null,
				right: null

		@epochOptions.range.left = dataObj.range?['left']
		@epochOptions.range.right = dataObj.range?['right']
		@epochOptions.windowSize = dataObj.windowSize
		if dataObj.type? and dataObj.type isnt @epochOptions.type
			@epochOptions.type = dataObj.type
			@epochChart.clear()
			@epochChart = @elEpochHolder.el.epoch @epochOptions
		else
			@epochChart.option @epochOptions
		if dataObj.data? 
			@epochChart.setData dataObj.data
		@epochChart.redraw()
		return @epochOptions

	pushData: (data) =>
		@epochChart.push data
		@epochChart.redraw()

	onResize: (w, h) =>
		@elEpochHolder.width w
		@elEpochHolder.height h
		if @epochChart?
			@epochChart.option 'width', w
			@epochChart.option 'height', h
			@epochChart.redraw()

	show: (name)=>

	serialize: () =>
		return @epochOptions

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options or typeof options isnt "object"
				reject "ViewChartEpoch: invalid data passed"

			@setOptions options
			resolve true
