import { DataFormatter } from '../src';

describe("DataFormatter data type Number_System Test", () =>
    it("should be formatted value", function() {
        const dataFormatter = new DataFormatter();

        let value: any = "20";
        let result = dataFormatter.formatData("number_system", value);
        result.should.equal("20 b");

        value = "20000";
        result = dataFormatter.formatData("number_system", value);
        result.should.equal("19.53 kb");

        value = 1024000;
        result = dataFormatter.formatData("number_system", value);
        result.should.equal("1 mb");

        value = 245210254;
        result = dataFormatter.formatData("number_system", value);
        result.should.equal("239.46 mb");

        value = "21525.35";
        result = dataFormatter.formatData("number_system", value);
        result.should.equal("21.02 kb");

        value = NaN;
        result = dataFormatter.formatData("number_system", value);
        result.should.equal("isNaN");
    })
);
