"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var edgeapi_1 = require("edgeapi");
var edgecommonconfig_1 = require("edgecommonconfig");
var RuleProcessor_1 = require("./RuleProcessor");
var RuleProcessorWorkqueue = /** @class */ (function (_super) {
    __extends(RuleProcessorWorkqueue, _super);
    function RuleProcessorWorkqueue() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RuleProcessorWorkqueue.prototype.onInit = function () {
        var _this = this;
        this.lastRecord = null;
        if (!this.rule.wq_name) {
            this.setError('Invalid Workqueue or missing "wq_name"');
        }
        edgeapi_1.default.doGetApi()
            .then(function (api) {
            _this.api = api;
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    RuleProcessorWorkqueue.prototype.onExecute = function (job, count) {
        var _this = this;
        if (count === void 0) { count = 0; }
        edgecommonconfig_1.config.status('WorkQueue Match:', job);
        if (!this.api) {
            edgecommonconfig_1.config.status('WorkQueue Match, job is being postponed until api is available');
            if (count === this.limit) {
                throw new Error('RuleProcessorWorkqueue onEexecute, api not defined');
            }
            // No API defined, postpone this rule
            setTimeout(function () {
                _this.onExecute(job);
            }, 1000);
            return;
        }
        // Don't resend the last request
        if (this.lastRecord === job.record_id) {
            return;
        }
        this.lastRecord = job.record_id;
        this.api.workqueue_doAddRecord(this.rule.wq_name, job.record_id);
    };
    return RuleProcessorWorkqueue;
}(RuleProcessor_1.default));
exports.default = RuleProcessorWorkqueue;
//# sourceMappingURL=RuleProcessorWorkqueue.js.map