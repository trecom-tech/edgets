//
// class for percent data type
//
// @extends [DataFormatterType]
//

import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

const numeral = require('numeral');
const common  = new DataFormatCommon();

export class DataFormatPercent extends DataFormatterType {
    name: string        = 'percent';
    width: number       = 80;
    input_width: number = 80;
    align: string       = 'right';

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        return numeral(common.getNumber(data)).format('#,###.[##] %');
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        const num = common.getNumber(data);
        return num;
    }
}

export default DataFormatPercent;
