"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug = require('slug');
const LRU = require('lru-cache');
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const ApiProvider_1 = require("./ApiProvider");
const DataSetConfig = require("edgecommondatasetconfig");
const EdgeError_1 = require("./EdgeError");
const collectionName = 'table';
const dbName = 'lookup';
const tableListCacheKey = 'tableList';
const allowEditTablesP = 'allowEditTables';
class ProviderTable extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    /**
     * Get invoked when provider is added to gateway
     */
    onInit() {
        const dataSet = slug(dbName);
        this.dsLookup = new edgedatasetmanager_1.default(dataSet);
        // Setup Cache
        const cacheOptions = {
            max: 100,
            maxAge: 1000 * 60,
        };
        this.cache = new LRU(cacheOptions);
    }
    /**
     * Returns base name. {BaseName}_{MemberName}
     */
    getBaseName() {
        return 'table';
    }
    /**
     * returns cache key
     * @param tableName - [string] table Name
     */
    internalGetRecordKey(tableName) {
        return `TABLE_${tableName}`;
    }
    /**
     * returns cache key for table list
     * @param tableName - [string] table Name
     */
    internalGetListKey(prefix) {
        return `${tableListCacheKey}_${prefix}`;
    }
    /**
     * Save `newData` to `path`
     * @param path - [string] Path string to save newData
     * @param newData - [any] JSON object to save
     * @returns difference of new and old data or true when there's no reference.
     */
    internalDoSaveData(table) {
        return __awaiter(this, void 0, void 0, function* () {
            const tableSettings = table.serialize();
            const differences = yield this.dsLookup.doUpdate(`/${collectionName}/${table.tableName}`, tableSettings, '');
            if (Array.isArray(differences)) {
                edgecommonconfig_1.default.status(`internalDoSaveData Complete:${differences.length}`);
            }
            const recordKey = this.internalGetRecordKey(table.tableName);
            this.cache.reset();
            this.cache.set(recordKey, tableSettings);
            return true;
        });
    }
    /**
     * Update / Create table
     * @param tableName - [string] table name
     * @param jsonData - [any] schema data in json for edgedatasetconfig's table
     */
    doSetTable(user, tableName, jsonData) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.hasRole(allowEditTablesP)) {
                throw this.failedRoleCheck(user, allowEditTablesP, 'doSetTable');
            }
            edgecommonconfig_1.default.status(`doGetTable tableName=${tableName} jsonData=${jsonData}`);
            const strAuctionTableName = slug(tableName);
            const table = new DataSetConfig.Table(strAuctionTableName);
            table.unserialize(jsonData);
            return this.internalDoSaveData(table);
        });
    }
    /**
     * Get table
     * @param tableName - [string] table name
     */
    doGetTable(tableName) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status(`doGetTable tableName=${tableName}`);
            const strAuctionTableName = slug(tableName);
            const recordKey = this.internalGetRecordKey(strAuctionTableName);
            const cachedTableData = this.cache.get(recordKey);
            if (cachedTableData) {
                edgecommonconfig_1.default.status(`doGetTable returns cached ${cachedTableData}`);
                return cachedTableData;
            }
            const tableData = yield this.dsLookup.doGetItem(`/${collectionName}/${strAuctionTableName}`);
            edgecommonconfig_1.default.status(`doGetTable returns cached ${tableData}`);
            return tableData.data;
        });
    }
    /**
     * Change column field
     * @param tableName - [string] table name
     * @param columnName - [string] column name
     * @param changeType - [string] column field to change
     * @param newValue - [any] field data
     */
    doChangeTable(user, tableName, columnName, changeType, newValue) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.hasRole(allowEditTablesP)) {
                throw this.failedRoleCheck(user, allowEditTablesP, 'doSetTable');
            }
            edgecommonconfig_1.default.status(`doChangeTable tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}`);
            const strAuctionTableName = slug(tableName);
            const tableData = yield this.doGetTable(strAuctionTableName);
            const table = new DataSetConfig.Table(strAuctionTableName);
            table.unserialize(tableData);
            const column = table.getColumn(columnName);
            if (column) {
                column.changeColumn(changeType, newValue);
                edgecommonconfig_1.default.status(`doChangeTable succeed tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}`);
                return this.internalDoSaveData(table);
            }
            else {
                edgecommonconfig_1.default.status(`doChangeTable failed tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}
                with Column is not found`);
                throw new EdgeError_1.EdgeError('Column is not found', 'TABLE_COLUMN_NOT_FOUND', 404);
            }
        });
    }
    /**
     * Get tables starts with prefix
     * @param prefix - [string] prefix of table to search
     */
    doListTables(prefix = '') {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status(`doListTables prefix=${prefix}`);
            let filter = '';
            if (prefix) {
                filter = `id:~^${prefix}`;
            }
            const listCacheKey = this.internalGetListKey(prefix);
            const cacheResult = this.cache.get(listCacheKey);
            if (cacheResult) {
                edgecommonconfig_1.default.status(`doListTables returns lru cached results ${cacheResult}`);
                return cacheResult;
            }
            const tableSettings = yield this.dsLookup.doGetItems(`/${collectionName}/${filter}`, null, null, 0, 1000000);
            this.cache.set(listCacheKey, tableSettings);
            edgecommonconfig_1.default.status(`doListTables returns results ${tableSettings}`);
            return tableSettings;
        });
    }
}
exports.default = ProviderTable;
;
//# sourceMappingURL=ProviderTable.js.map