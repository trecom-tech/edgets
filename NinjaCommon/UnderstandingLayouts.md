# Understanding Auto Layout

> The auto layout function allows views and screens to specify a layout in terms of sub views
> in a JSON format.  This quickly builds screens and assigns callback functions and variables
> for all sub views in a simple format.

## getLayout()
> The getLayout function is called, if it exists.   It can return

## String
> The name of a view to become the subview

## Object
> An object where the keys are the variable names to assign

## Array
> An array of view definitions for views that require more than one.
> In this case the


Example 1:  Create a simple