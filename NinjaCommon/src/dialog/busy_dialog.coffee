## -------------------------------------------------------------------------------------------------------------
## class BusyDialog to show the processing block ui
##

class BusyState

    text : ""
    pos  : 0
    lastPercent: -1

class BusyDialog

    # @property [String] content the content to show on the popup
    content:       "Processing please wait"

    # @property [Boolean] showing the status if currenlty showing or not
    showing:       false

    # @property [Array] busyStack information about all the stacks if nested dialogs are used
    busyStack:     []

    # @property [Array] callbackStack stack of the callback to execute
    callbackStack: []

    ## -------------------------------------------------------------------------------------------------------------
    ## function to execute the dialog
    ##
    ## @param [String] strText text to show on the box
    ## @param [Function] callbackFunction function to execute on the finished
    ##
    exec: (strText, callbackFunction) =>

        @callbackStack.push callbackFunction
        setTimeout () =>
            @showBusy(strText)

            setTimeout () =>
                callbackFunction = @callbackStack.pop()
                if callbackFunction?
                    callbackFunction()
                else
                    console.log "SHOULD NOT BE NULL:", strText, @callbackStack

                @finished()
            , 500

        , 0

    ##|
    ##|  Show busy waiting for data on a promise
    waitFor: (strText, promiseValue, timeout) =>

        @showBusy strText
        new Promise (resolve, reject)=>

            @setMinMax(0,0,0)

            if !timeout? then timeout = 30
            timerValue = setTimeout ()=>
                # console.log "Timeout waiting on promise:", strText
                resolve(null)
            , timeout * 1000

            promiseValue.then (result)=>
                clearTimeout timerValue
                @finished()
                resolve(result)

    step: (amount = null)=>
        if !amount? or typeof amount != "number" then amount = 1
        if @currentState.max == 0 then @currentState.max = 100
        @currentState.pos++
        if @currentState.pos > @currentState.max
            @currentState.pos = @currentState.max
        @updatePercent()

    setMinMax: (min, max, newPos = 0)=>

        $("#pleaseWaitDialog#{@currentState.id} .progressTextUnder").html ""
        @currentState.lastPercent = -1
        @currentState.min = min
        @currentState.max = max
        @updatePercent(newPos)

    updatePercent: (newPos = null)=>

        # console.log "updatePercent stack:", @busyStack
        # console.log "Current:", @currentState

        if newPos? then @currentState.pos = newPos

        $("#pleaseWaitDialogTitle#{@currentState.id}").html @currentState.text

        busyPBar = $("#busyProgressBar#{@currentState.id}")
        dlgProgress = $("#pleaseWaitDialog#{@currentState.id} .progress")

        if !@currentState.pos? or @currentState.pos == 0 or !@currentState.max?
            dlgProgress.hide()
            busyPBar.attr("aria-valuenow", 0).css("width", 0)
        else
            percent = Math.floor((@currentState.pos / @currentState.max) * 100) + "%"
            if percent == @currentState.lastPercent then return

            dlgProgress.show()

            if @currentState.pos+1 == @currentState.max then percent = "100%"

            $("#pleaseWaitDialog#{@currentState.id} .progressTextUnder").html "#{@currentState.pos} of #{@currentState.max} (#{percent})"
            busyPBar.css("width", percent)
            busyPBar.attr
                "aria-valuenow" : @currentState.pos
                "aria-valuemin" : @currentState.min
                "aria-valuemax" : @currentState.max

        true

    ## -------------------------------------------------------------------------------------------------------------
    ## function to make the processing finish
    ##
    finished: () =>
        if @busyStack.length > 0
            $("#pleaseWaitDialog#{@busyStack[@busyStack.length-1].id}").remove()
        @busyStack.pop()

        if @busyStack.length > 0
            @currentState = @busyStack[@busyStack.length-1]
            @updatePercent()

    ## -------------------------------------------------------------------------------------------------------------
    ## function to show the busy dialog
    ##
    ## @param [String] strText text to show on the modal
    ## @param [Object] options to handle the behaviour of the modal
    ##
    showBusy: (strText, options) =>
        $(".pleaseWaitDialog").remove()
        gid = GlobalValueManager.NextGlobalID()
        # @property [String] template the template to use in the BusyDialog
        template = Handlebars.compile '''
        <div class="pleaseWaitDialog" id="pleaseWaitDialog'''+gid+'''">
            <div class="modal-body">
                <h4 id="pleaseWaitDialogTitle'''+gid+'''">{{content}}</h4>

                <div class="progress" style='display: none;'>
                  <div id="busyProgressBar'''+gid+'''" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                  </div>
                </div>
                <div class="progressTextUnder">Loading</div>
            </div>
        </div>
        '''

        # @property [String] pleaseWaitHolder the html for the holder
        pleaseWaitHolder = $("body").append template(this)

        # @property [JQueryElement] modal the currently showing modal
        $("#pleaseWaitDialog#{gid}").hide()

        state = new BusyState()
        state.text = strText
        state.id = gid

        @busyStack.push state
        @currentState = state

        ##
        ##  Possibly overwrite default options
        if typeof options == "object"
            for name, val of options
                this[name] = val

        @show()
        @updatePercent()

    ## -------------------------------------------------------------------------------------------------------------
    ## function to show the modal at proper position
    ##
    ## @param [Object] options options to handle behaviour
    ##
    show: () =>
        wDlg = $("#pleaseWaitDialog#{@currentState.id}")
        w = $(window).width()
        h = $(window).height()

        otop = $(window).scrollTop()

        mw = wDlg.width()
        mh = wDlg.height()

        # console.log "Window (#{w} x #{h}) offset (#{otop}) modal (#{mw} x #{mh})"

        left = (w-mw)-20
        top  = (h-mh)-20

        wDlg.show()
        wDlg.css
            position : "fixed"
            left     : left
            top      : top

$ ->

    ## -------------------------------------------------------------------------------------------------------------
    ## on document ready creating global object of BusyDialog
    ##
    window.globalBusyDialog = new BusyDialog()
