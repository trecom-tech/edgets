## -gao
## class to create Items for ViewNavBar
##
class NavItem
	@DEFAULT_ICON: "fa-check"
	## constructor to create a new item
	constructor: (@widget, optionData = {}) ->
		@options             = {}

		@wgtLink             = @widget.add "a"
		@wgtIconWrapper      = @wgtLink.addDiv "ninja-nav-item-icon-wrapper fa-hover"
		@wgtTitle            = @wgtLink.addDiv "ninja-nav-item-title"

		@options.id          = optionData.id
		@options.parentGroup = optionData.parentGroup
		@options.icon        = optionData.icon || NavItem.DEFAULT_ICON
		@options.title       = optionData.title
		@options.isEnabled   = if optionData.isEnabled is undefined then true else optionData.isEnabled
		@options.isHidden    = if optionData.isHidden is undefined then false else optionData.isHidden
		@options.isSelected  = if optionData.isSelected is undefined then false else optionData.isSelected
		@options.isCheckable = if optionData.isCheckable is undefined then false else optionData.isCheckable
		@options.isChecked   = if optionData.isChecked is undefined then false else optionData.isChecked
		@options.tooltip	 = if optionData.tooltip is undefined then null else optionData.tooltip
		@options.filteredOut = optionData.filteredOut
		
		@setClickFunction optionData.callback
		@setTitle @options.title
		@setIcon @options.icon
		@setEnabled @options.isEnabled
		@setHidden @options.isHidden
		@setSelected @options.isSelected
		@setCheckable @options.isCheckable
		@setChecked @options.isChecked
		@setTooltip @options.tooltip
		@setFilteredOut @options.filteredOut

		@widget.el.on "click", (e) =>
			@setSelected true
			e.preventDefault()
			e.stopPropagation()

		@tooltipWindow 	= new FloatingWindow(0, 0, 100, 100)

	setTitle: (strTitle) =>
		if !strTitle then return false
		@options.title = strTitle
		@wgtTitle.html "<span>#{@options.title}</span>"	
		true

	setIcon: (icon) =>
		if !icon? then return false
		@options.icon = icon
		if !@wgtIcon?
			@wgtIcon = @wgtIconWrapper.addDiv "ninja-nav-item-icon ninja-nav-icon"
		if icon.includes("class")
			@wgtIcon.html @options.icon
		else
			@wgtIcon.html "<i class='#{@options.icon}'></i>"
		true

	setEnabled: (bool) =>
		if typeof bool isnt "boolean" 
			return false
		@options.isEnabled = bool
		@widget.setClass "disabled", !bool
		if @options.callback
			@widget.el.off "click", @options.callback
			if bool
				@widget.el.on "click", @options.callback

		true

	setHidden: (bool) =>
		if typeof bool isnt "boolean" 
			return false
		
		@options.isHidden = bool
		@widget.setClass "hidden", bool
		true

	setFilteredOut: (bool) =>
		@options.filteredOut = bool
		@widget.setClass "hidden", bool
		true

	setSelected: (bool) =>
		if typeof bool isnt "boolean"
			return false
		if !@options.isEnabled
			return false
		@options.isSelected = bool
		@widget.setClass "selected", bool
		true

	setClickFunction: (func) =>
		if typeof func isnt "function" 
			return false
		if @options.callback and typeof @options.callback is "function"
			@widget.el.off "click", @options.callback
		@options.callback = func
		@setEnabled @options.isEnabled
		true

	setCheckable: (bool) =>
		if typeof bool isnt "boolean" then return false
		@options.isCheckable = bool
		
		checkHandler = (e) =>
			@setChecked !@getChecked()
			true

		if !bool 
			@setChecked false
			@widget.el.off "click", checkHandler
		else
			@setChecked @options.isChecked
			@widget.el.on "click", checkHandler
		true
	
	setChecked: (bool) =>
		if typeof bool isnt "boolean" then return false
		if @options.isCheckable is false then return false
		@options.isChecked = bool
		if bool
			@setIcon "fa-check"
		else
			@setIcon ""
		true

	getChecked: () =>
		if !@options.isCheckable then return false
		if !@options.isChecked? then return false
		@options.isChecked

	setTooltip: (tooltip) =>
		if tooltip
			tooltipTxt = ''
			if typeof tooltip is 'function'
				tooltipTxt = tooltip()
			else
				tooltipTxt = tooltip

			@widget.el.on "mouseover", (e) =>
				coords = GlobalValueManager.GetCoordsFromEvent e
				setTimeout @showTooltip, 200, coords.x, coords.y, tooltipTxt
			@widget.el.on "mouseout", (e) =>
				setTimeout @hideTooltip, 200

	## show a tooltip with the corresponding note's content on itself
	showTooltip: (x, y, note) =>
		@tooltipWindow.html "<div class='img-annotation-tooltip'>#{note}</div>"
		@tooltipWindow.show()
		noteWidth = textWidth note
		noteHeight = 20
		if noteWidth > 300
			noteWidth = 310
			noteHeight = 20 * Math.ceil(noteWidth / 300)
		@tooltipWindow.setSize noteWidth, noteHeight
		@tooltipWindow.moveTo x, y

	## hide a tooltip for a note
	hideTooltip: () =>
		@tooltipWindow.hide()

	serialize: () => 
		defaultObj = 
			icon 		: null
			title 		: null
			isEnabled 	: false
			isHidden 	: false
			isSelected 	: false
			isCheckable : false
			isChecked   : false
			filteredOut : false
			callback 	: null
			tooltip		: null
		obj = Object.assign defaultObj, @options
		obj

	deserialize: (obj) =>
		@setTitle obj.title
		@setIcon obj.icon
		@setEnabled obj.isEnabled
		@setHidden obj.isHidden
		@setSelected obj.isSelected
		@setCheckable obj.isCheckable
		@setChecked obj.isChecked
		@setTooltip obj.tooltip
		@setFilteredOut obj.filteredOut

		@setClickFunction obj.callback
		true
