newImage1 = new Image()
newImage1.src = "./js/test_Data/images/1.jpg"
newImage2 = new Image()
newImage2.src = "./js/test_Data/images/2.jpg"
obj = 
	imageData : 
		src : "./js/test_Data/images/1.jpg"
	tableName : "zipcode"
	path      : "/zipcode/03105/images"
	notes     : 
		[
			x 			: 0.2
			y 			: 0.5
			note 		: "Note text 1"
			noteStamp 	: new Date("2017-09-09")
		,
			x 			: 0.7
			y 			: 0.8
			note 		: "Note text 2"
			noteStamp 	: new Date("2018-01-01")
		]

$ ->
	##|
	##|  Load the zipcodes JSON file.
	##|  This will insert the zipcodes into the global data map.
	addTest "Loading Zipcodes", () ->

		new Promise (resolve, reject) ->
			ds  = new DataSet "zipcode"
			ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
			ds.doLoadData()
			.then (dsObject)->
				resolve(true)
			.catch (e) ->
				console.log "Error loading zipcode data: ", e
				resolve(false)
	##|
	##|  This is just for diagnostics,  you don't need to verify the data map is
	##|  loaded normally.  The data types should be loaded upon startup.
	addTest "Confirm Zipcodes datatype loaded", () ->
		dm = DataMap.getDataMap()
		if !dm? then return false

		zipcodes = dm.types["zipcode"]
		if !zipcodes? then return false
		if !zipcodes.col["code"]? then return false
		DataMap.addColumn "zipcode", 
			name: "images"
			source: "images"
		true				

	##
	## Simple Image Annotation View
	##
	addTestButton "Simple ImageAnnotation", "Open", ()->
		addHolder().setView "ImageAnnotation", (view)->
			view.setImage newImage1
			view.setDataPath "zipcode", "/zipcode/03105/images"
		true

	##
    ##  popup window Image Annotation View
    ##
	addTestButton "ImageAnnotation in popup", "Open", ()->
		doPopupView 'ImageAnnotation', 'PopupImageAnnotation', 'imagestrip_popup', 800, 600, (view)->	
			view.setImage newImage2
			view.setDataPath "zipcode", "/zipcode/03106/images"
		true

	##
	## Tabs with Image Annotation View
	##
	addTestButton "ImageAnnotation in tab", "Open", ()->
		addHolder()
		.setView "DynamicTabs", (viewTabs)->
			viewTabs.doAddViewTab "ImageAnnotation", "TabImageAnnotation", (view)->		
				view.setImage newImage1
				view.setDataPath "zipcode", "/zipcode/03107/images"
			viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
		true	
	##
	## Simple Image Annotation View
	##
	addTestButton "ImageAnnotation Deserialize", "Open", ()->
		addHolder().setView "ImageAnnotation", (view)->
			view.deserialize obj

			setTimeout () ->
				doPopupView 'ImageAnnotation', 'popup-image-annotation2', 'imagestrip_popup', 800, 600, (viewPopup)->
					viewPopup.deserialize view.serialize()
			, 2000
		true

	go()