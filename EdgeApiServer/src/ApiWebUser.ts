import * as koa         from 'koa';
import IApiUser         from './interfaces/IApiUser';
import { IUser }        from './interfaces/IUser';
import EdgeRequest      from 'edgecommonrequest';
import { config }       from 'edgecommonconfig';
import EdgeApi          from 'edgeapi';
const moment = require('moment-timezone');

//  Store know IP address for this node.
let globalNetworkAddress : string = null;

export default class ApiWebUser extends IApiUser {
    public myName: number       = null;
    public rec: IUser           = null;
    public authAttempts: number = 0;

    public ipAddress  :string                   = null;
    public userAgent  :string                   = null;
    public cookie     :string                   = null;
    public location   :any                      = {};    

    constructor(public myConnectionID: number, public userIpAddress :string, public request : any) 
    {        
        super();
        this.myName    = this.myConnectionID;

        // TODO: Make sure these are valid objects before trying to assign
        if (request && request.headers)
        {
            this.userAgent = request.headers['user-agent'];
            this.cookie    = request.headers['cookie'];
        }

        if (request && request.headers && request.headers['x-real-ip'])
        {
            userIpAddress = request.headers['x-real-ip'];
        }

        if (request && request.headers && request.headers['x-forwarded-for"'])
        {
            userIpAddress = request.headers['x-forwarded-for"'];
        }        
        

        // Assign IP Address from remote connection
        this.ipAddress = userIpAddress;
    }

    public async doInitUser()
    {
        await this.internalLookupIP(); 

        // convert the timezone text to an offset
        const timezone = moment.tz.zone(this.location.timezone);
        const offset = (timezone && timezone.utcOffset(new Date())) || 0;
        this.timezoneOffset = (-1 * parseFloat(offset)) / 60;
    }

    addSubscription(data: any) {
        // TODO should be implementer later
        return false;
    }

    /**
     * Lookup the IP address using an API or cache.  Associate the IP data with the
     * connection.  In the future this may also lead to geo blocking based on country
     * or some other aspect.
     */
    async internalLookupIP()
    {
        // Special case, translate the IP address to the local machine.
        if (this.ipAddress === "::1")
        {
            if (globalNetworkAddress)
            {
                this.ipAddress = globalNetworkAddress;
            } else {
                config.status("internalLookupIP: Finding out my network address");
                let req = new EdgeRequest();
                let ipResult :any = await req.doGetLink("https://api.ipify.org?format=json");
                if (ipResult && ipResult.ip)
                {
                    globalNetworkAddress = ipResult.ip;
                    this.ipAddress = ipResult.ip;
                } else {
                    // TODO: What happens if this fails?  Not sure what to do
                }
            }
        }

        if (this.ipAddress.includes('127.0.0.1')) {
            this.location = {
                "as" : "Localhost Testing",
                "city" : "Testing",
                "country" : "Testing",
                "countryCode" : "TST",
                "isp" : "LOCAL",
                "lat" : 0,
                "lon" : 0,
                "org" : "Local",
                "query" : "127.0.0.1",
                "region" : "TST",
                "regionName" : "Testing",
                "status" : "success",
                "timezone" : "America/New_York",
                "zip" : "00000"
            };
        } else {
            // First check cache
            let strCacheID = "IP-" + this.ipAddress;
            let cachedResult = await EdgeApi.cacheGet(strCacheID);
            if (!cachedResult)
            {
                // See https://members.ip-api.com/docs/json
                // Todo: This is currently hard coded for a protovate API Key and should be
                // moved to credentials at some point.
                //
                config.status("ApiSocketUser:internalLookupIP IP Address not in cache for ", strCacheID);
                let req = new EdgeRequest();
                let result :any = await req.doGetLink("https://pro.ip-api.com/json/" + this.ipAddress + "?key=bR6cYYCubwO3wmd");

                // Update the user object with a location.
                this.location = result;

                // Update cache
                if (result && result.timezone)
                {
                    EdgeApi.cacheSet(strCacheID, result);
                }
            } else {
                try  {
                    this.location = JSON.parse(cachedResult.toString());
                } catch (e)
                {
                    config.status("internalLookupIP = Invalid Cache Result");
                    EdgeApi.cacheSet(strCacheID, null);
                }
            }
        }
    }
};
