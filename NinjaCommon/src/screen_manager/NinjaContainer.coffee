##|
##|  A container holds content and responds to outside events.
##|  All containers are a fixed size and absolute
##|

###

setHolder(el) - Pass in another container or jQuery element.
move(x,y,w,h)
show()
hide()
height()
width()
outerHeight()
outerWidth()
offset()

add(tagName, classes, id, attributes)
addDiv(classes, id, attributes)

getChildren() - returns an array of children where each is a NinjaContainer
setSize(w,h) - Request the element should change to a new size
onResize(w,h) - Called if this specific element has been resized

###

globalTagID = 0
globalDebugResize = !true
globalDebugMove = !true

class NinjaContainer

    constructor: ()->

        ##|
        ##|  Holds the main top of this container
        @el = null
        @element = null

        ##|
        ##|  A reference to the children added to this element
        @children = []

        ##|
        ##|  Already showing by default
        @visible = true

        ##|
        ##|  Unique ID for this container
        @gid     = globalTagID++

        ##|
        ##|  Special holder if this container contains a view
        @childView = null

        ##|
        ##|  Absolute position based tag
        @isAbsolute = false

        true

    ##|
    ##|  Sets the view to a docked view and callback with
    doSetViewWithNavbar: (viewName1, callBackWithViews)=>

        new Promise (resolve, reject)=>

            @setView "Docked", (dockView)=>
                dockView.setDockSize 40
                dockView.getFirst().setView "NavBar", (viewNavbar)=>
                    dockView.getSecond().setView viewName1, (viewNew)=>
                        if callBackWithViews? then callBackWithViews(viewNew, viewNavbar)
                        @resetSize()
                        resolve(viewNavbar)

    ##|
    ##|  Creates a splitter from the current view and calls the callback with splitter, and
    ##|  two other defined views created
    ##|
    doSplitView: (viewName1, viewName2, callBackWithViews)=>

        new Promise (resolve, reject)=>

            @setView "Splittable", (viewSplitter)=>
                viewSplitter.getFirst().setView viewName1, (viewNew1)=>
                    viewSplitter.getSecond().setView viewName2, (viewNew2)=>
                        if callBackWithViews?
                            callBackWithViews(viewSplitter, viewNew1, viewNew2)
                            @resetSize()

                        resolve(viewSplitter)

    ##|
    ##|  When a sub view auto defines variables
    ##|  we need to copy them up to the next higher view
    internalCopyVariables: (subView)=>

        if !@_variableNames? then @_variableNames = []

        changeParent = false
        if subView? and subView._variableNames?
            for varName in subView._variableNames
                this[varName] = subView[varName]
                if @_variableNames.indexOf(varName) < 0
                    @_variableNames.push(varName)

                if @parent? and @parent[varName] isnt this[varName]
                    changeParent = true

            if changeParent is true and @parent.internalCopyVariables?
                @parent.internalCopyVariables this

        true

    ##|
    ##| Process the layout for this view
    ##| @param result {LayoutHelper} - Any layout helper class
    ##|
    internalProcessLayout : (options)=>

        internalFinishLayout = (layoutOptions, newView)=>

            newPromise ()=>

                result = true
                if !@_variableNames? then @_variableNames = []

                ##|
                ##|  Called after the layout is done to check final
                ##|  common options such as name and callback
                if newView.deserialize?
                    try
                        result = newView.deserialize(layoutOptions)
                        if result? and result.then?
                            result = yield result
                    catch 
                        console.log "Exception deserializing with this config: #{layoutOptions}, in this view: #{newView}"

                if layoutOptions.callback?
                    if typeof layoutOptions.callback == "string"
                        if typeof this[layoutOptions.callback] == "function"
                            result = this[layoutOptions.callback](newView, layoutOptions, result)
                            if result? and result.then?
                                result = yield result

                if layoutOptions.name?
                    this[layoutOptions.name] = newView
                    @_variableNames.push layoutOptions.name

                ##|
                ##| copy variable names from children
                @internalCopyVariables(newView)

                ##|
                ##|  If the deserialize has oninit then call it to initialize the class
                ##|
                if layoutOptions.onInit?
                    layoutOptions.oninit = layoutOptions.onInit

                if layoutOptions.oninit? and typeof layoutOptions.oninit == "function"
                    result = layoutOptions.oninit(newView, layoutOptions)
                    if result? and result.then?
                        result = yield result

                result

        internalSetError = (message)=>
            @setView "ErrorMessage", (newView)=>
                newView.setMessage message

        new Promise (resolve, reject)=>

            if !@_variableNames? then @_variableNames = []

            ##|
            ##|  Perform the layout based on the data in "result"
            if !options?
                resolve(false)
                return false

            if typeof options == "string"
                ##|
                ##|  Single view name
                @setView options, (newView)=>
                    ##|
                    ##|  View is created
                    internalFinishLayout {}, newView
                .then (result)=>
                    resolve result

            else if typeof options == "object"

                ##|  Complete definition
                if !options.view?
                    console.log "View name not specified"
                    internalSetError "Missing view name 'view' in options to layout"
                    resolve(false)
                    return false

                ##|
                ##|  Shortcut for toolbar
                if options.view.toLowerCase() == "dockedtoolbar"
                    options.view = "Docked"
                    options.size = 40
                    options.docked =
                        view: "NavBar"
                        name: "viewToolbar"
                        items: options.items

                @setView options.view, (newView)=>
                    internalFinishLayout options, newView
                .then (result)=>
                    resolve(result)

            else

                console.log "internalProcessLayout Unknown type #{typeof options} OPTIONS:", options
                resolve(false)


            # if result.doSetLayout?
            #     p = result.doSetLayout(this)
            #     if p? and p.then?
            #         p.then (outputFromLayout)=>
            #             internalFinishLayout(result, outputFromLayout)
            #             resolve(outputFromLayout)
            #     else
            #         internalFinishLayout(result, p)
            #         resolve(p)
            # else
            #     console.log "autoLayout internalProcessLayout result missing doSetLayout:", result
            #     resolve(true)

    ##|
    ##|  If data exists in the view/screen/container with layout information
    ##|  then automatically create the sub layouts
    ##|
    ##|  @return Returns a promise in all cases but may return the same promise if called multiple times.
    ##|  @resolve Resolve's false if no layout function exists.  Returns a subview if a layout function was used.
    ##|
    autoLayout: ()=>

        if @_autoLayoutPromise? then return @_autoLayoutPromise
        @_autoLayoutPromise = new Promise (resolve, reject)=>

            if !@getLayout? or typeof @getLayout != "function"
                resolve(false)
                return false

            result = @getLayout @width(), @height()

            if result? and result.then? and typeof result.then == "function"
                result.then (nextResult)=>
                    # console.log "Promised getLayout Result:", nextResult
                    @internalProcessLayout(nextResult)
                    .then resolve
            else
                @internalProcessLayout(result)
                .then resolve

    ##|
    ##|  Remove the current view
    removeView: ()=>

        ##|
        ##| check that is actually removing children and whatever
        ##| should be removed when you unset the view.
        ##|
        try
            if @children? and @children.length > 0
                for child in @children
                    child.removeView()

            ## Datach all global events from globalEventObjects
            @detachAllGlobalEvents "Table"
            @detachAllGlobalEvents "Keyboard"

            ## Remove other things if there's any, e.g. ViewTable's TableView
            @removeExtraThings()

            delete @childView
            @children = []
            @el.html ""        

        catch e
            console.log "Exception in removing view: ", e
            return false

        true

        
    removeExtraThings: () =>
        true

    ##
    ## Hook global events 
    ## @param objId[String]: id of global events object, e.g. "Table" for globalTableEvents
    ## @param eventName[String]: name of an event to get hooked
    ## @param handler[function]: event handler
    attachGlobalEvent: (objId, eventName, handler) =>
        if !@globalEvents? then @globalEvents = {}
        try
            switch objId
                when "Table"
                    globalTableEvents.on eventName, handler
                    if !@globalEvents["Table"]?
                        @globalEvents["Table"] = []
                    @globalEvents["Table"].push 
                        eventname: eventName
                        handler: handler
                    break

                when "Keyboard"
                    globalKeyboardEvents.on eventName, handler
                    if !@globalEvents["Keyboard"]?
                        @globalEvents["Keyboard"] = []
                    @globalEvents["Keyboard"].push 
                        eventname: eventName
                        handler: handler            
                    break
                else
                    break

        catch e
            console.log "Exception in attachGlobalEvent: ", e, objId, eventName, handler
            return false
        true

    ##
    ## Unhook an event from global object
    detachGlobalEvent: (objId, eventName, handler) =>
        try
            switch objId
                when "Table"
                    globalTableEvents.off eventName, handler
                    position = -1
                    for ev, index in @globalEvents["Table"]
                        if ev.eventname is eventName then position = index 
                    if position >= 0
                        @globalEvents["Table"].splice position, 1
                    break
                
                when "Keyboard"
                    globalKeyboardEvents.off eventName, handler
                    position = -1
                    for ev, index in @globalEvents["Keyboard"]
                        if ev.eventname is eventName then position = index 
                    if position >= 0
                        @globalEvents["Keyboard"].splice position, 1
                    break

                else
                    break
        catch e
            console.log "Exception in detachGlobalEvent: ", e, objId, eventName, handler
            return false
        true
        
    ##
    ## Unhook all global events that are already attached to this 
    detachAllGlobalEvents: (objId) =>
        if !@globalEvents then return true
        try
            switch objId
                when "Table"
                    if @globalEvents["Table"]?
                        for ev, index in @globalEvents["Table"]
                            globalTableEvents.off ev.eventname, ev.handler
                        @globalEvents["Table"] = []
                        break

                when "Keyboard"
                    if @globalEvents["Keyboard"]?
                        for ev, index in @globalEvents["Keyboard"]
                            globalKeyboardEvents.off ev.eventname, ev.handler
                        @globalEvents["Keyboard"] = []
                        break

                else 
                    break

        catch e
            console.log "Exception in detachAllGlobalEvents: ", e, objId
            return false
        true
        
    ##  Destroy an element, remove all children and destroy them.
    ##
    destroy: ()=>
        ##|
        ##|  remove this element and remove it from the DOM
        if !@el? then return
        @el.remove()
        delete @el
        delete @children
        
        if @parent?
            index = @parent.children.indexOf this
            if index >= 0
                @parent.children.splice index, 1        
        return true

    ##|
    ##|  Set the HTML to a given view namext
    ##|  Execute the callback with the view before returning if
    ##|  a callback is specified.  If the callback returns a promise
    ##|  that promise is yielded before the setView is returned.
    ##|
    ##|  if you send optionalData and the view has setData it will be called
    ##|
    setView: (viewName, viewCallback, optionalData)=>
        if @isSettingView is true
            #console.log "Another view is already being set..."
            return false

        new Promise (resolve, reject)=>
            ## Reset this holder before loading new view in it
            @removeView()
            @isSettingView = true
            if typeof viewName == "object" and viewName.setView?
                ##|
                ##| viewName is actually a NinjaContainer object or something based on NinjaContainer
                ##| so we can say it's already loaded.
                @children.add(viewName)
                if viewCallback? then @viewCallback(viewName)
                return true

            doLoadView viewName
            .then (view)=>

                ##|
                ##|  The view now controls this el
                viewHolder = @addDiv "viewHolder View#{viewName}"
                viewHolder.setAbsolute()
                viewHolder.children.push view

                view.setHolder viewHolder
                view.parent = this

                if view.setData?
                    view.setData(optionalData)

                @childView = view

                # view.once "view_ready", ()=>
                $(document).ready ()=>

                    offsetTop  = @getTopFixHeight()
                    newWidth   = @getInsideWidth()
                    newHeight  = @getInsideHeight() - offsetTop
                    offsetLeft = 0

                    if globalDebugResize
                        console.log "NinjaContainer setView=#{viewName} ready ", newWidth, newHeight

                    view.move offsetLeft, offsetTop, newWidth, newHeight
                    @resetSize()

                    view.autoLayout()
                    .then ()=>

                        allPending = []

                        if view.onShowScreen?
                            result = view.onShowScreen(newWidth, newHeight)
                            if result? and result.then? and typeof result.then == "function"
                                allPending.push result

                        ##|
                        ##|  If onShowScreen was a promise then we wait for it before
                        ##|  we call the actual view callback function.
                        ##|
                        Promise.all allPending
                        .then ()=>
                            if viewCallback?
                                result = viewCallback(view)
                                if result? and typeof result == "object" and result.then?
                                    result.then ()=>
                                        @isSettingView = false
                                        resolve(view)
                                    return true
                            @isSettingView = false
                            resolve(view)

    debugText: (txt)=>

        return

        if !@elDebug?
            @elDebug = $("<div />")
            @el.append(@elDebug)

            @elDebug.css
                position       : "absolute"
                zIndex         : 500000
                top            : 4
                left           : 4
                padding        : 4
                backgroundColor: "#efefbb"
                border         : "1px solid #300030"
                color          : "#200020"
                overflow       : "hidden"

        @elDebug.html txt
        true

    setSize: (w, h)=>
        ##|
        ##|  Called if you want this contain to change width and height without changing top/left location

        if globalDebugResize
            console.log "NinjaContainer setSize #{w}, #{h}", @el
            @debugText "s(#{w}, #{h})"

        if w != @cachedWidth or h != @cachedHeight
            @element.style.width = w + "px"
            @element.style.height = h + "px"
            @resetCached()
            @internalUpdateSizePosition("setSize #{w}, #{h}")

            if @childView
                offsetLeft = 0
                offsetTop  = @getTopFixHeight()
                @childView.move offsetLeft, offsetTop, w, h-offsetTop
                # @childView.move offsetLeft, offsetTop, w, h

        true

    ##|
    ##|  Call this to automatically reset your size but also tell the parent to try to reset his size
    resetSize: ()=>
        @internalUpdateSizePosition("resetSize")
        @setSize @width(), @height()
        if @parent? and @parent.resetSize?
            @parent.resetSize();

        true

    ##|
    ##|  This function is called when the control has already resized and the DOM should be stable
    ##|  By default all we are doing is passing this event to the children.   This is unlikely to be
    ##|  correct because if you have more than one child they can't both be the same size as this holder
    ##|
    onResize: (w, h)=>

        if globalDebugResize
            console.log @el, "NinjaContainer onResize #{w}, #{h}"

        if w < 0 then w = 0
        if h < 0 then h = 0

        return { width: @width(), height: @height() }

    show: ()=>
        if !@el? then return this
        if @visible is true then return this
        if @parent?.onChildVisibility?
            @visible = @parent.onChildVisibility(this, "show")
        else if @visible != true
            @el.show()
            @visible = true
        this

    hide: ()=>
        if !@el? then return this
        if @visible is false then return this
        if @parent?.onChildVisibility?
            @visible = !(@parent.onChildVisibility(this, "hide"))

        else if @visible == true
            @el.hide()
            @visible = false
        this

    getVisible: () => 
        @visible 

    setVisible: (@visible) =>

    setScrollable: ()=>
        @element.style.overflow = "auto"

    ##|
    ##|  Remove the absolute values and allow this view to "free flow"
    ##|  TODO:  It's not clear how this will work with size events
    ##|         need to check if resize and other events still work?
    removeAbsolute: ()=>
        if @isAbsolute == false then return true
        @isAbsolute = false

        # @element.style.position = "inline"
        @element.style.position = null
        @element.style.width    = null
        @element.style.height   = null
        @element.style.top      = null
        @element.style.left     = null
        @element.style.overflowX = "hidden"
        @element.style.overflowY = "auto"

        # @internalUpdateSizePosition("removeAbsolute")
        true

    ##|
    ##|  Make this container absolute positioned
    setAbsolute: ()=>
        if @isAbsolute == true then return true
        @isAbsolute = true

        @element.style.position = "absolute"
        @element.style.width    = "100%"
        @element.style.height   = "100%"
        @element.style.top      = 0
        @element.style.left     = 0
#        @element.style.overflow = "hidden"

        @internalUpdateSizePosition("setAbsolute")
        true

    ##|
    ##|  Assign this container to be held by another container and thus
    ##|  it will receive events from that parent container and it will take over
    ##|  controller it's el variable or jQuery variable
    setHolder: (elHolder)=>

        if typeof elHolder == "object" and elHolder.setHolder?
            ##|
            ##|  Passing in another container
            @el = elHolder.el

        else

            @el = $(elHolder)

        @element = @el[0]
        @resetCached()

        true

    ##|
    ##|  Returns the position of the container
    ##|  { top, left } - https://api.jquery.com/position/
    position: ()=>
        if !@el? then return { top: 0, left: 0 }
        if @cachedPosition? then return @cachedPosition
        @cachedPosition = @el.position()
        return @cachedPosition

    ##|
    ##|  Reposition aboslute elements
    move: (x, y, w, h)=>

        updateNeeded = false

        if !@isAbsolute
            @setAbsolute()
            updateNeeded = true

        if globalDebugMove
            console.log @element, "NinjaContainer move (#{x}, #{y}, #{w}, #{h})"

        @debugText "m(#{x}, #{y}, #{w}, #{h})"

        if x != @x
            @x = x
            @element.style.left = @x + "px"
            updateNeeded = true

        if y != @y
            @y = y
            @element.style.top = @y + "px"
            updateNeeded = true

        if w != @cachedWidth or h != @cachedHeight
            @setSize w, h
            updateNeeded = true

        if updateNeeded
            @internalUpdateSizePosition("move #{x} vs #{@x}, #{y} vs #{@y}, #{w} vs #{@cachedWidth}, #{h} vs #{@cachedHeight}")

        return updateNeeded

    ##|
    ##|  Returns the height
    getHeight: ()=>
        if @cachedHeight? then return @cachedHeight
        if !@el? then return 0

        if @isAbsolute
            @cachedHeight = @el.outerHeight()
        else
            @cachedHeight = @el.height()

        return @cachedHeight

    ##|
    ##|  Possible badge text for views that support it
    ##|  check all children to see if one does
    getBadgeText: ()=>

        if !@children?
            return null

        for c in @children
            if c.getBadgeText?
                testText = c.getBadgeText()
                if testText? then return testText

        return null

    ##|
    ##|  Set the height
    setHeight: (h)=>
        if h == @cachedHeight then return h
        @internalUpdateSizePosition("setHeight #{h} vs #{@cachedHeight}")
        @cachedHeight = h
        if @isAbsolute
            @el.outerHeight(h)
        else
            @el.height(h)

        return h

    height: (h)=>
        if h? then @setHeight(h) else @getHeight()

    setMinHeight: (newMin)=>
        @minHeight = newMin
        if @childView? and @childView.setMinHeight? then @childView.setMinHeight(newMin)
        @resetSize()
        return @minHeight

    setMaxHeight: (newMax)=>
        @maxHeight = newMax
        if @childView? and @childView.setMaxHeight? then @childView.setMaxHeight(newMin)
        @resetSize()
        return @maxHeight

    getMinHeight: ()=>
        if @minHeight? then return @minHeight
        if @childView? and @childView.getMinHeight? then return @childView.getMinHeight()
        return null

    getMaxHeight: ()=>
        if @maxHeight? then return @maxHeight
        if @childView? and @childView.getMaxHeight? then return @childView.getMaxHeight()
        return null

    getInsideWidth: ()=>
        if @cachedInsideWidth? then return @cachedInsideWidth
        @cachedInsideWidth = @el.width()
        return @cachedInsideWidth

    getInsideHeight: ()=>
        if @cachedInsideHeight? then return @cachedInsideHeight
        @cachedInsideHeight = @el.height()
        return @cachedInsideHeight


    ##|
    ##|  Get the current width
    getWidth: ()=>
        if @cachedWidth? then return @cachedWidth
        if !@el? then return 0

        if @isAbsolute?
            @cachedWidth = @el.outerWidth()
        else
            @cachedWidth = @el.width()

        return @cachedWidth

    ##|
    ##|  set the current width
    setWidth: (w)=>
        if w == @cachedWidth then return w
        @internalUpdateSizePosition("setWidth #{w} vs #{@cachedWidth}")
        @cachedWidth = w

        if @isAbsolute
            @el.outerWidth(w)
        else
            @el.width(w)

        return w

    width: (w)=>
        if w? then @setWidth(w) else @getWidth(w)

    setMinWidth: (newMin)=>
        if newMin == @minWidth then return @minWidth
        @minWidth = newMin
        if @childView? and @childView.setMinWidth? then @childView.setMinWidth(newMin)
        @resetSize()
        return @minWidth

    setMaxWidth: (newMax)=>
        if @maxWidth == newMax then return @maxWidth
        @maxWidth = newMax
        if @childView? and @childView.setMaxWidth? then @childView.setMaxWidth(newMin)
        return @maxWidth

    getMinWidth: ()=>
        if @minWidth? then return @minWidth
        if @childView? and @childView.getMinWidth? then return @childView.getMinWidth()
        return null

    getMaxWidth: ()=>
        if @maxWidth? then return @maxWidth
        if @childView? and @childView.getMaxWidth? then return @childView.getMaxWidth()
        return null

    ##|
    ##| amount of space on top
    getTopFixHeight: ()=>
        if @wgtTopFixed?
            return @wgtTopFixed.height()
        return 0

    ##|
    ##|  Create a widget on the top that fixed height and return the widget
    createTopFixed: (height)=>
        @wgtTopFixed = new WidgetTag(@elHolder)
        @wgtTopFixed.move(0, 0, @width(), height)
        @internalUpdateSizePosition("creating top area")
        return @wgtTopFixed

    ##|
    ##| Return outer width
    outerWidth: (w)=>
        if w? and @el? then return @el.outerWidth w
        if @cachedOuterWidth? then return @cachedOuterWidth
        if !@el? then return 0
        @cachedOuterWidth = @el.outerWidth()
        return @cachedOuterWidth

    ##|
    ##| return outer height
    outerHeight: (h)=>
        if h? and @el? then return @el.outerHeight h        
        if @cachedOuterHeight? then return @cachedOuterHeight
        if !@el? then return 0
        @cachedOuterHeight = @el.outerHeight() - @getTopFixHeight()
        return @cachedOuterHeight

    scrollTop: ()=>
        if @element? then return @element.scrollTop
        return 0

    offsetTop: ()=>
        if @element? then return @element.offsetTop
        if @el? then return @el.offset().top
        return 0

    offset: ()=>
        if @cachedOffset then return @cachedOffset
        if !@el? then return { top: 0, left: 0 }
        @cachedOffset = @el.offset()
        return @cachedOffset

    ##|
    ##|  Give the element a new zindex value
    ##|  Can be any of number, auto, initial, inherit
    ##|  see http://www.w3schools.com/jsref/prop_style_zindex.asp
    setZ: (newZIndex = "auto")=>
        @element.style.zIndex = newZIndex

    getZ: ()=>
        return @element.style.zIndex

    getChildren: () =>
        return @children

    ##|
    ##|  Adds a new child tag under this one of a given type
    ##|  with a default class, id, and attributes
    ##|
    add: (tagName, classes, id, attributes) =>
        tag = new WidgetTag tagName, classes, id, attributes
        tag.parent = this
        @el.append tag.el
        @children.push tag
        return tag

    ##|
    ##|  Add the element but do it within the certain order
    addAtPosition: (tagName, classes, position) =>

        tag = new WidgetTag tagName, classes
        tag.parent = this

        count = 0
        for child in @children
            if position == count
                tag.el.insertBefore(child.el)
                @children.splice count, 0, tag
                return tag

            count++

        @el.append tag.el
        @children.push tag
        return tag

    ##|
    ##|  Shortcut to add a div
    addDiv: (classes, id, attributes) =>
        return @add "div", classes, id, attributes

    ##|
    ##|  Internal helper function that should be called if any function
    ##|  knowingly changes the size or position
    ##|
    internalUpdateSizePosition : (txtDebug)=>
        if globalDebugResize
            @iusp = (@iusp || 0) + 1
            console.log @element, "NinjaContainer internalUpdateSizePosition #{@iusp} - #{txtDebug}"

        @resetCached()

        if @internalUpdateTimer? then clearTimeout(@internalUpdateTimer)
        @internalUpdateTimer = setTimeout ()=>

            @cachedHeight      = @getHeight()
            @cachedWidth       = @getWidth()
            @cachedOuterHeight = @outerHeight()
            @cachedOuterWidth  = @outerWidth()
            @cachedOffset      = @el.offset()
            @cachedPosition    = @el.position()

            if @cachedPosition? and @cachedPosition.top?
                @x = @cachedPosition.left
                @y = @cachedPosition.top
            else
                @x = 0
                @y = 0

            @onResize(@cachedOuterWidth, @cachedOuterHeight)

        , 10

        true

    ##|
    ##|  Send an event or variable down the list of children views
    ##|  allowing any other sub view to act on it.
    cascade: (eventName, vriables...)->

        if !@children? then return
        for c in @children
            try
                if c[eventName]?
                    c[eventName].apply(c, vriables)
                else if c["watch"]? and typeof c["watch"] == "function"
                    if !vriables? then vriables = []
                    vriables.splice(0, 1, eventName)
                    c["watch"].apply(c, vriables)

                if c["cascade"]? and typeof c["cascade"] == "function"
                    c["cascade"](eventName, vriables...)
            catch e
                console.log "Exception during cascade('#{eventName}'):", e

        true

    ##|
    ##|  Clear internally cached values
    ##|
    resetCached: ()=>
        delete @cachedInsideWidth
        delete @cachedInsideHeight
        delete @cachedHeight
        delete @cachedWidth
        delete @cachedOuterHeight
        delete @cachedOuterWidth
        delete @cachedOffset
        delete @cachedPosition
        delete @x
        delete @y

        true