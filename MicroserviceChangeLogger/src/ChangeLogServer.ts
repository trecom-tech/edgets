/**
 * General configuration and error handlers
 */
import { config }   from 'edgecommonconfig';
import Worker from './Worker';

config.setTitle('Edge Server');
// Start the worker
const worker = new Worker('azureTest', 'changelogs');
worker.doRun();