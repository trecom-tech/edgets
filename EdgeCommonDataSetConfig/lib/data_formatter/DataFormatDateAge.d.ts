import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatDateAge extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    elEditor: any;
    datePicker: any;
    editorShowing: boolean;
    elDataEditor: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options: any, path: any): string;
    unformat(data: any, path: any): Date | "";
    getValueInTooltip(value: any): any;
}
export default DataFormatDateAge;
