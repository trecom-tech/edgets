/// <reference types="node" />
export declare class EdgeRequest {
    username: string;
    password: string;
    headers: any;
    cookies: any;
    nc: number;
    requestOptions: any;
    tokenDate: Date;
    challenge: any;
    method: string;
    baseUrl: string;
    userAgent: string;
    userAgentPassword: string;
    strDebugFilename: string;
    followRedirect: boolean;
    timeoutMS: number;
    useStatusUpdate: boolean;
    desiredResultKey: string;
    cacheFile: any;
    postData: any;
    authParams: any;
    watchdogTimer: NodeJS.Timeout;
    req: any;
    setUseGlobalProxy(): boolean;
    setMethod(method: string): void;
    /**
     * setUseragentPassword
     * @param userAgentPassword
     */
    setUseragentPassword(userAgentPassword: string): void;
    /**
     * setUseragent
     * @param userAgent
     */
    setUseragent(userAgent: string): void;
    setUsername(username: string, password: string): void;
    /**
     * Add a custom header value
     *
     * @param varName
     * @param value
     */
    addHeader(varName: string, value: string): boolean;
    /**
     * Given a URL it will use the existing options to download it.
     * @param dataUrl
     * @param qs
     */
    doGetLink(dataUrl: string, qs?: any): Promise<unknown>;
    /**
     * Given an object of options in node's http options format it will download.
     * @param options
     */
    doRequest(options: any): Promise<unknown>;
    /**
     * Given a res object from Node's http response, check for cookie headers
     * update the internal cookie jar.
     *
     * @param res
     */
    internalParseCookie(res: any): boolean;
    /**
     * Internal function that loks for a redirect header
     * @param res
     * @param options
     */
    internalCheckRedirect(res: any, options: any): boolean;
    /**
     *  Internal function that looks for a Microsoft Digest challenge request and will
     *  update the headers for a correct response.
     * @param res
     * @param options
     */
    internalCheckResponseForRealm(res: any, options: any): boolean;
    /**
     *
     * @param options
     */
    internalSetNextCommand(options: any): boolean;
    /**
     *
     * @param digest
     */
    parseChallenge(digest: string): any;
    /**
     * Return the actual header
     * @param params
     */
    compileParams(params: any): string;
    /**
     * Update and zero pad nc
     */
    updateNC(): string;
    /**
     * Helper function that finds elements in an object set
     * @param obj
     * @param name
     * @param findOne
     */
    findElements(obj: any, name: string, findOne: any): any;
    /**
     * The XML to JSON gives us weird arrays with 1 element because of how XML works
     * we are going to clean that up here.
     * @param obj
     */
    fixupJsonResults(obj: any): boolean;
    /**
     * Given the final http response, convert the XML and check error codes.
     * Returns either a json object converted from XML or whatever page is returned in raw
     * html format if no XML was received.
     * @param res
     * @param body
     * @param bodyBuffer
     */
    doInternalProcessResult(res: any, body: any, bodyBuffer: any): Promise<unknown>;
    onBeforeSendRequest(): boolean;
    resetTimeout(shouldRestart?: boolean): boolean;
    doInternalSendRequest(options: any): Promise<unknown>;
    onRequestTimeout: () => boolean;
}
export default EdgeRequest;
