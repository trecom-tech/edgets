import { ColumnBase }    from './column_base';
import { DataFormatter } from '../data_formatter/DataFormatter';

const math = require('mathjs');

math.import({
    "IF"(condition: any, trueExpr: any, falseExpr: any) {
        if (condition) {
            return trueExpr;
        }
        return falseExpr;
    }
});

const dataFormatter = new DataFormatter();

export class Column extends ColumnBase {
    static defaultColumn: any = {};
    tableName: string;
    formater: any             = {};
    parser: any               = {};
    scope: any[]              = [];
    defaultColumn: any        = {};
    data: any                 = {};
    clickable: boolean;
    isGrouped: boolean;
    visible: boolean;
    renderFunctionCache: any;
    colorFunctionCache: any;
    editFunctionCache: any;
    actualWidth: number;
    sort: number;
    tagSort: any;

    /**
     * constructor which create instance of Column object with tableName
     * @param [String] tableName
     */
    constructor(tableName?: string) {
        super();
        this.tableName = tableName;
        this.data      = {};
        this.parser    = math.parser();
    }

    init() {
        // should be implemented
    }

    // 
    //  function to set the given scope.
    //  set scope for excel statement, that is , symbol variable name will be assigned real value
    //  format : scope = [{"varName1": realValue1},
    //                       {"varName2": realValue2},
    //                       {"varName3": realValue3},
    //                       ...
    //                     ];
    //  e.x : expression = "IF(a > 0 , a, -a)"
    //        scope = [{"a" : 1}];
    //      result: output is 1
    // 
    // 
    setScope(scope: any) {
        this.scope = scope;
    }

    /**
     * function to run excel statement,
     * run excel statement from parsing string if given expression is started with "="
     * @param expression [String] expression to parse into excel statement
     * @param scope [Object]
     */
    excelStatementRender(expression: string, scope: any[]) {
        try {
            if ((this.parser == null)) {
                this.init();
            }

            this.setValues(scope, "");
            const result = this.parser.eval(expression);
            return result;

        } catch (e) {
            console.log("Statement running error:", e, "in expression", expression, "scope:", scope);
            return 0;
        }
    }

    /**
     * function to set the values for the eval
     * @param [Object] scope - scope to attach with prefix and set value into parser
     * @param prefix
     */
    setValues(scope: any[], prefix?: string) {

        if (prefix == null) {
            prefix = "";
        }
        for (let varName in scope) {

            const value = scope[varName];
            if ((value == null)) {
                continue;
            }

            const txt = prefix + varName.replace(/[^a-zA-Z0-9]/g, "").toLowerCase();
            if (typeof value === "object") {
                if (value.getTime != null) {
                    this.parser.set(txt, value);
                } else {
                    this.setValues(value, txt + "_");
                }
            } else {
                this.parser.set(txt, value);
            }
        }

        return true;
    }

    internalMathRender(a: any, b: any, c: any) {
        console.log("INTERNAL MATH RENDER:", this.data, "a=", a, "b=", b, "c=", c);
        return "X";
    }

    /**
     * function to return the name of the column. The name is the "Display label"
     */
    getName() {
        return this.data.name;
    }

    /**
     * function to return the source of the column in the data store, the actual field in the database
     */
    getSource() {
        return this.data.source;
    }

    /**
     * function to return true if the column can be edited.
     */
    getEditable() {
        return this.data.editable;
    }

    /**
     * function to return the text name of the data type (see Data Formats)
     */
    getType() {
        if (this.data.type != null) {
            return this.data.type;
        }
        return "text";
    }

    getDefault() {
        if (this.data.default != null) {
            return this.data.default;
        }
        return;
    }

    /**
     * function to return the alignment if any such as "right", "left", "center"
     */
    getAlign() {
        if ((this.data.align != null) && (typeof (this.data.align) === "string") && (this.data.align.length > 0)) {
            return this.data.align;
        }
        const f = this.getFormatter();
        if ((f != null) && (f.align != null)) {
            return f.align;
        }

        return null;
    }

    /**
     * function to return the order in terms of display for the column
     */
    getOrder() {
        return this.data.order;
    }

    /**
     * function to render
     * Returns true if the field is a calculation or has a render function.
     * Returns false if it's a pure data field
     */
    getIsCalculation() {
        if ((this.data != null) && (this.data.calculation != null) && (this.data.calculation === true)) {
            return true;
        }

        if (this.getRenderFunction() !== null) {
            return true;
        }

        return false;
    }

    getRequired() {
        if ((this.data.required != null) && (this.data.required === true)) {
            return true;
        }
        return false;
    }

    /**
     * function to return true if the field should be shown to the user
     */
    getVisible() {
        if (this.getAlwaysHidden()) {
            return false;
        }
        if (this.getDeleted()) {
            return false;
        }
        if (this.data.visible != null && this.data.visible === true) {
            return true;
        }
        if (this.data.visible != null && this.data.visible === false) {
            return false;
        }
        if (this.isGrouped != null && this.isGrouped === true) {
            return false;
        }
        return true;
    }

    /**
     * function to return true if the field is "always hidden", meaning,
     * not even the admin should see the field.
     * This is for internal fields, id numbers,
     * other things that would not ever need to be shown to anyone.
     *  The visibility can't be changed.
     */
    getAlwaysHidden() {
        if ((this.data.hideable != null) && (this.data.hideable === true)) {
            return true;
        }
        return false;
    }

    /**
     * function to return true if the field is "removed" by user
     */
    getDeleted() {
        if ((this.data.deleted != null) && (this.data.deleted === true)) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if the field is clickable.
     * This would only be true if there is an action defined in the formatter.
     */
    getClickable() {

        if ((this.clickable != null) && (this.clickable === true)) {
            return true;
        }

        if ((this.clickable != null) && (this.clickable === false)) {
            return false;
        }

        if ((this.data.clickable != null) && (this.data.clickable === true)) {
            return true;
        }

        if ((this.data.clickable != null) && (this.data.clickable === false)) {
            return false;
        }

        const f = this.getFormatter();
        if ((f != null) && (f.clickable != null) && (f.clickable === true)) {
            return true;
        }

        return false;
    }

    /**
     * function to return "options" for the field.
     * The options are used by the data formatter.\
     * The options for a
     * number field would be the numeral() module formatting,
     * the date would be the moment() module formatting,
     * the enum types have an array or list of options, etc.
     */
    getOptions() {
        if (this.data.options != null) {
            return this.data.options;
        }
        return null;
    }

    /**
     * Change the configuration for a given option to a new value.
     * For example, "width" to 100.
     * @param varName
     * @param value
     */
    changeColumn(varName: string, value: any) {
        if (this.data[varName] === value) {
            return;
        }

        if ((varName === "renderFunction") || (varName === "render")) {
            if (value === "") {
                value = null;
            }
            this.renderFunctionCache = null;
            this.data.render         = value;
        } else if (varName === "cellColor") {
            this.colorFunctionCache = null;
        }

        // console.log "changeCol #{varName} to #{value} for #{@getSource()}"
        this.data[varName] = value;
        delete this.formatter;
        delete this.actualWidth;
        return true;
    }

    /**
     * function to open tooltip from DataFormatter if user hover mouse
     * Use the configuration of the table to generate a short text to be used as a tooltip.
     * Uses the Data Formatter associated with the column
     * @param row for tooltip in table
     * @param value to display with tooltip
     * @param tooltipWindow which is instance of tooltip popup window
     */
    renderTooltip(row: any, value: any, tooltipWindow: any) {
        const f = this.getFormatter();
        if ((f != null) && (f.renderTooltip != null)) {
            return f.renderTooltip(row, value, tooltipWindow);
        } else {
            console.log("renderTooltip formatter not found:", f);
        }

        return false;
    }

    /**
     * function to return value
     * Uses the configuration to return a printable / display value.
     * If the column has a formatting function (render) or formatter type then it will be used.
     * Returns html/text to be displayed.
     * @param value [mixed] The specific value for the field
     * @param keyValue [mixed] not sure, to be documented
     * @param row [object] The full object for the row as this column might need other fields to render.
     */
    renderValue(value: any, keyValue: any, row: any) {

        let f = this.getRenderFunction();
        if (f != null) {
            return f(value, this.tableName, this.getSource(), keyValue, row);
        }

        f = this.getFormatter();
        if (f != null) {
            return f.format(value, this.getOptions(), this.tableName, keyValue);
        }

        return value;
    }

    /**
     * handle the column once it gets focused
     * @param e
     * @param col
     * @param data
     */
    onFocus(e: any, col: any, data: any) {
        const f = this.getFormatter();
        if ((f != null) && (f.onFocus != null)) {
            f.onFocus(e, col, data);
        }
        return true;
    }

    /**
     * -gao
     * get cell color value
     * @param value
     * @param keyValue
     * @param row
     */
    getCellColor(value: any, keyValue: any, row: any) {
        if (!this.data.hasColorFunction) {
            return null;
        }
        const f = this.getColorFunction();
        if (f != null) {
            return f(value, this.tableName, this.getSource(), keyValue, row);
        }

        return null;
    }

    /**
     * RenderHeader function to render the header for the column
     * @param parent extra class name that will be included in the th
     * @param location html the html for the th
     * @constructor
     */
    RenderHeader(parent: any, location: any) {

        if (this.visible === false) {
            return;
        }

        let html = this.getName();
        if (this.sort === -1) {
            html += "<i class='pull-right fa fa-sort-down'></i>";
        } else if (this.sort === 1) {
            html += "<i class='pull-right fa fa-sort-up'></i>";
        }

        parent.html(html);
        parent.addClass("tableHeaderField");

        return parent;
    }

    RenderHeaderHorizontal(parent: any, location: any) {

        if (this.visible === false) {
            return;
        }

        parent.html(this.getName());
        parent.addClass("tableHeaderFieldHoriz");
        parent.el.css({
            "text-align"      : "right",
            "padding-right"   : 8,
            "border-right"    : "1px solid #CCCCCC",
            "background-color": "linear-gradient(to right, #fff, #f2f2f2);"
        });

        this.sort = 0;

        return parent;
    }

    UpdateSortIcon(newSort: any) {

        this.sort = newSort;
        this.tagSort.removeClass("fa-sort");
        this.tagSort.removeClass("fa-sort-up");
        this.tagSort.removeClass("fa-sort-down");

        if (this.sort === -1) {
            this.tagSort.addClass("fa-sort-down");
        } else if (this.sort === 0) {
            this.tagSort.addClass("fa-sort");
        } else if (this.sort === 1) {
            this.tagSort.addClass("fa-sort-up");
        }

        return true;
    }

    /**
     * function to render the field (computes a value)
     */
    getRenderFunction() {
        if (this.renderFunctionCache != null) {
            return this.renderFunctionCache;
        }

        if ((this.data.render == null)) {
            return null;
        }

        if ((typeof this.data.render === "string") && (this.data.render.charAt(0) === '=')) {
            return this.excelStatementRender(this.data.render.substr(1), this.scope);
        }

        const template = `\
try {
XXCODEXX
} catch (e) { console.log("Render error:",e); console.log("val=",val,"tableName=",tableName,"fieldName=",fieldName,"id=",id); return "Error"; }\
`;

        let funcStr = this.data.render.toString();
        if (this.data.render.toString().includes("function")) {
            funcStr = this.data.render.toString().match(/function[^{]+\{([\s\S]*)\}$/)[1];
        }

        this.renderFunctionCache = new Function("val", "tableName", "fieldName", "id", "row", template.replace("XXCODEXX", funcStr));
        return this.renderFunctionCache;
    }

    /**
     * -gao
     * function to get color of cell
     */
    getColorFunction() {
        if (this.colorFunctionCache != null) {
            return this.colorFunctionCache;
        }
        if ((this.data.cellColor == null)) {
            return null;
        }

        if ((typeof this.data.cellColor === "string") && (this.data.cellColor.charAt(0) === '=')) {
            return this.excelStatementRender(this.data.cellColor.substr(1), this.scope);
        }

        //
        // remove function() wrapper and just use code
        const reFunctionFind = /function[^{]+\{([\s\S]*)\}$/;
        if (typeof this.data.cellColor !== "string") {
            this.data.cellColor = this.data.cellColor.toString();
        }
        const m = this.data.cellColor.match(reFunctionFind);
        if (m != null) {
            this.data.cellColor = m[1];
        }

        try {
            this.colorFunctionCache = new Function("val", "tableName", "fieldName", "id", "row", this.data.cellColor);
            return this.colorFunctionCache;
        } catch (e) {
            console.log("Error getting function from code:", this.data.cellColor);
            return null;
        }
    }

    /**
     * -gao
     * check if this column has cell color function
     */
    getHasColorFunction() {
        if (this.colorFunctionCache != null) {
            return true;
        }
        if (this.data.cellColor != null) {
            return true;
        }
        return false;
    }

    /**
     * function to return instance of DataFormatter
     */
    getFormatter() {
        if (this.formatter) {
            return this.formatter;
        }
        return this.formatter = dataFormatter.getFormatter(this.getType());
    }

    /**
     * function to return initial column
     * Called once when the column is created to see if the
     * class wants to update the information on the column type
     */
    deduceInitialColumnType() {
        if ((this.data.type != null) && (typeof this.data.type === "string") && (this.data.type.length > 0)) {
            return false;
        }

        const reYear     = /year/i;
        const reDistance = /distance/i;

        this.data.skipDeduce       = false;
        this.data.deduceAttempts   = 0;
        this.data.foundOnlyNumbers = true;

        if (/Date/i.test(this.data.name)) {
            // @changeColumn "type", "datetime"
            this.changeColumn("type", "timeago");
            this.changeColumn("width", 110);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/ Date/i.test(this.data.name)) {
            this.changeColumn("type", "age");
            this.changeColumn("width", 110);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/ Price/i.test(this.data.name)) {
            this.changeColumn("type", "money");
            this.changeColumn("width", 90);
            this.changeColumn("align", "right");
            this.data.skipDeduce = true;
            return;
        }

        if (/Is /i.test(this.data.name)) {
            this.changeColumn("type", "boolean");
            this.changeColumn("width", 70);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/^Is/i.test(this.data.name)) {
            this.changeColumn("type", "boolean");
            this.changeColumn("width", 70);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (reYear.test(this.data.name)) {
            this.changeColumn("type", "int");
            this.changeColumn("options", '////');
            this.changeColumn("width", 50);
            this.changeColumn("align", "right");
            this.data.skipDeduce = true;
            return;
        }

        if (reDistance.test(this.data.name)) {
            this.changeColumn("type", "distance");
            this.changeColumn("width", 66);
            this.changeColumn("align", "right");
            this.data.skipDeduce = true;
            return;
        }

        if (this.data.name === "id") {
            this.changeColumn("type", "text");
            this.changeColumn("width", null);
            this.changeColumn("visible", false);
            this.changeColumn("align", "left");
            this.changeColumn("name", "ID");
            return;
        }

        if ((this.data.source === "lat") || (this.data.source === "lon")) {
            this.changeColumn("type", "decimal");
            this.changeColumn("width", 60);
            this.changeColumn("visible", true);
            this.changeColumn("align", "right");
            this.changeColumn("options", '#.//////');
            return;
        }

        if (/^sourcecode/i.test(this.data.name)) {
            this.changeColumn("type", "sourcecode");
            this.changeColumn("width", 60);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/^memo/i.test(this.data.name)) {
            this.changeColumn("type", "memo");
            this.changeColumn("width", 60);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/^imagelist/i.test(this.data.name)) {
            this.changeColumn("type", "imagelist");
            this.changeColumn("width", 60);
            this.changeColumn("align", "left");
            this.data.skipDeduce = true;
            return;
        }

        if (/^number system/i.test(this.data.name)) {
            this.changeColumn("type", "number_system");
            this.changeColumn("width", 60);
            this.data.skipDeduce = true;
            return;
        }

    }

    /**
     * Given some new data, see if we need to automatically change
     * the data type on this column.
     * @param newData
     */
    deduceColumnType(newData: any) {

        let columnObj;
        const checkObjectArray = function (array: any[]) {
            if (array.length === 0) {
                return false;
            }
            let result = true;
            for (let a of Array.from(array)) {
                if (!a || (typeof a !== "object")) {
                    result = false;
                }
            }
            return result;
        };

        //  -Liu
        //  check defaultColumn object to check
        //  this column is contained in it
        columnObj = Column.defaultColumn[this.data.name != null ? this.data.name.toLowerCase() : undefined];
        if (columnObj != null) {
            for (let varName in columnObj) {
                const value = columnObj[varName];
                this.changeColumn(varName, value);
            }
            return true;
        }

        if ((this.data.skipDeduce != null) && (this.data.skipDeduce === true)) {
            return null;
        }
        if (this.data.deduceAttempts++ > 50) {
            return null;
        }
        if ((newData == null)) {
            return null;
        }
        if (this.data.type !== "text") {
            return null;
        }

        if (typeof newData === "string") {

            if (this.reDate1.test(newData)) {
                this.changeColumn("type", "timeago");
                this.changeColumn("width", 80);
                this.data.skipDeduce = true;
                return;
            }

            if (this.reDate2.test(newData)) {
                this.changeColumn("type", "timeago");
                this.changeColumn("width", 110);
                this.data.skipDeduce = true;
                return true;
            }

            // console.log "name=", @data.name, "newdata=", newData, typeof newData, @data.skipDeduce

            if (/^https*/.test(newData)) {
                this.changeColumn("type", "link");
                this.changeColumn("align", "center");
                this.changeColumn("width", 80);
                this.data.skipDeduce = true;
                return true;
            }

            if (/^ftp*:/.test(newData)) {
                this.changeColumn("type", "link");
                this.changeColumn("align", "center");
                this.changeColumn("width", 80);
                this.data.skipDeduce = true;
                return true;
            }

            if (this.data.foundOnlyNumbers && this.reNumber.test(newData)) {
                this.changeColumn("type", "int");
                this.changeColumn("width", 80);
                return;
            }

            if (this.data.foundOnlyNumbers && this.reDecimal.test(newData)) {
                this.changeColumn("type", "decimal");
                this.changeColumn("width", 100);
                return;
            }

            if (this.data.foundOnlyNumbers) {
                this.changeColumn("type", "text");
                this.data.foundOnlyNumbers = false;
            }

        } else if (typeof newData === "number") {

            if (this.data.type === "text") {
                this.changeColumn("type", "int");
                this.changeColumn("align", "right");
                this.changeColumn("width", 80);
            }

            if (Math.floor(newData) !== Math.ceil(newData)) {
                this.changeColumn("type", "decimal");
                this.changeColumn("align", "right");
                this.changeColumn("width", 80);
                this.changeColumn("options", "#,//#.//#");
            }

        } else if (typeof newData === "boolean") {

            this.changeColumn("type", "boolean");
            this.changeColumn("width", 70);
            this.data.skipDeduce = true;
            return true;

        } else if (typeof newData === "object") {

            if (newData.getTime != null) {
                this.changeColumn("type", "age");
                this.changeColumn("width", "130");
                this.data.skipDeduce = true;
            } else if (Array.isArray(newData)) {
                if (checkObjectArray(newData)) {
                    this.changeColumn("type", "array");
                    this.changeColumn("width", null);
                } else {
                    this.changeColumn("type", "tags");
                    this.changeColumn("autosize", true);
                    this.changeColumn("width", null);
                }
            } else {
                this.changeColumn("type", "simpleobject");
                this.changeColumn("width", null);
                this.data.skipDeduce = true;
            }
            return true;
        }

        return null;
    }

    getAutoSize() {
        if ((this.data.autosize != null) && (this.data.autosize === true)) {
            return true;
        }
        const width = this.getWidth();
        if ((width != null) && (width > 0)) {
            return false;
        }
        return true;
    }

    /**
     * function to return width ( px ) of this column
     */
    getWidth() {

        if (typeof this.data.width === "string") {
            this.data.width = parseInt(this.data.width, 10);
        }

        // if width is 0 then consider as auto width = left padding + max length text width + right padding
        if ((this.data.width === 0) || (this.data.width === '0px') || (this.data.width === "") || (this.data.width == null)) {
            const f = this.getFormatter();
            if ((f != null) && (f.width != null) && (f.width > 0)) {
                return f.width;
            }

            return null;
        }

        return this.data.width;
    }

    /**
     * -gao
     * function to get linked field
     */
    getLinked() {
        if (this.data.linked) {
            return this.data.linked;
        }
        return null;
    }

    /**
     * function to get "pii" field
     */
    getPii() {
        if (this.data.pii) {
            return this.data.pii;
        }
        return null;
    }

    /**
     * function to get "blind" field
     */
    getBlind() {
        if (this.data.blind) {
            return this.data.blind;
        }
        return null;
    }

    /**
     * function to get if this column is in "title locked columns" in the table
     */
    getLocked() {
        if (this.data.locked) {
            return this.data.locked;
        }
        return false;
    }

    /**
     * -gao
     * funciton to get permissions to edit
     */
    getEditPermissions() {
        if (!this.data.editPermissions) {
            return null;
        }
        const value = this.data.editPermissions;
        //  if editpermission is string, then it should be converted to Array
        if (Array.isArray(value)) {
            return value;
        } else if (typeof value === "string") {
            return value.split(',');
        }
        return null;
    }

    /**
     * -gao
     * funciton to get permissions to view
     */
    getViewPermissions() {
        if (!this.data.viewPermissions) {
            return null;
        }
        const value = this.data.viewPermissions;
        //  if viewPermissions is string, then it should be converted to Array
        if (Array.isArray(value)) {
            return value;
        } else if (typeof value === "string") {
            return value.split(',');
        }
        return null;
    }

    //
    //  Flags (permisssions) that are required to view the field.   Should be validated
    //  against the current user.   One of the flags in the array should be valid.
    /**
     * Flags (permisssions) that are required to view the field.   Should be validated
     * against the current user.   One of the flags in the array should be valid.
     */
    getFlagsRequiredView() {
        if (!this.data.flagsview) {
            return [];
        } else if (Array.isArray(this.data.flagsview)) {
            return this.data.flagsview;
        } else if (typeof this.data.flagsview === "string") {
            return this.data.flagsview.split(',');
        }
        return [];
    }

    /**
     * Flags (permissions) that are required to edit the field assuming editing is enabled.
     */
    getFlagsRequriedEdit() {
        if (!this.data.flagsedit) {
            return [];
        } else if (Array.isArray(this.data.flagsedit)) {
            return this.data.flagsedit;
        } else if (typeof this.data.flagsedit === "string") {
            return this.data.flagsedit.split(',');
        }

        return [];
    }

    /**
     * If true (default) show this field in the new form
     */
    getVisibleOnNew() {
        if ((this.data.visiblenewform != null) && (typeof (this.data.visiblenewform) === "boolean")) {
            return this.data.visiblenewform;
        }
        if (this.getIsCalculation()) {
            return false;
        }
        if (this.getSource().toLowerCase() === "id") {
            return false;
        }
        if (this.getSource().charAt(0) === "_") {
            return false;
        }
        return true;
    }

    getVisibleOnEdit() {
        if ((this.data.visibleeditform != null) && (typeof (this.data.visibleeditform) === "boolean")) {
            return this.data.visibleeditform;
        }
        if (this.getIsCalculation()) {
            return false;
        }
        if (this.getSource().toLowerCase() === "id") {
            return false;
        }
        if (this.getSource().charAt(0) === "_") {
            return false;
        }
        return true;
    }

    //
    // ------------------------------------ Properties that deal with validation -----------------------------------

    //
    //  Should a value be validated prior to allowing a new record to be created?
    getValidateOnCreate() {
        if ((this.data.validateoncreate != null) && (typeof (this.data.validateoncreate) === "boolean")) {
            return this.data.validateoncreate;
        }
        if (this.getIsCalculation()) {
            return false;
        }
        if (this.getSource().toLowerCase() === "id") {
            return false;
        }
        if (this.getSource().charAt(0) === "_") {
            return false;
        }
        return true;
    }

    //
    //  Should a valid be validated prior to allowing a change to the record?
    getValidateOnEdit() {
        if ((this.data.validateonedit != null) && (typeof (this.data.validateonedit) === "boolean")) {
            return this.data.validateonedit;
        }
        if (this.getIsCalculation()) {
            return false;
        }
        if (this.getSource().toLowerCase() === "id") {
            return false;
        }
        if (this.getSource().charAt(0) === "_") {
            return false;
        }
        return true;
    }

    //
    //  An optional message to show if validation fails.   Only Error or Warning should be returned.
    //  an error message should stop edit/create but a wanring should not.
    getValidateErrorMessage() {
        if ((this.data.validate_err_message != null) && (typeof (this.data.validate_err_message) === "string")) {
            return this.data.validate_err_message;
        }
        return null;
    }

    //
    //
    getValidateWarningMessage() {
        if ((this.data.validate_warn_message != null) && (typeof (this.data.validate_warn_message) === "string")) {
            return this.data.validate_warn_message;
        }

        return null;
    }

    //
    //   Could be either javascript code or number or a range of numbers of whatever is required for validation.
    //   format and value to be determined.
    getValidateOptions() {
        if (this.data.validate_options != null) {
            return this.data.validate_options;
        }

        return null;
    }

    getValidationType() {
        if (this.data.validation_type != null) {
            return this.data.validation_type;
        }

        return null;
    }

    getValidationDetails() {
        if (this.data.validation_details != null) {
            return this.data.validation_details;
        }

        return null;
    }

    /**
     * function to edit the column
     */
    getEditFunction() {
        if (this.editFunctionCache != null) {
            return this.editFunctionCache;
        }

        if ((this.data.editFunction == null)) {
            return null;
        }

        if ((typeof this.data.editFunction === "string") && (this.data.editFunction.charAt(0) === '=')) {
            return this.excelStatementRender(this.data.editFunction.substr(1), this.scope);
        }

        let funcStr = this.data.editFunction.toString();
        if (funcStr.includes("function")) {
            funcStr = this.data.editFunction.toString().match(/function[^{]+\{([\s\S]*)\}$/)[1];
        }

        this.editFunctionCache = new Function("el", "val", "path", "saveCallback", funcStr);
        return this.editFunctionCache;
    }

    /**
     * function to get this column
     */
    serialize() {

        const obj: any = {};
        if (this.data.name != null) {
            obj.name = this.data.name;
        }

        if (this.data.type != null) {
            obj.type = this.data.type;
        }

        if (this.data.default != null) {
            obj.type = this.data.default;
        }

        if (this.data.width != null) {
            obj.width = this.data.width;
        }

        if (this.data.options != null) {
            obj.options = this.data.options;
        }

        if (this.data.editable != null) {
            obj.editable = this.data.editable;
        }

        if (this.data.visible != null) {
            obj.visible = this.data.visible;
        }

        if (this.data.clickable != null) {
            obj.clickable = this.data.clickable;
        }

        if (this.data.align != null) {
            obj.align = this.data.align;
        }

        if (this.data.source != null) {
            obj.source = this.data.source;
        }

        if (this.data.required != null) {
            obj.required = this.data.required;
        }

        if (this.data.hideable != null) {
            obj.hideable = this.data.hideable;
        }

        if (this.data.deleted != null) {
            obj.deleted = this.data.deleted;
        }

        if (this.data.system != null) {
            obj.system = this.data.system;
        }

        if (this.data.autosize != null) {
            obj.autosize = this.data.autosize;
        }

        if (this.data.order != null) {
            obj.order = this.data.order;
        }

        if (this.data.calculate != null) {
            obj.calculate = this.data.calculate;
        }

        if (this.data.render != null) {
            obj.render = this.data.render;
        }

        if (this.data.cellColor != null) {
            obj.cellColor = this.data.cellColor;
        }

        if (this.data.linked != null) {
            obj.linked = this.data.linked;
        }

        if (this.data.editPermissions != null) {
            obj.editPermissions = this.data.editPermissions;
        }

        if (this.data.viewPermissions != null) {
            obj.viewPermissions = this.data.viewPermissions;
        }

        obj.pii    = this.getPii();
        obj.blind  = this.getBlind();
        obj.locked = this.getLocked();

        //  Permissions settings
        //
        obj.flagsview       = this.getFlagsRequiredView();
        obj.flagsedit       = this.getFlagsRequriedEdit();
        obj.visiblenewform  = this.getVisibleOnNew();
        obj.visibleeditform = this.getVisibleOnEdit();

        //  Validation settings
        //
        obj.validateoncreate      = this.getValidateOnCreate();
        obj.validateonedit        = this.getValidateOnEdit();
        obj.validate_err_message  = this.getValidateErrorMessage();
        obj.validate_warn_message = this.getValidateWarningMessage();
        obj.validate_options      = this.getValidateOptions();

        obj.validation_type    = this.getValidationType()
        obj.validation_details = this.getValidationDetails()

        //  Edit function
        if (this.data.editFunction) {
            obj.editFunction = this.data.editFunction;
        }
        return obj;
    }

    /**
     * function to set column value from configuration
     * @param obj - obj to set column's value
     */
    deserialize(obj: any) {
        this.renderFunctionCache = null;
        this.colorFunctionCache  = null;
        for (const key in this.data) {
            delete this.data[key];
        }

        for (const varName in obj) {
            const value = obj[varName];
            this.changeColumn(varName, value);
        }

        return true;
    }
}

export default Column;