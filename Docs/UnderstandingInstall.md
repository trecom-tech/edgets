# Setup a server for the Edge

* [Read a guided walkthrough for Ubuntu](EdgeServerInstall.md)

## Configuration Folders

Create the require folders on the server.  Login as the user that will
run the Edge software:

	mkdir ~/EdgeConfig
	mkdir ~/EdgeData
	mkdir ~/EdgeData/logs
	mkdir ~/EdgeData/import

Copy the credentials from your secure system to the target system

	scp ~/EdgeConfig/* newServer:EdgeConfig/

Make sure node is up to date

	node -v
	npm -v

On OSX make sure Brew is up to date

	brew update
	brew doctor

Install the "Jobs" repo, this is the main application that needs to be on the server

	cd ~/rtc  (or whatever the base folder is that everyone is using)
	git clone git@gitlab.protovate.com:Edge/Jobs.git
	cd Jobs
	npm install

Configure the main jobs that this server would need

    cp AppLauncherConfig-example.json ~/EdgeConfig/AppLauncherConfig.json
    vim ~/EdgeConfig/AppLauncherConfig.json

Starting App Server

* [Read more about the AppServer and JobServer](http://gitlab.protovate.com/Edge/Jobs/blob/master/README.md)
* [Read more about the JobTool](http://gitlab.protovate.com/Edge/Jobs/blob/master/UnderstandingJobTool.md)

Run the AppLauncher (Microservices that are always running)

	c src/AppLauncher

Run the JobLauncher (Micrososervices that are run on a schedule)

	c src/JobServer



	
	

