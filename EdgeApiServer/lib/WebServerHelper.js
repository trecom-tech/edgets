"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const stylus = require('stylus');
const nib = require('nib');
const pug = require('pug');
const chalk = require('chalk');
const version = require('project-version');
const coffeeScript = require('coffeescript');
const less = require('less');
const os = require("os");
const fs = require("fs");
const edgecommonconfig_1 = require("edgecommonconfig");
//  Set to true when you want to know when requsts are not found.
let globalDebugMissingFiles = false;
//  Helper Class
class WebServerHelper {
    /**
     * Given a filename and path list, resolve with the full path that exists.
     * @param filename
     * @param pathList
     * @param fake
     */
    static doFindFileInPath(filename, pathList) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = edgecommonconfig_1.default.FindFileInPath(filename, pathList, false);
            if (result == null) {
                if (globalDebugMissingFiles) {
                    console.log(`File '${filename}' ${filename.length} not found in :`, JSON.stringify(pathList));
                }
                return null;
            }
            return result;
        });
    }
    static doCompileLessFiles(filenameList) {
        return new Promise(function (resolve, reject) {
            try {
                let strContent = '';
                for (let filename of filenameList) {
                    strContent += fs.readFileSync(filename);
                }
                return less.render(strContent, { compress: false }, (err, output) => {
                    if (err != null) {
                        console.log('LESS Compile Error:', err);
                    }
                    resolve(output);
                });
            }
            catch (e) {
                console.log(`doCompileLessFile, file not found ${filenameList}`);
                resolve({ css: '' });
            }
        });
    }
    static doCompileStylusFile(filename) {
        return new Promise(function (resolve, reject) {
            if (filename == null) {
                resolve('');
            }
            return fs.readFile(filename, 'utf8', function (_err, content) {
                try {
                    return stylus(content).set('filename', filename).set('compress', false).use(nib()).render((err, css) => {
                        if (err != null) {
                            console.log(chalk.yellow('Error in Stylus file: ') + filename);
                            console.log(err);
                            return resolve('');
                        }
                        else {
                            return resolve(css);
                        }
                    });
                }
                catch (ex) {
                    if (content == null) {
                        console.log(`No such file: ${filename}`);
                    }
                    else {
                        console.log('Inner exception from Stylus: ', chalk.yellow(ex));
                    }
                    return resolve('');
                }
            });
        });
    }
    static doCompilePugFile(filename) {
        return new Promise(function (resolve, reject) {
            if (filename == null) {
                resolve('');
            }
            try {
                fs.readFile(filename, 'utf8', function (err, content) {
                    if (err != null) {
                        resolve('');
                    }
                    let html = pug.render(content, {
                        filename,
                        pretty: false,
                        debug: false,
                        buildnum: version,
                        ioserver: os.hostname() + ':' + edgecommonconfig_1.default.WebsocketPort,
                    });
                    resolve(html);
                });
            }
            catch (e) {
                console.log('Unable to compile jade: ', filename);
                resolve('');
            }
        });
    }
    static doCompileCoffeeFile(filename) {
        return new Promise((resolve, reject) => {
            try {
                edgecommonconfig_1.default.status('doCompileCoffeeFile Reading File:', filename);
                if (filename == null) {
                    edgecommonconfig_1.default.status('doCompileCoffeeFile bad call, missing filename');
                    resolve('');
                }
                fs.readFile(filename, 'utf8', function (_err, content) {
                    try {
                        let compiled = coffeeScript.compile(content, { bare: true });
                        resolve(compiled);
                    }
                    catch (ex) {
                        console.log('Content=', content);
                        console.log('Inner exception from CoffeeScript: ', chalk.yellow(ex));
                        resolve();
                    }
                });
            }
            catch (err) {
                reject(err);
            }
        });
    }
}
exports.default = WebServerHelper;
;
//# sourceMappingURL=WebServerHelper.js.map