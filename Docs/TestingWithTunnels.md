## Introduction to ssh tunneling

SSH tunneling is a method of transporting arbitrary networking data over an encrypted SSH connection.
SSH is a standard for secure remote logins and file transfers over untrusted networks. 
It also provides a way to secure the data traffic of any given application using port forwarding, basically tunneling any TCP/IP port over SSH. 
This means that the application data traffic is directed to flow inside an encrypted SSH connection so that it cannot be eavesdropped or intercepted while it is in transit. 
SSH tunneling enables adding network security to legacy applications that do not natively support encryption.

The SSH connection is used by the application to connect to the application server. 
With tunneling enabled, the application contacts to a port on the local host that the SSH client listens on. 
The SSH client then forwards the application over its encrypted tunnel to the server. 
The server then connects to the actual application server - usually on the same machine or in the same data center as the SSH server. 
The application communication is thus secured, without having to modify the application or end user workflows.


### SSH Tunnel - Port Forwarding

There are two ways to create an SSH tunnel, local and remote port forwarding.
We can use local port forwarding for testing of our application

### How we can use ssh tunnel for testing of our application?

We can use ssh tunnel to connect to databases and services hosted in devserver instead of using local databases and services.
For this usecase, we can use local port forwarding technology.

For example, to connect mongodb hosted in devserver, we can create ssh tunnel like following:

`$ ssh -L 27017:localhost:27017 qiwong@40.112.54.39`

*Please note that you should have ssh access to the dev server to create ssh tunnel.


After ssh tunnel is created, we can connect to mongodb as if it's hosted locally.

### Setting up ssh tunnel on MAC using SSH Core Tunnel

SSH tunnel can be easily setup using SSH Core Tunnel on MAC.

https://coretunnel.app/

![Image of Core TUnnel](files/core_tunnel.png)
