//
// class for timeago data type
//
// @extends [DataFormatterType]
//
import { DataFormatterType } from './DataFormatterType';

const numeral = require('numeral');

export class DataFormatDuration extends DataFormatterType {
    name: string        = 'duration';
    width: number       = 90;
    input_width: number = 90;
    align: string       = 'right';

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        let min;
        let txt;
        if ((data == null)) {
            return "";
        }

        if (typeof data === "string") {
            data = parseFloat(options);
        }

        let sec = data / 1000;
        if (sec < 60) {
            txt = numeral(sec).format("#.###") + " sec";
        } else if (sec < (60 * 60 * 2)) {
            min = Math.floor(sec / 60);
            sec = sec - (min * 60);
            txt = min + " min, " + Math.floor(sec) + " sec.";
        } else {
            const hrs = Math.floor(sec / (60 * 60));
            min       = Math.floor((sec - (hrs * 60 * 60)) / 60);
            txt       = hrs + " hrs, " + min + " min";
        }

        return txt;
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    unformat(data: any, path: any) {
        return data;
    }
}

export default DataFormatDuration;