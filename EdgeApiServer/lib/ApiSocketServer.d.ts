/// <reference types="socket.io" />
import * as redis from 'redis';
import RateCapture from 'edgecommonratecapture';
import ApiSocketUser from './ApiSocketUser';
import ApiWebServer from './ApiWebServer';
import { EdgeApiDefinition, EdgeSocketRequestData } from 'edgeapi';
import { IPubSubData } from './interfaces/IPubSubData';
export declare const REDIS_PUBSUB_CHANNEL = "MESSAGES";
declare class ApiSocketServer {
    bar: RateCapture;
    io: SocketIO.Server;
    statusSetupServerPromise: Promise<any>;
    setupChangeEventServerPromise: Promise<any>;
    redisPubsubQueue: redis.RedisClient;
    cache: redis.RedisClient;
    log: import("edgecommonconfig").DelayedWinstonLog;
    statusChanges: any[];
    statusMessages: any[];
    statusUpdates: any[];
    localApiCallList: any;
    registerdCalls: any;
    private allClients;
    globalUnique: number;
    socketCount: number;
    webServer: ApiWebServer;
    total_api_calls_done: number;
    total_api_calls: number;
    static getRedisReadonlyConnection(): redis.RedisClient;
    static getRedisConnection(): redis.RedisClient;
    constructor(apiWebServer: ApiWebServer);
    /**
     *
     * @param apiWebServer
     */
    onInit(apiWebServer: ApiWebServer): redis.RedisClient;
    /**
     * Add a custom URL or Path
     * @param pathPattern {string|regex} The pattern to match
     * @param callback {function}
     */
    addRoute(pathPattern: string, callback: Function): void;
    /**
     * Remove any registrations that the user is setup for
     * @param user
     */
    removeRegistrations(user: ApiSocketUser): boolean;
    onRegisterApiFunction(apiDefinition: EdgeApiDefinition, user: ApiSocketUser): boolean;
    /**
     * Handles incoming channel messages
     * @param data
     */
    onIncomingChannelMessage(data: IPubSubData): void;
    /**
     * Handles incoming change messages
     * @param data
     */
    onIncomingChangeNotification(data: any): boolean;
    setupChangeEventServer(): Promise<any>;
    /**
     * Setup an internal server that waits for remote status messages
     */
    setupStatusServer(): Promise<any>;
    /**
     *
     * @param user
     * @param uuid
     * @param newResult
     */
    doSendFlatResponse(user: ApiSocketUser, uuid: number, newResult: any): boolean;
    /**
     *
     * @param user
     * @param callName
     * @param data
     * @constructor
     */
    MakeDynamicCall(user: ApiSocketUser, callName: string, data: EdgeSocketRequestData): any;
    sendApiList(user: ApiSocketUser): boolean;
    resendApiList(): boolean;
    setupSocketServer(httpApp: any, httpsApp: any): boolean;
}
export default ApiSocketServer;
