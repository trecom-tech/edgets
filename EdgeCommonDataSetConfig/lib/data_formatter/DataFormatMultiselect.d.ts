import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatMultiselect extends DataFormatterType {
    name: string;
    options: any[];
    align: string;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    format(currentValue: any, options: any, path: any): string;
    unformat(currentValue: any, path: any): any;
}
export default DataFormatMultiselect;
