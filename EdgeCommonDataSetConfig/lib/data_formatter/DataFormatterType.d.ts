/// <reference types="jquery" />
export declare class DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    editorShowing: boolean;
    editorPath: string;
    align: string;
    elEditor: any;
    onSaveCallback: any;
    elDataEditor: any;
    constructor();
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    attachEditor(elSourceInput: any, path: any): void;
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    allowKey(keyCode: any): boolean;
    editData(parentElement: any, currentValue: any, path: any, onSaveCallback: any): boolean;
    saveValue(newValue: any): boolean;
    appendEditor(): JQuery<HTMLElement>;
    onGlobalMouseDown(e: any): boolean;
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
    getValueInTooltip(value: any): any;
}
export default DataFormatterType;
