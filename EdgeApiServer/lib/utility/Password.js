"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require('crypto');
const edgecommonconfig_1 = require("edgecommonconfig");
function generatePasswordHash(password) {
    const ha1 = new crypto.createHash('md5');
    ha1.update(edgecommonconfig_1.config.getCredentials('secretKey') + password);
    return ha1.digest("hex");
}
exports.generatePasswordHash = generatePasswordHash;
//# sourceMappingURL=Password.js.map