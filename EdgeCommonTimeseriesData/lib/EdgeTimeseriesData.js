"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const Influx = require('influx');
let globalIsDelayed = 0;
class TimeSeriesDatabase {
    constructor(parent, databaseName) {
        this.parent = parent;
        this.databaseName = databaseName;
        this.pendingPoints = [];
        this.pendingTimer = null;
    }
    /**
     * Send the pending data
     */
    sendTimer() {
        if (this.parent == null || this.parent.client == null) {
            this.pendingTimer = null;
            this.pendingPoints = [];
            return false;
        }
        this.parent.client
            .writePoints(this.pendingPoints, {
            database: this.databaseName,
            precision: 's',
        })
            .then(() => {
            this.pendingTimer = null;
            return (this.pendingPoints = []);
        })
            .catch((ex) => {
            let log = edgecommonconfig_1.config.getLogger('TimeSeriesData2');
            log.error('ExcpetionInWritePoints2', {
                database: this.databaseName,
                points: this.pendingPoints,
                ex,
            });
            this.pendingPoints = [];
            return (this.pendingTimer = null);
        });
        return true;
    }
    /**
     * Change to doSaveMeasurement for those that are approved during the testing phase.
     * @param measurement
     * @param tags
     * @param data
     */
    doSaveMeasurement(measurement, tags, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.parent.client == null) {
                return false;
            }
            // tslint:disable-next-line:no-control-regex
            let spaceReg = new RegExp('[ /\n\r]', 'g');
            measurement = measurement.replace(spaceReg, '');
            if (typeof tags !== 'object' || typeof data !== 'object') {
                data = {};
            }
            try {
                this.pendingPoints.push({
                    measurement,
                    tags,
                    fields: data,
                    timestamp: new Date(),
                });
                if (this.pendingTimer == null) {
                    this.pendingTimer = setTimeout(this.sendTimer, 10000);
                }
                return true;
            }
            catch (e) {
                console.log('doSaveMeasurement write exception:', e);
                console.log(`[${globalIsDelayed}] TimeSeriesDatabase doSaveMeasurement delay enabled on ${this.databaseName} for `, measurement, data);
                return false;
            }
        });
    }
}
exports.TimeSeriesDatabase = TimeSeriesDatabase;
class EdgeTimeseriesData {
    constructor() {
        this.base = '';
        this.dbCache = {};
        const credentials = edgecommonconfig_1.config.getCredentials('influxdb');
        if (credentials == null ||
            credentials.type == null ||
            credentials.type !== 'influxdb') {
            edgecommonconfig_1.config.error('InfluxDB credentials not found');
            return;
        }
        else if (credentials.type === 'influxdb') {
            this.client = new Influx.InfluxDB(credentials);
        }
        else {
            edgecommonconfig_1.config.error('TimeSeries config found but missing type = influxdb');
        }
        this.base = `http://${credentials.host}/`;
        this.log = edgecommonconfig_1.config.getLogger('Influx');
    }
    static getInstance() {
        if (EdgeTimeseriesData.singleInstance === null) {
            EdgeTimeseriesData.singleInstance = new EdgeTimeseriesData();
        }
        return EdgeTimeseriesData.singleInstance;
    }
    /**
     * Creates database only if it doesn't exist, returns success
     * @param databaseName
     */
    doCreateDatabase(databaseName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.dbCache[databaseName] != null) {
                return this.dbCache[databaseName];
            }
            if (this.client != null) {
                try {
                    let databases = yield this.client.getDatabaseNames();
                    if (!Array.from(databases).includes(databaseName)) {
                        return yield this.client.createDatabase(databaseName);
                    }
                    if (this.dbCache[databaseName] == null) {
                        this.dbCache[databaseName] = new TimeSeriesDatabase(this, databaseName);
                    }
                    return this.dbCache[databaseName];
                }
                catch (e) {
                    edgecommonconfig_1.config.error('InfluxDB createDatabase exception: ', e.message);
                    return false;
                }
            }
            else {
                return false;
            }
        });
    }
    /**
     * Retrieves all database names
     */
    doGetDatabases() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client == null) {
                return null;
            }
            try {
                return yield this.client.getDatabaseNames();
            }
            catch (e) {
                return edgecommonconfig_1.config.error('InfluxDB getDatabases exception: ', e.message);
            }
        });
    }
    /**
     * Retrieves all measurements per database name
     * @param databaseName
     * @param measurement
     * @param tags
     * @param data
     */
    doWriteMeasurement(databaseName, measurement, tags, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client == null) {
                throw edgecommonconfig_1.config.error('InfluxDB doWriteMeasurement exception: ', 'client is not initilized');
            }
            try {
                yield this.client.writePoints([
                    {
                        measurement: measurement,
                        tags: tags,
                        fields: data,
                    },
                ], {
                    database: databaseName,
                    precision: 's',
                });
                return true;
            }
            catch (e) {
                return edgecommonconfig_1.config.error('InfluxDB getMeasurements exception: ', e.message);
            }
        });
    }
    /**
     * Retrieves all measurements per database name
     * @param databaseName
     */
    doGetMeasurements(databaseName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client == null) {
                return null;
            }
            try {
                return yield this.client.getMeasurements(databaseName);
            }
            catch (e) {
                return edgecommonconfig_1.config.error('InfluxDB getMeasurements exception: ', e.message);
            }
        });
    }
    /**
     * Execute a query and get result, use fields same as '*' or 'cpu, mem, disk',
     * as usual select operation in database queries
     * @param databaseName
     * @param sql
     */
    doQuery(databaseName, sql) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client == null) {
                return null;
            }
            const opts = { database: databaseName };
            try {
                let results = {
                    data: [],
                };
                const all = yield this.client.query(sql, opts);
                for (const varName in all) {
                    const value = all[varName];
                    if (value.time != null) {
                        const d = new Date(value.time);
                        value.time = d;
                        results.data.push(value);
                    }
                }
                return results;
            }
            catch (e) {
                return edgecommonconfig_1.config.error('InfluxDB query exception: ', e.message);
            }
        });
    }
    dropDatabase(databaseName) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.client.dropDatabase(databaseName);
        });
    }
}
EdgeTimeseriesData.singleInstance = null;
exports.default = EdgeTimeseriesData;
//# sourceMappingURL=EdgeTimeseriesData.js.map