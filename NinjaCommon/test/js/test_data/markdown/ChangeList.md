# NinjaCommon ChangeLog

## 2018-06-07, Version 0.217.0 by @brianpollack, @gao, @song, @xin

### Notable Changes

* **Refactoring**
  * Moved global Bootstrap classes. [#340](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/340)
  * Refactored ViewForm's stylus code structure. [#340](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/340)
  * Changed Boolean DataFormatter's editor. [#339](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/339)
  * Refactored ViewFileDropTarget's code [#338](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/338)

### Commits

* [[`2c50efea`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/2c50efea30f28c7942bd8636c9cbf3c0d3221a58)] - **Update**: Moved global Boostrap classes [#340](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/340)
* [[`3be70fc4`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/3be70fc4e11cb123d29b165c1dc030a526137aef)] - **Update**: Updated structure of ViewForm's stylus code [#340](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/340)
* [[`a40ecfac`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/a40ecfacc3fbb230e8b25049061eee2137253f53)] - **Update**: Changed Boolean DataFormatter's editor [#339](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/339)
* [[`9be4bd52`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/9be4bd5294e2bb680c0146d2b4fa885cf5ea77a8)] - **Update**: Refactored ViewFileDropTarget's code [#338](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/338)

## 2018-06-06, Version 0.216.0 by @brianpollack, @gao, @liu, @song, @xin

### Notable Changes

* **Fixes in FileManager, Table, Calendar and Map**
  * Bunch bug fixes in FileManager [#334](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/334) [#335](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/335)
  * Custom Tooltip and fixes in Table [#332](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/332) [#330](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/330)
  * Bug fixes in Map [#331](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/331)
  * Added tooltip in Calendar [#333](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/333)

### Commits

* [[`06e4c05c`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/06e4c05c297f1fcdf2906e4127a57caad7e87a3f)] [[`a0d4d64b`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/a0d4d64be7babedc4c41dc709e2f63b253a63488)] - **Update**: Bug fixes in FileManager 
* [[`0a350201`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/0a350201b65c7f2063c21481769ea286e3100aec)] [[`c97ef0d9`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/c97ef0d9a95ca2ac9e8511d92c8266739acedc04)] [[`f0cef3a5`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/f0cef3a5e402bcabffaf9757482b90703a66207f)] - **Update**: Custom Tooltip and fixes in Table [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)

## 2018-06-01, Version 0.211.0 by @brianpollack, @gao, @song, @xin

### Notable Changes

* ** Fixes in Stepper, Table, FileManager and Tiles
  * Fixes in Table [#329](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/329)
  * Updates in FileManager 
  * Updates in Tiles [#327](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/327)
  * Updates in Stepper [#326](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/326)

### Commits

* [[`47907a11`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/47907a11f974c1cc3751c9e456096445b18f65b9)] - **Update** Fixes in Table
* [[`01a578e7`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/01a578e7acfe7009105f422146dc93268670ef83)] - **Update** Updates in FileManager
* [[`131388b2`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/131388b2f81431c0821cef58bf3d165439c23232)] [[`e35b7b46`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/e35b7b4686d7ea79e9c868722c17eb55f81170d1)] - **Update** Updates and fixes in Tiles
* [[`42ede0ad`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/42ede0adbd735cfc48cc199ddc1b4f50adef0517)] - **Update** Updates in Stepper

## 2017-10-20, Version 0.47.0 by @brianpollack, @gao

### Notable Changes

* **New Component: GridStack**
  * GridStack is now added into NinjaCommon. [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)
  * NinjaCommon's GridStack has some unique styles in it. [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)

### Commits

* [[`edea946f`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/edea946f1dbff1cf3a26bbfc963bcc8c3990b93e)] - **Add**: Add ViewGrid.coffee [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)
* [[`dc674d1b`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/dc674d1b5ddfad9e63f37e0d9254088c9b730bf5)] - **Update**: Update Ninjacontainer/addDiv [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)
* [[`fe92ee97`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/fe92ee97c37b4b83e258be5b002c5b67531fa4fa)] - **Add**: Add library files for Gridstack [#85](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/85)


## 2017-10-03, Version 0.46.0 by @brianpollack, @gao

### Notable Changes

* **Test Page Redesign**
  * SideBar is now organized by grouping similar cases and also looking better. [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
  * Ready to show both CoffeeScript and compiled JavaScript codes 
* **View Markdown File**
  * Markdown are possible to be previewed. [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
  * `UnderstandingNinja.md` that shows overview of NinjaCommon was added

### Commits

* [[`62619008`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/6261900867f087e10c7ab647c10b5d6b9d8dc6d2)] - **Add**: add function "addMarkdownfile" into test_common.coffee [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
* [[`aab6a677`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/aab6a677ae9274d2f7adf15af433f4cd378c0039)] - **Add**: add markdown-it.js into vendor [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
* [[`2665c15b`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/2665c15bec36f980c2f0b3fe8531f6242c18f643)] - **Add**: Add ViewMarkDown [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
 [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
* [[`2fc97d18`](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/commit/2fc97d18ed6e9db1f2f45698d560815a43c843c4)] - **Add**: Get coffee code of test functions
 [#84](http://gitlab.protovate.com/Edge/CoffeeNinjaCommon/merge_requests/84)
