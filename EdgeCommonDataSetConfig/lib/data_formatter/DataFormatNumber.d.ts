import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatNumber extends DataFormatterType {
    name: string;
    input_width: number;
    align: string;
    elEditor: any;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    attachEditor(elSourceInput: any, path: any): any;
}
export default DataFormatNumber;
