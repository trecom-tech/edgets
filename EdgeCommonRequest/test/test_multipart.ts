import { EdgeRequest } from '../src/EdgeRequest';
import { MultipartParser } from '../src/MultipartParser';

const fs = require('fs');

// bodyBuffer = fs.readFileSync "multipart1.dat"
// strBoundry = 'FLEXzCKjgBFLvsdvAtTbCDspWS9QUMTNZMsorTrAAlvrDoHy6RN8z4'

const bodyBuffer = fs.readFileSync("multipart2.dat");
const strBoundry = "DYNACONN-BOUNDARY";

const m = new MultipartParser(strBoundry, bodyBuffer);
m.doParse();

console.log("ITEMS=", m.items);