import * as should    from 'should';
import LGBotLib       from '../src/LGBotLib';
import EdgeApi        from 'edgeapi';
import config         from 'edgecommonconfig';

const REDIS_HOST      = process.env.REDIS_HOST || 'redis://127.0.0.1:6379';
config.setCredentials('redisReadHost', `${REDIS_HOST}`);
config.setCredentials('redisHostWQ', `${REDIS_HOST}`);
config.setCredentials('redisHost', `${REDIS_HOST}`);

describe('LGBotLib', () => {

    const bot = new LGBotLib();

    describe('Display the items pending in work queues', () => {

        before(async () => {
            const api: any = await EdgeApi.doGetApi();
            await api.workqueue_doAddRecord('test_workqueue', 'test_workqueue');
            return true;
        });

        it('lg workqueues', async () => {
            const expected = { 
                type: 'object', 
                error: false, 
                data: [{ workqueue: 'test_workqueue', pending: 1 }] };
            const search = 'workqueues';
            const argv = { _: [ 'workqueues' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg wq', async () => {
            const expected = { 
                type: 'object', 
                error: false, 
                data: [{ workqueue: 'test_workqueue', pending: 1 }] };
            const search = 'wq';
            const argv = { _: [ 'wq' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg wqeta', async () => {
            const expected = { 
                type: 'object', 
                error: false, 
                data: [{ workqueue: 'test_workqueue', pending: 1, eta: 'Increasing' }] };
            const search = 'wqeta';
            const argv = { _: [ 'wqeta' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

    });

    describe('Call any api', () => {

        it('lg call <provider> <api> <params...>', async () => {
            const expected = { type: 'object', error: false, data: true };
            const search = 'call workqueue addrecorddelayed v2-import 123451';
            const argv = { _: [ 'call', 'workqueue', 'addrecorddelayed', 'v2-import', 123451 ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

    });

    describe('Listing files in Azure', () => {

        it('lg logs ls', async () => {
            const expected = { 
                type: 'object',
                error: false,
                data: [{ name: 'test', type: 'd' }],
                suggest: 'table',
                path: '' };
            const search = 'logs ls';
            const argv = { _: [ 'logs', 'ls' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg logs ls <path>', async () => {
            const expected = { 
                type: 'object',
                error: false,
                data: [{ name: 'job_output', type: 'd' }],
                suggest: 'table',
                path: 'test' };
            const search = 'logs ls test';
            const argv = { _: [ 'logs', 'ls', 'test' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg logs ls <path>/<Regexp>', async () => {
            const expected = { 
                type: 'object',
                error: false,
                data: [{ name: 'job_output_1_Geo.json', type: 'f', size: 0 },
                       { name: 'job_output_2_Geo.json', type: 'f', size: 0 }],
                suggest: 'table',
                path: 'test/job_output' };
            const search = 'logs ls test/job_output/Geo';
            const argv = { _: [ 'logs', 'ls', 'test/job_output/Geo' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg largvogs get <path>/<Regexp>', async () => {
            const expected = { 
                type: 'object',
                error: false,
                data: [{ name: 'job_output_1_Geo.json', type: 'f', size: 0 },
                       { name: 'job_output_2_Geo.json', type: 'f', size: 0 }],
                suggest: 'table',
                path: 'test/job_output/Geo' };
            const search = 'logs get test/job_output/Geo';
            const argv = { _: [ 'logs', 'get', 'test/job_output/Geo' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

    });

    describe('Operations(Get, Load, Export) to the Database', () => {

        const expected = { 
            type: 'array',
            error: false,
            data: [ { 
                id: '36_Listing_5049080',
                name: 'brian',
                age: '43',
                is_verified: false 
            } ] 
        };

        before(() => {
            process.chdir('test/test_data');
            
            return true;
        });

        it('lg load', async () => {
            const search = 'load';
            const argv = { _: [ 'load' ] };
            const actual: any = await bot.doProcess(search, argv);
            should.equal(actual.type, expected.type);
            should.equal(actual.error, expected.error);
            should.equal(actual.data[0].id, expected.data[0].id);
            should.equal(actual.data[0].name, expected.data[0].name);
            should.equal(actual.data[0].active, true);
        });

        it('lg load os', async () => {
            const search = 'load os';
            const argv = { _: [ 'load', 'os' ] };
            const actual: any = await bot.doProcess(search, argv);
            should.equal(actual.type, expected.type);
            should.equal(actual.error, expected.error);
            should.equal(actual.data[0].id, expected.data[0].id);
            should.equal(actual.data[0].name, expected.data[0].name);
            should.equal(actual.data[0].active, true);
        });

        it('lg load os/jobs', async () => {
            const search = 'load os/jobs';
            const argv = { _: [ 'load', 'os/jobs' ] };
            const actual: any = await bot.doProcess(search, argv);
            should.equal(actual.type, expected.type);
            should.equal(actual.error, expected.error);
            should.equal(actual.data[0].id, expected.data[0].id);
            should.equal(actual.data[0].name, expected.data[0].name);
            should.equal(actual.data[0].active, true);
        });

        it('lg load test.json --dataset os --collection jobs', async () => {
            process.chdir('os/jobs');
            const search = 'load test.json';
            const argv = { 
                _: [ 'load', 'test.json' ],
                dataset: 'os',
                collection: 'jobs' };
            const actual: any = await bot.doProcess(search, argv);
            should.equal(actual.type, expected.type);
            should.equal(actual.error, expected.error);
            should.equal(actual.data[0].id, expected.data[0].id);
            should.equal(actual.data[0].name, expected.data[0].name);
            should.equal(actual.data[0].active, true);
            process.chdir('../../');
        });

        it('lg get <dataSet> <path>', async () => {
            const search = 'get os /jobs/36_Listing_5049080';
            const argv = { _: [ 'get', 'os', '/jobs/36_Listing_5049080' ] };
            const actual: any = await bot.doProcess(search, argv);
            should.equal(actual.type, expected.type);
            should.equal(actual.error, expected.error);
            should.equal(actual.data[0].id, expected.data[0].id);
            should.equal(actual.data[0].name, expected.data[0].name);
            should.equal(actual.data[0].active, true);
        });

        it('lg export os jobs', async () => {
            const expected = { type: 'object', error: false, data: true };
            const search = 'export os jobs';
            const argv = { _: [ 'export', 'os', 'jobs' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg export os jobs --path ~/', async () => {
            const expected = { type: 'object', error: false, data: true };
            const search = 'export os jobs';
            const argv = { _: [ 'export', 'os', 'jobs' ], path: '~/' };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        after(() => {
            process.chdir('../../');
        });

    });

    describe('Get data from REDIS Cache', () => {
        
        it('lg cacheget key', async () => {
            const key = 'test';
            const value = 'test';
            await EdgeApi.cacheSet(key, value);
            
            const expected = { type: 'object', error: false, data: 'test' };
            const search = `cacheget ${key}`;
            const argv = { _: [ 'cacheget', `${key}` ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

    });

    describe('Access other redis commands', () => {
        
        it('lg redis command count', async () => {
            const expected = { type: 'object', error: false, data: 200 };
            const search = 'redis command count';
            const argv = { _: [ 'redis', 'command', 'count' ] };
            const actual = await bot.doProcess(search, argv);
            should.deepEqual(actual, expected);
        });

        it('lg redis info', async () => {
            const search = 'redis info';
            const argv = { _: [ 'redis', 'info' ] };
            const actual: any = await bot.doProcess(search, argv);
            should.exist(actual.data);
        });

    });

});