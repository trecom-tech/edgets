$ ->
    loadZipcodes()
    .then ()->

        DataMap.getDataMap().updatePathValueEvent "/zipcode/02532/area_code", "TESTING"
        DataMap.changeColumnAttribute "zipcode", "area_code", "type", "memo"
        viewTable  = null

        addTest "Sorting, Fixed Header, Group By", ()->
            addHolder().setView "Table", (view)->
                viewTable = view
                table = viewTable.addTable "zipcode"
                viewTable.setAutoFillWidth()
                viewTable.groupBy("county")
                viewTable.toggleGroupVisible "County: BARNSTABLE", false
            true
        
        addTestButton "Toggle a Group", "Open", ()->
            groupObj = 
                County : "BRISTOL"
                City   : "Sandwich"

            viewTable.toggleGroupVisible groupObj
            true

        go()
