## SSL

### How EdgeApiServer runs with SSL configured.

1. When ApiServer starts, it looks for `privkey-{os.hostname()}.pem` and `fullchain-{os.hostname()}.pem` from current project root path and `~/EdgeConfig`. (Check out `getHttpsServer` of `ApiWebServer` to understand how https server is created).
2. Https server is created and listened on port 443 or 8443(testing) from `onInit` of `ApiSocketServer`.
3. Finally, Socket server also listens on corresponding secured port. Web server will run on secured port on https protocol.

### Testing for SSL

The server on test mode will create a self-signed key and pem before it starts [test/helper/setup.ts](test/helper/setup.ts).
Some test cases to verify https server is written on ApiWebServer.spec.ts.


### How to create Self-signed SSL
```
openssl req -nodes -new -x509 -keyout server.key -out server.cert
```
Fill some information to generate csr like country, state etc.

server.cert is the self-signed certificate file.
server.key is the private key of the certificate.

Rename those file according to the pc name `privkey-{os.hostname()}.pem` and `fullchain-{os.hostname()}.pem`.


