##
## View to show Full Calendar
## Reference: https://fullcalendar.io/
class ViewCalendar extends View

    getDependencyList: ()=>
        return [ "/vendor/fullcalendar/fullcalendar.min.js", "/vendor/fullcalendar/fullcalendar.min.css" ]

    onShowScreen: () =>
        GlobalClassTools.addEventManager(this)

        @widgetHolder = @addDiv "ninja-fullcalendar-holder"
        if !@events? then @events = []

        if !@options?
            @options =
                header:
                    left: 'prev,next today'
                    center: 'title'
                    right: 'month,agendaWeek,agendaDay'
                defaultView: 'month'
                editable: true
                droppable: true
                dragRevertDuration: 0
                eventLimit: false
                themeSystem: 'bootstrap3'
                height: 'auto'
                eventClick: (calEvent, jsEvent, view) =>
                    @emitEvent "calendar_event_selected", [calEvent, jsEvent, view]
                    true
                eventRender: (eventObj, element) =>
                    element.on 'mouseover', (e) =>
                        @showTooltip e, eventObj, element
                        true

                    element.on 'mouseout', (e) =>
                        @hideTooltip e, eventObj, element
                        true

            @editable = true

        @widgetHolder.getTag().fullCalendar @options
        true

    addEvent: (eventObj) =>
        formatStr = "YYYY-MM-DD"
        now = moment().format(formatStr)
        event = 
            title: "event#{@events.length}"
            start: now
            end:   now

        ##
        ## tidy input event object up
        if eventObj.start and eventObj.start.getTime?
            eventObj.start = moment(eventObj.start).format(formatStr)

        if eventObj.end and eventObj.end.getTime?
            eventObj.end = moment(eventObj.end).format(formatStr)

        # console.log("EventObj: ", eventObj)

        $.extend event, eventObj
        @events.push event
        @widgetHolder.getTag().fullCalendar 'addEventSource', [ event ]
        event

    resetEvents: () =>
        @events = []
        @widgetHolder.getTag().fullCalendar 'removeEvents'
        true

    setOptions: (options) =>
        if !options? or typeof options isnt "object" then return false
        $.extend @options, options
        true

    setCalendarView: (viewName, dateOrRange) =>
        if !viewName? then return false
        @setOptions { defaultView: viewName }
        @widgetHolder.getTag().fullCalendar 'changeView', viewName, dateOrRange
        true

    getCalendarView: () =>
        cView = @widgetHolder.getTag().fullCalendar 'getView'
        cView

    setEditable: (editable = true) =>
        if Boolean(@editable) is Boolean(editable) then return false
        @editable = Boolean(editable)
        @setOptions { editable: @editable }
        @widgetHolder.getTag().fullCalendar 'option', { editable: @editable }
        true

    getEditable: () =>
        @editable

    renderDescription: (data) ->
        if !data.description? then return data.title
        data = data.description

        if typeof data is "object"
            if Array.isArray(data)
                return data.join(', ')
            else 
                result = "<table>"
                for key, val of data
                    result += "<tr><td>#{key}:</td> <td>#{val}</td></tr>"
                result += "</table>"
                return result
        data

    showTooltip: (ev, eventObj, element) =>
        if !@floatingWindow? then @floatingWindow = new FloatingWindow(0, 0, 100, 100)
        w = @floatingWindow.getBodyWidget()
        w.resetClasses "calendarTooltipBody"
        @floatingWindow.hide()
        @tooltipShowing = false

        coords = GlobalValueManager.GetCoordsFromEvent ev
        @floatingWindow.floatingWin.setClass "calendarTooltip", true
        @floatingWindow.html @renderDescription(eventObj)
        @floatingWindow.moveTo coords.x - (@floatingWindow.getWidth() / 2), coords.y + 20
        @floatingWindow.setSize 300, 200
        @floatingWindow.show()
        @tooltipShowing = true

    hideTooltip: (ev, evenetObj, element) =>
        if @tooltipShowing? and @tooltipShowing is true
            @tooltipShowing = false
            @floatingWindow.hide()

    serialize: () =>
        return 
            options: @options
            events:  @events

    deserialize: (options)=>

        new Promise (resolve, reject)=>
            if options.defaultview?
                @setCalendarView options.defaultview

            if options.editable?
                @setEditable options.editable

            if options.options?
                @setOptions options.options

            if options.events?
                for event in options.events
                    @addEvent event

            resolve true