$ ->
    item1 = 
        name: "tile1"
        title: "First title"
        icon: "<i class='far fa-car' />"
        description: "10"
        subtxt: "Sub Title"
        theme: "tilecolor0"

    item2 = 
        name: "tile2"
        title: "Second title"
        icon: "<i class='far fa-camera' />"
        description: "22"
        subtxt: "Sub title 2"

    item3 = 
        name: "tile3"
        title: "Third title"
        icon: "<i class='far fa-pencil' />"
        description: "399"
        subtxt: "Sub title 3"

    addTestButton "Tiles Basic", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description
            view.setTitle "Title of View"
        true

    addTestButton "Tiles Set Width", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description
            view.setTileWidth  200
        true        

    addTestButton "Tiles Set Width By Array", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description
            view.setTileWidth  [ 200, 300, 400 ]
        true

    addTestButton "Tiles Set Width By Percent", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            # view.addTile item3.name, item3.title, item3.description
            view.setTileWidth  [ 0.5, 0.5 ]
            view.setTileHeight 1
        true        

    addTestButton "Tiles Set Height by Percent", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description
            view.setTileWidth  [ 200, 300, 400 ]
            view.setTileHeight 1 ## Full height
        true

    addTestButton "Tiles Set Title and Description", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title
            view.addTile item3.name, item3.title
            view.setTileWidth  [ 200, 300, 400 ]
            view.setTileDescription item2.name, "New Description"
            view.setTileTitle item3.name, "New Title"
            view.setTileSubTitle item3.name, "<i class='far fa-book'>New SubTitle</i>", (e) ->
                console.log "Item3 sub title is clicked! ", e
            # setTimeout () =>
            #     view.setTileSubTitle item3.name, null
            # , 3000
        true

    addTestButton "Tiles Custom Styles", "Open", () ->
        $("body").append '''
            <style type="text/css">
                .custom-tile-title {
                    color: red;
                }
            </style>
        '''
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title
            view.addTile item3.name, item3.title
            view.setTileWidth  [ 200, 300, 400 ]
            view.setTileDescription item2.name, "New Description"
            view.setTileTitle item3.name, "New Title"
            view.setTitleClass item3.name, "custom-tile-title"
        true

    addTestButton "Tiles RemoveAll", "Open", () ->
        addHolder().setView "Tiles", (view) ->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description
            setTimeout () ->
                view.removeAllTiles()
            , 3000
        true

    addTestButton "Tiles on Popup", "Open", () ->
        doPopupView "Tiles", "Popup with Tiles", null, 800, 600, (view)->
            view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
            view.addTile item2.name, item2.title, item2.description
            view.addTile item3.name, item3.title, item3.description  
            view.setTileWidth [ 0.2, 0.3, 0.5 ]
        true

    addTestButton "Tiles in tab", "Open", ()->
        addHolder()
        .setView "DynamicTabs", (viewTabs)->
            viewTabs.doAddViewTab "Tiles", "tilestab", (view)->
                view.addTile item1.name, item1.title, item1.description, item1.subtxt, item1.theme
                view.addTile item2.name, item2.title, item2.description
                view.addTile item3.name, item3.title, item3.description                
            viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
        true

    addTestButton "Tiles Deserialize", "Open", () ->
        addHolder()
        .setView "Tiles", (view) ->
            view.deserialize 
                tiles: [
                    item1, item2, item3
                ]
                tilewidth: [
                    200, 300, 400
                ]
                title: "This is Title"

            setTimeout () ->
                options = view.serialize()
                console.log "Tiles serialized data: ", options
                doPopupView "Tiles", "Tiles Serialized", null, 1200, 800, (viewPopup) ->
                    viewPopup.deserialize options
            , 3000
        true

    addTestButton "Simple Tiles Set Width", "Open", () ->
        addHolder().setView "TileSimple", (view) ->
            view.addTile 0, item1.name, item1.title, item1.icon, item1.description, item1.subtxt, item1.theme
            view.addTile 0, item2.name, item2.title, item2.icon, item2.description, item2.subtxt
            view.addTile 1, item3.name, item3.title, item3.icon, item3.description, item3.subtxt
            # view.addTileInNewRow item3.name, item3.title, item3.description
            view.setTileWidth  300
            view.setMargin 20
        true

    go()
