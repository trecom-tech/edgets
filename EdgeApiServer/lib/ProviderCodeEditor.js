"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug = require('slug');
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgeapi_1 = require("edgeapi");
const ApiProvider_1 = require("./ApiProvider");
const EdgeError_1 = require("./EdgeError");
const DynamicCodeModel_1 = require("./DynamicCodeModel");
class ProviderCodeEditor extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
        this.dataStore = new edgedatasetmanager_1.default('lookup');
        return true;
    }
    getBaseName() {
        return 'code';
    }
    doRunCode(user, category, name, userObject, params, executeOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            let result;
            let codeobj = yield this.doInternalGetModuleLocal(category, name);
            if (codeobj == null || codeobj.getProvider == null) {
                console.log('Loading result:', codeobj);
                throw new EdgeError_1.default(`Module loading issue category='${category}', name='${name}'`, 'PROVIDER_CODE_EDITOR');
            }
            let provider = codeobj.getProvider();
            if (!codeobj.data.compiles) {
                console.log('Codeobj doesn\'t compile:', codeobj);
                throw new EdgeError_1.default('Module doesn\'t currently compile', 'PROVIDER_CODE_EDITOR');
            }
            if (provider == null) {
                console.log('Codeobj unable to get provider');
                throw new EdgeError_1.default('Unable to get code provider', 'PROVIDER_CODE_EDITOR');
            }
            // Default execute options from DynamicProvider
            let options = provider.initRunOptions();
            if (executeOptions != null) {
                edgeapi_1.default.deepMergeObject(options, executeOptions);
            }
            // Set the two main function parameters
            options.userObject = userObject;
            options.params = params;
            console.log('Running code', provider, ' with ', options);
            edgecommonconfig_1.default.status('doRunCode module=', name, 'executeOptions=', options);
            if (options.trace) {
                options.onConsoleLog = (time_ms, values) => {
                    return user.sendPush('runcodeLog', {
                        type: 'c',
                        time_ms,
                        values,
                    });
                };
            }
            result = yield provider.doCallFunction(options);
            console.log('SENDING RESULT:', result);
            return result;
        });
    }
    // A standard id value for the database based on the name.
    internalGetKey(category, name, branch) {
        return slug(category + '_' + name + '_' + branch).toLowerCase();
    }
    // List the available modules
    // @param condition {string} A datapath search string
    // @return An array of matching code modules
    doListAvailableModules(user, condition = null) {
        // TODO: Check for permissions in the user object before loading code modules
        // Search conditions
        let path = '/code/';
        if (condition != null) {
            path += condition;
        }
        let fieldList = {
            id: 1,
            category: 1,
            name: 1,
            meta: 1,
            compiles: 1,
            version: 1,
            _lastModified: 1,
            compile_host: 1,
            status: 1,
            deployment: 1,
        };
        let sortOrder = { id: -1 };
        let limit = 10000;
        return this.dataStore.doGetItems(path, fieldList, sortOrder, 0, limit);
    }
    // Save the notes (the README tab)
    doSaveModuleNotes(user, category, name, newNotes) {
        return __awaiter(this, void 0, void 0, function* () {
            let codeobj = yield this.doInternalGetModuleLocal(category, name);
            if (codeobj == null || codeobj.setCoffeescriptCode == null) {
                throw new EdgeError_1.default(`Error saving module category='${category}', name='${name}'`, 'PROVIDER_CODE_EDITOR');
            }
            codeobj.setNotes(newNotes);
            codeobj.save();
            return true;
        });
    }
    // Save the development version of a module to the database
    doSaveModuleLocal(user, category, name, code) {
        return __awaiter(this, void 0, void 0, function* () {
            let { username } = user;
            if (username == null || username.length < 1) {
                username = 'brian';
            }
            let codeobj = yield this.doInternalGetModuleLocal(category, name);
            if (codeobj == null || codeobj.setCoffeescriptCode == null) {
                throw new EdgeError_1.default(`Error saving module category='${category}', name='${name}'`, 'PROVIDER_CODE_EDITOR');
            }
            codeobj.setCoffeescriptCode(code);
            codeobj.setBranch('dev');
            codeobj.save();
            return codeobj.data;
        });
    }
    doTestCaseAdd(category, name, options, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let module = yield this.doInternalGetModuleLocal(category, name);
            if (id != null) {
                module.updateTestCase(id, options);
            }
            else {
                module.addTestCase(options);
            }
            return module.data;
        });
    }
    doTestCaseRemove(category, name, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let module = yield this.doInternalGetModuleLocal(category, name);
            module.removeTestCase({});
            return module.data;
        });
    }
    // Load a module and return it's data
    doGetModuleLocal(user, category, name) {
        return __awaiter(this, void 0, void 0, function* () {
            let module = yield this.doInternalGetModuleLocal(category, name);
            if (module != null) {
                return module.data;
            }
            return null;
        });
    }
    doInternalGetModuleLocal(category, name) {
        return __awaiter(this, void 0, void 0, function* () {
            // load local code
            let key = this.internalGetKey(category, name, 'dev');
            if (this.modlist == null) {
                this.modlist = {};
            }
            if (this.modlist[key] == null) {
                this.modlist[key] = new DynamicCodeModel_1.default(key);
                yield this.modlist[key].doVerifyLoaded();
            }
            this.modlist[key].setCategory(category);
            this.modlist[key].setName(name);
            this.modlist[key].setBranch('dev');
            this.modlist[key].getProvider();
            return this.modlist[key];
        });
    }
}
exports.default = ProviderCodeEditor;
;
//# sourceMappingURL=ProviderCodeEditor.js.map