$ ->
	GroupOptions1 =
		title 		: "Group 1"
		icon		: "<i class='fa fa-camera' />"
		isEnabled	: true
		isOpen		: false
		hasFilter   : true

	GroupOptions2 =
		title 		: "Group 2"
		isEnabled	: true
		isOpen		: false


	ItemOptions1 =
		title 		: "Item 1"
		icon 		: "fa fa-book"
		isEnabled 	: true
		isHidden 	: false
		isSelected 	: false
		tooltip 	: "Item Tooltip"

	ItemOptions2 =
		title 		: "Item 2"
		icon 		: "fa fa-pencil"
		isEnabled 	: true
		isHidden 	: false
		isSelected 	: false
		tooltip 	: "Item Tooltip 2"

	ItemOptionsToTop =
		title 		: "Item At the Top"
		isEnabled 	: true
		isHidden 	: false
		isSelected 	: false
		location	: 'top'
		tooltip		: () ->
			return "<div><i class='fa fa-camera'></i>Camera Tooltip</div>"

	addTestButton "View Nav Menu Basic", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu"
			group1 = view.addGroup GroupOptions1
			item1 = group1.addItem ItemOptions1
			item1.setClickFunction (e) => console.log "Item 1 is clicked!"
			item2 = group1.addItem ItemOptions1
			item2.setEnabled false

	addTestButton "View Nav Menu with no title", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			group1 = view.addGroup GroupOptions1
			item1 = group1.addItem ItemOptions1
			item1.setClickFunction (e) => console.log "Item 1 is clicked!"
			item2 = group1.addItem ItemOptions1
			item2.setEnabled false
			view.show()

	addTestButton "View Nav Menu with no items", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			group1 = view.addGroup
				title: "Group with no items"
				hasFilter   : true
			group2 = view.addGroup GroupOptions2
			group2.setTitle "Group: MaxItems = 3"
			group2.setIcon "<i class='far fa-car' />"
			group2.setMaxItems 3
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			item21.setIcon "fa-adjust"
			item22 = group2.addItem ItemOptions1
			item22.setTitle "Item 2-2"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-3"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-4"


	addTestButton "View Nav Menu 2 Groups", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu 2 Groups"
			group1 = view.addGroup GroupOptions1
			group1.setTitle "Group with item at the top"
			item11 = group1.addItem ItemOptions1
			item11.setClickFunction (e) => console.log "Item 1 is clicked!"
			item12 = group1.addItem ItemOptions1
			item12.setEnabled false
			item13 = group1.addItem ItemOptionsToTop

			group2 = view.addGroup GroupOptions2
			group2.setTitle "Group: MaxItems = 3"
			group2.setIcon "fa-cab"
			group2.setMaxItems 3
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			item21.setIcon "fa-adjust"
			item22 = group2.addItem ItemOptions1
			item22.setTitle "Item 2-2"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-3"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-4"

			group2.setOpen true

	addTestButton "No group icons", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu 2 Groups"
			group1 = view.addGroup
				title: "Group 1 no icon"
			item11 = group1.addItem ItemOptions1
			item12 = group1.addItem ItemOptions1
			item13 = group1.addItem ItemOptionsToTop
			item14 = group1.addItem ItemOptions1

			group2 = view.addGroup
				title: "Group 2 no icon"
			item22 = group2.addItem ItemOptions1
			item23 = group2.addItem ItemOptions1
			item23 = group2.addItem ItemOptions1

			group2.setOpen true

	addTestButton "View NavMenu Serialize", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu Serialize"
			group1 = view.addGroup GroupOptions1
			group1.setTitle "Group with Hidden Item"
			item11 = group1.addItem ItemOptions1
			item11.setClickFunction (e) => console.log "Item 1 is clicked!"
			item12 = group1.addItem ItemOptions1
			item12.setEnabled false
			item13 = group1.addItem ItemOptionsToTop
			item14 = group1.addItem ItemOptions1
			item14.setHidden true

			group2 = view.addGroup GroupOptions2
			group2.setTitle "Group: MaxItems = 3"
			group2.setMaxItems 3
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			item21.setIcon "fa-adjust"
			item22 = group2.addItem ItemOptions1
			item22.setTitle "Item 2-2"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-3"
			item23 = group2.addItem ItemOptions1
			item23.setTitle "Item 2-4"

			obj = group2.serialize()

			group3 = view.addGroup {}
			group3.deserialize obj

	addTestButton "View Nav Menu Narrowed Mode", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu"
			view.setSize 99, 300
			group1 = view.addGroup GroupOptions1
			item11 = group1.addItem ItemOptions1
			item11.setTitle "Item 1-1"
			item11.setClickFunction (e) => console.log "Item 11 is clicked!"
			item12 = group1.addItem ItemOptions1
			item12.setEnabled false
			item12.setTitle "Item 1-2"
			group2 = view.addGroup GroupOptions2
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			item21.setClickFunction (e) => console.log "Item 21 is clicked!"
			item22 = group2.addItem ItemOptions1
			item22.setTitle "Item 2-2"
			item22.setEnabled false	

	addTestButton "View Nav Menu Checkable Items", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu"
			group1 = view.addGroup GroupOptions1
			item11 = group1.addItem ItemOptions1
			item11.setTitle "Item 1-1"
			item11.setCheckable true
			item11.setClickFunction (e) => console.log "Item 11 is clicked!"
			item12 = group1.addItem ItemOptions1
			item12.setEnabled false
			item12.setTitle "Item 1-2"
			group2 = view.addGroup GroupOptions2
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			item21.setClickFunction (e) => console.log "Item 21 is clicked!"
			item22 = group2.addItem ItemOptions1
			item22.setTitle "Item 2-2"
			item22.setEnabled false

	addTestButton "View Nav Menu Reset Groups", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			group1 = view.addGroup GroupOptions1
			item1 = group1.addItem ItemOptions1
			item1.setClickFunction (e) => console.log "Item 1 is clicked!"
			item2 = group1.addItem ItemOptions1
			item2.setEnabled false
			group2 = view.addGroup GroupOptions2
			item21 = group2.addItem ItemOptions1
			item21.setTitle "Item 2-1"
			setTimeout () ->
				view.resetGroups()
				group1 = view.addGroup GroupOptions1
				item1 = group1.addItem ItemOptions1
			, 3000

	addTestButton "Group Remove All Items", "Open", (e)->
		addHolder().setView "NavMenu", (view)->
			view.setTitle "Test Navmenu 2 Groups"
			group1 = view.addGroup
				title: "Group 1 no icon"
			item11 = group1.addItem ItemOptions1
			item12 = group1.addItem ItemOptions1
			item13 = group1.addItem ItemOptionsToTop
			item14 = group1.addItem ItemOptions1
			group1.setOpen true

			group2 = view.addGroup
				title: "Group 2 no icon"
			item22 = group2.addItem ItemOptions1
			item23 = group2.addItem ItemOptions1
			item23 = group2.addItem ItemOptions1

			group2.setOpen true			

			setTimeout () ->
				group1.removeAllItems()
			, 3000

			setTimeout () ->
				group2.removeAllItems()
			, 5000
		true

	addTestButton "Navmenu on Popup", "Open", () ->
		doPopupView "NavMenu", "NavMenu Serialized", null, 1000, 800, (view) ->
			view.deserialize
				groups: [
					options: GroupOptions1
					items: [
						options: ItemOptions1
					,
						options: ItemOptions2
					,
						options: ItemOptionsToTop
					,
						options: ItemOptions1
					]
				,
					options: GroupOptions2
					items: [
						options: ItemOptions1
					,
						options: ItemOptions2
					]
				]

	addTestButton "NavMenu Serialized", "Open", (e) ->
		addHolder()
		.setView "NavMenu", (view) ->
			view.deserialize
				groups: [
					options: GroupOptions1
					items: [
						options: ItemOptions1
					,
						options: ItemOptions2
					,
						options: ItemOptionsToTop
					,
						options: ItemOptions1
					]
				,
					options: GroupOptions2
					items: [
						options: ItemOptions1
					,
						options: ItemOptions2
					]
				]

			setTimeout () ->
				options = view.serialize()
				console.log "Serialized Data: ", options
				doPopupView "NavMenu", "NavMenu Serialized", null, 1000, 800, (viewPopup) ->
					viewPopup.deserialize options
			, 3000

	go()
