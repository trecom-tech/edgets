import { config }   from 'edgecommonconfig';
import MongoGateway from './MongoGateway';
import {
    Collection,
    FilterQuery,
    Cursor,
    AggregationCursor,
}                   from 'mongodb';
import { Func }     from 'mocha';

export class MongoModel {
    theCollection: Collection;
    internalCollectionPromise: Promise<Collection>;

    /**
     *
     * @param name [string] collectionName
     * @param gateway MongoGateway object
     */
    constructor(public name: string, public gateway: MongoGateway) {
        //
        // New model is created
        // name = the name of the collection
        // gateway = a reference to the MongoGateway object that issued the collection
        //
        // The collection is not yet connected until requested with doInternalGetCollection


        this.theCollection             = null;
        this.internalCollectionPromise = null;

        // tslint:disable-next-line:no-floating-promises
        this.doInternalGetCollection();
    }

    /**
     * Connects to the database and gets a reference to the table
     * which is a collection in mongo terms
     *
     */
    doInternalGetCollection() {

        if (!this.internalCollectionPromise) {
            this.internalCollectionPromise = new Promise(async (resolve, reject) => {
                try {
                    await this.gateway.doConnectToDatabase();
                    this.theCollection = this.gateway.db.collection(this.name);
                    resolve(this.theCollection);
                } catch (err) {
                    reject(err);
                }
            });
        }

        return this.internalCollectionPromise;
    }

    async doAggregate(aggList: any): Promise<any[]> {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        return this.theCollection.aggregate(aggList).toArray();
    }

    /**
     * Given a specific field, return the unique values and counts.
     * @param fieldName
     * @param matchCondition
     */
    async doGetUniqueValues(fieldName: any, matchCondition?: any) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        const aggList = [];

        if (matchCondition != null) {
            aggList.push({
                '$match': matchCondition,
            });
        }

        aggList.push({
            '$group': {
                '_id'  : `$${fieldName}`,
                'count': {
                    '$sum': 1,
                },
            },
        });

        console.log('Using: ', aggList);

        return this.theCollection.aggregate(aggList).toArray();
    }

    async doUpdateOne(searchCondition: any, setCondition: any, isMulti: boolean = false) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        try {
            const result = await this.theCollection.updateOne(searchCondition, setCondition);
            return result;
        } catch (e) {

            let log = config.getLogger(`doUpdateOne-${this.name}`);
            log.error('doUpdateOne Exception', {
                searchCondition,
                setCondition,
                isMulti,
                exception: e,
            });

            throw e;
        }
    }

    async doInsertMany(newDocuments: any) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        //
        // Fast multiple document insert
        return this.theCollection.insertMany(newDocuments);
    }

    async doInsertOne(newDocument: any) {

        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        try {
            const result = await this.theCollection.insertOne(newDocument);
            return result;
        } catch (e) {

            console.log('doInsertOne Exception:', e);
            console.log('newDocument:', newDocument);
            return null;
        }
    }

    async doRemove(condition: any) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }
        // tslint:disable-next-line:deprecation
        const result = await this.theCollection.remove(condition);
        return result;
    }

    /**
     * Insert or update a document with a given ID
     * @param value
     * @param newDataObj
     */
    async doUpsertOne(value: any, newDataObj: any) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        let newData: any = {};
        newDataObj.id    = value;
        newData['$set']  = newDataObj;

        let options = {
            'new'   : true,
            'upsert': true,
        };

        let condition = { id: value };

        try {
            const r = await (this.theCollection as any).findAndModify(condition, {}, newData, options);
        } catch (e) {
            console.log('DatabaseError doUpsertOne', newDataObj);
            console.info(`DataSet   :${this.name}`);
            console.info(`Document  :${JSON.stringify(value)}`);
            console.info(`Exception :${e}`);
            return false;
        }

        return true;
    }

    async doAppend(condition: any, subPath: any, newData: any) {

        try {

            config.status('doAppend condition=', condition);

            if (!this.theCollection) {
                await this.doInternalGetCollection();
            }

            const setData: any = {};
            setData['$set']    = { '_lastModified': new Date() };
            setData['$push']   = {};

            if (typeof newData === 'object' && Array.isArray(newData)) {
                setData['$push'][subPath.join('.')] = { '$each': newData };
            } else {
                setData['$push'][subPath.join('.')] = newData;
            }

            const options = { multi: true };

            try {

                // tslint:disable-next-line:deprecation
                const r = await this.theCollection.update(condition, setData, options);
                return r;
                // r = await @theCollection.findAndModify condition, { }, newData , options
            } catch (e) {
                console.log('DatabaseError doAppend', condition, subPath);
                let log = config.getLogger(`doAppend-${this.name}`);
                log.error('Append Error', {
                    subPath,
                    condition,
                    newData,
                    options,
                    setData,
                    exception: e,
                });

                return false;
            }
        } catch (err) {
            throw err;
        }
    }

    async doFindAndInsert(condition: any, newData: any, attemptCount: number = 0) {

        try {

            let value;
            config.status('doFindAndInsert condition=', condition, 'newData=', newData);

            if (!this.theCollection) {
                await this.doInternalGetCollection();
            }

            let modifyOptions: any                         = {};
            modifyOptions['$setOnInsert']                  = {};
            modifyOptions['$setOnInsert']['_lastModified'] = new Date();

            let topLevelKeys: any = {};
            topLevelKeys['_id']   = 1;
            for (const varName in newData) {
                value                 = newData[varName];
                topLevelKeys[varName] = 1;
            }

            for (let varName in condition) {
                value                 = condition[varName];
                topLevelKeys[varName] = 1;
                newData[varName]      = value;
            }

            modifyOptions['$setOnInsert'] = newData;

            const options = {
                'new'   : true,
                'upsert': true,
                'fields': topLevelKeys,
            };

            if (config.traceEnabled) {
                config.status('MongoModel doFindAndInsert Starting');
            }
            // config.dump "doFindAndInsert",
            //     condition     : condition
            //     newData       : newData
            //     modifyOptions : modifyOptions
            //     options       : options

            // TODO mainly because Collection does not have findAndModify
            let r: any = await (this.theCollection as any).findAndModify(condition, {}, modifyOptions, options);

            let result = {
                isNew: r.lastErrorObject.updatedExisting !== true,
                count: r.lastErrorObject.n,
                value: r.value,
            };

            return result;
        } catch (e) {

            if (attemptCount === 0) {
                config.status('Attempting to reFindAndModify');
                let val: any = await this.doFindAndInsert(condition, newData, attemptCount + 1);
                return val;
            }

            let log = config.getLogger(`doFindAndInsert-${this.name}`);
            log.error('doFindAndInsert Exception', {
                condition,
                newData,
                exception: e,
            });

            console.log('DatabaseError doFindAndInsert', newData);
            console.log('DatabaseException: ', e.toString());
            return null;
        }
    }

    async doInsertOrUpdate(condition: any, newData: any) {
        try {

            if (!this.theCollection) {
                await this.doInternalGetCollection();
            }

            const setData = { '$set': newData };
            // tslint:disable-next-line:deprecation
            const result  = await this.theCollection.update(condition, setData, { upsert: true });
            return result;
        } catch (e) {

            const log = config.getLogger('doInsertOrUpdate');
            log.error('doInsertOrUpdate Exception', {
                condition,
                newData,
                exception: e,
            });

            console.log('DatabaseError doInsertOrUpdate', newData);
            console.log('DatabaseException: ', e.toString());
            return null;
        }
    }

    //
    // search for one or more items
    //
    // @param value [mixed] Value must be the object or value for "id"
    // @param createIfNeeded [bool] Create the record if it doesn't already exist
    // @param optionalPath [Array] Optional list of deeper paths within the object to return
    // @param returnFullDetail [bool] Normally just the object value is returned, otherwise the value and status
    //
    async doFindByID(value: any, createIfNeeded: boolean = false, optionalPath: string[] = [], returnFullDetail: boolean = false) {
        try {

            let r;
            let result;
            if (!this.theCollection) {
                await this.doInternalGetCollection();
            }

            const condition = { id: value };

            if (createIfNeeded) {

                //
                // Find or Create record
                const options = {
                    'new'   : true,
                    'upsert': true,
                };

                if (optionalPath) {
                    // TODO set function weird from original coffee
                    // ({ 'fields': optionalPath.join('.') });
                }

                r = await (this.theCollection as any).findAndModify(condition, {}, { '$setOnInsert': condition }, options);
                if (returnFullDetail) {
                    result = {
                        isNew: r.lastErrorObject.updatedExisting !== true,
                        count: r.lastErrorObject.n,
                        value: r.value,
                    };
                    return result;
                } else if (r != null && r.value != null && r.value._id != null) {
                    return r.value;
                } else {
                    console.log('Internal error, should not be here with a null value:', r);
                    return null;
                }
            } else {

                // Only find
                r = await this.theCollection.findOne(condition);

                if (returnFullDetail != null && returnFullDetail === true) {
                    result = {
                        isNew: false,
                        count: 1,
                        value: r,
                    };
                    return result;
                }
            }

            return r;
        } catch (e) {

            console.log('DatabaseError doFindByID', value);
            console.info(`Exception :${e}`);
            return false;
        }
    }

    /**
     * Count the results of a search before searching
     * @param condition
     */
    async doCount(condition: any) {
        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        config.status(`MongoModel ${this.name} doCount condition=`, condition);
        const result = await this.theCollection.find(condition).count();
        config.status(`MongoModel ${this.name} doCount result=`, result);
        return result;
    }

    async doFindNearbyIds(conditions: any, location: any, miles: any, _offset: number, limit: number, fieldList?: any) {

        let coords;
        let cord = null;
        if (location != null && location.coordinates != null) {
            coords = location.coordinates;
        } else if (location != null && location[0] != null) {
            coords = location;
        } else if (location != null && location.lat != null) {
            coords = [location.lon, location.lat];
        }

        // console.log "doFindNearbyConditions:", conditions

        const aggList         = [];
        const geoOptions: any = {
            '$geoNear': {
                'near'         : {
                    'type'       : 'Point',
                    'coordinates': coords,
                },
                'distanceField': 'distance',
                'maxDistance'  : 1609.34 * miles,
                'spherical'    : true,
            },
        };
        // "query" : { "loc.type" : "Point" }

        if (limit != null && typeof limit === 'number') {
            geoOptions['$geoNear']['limit'] = limit;
        }

        aggList.push(geoOptions);

        if (Object.keys(conditions).length > 0) {
            aggList.push({
                '$match': conditions,
            });
        }

        const requestFields: any = { id: 1, distance: 1 };
        if (fieldList != null) {
            let varName;
            if (Array.isArray(fieldList)) {
                for (varName of Array.from(fieldList)) {
                    requestFields[varName] = 1;
                }
            } else {
                for (varName in fieldList) {
                    let value              = fieldList[varName];
                    requestFields[varName] = value;
                }
            }
        }

        aggList.push({ '$project': requestFields });
        aggList.push({ '$sort': { 'distance': 1 } });

        // if limit?
        //     aggList.push { '$limit' : limit }

        if (config.traceEnabled) {
            config.status('doFindNearbyIds Starting search');
            config.dump('doFindNearbyIds doAggregate:', aggList);
        }

        let result = await this.doAggregate(aggList);

        if (config.traceEnabled) {
            config.status(`doFindNearbyIds found ${result.length}`);
        }

        return result;
    }

    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param conditions [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles [number] the number of miles to search around
     * @param offset [number] Optional starting point in the result list (default 0)
     * @param limit [number] Optional the maximum number of results (default 1000)
     */
    async doFindNearby(conditions: any, location: any, miles: any, offset: number, limit: number) {

        let cord = null;
        if (location == null) {
            config.status('doFindNearby missing location for search');
            return null;
        }

        const condition: any = {
            loc: {
                '$nearSphere' : location,
                '$maxDistance': 1609.34 * miles,
            },
        };

        //
        // Apply other conditions to the search if needed
        if (conditions != null && typeof conditions === 'object') {
            for (const keyName in conditions) {
                const keyValue     = conditions[keyName];
                condition[keyName] = keyValue;
            }
        }

        if (!this.theCollection) {
            await this.doInternalGetCollection();
        }

        let r = this.theCollection.find(condition);

        if (limit != null) {
            r = r.limit(limit);
        }

        if (offset != null) {
            r = r.skip(offset);
        }

        // r.sort { distance: 1 }
        try {
            const all = await r.toArray();
            return all;
        } catch (ex) {
            let log = config.getLogger(`doFindNearby-${this.name}`);
            log.error('doFindNearby', {
                condition,
                location,
                miles,
                offset,
                limit,
                ex,
            });
            console.log('Exception during doFindNearby: ', ex);
            console.log('Search condition:', condition);
            return {};
        }
    }

    /**
     * Do find on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [object] condition to apply find
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit 
     */
    async doFind(condition: any, fieldList: any = null, sortCondition: any = null, offset: number = 0, limit: number = 100) {

        config.status(`doFind ${this.gateway.dbName}/${this.name} `, {
            'condition:': condition,
            'fieldList=': fieldList,
            'offset='   : offset,
            'limit='    : limit,
        });

        const r = await this.doGetCursor(condition, fieldList, sortCondition, offset, limit);
        if (r == null) {
            return [];
        }

        return r.toArray();
    }

    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit 
     */
    async doGetCursor(condition: FilterQuery<any>, fieldList: any, sortCondition: any, offset: number, limit: number): Promise<Cursor<any>> {

        try {

            let r;
            if (!this.theCollection) {
                await this.doInternalGetCollection();
            }

            if (fieldList != null) {
                // tslint:disable-next-line:deprecation
                r = this.theCollection.find(condition, fieldList);
            } else {
                // tslint:disable-next-line:deprecation
                r = this.theCollection.find(condition);
            }

            if (limit != null) {
                if (typeof limit !== 'number') {
                    console.log('Warning: invalid limit:', limit + '');
                }

                r = r.limit(limit);
            }

            if (offset != null) {
                r = r.skip(offset);
            }

            if (sortCondition != null) {
                r = r.sort(sortCondition);
            }

            return r;
        } catch (e) {

            let log = config.getLogger(`doGetCursor-${this.name}`);
            log.error('doGetCursor Exception', {
                condition,
                fieldList,
                sortCondition,
                offset,
                limit,
                exception: e,
            });

            console.log('doGetCursor Exception', {
                condition,
                fieldList,
                sortCondition,
                offset,
                limit,
                exception: e,
            });

            return null;
        }
    }

    //
    // Enable iteration through an entire collection
    //
    async doStream(condition: any, fieldList: any, dateFieldName: any, _lastModified: any, limit: number, callback: Function) {

        if (fieldList == null) {
            fieldList = null;
        }
        if (_lastModified == null) {
            _lastModified = new Date('2016-01-01');
        }
        if (typeof _lastModified === 'string') {
            _lastModified = new Date(_lastModified);
        }
        if (typeof _lastModified === 'number') {
            _lastModified = new Date(_lastModified);
        }

        if (_lastModified == null || isNaN(_lastModified.getTime())) {
            _lastModified = new Date('2015-01-01');
        }

        console.log('fieldLIst     : ', fieldList);
        console.log('_lastModified : ', _lastModified);
        console.log('Limit         : ', limit);
        console.log('Callback      : ', callback);
        console.log('dateFieldName : ', dateFieldName);

        if (typeof callback !== 'function') {
            console.log('Callback [', callback, '] is not a function');
            return null;
        }


        if (_lastModified != null) {
            condition[dateFieldName] = { '$gte': new Date(_lastModified) };
        }

        config.status(`MongoModel ${this.name} doStream`, condition);

        let count = await this.doCount(condition);

        config.status('doStream available records:', count);
        console.log('doStream available records:', count);
        let sortCondition: any = {};

        if (dateFieldName != null) {
            sortCondition[dateFieldName] = 1;
        }

        return new Promise((resolve, reject) => {
            return this.doGetCursor(condition, fieldList, sortCondition, 0, limit).then(cursor => {
                if (cursor == null) {
                    resolve(null);
                }
    
                let totalReceived = 0;
    
                let stream = cursor.stream();
                stream.on('data', doc => {
                    if (doc[dateFieldName] != null && typeof doc[dateFieldName] === 'string') {
                        doc[dateFieldName] = new Date(doc[dateFieldName]);
                    }
    
                    // console.log "Check [", doc[dateFieldName].getTime(), "][",_lastModified.getTime(), "]"
                    if (doc[dateFieldName] != null && doc[dateFieldName].getTime() > _lastModified.getTime()) {
                        // console.log "USING:", doc[dateFieldName]
                        _lastModified = doc[dateFieldName];
                    }
    
                    totalReceived++;
                    return callback(doc, totalReceived, count);
                });
    
                stream.on('error', err => {
                    console.log('doStream Stream error:', err);
                    console.log('doStream Stream condition:', condition);
                    resolve(_lastModified);
                });
    
                stream.on('end', (_err: Error) => {
                    resolve(_lastModified);
                });
            });
        })
    }

    //
    // Enable iteration through an entire collection
    //
    async doTail(condition: any, fieldList: any, offset: number, limit: number, callback: Function, sortCondition?: any) {
        let shouldWait: any;

        if (sortCondition == null) {
            sortCondition = {};
        }
        if (fieldList == null) {
            fieldList = null;
        }
        if (offset == null) {
            offset = 0;
        }
        if (limit == null) {
            limit = null;
        }
        if (shouldWait == null) {
            let shouldWait = true;
        }

        // TODO roy: this one should be written this way
        /**
         * Make sure to resolve after it returns all result...
         * Commenting for now just becuase it affects other portion of code.
         * Come back when this caurses issue.
         * ```
         * return new Promise(resolve => {
         *  this.doGetCursor().then(coursor => {
         *    const stream = cursor.stream();
         *    stream.on('end', ()=>{resolve(totalReceived)});
         *  })
         * })
         * ```
         */
        return new Promise((resolve, reject) => {
            try {
                return this.doGetCursor(condition, fieldList, sortCondition, offset, limit).then(cursor => {

                    if (cursor == null) {
                        return null;
                    }

                    let totalReceived = 0;

                    const stream = cursor.stream();
                    stream.on('data', doc => {
                        // console.log "doTail Data=", doc
                        totalReceived++;
                        return callback(doc, totalReceived);
                    });

                    stream.on('error', err => {
                        console.log('doTail Stream error:', err);
                        console.log('doTail Stream condition:', condition);
                        resolve(totalReceived);
                    });

                    stream.on('end', (_err: Error) => {
                        resolve(totalReceived);
                    });
                });
            } catch (e) {
                console.log('doTail Exception:', e);
                console.log('condition:', condition);
                console.log('fieldList:', fieldList);
                console.log('offset:', offset);
                console.log('limit:', limit);

                let log = config.getLogger('doTail');
                log.error('doTail', { condition, err: e });

                return reject(null);
            }
        });
    }

    async doCreateIndex(indexName: any, indexOptions: any) {
        await this.doInternalGetCollection();
        return this.theCollection.createIndex(indexName, indexOptions);
    }
}

export default MongoModel;
