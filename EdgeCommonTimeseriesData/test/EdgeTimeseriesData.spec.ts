
import EdgeTimeseriesData, { TimeSeriesDatabase } from '../src/EdgeTimeseriesData';
import config from 'edgecommonconfig';
const os = require('os');

config.setCredentials('influxdb', {
  type: "influxdb",
  host: process.env.INFLUX_HOST,
  username: "",
  password: ""
});

describe('EdgeTimeseriesData', () => {
  let timeseriesData: any;

  // Act before assertions
  before(async () => {
    timeseriesData = EdgeTimeseriesData.getInstance();
    await timeseriesData.dropDatabase('testdb');
  });

  it('doGetDatabases should return _intenal', async () => {
    let names = await timeseriesData.doGetDatabases();
    names.should.containEql('_internal');
  });

  it('doCreateDatabase should create db', async () => {
    return timeseriesData.doCreateDatabase('testdb').then((db: any) => {
      db.should.have.property('results');
    });
  });


  it('  ', async () => {
    let tags = { host: os.hostname() };

    let data = {
        avg: 4,
        total: 8,
        remain: 0,
        name: 'Test1',
        host: 'cornfield',
        memusage: 18078536,
        type: 'status',
        slot: 0,
        uuid: 'cornfieldTest11493741174841',
        first_seen: '2017-05-02T16:06:14.841Z',
        numproc: 8,
        proctime: 0
    };
    let response = await timeseriesData.doWriteMeasurement('testdb', 'RunJob', tags, data);
    response.should.be.eql(true);
    let result = await timeseriesData.doQuery('testdb', 'SELECT * FROM RunJob ORDER BY time DESC limit 20');
    result.should.have.property('data');
    result.data.should.be.an.Array();
  });

  it('doCreateDatabase query', async () => {
    return timeseriesData.doQuery('testdb', "SELECT * FROM RunJob ORDER BY DESC limit 20")
    .then((all: any) => {
      all.data.should.be.an.Array();
      all.data.length.should.be.eql(1);
    });
  });

  it('doGetMeasurements should return measurements', async () => {
    return timeseriesData.doGetMeasurements('testdb')
    .then((all: any) => {
      all.should.be.an.Array();
      all.length.should.be.eql(1);
    });
  });

});

describe('TimeSeriesDatabase', () => {
  let timeseriesData: any;

  // Act before assertions
  before(async () => {
    timeseriesData = new TimeSeriesDatabase(EdgeTimeseriesData.getInstance(), 'testdb');
  });


  it('doSaveMeasurement', async () => {
    return timeseriesData.doSaveMeasurement('Test', { tags: 'tags' }, { test: 'data' })
    .then((resp: any) => {
      resp.should.be.eql(true);
    });
  });

  it('doSaveMeasurement', async () => {
    return timeseriesData.doSaveMeasurement(null)
    .catch((err: any) => {
      timeseriesData.sendTimer();
      console.log(err);
    });
  });
});