import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';

const tableSettings: any = {
    "active": {
        "align": null,
        "autosize": false,
        "clickable": false,
        "displayFormat": null,
        "editable": true,
        "hideable": true,
        "name": "Active",
        "options": null,
        "order": 9,
        "required": false,
        "source": "active",
        "system": false,
        "tooltip": "",
        "type": "boolean",
        "visible": true,
        "width": 60
    },
    "classes": {
        "align": "right",
        "autosize": true,
        "clickable": false,
        "displayFormat": null,
        "editable": false,
        "hideable": true,
        "name": "Classes",
        "options": null,
        "order": 14,
        "render": "if (typeof(val) == \"object\") return Object.keys(val).length; \nreturn 0;",
        "required": false,
        "source": "classes",
        "system": false,
        "tooltip": "Testing Tooltip",
        "type": "simpleobject",
        "visible": true,
        "width": null
    }
};

describe('ProviderTable', () => {

    let api: EdgeApi;

    const testUser = {
        id: 'roy',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8',
        name: 'Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'roy@protovate.com',
        partner_id: 999,
        roles: {
            'global': [
                'allowEditTables'
            ],
        },
    };

    const allowEditTablesGroup = {
        id: 'allowEditTables',
        name: 'allowEditTables',
        description: 'Test Group for allowEditTables',
        roles: {
            'global': [
                'allowEditTables'
            ],
        },
        members: [
            'roy',
        ],
    };

    before(async () => {
        api = new EdgeApi();
        await api.doConnect();
        
        // Removing all data before tests
        await api.data_doDeletePath('master', `/user/${testUser.id}`);
        await api.data_doRemove('lookup', 'table');
        await api.data_doUpdatePath('lookup', `/groups/${allowEditTablesGroup.id}`, allowEditTablesGroup);
        await api.data_doUpdatePath('master', `/user/${testUser.id}`, testUser);
        await api.auth_doAuthUser('roy', 'pass', 'global');
    });

    it('doSetTable should success', async () => {
        const result = await api.table_doSetTable('servicex', tableSettings);
        expect(result).to.equal(true);
    });

    it('doGetTable should success', async () => {
        const result = await api.table_doGetTable('servicex');
        expect(result).to.have.property('id', 'servicex');
    });

    it('doChangeTable should success', async () => {
        const result: any = await api.table_doChangeTable('servicex', 'classes', 'align', 'left');
        expect(result).to.equal(true);
        // result.should.have.property('id', 'servicex');
        // result.classes.should.have.property('align', 'left');
    });

    it('doListTables should success', async () => {
        const result: any = await api.table_doListTables();
        expect(result.length).to.be.at.least(1);
    });

    it('doListTables should load from cache', async () => {
        const result: any = await api.table_doListTables();
        expect(result.length).to.be.at.least(1);
    });

    it('doListTables with prefix', async () => {
        const result: any = await api.table_doListTables('ser');
        expect(result.length).to.be.at.least(1);
    });
});