# ninjadebug
> Node.js debugging tools

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/ninjadebug.git
    
    import Ninja from 'ninjadebug';
    
    let ninja = new Ninja();
    
## General helper functions

### dump
> Dump attempts to display a variable as if it were an assignment that you can just copy/paste back into your code if needed.

    ninja.dump(titleName, items);
    
### dumpDate
> Returns a Date formatted value as debugDateFormat `dddd, MMMM Do YYYY, h:mm:ss a`

    ninja.dumpDate(Date.now());

### dumpNumber
> Returns a number formatted value as debugNumberFormat `#,###.[####]` 

    ninja.dumpNumber(1234.56789);
    
### dumpObject
> Returns a formatted object

    ninja.dumpObject(objectData, indent);

### dumpPercent
> Returns a text formatted value for a decimal that is a percent

    ninja.dumpPercent(123.45);

### dumpVar
> Returns a formatted value as several types - `null, string, number and boolean`

### log
> Log will take any number of items and output them similar to console.log
except that color is added automatically and objects are dummped nicely.

    ninja.log(items);
    
### pad
> Output a string from a number padded right to a given string length

    ninja.pad(items, 5, chalk.yellow, '#,###.[####]');
