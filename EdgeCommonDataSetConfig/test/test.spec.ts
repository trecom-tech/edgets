let key;
const jsonfile          = require('jsonfile');
import * as DataSetConfig from '../src';

const AllTestData = jsonfile.readFileSync("test/lookup.table.json");
for (key in AllTestData) {
    const v = AllTestData[key];
    if ((v.id == null) || (v.layout == null)) { continue; }
    const table_name = v.id;
}

describe("DataSetConfig Table Test", function() {
    it("should be created with table name", function() {
        const firstTableData = AllTestData[0];
        const tableName      = firstTableData.id;
        const firstTable     = new DataSetConfig.Table(tableName);
        firstTable.tableName.should.equal(tableName);
    });

    it("Test unserialize() to set columns from column configuration and test serialize() in table", function() {
        const firstTableData = AllTestData[1];
        const tableName      = firstTableData.id;
        const firstTable     = new DataSetConfig.Table(tableName);
        const columnSettings = firstTableData.layout;
        firstTable.unserialize(columnSettings);
        key = Object.keys(columnSettings)[0];
        let result = firstTable.getColumn(key);
        result.should.be.type('object');
        result.tableName.should.equal(tableName);
        result = firstTable.serialize();
        result.should.be.type('object');
        Object.keys(result).length.should.be.above(0);
    });

    return it("test getColumn method", function() {
        const tableData     = AllTestData[2];
        const tableName     = tableData.id;
        const table         = new DataSetConfig.Table(tableName);
        const columnConfig  = tableData.layout;
        table.unserialize(columnConfig);
        key           = Object.keys(columnConfig)[2];
        const original      = columnConfig[key];
        const result        = table.getColumn(key);
        result.getSource().should.equal(original.source);
    });
});


describe("DataSetConfig Column Test", function() {

    const firstTableData  = AllTestData[0];
    const tableName       = firstTableData.id;
    const firstTable      = new DataSetConfig.Table(tableName);
    const columnSettings  = firstTableData.layout;
    
    it("should be created with table name and match field value", () => {
        let left;
        firstTable.unserialize(columnSettings);
        key             = Object.keys(columnSettings)[0];
        let original        = columnSettings[key];
        let result          = firstTable.getColumn(key);
        result.should.be.type('object');
        result.getName().should.equal(original.name);
        result.getSource().should.equal(original.source);
        result.getEditable().should.equal(original.editable);
        result.getType().should.equal(original.type);
        result.getAlign().should.equal(original.align);
        result.getOrder().should.equal(original.order);
        left = original.calculation === true;
        result.getIsCalculation().should.equal((original.calculation === true) != null ? left : {true : false});
        result.getAlwaysHidden().should.equal(original.hideable);
        result.getClickable().should.equal(original.clickable);
        result.changeColumn('width', 100);
        result.getWidth().should.equal(100);
        original = columnSettings['classes'];
        result   = firstTable.getColumn('classes');
        result.getRenderFunction().should.be.type('function');
    });

    it("should be function type if function field is available", () => {
        firstTable.unserialize(columnSettings);
        key             = Object.keys(columnSettings)[0];
        const original        = columnSettings['classes'];
        const result          = firstTable.getColumn('classes');
        result.getRenderFunction().should.be.type('function');
    });

    it("should be worked fine on serialize() for column",  () => {
        firstTable.unserialize(columnSettings);
        key             = Object.keys(columnSettings)[0];
        const original        = columnSettings[key];
        const column          = firstTable.getColumn(key);
        const result          = column.serialize();
        result.name.should.equal(original.name);
        result.type.should.equal(original.type);
        result.width.should.equal(original.width);
        result.order.should.equal(original.order);
    });

    it("should be worked on Excel function if given render string is started with '='", () => {
        key             = Object.keys(columnSettings)[0];
        const original        = columnSettings[key];
        original.render = '=IF(a>b, a, b)';
        let column          = new DataSetConfig.Column(firstTableData.id);

        column.deserialize(original);
        column.setScope({'a': 10, 'b': 20 });
        column.getRenderFunction().should.equal(20);
        original.render = '=items*cost';
        column          = new DataSetConfig.Column(firstTableData.id);
        column.deserialize(original);
        column.setScope({'items': 10, 'cost': 20 });
        column.getRenderFunction().should.equal(200);
    });
});