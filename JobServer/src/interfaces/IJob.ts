export interface IJob {
    id                : string;
    title             : string;
    args?             : string | null;
    enabled?          : boolean;
    folder?           : string | null;
    freqType?         : string | null;
    freqRate?         : number | null;
    lastDataReceived? : Date   | null;
    lastDuration?     : number | null;
    lastRun?          : Date   | null;
    npm?              : string | null;
    owner?            : string | null
    pending?          : boolean;
    script?           : string | null
    timeout?          : number | null;
}