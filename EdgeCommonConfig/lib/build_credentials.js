"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const yargs = require("yargs");
const Config_1 = require("./Config");
const Encrypter_1 = require("./Encrypter");
const fs = require("fs");
const path = require("path");
const argv = yargs.usage('Usage: $0 [options]')
    .example('$0 --in ./prod_credentials/ --out ~/EdgeConfig/', 'Encrypt the credentials and put them to output folder')
    .alias('o', 'out')
    .nargs('o', 1)
    .describe('o', 'Output folder')
    .alias('i', 'in')
    .nargs('i', 1)
    .describe('i', 'Input folder')
    .demandOption(['i', 'o'])
    .help('h')
    .alias('h', 'help')
    .argv;
function processBuildCredentials() {
    return __awaiter(this, void 0, void 0, function* () {
        const inputPath = argv.in;
        const outputPath = argv.out;
        const key = Config_1.default.getKey();
        if (!key) {
            throw new Error('Can not find key');
        }
        const files = fs.readdirSync(inputPath);
        for (const fileName of files) {
            if (path.extname(fileName) === '.json') {
                const fileInputPath = path.join(inputPath, fileName);
                const fileContent = fs.readFileSync(fileInputPath);
                const encryptedContent = Encrypter_1.encrypt(fileContent.toString(), key);
                const fileOutputPath = path.join(outputPath, fileName);
                fs.writeFileSync(fileOutputPath, encryptedContent);
            }
        }
    });
}
processBuildCredentials()
    .then((result) => {
    console.log(result);
})
    .catch((err) => {
    console.log(err);
});
//# sourceMappingURL=build_credentials.js.map