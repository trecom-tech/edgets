$ ->

	TableZipcode = []
	TableZipcode.push
		name        : 'Code'
		source      : 'code'
		visible     : true
		editable    : false
		type        : 'text'
		required    : true

	TableZipcode.push
		name        : 'City'
		source      : 'city'
		visible     : true
		editable    : true
		type        : 'text'

	TableZipcode.push
		name        : 'State'
		source      : 'state'
		visible     : true
		editable    : false
		type        : 'text'

	TableZipcode.push
		name        : 'County'
		source      : 'county'
		visible     : true
		editable    : false
		type        : 'text'

	TableZipcode.push
		name        : 'Latitude'
		source      : 'lat'
		visible     : true
		editable    : true
		type        : 'decimal'

	TableZipcode.push
		name        : 'Longitude'
		source      : 'lon'
		visible     : true
		editable    : true
		options	    : "##.########"
		type        : 'decimal'

	newData1 = 
		city: "new city 1"
		state: "new state 1"
		county: "new county 1"
		lat: 1.23456
		lon: 9.87654

	newData2 = 
		city: "new city 2"
		state: "new state 2"
		county: "new county 2"
		lat: 2.34567
		lon: 8.76543

    ##|
    ##|  Load the JSON files.
    ##|  This will insert the zipcodes and testData into the global data map.
	addTest "Loading Data from files..", () ->
		loadZipcodes()
		.then ()->
			DataMap.setDataTypes 'zipcode', TableZipcode
			return true

		true


	addTestButton "Form with Pathfield - zipcode", "Open", ()=>
		addHolder().setView "Form", (view)->

			view.addInput "code", "zipcode"
			view.addInput "city", "City"
			view.addInput "state", "State"
			view.addInput "lon", "Longitude", { options: "##.######" }
			view.addInput "lat", "Latitude", { options: "##.######" }

			view.setPath "zipcode", "03105"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}"
			view.show()
		true

	addTestButton "Change record id on form", "Open", ()=>
		addHolder().setView "TestForms1", (view)->

	addTestButton "Dynamic form create", "Open", ()=>
		addHolder().setView "TestForms2", (view)->

	addTestButton "Form  and Table New Data Event", "Open", ()=>
		addHolder().setView "Form", (view)->

			view.addInput "code", "zipcode"
			view.addInput "city", "City"
			view.addInput "state", "State"
			view.addInput "lon", "Longitude", { options: "##.######" }
			view.addInput "lat", "Latitude", { options: "##.######" }

			view.setPath "zipcode", "03105"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}"
			view.show()
			setTimeout () =>
				DataMap.addData "zipcode", "03105", newData1
			, 3000
			setTimeout () =>
				DataMap.addData "zipcode", "03105", newData2
			, 6000
		true

	go()