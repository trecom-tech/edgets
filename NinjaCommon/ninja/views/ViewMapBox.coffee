ACCESS_TOKEN = 'pk.eyJ1IjoiYnJpYW5wb2xsYWNrIiwiYSI6IkdtQ2lqSmcifQ.jazOduUIH0pj8dRBFtZAGg'

##|
##|  SEE:
##|  https://www.mapbox.com/mapbox-gl-js/api/#map
##|  https://www.mapbox.com/mapbox-gl-js/example/
##|

##|
##|  Support class, contains functions to create a layer based on GeoJSON
class ViewMapGeojson

    ##|
    ##|  @param layerType {string} The layer type, default to 'FeatureCollection'
    ##|
    constructor: (layerType)->

        if !layerType? or layerType == "" then layerType = "FeatureCollection"
        @bounds = new mapboxgl.LngLatBounds()
        @geoJson =
            type: layerType
            features: []

    ##|
    ##|  Add a new location where you have the full location and properties
    geoJsonAdd: (location)=>

        if !location?
            console.log "Warning: geoJsonAdd called wit hout location:", location
            return false

        if !location.type?
            console.log "Warning: geoJsonAdd called without location type:", location
            return false

        if !location.geometry?
            console.log "Warning: geoJsonAdd called without location type:", location
            return false

        bbox = turf.extent(location)
        @bounds.extend mapboxgl.LngLatBounds.convert(bbox)

        @geoJson.features.push location
        return location

    ##|
    ##|  Add a new location to the feature
    ##|  @param location {object} The GeoJSON specific location geometry
    ##|  @param options {object} Any options to change  the geojson point property
    ##|  @return point {object} The geojson point that was added to the feature
    ##|
    geoJsonAddLocation: (location, options)=>

        point =
            type: 'Feature'
            geometry: location
            properties:
                title: ""
                description: ""

        if options?
            $.extend point.properties, options

        @geoJson.features.push point
        return point

    ##|
    ##|  Add a new point to the feature
    ##|  @param lat {double} Latitude
    ##|  @param lon {double} Longitude
    ##|  @param options {object} Optional values to change the point property
    ##|  @return point {object} The geojson point that was added to the feature
    ##|
    addPoint: (lat, lon, options)=>
        if lat > 90 or lat < -90
            console.log "Map Adding point - Invalid lat: ", lat
            return null

        point =
            type: 'Feature'
            geometry:
                type: 'Point'
                coordinates: [lon, lat]
            properties:
                title: ""
                description: ""

        if options?
            $.extend point.properties, options

        @geoJson.features.push point
        @bounds.extend [lon, lat]
        return point

    ##|
    ##|  Add a line string where the pointsLIst is an array of lat:, lon: objects
    addLine: (pointsList, options)=>

        line =
            type: 'Feature'
            properties:
                color: '#ff0000'
            geometry:
                type: 'LineString'
                coordinates: []

        for item in pointsList
            if item.lat?
                line.geometry.coordinates.push([item.lon, item.lat])
            else
                line.geometry.coordinates.push(item)

        if options?
            $.extend line.properties, options

        @geoJson.features.push line
        return line

class ViewMapBox extends View

    ##|
    ##|  All points that are currently shown on the map
    allPoints: []

    ##|
    ##|  All layers that are currently shown on the map
    allLayers: []

    ##|
    ##|  Default style for something that is a "Parcel" (spot of land)
    styleParcel:
        "color" : "rgba(135, 210, 223, 0.5)"
        "border" : "1px solid rgba(46, 161, 243, 0.20)"

    ##|
    ##|  Defualt style for something that is a parcel in hover state
    styleParcelHover =
        "color" : "rgba(237, 192, 80, 1.0)"
        "border" : "1px solid rgba(217, 172, 60, 1.0)"

    getDependencyList: ()=>
        [ "//api.tiles.mapbox.com/mapbox-gl-js/v0.44.0/mapbox-gl.css",
        "//api.tiles.mapbox.com/mapbox-gl-js/v0.44.0/mapbox-gl.js",
        "//api.tiles.mapbox.com/mapbox.js/plugins/turf/v3.0.11/turf.min.js",
        "//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.0/mapbox-gl-draw.js",
        "//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.0/mapbox-gl-draw.css",
        "//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.1.1/mapbox-gl-geocoder.min.js",
        "//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.1.1/mapbox-gl-geocoder.css",
        "//api.mapbox.com/mapbox.js/plugins/turf/v2.0.2/turf.min.js" ]


    constructor: () ->

        GlobalClassTools.addEventManager(this)

    setDrawingMode: (isEnabled = true) =>
        @drawingmode = isEnabled
        if isEnabled == true
            if @draw?
                return true
            else
                @draw = new MapboxDraw({
                    displayControlsDefault: false,
                    controls: {
                        polygon: true,
                        trash: true
                    }
                });

                @map.addControl(@draw);

                @map.on "draw.create", (e)=>
                    data = @draw.getAll();
                    if data.features.length > 0
                        @emitEvent "update", [data]
                        # console.log "Get draw points successfully."
                    else
                        console.log "Use the draw tools to draw a polygon!"

                @map.on "draw.update", (e)=>
                    data = @draw.getAll();
                    if data.features.length > 0
                        @emitEvent "update", [data]
                        # console.log "Get draw points successfully."
                    else
                        console.log "Use the draw tools to draw a polygon!"

                @map.on "draw.delete", (e)=>
                    @emitEvent "update", []
                    # console.log "None points."

            # console.log "Enabled drawing mode in mapbox."
        else
            @map.removeControl @draw
            @draw = null
            # console.log "Disabled drawing mode in mapbox."


    ##|   Set the viewport given a location
    ##|   @param lat {double} Latitute
    ##|   @param lon {double} Longitude
    ##|   @param zoomLevel {int=16} The map tile zoom level to use (See Slippy Map tiles)
    ##|
    setViewLocation: (lat, lon, zoomLevel = 16) =>

        if !lat? or !lon?
            console.log "Invalid call to setViewLocation(#{lat}, #{lon})"
            return

        ##|
        ##|  Set the location on the map
        @map.setCenter [lon, lat]
        if zoomLevel?
            @map.setZoom zoomLevel

        true

    ##|
    ##|  When the map view resizes, move the map control to the location.
    setSize: (w, h)=>
        super(w,h)
        if !@mapControl? then return true
        @mapControl.css
            left  : 0
            top   : 0
            width : w
            height: h
        true

    ##|
    ##| Change the pitch display on the map
    setPitch: (@pitch = 0)=>
        @map.setPitch(@pitch)

    ##|
    ##|  Enable navigation controls
    setNavigationControl: (@enablenavigationcontrol = true)=>
        if @enablenavigationcontrol == false
            if @navigationControl?
                @map.removeControl(@navigationControl)
                delete @navigationControl
            return true

        if @navigationControl?
            return true

        @map.addControl(new mapboxgl.ScaleControl({
            maxWidth: 80,
            unit: 'imperial'
        }));

        @navigationControl = new mapboxgl.NavigationControl()
        @map.addControl @navigationControl
        return @navigationControl

    ##|
    ##|  Enable the geocoder search control
    ##|
    setSearchControlEnabled: (@enablesearchcontrol = true)=>
        if @enablesearchcontrol == false
            if @searchControl?
                @map.removeControl @searchControl
                delete @searchControl
            return true

        if @searchControl?
            return true

        @searchControl = new MapboxGeocoder
            accessToken: ACCESS_TOKEN
        result = @map.addControl @searchControl

    ##|
    ##|  Set the map style
    ##|  @param newStyleName {string} one of basic, streets, bright, light, dark, satellite
    ##|
    setStyle: (@style)=>
        s = @map.getStyle()
        @map.setStyle('mapbox://styles/mapbox/' + @style + '-v9');

    ##|
    ##|  After a resize, check if the map is loaded and call its resize member function
    onResize: (w, h)=>
        super(w,h)
        if @map? and @map.loaded()
            @map.resize()
        else
            setTimeout @onResize, 500, w, h

        true

    ##|
    ##|  Initialize the map control
    onShowScreen: () =>

        new Promise (resolve, reject)=>

            @allPoints = []
            @allLayers = {}
            @allPopups = []
            @contextMenuEventSet = 0

            @gid = "Map" + GlobalValueManager.NextGlobalID()
            @mapControl = $ "<div id='#{@gid}' class='map'></div>"
            @el.append(@mapControl)
            @mapControl.css
                position: "absolute"
                left:     0
                top:      0
                width:    @width()
                height:   @height()

            ##|
            ##|  Setup MapBox primary object
            mapboxgl.accessToken = ACCESS_TOKEN
            @map = new mapboxgl.Map
                container:          @gid
                style:              "mapbox://styles/mapbox/streets-v9"
                center:             [-80.890674, 35.445173]
                zoom:               16
                maxZoom:            20
                maxNativeZoom:      19
                minNativeZoom:      5
                minZoom:            5
                attributionControl: false
                zoomControl:        true

            @map.on "load", (e) =>
                # console.log "ViewMapBox onShowScreen mapgl.on load bounds=", @map.getBounds();
                resolve(true)
                return true

            @map.on "contextmenu", (e) =>
                @emitEvent "contextmenu", [e, @map.getBounds()]
                # console.log "ViewMapBox onShowScreen mapgl.on contextmenu bounds = ", @map.getBounds()
                return true                     

            @map.on "zoom", (e) =>
                @emitEvent "zoom", [@map.getBounds()]
                # console.log "ViewMapBox onShowScreen mapgl.on zoomed bounds=", @map.getBounds();
                return true

            @map.on "zoomed", (e) =>
                @emitEvent "zoomed", [@map.getBounds()]
                # console.log "ViewMapBox onShowScreen mapgl.on zoomed bounds=", @map.getBounds();
                return true

            @map.on "resize", (e) =>
                @emitEvent "resize", [@map.getBounds()]
                return true
                # console.log "ViewMapBox onShowScreen mapgl.on resize bounds=", @map.getBounds();

            @map.on "moveend", (e) =>
                @emitEvent "moved", [e, @map.getBounds()]
                return true
                # console.log "ViewMapBox onShowScreen mapgl.on moveend bounds=", @map.getBounds();


    addBuildingsLayer: ()=>
        @enablebuildingslayer = true
        layers = @map.getStyle().layers;

        labelLayerId = null
        for layer in layers
            if layer.type == 'symbol' and layer.layout['text-field']
                labelLayerId = layer.id

        layer = @addLayer
            "id":           "3d-buildings"
            "source":       "composite"
            "source-layer": "building"
            "filter":       ['==', 'extrude', 'true']
            "type":         "fill-extrusion"
            "minzoom":      15
            "paint" :
                'fill-extrusion-color': '#aaa',

                # use an 'interpolate' expression to add a smooth transition effect to the
                # buildings as the user zooms in
                'fill-extrusion-height': [
                    "interpolate", ["linear"], ["zoom"],
                    15, 0,
                    15.05, ["get", "height"]
                ]

                'fill-extrusion-base': [
                    "interpolate", ["linear"], ["zoom"],
                    15, 0,
                    15.05, ["get", "min_height"]
                ],

                'fill-extrusion-opacity': .6
        ,  labelLayerId
        # console.log "Adding layer:", layer

        return layer


    ##|
    ##|  Record what layers are showing
    saveLayerState: ()=>
        @layerState = {}
        for name, layer of @allLayers
            @layerState[name] = false
            if @allLayers[name].options.filter? and @allLayers[name].options.filter.toString().match /false/
                @layerState[name] = true
        true

    ##|
    ##|  Store the previously saved state of showing layers
    restoreLayerState: ()=>
        if !@layerState? then return
        for name, layer of @allLayers
            if @allLayers[name].options.filter? and @allLayers[name].options.filter.toString().match /false/
                if @layerState[name] != true then @onToggleLayer(null, name)
            else
                if @layerState[name] != false then @onToggleLayer(null, name)

        true

    ##|
    ##|  Hide all map layers
    hideAllLayers: ()=>
        for name, layer of @allLayers
            if @allLayers[name].options.filter? and @allLayers[name].options.filter.toString().match /false/
                continue
            @onToggleLayer(null, name)
        true

    addSource: (id, source) =>
        if !id? or typeof id isnt "string" then return false
        if @map.getSource(id)
            @map.removeSource id
        @map.addSource id, source
        true

    ##
    ## Add a layer and make sure its id is unique
    addLayer: (options) =>
        layerId = options.id
        if !layerId? then return null
        @removeLayer layerId
        source = options.source
        if source?
            if typeof source is "string" and @map.getSource(source) and !@checkSourceIsUsed(source)
                @map.removeSource source
            else if typeof source is "object" and @map.getSource(layerId) and !@checkSourceIsUsed(layerId)
                @map.removeSource layerId
        @map.addLayer options

    ##|
    ##|  Remove a specific layer
    removeLayer: (layer)=>
        try
            if typeof layer == "string"
                ##|
                ##|  Remove by ID
                delete @allLayers[layer]
                if @map.getLayer(layer)
                    @map.removeLayer layer
            else
                console.log "Can't remove layer from object:", layer

        catch e
            console.log "removeLayer exception:", e

        true

    ##|
    ##|  Toggle a specific layer showing
    ##|  @param e {event} The event object if called from an event or null
    ##|  @param name {string} The name of the layer
    ##|
    toggleLayer: (name)=>
        # console.log "Toggle Layer:", name

        if !@allLayers[name]?
            console.log "Error in toggleLayer, layer #{name} not found."
            return true

        visibility = @map.getLayoutProperty(name, 'visibility');
        if (visibility is 'visible')
            @map.setLayoutProperty(name, 'visibility', 'none');
        else
            @map.setLayoutProperty(name, 'visibility', 'visible');

        true


    ##|
    ##|  Reset the map view back to starting, remove all layers and reset all states
    ##|
    resetView: =>

        if @allPoints? and @allPoints.length?
            @map.removeLayer o for o in @allPoints

        for name, layer of @allLayers
            @map.removeLayer name
            @map.removeSource name

        @allPoints = []
        @allLayers = {}
        @allPopups = []

        @mapPointLat = 0
        @mapPointLon = 0
        true

    fit: (loc)=>
        @map.fitBounds(loc.bounds, { padding: 20 })

    ##|
    ##|  Fit all points on the map to the visible window
    ##|  @return true - Always returns true
    ##|
    fitAll: () =>
        ##|
        ##|  TODO:  Not yet coded,  see map.fitBounds function
        ##|

        bounds = new mapboxgl.LngLatBounds()
        for id, layer of @allLayers when layer.getBounds?
            bounds.extend(layer.getBounds())

        @map.fitBounds(bounds, { padding: 120 })
        true

    ##|
    ##|  Accepts a layer object (which is ViewMapGeojson) and adds it as a layer to the map
    ##|  returns the mapbox layer object.
    ##|
    ##|  @param layerObject {VieMapGeoJson} Feature details with geometry
    ##|  @param name {String} the name of the layer used to toggle it
    ##|  @param callback {function} An optional function that is called with (e, name, myLayer) when the layer is available on the map.
    ##|
    ##|  @return myLayer {Mapbox layer object} The mapbox / leaflet layer object that was added
    ##|

    ##|
    ##|  Create a new map layer based on GeoJSON
    ##|
    initGeoJson: ()=>
        geojson = new ViewMapGeojson()
        return geojson

    addGeoJsonLayer: (layerObject, name, callback)=>

        myLayer = L.mapbox.featureLayer().addTo(@map);
        if callback?
            myLayer.on "layeradd", (e)->
                callback(e, name, myLayer)

        myLayer.setGeoJSON layerObject.geoJson
        @allPoints.push myLayer

        if name?
            @allLayers[name] = myLayer

        @emitEvent "layers_change", [Object.keys(@allLayers).length]
        layerObject.myLayer = myLayer
        return myLayer

    ##|
    ##|  Load a custom image
    addCustomSymbol: (symbolName, urlToImage, callback)=>

        new Promise (resolve, reject)=>

            @map.loadImage urlToImage, (err, image)=>
                if err?
                    console.log "addCustomSymbol error, url=#{urlToImage}, err=", err
                else
                    if @map.hasImage(symbolName) is false
                        @map.addImage(symbolName, image)
                        if !@customSymbols? then @customSymbols = {}
                        @customSymbols[symbolName] = urlToImage
                if callback?
                    callback(symbolName, image)

                resolve(true)

    addFilledLayer: (layerId, layerObject, fillColor, otherOptions, callback)=>

        options =
            id: layerId
            type: "fill"
            source:
                type: "geojson"
                data: layerObject.geoJson
            layout: {}
            paint:
                "fill-color" : fillColor
                "fill-opacity" : 0.5

        if otherOptions?
            $.extend options, otherOptions

        layer = @addLayer options
        if layer then @allLayers[layerId] = layer
        @emitEvent "layers_change", [Object.keys(@allLayers).length]
        return layer

    addSymbolLayer: (layerId, layerObject, otherOptions, showPopup, callback)=>

        options =
            id: layerId
            type: "symbol"
            minzoom: 8
            source:
                type: "geojson"
                data: layerObject.geoJson
            layout:
                "icon-image":            "{icon}"
                "icon-allow-overlap":    true
                "icon-ignore-placement": true
                "text-field":            "{title}"
                "text-allow-overlap":    true
                "text-font":             ["Arial Unicode MS Regular"]
                "text-offset":           [0, 0.8]
                "text-anchor":           "top"
                "text-size":             { stops: [ [7, 4], [10, 6], [14, 8], [15, 10], [16, 14]]}

        if otherOptions?
            $.extend options, otherOptions

        layer = @addLayer options
        if layer then @allLayers[layerId] = layer
        @emitEvent "layers_change", [Object.keys(@allLayers).length]
        ##
        ## show popup when mouse hover on mark ups
        if showPopup is true
            popup = @addCustomPopup()
            @map.on "mouseenter", layerId, (e) =>
                ## Change the cursor style as a UI indicator.
                @map.getCanvas().style.cursor = 'pointer'
                ## Populate the popup and set its coordinates
                ## based on the feature found.
                popup.setLngLat(e.features[0].geometry.coordinates)
                    .setHTML(e.features[0].properties.description)
                    .addTo(@map)
            @map.on "mouseleave", layerId, (e) =>
                @map.getCanvas().style.cursor = ''
                popup.remove()

        return layer

    hideLayer: (layerId)=>
        if !@allLayers[layerId]?
            # console.log "Error in hideLayer, layer #{layerId} not found."
            return false
        @map.setLayoutProperty(layerId, 'visibility', 'none')
        true

    showLayer: (layerId)=>
        if !@allLayers[layerId]?
            # console.log "Error in showLayer, layer #{layerId} not found."
            return false
        @map.setLayoutProperty(layerId, 'visibility', 'visible')
        true

    addLinesLayer: (layerId, layerObject, otherOptions, callback)=>

        options =
            id: layerId
            type: "line"
            source:
                type: "geojson"
                data: layerObject.geoJson
            paint:
                "line-width" : 3
                "line-color" : ["get", "color"]

        if otherOptions?
            $.extend options, otherOptions

        layer = @map.getSource(layerId)
        if layer?
            layer.setData(options.source.data)
            @showLayer(layerId)
        else
            layer = @addLayer options

        if layer then @allLayers[layerId] = layer
        @emitEvent "layers_change", [Object.keys(@allLayers).length]
        return layer

    ##
    ## add custom popu
    addCustomPopup: (otherOptions) =>
        markerHeight = 20
        markerRadius = 10
        linearOffset = 15
        popupOffsets =
            'top': [0, 0]
            'top-left': [0,0]
            'top-right': [0,0]
            'bottom': [0, -markerHeight]
            'bottom-left': [linearOffset, (markerHeight - markerRadius + linearOffset) * -1]
            'bottom-right': [-linearOffset, (markerHeight - markerRadius + linearOffset) * -1]
            'left': [markerRadius, (markerHeight - markerRadius) * -1]
            'right': [-markerRadius, (markerHeight - markerRadius) * -1]    
        options = 
            closeButton  : false
            closeOnClick : false
            offset       : popupOffsets
        
        if otherOptions?
            $.extend options, otherOptions

        popup = new mapboxgl.Popup(options)
        @allPopups.push popup
        popup    

    ##|
    ##| add labels at specified (lng, lat) of each element
    ##| @param: array labels
    ##|
    addLabels: (labels=null)=>
        geojson_blue =
            "type": "geojson"
            "data":
                "type": "FeatureCollection"
                "features": []

        geojson_red =
            "type": "geojson"
            "data":
                "type": "FeatureCollection"
                "features": []

        if labels?
            for lab in labels
                if lab.color is "blue"
                    geojson_blue.data.features.push {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [lab.lon, lab.lat]
                        },
                        "properties": {
                            "title": lab.label
                        }
                    }
                else if lab.color is "red"
                    geojson_red.data.features.push {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [lab.lon, lab.lat]
                        },
                        "properties": {
                            "title": lab.label
                        }
                    }
        else
            return

        @addSource "points_red", geojson_red
        @addLayer {
            "id": "point_red"
            "type": "circle"
            "source": "points_red"
            "paint":
                "circle-radius": 25
                "circle-color": "red"
        }
        @addLayer {
            "id": "symbols_red"
            "type": "symbol"
            "source": "points_red"
            "layout":
                "symbol-placement": "point"
                "text-font": ["Open Sans Regular"]
                "text-field": '{title}'
                "text-size": 14
            "paint":
                "text-color":"white"
        }

        @addSource "points_blue", geojson_blue
        @addLayer {
            "id": "point_blue"
            "type": "circle"
            "source": "points_blue"
            "paint":
                "circle-radius": 25
                "circle-color": "blue"
        }
        @addLayer {
            "id": "symbols_blue"
            "type": "symbol"
            "source": "points_blue"
            "layout":
                "symbol-placement": "point"
                "text-font": ["Open Sans Regular"]
                "text-field": '{title}'
                "text-size": 14
            "paint":
                "text-color":"white"
        }


    ##|
    ##|  Make a callback and pass in a popupmenu object
    ##|  @param contextMenuTitle {string} the title of the context menu to show
    ##|  @param contextMenuCallbackFunction {function} A callback with the popup menu, lat, and lon of the click.
    ##|
    setupContextMenu: (@contextMenuTitle, @contextMenuCallbackFunction) =>

        if @contextMenuEventSet != 0
            return true

        @contextMenuEventSet = 1
        @map.on "contextmenu", (obj) =>

            obj.originalEvent.preventDefault()
            obj.originalEvent.stopPropagation()

            coords = GlobalValueManager.GetCoordsFromEvent(obj.originalEvent)
            popupMenu = new PopupMenu @contextMenuTitle, coords.x, coords.y
            @contextMenuCallbackFunction popupMenu, obj.latlng.lat, obj.latlng.lng
            true

    ##|
    ##|  See https://www.mapbox.com/mapbox-gl-js/example/flyto-options/
    flyTo: (loc, options)=>

        if !loc? then return

        dest = {}

        if loc.coordinates?
            dest.center = loc.coordinates
        else if Array.isArray(loc)
            dest.center = loc
        else
            console.log "Unknown location to fly:", loc

        if options?
            $.extend dest, options, false

        # console.log "FLY:", dest
        @map.flyTo(dest)
        true

    checkSourceIsUsed: (source) =>
        layers = @map.style._layers
        if !layers?
            return false

        for id, layer of layers
            if layer.source is source then return true
        false

    serialize: () =>
        if !@map? then return null
        center = @map.getCenter()
        size = 
            width:  @mapControl.width()
            height: @mapControl.height()
        symbols = []
        if @customSymbols?
            for title, url of @customSymbols
                symbols.push
                    title: title
                    image: url
        layers = []
        for name, l of @allLayers
            layer = @map.getLayer(name)
            if !layer? then continue
            try
                if layer.type is "symbol"
                    points = []
                    source = this.map.getSource(name).serialize()
                    for feature in source.data.features
                        if feature.geometry.type is "Point"
                            points.push
                                lon: feature.geometry.coordinates[0]
                                lat: feature.geometry.coordinates[1]
                                options: feature.properties
                    layers.push
                        name: name
                        type: "symbol"
                        points: points

                else if layer.type is "line"
                    lines = []
                    source = this.map.getSource(name).serialize()
                    for feature in source.data.features
                        if feature.geometry.type is "LineString"
                            lines.push
                                start:
                                    lon: feature.geometry.coordinates[0][0]
                                    lat: feature.geometry.coordinates[0][1]
                                end:
                                    lon: feature.geometry.coordinates[1][0]
                                    lat: feature.geometry.coordinates[1][1]
                                options: feature.properties
                    layers.push
                        name: name
                        type: "line"
                        lines: lines
            catch e
                console.log "Mapbox error in serializing layers: ", e

        return
            center: center
            drawingmode: @drawingmode
            style: @style
            pitch: @pitch
            enablesearchcontrol: @enablesearchcontrol
            enablenavigationcontrol: @enablenavigationcontrol
            enablebuildingslayer: @enablebuildingslayer
            symbols: symbols
            layers: layers

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewMapBox: invalid data passed"
            if !@map?
                setTimeout () =>
                    @deserialize options
                    resolve false
                , 100

            if options.center?
                @setViewLocation options.center.lat, (options.center.lon or options.center.lng), (options.center.zoomlevel or options.zoomlevel)

            if options.size?
                @setSize (options.size.w or options.size.width), (options.size.h or options.size.height)

            if options.drawingmode?
                @setDrawingMode options.drawingmode

            if options.style?
                @setStyle options.style

            if options.pitch?
                @setPitch options.pitch

            if options.enablesearchcontrol?
                @setSearchControlEnabled options.enablesearchcontrol

            if options.enablenavigationcontrol?
                @setNavigationControl options.enablenavigationcontrol

            if options.enablebuildingslayer
                @addBuildingsLayer()

            symbols = []
            if options.symbols?
                for symbol in options.symbols
                    symbols.push @addCustomSymbol symbol.title, symbol.image, symbol.callback

            Promise.all(symbols)
            .then () =>
                if options.layers? and options.layers.length > 0
                    for layer in options.layers
                        if layer.type is "symbol"
                            if layer.points? and layer.points.length > 0
                                loc = @initGeoJson()
                                for p in layer.points
                                    loc.addPoint p.lat, p.lon, p.options
                                @addSymbolLayer (layer.name or "points"), loc, layer.options, layer.showtooltip, layer.callback
                                if layer.fit
                                    @fit loc
                        else if layer.type is "line"
                            if layer.lines? and layer.lines.length > 0
                                lines = @initGeoJson()
                                for l in layer.lines
                                    lines.addLine [ l.start, l.end ], l.options
                                @addLinesLayer (layer.name or "lines"), lines, layer.options, layer.callback
                                if layer.fit
                                    @fit lines

            resolve true
