"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const IApiUser_1 = require("./interfaces/IApiUser");
const edgecommonrequest_1 = require("edgecommonrequest");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgeapi_1 = require("edgeapi");
const moment = require('moment-timezone');
//  Store know IP address for this node.
let globalNetworkAddress = null;
class ApiWebUser extends IApiUser_1.default {
    constructor(myConnectionID, userIpAddress, request) {
        super();
        this.myConnectionID = myConnectionID;
        this.userIpAddress = userIpAddress;
        this.request = request;
        this.myName = null;
        this.rec = null;
        this.authAttempts = 0;
        this.ipAddress = null;
        this.userAgent = null;
        this.cookie = null;
        this.location = {};
        this.myName = this.myConnectionID;
        // TODO: Make sure these are valid objects before trying to assign
        if (request && request.headers) {
            this.userAgent = request.headers['user-agent'];
            this.cookie = request.headers['cookie'];
        }
        if (request && request.headers && request.headers['x-real-ip']) {
            userIpAddress = request.headers['x-real-ip'];
        }
        if (request && request.headers && request.headers['x-forwarded-for"']) {
            userIpAddress = request.headers['x-forwarded-for"'];
        }
        // Assign IP Address from remote connection
        this.ipAddress = userIpAddress;
    }
    doInitUser() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.internalLookupIP();
            // convert the timezone text to an offset
            const timezone = moment.tz.zone(this.location.timezone);
            const offset = (timezone && timezone.utcOffset(new Date())) || 0;
            this.timezoneOffset = (-1 * parseFloat(offset)) / 60;
        });
    }
    addSubscription(data) {
        // TODO should be implementer later
        return false;
    }
    /**
     * Lookup the IP address using an API or cache.  Associate the IP data with the
     * connection.  In the future this may also lead to geo blocking based on country
     * or some other aspect.
     */
    internalLookupIP() {
        return __awaiter(this, void 0, void 0, function* () {
            // Special case, translate the IP address to the local machine.
            if (this.ipAddress === "::1") {
                if (globalNetworkAddress) {
                    this.ipAddress = globalNetworkAddress;
                }
                else {
                    edgecommonconfig_1.config.status("internalLookupIP: Finding out my network address");
                    let req = new edgecommonrequest_1.default();
                    let ipResult = yield req.doGetLink("https://api.ipify.org?format=json");
                    if (ipResult && ipResult.ip) {
                        globalNetworkAddress = ipResult.ip;
                        this.ipAddress = ipResult.ip;
                    }
                    else {
                        // TODO: What happens if this fails?  Not sure what to do
                    }
                }
            }
            if (this.ipAddress.includes('127.0.0.1')) {
                this.location = {
                    "as": "Localhost Testing",
                    "city": "Testing",
                    "country": "Testing",
                    "countryCode": "TST",
                    "isp": "LOCAL",
                    "lat": 0,
                    "lon": 0,
                    "org": "Local",
                    "query": "127.0.0.1",
                    "region": "TST",
                    "regionName": "Testing",
                    "status": "success",
                    "timezone": "America/New_York",
                    "zip": "00000"
                };
            }
            else {
                // First check cache
                let strCacheID = "IP-" + this.ipAddress;
                let cachedResult = yield edgeapi_1.default.cacheGet(strCacheID);
                if (!cachedResult) {
                    // See https://members.ip-api.com/docs/json
                    // Todo: This is currently hard coded for a protovate API Key and should be
                    // moved to credentials at some point.
                    //
                    edgecommonconfig_1.config.status("ApiSocketUser:internalLookupIP IP Address not in cache for ", strCacheID);
                    let req = new edgecommonrequest_1.default();
                    let result = yield req.doGetLink("https://pro.ip-api.com/json/" + this.ipAddress + "?key=bR6cYYCubwO3wmd");
                    // Update the user object with a location.
                    this.location = result;
                    // Update cache
                    if (result && result.timezone) {
                        edgeapi_1.default.cacheSet(strCacheID, result);
                    }
                }
                else {
                    try {
                        this.location = JSON.parse(cachedResult.toString());
                    }
                    catch (e) {
                        edgecommonconfig_1.config.status("internalLookupIP = Invalid Cache Result");
                        edgeapi_1.default.cacheSet(strCacheID, null);
                    }
                }
            }
        });
    }
}
exports.default = ApiWebUser;
;
//# sourceMappingURL=ApiWebUser.js.map