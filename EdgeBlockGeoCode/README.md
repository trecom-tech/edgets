# GeoCode
> Uses multiple methods to convert an address to lat/lon location

    npm install git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeBlockGeoCode

    import GeoCode from 'EdgeBlockGeoCode';
    const geocode = new GeoCode();

    const testAddress1 = {
        display: "293 Montibello Dr, Mooresville, NC 28117",
        number : 239,
        street : "Montibello Dr",
        city   : "Mooresville",
        state  : "NC",
        hash   : "TEST0000111122223333"
    };

    geocode.process(address);
    geocode.doGetCensus(address);
    geocode.doGetGeocodio(address);
    geocode.doGetMapbox(address);
    geocode.doGetOpenCage(address);
    geocode.doGetMapboxReverse(35.58838, -80.8884);


Results:

    {
        _lastModified: 2017-02-03T20:36:55.296Z,
        source: 'CensusData',
        confidence: 100,
        lat: 35.58838,
        lon: -80.8884,
        address:
        {
            display: '293 MONTIBELLO DR, MOORESVILLE, NC, 28117',
            city: 'MOORESVILLE',
            state: 'NC',
            zip: 'NC' },
            other: {}
        }
    }