import IApiUser from './IApiUser';
export declare class IApiUserOffline extends IApiUser {
    addSubscription(data: any): boolean;
    isLoaded(): boolean;
}
export default IApiUserOffline;
