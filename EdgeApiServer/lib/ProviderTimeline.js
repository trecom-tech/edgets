"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug = require('slug');
const moment = require('moment');
const EdgeError_1 = require("./EdgeError");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const ApiProvider_1 = require("./ApiProvider");
const edgeapi_1 = require("edgeapi");
const collectionName = 'timeline';
const dbName = 'comms';
class ProviderTimeline extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
        const dataSet = slug(dbName);
        this.dsComms = new edgedatasetmanager_1.default(dataSet);
    }
    getBaseName() {
        return 'timeline';
    }
    /**
     * Create/Upate a new record in comms/timeline db with
     * Hashed string of {participant_id, category, eventStamp}
     * @param participant_id - [string] participant_id
     * @param category - [string] category
     * @param subcategory - [string] subcategory
     * @param text - [string] text
     * @param eventStamp - [any] eventStamp
     * @param eventOffset - [any] eventOffset
     * @param options - [any] option
     * @returns id of added/updated timelined db id
     */
    doAddTimeline(user, participant_id, category, subcategory, text, eventStamp, eventOffset, options) {
        return __awaiter(this, void 0, void 0, function* () {
            // Only whne user is same id to participant 
            // Or has serviceUser role
            if (!user.hasRole('adminManageUsers') && !user.hasRole('serviceUser') && user.rec.id !== participant_id) {
                throw new EdgeError_1.default('Authentication required', 'PROVIDER_TIMELINE');
            }
            if (!participant_id) {
                throw new EdgeError_1.default(`Validation Error: participant_id cannot be empty.`, 'PROVIDER_TIMELINE');
            }
            if (!text) {
                throw new EdgeError_1.default(`Validation Error: text cannot be empty.`, 'PROVIDER_TIMELINE');
            }
            if (eventStamp) {
                const timeStamp = moment(eventStamp);
                if (!timeStamp.isValid()) {
                    throw new EdgeError_1.default(`Validation Error: eventStamp is invalid.`, 'PROVIDER_TIMELINE');
                }
                eventStamp = new Date(timeStamp.utc().toISOString());
            }
            if (eventOffset === null || eventOffset === undefined) {
                throw new EdgeError_1.default(`Validation Error: eventOffset can not be empty.`, 'PROVIDER_TIMELINE');
            }
            const hashData = {
                participant_id,
                category,
                eventStamp
            };
            const timelineId = edgeapi_1.default.getHash(hashData);
            const timelineData = {
                participant_id,
                category,
                subcategory,
                text,
                eventStamp,
                eventOffset,
                options
            };
            yield this.dsComms.doUpdate(`/${collectionName}/${timelineId}`, timelineData);
            return timelineId;
        });
    }
    /**
     * Get all timeline items of that specific participant.
     * @param user - [ApiSocketUser]
     * @param participant_id - [string]
     * @returns participant's timeline documents.
     */
    doGetTimeline(user, participant_id) {
        return __awaiter(this, void 0, void 0, function* () {
            // Only whne user is same id to participant 
            // Or has serviceUser role
            if (!user.hasRole('serviceUser') && user.rec.id !== participant_id) {
                throw new EdgeError_1.default('Authentication required', 'PROVIDER_TIMELINE');
            }
            if (!participant_id) {
                throw new EdgeError_1.default(`Validation Error: participant_id cannot be empty.`, 'PROVIDER_TIMELINE');
            }
            const cond = `/timeline/participant_id:${participant_id}`;
            const fieldList = {};
            const sortOrder = { _id: -1 };
            const offset = 0;
            const limit = 10000000;
            const result = yield this.dsComms.doGetItems(cond, fieldList, sortOrder, offset, limit);
            return result;
        });
    }
}
exports.default = ProviderTimeline;
;
//# sourceMappingURL=ProviderTimeline.js.map