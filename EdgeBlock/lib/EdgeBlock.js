"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonrequest_1 = require("edgecommonrequest");
const edgeapi_1 = require("edgeapi");
const edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
//
//  Reference to the global cache for the block manager
let globalBlockManager = null;
//
//  The Block Manager is an interface that can dynamic load a "Block" at runtime
//  by loading the module and then keeping it loaded in a cache for future calls
//
class BlockManager {
    constructor() {
        this.blocks = {};
    }
    load(name, obj) {
        this.blocks[name] = new obj();
        return true;
    }
    run(name, ...param) {
        try {
            if ((this.blocks[name] == null)) {
                //
                //  Try to find the block in coffee or js file in one of these available
                //  path names and if found, load it.
                //
                let filename = edgecommonconfig_1.default.FindFileInPath(name + ".coffee", BlockManager.blockPaths);
                edgecommonconfig_1.default.status(`BlockManager run name=${name}, searching in ${BlockManager.blockPaths.join('m')}: ${filename}`);
                if (filename === null) {
                    filename = edgecommonconfig_1.default.FindFileInPath(name + ".js", BlockManager.blockPaths);
                    edgecommonconfig_1.default.status(`BlockManager run name=${name}, searching for .js in ${BlockManager.blockPaths.join('m')}: ${filename}`);
                }
                if (filename === null) {
                    edgecommonconfig_1.default.status(`BlockManager run name=${name} block file not found`);
                    throw new Error(`Unable to find block ${name}`);
                }
                filename = filename.replace(".coffee", "").replace(".js", "");
                const lib = require(filename);
                this.blocks[name] = new lib(name);
            }
            return this.blocks[name].process.apply(this.blocks[name], param);
        }
        catch (e) {
            return console.log("BlockManager exception:", e);
        }
    }
}
BlockManager.blockPaths = [
    "./",
    "../",
    "./node-modules/",
    "../node-modules/",
    process.env.HOME + "/EdgeData/Blocks/"
];
class EdgeBlock {
    static getBlockManager() {
        if (globalBlockManager === null) {
            globalBlockManager = new BlockManager();
        }
        return globalBlockManager;
    }
    ;
    constructor(blockName) {
        this.blockName = blockName;
        if ((this.blockName == null)) {
            this.blockName = this.constructor.name;
        }
        this.log = edgecommonconfig_1.default.getLogger(`block-${this.blockName}`);
        this.api = null;
    }
    doGetApi() {
        return edgeapi_1.default.doGetApi();
    }
    //
    //  Given a URL it will use the existing options to download it.
    //
    doGetLink(dataUrl, qs) {
        const r = new edgecommonrequest_1.default();
        return r.doGetLink(dataUrl, qs);
    }
    //
    //  Given a mongo object ID, return a date based on when it was created
    //
    dateFromObjectId(objectId) {
        return new Date(parseInt(objectId.toString().substring(0, 8), 16) * 1000);
    }
    //
    //  General error function to report one or more errors in the
    //  attempt to process the block data.
    //
    setError(message, optionalData) {
        edgecommonconfig_1.default.status(`EdgeBlock ${this.blockName} error: ${message}`);
        this.log.error(message, optionalData);
        return false;
    }
    //
    //  Send a record to a queue for additional processing
    //
    sendToQueue(queueName, data) {
        return new Promise((resolve, reject) => {
            const strBuffer = JSON.stringify(data);
            edgecommonconfig_1.default.status(`EdgeBlock ${this.blockName} sendToQueue ${queueName}:`, strBuffer);
            if ((this.cacheMessageQueue == null)) {
                this.cacheMessageQueue = {};
            }
            if ((this.cacheMessageQueue[queueName] == null)) {
                this.cacheMessageQueue[queueName] = 1;
                edgecommonmessagequeue_1.default.doOpenMessageQueue(queueName)
                    .then((mq) => {
                    this.cacheMessageQueue[queueName] = mq;
                    this.cacheMessageQueue[queueName].doSubmitBuffer(strBuffer);
                    resolve(true);
                });
            }
            else {
                if (typeof this.cacheMessageQueue[queueName] === "number") {
                    setTimeout(this.sendToQueue, 100, queueName, data);
                    resolve(true);
                }
                else {
                    this.cacheMessageQueue[queueName].doSubmitBuffer(strBuffer);
                    resolve(true);
                }
            }
            return true;
        });
    }
    //
    //  Add stats for the day key a key as an object or scalar
    updateStatsToday(name, keys) {
        return __awaiter(this, void 0, void 0, function* () {
            if ((this.api == null)) {
                const api = yield edgeapi_1.default.doGetApi();
                this.api = api;
                this.updateStatsToday(name, keys);
            }
            else {
                if (typeof keys !== "object") {
                    const tmp = {};
                    tmp[keys] = 1;
                    keys = tmp;
                }
                for (let keyName in keys) {
                    const keyValue = keys[keyName];
                    this.api.stats_doAddStats("today", name, keyName, keyValue);
                }
            }
            return true;
        });
    }
}
exports.default = EdgeBlock;
//# sourceMappingURL=EdgeBlock.js.map