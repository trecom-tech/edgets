class ViewDocumentPDF extends View
    controlHeight: 80
    canvasMargin : 10
    MAX_SCALE    : 1.5
    MIN_SCALE    : 0.1

    getDependencyList: () =>
        return [ "/vendor/pdfjs/pdf.js" ]

    getLayout: () =>
        view: "Docked"
        name: "viewDocked"
        size: 70

    onShowScreen: () =>
        @pdfjsLib = window['pdfjs-dist/build/pdf']
        @pdfjsLib.GlobalWorkerOptions.workerSrc = '/vendor/pdfjs/pdf.worker.js'
        @pdfjsLib.workerSrc = '/vendor/pdfjs/pdf.worker.js'

        # @pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js'
        # console.log 'PdfjsLib = ', @pdfjsLib

        @wgtControlsWrapper = @viewDocked.getFirst().addDiv "controls-wrapper"
        @wgtButons = @wgtControlsWrapper.addDiv "buttons-wrapper"
        @wgtButons.setView "NavBar", (@viewNavBar) =>
            @viewNavBar.deserialize
                items: [
                    type: "button"
                    icon: "far fa-folder-open"
                    name: "nav-btn-save"
                    text: "Open"
                    callback: @onClickOpenButton
                ,
                    type: "button"
                    icon: "far fa-minus-circle"
                    name: "nav-btn-zoom-out"
                    text: "Zoom out"
                    callback: @onZoomout
                ,
                    type: "button"
                    icon: "far fa-plus-circle"
                    name: "nav-btn-zoom-in"
                    text: "Zoom in"
                    callback: @onZoomin
                ,
                    type: "button"
                    icon: "far fa-arrow-left"
                    name: "nav-btn-prev"
                    text: "Prev"
                    callback: @onPrevPage
                ,
                    type: "button"
                    icon: "far fa-arrow-right"
                    name: "nav-btn-next"
                    text: "Next"
                    callback: @onNextPage
                ]

        @wgtInfo = @wgtControlsWrapper.addDiv "info-wrapper"
        @wgtTitle = @wgtControlsWrapper.addDiv "title-wrapper"
        @wgtFileInput = @viewDocked.getBody().add "input", "pdf-file-input", "pdf-file-input",
            type: "file"

        @wgtContent = @viewDocked.getBody().addDiv "content-wrapper"

        @canvasList = []
        @pdfDoc = null
        @totalPages = 0
        @pageNum = 1
        @pageRendering = false
        @pageNumPending = null
        @scale = 1
        @pdfTitle = ""

        @wgtFileInput.el.on "change", @onOpenPDFFile
        @wgtContent.on "scroll", @onScrollContent
        true

    setPDFSource: (@pdfSrc) =>
        if !@pdfjsLib?
            console.log "ViewDocumentPDF: Pdf library not loaded."

        return @pdfjsLib.getDocument(@pdfSrc).promise
        .then (@pdfDoc) =>
            @totalPages = @pdfDoc.numPages
            @setTitle()
            @setInfo()

            @renderAllPages()
            .then () =>
                @goToPage(1)
            .catch (err) =>
                console.log "ViewDocumentPDF: error rendering pages: ", err

        .catch (err) =>
            console.log "ViewDocumentPDF: error loading pdf from ", @pdfSrc
            true

    setScale: (scale) =>
        if scale < @MIN_SCALE or scale > @MAX_SCALE or scale is @scale then return false
        @scale = scale
        @viewNavBar.buttonList[1].disable(@scale <= @MIN_SCALE)
        @viewNavBar.buttonList[2].disable(@scale >= @MAX_SCALE)
        @renderAllPages()

    onClickOpenButton: (e) =>
        @wgtFileInput.el.click()
        true

    onOpenPDFFile: (e) =>
        # console.log "Opening pdf file: ", e.target.files
        file = e?.target?.files?[0]
        if !file?
            console.log "ViewDocumentPDF: invalid file opened: ", file
            return false
        if file.type isnt "application/pdf"
            console.log "ViewDocumentPDF: This file is not a pdf", file.type
            m = new ModalForm
                title: "File Error"
                content: "The file you've selected is not pdf."

            return false

        renderTypedArrayToPDF = (typedarray) =>
            @setPDFSource typedarray
            true

        fileReader = new FileReader()
        fileReader.onload = () ->
            typedarray = new Uint8Array(@result)
            # console.log "OnLoad typedarray = ", typedarray
            renderTypedArrayToPDF typedarray

        fileReader.readAsArrayBuffer(file)

    onScrollContent: (e) =>
        # console.log "Scroll event: ", e, @getCurrentPage()
        @pageNum = @getCurrentPage()
        @setInfo()
        true

    onZoomin: () =>
        @setScale(parseFloat((@scale + 0.1).toFixed(1)))

    onZoomout: () =>
        @setScale(parseFloat((@scale - 0.1).toFixed(1)))

    setTitle: () =>
        if @pdfSrc? and typeof @pdfSrc is "string"
            # console.log "title = ", @pdfjsLib.getFilenameFromUrl(@pdfSrc)
            @pdfTitle = @pdfjsLib.getFilenameFromUrl(@pdfSrc)
        else
            @pdfTitle = ""

        @wgtTitle.html @pdfTitle
        true

    setInfo: () =>
        @wgtInfo.html "Page #{@pageNum} / #{@totalPages}"

    renderAllPages: () =>
        if !@pdfDoc?
            console.log "PDF Document not ready to render." 
            return Promise.resolve(false)

        if @pageRendering is true
            # console.log "Rendering pages already..."
            return Promise.resolve(false)

        promises = []
        for pageNum in [1...@pdfDoc.numPages+1]
            promises.push(@renderPage pageNum)

        @pageRendering = true
        Promise.all(promises)
        .then () =>
            @pageRendering = false

    renderPage: (pageNum) =>
        if !@pdfDoc? then return false

        if pageNum <= @canvasList.length
            canvas = @canvasList[pageNum - 1].el[0]
            context = @canvasList[pageNum - 1].context
        else
            id = GlobalValueManager.NextGlobalID()
            wgtCanvas = @wgtContent.add "canvas", "pdf-canvas", "pdf-canvas#{id}"

            el = wgtCanvas.el
            canvas = wgtCanvas.el[0]
            context = canvas.getContext('2d')

            @canvasList.push
                el: el
                context: context

        @pageRendering = true
        return @pdfDoc.getPage(pageNum).then (page) =>
            # console.log "Page #{pageNum} loaded"
            viewport = page.getViewport
                scale: @scale
            # console.log "viewport = ", viewport
            canvas.height = viewport.height
            canvas.width  = viewport.width

            renderContext =
                canvasContext: context
                viewport:      viewport

            renderTask = page.render renderContext
            renderTask.promise.then () =>
                pageNum

    queueRenderPage: (num) =>
        if @pageRendering
            @pageNumPending = num
        else
            @renderPage(num)

    goToPage: (pageNum) =>
        el = @canvasList?[@pageNum - 1]?.el
        if !el? then return false

        elTop = 0
        for index in [0...pageNum-1]
            item = @canvasList[index]
            elTop += (item.el.height() + @canvasMargin)
        @wgtContent.el[0].scrollTop = elTop
        @pageNum = @getCurrentPage()
        @setInfo()
        @viewNavBar.buttonList[3].disable(pageNum <= 1)
        @viewNavBar.buttonList[4].disable(pageNum >= @totalPages)
        true

    getCurrentPage: () =>
        totalHeight = 0
        contentScroll = @wgtContent.el[0].scrollTop
        for item, index in @canvasList
            el = item.el
            canvas = el[0]
            if contentScroll < totalHeight
                # console.log "Scrolltop: ", contentScroll, totalHeight
                @pageNum = index
                return @pageNum

            totalHeight += (el.height() + @canvasMargin)
        @pageNum = @canvasList.length
        @pageNum

    onPrevPage: () =>
        currentPage = @getCurrentPage()
        if !currentPage? or currentPage <= 1
            return false
        @goToPage(currentPage - 1)
        true

    onNextPage: () =>
        currentPage = @getCurrentPage()
        if !currentPage? or currentPage >= @totalPages
            return false

        @goToPage(currentPage + 1)
        true

    setWidth: (@w) =>
        @width @w

    onResize: (w, h) =>
        true

    serialize: () =>
        obj = 
            source: @pdfSrc
            scale: @scale
            width: @w
        obj

    deserialize: (optionData) =>
        new Promise (resolve, reject) =>
            if !optionData? or typeof optionData isnt "object"
                reject "ViewDocumentPDF: invalid data to deserialize."

            if optionData.width?
                @setWidth optionData.width

            if optionData.scale?
                @setScale(optionData.scale)

            if optionData.source? or optionData.src?
                @setPDFSource(optionData.source or optionData.src)
                .then () =>
                    resolve true

            resolve false
