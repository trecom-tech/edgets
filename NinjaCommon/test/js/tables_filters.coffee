$ ->

    ##|
    ##|  This is just for diagnostics,  you don't need to verify the data map is
    ##|  loaded normally.  The data types should be loaded upon startup.
    addTest "Confirm Zipcodes datatype loaded", () ->
        dm = DataMap.getDataMap()
        if !dm? then return false

        zipcodes = dm.types["zipcode"]
        if !zipcodes? then return false
        if !zipcodes.col["code"]? then return false

        true

    ##|
    ##|  Load the zipcodes JSON file.
    ##|  This will insert the zipcodes into the global data map.
    addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

    addTestButton "Render table - Sort and Filter function to hide Areacode", "Open", ()->

        addHolder()
        .setView "Table", (view)->

            noAreaCode = (col)  -> col.name != "Area Code"
            filter = (obj, key) -> obj.county == "HAMPDEN"
            table = view.addTable "zipcode", noAreaCode, filter
            table.sort = (a, b) ->

                aa = DataMap.getDataField("zipcode", a.key, "city")
                bb = DataMap.getDataField("zipcode", b.key, "city")
                if aa < bb then return -1
                if aa > bb then return 1
                return 0
        true

    addTestButton "Table Set Filter", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            setTimeout () ->
                view.setFilter "code", "comparator", ">"
                view.setFilter "code", "value", "1040"
            , 2000

            setTimeout () ->
                view.setFilter "state", "comparator", "=~"
                view.setFilter "state", "value", "hamp"
            , 4000

            setTimeout () ->
                view.setFilter "city", "comparator", "="
                view.setFilter "city", "value", "Manchester"
            , 6000

            setTimeout () ->
                view.setFilter "state", "value", ""
            , 8000            
        true

    addTestButton "Table Reset All Filters", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            view.addFlag
                name: "Flag 1"
                icon: "fa fa-book"
            view.addFlag
                name: "Flag 2"
                icon: "fa fa-ban"
            view.setEnableFlags(true, 0)
            view.setEnableCheckboxes(true, 5)
            setTimeout () ->
                view.setFilter "code", "comparator", ">"
                view.setFilter "code", "value", "1040"
            , 2000

            setTimeout () ->
                view.setFilter "city", "comparator", "="
                view.setFilter "city", "value", "Manchester"
            , 4000

            setTimeout () ->
                view.resetCurrentFilters()
            , 6000
        true

    addTestButton "Save Searches", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.setTitle "Zipcode Table"
            view.setSavedSearch "test_saved_search"
        true

    addTestButton "Set Next Filter", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            setTimeout () ->
                table.setNextFilter "state"
            , 2000
            setTimeout () ->
                table.setNextFilter "state"
            , 4000
            setTimeout () ->
                table.setNextFilter "state"
            , 6000
            setTimeout () ->
                table.setNextFilter "state"
            , 8000
            setTimeout () ->
                table.setNextFilter "state"
            , 10000
        true

    go()