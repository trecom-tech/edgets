"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MultipartParser {
    constructor(boundary, bodyBuffer) {
        this.x = 0;
        this.y = 0;
        this.nlcount = 0;
        this.max_len = 0;
        this.mode = 'Searching';
        this.current = '';
        this.start = 0;
        this.items = [];
        this.bodyBuffer = bodyBuffer;
        this.max_len = this.bodyBuffer.length;
        this.boundary = `--${boundary}`;
    }
    nextChar() {
        if (this.x < this.max_len) {
            return this.bodyBuffer[this.x++];
        }
        return null;
    }
    moveToEndOfLine() {
        let str = "";
        while (true) {
            const ch = this.nextChar();
            if ((ch === 10) || (ch === 13)) {
                const peek = this.bodyBuffer[this.x];
                if ((peek === 10) || (peek === 13)) {
                    this.x++;
                }
                return str;
            }
            else if (ch === null) {
                return null;
            }
            else {
                str += String.fromCharCode(ch);
            }
        }
    }
    //
    //  We got the boundary, now search until \n\n or \r\n or \r\n\r\n
    parseHeaders() {
        while (true) {
            const line = this.moveToEndOfLine();
            if (line === null) {
                return false;
            }
            if (line.length === 0) {
                return true;
            }
            const parts = line.split(/: /, 2);
            this.currentItem[parts[0]] = parts[1];
        }
    }
    getBody() {
        this.currentItem.buffer = Buffer.alloc(this.x - this.start - this.boundary.length - 2);
        this.bodyBuffer.copy(this.currentItem.buffer, 0, this.start, this.x - this.boundary.length - 2);
        this.items.push(this.currentItem);
        return true;
    }
    findBoundary() {
        while (true) {
            const line = this.moveToEndOfLine();
            if (line === null) {
                return false;
            }
            if (line === this.boundary) {
                if (this.currentItem != null) {
                    this.getBody();
                }
                this.currentItem = {};
                this.currentItem.buffer = null;
                return true;
            }
            if (line === (this.boundary + "--")) {
                this.getBody();
                return true;
            }
        }
    }
    doParse() {
        return (() => {
            const result = [];
            while (true) {
                if (!this.findBoundary()) {
                    break;
                }
                if (!this.parseHeaders()) {
                    break;
                }
                result.push(this.start = this.x);
            }
            return result;
        })();
    }
}
exports.MultipartParser = MultipartParser;
exports.default = MultipartParser;
//# sourceMappingURL=MultipartParser.js.map