export default class EdgeRateCapture {
    readonly name: string;
    readonly uuid: string;
    readonly max: number;
    sampleValues: number[];
    sampleTimes: number[];
    loopTimer: number;
    head: number;
    remain: number;
    lastTimer: any;
    lastValue: number;
    varList: any;
    varNames: any;
    lastVars: any;
    firstSeen: string;
    hideOutput: boolean;
    logTimerseriesData: boolean;
    totalSamples: number;
    lastTimerStatus: any;
    lastValueStatus: any;
    statusUpdateClient: any;
    status: any;
    timeSeriesData: any;
    timeSeriesDatabase: any;
    timeSeriesCounter: number;
    constructor(name: string);
    onBeforeUpdate(varList: any, status: any): boolean;
    /**
     * Reset all the values we are tracking
     */
    reset(): boolean;
    /**
     * Stop tracking
     */
    stop(): void;
    /**
     * Increase the total units captured
     * @param totalSamples
     * @param remain
     */
    addSample(totalSamples: number, remain?: number): void;
    incVar(name: string, amount?: number): any;
    setVar(name: string, amount?: number): any;
    addVar(name: string, title: string, value?: number): boolean;
    /**
     * Raw data to send to the status server
     */
    private getStatusData;
    private statusUpdateTimer;
    /**
     * Display the status text
     */
    private showStatusTextDetailed;
    private getCurrentRate;
}
