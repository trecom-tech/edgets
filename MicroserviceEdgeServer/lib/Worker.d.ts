import ClusterWorker from 'edgecommonclusterworker';
export declare class Worker extends ClusterWorker {
    reDoubleSlash: RegExp;
    changemqConnection: any;
    dataStore: any;
    invalidJobLog: any;
    sampleData: any[];
    nCounter: number;
    cache: any;
    constructor(...args: any[]);
    onInit(): Promise<boolean>;
    processJobMessage(job: any): boolean;
    doOpenChangeMessageQueue(): Promise<any>;
    processJobInsertData(job: any): any;
    processJobDelete(job: any): Promise<boolean>;
    processJobAppendData(job: any): Promise<boolean>;
    processJobSaveData(job: any): Promise<boolean>;
    processJob(job: any): any;
    setup(): Promise<boolean>;
}
export default Worker;
