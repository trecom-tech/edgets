export declare class NpmModuleTools {
    /**
     * verify packages are installed
     * @param moduleSource
     * @param path
     */
    static verifyInstalled(moduleSource: any, path: any, forceInstall?: boolean): Promise<unknown>;
}
export default NpmModuleTools;
