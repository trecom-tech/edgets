import MongoGateway from './MongoGateway';
import DataSetConfig from './DataSetConfig';
import MongoModel from './MongoModel';
export declare class DataSetManager {
    changeCollection: MongoModel;
    changeCollectionPromise: Promise<MongoModel>;
    settingsPromise: {
        [tableName: string]: Promise<DataSetConfig>;
    };
    collectionPromise: {
        [collectionName: string]: Promise<MongoModel>;
    };
    collections: {
        [collectionName: string]: MongoModel;
    };
    alternateConnectionConfig: any;
    db: MongoGateway;
    log: any;
    databaseName: string;
    /**
     * Static member function opens a database set manager and returns the named collection
     * @param databaseName
     * @param collectionName
     */
    static doOpen(databaseName: string, collectionName: string): Promise<MongoModel>;
    constructor(databaseName: string, alternateConnectionConfig?: any);
    onInit(): {};
    doGetChangeCollection(): Promise<MongoModel>;
    /**
     * Get a reference to the settings for this table
     * @param tableName
     */
    doInternalGetSettings(tableName: string): Promise<DataSetConfig>;
    /**
     * Get a list of tables and counts open
     */
    doGetUpdatedStats(): Promise<any>;
    /**
     * Create or verify an index
     * @param tableName - [string] Collection Name
     * @param keyField - [string] Field name to index
     * @param indexType - [string] Index type: mongo field
     * @param isUnique - [string] Uniqueness to provide mongo indexing function
     */
    doVerifySimpleIndex(tableName: string, keyField: any, indexType?: any, isUnique?: any): Promise<void>;
    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetItems(path: any, fieldList?: any, sortCondition?: any, offset?: any, limit?: any): Promise<any[]>;
    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doStream(path: any, fieldList: any, offset: any, limit: any, callback: any): Promise<{}>;
    /**
     * Exactly the same as doGetItems except that records are returned
     * as an array with the id as the key field
     * @param path
     * @param fieldList
     * @param sortCondition
     * @param offset
     * @param limit
     */
    doGetItemsArray(path: string, fieldList?: any, sortCondition?: any, offset?: number, limit?: number): Promise<any>;
    /**
     * Count some search
     * @param path
     */
    doCount(path: any): Promise<number>;
    getDistanceFromPolygon(origin: any, loc: {
        coordinates: string[];
    }): number;
    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param path [mixed] Can be a compiled path or string path
     * @param centerLocation [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles [number] the number of miles to search around
     * @param offset [number] Optional starting point in the result list (default 0)
     * @param limit [number] Optional the maximum number of results (default 1000)
     */
    doFindNearby(path: any, centerLocation: any, miles: any, offset: number, limit: number): Promise<any>;
    doFindNearbyIds(path: any, centerLocation: any, miles: any, offset: number, limit: number, fieldList?: any): Promise<any[]>;
    doGetUniqueValues(path: any, matchCondition: any): Promise<any[]>;
    /**
     * Lookup a record within a dataset.
     * @param path - [string] path
     * @param createIfNeeded - [string] if element does not exist on path, create one
     * @returns first element in that search criteria
     */
    doGetItem(path: any, createIfNeeded?: any): Promise<any>;
    /**
     *
     * @param path
     * @param newJson
     * @param updateAgentID
     */
    doInsert(path: any, newJson: any, updateAgentID?: any): Promise<boolean>;
    /**
     * Given a path, delete everything under it
     * @param path
     */
    doDeletePath(path: any): Promise<false | any[]>;
    /**
     *
     * @param path
     * @param newJson
     */
    doCheckUpdateHash(path: any, newJson: any): Promise<boolean>;
    /**
     *
     * @param path
     * @param newJson
     */
    doSaveUpdateHash(path: any, newJson: any): Promise<boolean>;
    /**
     * Given a path and some json data, append the data to the path
     * treating the path like an array. if the path is not an array then
     * we find the next id and add it that way.
     * @param path
     * @param newJson
     * @param updateAgentID
     */
    doAppend(path: any, newJson: any, updateAgentID?: any): Promise<{
        kind: string;
        path: any;
        lhs: string;
        rhs: any;
    }[] | {
        ok: any;
        nModified: any;
        n: any;
    }>;
    /**
     * Given a path and some new JSON data, update the object
     * save the results back to the database
     * @param path - [String|object] Path can be a string path or a parsed path
     * @param newJson - [any] the data to be added or updated
     * @param updateAgentID - [string] the _id of an approved agent (optional)
     */
    doUpdate(path: string | any, newJson: any, updateAgentID?: string): Promise<any[] | boolean>;
    doRecordDataChanges(changeLogs: any[]): Promise<boolean>;
    /**
     * Run mongo remove with search Criteria
     * @param tableName
     * @param searchCondition
     */
    doRemove(tableName: string, searchCondition?: any): Promise<import("mongodb").WriteOpResult>;
    /**
     * Execute an aggregate against a table
     * @param path
     * @param aggList
     */
    doAggregate(path: any, aggList: any): Promise<any[]>;
    /**
     * Get the collection for a given category
     * @param name collection name
     */
    doGetCollection(name: string): Promise<MongoModel>;
}
export default DataSetManager;
