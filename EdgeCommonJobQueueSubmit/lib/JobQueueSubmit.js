"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
class JobQueueSubmit {
    //  priorityType default is null or 'normal'
    //  priorityType options: 'High', 'Low', 'high', 'low'
    constructor(priorityType) {
        this.messageQueueItemUpdates = null;
        this.priorityType = priorityType;
    }
    //  Submit data to the Edge Server to be saved to the item
    doSubmitAppend(dataSet, path, newJson) {
        const str = JSON.stringify({
            type: 'append',
            ds: dataSet,
            path,
            data: newJson
        });
        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doSubmitInsert not connected');
        }
        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }
    //  Submit data to the Edge Server to be saved to the item
    doSubmitInsert(dataSet, path, data) {
        const str = JSON.stringify({
            type: 'insertdata',
            ds: dataSet,
            path,
            data
        });
        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doSubmitInsert not connected');
        }
        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }
    //  Submit data to the Edge Server to be saved to the item
    doDeletePath(dataSet, path) {
        const str = JSON.stringify({
            type: 'deletepath',
            ds: dataSet,
            path
        });
        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doDeletePath not connected');
        }
        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }
    //  Submit data to the Edge Server to be saved to the item
    doSubmitData(dataSet, path, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const str = JSON.stringify({
                type: 'savedata',
                ds: dataSet,
                path,
                data
            });
            if (this.messageQueueItemUpdates == null) {
                edgecommonconfig_1.default.status('JobQueueSubmit Warning, not already connected.');
                yield this.doInternalConnectItemUpdateMessageQueue();
                edgecommonconfig_1.default.status(`JobQueueSubmit Connected not, submitting ${path}`);
                return this.messageQueueItemUpdates.doSubmitBuffer(str);
            }
            return this.messageQueueItemUpdates.doSubmitBuffer(str);
        });
    }
    doInternalConnectItemUpdateMessageQueue() {
        if (this.internalConnectionPromise != null) {
            return this.internalConnectionPromise;
        }
        return this.internalConnectionPromise = new Promise((resolve, reject) => {
            edgecommonconfig_1.default.status('JobQueueSubmit doInternalConnectItemUpdateMessageQueue Connecting');
            let p = null;
            if ((this.priorityType === 'High') || (this.priorityType === 'high')) {
                p = edgecommonmessagequeue_1.default.doOpenMessageQueue(edgecommonconfig_1.default.mqItemUpdatesHigh);
            }
            else if ((this.priorityType === 'Low') || (this.priorityType === 'low')) {
                p = edgecommonmessagequeue_1.default.doOpenMessageQueue(edgecommonconfig_1.default.mqItemUpdatesLow);
            }
            else {
                p = edgecommonmessagequeue_1.default.doOpenMessageQueue(edgecommonconfig_1.default.mqItemUpdates);
            }
            p.then(mq => {
                this.messageQueueItemUpdates = mq;
                edgecommonconfig_1.default.status('JobQueueSubmit doInternalConnectItemUpdateMessageQueue Connected');
                resolve(this.messageQueueItemUpdates);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
exports.JobQueueSubmit = JobQueueSubmit;
exports.default = JobQueueSubmit;
//# sourceMappingURL=JobQueueSubmit.js.map