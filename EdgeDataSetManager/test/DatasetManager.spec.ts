const path     = require('path');
const jsonfile = require('jsonfile');

import DataSetManager from '../src/DataSetManager';
import DataSetConfig  from '../src/DataSetConfig';
import DataSetPath    from '../src/DataSetPath';
import { config }     from 'edgecommonconfig';
import { expect }     from 'chai';

config.setCredentials('MongoDB', {
    url    : `${process.env.DB_HOST}`,
    options: {
        connectTimeoutMS: 600000,
        poolSize        : 16,
        socketTimeoutMS : 600000,
    },
});
config.setCredentials('redisReadHost', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHostWQ', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHost', `${process.env.REDIS_HOST}`);

const NumberInt = (x: any) => {
    return x;
};

const ISODate = (x: any) => {
    return new Date(x);
};

describe('DataSetManager', () => {
    let dm: DataSetManager;
    let ds: DataSetManager;
    // Act before assertions
    before(async () => {
        ds = new DataSetManager('rets_raw');
        dm = new DataSetManager('geo_test');
    });


    it('import zipcodes should return success', async () => {
        let total: any                   = 0;
        let zipcodeData: { data: any[] } = jsonfile.readFileSync(path.join(__dirname, './data/zipcodes.json'));

        for (let rec of zipcodeData.data) {
            rec.lat = parseFloat(rec.lat);
            rec.lon = parseFloat(rec.lon);
            await dm.doUpdate(`/zipcodes/${rec.code}`, rec);
            total++;
        }
        total.should.be.eql(854);
    });

    it('doInternalGetSettings should return success', async () => {
        const result: any = await ds.doInternalGetSettings('raw');

        let obj = {
            'id'           : '19_Resi_45118505',
            'sys_id'       : '45118505',
            'server_id'    : 19,
            'class_name'   : 'Resi',
            'class_key'    : '19_Resi',
            'raw'          : {
                'Assumable'                       : 'No',
                'Baths Full'                      : 1,
                'Baths Half'                      : 0,
                'Baths Total'                     : 1,
                'Beds Total'                      : 3,
                'Buyer Agency Compensation'       : '3',
                'Buyer Agency Compensation Type'  : 'Percentage',
                'Buyer Agent Sale YN'             : false,
                'CDOM'                            : 1,
                'CDOM Date'                       : new Date('2017-07-30T22:53:26.899+0000'),
                'City'                            : 'Mount Holly',
                'Construction Type'               : 'Site Built',
                'County Or Parish'                : 'Gaston',
                'Current Price'                   : 99000,
                'Deed Reference'                  : '2323',
                'DOM'                             : 1,
                'DOM Date'                        : new Date('2017-07-30T22:53:26.899+0000'),
                'Driveway'                        : 'Concrete',
                'Elementary School'               : 'Unspecified',
                'Equipment'                       : ['Electric Range/Oven', 'Refrigerator'],
                'Exterior Construction'           : 'Brick Veneer Full',
                'Fireplace YN'                    : false,
                'Flooring'                        : ['Vinyl/Linoleum', 'Carpet'],
                'Foundation Details'              : 'Other',
                'Geocode Source'                  : 'Manual',
                'Heating'                         : 'Electric Hot Air',
                'High School'                     : 'Unspecified',
                'Latitude'                        : 35.313416,
                'Laundry Location'                : '1st Floor',
                'List Agent MUI'                  : 1996753,
                'List Agent Direct Work Phone'    : '704-998-9520',
                'List Agent Full Name'            : 'Kathryn Cook Crespo',
                'List Agent MLSID'                : '29253',
                'Listing Agent MLS Board'         : 'Charlotte Regional REALTOR Association Inc',
                'Listing Agent Primary Board'     : 'Charlotte Regional REALTOR Association Inc',
                'Listing Contract Date'           : new Date('2017-07-30T00:00:00.000+0000'),
                'Listing Financing'               : ['Cash', 'Conventional'],
                'Listing Service YN'              : true,
                'Listing Type'                    : 'Exclusive Right',
                'List Office MUI'                 : 1004594,
                'List Office MLSID'               : '7364',
                'List Office Name'                : 'Bottom Line Realty & Mgmt LLC',
                'List Office Phone'               : '704-379-7492',
                'List Price'                      : 99000,
                'Longitude'                       : -81.054882,
                'Lot Size Area'                   : 0.27,
                'Matrix Unique ID'                : 45118505,
                'Matrix Modified DT'              : new Date('2017-07-30T15:35:48.517+0000'),
                'Middle Or Junior School'         : 'Unspecified',
                'MLS Number'                      : '3306385',
                'Named Prospects Exempted YN'     : false,
                'New Construction YN'             : false,
                'Original Entry Timestamp'        : new Date('2017-07-30T15:35:25.530+0000'),
                'Original List Price'             : 99000,
                'Parcel Number'                   : '179484',
                'Parking'                         : 'Driveway',
                'Permit Address Internet YN'      : true,
                'Permit Internet YN'              : true,
                'Permit Syndication YN'           : true,
                'Photo Count'                     : 12,
                'Photo Modification Timestamp'    : new Date('2017-07-30T15:35:48.517+0000'),
                'Plat Reference Section Pages'    : '20',
                'Postal Code'                     : '28120',
                'Postal Code Plus 4'              : '9221',
                'Price Change Timestamp'          : new Date('2017-07-30T15:35:25.530+0000'),
                'Private Remarks'                 : 'Seller has just drilled a new well. Working with the city to get all copies of all the well certifications. House will be sold as is. Seller is not aware of any issues. We are in the process of repairing the front broken window glass',
                'Property Sub Type'               : '1 Story',
                'Property Type'                   : 'Single Family',
                'Publically Maintained Road'      : true,
                'Public Remarks'                  : 'This Charming 3 Bedrooms house has tons of Potential and Lots of Natural light. New Carpet and freshly painted - tasteful colors throughout!',
                'RATIO Current Price By Acre'     : 366666.67,
                'RATIO Current Price By SQFT'     : 103.77,
                'Room Count'                      : 1,
                'Sewer'                           : 'County Sewer',
                'Showing Instructions'            : ['Showing Service', 'Vacant'],
                'Showing Phone Number'            : '800-746-9464',
                'Special Listing Conditions'      : 'None',
                'Sq Ft Additional'                : 0,
                'Sq Ft Basement'                  : 0,
                'Sq Ft Lower'                     : 0,
                'Sq Ft Main'                      : 954,
                'Sq Ft Third'                     : 0,
                'Sq Ft Total'                     : 954,
                'Sq Ft Unheated Basement'         : 0,
                'Sq Ft Unheated Lower'            : 0,
                'Sq Ft Unheated Main'             : 0,
                'Sq Ft Unheated Third'            : 0,
                'Sq Ft Unheated Total'            : 0,
                'Sq Ft Unheated Upper'            : 0,
                'Sq Ft Upper'                     : 0,
                'State Or Province'               : 'North Carolina',
                'Status'                          : 'Active',
                'Status Change Timestamp'         : new Date('2017-07-30T15:35:25.530+0000'),
                'Status Contractual Search Date'  : new Date('2017-07-30T00:00:00.000+0000'),
                'Street Name'                     : 'Greentree',
                'Street Number'                   : '107',
                'Street Number Numeric'           : 107,
                'Street Suffix'                   : 'Drive',
                'Street View Param'               : '1$35.313416$-81.05488200000002$32.62$13.33$1.00$JZIpXvVQXZ_1NudbkwmDRg',
                'Sub Agency Compensation'         : '0',
                'Sub Agency Compensation Type'    : 'Dollar',
                'Subdivision Name'                : 'Dickson Heights',
                'Supplement Count'                : 1,
                'Supplement Modification Timestam': new Date('2017-07-30T15:08:11.757+0000'),
                'Table'                           : 'Listing - Residential',
                'Tax Legal Description'           : 'DICKSON HEIGHTS BLK A L 6',
                'Tax Location'                    : 'Mt Holly',
                'Variable Rate Commission YN'     : false,
                'VOWAVMYN'                        : true,
                'Water'                           : 'Well',
                'Year Built'                      : 1975,
                'Zoning Specification'            : 'R1',
            },
            '_lastModified': new Date('2017-07-31T22:56:45.570+0000'),
        };

        const updateResult = await ds.doUpdate('/raw/19_Resi_45118505', obj, null);
        console.log('Result of doUpdate=', updateResult);
    });

    it('doInternalGetSettings should return success', async () => {

        let obj = {
            'id'        : '59_RE_1_509498',
            'sys_id'    : '509498',
            'server_id' : NumberInt(59),
            'class_name': 'RE_1',
            'class_key' : '59_RE_1',
            'raw'       : {
                'System ID'                : NumberInt(509498),
                'Class'                    : 'RESIDENTIAL',
                'Property Type'            : 'Single Family Detached',
                'Area'                     : 'Covington North',
                'Search Price'             : NumberInt(111000),
                'Listing Price'            : NumberInt(109900),
                'Address Number'           : '3603',
                'Address Search Number'    : NumberInt(3603),
                'Address Street'           : 'Glenn',
                'City'                     : 'Covington',
                'State'                    : 'KY',
                'ZIP'                      : '41015',
                'Status Category'          : 'Sold',
                'Status Detail'            : '0',
                'Sale/Rent'                : 'For Sale',
                'Agent'                    : NumberInt(5529),
                'List Office'              : NumberInt(104),
                'Listing Date'             : ISODate('2017-08-21T00:00:00.000+0000'),
                'Original Price'           : NumberInt(109900),
                'Financing'                : 'Other',
                'Pending Date'             : ISODate('2017-08-21T00:00:00.000+0000'),
                'Selling Date'             : ISODate('2017-09-29T00:00:00.000+0000'),
                'Selling Price'            : NumberInt(111000),
                'Selling Agent'            : NumberInt(5529),
                'Selling Office'           : NumberInt(104),
                'HotSheet Date'            : ISODate('2017-10-02T00:00:00.000+0000'),
                'Status Date'              : ISODate('2017-10-02T00:00:00.000+0000'),
                'Price Change Date'        : ISODate('2017-10-02T00:00:00.000+0000'),
                'Entry Date'               : ISODate('2017-10-02T18:49:00.000+0000'),
                'Last Modified Date Time'  : ISODate('2017-10-02T19:27:00.000+0000'),
                'Photo Count'              : NumberInt(29),
                'PhotoTimestamp'           : ISODate('2017-10-02T18:49:00.000+0000'),
                'Inactive Date'            : ISODate('2017-08-21T00:00:00.000+0000'),
                'Attached Document Count'  : NumberInt(1),
                'MLS Number'               : '509498',
                'Street Designation'       : 'Avenue',
                'LVT Date'                 : ISODate('2017-10-02T00:00:00.000+0000'),
                'Address'                  : '3603 Glenn Avenue',
                'Status'                   : 'Sold',
                'DOM'                      : NumberInt(0),
                'DOM Date'                 : ISODate('2018-03-29T12:44:31.773+0000'),
                'DOMLS'                    : NumberInt(0),
                'Cumulative Days On Market': NumberInt(0),
                'New Construction'         : 'No',
                'Sub Type Description'     : 'Stick Built/Modular/Masonry',
                'Syndication Zillow Group' : 'No',
                'Property Sub Type'        : 'Single Family',
                'Appointment Center'       : '(859) 594-3999',
                'Bathrooms Display'        : '2 (2 0)',
                'Tax ID#'                  : '056-32-24-002.00',
                'Full Bathrooms'           : NumberInt(2),
                'Half Bathrooms'           : NumberInt(0),
                'Bedrooms'                 : NumberInt(3),
                '# Rooms'                  : NumberInt(7),
                '# Fireplaces'             : NumberInt(0),
                'Age'                      : NumberInt(127),
                'FIPS Code'                : NumberInt(21117),
                '# Garage Spaces'          : NumberInt(0),
                'Property Condition'       : 'No',
                'Multiple Listing Service' : 'Northern Kentucky Multiple Listi',
                'Master Bath 1 Level'      : NumberInt(1),
                'Entry Level'              : NumberInt(1),
                'Living Rm Level'          : NumberInt(1),
                'Dining Rm Level'          : NumberInt(1),
                'Kitchen Level'            : NumberInt(1),
                'Family Rm Level'          : NumberInt(1),
                'Laundry Rm Level'         : NumberInt(1),
                'Master Bedroom Level'     : NumberInt(2),
                'Bedroom2 Level'           : NumberInt(2),
                'Bedroom3 Level'           : NumberInt(2),
                'Bath 2 Level'             : NumberInt(2),
                'Agreement Type'           : 'Exclusive Right',
                'County'                   : 'Kenton',
                'Entry Dim'                : '16 x 6',
                'Living Room Dim'          : '14 x 13',
                'Dining Rm Dim'            : '19 x 12',
                'Kitchen Dim'              : '14 x 12',
                'Family Rm Dim'            : '14 x 14',
                'Laundry Rm Dim'           : '11 x 9',
                'Master Bedroom Dim'       : '18 x 13',
                'Bedroom2 Dim'             : '13 x 13',
                'Bedroom3 Dim'             : '12 x 10',
                'Owner Name'               : 'Stilz',
                'Suburb'                   : 'Covington',
                'Tax Rate'                 : 'of record',
                'Financing Comments'       : 'Seller paid $3000 in CC',
                'Lot Dimensions'           : 'of record',
                'MLS Origin'               : 'Nky',
                'Bathrooms'                : NumberInt(2),
                'Auction'                  : false,
                'REALTORcom'               : true,
                'HOA'                      : false,
                'On Market'                : false,
                'Family Room Y/N'          : false,
                'Formal Dining Rm Y/N'     : true,
                'Bedroom on Level 1'       : true,
                'Bathroom on Level 1'      : true,
                'Syndication-ListHub'      : true,
                'Estimated Selling Date'   : ISODate('2017-09-29T00:00:00.000+0000'),
                'Lot Square Footage'       : NumberInt(0),
                'SP % LP'                  : NumberInt(101),
                'Year Built'               : '1890',
                'SIC'                      : 'Covington',
                'Showing Instructions'     : 'Appt Icon on MLS',
                'Sub Agency'               : '0%',
                'Buyer Agency'             : '3%',
                'Transaction Broker'       : '0%',
                'School District'          : 'Covington Independen',
                'Elementary School'        : 'Latonia Elementary',
                'JHS/Middle School'        : 'Holmes Middle School',
                'High School'              : 'Holmes Senior High',
                'Degree Complete'          : 'Existing Structure',
                'Architecture'             : 'Traditional',
                'Basement Type'            : 'Full',
                'Cooling'                  : 'Central Air',
                'Dining Rm Feat'           : 'Fireplace',
                'Entry (Foyer)'            : 'Transom/Sidelit',
                'Exterior Finish'          : 'Vinyl',
                'Family Rm Feat'           : 'Bookcase',
                'Fireplace'                : 'Inoperable',
                'Foundation'               : 'Block',
                'Garage'                   : 'None',
                'Gas'                      : 'Natural',
                'Heating'                  : 'Gas',
                'Kitchen Features'         : 'Ceramic Tile',
                'Levels'                   : '1 Story',
                'Living Rm Feat'           : 'Fireplace',
                'Master Bath 1 Feat'       : 'Ceramic Tile',
                'MasterBed Feat'           : 'Skylight',
                'Occupancy'                : 'Negotiable',
                'Parking'                  : 'Off Street',
                'Property Ownership'       : 'Consumer',
                'Roof'                     : 'Shingle',
                'Sewer'                    : 'Private',
                'Water Source'             : 'Public',
                'Windows'                  : 'Insulated',
                'Marketing Remarks'        : 'Sold when submitted.',
                'Directions'               : '275 East to Exit #16 (Taylor Mill), Right on East Southern, Left on Glenn',
                'LA1Agent Logon Name'      : '444406724',
                'LA1Agent Type'            : 'Team',
                'LA1Agent First Name'      : 'The',
                'LA1Agent Last Name'       : 'Treas Team',
                'LA1AgentAddressStreetName': '60 Cavalier Blvd.',
                'LA1Agent City'            : 'Florence',
                'LA1Agent State'           : 'KY',
                'LA1Agent Zip'             : '41042',
                'LA1AgentPhone1Description': 'DIRCT',
                'LA1Agent Phone1 CountryId': 'United States (+1)',
                'LA1Agent Phone1 Number'   : '859-525-5708',
                'LA1Agent Phone2 CountryId': 'United States (+1)',
                'LA1Agent Phone2 Number'   : '859-801-1088',
                'LA1Agent Phone3 CountryId': 'United States (+1)',
                'LA1Agent Phone3 Number'   : '859-525-5788',
                'LA1AgentPhone4Description': 'OFC',
                'LA1Agent Phone4 CountryId': 'United States (+1)',
                'LA1Agent Phone4 Number'   : '859-525-7900',
                'LA1Agent Email'           : 'TreasTeam@huff.com',
                'LA1Agent Url'             : 'www.TreasTeam.com',
                'LA1Agent Status'          : 'MLS and BOARD',
                'LA1User Code'             : '444406724',
                'LA1Office MLS ID'         : '432500054',
                'LA1Full Name'             : 'Treas Team',
                'SA1Agent Type'            : 'Team',
                'SA1Agent First Name'      : 'The',
                'SA1Agent Last Name'       : 'Treas Team',
                'SA1AgentAddressStreetName': '60 Cavalier Blvd.',
                'SA1Agent City'            : 'Florence',
                'SA1Agent State'           : 'KY',
                'SA1Agent Zip'             : '41042',
                'SA1AgentPhone1Description': 'DIRCT',
                'SA1Agent Phone1 CountryId': 'United States (+1)',
                'SA1Agent Phone1 Number'   : '859-525-5708',
                'SA1Agent Phone2 CountryId': 'United States (+1)',
                'SA1Agent Phone2 Number'   : '859-801-1088',
                'SA1Agent Phone3 CountryId': 'United States (+1)',
                'SA1Agent Phone3 Number'   : '859-525-5788',
                'SA1AgentPhone4Description': 'OFC',
                'SA1Agent Phone4 CountryId': 'United States (+1)',
                'SA1Agent Phone4 Number'   : '859-525-7900',
                'SA1Agent Email'           : 'TreasTeam@huff.com',
                'SA1Agent Url'             : 'www.TreasTeam.com',
                'SA1Agent Status'          : 'MLS and BOARD',
                'SA1User Code'             : '444406724',
                'SA1Office MLS ID'         : '432500054',
                'SA1Full Name'             : 'Treas Team',
                'LO1Office Identifier'     : NumberInt(104),
                'LO1Main Office ID'        : NumberInt(835),
                'LO1Office Type'           : 'REALTOR Office',
                'LO1Office Abbreviation'   : '432500054',
                'LO1Office Name'           : 'Huff Realty - Florence',
                'LO1OffceAddressStreetName': '60 Cavalier Blvd.',
                'LO1Office City'           : 'Florence',
                'LO1Office State'          : 'KY',
                'LO1Office Zip'            : '41042',
                'LO1OfficePhone1Descriptin': 'Offc',
                'LO1OfficePhone1CountryId' : 'United States (+1)',
                'LO1Office Phone1 Number'  : '859-525-7900',
                'LO1Office Email'          : 'shuffteam@huff.com',
                'SO1Office Identifier'     : NumberInt(104),
                'SO1Main Office ID'        : NumberInt(835),
                'SO1Office Type'           : 'REALTOR Office',
                'SO1Office Abbreviation'   : '432500054',
                'SO1Office Name'           : 'Huff Realty - Florence',
                'SO1OffceAddressStreetName': '60 Cavalier Blvd.',
                'SO1Office City'           : 'Florence',
                'SO1Office State'          : 'KY',
                'SO1Office Zip'            : '41042',
                'SO1OfficePhone1Descriptin': 'Offc',
                'SO1OfficePhone1CountryId' : 'United States (+1)',
                'SO1Office Phone1 Number'  : '859-525-7900',
                'SO1Office Email'          : 'shuffteam@huff.com',
                'Publish to Internet'      : 'Yes',
                'Show Address on Internet' : 'Yes',
                'Allow Public Blogging on' : 'No',
                'Allow AVM on Internet'    : 'No',
                'Publish to IDX'           : 'Yes',
                'Doc Timestamp'            : ISODate('2017-10-02T19:27:00.000+0000'),
            },
        };

        const result = await ds.doUpdate('/raw/59_RE_1_509498', obj, null);
        console.log('Result of doUpdate=', result);
    });


    it('doAggregate: Simple projection test for zipcodes Collection', async () => {
        const aggList = [{
            '$match': {
                'state': 'MA',
            },
        }, {
            '$sort': {
                'id': -1,
            },
        }, {
            '$project': {
                'id'   : 1,
                'city' : 1,
                'state': 1,
                'code' : 1,
            },
        }, {
            '$limit': 100,
        }, {
            '$group': {
                _id  : '$city',
                count: {
                    '$sum': 1,
                },
            },
        }];

        const result = await  dm.doAggregate('/zipcodes', aggList);

        result.length.should.be.eql(86);
        result[0].should.have.property('_id');
        result[0].should.have.property('count');
    });

    it('Complex aggregation test for zipcodes Collection', async () => {
        let aggList = [{
            '$match': {
                '$or': [{ 'state': 'MA' }, { 'state': 'RI' }],
            },

        }, {
            '$match': {
                'id': {
                    '$gt': 1440,
                },
            },
        }, {
            '$project': {
                'id'  : 1,
                'city': 1,
            },
        }, {
            '$limit': 100,
        }, {
            '$group': {
                _id  : '$city',
                count: {
                    '$sum': 1,
                },
            },
        }, {
            '$sort': {
                'count': 1,
            },
        }];

        const result = await dm.doAggregate('/zipcodes', aggList);
        result.length.should.be.eql(75);
        result[0].should.have.property('_id');
        result[0].should.have.property('count');
        result[0].count.should.be.greaterThanOrEqual(1);
    });


    it('test with query \'country:HAMPSHIRE\' for zipcodes Collection', async () => {
        const count = await dm.doCount('/zipcodes/county:HAMPSHIRE');
        count.should.be.eql(29);
    });

    it('test with query \'state:RI\' for zipcodes Collection', async () => {
        const count = await dm.doCount('/zipcodes/state:RI');
        count.should.be.eql(91);
    });

    it('test with query \'id:1082\' for zipcodes Collection', async () => {
        const count = await dm.doCount('/zipcodes/id:1082');
        count.should.be.eql(1);
    });

    it('doStream',  () => {
        let path = new DataSetPath('/item/in:1,2,4,90,23');
        console.log('Path:', path.condition);

        path = new DataSetPath('/raw/in:19_Resi_24965681');


        let strList = ['19_Resi_24965681'].join(',');
        return ds.doStream(`/raw/in:${strList}`, {}, 0, null, (record: any) => {
            return console.log('Stream record:', record);
        }).then(function(result: any) {
            // console.log("Finished result:", result);
        }).catch(console.log);
    });


    it('Simple test for zipcodes Collection', async () => {
        const zipcodes = await dm.doGetItemsArray('/zipcodes/county:HAMPSHIRE');
        (typeof zipcodes as any).should.be.eql('object');

        for (const zipcode of zipcodes) {
            zipcode.should.have.property(["id", "code", "city", "state", "county", "area_code", "lat", "lon"]);
            zipcode.county.should.equal('HAMPSHIRE');
        }
    });

    it('add simple test record should success', async () => {
        const dm         = new DataSetManager('test_data');
        const testRecord = {
            name    : 'Brian Pollack',
            birthday: new Date('1975-10-02'),
            address : {
                number : 293,
                street : 'Montibello',
                zipcode: 28117,
                city   : 'Mooresville',
                state  : 'NV',
            },
        };

        const result = await dm.doUpdate('/people/brian', testRecord);
    });

    it('Simple test for people Collection', async () => {
        const dm         = new DataSetManager('test_data');
        const item = await dm.doGetItem('/people/brian');
        const stats = await dm.doGetUpdatedStats();
        stats.should.have.property('people');
        stats.should.have.property('change');
        (typeof stats.change as any).should.be.eql('number');
    });

    it('doDeletePath should remove records from collection', async () => {
        const dm         = new DataSetManager('test_data');
        await dm.doDeletePath('/people/brian');
        const item = await dm.doGetItem('/people/brian');
    });

    it('doInsert should add data', async () => {
        const dm         = new DataSetManager('test_data');
        const testRecord = {
            id      : 'qi',
            name    : 'Qi Wong',
            birthday: new Date('1990-01-05'),
            address : {
                number : 293,
                street : 'Montibello',
                zipcode: 28117,
                city   : 'Mooresville',
                state  : 'NV',
            },
            hobbies: [
                'reading'
            ]
        };

        const result = await dm.doInsert('/people/qi', testRecord);
    });

    it('doAppend should append data', async () => {
        const dm         = new DataSetManager('test_data');
        const appendData =  [
            'football',
            'drawing'
        ];

        const result = await dm.doAppend('/people/qi/hobbies', appendData);
    });

    it('doVerifySimpleIndex should verifiy index', async () => {
        const dm         = new DataSetManager('test_data');
        const result = await dm.doVerifySimpleIndex('people', 'name');
        // expect(result).to.equal('name_1');
    });

    it('doVerifySimpleIndex should verifiy index', async () => {
        const dm         = new DataSetManager('test_data');
        const result = await dm.doVerifySimpleIndex('people', 'name');
        // expect(result).to.equal('name_1');
    });
});
