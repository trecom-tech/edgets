# EdgeCommonStatusUpdateClient
> Send status messages from a program to the server for appearance on the dashboard.

    npm install --save git+ssh://git@gitlab.protovate.com:Edge/EdgeCommonStatusUpdateClient.git
    import StatusUpdateClient from 'edgecommonstatusupdateclient';

## Creating an instance

The server accepts certain types of status update messages but this client doesn't specifically care about that.  It will send whatever
type you decide to create.  

    const client = new StatusUpdateClient("name", "detaultType");

## Starting a job or process

    job = startJob(name, args, slot);

When you start a job you are setting the "name" for the current status update client.  If you don't specifiy another name in the sendStatus command then the startJob name will be used.  Slot is a unique id given the same "name".   Consider this a worker or thread ID number.

Sending an update is a simple call and whatever you send it timestamped and put into the queue for display on the dashboard.

    client.sendStatus(dataObject);

When the job is done you can call

    job.stop();

## Notice

TODO:  Convert the use of aqmp to EdgeCommonMessageQueue so it's not directly connecting to the message queue.
