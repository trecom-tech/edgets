from socketIO_client import SocketIO, LoggingNamespace
import time
import random

class EdgeApiClient():

	def setServer(self, host, port):
		if (host == "" or port == 0):
			print "The host or port is not correct. Please try again"
			return

		self.host = host
		self.port = port
		self.dynamicResolveList = {}

	def on_connect(self):
	    print 'Sever connected'

	def on_disconnect(self):
	    print 'Sever disconnect'

	def on_reconnect(self):
	    print 'Server reconnect'

	def on_list_response(self, args):
		if args['uuid'] in self.dynamicResolveList:
		    self.dynamicResolveList[args['uuid']](args['result'])
		else:
			print('Not Received correct data')

	def connect(self):
		self.socketIO = SocketIO(self.host, self.port, LoggingNamespace)
		self.socketIO.on('connect', self.on_connect)
		self.socketIO.on('disconnect', self.on_disconnect)
		self.socketIO.on('reconnect', self.on_reconnect)

	def get_list(self, callback):
		self.socketIO.on('api-list', callback)

	def make_call(self, callname, callback):
		if callname == "":
			print "The callname parameter must be valid. please input correct data!"
			return

		data = {
			"callName": callname,
			"uuid": self.get_uuid()
		}

		self.dynamicResolveList[data['uuid']] = callback

		self.socketIO.emit('cmd', data)
		self.socketIO.on('cmd-reply', self.on_list_response)
		self.socketIO.wait(seconds=2)

	def get_uuid(self):
		unixsec = int(time.time())
		random_number = int(random.random() * 100000)
		return str(unixsec) + '-' + str(random_number)