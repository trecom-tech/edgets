## Typical onExecute Data:

onExecute receives a changeRec which would look something like this:

    { 
        dataset     : 'test',
        collection  : 'test',
        change_date : 2019-04-20T16:07:58.540Z,
        kind        : 'insert',
        record_id   : '19_Resi_32902442',
        path        : '/raw/19_Resi_32902442/raw/Listing Agent Primary Board',
        stamp       : 2019-04-22T16:07:58.541Z 
    }

