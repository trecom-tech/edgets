import { DelayedWinstonLog } from 'edgecommonconfig';
import ApiGateway from './ApiGateway';
import ApiSocketUser from './ApiSocketUser';
import EdgeError from './EdgeError';
import { EdgeSocketResponseData } from 'edgeapi';
import { IPriorityType } from './interfaces/IPriorityType';
import IApiUser from './interfaces/IApiUser';
/**
 * This makes some class object into a provider of API calls.
 * It works by taking the prototype object and looking at all the function calls
 * and then making dynamic call references for them.
 *
 * All functions must start with "do"
 * All functions with Internal will be skipped
 */
export default class ApiProvider {
    [DynamicFunctionName: string]: any;
    _apiGatewayRef: ApiGateway;
    dummyPromise: Promise<boolean>;
    apiCallFunc: {
        [funcName: string]: any;
    };
    apiCallList: {
        [funcName: string]: any;
    };
    private redisPubsubQueue;
    protected authLogger: DelayedWinstonLog;
    constructor(thePrototype: ApiProvider);
    protected onInit(): void;
    getBaseName(): void;
    /**
     * Given a function call, return the argunments required
     * @param thePrototype
     * @param functionCall
     */
    getParamNames(thePrototype: ApiProvider, functionCall: string): any[];
    sendToChannel(channelName: string, priority: IPriorityType, data: any): void;
    /**
     * Look at the functions exposed by the API and create an
     * array to reference those functions so that the socket can
     * RPC call them.
     * @param thePrototype
     */
    determineApiCalls(thePrototype: ApiProvider): boolean;
    /**
     * Handle a call to the API
     * Exactly same as `doProcessApiCall` of ApiGateway except this one is runnable
     * directly from providers
     * @param callName - [string] function name provider_method
     * @param callData - [any]
     * @param callerSession - [ApiSocketUser]
     */
    doCall(callName: string, callData: any, callerSession: IApiUser): Promise<EdgeSocketResponseData>;
    /**
     * Shortcut function to make it easy for any provider to update data.
     * @param user The calling user for authentication and role verification
     * @param dataSet - [string] The dataset to update
     * @param path - [string] The complete path to update
     * @param newData - [string] The object values to replace within the stored data.
     */
    internalUpdatePath(user: ApiSocketUser, dataSet: string, path: string, newData: object): Promise<EdgeSocketResponseData>;
    /**
     * Converts something into a hash such that an ID can be generated.  Will always create the
     * same id from the same source content.
     * @param sourceObject Any object or string value which is used to produce a hash.
     */
    fastHash(sourceObject: any): string;
    /**
     * failedRoleCheck log failed role check error and normalize error response
     * @param user authenticating user
     * @param role required role
     * @param caller  caller method name
     */
    failedRoleCheck(user: ApiSocketUser, role: string, caller?: string): EdgeError;
}
