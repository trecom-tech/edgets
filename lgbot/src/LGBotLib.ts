import * as os      from 'os';
import * as fs      from 'fs';
import * as path    from 'path';
import * as numeral from 'numeral';
import { parse }    from 'shell-quote';
import { config }   from 'edgecommonconfig';
import EdgeApi      from 'edgeapi';
import AzureSync    from './AzureSync';

let GlobalSessionID = 10000;

export default class LGBotLib {

    api                         : any;
    argv                        : any;
    log                         : any;
    sessionId                   : number;
    reCommandPattern            : any[];

    constructor() {
        this.log              = config.getLogger('LGBotLib');

        // Assign a session to this caller
        this.sessionId        = ++GlobalSessionID;

        // Setup a table for commands
        this.reCommandPattern = [];
        this.reCommandPattern.push({ re: new RegExp(/help/, 'i'), cmd: this.returnHelpText });
        this.reCommandPattern.push({re: new RegExp(/^\?$/), cmd: this.returnHelpText });

        // Workqueue related functions
        this.reCommandPattern.push({ re: new RegExp(/^workq*u*e*s*/, 'i'), cmd: this.doShowWorkqueues });
        this.reCommandPattern.push({ re: new RegExp(/^wqeta/, 'i'), cmd: this.doShowWorkqueuesEta });
        this.reCommandPattern.push({ re: new RegExp(/^wq/, 'i'), cmd: this.doShowWorkqueues });

        // Make an API call
        this.reCommandPattern.push({ re: new RegExp(/call (.*)/, 'i'), cmd: this.doCallApi });

        // Get raw data from the database
        this.reCommandPattern.push({ re: new RegExp(/^get (.*)/, 'i'), cmd: this.doGetData, quotes: 1 });
        this.reCommandPattern.push({ re: new RegExp(/^load(.*| .*)/, 'i'), cmd: this.doLoadData, quotes: 1 });
        this.reCommandPattern.push({ re: new RegExp(/^export (.*)/, 'i'), cmd: this.doExportData, quotes: 1 });

        // Read / write to REDIS cache
        this.reCommandPattern.push({ re: new RegExp(/cacheget (.*)/, 'i'), cmd: this.doCacheGet });
        this.reCommandPattern.push({ re: new RegExp(/redis (.*)/, 'i'), cmd: this.doRedisRaw });

        // Azure file system
        this.reCommandPattern.push({ re: new RegExp(/logs ls(.*)/, 'i'), cmd: this.doLogsListFiles, quotes: 1 });
        this.reCommandPattern.push({ re: new RegExp(/logs get(.*)/, 'i'), cmd: this.doLogsGetFile, quotes: 1 });
    }

    /**
     * Returns a basic object ready for result data
     */
    private initResults() {
        return { type: 'text', error: false, data: null };
    }

    /**
     * Returns an error to the caller 
     */
    private setError(message: string) {
        config.status('Returning error:', message);
        return { type: 'text', error: message };
    }

    /**
     * Returns help text to the user
     */
    private returnHelpText() {
        const pathList = [ '.', os.homedir() + '/EdgeConfig/' ];
        const paths    = (require.main === module) ? require.main.paths : [];
        
        for (let p of paths) {
            pathList.push(`${p}/..`);
            pathList.push(`${p}/lgbot/`);
        }

        pathList.push(__dirname + '/../');
        
        const helpTextFile = config.FindFileInPath('help_text.txt', pathList);
        const helpText     = (!helpTextFile) ? 
                            'Unable to find help_text.txt' :
                            fs.readFileSync(helpTextFile).toString();
        const result:any   = this.initResults();        
        result.data        = helpText;

        return result;
    }

    private async doLogsListFiles(path: any) {
        path             = path         || '';
        path             = path.pattern || path;
        const azureShare = new AzureSync();
        const pathParts  = path.split('/');
        let filePattern  = null;

        if (pathParts.length === 1) {
            filePattern = '.';
            path = pathParts[0];
        } else {
            filePattern = pathParts.pop();
            path = pathParts.join('/');
        }

        // Init array of results with empty
        const output: any   = this.initResults();
        output.suggest      = 'table';
        output.type         = 'object';
        output.path         = path;
        output.data         = [];

        // Now filter out matching files
        const allFiles: any = await azureShare.listFiles(path);
        const fileRegexp    = new RegExp(filePattern, "i");

        for (let item of allFiles) {
            if (fileRegexp.test(item.name)) {
                output.data.push(item);
            }
        }

        return output;
    }

    private async doLogsGetFile(path: any) {
        const azureShare    = new AzureSync();
        const allFiles: any = await this.doLogsListFiles(path);
        const output: any   = this.initResults();
        output.suggest      = 'table';
        output.type         = 'object';
        output.path         = path;
        output.data         = [];

        // List all files in the given path
        for (let item of allFiles.data) {
            if (item.type === 'f') {
                output.data.push(item);
                await azureShare.downloadFile(allFiles.path, item.name, '.', item.name);
            }
        }

        return output;
    }

    private async doGetData(dataSet: string, path: string) {
        const start       = this.argv.start || 0;
        const limit       = this.argv.limit || 100;
        const fields: any = {};

        if (this.argv.field) {
            if (typeof this.argv.field === 'string') {
                fields[this.argv.field] = 1;
            } else {
                for (let name of this.argv.field) {
                    fields[name] = 1;
                }
            }
        }

        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        const all    = await this.api.data_doGetItems(dataSet, path, fields, {}, start, limit);
        const output = this.initResults();
        output.type  = 'array';
        output.data  = all;

        return output;
    }

    private async doLoadData(pathStr?: string) {
        const dataSet    = this.argv.dataset;
        const collection = this.argv.collection;

        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        const output: any = this.initResults();
        output.type       = 'array';
        output.data       = [];
        let files         = this.getJSONFiles(pathStr, dataSet, collection);
        
        for (let file of files) {
            try {
                let data            = JSON.parse(fs.readFileSync(file.path).toString());
                data.first_modified = data.first_modified || new Date();
                data.last_modified  = data.last_modified || new Date();
                data.active         = data.active || true;
                const id            = data.id || path.basename(file, path.extname(file));

                await this.api.data_doUpdatePath(file.dataSet, `/${file.collection}/${id}`, data);
                output.data.push(data);
            } catch (e) {
                console.log('doLoadData', e);
            }
        }

        return output;
    }

    private getJSONFiles(pathStr?: string, dataSet?: string, collection?: string) {
        let jsonFiles: any[] = [];
    
        if (!dataSet && collection) {
            throw new Error('Collection cannot be specified without Dataset');
        }
    
        if (pathStr) {
            pathStr = path.normalize(pathStr);

            if (dataSet) {
                if (collection) {
                    jsonFiles.push({
                        dataSet: dataSet, 
                        collection: collection, 
                        path: path.resolve(pathStr)
                    });
                } else {
                    const pathParts  = pathStr.split('/');
                    const collection = pathParts.shift();
                    
                    if (collection) {
                        const file  = pathParts.shift();
                        const files = (file) ? [file] : fs.readdirSync(path.resolve(collection));
        
                        process.chdir(collection);
                        for (let file of files) {
                            if (path.parse(file).ext === '.json') {
                                jsonFiles = jsonFiles.concat(this.getJSONFiles(file, dataSet, collection));                            
                            }
                        }
                        process.chdir('../');
                    }
                }
            } else {
                const pathParts = pathStr.split('/');
                const dataSet   = pathParts.shift();
                
                if (dataSet) {
                    const collection  = pathParts.shift();
                    const collections = (collection) ? [collection] : fs.readdirSync(path.resolve(dataSet));
                    
                    process.chdir(dataSet);
                    for (let collection of collections) {
                        if (fs.statSync(path.resolve(collection)).isDirectory()) {
                            jsonFiles = jsonFiles.concat(this.getJSONFiles(collection, dataSet));
                        }
                    }
                    process.chdir('../');
                }
            }
        } else {
            const dataSets = fs.readdirSync(process.cwd());
            for (let dataSet of dataSets) {
                if (fs.statSync(path.resolve(dataSet)).isDirectory()) {
                    jsonFiles = jsonFiles.concat(this.getJSONFiles(dataSet));
                }
            }
        }
        
        return jsonFiles;
    }

    private async doExportData(dataSet: string, collection: string) {
        if (!dataSet || dataSet.length === 0) {
            return this.setError('DataSet must be specified');
        }

        if (!collection || collection.length === 0) {
            return this.setError('Collection must be specified');
        }

        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        if (this.argv.path) {
            const pathStr = this.argv.path.replace('~', process.env.HOME);
            process.chdir(pathStr);
        }
        
        const documents      = await this.api.data_doGetAllItems(dataSet, `/${collection}/`);
        const dataSetPath    = path.resolve(dataSet);
        const collectionPath = path.resolve(dataSet, collection);
        const output: any    = this.initResults();
        output.type          = 'object';

        try {
            if (!fs.existsSync(dataSetPath)) {
                fs.mkdirSync(dataSetPath);
            }
    
            if (!fs.existsSync(collectionPath)) {
                fs.mkdirSync(collectionPath);
            }
            
            process.chdir(collectionPath);

            for (let document of documents) {
                fs.writeFileSync(`${document.id}.json`, JSON.stringify(document), 'utf8');
            }
            
            output.data  = true;
        } catch (e) {
            console.log('Cannot make directory or file', e);
            output.data = false;
        }

        return output;
    }

    private async doCallApi(callParamsString: string) {
        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        const callOptions  = parse(callParamsString);
        const providerName = callOptions.shift();
        const apiName      = callOptions.shift();
        const name1        = new RegExp(`${providerName}.*_${apiName}.*`, 'i');
        const name2        = new RegExp(`${providerName}.*_do${apiName}.*`, 'i');
        let found          = null;

        for (let callName in this.api) {
            if (typeof callName !== 'string') {
                continue;
            }

            if (callName === apiName) {
                found = callName;
                break;
            }

            if (name1.test(callName) || name2.test(callName)) {
                if (found) {
                    return this.setError(`Too many matches found, ${found} and ${callName}`);
                }

                found = callName;
            }
        }

        if (!found) {
            return this.setError(`No api call provider=${providerName} call=${apiName}`);
        }

        const output = this.initResults();
        output.type  = 'object';
        output.data  = await this.api[found].apply(this.api, callOptions);

        return output;
    }

    /**
     * Get the list of items in the workqueue and compare it over time.
     */
    private async doShowWorkqueuesEta() {
        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        const output: any = this.initResults();
        output.type       = 'object';
        const list1       = await this.api.workqueue_doGetWorkqueueList();

        await new Promise((resolve, reject) => {
            setTimeout(resolve, 1000 * 10);
        });
        
        const list2       = await this.api.workqueue_doGetWorkqueueList();

        output.data = list1.filter((a: any) => { return a.pending > 0; }).map((a: any) => {
            if (a.pending === 0) {
                return null;
            }

            for (let item of list2) {
                if (item.workqueue === a.workqueue) {
                    const result  = item;
                    const rate    = -1 * ((item.pending - a.pending) / 10);
                    const seconds = result.pending / rate;

                    if (seconds < 0) {
                        result.eta  = 'Increasing';
                    } else if (seconds > 60) {
                        result.eta  = numeral(seconds/60).format('#,###') + ' min.';
                        result.rate = numeral(rate).format('#,###.##') + '/sec';
                    } else {
                        result.eta  = numeral(seconds).format('#,###') + ' sec.';
                        result.rate = numeral(rate).format('#,###.##') + '/sec';
                    }

                    return result;
                }
            }
        });
        
        output.data.sort((a: any, b: any) => {
            return b.pending - a.pending;
        });

        return output;
    }

    /**const command
     * Get the list of items pending in work queues
     */
    private async doShowWorkqueues() {
        if (!this.api) {
            this.api = await EdgeApi.doGetApi();
        }

        const output:any = this.initResults();
        output.type      = 'object';
        output.data      = await this.api.workqueue_doGetWorkqueueList();

        output.data.sort((a: any, b: any) => {
            return b.pending - a.pending;
        });

        return output;
    }

    private doRedisRaw(command: any) {
        return new Promise((resolve) => {
            const commandParts = command.split(' ');
            let params         = null;

            if (commandParts.length === 0) {
                return this.setError('Invalid command');
            } else if (commandParts.length > 1) {
                command = commandParts.shift();
                params  = commandParts;
            }
            
            const conn  = EdgeApi.getRedisConnection();
            conn.send_command(command, params, (err, data) => {
                if (err) {
                    throw err;
                }

                const output:any = this.initResults();
                output.type      = 'object';
                output.data      = data;

                resolve(output);
            });
        });
    }

    /**
     * Get a value from the cache
     */
    private async doCacheGet(key: any) {
        const output:any = this.initResults();
        output.type      = 'object';
        output.data      = await EdgeApi.cacheGet(key);
        
        if (output.data && 
            typeof output.data === 'string' &&
            (output.data.charAt(0) === '{') || output.data.charAt(0) === ']') {
            
            try {
                output.data = JSON.parse(output.data);
            } catch (e) {
                console.log('doCacheGet', e);
            }
        }

        return output;
    }

    /**
     * This is called with the command to process.   The command
     * should return an object.   See "initResults" function.
     * New commands should / can return "suggest" object value as "table" or "json"
     * suggesting the best way to display the type of data being returned.
     */
    doProcess(text: string, argv: any) {
        this.argv = argv;
        config.status('LGBotLib doProcess:', text);

        return new Promise((resolve, reject) => {
            let foundOne = false;

            for (let option of this.reCommandPattern) {
                const re              = option.re;
                const commandFunction = option.cmd;
                let found:any         = text.match(re);
                
                if (found) {
                    // Found is an array of strings based on the regexp matching, for example
                    // /search (.*)
                    // [0] = search waterlynn
                    // [1] = waterlynn
                    // so remove the first element because it's the full string.
                    foundOne = true;
                    found.shift();
                    config.status(`LGBotLib doProcess found re ${re.toString()} :`, found);

                    if (option.quotes) {
                        found = parse(found[0]);
                    }

                    const result = commandFunction.apply(this, found);

                    if (result && result.then && typeof result.then === 'function') {
                        config.status('LGBotLib doProcess result is a promise, calling');

                        // Promise returned
                        result.then((finalResult: any) => {
                            config.status('LGBotLib doProcess finalResult=', finalResult);
                            resolve(finalResult);
                        });
                    } else {
                        config.status('LGBotLib doProcess result=', result);
                        resolve(result);
                    }

                    return;
                }
            }

            if (!foundOne) {
                resolve(this.setError(`Invalid command '${text}' try 'help'`));
            }
        });
    }

}