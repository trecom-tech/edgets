# Work Queues

> Work Queues are memory based list of records that need to be 
> processed.  It's like a message queue except that it's based in memory
> and it will remove duplicate records and only an ID is stored.

Add a record to a work queue

	workqueue_doAddRecord(workQueueName, record id)
	
Get the number of records in the queue by name

	workqueue_doGetCount(workQueueName)
	
Get one record that is pending, returns quickly and may 
return null if nothing is pending

	workqueue_doGetOnePending(workQueueName)
	
Get one record but wait and only return when a record is available.

	workqueue_doWaitForPending(workQueueName)

Get array of all known work queues and number of items pending per specific work queue
	
	workqueue_doGetWorkqueueList() 
