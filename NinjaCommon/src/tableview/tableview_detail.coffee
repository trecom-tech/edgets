## -------------------------------------------------------------------------------------------------------------
## class TableViewDetailed widget to display the table in vertical manner it mostly used to display single row
## including all data for the single row
##
## @extends [TableView]
##
class TableViewDetailed extends TableView

	# @property [Integer] leftWidth
	leftWidth : 100

	# @property [Integer] dataWidth - the smallest width for a column
	dataWidth : 120

	setLeftWidth: (@leftWidth)=>
		##|
		##|  Do we need to update cached values at all?

	constructor: (@elTableHolder, @showCheckboxes) ->
		super(@elTableHolder, @showCheckboxes)
		@showFilters      = true
		@fixedHeader      = true
		@showGroupPadding = false
		@showResize       = false

	getTableTotalRows: ()=>
		if !@colByNum? then return 0
		count = Object.keys(@colByNum).length

	getTableTotalCols: ()=>
		return @totalAvailableRows + 1

	##|  Number of visible columns
	getTableVisibleCols: ()=>

		if @cachedTotalVisibleCols? then return @cachedTotalVisibleCols

		visColCount = 0
		x           = 0
		colNum      = @offsetShowingLeft
		maxWidth    = @getTableVisibleWidth()
		totalCols   = @getTableTotalCols()

		while x < maxWidth and colNum < totalCols

			while (colNum < totalCols) and @shouldSkipCol(location)
				colNum++

			if colNum >= totalCols
				break

			location =
				colNum: colNum
				visibleCol: visColCount

			x = x + @getColWidth location
			visColCount++
			colNum++

		@cachedTotalVisibleCols = visColCount
		return visColCount

	getTableLastVisibleCols: ()=>

		if @cachedLastTotalVisibleCol? then return @cachedLastTotalVisibleCol

		visColCount = 0
		x           = 0
		maxWidth    = @getTableVisibleWidth()
		totalCols   = @getTableTotalCols()

		colNum      = totalCols - 1

		while x <= maxWidth and colNum >= 0

			while (colNum < totalCols) and @shouldSkipCol(location)
				colNum--

			location =
				colNum: colNum
				visibleCol: visColCount

			x = x + @getColWidth location
			visColCount++
			colNum--

		@cachedLastTotalVisibleCol = visColCount
		return visColCount

	getTableMaxVisibleCols: ()=>

		if @cachedMaxTotalVisibleCol? then return @cachedMaxTotalVisibleCol

		visColCount = 0
		x           = 0
		colNum      = @getTableTotalCols() - 1
		maxWidth    = @getTableVisibleWidth()

		#while x < maxWidth and colNum >= 0
		cn = 0
		while x < maxWidth and cn <= colNum
			location =
				colNum     : cn
				visibleCol : cn

			if (cn <= colNum) and @shouldSkipCol(location)
				cn++
				continue

			x = x + @getColWidth(location)
			visColCount++
			cn++

		if visColCount > 0 then @cachedMaxTotalVisibleCol = visColCount
		return visColCount

	getColWidth: (location)=>
		if @showHeaders and location.visibleCol == 0 then return @leftWidth
		if @totalAvailableRows == location.visibleCol
			return @getTableVisibleWidth() - @leftWidth - (@dataWidth * (@totalAvailableRows-1))
		return @dataWidth

	getCellStriped: (location)=>
		if @showHeaders and location.visibleCol == 0 then return false
		return location.visibleRow % 2 == 1

	##|
	##|  Return true if a cell is editable
	##|
	getCellEditable: (location)=>
		if !@colByNum[location.rowNum]? then return null
		return @colByNum[location.rowNum].getEditable()
	##|
	##|  Return right/left/center - left is assumed by default
	getCellAlign: (location)=>
		if !@colByNum[location.rowNum]? then return null
		if location.visibleCol == 0 then return 'right'
		return 'left'
		# return @colByNum[location.rowNum].getAlign()

	getCellTablename: (location)=>
		if !@colByNum[location.rowNum]? then return null
		return @primaryTableName

	getCellSource: (location)=>
		if !@colByNum[location.rowNum]? then return null
		return @colByNum[location.rowNum].getSource()

	getCellRecordID: (location)=>
		if !@rowDataRaw[location.colNum]? then return 0
		return @rowDataRaw[location.colNum].id

	getCellFormatterName: (location)=>
		if !@colByNum[location.rowNum]? then return null
		return @colByNum[location.rowNum].getFormatterName()

	shouldSkipRow: (rowNum)=>
		if !@colByNum[location.rowNum]? then return true
		return false

	shouldSkipCol: (colNum)=>
		if !@rowDataRaw[location.colNum]? then return false
		if @rowDataRaw[location.colNum].visible? and @rowDataRaw[location.colNum].visible == false then return true
		return false

	isHeaderCell: (location)=>
		if @showHeaders and location.visibleCol == 0 then return true
		if @showFilters and location.visibleRow == 0 then return true
		return false

	shouldAdvanceCol: (location)=>
		if @showHeaders and location.visibleCol == 1 then return false
		return true

	##|
	##|  Returns a state record for the current row
	##|  data - Cells of data
	##|  locked - Cells of header or locked content
	##|  group - Starting a new group
	##|  skip - Skip this row
	##|  invalid - Invalid row
	##|
	getRowType: (location)=>
		if @isHeaderCell(location) then return "locked"
		if !@colByNum[location.rowNum]? then return "invalid"
		if @colByNum[location.rowNum].shouldSkip is true then return "skip"
		return "data"

	getColumnType: (colName) =>
		if colName is "header" then return 1
		for actionCol in @actionColList
			if actionCol.getSource() is colName
				return 2
		if @rowDataRaw[colName]? then return 1
		return 0

	##|
	##|  Return true if a cell is editable
	##|
	getCellEditable: (location)=>
		if !@colByNum[location.rowNum]? then return false
		return @colByNum[location.rowNum].getEditable()

	getCellClickable: (location)=>
		if @colByNum[location.rowNum]?
			# console.log "Checking clickable:", location.colNum, " =",@colByNum[location.colNum].getClickable()
			return @colByNum[location.rowNum].getClickable()
		return false		

	setHeaderField: (location)=>
		if location.visibleRow is 0 and @showFilters
			@setHeaderFilterField location
		else
			location.cell.html ""
			if !@colByNum[location.rowNum]? then return false
			@colByNum[location.rowNum].RenderHeaderHorizontal location.cell, location
			location.cell.setDataPath "/#{location.tableName}/Header/#{location.sourceName}"

	setHeaderFilterField: (location)=>
		if location.colNum == 0 and location.visibleCol == 0
			value = "header"
		else 
			value = location.colNum

		path = "/#{location.tableName}/Filter/#{value}"
		if location.cell.children.length == 0
			location.cell.addClass "dataFilterWrapper"
			location.cell.add "div", "dataFilterIcon"
			location.cell.add "input", "dataFilter"
			location.cell.children[0].bind "click", @showComparators
			location.cell.children[1].bind "keyup", @onFilterKeypress
			location.cell.children[0].addClass "default"

		if location.sourceName is "row_selected" or location.sourceName is "row_flagged"
			location.cell.children[0].setDataPath path
			location.cell.children[1].setDataPath path

		else if location.cell.children[0].getDataValue("path") isnt path
			location.cell.children[0].setDataPath path
			location.cell.children[1].setDataPath path


			filterWidth = if location.maxWidth - location.x < @getColWidth(location) then location.maxWidth - location.x else @getColWidth(location)
			filterWidth = if filterWidth > 24 then filterWidth - 24 else 0
			location.cell.children[1].move 24, 0, filterWidth, location.rowHeight
		
		@setFilterFunction location

		true		

	getCellSelected: (location)=>
		if @rowDataRaw[location.colNum]? and @rowDataRaw[location.colNum].row_selected
			return true

		return false

	getCellType: (location)=>
		if @isHeaderCell(location) then return "locked"
		if not location.colNum? or !@rowDataRaw[location.colNum]?
			console.log "detail return invalid 1", location.colNum
			return "invalid"
		if !@rowDataRaw[location.colNum]?
			console.log "detail return invalid 2"
			return "invalid"
		if @rowDataRaw[location.colNum].type? then return @rowDataRaw[location.colNum].type
		return "data"

	setDataField: (location)=>

		col = @colByNum[location.rowNum]
		if col.getSource() == "row_selected"
			if @getRowSelected(@rowDataRaw[location.colNum].id)
				location.cell.html @imgChecked
			else
				location.cell.html @imgNotChecked

		else if col.render?
			location.cell.html col.render(@rowDataRaw[location.colNum][col.getSource()], @rowDataRaw[location.colNum])
		else
			displayValue = DataMap.getDataFieldFormatted @primaryTableName, @rowDataRaw[location.colNum].id, col.getSource()
			location.cell.html displayValue

		true

	applyFilters: () =>
		filters = []
		strJavascript = ""
		for tableName, fieldList of @currentFilters
			for fieldNumber, filterValue of fieldList
				if !filterValue.comparatorFunc or !filterValue.value or filterValue.value is '' then continue
				if fieldNumber is "header"
					strJavascript += "value = col.getName();\n"
				else
					strJavascript += "value = DataMap.getDataFieldFormatted(this.primaryTableName, this.rowDataRaw[#{fieldNumber}].id, col.getSource());\n"
				field = "colData"
				strJavascript += "if (typeof(value) == 'undefined') return false;\n"
				strJavascript += "if (value == null) return false;\n"
				strJavascript += "if ((#{filterValue.comparatorFunc})(value, '#{filterValue.value}') == false) return false;\n"
		strJavascript += "return true;\n"
		@filterFunction = new Function("col", strJavascript)
		true

	##
	## initialize/setup a filter for a location
	setFilterFunction: (location) =>
		dataPath   = location.cell.children[0].getDataValue("path")
		if !dataPath then return false

		parts      = dataPath.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnNum  = parts[3]

		el0 = location.cell.children[0].getTag()
		el1 = location.cell.children[1].getTag()
		el0.removeClass("default")

		if @getColumnType(columnNum) isnt 1
			#console.log "Filter on ActionColumn : Not working"
			return false
		
		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}
		
		if !@currentFilters[tableName][columnNum]?
			@currentFilters[tableName][columnNum] = {}

		if !@currentFilters[tableName][columnNum]['comparator']? 
			if columnNum is "row_selected" or columnNum is "row_flagged"
				@currentFilters[tableName][columnNum]['comparator'] = "none"	
			else 
				@currentFilters[tableName][columnNum]['comparator'] = "=~"

		filter = @currentFilters[tableName][columnNum]		
		filter['comparatorFunc'] = @createComparator filter['comparator']

		if columnNum is "row_flagged"
			if /row_flagged/.test(filter['comparator'])
				flagName        = filter['comparator'].replace "row_flagged_", ""
				filter['value'] = flagName
				icon            = @flagsColumn[@getFlagIndex(flagName)].icon
				el0.html "<i class='#{icon}'></i>"
			else
				el0.html ""
				filter['value'] = "none"
			el1.html ""

		else if columnNum is "row_selected"
			## empty content of cell of the filter's comparator
			el0.text ""
			el1.html ""
			filter['value'] = "none"

		else 
			el0.text filter['comparator']

			if filter['value']?
				el1.html filter['value']
			else
				el1.html ""

		return true	

	updateColumnList: () =>

		@colList  = []
		@colByNum = {}

		##|
		##|  Add a checkbox column if needed
		if @showCheckboxes
			c = new TableViewColCheckbox(@primaryTableName)
			@colList.push c

		##|
		##|  Find the columns for the specific table name
		columns = DataMap.getColumnsFromTable(@primaryTableName, @columnReduceFunction)
		columns = columns.sort (a, b)->
			return a.getOrder() - b.getOrder()

		for col in columns
			## Apply filters on column
			if !@filterFunction(col) then col.shouldSkip = true else col.shouldSkip = false

			if not col.getVisible() then continue

			foundInActionCol = false
			for acol in @actionColList
				if acol.getSource() == col.getSource()
					foundInActionCol = true
					break

			if foundInActionCol then continue

			if @isColumnEmpty(col)
				# console.log "Column is empty:", col.getSource()
				continue

			# if found == 0
			# console.log "LIST #{col.getSource()} to order: #{col.getOrder()}"
			@colList.push(col)

		##|
		##|  reset available columns and sort them
		total = 0
		sortedColList = @colList.sort (a, b)->
			return a.getOrder() - b.getOrder()

		for col in sortedColList

			foundInGroup = false
			for source in @currentGroups
				if source == col.getSource()
					col.isGrouped = true
					foundInGroup = true
					break

			if foundInGroup
				continue
			else
				col.isGrouped = false

			# console.log "colByNum[#{total}] = ", col.getName()
			@colByNum[total] = col
			total++

		## added by xgao
		## to show sorting arrow on ActionColumn header
		for acol in @actionColList
			acol.sort = 0
			if acol.constructor.name is "Column"
				for sortrule in @sortRules
					if sortrule.tableName is @primaryTableName and sortrule.source is acol.getSource()
						acol.sort = sortrule.state
				@colByNum[total] = acol
				total++

		return true

	## -----------------------------------------------------------------------------------------------ƒmove--------------
	## intenal event key press in a filter field, that executes during the filter text box keypress event
	##
	## @event onFilterKeypress
	## @param [Event] e jquery event keypress object
	## @return [Boolean]
	##
	onFilterKeypress: (e) =>

		parts      = e.path.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnNum  = parts[3]
		
		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}
		
		if !@currentFilters[tableName][columnNum]?
			@currentFilters[tableName][columnNum] = {}			

		#@currentFilters[tableName][columnNum]['value'] = $(e.target).val()
		# console.log "Current filter:", @currentFilters
		@updateFilter tableName, columnNum, 'value', $(e.target).val()

		return true

	## -gao
	## function to get count of Locked(header, filter) rows
	getTableLockedRows: () =>
		cnt = 0
		if @showFilters then cnt++
		if @actionColList? then cnt += @actionColList.length
		cnt
	
	## -gao
	## function to get count of Locked(action) columns
	getTableLockedColsLeft: () =>
		cnt = 0
		if @showHeaders then cnt++
		cnt

	getTableLockedColsRight: () =>
		return 0

	##
	## Calculate top margin of table content(aka. table data cells)
	getContentTopMargin: () =>
		marginTop = @headerCellHeight
		if @hasTitle() then marginTop += @titleHeight
		marginTop