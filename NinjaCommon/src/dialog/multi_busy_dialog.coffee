## -------------------------------------------------------------------------------------------------------------
## class BusyDialog to show the processing block ui
##

class BusyStateM
    text : ""
    min  : 0
    max  : 100
    pos  : 0

class MultiBusyDialog

    # @property [String] content the content to show on the popup
    content:       "Processing please wait"

    # @property [Array] status array of progress bars
    states: []

    # @property [Array] percentage array of progress bars
    lastPercent: []

    # @property [Array] busy array of progress bars
    busyStack: []

    ## -------------------------------------------------------------------------------------------------------------
    ## constructor
    ##
    constructor:  (title) ->

        # @property [String] template the template to use in the BusyDialog
        @template = Handlebars.compile '''
        <div class="hidex" id="pleaseWaitDialogM">
            <div class="modal-body">
                <h4 id='pleaseWaitDialogMTitle'>{{content}}</h4>
            </div>
        </div>
        '''

        # @property [String] pleaseWaitHolder the html for the holder
        @pleaseWaitHolder = $("body").append @template(this)

        # @property [JQueryElement] modal the currently showing modal
        @modal = $("#pleaseWaitDialogM")

        # @property [JQueryElement] elTitle the element in which the dialog is rendered
        @elTitle        = $("#pleaseWaitDialogMTitle")
        @elTitle.html title

        @modal.hide()

    ## -------------------------------------------------------------------------------------------------------------
    ## function to make the processing finish
    ##
    finished: (fid) =>
        @busyStack[fid] = null

        completed = true
        for busy in @busyStack
            if busy?
                completed = false
                break
        if completed
            window.setTimeout ()=>
                @modal.remove()
                ev = new CustomEvent("busydialog_finished", { detail: { fid: fid }})
                window.dispatchEvent ev
            , 1000
            return true
        else
            return false

    updatePercent: (fid, newPos = null)=>
        if newPos? then @states[fid].pos = newPos

        elProgressBar  = $("#busyProgressBar#{fid}")
        elProgressDiv  = @modal.find("#progress#{fid}")

        if @states[fid].pos == 0 and @states[fid].max == 0
            elProgressDiv.hide()
            elProgressBar.attr("aria-valuenow", 0).css("width", 0)
        else
            percent = Math.floor((@states[fid].pos / @states[fid].max) * 100) + "%"
            if percent == @lastPercent[fid] then return

            elProgressDiv.show()

            if @states[fid].pos+1 == @states[fid].max then percent = "100%"

            elProgressBar.css("width", percent)
            elProgressBar.attr
                "aria-valuenow" : @states[fid].pos
                "aria-valuemin" : @states[fid].min
                "aria-valuemax" : @states[fid].max
        true

    ## -------------------------------------------------------------------------------------------------------------
    ## function to add the busy progressbar
    ##
    ## @param [int] progressbar id
    ## @param [String] strText text to show on the bar
    ##
    addBusy: (fid, strText) =>
        bgs = Array("", "bg-success", "bg-info", "bg-warning", "bg-danger")
        progressbar = Handlebars.compile '<div id="progress'+fid+'" class="progress" style="display: none;"><div id="busyProgressBar'+fid+'" class="progress-bar  progress-bar-striped active '+bgs[fid%5]+'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">'+strText+'</div></div>'
        $('#pleaseWaitDialogM .modal-body').append progressbar(this)

        @states[fid] = new BusyStateM()
        @states[fid].text = strText

        @lastPercent[fid] = -1

        @busyStack[fid] = @states[fid]
        @updatePercent(fid, 0)

        @show()

    ## -------------------------------------------------------------------------------------------------------------
    ## function to show the modal at proper position
    ##
    ## @param [Object] options options to handle behaviour
    ##
    show: () =>

        w = $(window).width()
        h = $(window).height()

        otop = $(window).scrollTop()

        mw = @modal.width()
        mh = @modal.height()
        if mh > 300
            mh = 300
            @modal.css
                "height": mh
                "overflow-y": "scroll"

        # console.log "Window (#{w} x #{h}) offset (#{otop}) modal (#{mw} x #{mh})"

        left = (w-mw)/2
        top  = (h-mh)/2

        @modal.show()
        @modal.css
            position : "fixed"
            left     : left
            top      : top

    ##
    ## Remove modal immediately
    hide: () =>
        if !@modal? then return false
        @modal.remove()
        true
