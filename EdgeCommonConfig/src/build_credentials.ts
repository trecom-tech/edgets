import * as yargs  from 'yargs';
import config      from './Config';
import { encrypt } from "./Encrypter";
import * as fs     from 'fs';
import * as path   from 'path';

const argv = yargs.usage('Usage: $0 [options]')
    .example('$0 --in ./prod_credentials/ --out ~/EdgeConfig/', 'Encrypt the credentials and put them to output folder')
    .alias('o', 'out')
    .nargs('o', 1)
    .describe('o', 'Output folder')
    .alias('i', 'in')
    .nargs('i', 1)
    .describe('i', 'Input folder')
    .demandOption(['i', 'o'])
    .help('h')
    .alias('h', 'help')
    .argv;

async function processBuildCredentials() {
    const inputPath: string  = argv.in as string;
    const outputPath: string = argv.out as string;

    const key = config.getKey();
    if (!key) {
        throw new Error('Can not find key');
    }

    const files = fs.readdirSync(inputPath);
    for (const fileName of files) {
        if (path.extname(fileName) === '.json') {
            const fileInputPath = path.join(inputPath, fileName);
            const fileContent = fs.readFileSync(fileInputPath);
            const encryptedContent: string = encrypt(fileContent.toString(), key);

            const fileOutputPath = path.join(outputPath, fileName);
            fs.writeFileSync(fileOutputPath, encryptedContent);
        }
    }
}

processBuildCredentials()
    .then((result: any) => {
        console.log(result);
    })
    .catch((err: Error) => {
        console.log(err);
    })