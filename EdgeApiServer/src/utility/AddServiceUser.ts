import * as passwordUtil  from './Password';
import { DataSetManager } from 'edgedatasetmanager';

export const DEFAULT_SERVICE_USER = {
    apiUser: 'edgeApi',
    apiPassword: 'Hr^YQ5yh!2eM;QbV'
};

export async function addServiceUser(args: any = DEFAULT_SERVICE_USER) {

    const serviceGroup = {
        id: 'EdgeApiGroup',
        name: 'EdgeApiGroup',
        description: 'Test Group for Authenticating serviceUsers',
        roles: {
            test: [
                'serviceUser'
            ],
            global: [
                'serviceUser'
            ],
        },
        members: [
            args.apiUser,
        ],
    };

    const serviceUser = {
        id: args.apiUser,
        password: passwordUtil.generatePasswordHash(args.apiPassword),
        name: 'Service User',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'serviceuser@protovate.com',
        roles: {
            test: [
                'serviceUser'
            ],
            global: [
                'serviceUser'
            ],
        }
    };

    const dsLookup = new DataSetManager('lookup');
    const groupCollection = await dsLookup.doGetCollection("groups");
    const existingGroup = await groupCollection.doFindByID(serviceGroup.id);

    if (existingGroup) {
        // Dont add if group already exists, justlog that user already exists;
        console.log(`Group(${serviceGroup.id}) already exists`);
    } else {
        await groupCollection.doUpsertOne(serviceGroup.id, serviceGroup);
    }

    const dsMaster = new DataSetManager('master');
    const userCollection = await dsMaster.doGetCollection("user");
    const existingUser = await userCollection.doFindByID(args.apiUser);

    if (existingUser) {
        // Dont add if user already exists, justlog that user already exists;
        console.log(`User(${args.apiUser}) already exists`);
    } else {
        await userCollection.doUpsertOne(args.apiUser, serviceUser);
    }

    return true;
}
