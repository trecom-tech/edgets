import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
export default class ProviderOS extends ApiProvider {
    serverDetail: any;
    private db;
    private internalJobPromise;
    constructor(prototype: ApiProvider);
    onInit(): void;
    getBaseName(): string;
    doGetServerInformation(): Promise<any>;
    doExecuteJob(user: ApiSocketUser, job_id: string): Promise<unknown>;
    doCreateDailyJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string): Promise<unknown>;
    doCreateHourlyJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string): Promise<unknown>;
    doCreateOnetimeJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string): Promise<unknown>;
    doCreateJob(title: string, freqType: string, freqRate: number, workingFolder: string, scriptName: string, commandLineArgs: string, jobOwner: string, npm?: any): Promise<unknown>;
    doInternalGetJobCollection(): Promise<any>;
}
