"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const slug = require('slug');
const fs = require("fs");
const os = require("os");
const crypto = require("crypto");
const edgecommontablebase_1 = require("edgecommontablebase");
const edgecommonconfig_1 = require("edgecommonconfig");
const DynamicProvider_1 = require("./DynamicProvider");
// This is a model object that wraps a code module and stores/loads from
// the database.   The data values are stored in @data as with all TableBase
class DynamicCodeModel extends edgecommontablebase_1.TableBase {
    // -- Properties --
    // data.category = The category for this module
    // data.name     = The name of the module
    // data.code     = The coffeescript code for the mobile
    // -- Generated data --
    // data.js           = The compiled javascript
    // data.compiles     = True/False if the current version compiles correctly
    // data.version      = Date/Time of the last compile
    // data.hash         = The current sha1 hash to know if the code changed
    // data.compile_host = The hostname that compiled this version.
    // data.branch       = The branch level for this version of the code, "dev" and "prod" currently.
    // data.status       = The overall status, currently "unpublished", "published"
    constructor(id) {
        super('lookup', 'code', id);
        this.id = id;
        this.dirty = false;
        this.doSave = super.doSave;
    }
    setBranch(newBranch) {
        this.data.branch = newBranch;
        return this.data.id = slug(this.data.category + '_' + this.data.name + '_' + this.data.branch).toLowerCase();
    }
    setCategory(newCategory) {
        this.data.category = newCategory;
        return this.data.id = slug(this.data.category + '_' + this.data.name + '_' + this.data.branch).toLowerCase();
    }
    setName(newName) {
        this.data.name = newName;
        return this.data.id = slug(this.data.category + '_' + this.data.name + '_' + this.data.branch).toLowerCase();
    }
    // Set the coffeescript for this function
    // also compiles down to Javascript
    // @return {boolean} true if the code changed, false if it did not.
    setCoffeescriptCode(newCode) {
        if (this.data.code !== newCode) {
            this.data.code = newCode;
            this.internalCompile();
            return true;
        }
        return false;
    }
    setNotes(newNotes) {
        if (this.data.notes !== newNotes) {
            this.data.notes = newNotes;
            this.dirty = true;
            this.save();
        }
        return true;
    }
    setStatus(newStatus) {
        return this.data.status = newStatus;
    }
    //  Add a test case where options is all possible parameters
    addTestCase(options) {
        if (this.data.testCases == null) {
            this.data.testCases = [];
        }
        this.data.testCases.push(options);
        this.dirty = true;
        this.save();
        return true;
    }
    removeTestCase(id) {
        if (this.data.testCases == null) {
            this.data.testCases = [];
        }
        this.data.testCases.splice(id, 1);
        this.dirty = true;
        this.save();
        return true;
    }
    updateTestCase(id, options) {
        if (this.data.testCases == null) {
            this.data.testCases = [];
        }
        if (this.data.testCases[id] == null) {
            return false;
        }
        for (let varname in options) {
            let value = options[varname];
            this.data.testCases[id][varname] = value;
        }
        this.dirty = true;
        this.save();
        return true;
    }
    // Returns the dynamic code provider fully setup with code loaded
    getProvider() {
        if (this.provider != null) {
            return this.provider;
        }
        this.internalCompile();
        return this.provider;
    }
    // Update once the coffeescript code has changed to recompile the module
    internalCompile() {
        this.provider = new DynamicProvider_1.DynamicProvider();
        try {
            if (this.data.code == null) {
                this.data.code = '';
            }
            this.data.js = this.provider.doCompileCoffeeToCode(this.data.code);
            this.provider.createFunctionFromCode(this.data.js);
        }
        catch (e) {
            console.log('internalCompile error, code=', this.data.code, 'error=', e);
            this.data.error = e.toString();
        }
        this.data.meta = this.provider.serialize();
        this.data.compiles = true;
        this.data.version = new Date();
        if (this.data.category == null) {
            this.data.category = '';
        }
        if (this.data.name == null) {
            this.data.name = '';
        }
        if (this.data.code == null) {
            this.data.code = '';
        }
        // Compute a hash of the current code
        let recordHash = crypto.createHash('sha1');
        recordHash.update(this.data.category);
        recordHash.update(this.data.name);
        recordHash.update(this.data.code);
        let hash = recordHash.digest('hex');
        // If the hash changed, mark as dirty so it saves
        if (this.dirty === false && hash !== this.data.hash) {
            this.setStatus('unpublished');
            this.dirty = true;
        }
        this.data.compile_host = os.hostname();
        return true;
    }
    // If needed, save a copy to the database
    save() {
        let fullPath = edgecommonconfig_1.config.getDataPath(`code/${this.data.id}.coffee`);
        try {
            // For backup, save a local copy
            let wstream = fs.createWriteStream(fullPath);
            wstream.write(this.data.code);
            wstream.end();
        }
        catch (e) {
            console.log(`Error saving local copy of ${this.data.name} code module to ${fullPath}`);
        }
        if (!this.dirty) {
            return edgecommonconfig_1.config.status('DynamicCodeModel save:  hash has not changed, not saving.');
        }
        else {
            return this.doSave();
        }
    }
}
exports.default = DynamicCodeModel;
;
//# sourceMappingURL=DynamicCodeModel.js.map