$ ->

    ##|
    ##|  This is just for diagnostics,  you don't need to verify the data map is
    ##|  loaded normally.  The data types should be loaded upon startup.
    addTest "Confirm Zipcodes datatype loaded", () -> 
        dm = DataMap.getDataMap()
        if !dm? then return false

        zipcodes = dm.types["zipcode"]
        if !zipcodes? then return false
        if !zipcodes.col["code"]? then return false

        true

    ##|
    ##|  Load the zipcodes JSON file.
    ##|  This will insert the zipcodes into the global data map.
    addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

    addTestButton "Basic Combo Box", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            true
        true

    addTestButton "Combo Box: set placeholder", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            setTimeout () ->
                view.setPlaceHolder "New placeholder..."
            , 3000
        true

    addTestButton "Combo Box: set value", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            setTimeout () ->
                view.setValue "00501"
            , 3000
        true

    addTestButton "Combo Box: change width", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setCollectionName "zipcode"
            view.setWidth 300
            setTimeout () ->
                view.setWidth 500
            , 3000
        true

    addTestButton "Combo Box: Update renderers", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            setTimeout () ->
                view.setItemRenderer (item, escape) ->
                    return "<div>By new item renderer: #{item.code} #{item.city}</div>"
                view.setOptionRenderer (item, escape) ->
                    return "<div><div>By new option renderer: #{item.id}</div><div>#{item.code}</div><div>#{item.city}</div></div>"
            , 5000
            true
        true

    addTestButton "Combo Box: set filter function", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            view.setFilterer (row) ->
                row.city is "Manchester"
            true
        true

    addTestButton "Combo Box: event listeners", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            setTimeout () ->
                view.setCollectionName "zipcode"
            , 2000
            view.on "change_value", (value, row) ->
                alert "Combo value changed: id = #{row.id}, city = #{row.city} ", 
                true
            view.on "data_load", (newData) ->
                alert "Combo new data loaded: count = #{newData.length} "
                true
            true
        true


    addTestButton "Combo Box: table new data", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            setTimeout () ->
                DataMap.addData "zipcode", "99999",
                    area_code: "11111"
                    city: "New city"
                    code: "99999"
                    county: "New county"
                    id: "99999"
                    lat: "42.970084"
                    lon: "-71.405283"
                    state: "NS"
            , 3000
            true
        true

    addTestButton "ComboBox on Popup", "Open", () ->
        doPopupView "ComboBox", "ComboBox on Popup", null, 600, 800, (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            true
        true

    addTestButton "Combo Box: serialize/deserialize", "Open", ()->
        addHolder().setView "ComboBox", (view)->
            view.setWidth 300
            view.setCollectionName "zipcode"
            view.setPlaceHolder "New placeholder..."
            view.setFilterer (row) ->
                row.city is "Manchester"
            view.setItemRenderer (item, escape) ->
                return "<div>By new item renderer: #{item.code} #{item.city}</div>"
            view.setOptionRenderer (item, escape) ->
                return "<div><div>By new option renderer: #{item.id}</div><div>#{item.code}</div><div>#{item.city}</div></div>"
            view.setValue "00501"

            data = view.serialize()
            console.log "Combobox serialized data: ", data
            setTimeout () ->
                doPopupView "ComboBox", "ComboBox Deserialized", null, 800, 600, (view)->
                    view.deserialize data
                    true
            , 3000
            true
        true

    go()
