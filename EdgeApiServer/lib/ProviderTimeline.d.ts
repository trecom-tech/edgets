import DataSetManager from 'edgedatasetmanager';
import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
export default class ProviderTimeline extends ApiProvider {
    dsComms: DataSetManager;
    constructor(prototype: ApiProvider);
    onInit(): void;
    getBaseName(): string;
    /**
     * Create/Upate a new record in comms/timeline db with
     * Hashed string of {participant_id, category, eventStamp}
     * @param participant_id - [string] participant_id
     * @param category - [string] category
     * @param subcategory - [string] subcategory
     * @param text - [string] text
     * @param eventStamp - [any] eventStamp
     * @param eventOffset - [any] eventOffset
     * @param options - [any] option
     * @returns id of added/updated timelined db id
     */
    doAddTimeline(user: ApiSocketUser, participant_id: string, category: string, subcategory: string, text: string, eventStamp: any, eventOffset: any, options: any): Promise<string>;
    /**
     * Get all timeline items of that specific participant.
     * @param user - [ApiSocketUser]
     * @param participant_id - [string]
     * @returns participant's timeline documents.
     */
    doGetTimeline(user: ApiSocketUser, participant_id: string): Promise<any[]>;
}
