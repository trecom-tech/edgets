"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonrequest_1 = require("edgecommonrequest");
const edgeapi_1 = require("edgeapi");
const AWSElasticsearch = require('aws-es');
const Elasticsearch = require('elasticsearch');
const LRU = require('lru-cache');
class EdgeElastic {
    /**
     * Create an instance of EdgeElastic
     */
    constructor() {
        const credentials = edgecommonconfig_1.config.getCredentials('elasticsearch');
        if (credentials.type === 'es') {
            this.client = new Elasticsearch.Client(credentials);
        }
        else {
            this.client = new AWSElasticsearch(credentials);
        }
        this.base = `${credentials.host}/`;
        this.log = edgecommonconfig_1.config.getLogger('Elastic');
        // Setup Cache
        const cacheOptions = {
            max: 100,
            maxAge: 1000 * 60 * 5,
        };
        this.cache = new LRU(cacheOptions);
    }
    /**
     * @returns singleton for the elastic search
     */
    static getElastic() {
        if (EdgeElastic.singleInstance === null) {
            EdgeElastic.singleInstance = new EdgeElastic();
        }
        return EdgeElastic.singleInstance;
    }
    /**
     * get mappings for index/type
     * @param params {index, type}
     */
    doGetMapping(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { index, type } = params;
            let link = this.base + index + '/_mappings/' + type;
            let request = new edgecommonrequest_1.default();
            const output = yield request.doGetLink(link, {});
            edgecommonconfig_1.config.dump('output', output);
            return output;
        });
    }
    /**
     * delete index
     * @param params {index}
     */
    doDeleteIndex(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const output = yield this.client.indices.delete({
                index: params.index,
            });
            return output;
        });
    }
    /**
     * retrives all indices
     */
    doGetIndexes() {
        return __awaiter(this, void 0, void 0, function* () {
            const link = this.base + '_cat/indices';
            const request = new edgecommonrequest_1.default();
            const output = yield request.doGetLink(link, { v: true });
            return output;
        });
    }
    /**
     * update dataset index, collection type with id and body (doc)
     * @param options {dataset, collection, id, doc}
     */
    doUpdate(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, id, doc } = options;
            edgecommonconfig_1.config.status(`EdgeElastic doUpdate Start on ${dataset}:${collection}/${id}`);
            const result = yield this.client.index({
                index: dataset,
                type: collection,
                id,
                body: doc,
            });
            edgecommonconfig_1.config.dump('EdgeElastic doUpdate result:', result);
            return result;
        });
    }
    /**
     * get cached dataset index, colllection type
     * @param options {dataset: index, colleciton: type, body: search options}
     */
    doGet(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, body } = options;
            let searchOptions = {
                index: dataset,
                type: collection,
            };
            for (let varName in body) {
                searchOptions[varName] = body[varName];
            }
            try {
                if (edgecommonconfig_1.config.traceEnabled) {
                    edgecommonconfig_1.config.status(`EdgeElastic doSearch dataset=${dataset} collection=${collection} starting`);
                    edgecommonconfig_1.config.dump('Search Options', searchOptions);
                }
                const data = yield this.client.get(searchOptions);
                edgecommonconfig_1.config.status('EdgeElastic doSearch Done');
                return data;
            }
            catch (err) {
                this.log.error('doSearch Error', {
                    searchOptions,
                    err,
                });
                edgecommonconfig_1.config.status('EdgeElastic doSearch Done with errors');
                console.log(err);
                throw err;
            }
        });
    }
    /**
     * Search for collection
     * @param params {dataset, collection, query, sourceFilter, limit}
     */
    doSearch(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, query = '', sourceFilter = [], limit = 100, } = params;
            return new Promise((resolve, reject) => {
                let searchOptions = {
                    index: dataset,
                    type: collection,
                    _source: sourceFilter,
                    q: query,
                    scroll: '10s',
                    size: limit,
                };
                if (edgecommonconfig_1.config.traceEnabled) {
                    edgecommonconfig_1.config.status(`EdgeElastic doSearch dataset=${dataset} collection=${collection} starting`);
                    edgecommonconfig_1.config.dump('Search Options', searchOptions);
                }
                let allResults = null;
                const collectResultsFunction = (err, data) => {
                    if (err != null) {
                        console.log('doSearch error:', err);
                        this.log.error('doSearch Error', {
                            searchOptions,
                            err,
                        });
                        edgecommonconfig_1.config.status('EdgeElastic doSearch Done with errors');
                        reject(err);
                    }
                    else {
                        edgecommonconfig_1.config.status('EdgeElastic doSearch Done');
                        if (allResults == null) {
                            allResults = data;
                        }
                        else {
                            for (let result of Array.from(data.hits.hits)) {
                                allResults.hits.hits.push(result);
                            }
                        }
                        edgecommonconfig_1.config.status(`EdgeElastic we have ${allResults.hits.hits.length} of ${data.hits.total}`);
                        if (data.hits.total > allResults.hits.hits.length &&
                            allResults.hits.hits.length < limit) {
                            return this.client.scroll({
                                scrollId: data._scroll_id,
                                scroll: '10s',
                            }, collectResultsFunction);
                        }
                        else {
                            return resolve(allResults);
                        }
                    }
                };
                this.client.search(searchOptions, collectResultsFunction);
            });
        });
    }
    /**
     * set index dataset index with id and doc.
     * @param params {dataset, collection, id, doc}
     */
    doIndex(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, id, doc } = params;
            try {
                const recordHash = edgeapi_1.default.getHash(doc);
                const recordKey = `ELASTIC${dataset}_${collection}_${id}`;
                const cacheHash = this.cache.get(recordKey);
                if (cacheHash != null && cacheHash === recordHash) {
                    edgecommonconfig_1.config.status(`EdgeElastic doIndex hash match on ${recordKey}`);
                    return true;
                }
                edgecommonconfig_1.config.status(`EdgeElastic doIndex Start on ${dataset}:${collection}/${id}`);
                this.log.info('Submitted', {
                    dataset,
                    collection,
                    id,
                    url: `${this.base}${dataset}/${collection}/${id}`,
                });
                const result = yield this.client.index({
                    index: dataset,
                    type: collection,
                    id,
                    body: doc,
                });
                this.log.info('doIndex', {
                    result,
                    dataset,
                    collection,
                    id,
                });
                this.cache.set(recordKey, recordHash);
                return result;
            }
            catch (err) {
                console.log(err);
                this.log.error('doIndex', {
                    err,
                    dataset,
                    collection,
                    id,
                    doc,
                });
                throw err;
            }
        });
    }
    /**
     * Returns the hash values as stored in Elastic
     * @param params {dataset, collection ,id}
     */
    doGetDocumentHash(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, id } = params;
            let key = dataset + '_' + collection + '_' + id;
            const result = yield this.client.get({
                index: 'dochash',
                type: 'path',
                id: key,
            });
            edgecommonconfig_1.config.dump('getDocResult', { result });
            return result;
        });
    }
    setDocumentHash(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const { dataset, collection, id, doc, subpath } = params;
            try {
                const key = dataset + '_' + collection + '_' + id;
                const hash = edgeapi_1.default.getHash(doc);
                let hashes = {};
                hashes[subpath] = hash;
                const result = yield this.client.index({
                    index: 'dochash',
                    type: 'path',
                    id: key,
                    // doc_as_upsert : true
                    body: {
                        _lastUpdate: new Date(),
                        _updateRequired: true,
                        hashes,
                    },
                });
                return result;
            }
            catch (err) {
                this.log.err('setDocumentHash', {
                    err,
                    dataset,
                    collection,
                    id,
                    doc,
                });
                throw err;
            }
        });
    }
}
EdgeElastic.singleInstance = null;
exports.default = EdgeElastic;
//# sourceMappingURL=EdgeElastic.js.map