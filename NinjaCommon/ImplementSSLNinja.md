CoffeeNinja - Implement SSL with letsencrypt
--------------------------------------------

This doc guide you to implement CoffeeNinja SSL(HTTPS) with certbot(letsencrypt's SSL manager).

1. Install certbot(letsencrypt's SSL manager)

Follow the install instructions for certbot (letsencrypt's SSL manager) on https://certbot.eff.org/, making sure to select the correct server OS version.
```
    $ sudo apt-get update

    $ sudo apt-get install software-properties-common

    $ sudo add-apt-repository ppa:certbot/certbot

    $ sudo apt-get update

    $ sudo apt-get install certbot
```

2. Generate certification keys with letsencrypt

Before generate keys, you should confirm 80 and 443 port are free. Please stop apache or nginx or etc servers that use these ports.
Then,
```
    certbot certonly --standalone -d domain-name
```
- `certonly` is the command we are running, telling certbot to generate a letsencrypt SSL manually.
- `standalone` is mode to obtain a certificate if you don’t want to use (or don’t currently have) existing server software. For node engine, we choose this mode.
- `d` sets the domain you which to assign this SSL to, multiple domains can be registered at the same time by passing additional -d flags after each domain

After running the certonly command you should be prompted for an email address, used to notify you of upcoming SSL's due to expire and upon entering your email the process should complete with a success message similar to:
```
    IMPORTANT NOTES:
     - Congratulations! Your certificate and chain have been saved at
       /etc/letsencrypt/live/domain-name/fullchain.pem.
       Your cert will expire on 2017-03-01. To obtain a new or tweaked
       version of this certificate in the future, simply run certbot-auto
       again. To non-interactively renew *all* of your certificates, run
       "certbot-auto renew"
```

Then you can find certification key files in /etc/letsencrypt/live/domain-name/ directory.
There is `cert.pem`, `chain.pem`, `fullchain.pem`, `README` files.

3. Implement SSL code in project

Below is the SSL code implementation in koa framework.

<code>

    # server/NinjaWebServer.coffee

    const fs      = require('fs');
    const path    = require('path');
    const https   = require('https');
    const http    = require('http');
    const koa     = require('koa');

    const app = koa();

    const config = {
        domain: 'localhost',    // This is domain-name certificated by key
        http: {
            port: 9000,         // HTTP Port
        },
        https: {
            port: 8443          // HTTPS Port
            options: {
                key: fs.readFileSync(path.resolve(process.cwd(), 'certs/privkey.pem'), 'utf8').toString(),
                cert: fs.readFileSync(path.resolve(process.cwd(), 'certs/fullchain.pem'), 'utf8').toString(),
            }
        }
     }

    const serverCallback = app.callback();

    # HTTP Server
    const httpServer = http.createServer(serverCallback);
    httpServer.listen(config.http.port, (err: Error) => {
        if err?
            console.error "HTTP server FAIL: ", err, (err && err.stack)
        else
            console.log "HTTP  server OK: http://#{config.domain}:#{config.http.port}"
    });

    # HTTPS Server
    httpsServer = https.createServer(config.https.options, serverCallback);
    httpsServer.listen(config.https.port, (err: Error) => {
        if err?
            console.log "HTTPS server FAIL", err, (err && err.stack)
        else
            console.log "HTTPS server ok: https://#{config.domain}:#{config.https.port}"
    });
</code>

And copy */etc/letsencrypt/live/domain-name directory* to *server/certs* directory in project.

Please run `npm run start` in terminal, visit https://domain-name:8443


For more details, you can visit http://letsencrypt.readthedocs.io/en/latest/using.htm document.