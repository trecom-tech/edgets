import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
export default class ProviderData extends ApiProvider {
    private dataStore;
    private log;
    constructor(prototype: ApiProvider);
    /**
     * Get invoked when provider is added to gateway
     */
    onInit(): void;
    /**
     * Returns base name. {BaseName}_{MemberName}
     */
    getBaseName(): string;
    /**
     * Send a message to a channel
     *
     * @param channelName
     * @param message
     */
    doSendChannelMessage(channelName: string, message: any): void;
    doTestFormAuth(user: ApiSocketUser): import("./EdgeError").EdgeError | "Found";
    /**
     * Dump formdat and returns true
     * @param user - [ApiSocketUser] ApiSocketUser instance who is calling the endpoint.
     * @param formData - [any] JSON parameter
     */
    doTestForm(user: ApiSocketUser, formData: any): boolean;
    /**
     * Write obj into logs/filename
     * @param filename - [string] FileName to write obj.
     * @param obj - [any] JSON to write to file.
     */
    doRecordFile(filename: string, obj: any): boolean;
    /**
     * Returns cached DataSetManager instance
     * @param datasetName - [string] Dataset Name
     * @internal
     */
    private internalGetDataset;
    /**
     * Save `newData` to `path` of `datasetName`
     * @param datasetName - [string] Dataset Name
     * @param path - [string] Path string to save newData
     * @param newData - [any] JSON object to save
     * @returns difference of new and old data or true when there's no reference.
     */
    internalDoSaveData(datasetName: string, path: string, newData: any): Promise<boolean | any[]>;
    /**
     * Create or verify an index on a collection
     * @param dataSet - [string] Data Set Name
     * @param tableName - [string] Collection Name
     * @param keyField - [string] Field name to index
     * @param indexType - [string] Index type: mongo field
     * @param isUnique - [string] Uniqueness to provide mongo indexing function
     */
    doVerifySimpleIndex(dataSet: string, tableName: string, keyField: string, indexType: string, isUnique: boolean): Promise<void>;
    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param path - [mixed] Can be a compiled path or string path
     * @param centerLocation - [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles - [number] the number of miles to search around
     * @param offset - [number] Optional starting point in the result list (default 0)
     * @param limit - [number] Optional the maximum number of results (default 1000)
     */
    doFindNearby(dataSet: string, path: string, centerLocation: any, miles: any, offset: number, limit: number): Promise<any>;
    /**
     * DoFindNearbyIds - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * Difference to doFindNearby: it runs on mongo aggregation
     * @param path - [mixed] Can be a compiled path or string path
     * @param centerLocation - [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles - [number] the number of miles to search around
     * @param offset - [number] Optional starting point in the result list (default 0)
     * @param limit - [number] Optional the maximum number of results (default 1000)
     */
    doFindNearbyIds(dataSet: string, path: string, centerLocation: any, miles: any, offset: number, limit: number): Promise<any[]>;
    /**
     * Do call doGetItems with limit 1000000
     * @param dataSet - [string] Data Set Name
     * @param path - [string] path to collect
     */
    doGetAllItems(dataSet: string, path: string): Promise<any[]>;
    /**
     * Do search on `dataset` within `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param dataSet - [string] dataset name
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortOrder - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetItems(dataSet: string, path: string, fieldList: object, sortOrder: any, offset: number, limit: number): Promise<any[]>;
    /**
     * Same as doGetItems but stream the data
     * Do search on `dataset` within `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param user - [ApiSocketUser] socket user instance
     * @param dataSet - [string] dataset name
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortOrder - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetItemsStream(user: ApiSocketUser, dataSet: string, path: string, fieldList: any, sortOrder: string, offset: number, limit: number): Promise<string>;
    /**
     * Returns 1 item for path inside dataset
     * @param dataSet - [string] datasetname
     * @param path - [string] path string
     * @returns cached data in path
     */
    doGetItem(dataSet: string, path: string): Promise<any>;
    /**
     * Lookup items within IdList
     * @param user - [ApiSocketUser]
     * @param dataSet - [string]
     * @param collection - [string]
     * @param idList - [hash|ObjectId]
     */
    doGetItemsIfNeeded(user: ApiSocketUser | any, dataSet: string, collection: string, idList: any): Promise<unknown>;
    /**
     * Lookup a record within a dataset
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @param currentHash - [string] hash string
     * @returns cached data unless data is not updated
     */
    doGetItemIfNewer(dataSet: string, path: string, currentHash: string): Promise<unknown>;
    /**
     * Execute an aggregate. Excute doAggregate of DataSetManager with aggregation object
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @param aggList - [object] mongo aggregation object
     */
    doAggregate(dataSet: string, path: string, aggList: any): Promise<any[]>;
    /**
     * Delete collection found by path inside dataset
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @returns true whatever the case is
     */
    doDeletePath(dataSet: string, path: string): Promise<boolean>;
    /**
     * Act exactly like doUpate. Keeping just not to break old client
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePathHigh(dataSet: string, path: string, newData: any): Promise<boolean | any[]>;
    /**
     * Update data in path of dataset with newData
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePath(dataSet: string, path: string, newData: object): Promise<boolean | any[]>;
    /**
     * Act exactly like doUpate. Keeping just not to break old client
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePathLow(dataSet: string, path: string, newData: any): Promise<boolean | any[]>;
    /**
     * Run mongo remove command
     * @param dataSet - [string]
     * @param tableName - [string]
     * @param searchCondition - [any] condition to delete
     */
    doRemove(dataSet: string, tableName: string, searchCondition: any): Promise<import("mongodb").WriteOpResult>;
    /**
     * Get a list of tables and counts open
     * @param dataSet
     */
    doGetUpdatedStats(dataSet: string): Promise<any>;
    /**
     * Basically select path, count(*) group by path
     * @param dataSet - [string]
     * @param path - [string]
     * @param matchCondition - [any]
     */
    doGetUniqueValues(dataSet: string, path: string, matchCondition: any): Promise<any[]>;
    /**
     * Given a path and some json data, append the data to the path
     * treating the path like an array. if the path is not an array then
     * we find the next id and add it that way.
     * @param dataSet - [string]
     * @param path - [string]
     * @param newData - [any] object
     */
    doAppendPath(dataSet: string, path: string, newData: any): Promise<{
        ok: any;
        nModified: any;
        n: any;
    } | {
        kind: string;
        path: any;
        lhs: string;
        rhs: any;
    }[]>;
    /**
     * Api for calculating count for given path
     * @param dataSet - [string]
     * @param path - [string]
     */
    doCount(dataSet: string, path: string): Promise<number>;
}
