import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';
import ApiSocketUser from '../src/ApiSocketUser';

describe('ProviderUtility', () => {

    let api: EdgeApi;
    let apiSocketUser: ApiSocketUser;
    const testUser = {
        id: 'tao',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'admin@protovate.com',
        partner_id: 1,
        roles: {
            test: [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
            global: [
                'serviceUser',
            ]
        },
        attrib: {
            lunch: 'Pizza',
        },
    };

    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
        apiSocketUser = new ApiSocketUser(1, api.socket);

        // Setup test user;
        await api.data_doUpdatePath('master', `/user/${testUser.id}`, testUser);

        // Authenticate User
        await api.auth_doAuthUser('tao', 'pass', 'test');
    });

    it('doGetGuid', async () => {
        const result = await api.utility_doGetGuid();
        expect(result).to.be.a('string');
    });

    it('doGetServerStamp', async () => {
        const result = await api.utility_doGetServerStamp();
        expect(result).to.be.an('object');
        expect(result).to.have.property('timestamp')
    });

    it('doGetNextSequential', async () => {
        const result = await api.utility_doGetNextSequential('test');
        expect(result).to.be.a('number');
        const idForTest = await api.utility_doGetNextSequential('test');
        expect(result + 1).to.equal(idForTest);

        // try to call doGetNextSequential 100 times at once
        const allPromises = [];
        for (let i = 0; i < 100; i = i + 1) {
            allPromises.push(api.utility_doGetNextSequential('test'));
        }

        const allResult = await Promise.all(allPromises);
        const uniqueResult = allResult.filter((v, i, a) => a.indexOf(v) === i);
        expect(uniqueResult.length).to.equal(allResult.length);
    });

    it('doGetNextNewObject', async () => {
        const result = await api.utility_doGetNextNewObject('test', 'test');
        expect(result).to.be.an('object');
    });

});