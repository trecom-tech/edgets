//
// class for decimal data type
//
// @extends [DataFormatterType] data formatted data
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

const numeral = require('numeral');
const common  = new DataFormatCommon();

export class DataFormatFloat extends DataFormatterType {
    name: string        = 'decimal';
    width: number       = 100;
    input_width: number = 100;
    align: string       = 'right';

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if ((data == null)) {
            return "";
        }
        if ((options != null) && /#/.test(options)) {
            return numeral(common.getNumber(data)).format(options);
        } else {
            return numeral(common.getNumber(data)).format("#,###.######");
        }
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        return common.getNumber(data);
    }

    attachEditor(elSourceInput: any, path: any) {
        // #
        // # set type of this input as "number"
        return elSourceInput.css("text-align", "right");
    }

    //
    // funtion to allow specific key code
    //
    // @param [Integer] keyCode keyCode to allow
    // @return [Boolean]
    //
    allowKey(keyCode: number) {
        return true;

        if ((keyCode >= 48) && (keyCode <= 57)) {
            return true;
        }
        if ((keyCode >= 96) && (keyCode <= 105)) {
            return true;
        }
        if (keyCode === 190) {
            return true;
        }
        if (keyCode === 189) {
            return true;
        }
        if (keyCode === 119) {
            return true;
        }
        if (keyCode === 109) {
            return true;
        }
        // console.log "Rejecting key:", keyCode
        return false;
    }
}

export default DataFormatFloat;