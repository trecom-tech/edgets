import EdgeApi       from 'edgeapi';

describe('ProviderTimeline', () => {

    let api: EdgeApi;

    const testUser = {
        id: 'roy',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8',
        name: 'Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'roy@protovate.com',
        partner_id: 999,
        roles: {
            'global': [
                'serviceUser'
            ],
        },
    };

    before(async () => {
        api = new EdgeApi();
        await api.doConnect();

        // Removing all data before tests
        await api.data_doDeletePath('master', `/user/${testUser.id}`);
        await api.data_doRemove('comms', 'timeline');
        
        await api.data_doUpdatePath('master', `/user/${testUser.id}`, testUser);

        await api.auth_doAuthUser('roy', 'pass', 'global');
    });
    
    describe('timeline_doAddTimeline', () => {
        it('should return error without participant id', async () => {
            const participant_id = '';
            const category = 'ActionTaken';
            const subcategory = 'MobileActivity';
            const text = 'Participant opened application';
            const eventStamp = new Date("2019-03-31 14:23:10 UTC");
            const eventOffset = -4;
            const options = {
                ip_address: "10.1.1.19",
                device_type: "Apple iPhone",
            };

            const result = await api.timeline_doAddTimeline(participant_id, category, subcategory, text, eventStamp, eventOffset, options);
            
            result.should.have.property('e');
            result.e.should.match(/participant_id cannot be empty/);
        });

        it('should create with first participant id roy', async () => {
            const participant_id = 'roy';
            const category = 'ActionTaken';
            const subcategory = 'MobileActivity';
            const text = 'Participant opened application';
            const eventStamp = new Date("2019-03-31 14:23:10 UTC");
            const eventOffset = -4;
            const options = {
                ip_address: "10.1.1.19",
                device_type: "Apple iPhone",
            };

            const result = await api.timeline_doAddTimeline(participant_id, category, subcategory, text, eventStamp, eventOffset, options);
            result.should.have.length(40);
        });


        it('should update with first participant id roy', async () => {
            const participant_id = 'roy';
            const category = 'ActionTaken';
            const subcategory = 'MobileActivity';
            const text = 'Participant opened application';
            const eventStamp = new Date("2019-03-31 14:23:10 UTC");
            const eventOffset = -4;
            const options = {
                ip_address: "10.1.1.19",
                // Note this has changed
                device_type: "Android",
            };

            const result = await api.timeline_doAddTimeline(participant_id, category, subcategory, text, eventStamp, eventOffset, options);
            result.should.have.length(40);
        });
    });

    describe('timeline_doGetTimeline', () => {

        it('should return the results', async () => {
            const participant_id = 'roy';
            
            const result = await api.timeline_doGetTimeline(participant_id);
            result.should.have.length(2);
            result[0].should.have.property('participant_id', 'roy');
        });
    });

});
