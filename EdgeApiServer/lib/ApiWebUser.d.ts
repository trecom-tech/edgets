import IApiUser from './interfaces/IApiUser';
import { IUser } from './interfaces/IUser';
export default class ApiWebUser extends IApiUser {
    myConnectionID: number;
    userIpAddress: string;
    request: any;
    myName: number;
    rec: IUser;
    authAttempts: number;
    ipAddress: string;
    userAgent: string;
    cookie: string;
    location: any;
    constructor(myConnectionID: number, userIpAddress: string, request: any);
    doInitUser(): Promise<void>;
    addSubscription(data: any): boolean;
    /**
     * Lookup the IP address using an API or cache.  Associate the IP data with the
     * connection.  In the future this may also lead to geo blocking based on country
     * or some other aspect.
     */
    internalLookupIP(): Promise<void>;
}
