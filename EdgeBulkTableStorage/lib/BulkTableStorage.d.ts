export default class BulkTableStorage {
    configName: string;
    tableSvc: any;
    tableName: string;
    log: any;
    isReady: boolean;
    enabled: boolean;
    openTablePromise: any;
    creds: any;
    /**
     * Pass in the config name as defined in the credential like this:
     * azureTableStorage =
     *      connection: AZURE_STORAGE_CONNECTION_STRING
     */
    constructor(configName: string, tableName: string);
    /**
     * Insert a record using an inverse date time where lower values are newer so they query
     * at the top of the azure table results
     */
    doInsertLogRecord(partKey: string, newData: any): false | Promise<{}>;
    createQuery(fieldName: string, value: any, limit?: any): any;
    doQuery(query: any): Promise<{}>;
    private doInternalOpenTable;
    private internalSetupTabledata;
    /**
     * see https://github.com/Azure/azure-storage-node/blob/master/lib/services/table/tableservice.js
     */
    private doInsertRecord;
}
