export interface IContactInfo {
    type: string; // apple, google, sms, email, whatsapp
    priority: number;
    account: {
        deviceToken?: string;
        phoneNumber?: string;
        email?: string;
    }
}