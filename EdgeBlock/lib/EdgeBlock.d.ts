import EdgeApi from 'edgeapi';
declare class EdgeBlock {
    blockName: string;
    log: any;
    api: any;
    cacheMessageQueue: any;
    static getBlockManager(): any;
    constructor(blockName?: string);
    doGetApi(): Promise<EdgeApi>;
    doGetLink(dataUrl: string, qs?: any): Promise<{}>;
    dateFromObjectId(objectId: any): Date;
    setError(message: string, optionalData: any): boolean;
    sendToQueue(queueName: string, data: any): Promise<{}>;
    updateStatsToday(name: string, keys: any): Promise<boolean>;
}
export default EdgeBlock;
