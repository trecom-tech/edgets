/**
 * Handles configuration for a given DataSetManager
 * This classes is created as a member of a DataSetManager
 * to hold the available configurations for that specific dataset
 */

import { config } from 'edgecommonconfig';
import EdgeApi from 'edgeapi';
import DataSetManager from './DataSetManager';

// How long to use the same transform rules before reloading
// 1000 * 1000 = 1 second

let MAX_TRANSFORM_TIME = 1000 * 1000 * 60 * 5;

class DataSetTransformObject {
    basePointer: any;
    debug      : any;
    baseOutput : { [name: string]: any };

    constructor(public data: any, public isDebug: boolean) {
        this.basePointer = this.data;
        this.baseOutput  = this.data;

        if (this.isDebug == null) {
            this.isDebug = false;
        }

        if (this.isDebug) {
            this.debug = [];
        }
    }

    set(name: string, val: any) {
        if (this.isDebug) {
            this.debug.push({ type: 'set', name, val });
        }

        if (val == null || val === '') {
            delete this.baseOutput[name];
        } else {
            this.baseOutput[name] = val;
        }

        return true;
    }

    get(name: string) {
        if (this.isDebug) {
            this.debug.push({ type: 'get', name, val: this.basePointer[name] });
        }

        if (this.basePointer[name] != null) {
            return this.basePointer[name];
        }

        return null;
    }

    resetBase() {
        // set all pointers back to the start
        this.basePointer = this.data;
        this.baseOutput = this.data;
        return true;
    }

    setWith(subPath: string) {
        let parts: string[] = subPath.replace('/', '.').split('.');
        for (let name of Array.from(parts)) {
            if (!this.basePointer[name]) {
                this.basePointer[name] = {};
            }
            this.basePointer = this.basePointer[name];
        }

        return true;
    }

    setOutputTo(subPath: string) {
        let parts: string[] = subPath.replace('/', '.').split('.');
        for (let name of Array.from(parts)) {
            if (!this.baseOutput[name]) {
                this.baseOutput[name] = {};
            }
            this.baseOutput = this.baseOutput[name];
        }

        return true;
    }

    parseCurrency(val: string) {
        console.log('PARSE CURRENCY: ', val);
        return process.exit(0);
    }

    searchReplace(
        patternText: string,
        replaceText: string,
        sourceValue: string,
    ) {
        if (sourceValue == null) {
            return null;
        }
        if (replaceText == null) {
            return null;
        }

        let re = new RegExp(patternText, 'i');
        let str = sourceValue.toString();
        let m = str.match(re);
        if (m != null) {
            for (let x = 1; x <= 10; x++) {
                if (m[x] != null) {
                    replaceText = replaceText.replace(`$${x}`, m[x]);
                }
            }

            if (/^[0-9\.]+$/.test(replaceText)) {
                return parseFloat(replaceText);
            }
            return replaceText;
        }

        return null;
    }
}

export class DataSetConfig {
    settings: any = null;
    transformPromise: any;
    transformDateLoaded: any;
    settingsPromise: Promise<DataSetConfig>;
    transformCollection: any;
    transform: any;
    strDebug: any;
    configCollection: any;

    constructor(public ds: DataSetManager, public tableName: string) {
        this.settings = null;
        this.configCollection = null;
        this.strDebug = '';
    }

    debug(line: string) {
        return (this.strDebug += line);
    }

    //
    // Returns a settings value or false if not set
    check(flagName: string) {
        if (this.settings != null && this.settings[flagName] != null) {
            return this.settings[flagName];
        }

        return false;
    }

    //
    // Transform an object by applying the rules to it
    async doTransformObject(path: any, obj: any, isDebug?: any) {
        if (isDebug == null) {
            isDebug = false;
        }

        let e;
        config.status('doTransformObject Transform Path     :', path);
        config.status('doTransformObject Transform Incoming :', obj);

        let recordHashStart = EdgeApi.getHash(obj);

        this.internalCheckTransformExpired();

        if (!this.transform) {
            await this.doLoadTransforms();
        }

        if (this.transform.code == null) {
            config.status('doTransformObject no transform code');
            return true;
        }

        if (this.transform.blocks == null) {
            try {
                this.transform.blocks = Function('obj', this.transform.code);
            } catch (error) {
                e = error;
                console.log(
                    `Error in Config code for dataset ${this.ds.databaseName}`,
                    this.transform.code,
                );
                config.reportError('DataSetConfig doTransformObject:', e);
                process.exit(0);
            }
        }

        if (isDebug) {
            obj.rules = [];
        }

        let rule = this.transform.blocks;
        let tempObject = new DataSetTransformObject(obj, isDebug);

        try {
            rule(tempObject);
            if (isDebug) {
                obj.rules.push(tempObject.debug);
            }
        } catch (error1) {
            e = error1;
            config.reportError('Rule error', e);
            console.log('Rule error: ', e);
        }

        let recordHashEnd = EdgeApi.getHash(obj);
        if (recordHashEnd !== recordHashStart) {
            //
            // Hash has changed due to rules
            return true;
        }

        // console.log "Transformed object path=", path, "obj=", obj
        // config.dump "Transformed", path, obj
        // process.exit(0)
        return false;
    }

    //
    // Copy command
    // Takes a value from one name and moves it directly to another
    // requires:  sourceField, targetField
    //
    applyCommandCopy(
        obj: any,
        command: {
            subPath?: any[];
            sourceField?: string;
            targetField?: string;
        },
    ) {
        let basePointer = obj;
        if (command.subPath != null && command.subPath.length > 0) {
            for (let name of Array.from(command.subPath)) {
                if (
                    basePointer[name] != null &&
                    typeof basePointer[name] === 'object'
                ) {
                    basePointer = basePointer[name];
                } else {
                    console.log(`Did not find ${name} in object`);
                    return false;
                }
            }
        }

        if (basePointer[command.sourceField] != null) {
            // console.log "Copy #{command.targetField} value ", basePointer[command.sourceField]
            basePointer[command.targetField] = basePointer[command.sourceField];
            delete basePointer[command.sourceField];
        }

        return true;
    }

    async doGetTransformRule(ruleName: string) {
        this.internalCheckTransformExpired();

        if (!this.transform) {
            await this.doLoadTransforms();
        }

        if (this.transform != null && this.transform[ruleName] != null) {
            return this.transform[ruleName].xml;
        }

        return null;
    }

    //
    // Add/Change transform rules
    async doSetTransformRule(ruleName: string, ruleXml: string) {
        this.internalCheckTransformExpired();

        if (!this.transform) {
            await this.doLoadTransforms();
        }

        this.transform[ruleName] = { xml: ruleXml };

        return this.transformCollection.doUpsertOne(this.tableName, this.transform);

        //
        // TODO:  Flag or something so we know it needs to be converted to code
        //
    }

    /**
     * Load the settings and translations
     */
    doLoadSettings() {
        if (this.settingsPromise) {
            return this.settingsPromise;
        }

        return (this.settingsPromise = new Promise((resolve, reject) => {
            // @configCollection = @ds.db.collection "_config"
            // @settings = yield @configCollection.doFindByID @tableName, true
            // @internalVerifySettings()

            this.settings = {
                track_changes: true,
                active: true,
            };

            resolve(this);
        }));
    }

    //
    // Check to see if the transform rules are older than 10 minutes
    // if so, flush them from memory.
    // TODO:  Add a flag so that the tranform rules are reloaded
    // if a change notification comes from the status server.
    //
    internalCheckTransformExpired() {
        if (this.transformDateLoaded == null) {
            return false;
        }
        let now = new Date();
        if (
            now.getTime() - this.transformDateLoaded.getTime() >
            MAX_TRANSFORM_TIME
        ) {
            //
            // Drop the existing values from doLoadTranform
            delete this.transformPromise;
            delete this.transformCollection;
            delete this.transform;
            delete this.transformDateLoaded;
        }

        return true;
    }

    doLoadTransforms() {
        if (this.transformPromise) {
            return this.transformPromise;
        }

        this.transformPromise = new Promise(async (resolve, reject) => {
            this.transformCollection = this.ds.db.collection('_transform');
            this.transform = await this.transformCollection.doFindByID(
                this.tableName,
                true,
            );
            this.transformDateLoaded = new Date();

            // console.log @tableName, "TRANSFORM LOADED:", @transform
            // process.exit()

            resolve(this);
        });

        return this.transformPromise;
    }

    //
    // Look at the values and see if they are logical and initialized
    //
    internalVerifySettings() {
        let needsave = false;

        if (this.settings == null) {
            this.settings = {};
            needsave = true;
        }

        if (this.settings['track_changes'] == null) {
            this.settings['track_changes'] = true;
            needsave = true;
        }

        if (this.settings['active'] == null) {
            this.settings['active'] = true;
            needsave = true;
        }

        if (needsave) {
            return this.save();
        }
        return null;
    }

    //
    // Save back to server
    // This is not a promise function because we can allow this in the background
    save() {
        // @configCollection.doUpsertOne @tableName, @settings
        return true;
    }
}

export default DataSetConfig;
