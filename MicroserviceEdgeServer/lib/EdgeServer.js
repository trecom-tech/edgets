"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// General configuration and error handlers
const edgecommonconfig_1 = require("edgecommonconfig");
edgecommonconfig_1.default.setTitle("Edge Server");
// Worker thread
const Worker_1 = require("./Worker");
// Start the worker
const worker = new Worker_1.default(process.pid);
exports.default = worker;
//# sourceMappingURL=EdgeServer.js.map