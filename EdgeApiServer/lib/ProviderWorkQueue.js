"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgeapi_1 = require("edgeapi");
const edgecommonstatusupdateclient_1 = require("edgecommonstatusupdateclient");
const ApiProvider_1 = require("./ApiProvider");
//  This set of API functions provides an interface to add messages to
//  a "Work Queue".   Messages are added to the list in REDIS and
//  Will be unique to the set so that the list is not added more than once
//  Status update client is used to update the system with the number of
//  messages pending in Work Queue
// Rrefreshing Time Interval
const refreshTimeout = 1000 * 60 * 60; // 1 hr
class ProviderWorkQueue extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
        this.memoryCache = {};
        // Delayed workqueue for items that may take time to process
        this.knownListChanged = false;
        this.knownWorkQueues = [];
        this.memoryCacheDelayed = {};
        this.cacheTimeDelayed = 20;
    }
    onInit() {
        this.cacheTime = 2 * 1000;
        // Get initial list of workqueues
        edgeapi_1.default.cacheSetMembers('wq_known_queues').then((result) => {
            return this.knownWorkQueues = result;
        });
        this.workQueueStatus = {};
        let statusUpdateClient = new edgecommonstatusupdateclient_1.default(null, null);
        statusUpdateClient.doOpenChangeMessageQueue().then(() => {
            // Make it available to rest of the class
            console.log('StatusUpdate Connected');
            this.statusUpdateClient = statusUpdateClient;
            return setTimeout(() => this.internalStatusSendUpdates(), refreshTimeout);
        });
        setTimeout(() => {
            this.clearCache();
        }, this.cacheTime);
    }
    /**
     * Called on an interval to push anything in the cache array
     * to REDIS, otherwise keep it in memory
     */
    clearCache() {
        for (let wqName in this.memoryCache) {
            let list = this.memoryCache[wqName];
            if (list.length === 0) {
                continue;
            }
            // Quickly put the list in a temporary array so that other procs
            // don't screw up the list during the time it takes to submit to REDIS
            const tmpList = [];
            for (const item of list) {
                tmpList.push(item);
            }
            this.memoryCache[wqName] = [];
            // now memoryCache is empty so we can submit the list to REDIS
            // console.log "clearCache adding to #{wqName} ", tmpList.length, " items."
            edgeapi_1.default.cacheSetAdd(wqName, tmpList);
        }
        setTimeout(() => {
            this.clearCache();
        }, this.cacheTime);
        return true;
    }
    internalGetStatus(workQueueName) {
        if (this.workQueueStatus[workQueueName] == null) {
            this.workQueueStatus[workQueueName] = {
                total_in: 0,
                total_out: 0,
                total_pend: 0,
                total_hits: 0,
            };
        }
        return true;
    }
    /**
     * Update the number of items for a workqueue to keep status
     * @param workQueueName
     * @param amountRecv
     * @param amountProc
     */
    internalStatusInc(workQueueName, amountRecv, amountProc) {
        this.internalGetStatus(workQueueName);
        this.workQueueStatus[workQueueName].total_in += amountRecv;
        this.workQueueStatus[workQueueName].total_out += amountProc;
        return true;
    }
    internalStatusCacheHit(workQueueName) {
        this.internalGetStatus(workQueueName);
        this.workQueueStatus[workQueueName].total_hits++;
        return true;
    }
    internalStatusSetTotal(workQueueName, totalAmount) {
        this.internalStatusInc(workQueueName, 0, 0);
        this.workQueueStatus[workQueueName].total_pend = totalAmount;
        return true;
    }
    /**
     * Send updates to the API based on the number processed
     */
    internalStatusSendUpdates() {
        let loadavg = os.loadavg();
        if (loadavg[1] > 10.0) {
            console.log('Load average is high: 1m=', loadavg[0], ' 5m=', loadavg[1], ' 15m=', loadavg[2]);
            this.loadTooHigh = true;
        }
        else {
            delete this.loadTooHigh;
        }
        if (this.statusUpdateClient == null) {
            setTimeout(() => this.internalStatusSendUpdates(), refreshTimeout);
            return null;
        }
        this.doGetWorkqueueList().then(() => {
            for (let wqName in this.workQueueStatus) {
                let rec = this.workQueueStatus[wqName];
                let data = {};
                data.name = wqName;
                data.host = os.hostname();
                data.type = 'workqueue';
                data.total_in = this.workQueueStatus[wqName].total_in;
                data.total_out = this.workQueueStatus[wqName].total_out;
                data.total_pend = this.workQueueStatus[wqName].total_pend;
                data.total_hits = this.workQueueStatus[wqName].total_hits;
                data.stamp = new Date();
                this.statusUpdateClient.sendStatus(data);
                this.workQueueStatus[wqName].total_in = 0;
                this.workQueueStatus[wqName].total_out = 0;
            }
            return setTimeout(() => this.internalStatusSendUpdates(), refreshTimeout);
        });
        return true;
    }
    getBaseName() {
        return 'workqueue';
    }
    /**
     * Return a safe and REDIS compatible name for a work queue
     * @param workQueueName
     */
    internalGetName(workQueueName) {
        if (typeof workQueueName !== 'string') {
            workQueueName = `error${workQueueName}`;
        }
        let str = workQueueName.replace(/[^a-zA-Z0-9]/g, '_');
        return `wq_${workQueueName}`;
    }
    doAddRecordDelayed(workQueueName, record_id) {
        setTimeout(() => this.doAddRecord(workQueueName, record_id), 10000);
        return true;
    }
    /**
     * Add a record id to a work queue
     * @param workQueueName
     * @param record_id
     */
    doAddRecord(workQueueName, record_id) {
        if (record_id == null) {
            return false;
        }
        if (typeof record_id === 'number') {
            record_id = record_id.toString();
        }
        if (typeof record_id !== 'string') {
            edgecommonconfig_1.default.getLogger('Workqueue').error('Invalid doAddRecord', { wq: workQueueName, record_id });
            return false;
        }
        if (record_id.indexOf(':') !== -1 || record_id.indexOf('.') !== -1 || record_id.indexOf('=') !== -1) {
            return false;
        }
        if (!this.knownWorkQueues.includes(workQueueName)) {
            this.knownWorkQueues.push(workQueueName);
            this.knownListChanged = true;
            edgeapi_1.default.cacheSetAdd('wq_known_queues', workQueueName);
        }
        let wq = this.internalGetName(workQueueName);
        this.internalStatusInc(workQueueName, 1, 0);
        if (this.memoryCache[wq] == null) {
            this.memoryCache[wq] = [];
        }
        this.memoryCache[wq].push(record_id);
        return true;
    }
    /**
     * Return the number of items pending in a queue
     * @param workQueueName
     */
    doGetCount(workQueueName) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            let wq = this.internalGetName(workQueueName);
            let total = yield edgeapi_1.default.cacheGetSetLength(wq);
            if (this.memoryCache[wq] != null) {
                total += this.memoryCache[wq].length;
            }
            edgecommonconfig_1.default.status(`WorkQueue ${wq} length:`, total);
            resolve(total);
        }));
    }
    /**
     * Get the next available or return null
     * This will go very quickly and return null when there
     * is nothing in REDIS
     * @param workQueueName
     */
    doGetOnePending(workQueueName) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            let record;
            let wq = this.internalGetName(workQueueName);
            if (this.memoryCache[wq] != null && this.memoryCache[wq].length > 0) {
                record = this.memoryCache[wq].pop();
                this.internalStatusInc(workQueueName, 0, 1);
                this.internalStatusCacheHit(workQueueName);
                resolve(record);
            }
            record = yield edgeapi_1.default.cacheSetPop(wq);
            // config.status "WorkQueue pop #{wq}:", record
            if (record != null) {
                this.internalStatusInc(workQueueName, 0, 1);
            }
            if (record != null && (record.indexOf(':') !== -1 || record.indexOf('.') !== -1 || record.indexOf('=') !== -1)) {
                resolve(null);
            }
            resolve(record);
        }));
    }
    /**
     * Get array of all known work queues and number of items
     * pending per specific work queue
     */
    doGetWorkqueueList() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.knownWorkQueues == null || !this.knownListChanged === true) {
                // TODO - ROY - cacheSetMembers returns undefined when there's error from redis
                this.knownWorkQueues = (yield edgeapi_1.default.cacheSetMembers('wq_known_queues'));
                // Adding workaround when there's an error from redis
                if (!Array.isArray(this.knownWorkQueues)) {
                    this.knownWorkQueues = [];
                }
            }
            const ab = { test: true };
            const test = ab;
            const data = [];
            for (const name of this.knownWorkQueues) {
                const count = yield this.doGetCount(name);
                data.push({
                    workqueue: name,
                    pending: count,
                });
                this.internalStatusSetTotal(name, count);
            }
            return data;
        });
    }
    /**
     * Don't return until a pending job is available
     * @param workQueueName
     */
    doWaitForPending(workQueueName) {
        let waitCount = 0;
        return new Promise((resolve, reject) => {
            let check;
            return (check = () => {
                if (this.loadTooHigh != null && this.loadTooHigh) {
                    if (waitCount > 0) {
                        console.log(`Pausing workqueue ${workQueueName}, load is too high (${waitCount * 10} seconds)`);
                    }
                    waitCount++;
                    setTimeout(check, 10000);
                    return false;
                }
                return this.doGetOnePending(workQueueName).then(result => {
                    if (result != null) {
                        resolve(result);
                        return true;
                    }
                    else {
                        return setTimeout(check, 2000);
                    }
                });
            })();
        });
    }
}
exports.default = ProviderWorkQueue;
;
//# sourceMappingURL=ProviderWorkQueue.js.map