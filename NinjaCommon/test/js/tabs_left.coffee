$ ->

	addTestButton "Test Tabs", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabsLeft", (tabs)->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two"

		true

	addTestButton "Test Tabs with order", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabsLeft", (tabs)->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two", 1
			tabs.addTab "Test 3", "Default Three", 0
			tabs.addTab "Test 4", "Default Four"
			tabs.addTab "Test 5", "Default Five", 2

		return 1

	addTestButton "Test Tabs order/setTimeout", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabsLeft", (tabs)->
			setTimeout ->
				console.log "Added 0"
				tabs.addTab "Test 0", "Default One", 0
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 1"
				tabs.addTab "Test 1", "Default Two", 1
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 2"
				tabs.addTab "Test 2", "Default Three", 2
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 3"
				tabs.addTab "Test 3", "Default Four", 4
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 4"
				tabs.addTab "Test 4", "Default Five", 5
			, (2000 * Math.random())

		return 1

	addTestButton "Test Tabs with badge", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "Test 1", "Default One"
			tab2 = tabs.addTab "Test 2", "Default Two"
			tab1.setBadge(5)

		return 1

	addTestButton "Test Tabs with Icons", "Open", (e)->

		addHolder()
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"			

		return 1

	addTestButton "Tabs Disabled", "Open", (e)->

		addHolder()
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"
			tab2 = tabs.addTab "tab_3", "Default Three"
			setTimeout () ->
				tabs.setTabEnabled "tab_1", false
			, 2000
			setTimeout () ->
				tabs.setTabEnabled "tab_2", false
			, 4000
		return 1		

	addTestButton "Tabs Background color", "Open", (e)->

		addHolder()
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"
			tab3 = tabs.addTab "tab_3", "Default Three"
			tab3 = tabs.addTab "tab_4", "Default Four"
			tab3 = tabs.addTab "tab_5", "Default Five"			
			tabs.setTabBGColor "tab_1", 1
			tabs.setTabBGColor "tab_2", 2
			tabs.setTabBGColor "tab_3", 3
			tabs.setTabBGColor "tab_4", "tabcolor4"
			tabs.setTabBGColor "tab_5", "tabcolor5"	
			setTimeout () ->
				tabs.setTabEnabled "tab_1", false
			, 3000

			setTimeout () ->
				tabs.setTabEnabled "tab_1", true
			, 6000

		return 1

	addTestButton "Tabs with classes", "Open", (e)->

		addHolder()
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "tab_1", "Apple"
			tabs.setTabClass("tab_1", "ninja-tab-fixed-140")
			tabs.addIconToTab "tab_1", "far fa-book"

			tab1 = tabs.addTab "tab_2", "Ball"
			tabs.setTabClass /_2/, "ninja-tab-fixed-140"
			tabs.addIconToTab "tab_2", "fas fa-book"

			tab3 = tabs.addTab "This is a very long tab title that seems really dumb", "Nothing here"
			tabs.setTabClass tab3.id, "ninja-tab-fixed-140"
			tabs.addIconToTab tab3.id, "fal fa-book"

		true

	addTestButton "Tabs Close Buttons", "Open", (e)->

		addHolder()
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"	
			tabs.addCloseButton "tab_1"
			tab2 = tabs.addTab "tab_2", "Default Two"	
			tab2.setBadge(5,'warning')
			tabs.addIconToTab "tab_2", "fa fa-ban"	
			tabs.addCloseButton "tab_2"
			tab3 = tabs.addTab "tab_3", "Default Three"

		return 1	

	addTestButton "Tabs Close Buttons - Deserialize", "Open", (e)->		
		addHolder()
		.setView "TestLayout1", (view) ->
			view.runTest
				view: "DynamicTabsLeft"
				tabs: [
					title: "tab1"
					icon: "fa fa-book"
					close: true 
					ontabhide: (tab) ->
						console.log "Tab1 is being hidden...", tab.id
				, 
					title: "tab2"
					icon: "fa fa-ban"
					close: true
					ontabhide: (tab) ->
						console.log "Tab2 is being hidden...", tab.id					
				,
					title: "tab3"
					close: false
					ontabhide: (tab) ->
						console.log "Tab3 is being hidden...", tab.id					
				]
		
		return 1

	addTestButton "Test Tabs with badge with context classes", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabsLeft", (tabs)->
			tab1 = tabs.addTab "Test 1", "Default One"
			tab2 = tabs.addTab "Test 2", "Default Two"
			tab1.setBadge(5,'warning')
			tab2.setBadge(2,'danger','back') ## direction of the badge it can be front (default) or back

		return 1

	go()