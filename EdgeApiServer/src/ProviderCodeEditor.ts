const slug = require('slug');
import config         from 'edgecommonconfig';
import DataSetManager from 'edgedatasetmanager';
import EdgeApi        from 'edgeapi';
import ApiProvider    from './ApiProvider';
import EdgeError      from './EdgeError';

import DynamicCodeModel from './DynamicCodeModel';

export default class ProviderCodeEditor extends ApiProvider {
    dataStore: DataSetManager;
    modlist: any;

    constructor(prototype: ApiProvider) {
        super(prototype);
    }

    onInit() {
        this.dataStore = new DataSetManager('lookup');
        return true;
    }

    getBaseName() {
        return 'code';
    }

    async doRunCode(user: any, category: any, name: any, userObject: any, params: any, executeOptions: any) {

        let result: any;

        let codeobj = await this.doInternalGetModuleLocal(category, name);
        if (codeobj == null || codeobj.getProvider == null) {
            console.log('Loading result:', codeobj);
            throw new EdgeError(`Module loading issue category='${ category }', name='${ name }'`, 'PROVIDER_CODE_EDITOR');
        }

        let provider = codeobj.getProvider();

        if (!codeobj.data.compiles) {
            console.log('Codeobj doesn\'t compile:', codeobj);
            throw new EdgeError('Module doesn\'t currently compile', 'PROVIDER_CODE_EDITOR');
        }

        if (provider == null) {
            console.log('Codeobj unable to get provider');
            throw new EdgeError('Unable to get code provider', 'PROVIDER_CODE_EDITOR');
        }

        // Default execute options from DynamicProvider
        let options = provider.initRunOptions();
        if (executeOptions != null) {
            EdgeApi.deepMergeObject(options, executeOptions);
        }

        // Set the two main function parameters
        options.userObject = userObject;
        options.params     = params;

        console.log('Running code', provider, ' with ', options);
        config.status('doRunCode module=', name, 'executeOptions=', options);

        if (options.trace) {
            options.onConsoleLog = (time_ms: any, values: any) => {
                return user.sendPush('runcodeLog', {
                    type: 'c',
                    time_ms,
                    values,
                });
            };
        }

        result = await provider.doCallFunction(options);

        console.log('SENDING RESULT:', result);
        return result;
    }

    // A standard id value for the database based on the name.
    internalGetKey(category: any, name: any, branch: any) {
        return slug(category + '_' + name + '_' + branch).toLowerCase();
    }

    // List the available modules
    // @param condition {string} A datapath search string
    // @return An array of matching code modules
    doListAvailableModules(user: any, condition: any = null) {

        // TODO: Check for permissions in the user object before loading code modules

        // Search conditions
        let path = '/code/';
        if (condition != null) {
            path += condition;
        }

        let fieldList = {
            id: 1,
            category: 1,
            name: 1,
            meta: 1,
            compiles: 1,
            version: 1,
            _lastModified: 1,
            compile_host: 1,
            status: 1,
            deployment: 1,
        };

        let sortOrder = { id: -1 };
        let limit     = 10000;

        return this.dataStore.doGetItems(path, fieldList, sortOrder, 0, limit);
    }

    // Save the notes (the README tab)
    async doSaveModuleNotes(user: any, category: any, name: any, newNotes: any) {

        let codeobj = await this.doInternalGetModuleLocal(category, name);
        if (codeobj == null || codeobj.setCoffeescriptCode == null) {
            throw new EdgeError(`Error saving module category='${ category }', name='${ name }'`, 'PROVIDER_CODE_EDITOR');
        }

        codeobj.setNotes(newNotes);
        codeobj.save();
        return true;
    }

    // Save the development version of a module to the database
    async doSaveModuleLocal(user: any, category: any, name: any, code: any) {

        let { username } = user;
        if (username == null || username.length < 1) {
            username = 'brian';
        }

        let codeobj = await this.doInternalGetModuleLocal(category, name);
        if (codeobj == null || codeobj.setCoffeescriptCode == null) {
            throw new EdgeError(`Error saving module category='${ category }', name='${ name }'`, 'PROVIDER_CODE_EDITOR');
        }

        codeobj.setCoffeescriptCode(code);
        codeobj.setBranch('dev');
        codeobj.save();

        return codeobj.data;
    }

    async doTestCaseAdd(category: any, name: any, options: any, id: any) {

        let module = await this.doInternalGetModuleLocal(category, name);
        if (id != null) {
            module.updateTestCase(id, options);
        } else {
            module.addTestCase(options);
        }

        return module.data;
    }

    async doTestCaseRemove(category: any, name: any, id: any) {

        let module = await this.doInternalGetModuleLocal(category, name);
        module.removeTestCase({});
        return module.data;
    }

    // Load a module and return it's data
    async doGetModuleLocal(user: any, category: any, name: any) {

        let module = await this.doInternalGetModuleLocal(category, name);
        if (module != null) {
            return module.data;
        }
        return null;
    }

    async doInternalGetModuleLocal(category: any, name: any): Promise<any> {

        // load local code
        let key = this.internalGetKey(category, name, 'dev');
        if (this.modlist == null) {
            this.modlist = {};
        }
        if (this.modlist[key] == null) {
            this.modlist[key] = new DynamicCodeModel(key);
            await this.modlist[key].doVerifyLoaded();
        }

        this.modlist[key].setCategory(category);
        this.modlist[key].setName(name);
        this.modlist[key].setBranch('dev');
        this.modlist[key].getProvider();
        return this.modlist[key];
    }
};
