# EdgeCommonElastic
> Interface to ElasticSearch and Amazon ES

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonElastic.git
    import EdgeElastic from 'edgecommonelastic'

## What is EdgeElastic

This class is an interface to Elastic Search for high speed indexing of documents.  It supports
Amazon ES and Elastic Search 6.x

## Usage

### Indexing documents

This will check REDIS to see if the document is already in the index with the exact same attributes.  If so, it will not index the document again.

    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param id [string/number] the document id
    @param doc [mixed] the object to index

    doIndex: ({dataset, collection, id, doc})

### Retrieve mapping definition of index or index/type.

This will check index on elasticsearch and if it's available, it will return mapping difinition of index or index/type. if type is not specified, it will return all mapping difinition.

    @param index [string] the name of index 
    @param type  [string] the name of a type in index

    doGetMapping: ({index, type})

### Get All indexes information

This will return the All indexes information such like index's name, size, etc. There is no parameter.

    doGetIndexes: ()

### Update document

This will check elasticsearch to see if the id specified is available on collection in dataset and If so, update with doc parameter by id.

    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param id [string/number] the document id
    @param doc [mixed] the object updated to index

    doUpdate: ({dataset, collection, id, doc})

### Get document by id

This will check elasticsearch to see if the id in body parameter is existing on collection in dataset and If so, this will retrieve document by id. body should have at least id.
    
    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param body [object] the object which contain id

    doGet: ({dataset, collection, body})

### Search on elasticsearch with query

This will return documents matching a query, aggregations/facets, highlighted snippets, suggestions, and more.
    
    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param query [string] the string which express Query in the Lucene query string syntax
    @param limit [number] the number of at most document returning at once, default = 100   

    doSearch: ({dataset, collection, query, limit})

### Get hashed document

This will return docuemnt by string combined with dataset, collection and id on collection named "path" in dataset called "dochash"

    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param id [string/number] the document id

    doGetDocumentHash: ({dataset, collection, id})

### Set documnent hashed

This will create the string hased with doc parameter by EdgeApi.hash method  and add this string with key combined with dataset, collection and id parameters as id on 'path' collection in 'dochash' dataset.
    
    @param dataset [string] the name of the database or dataset to use
    @param collection [string] the name of the collection or table within that data dataset
    @param id [string/number] the document id
    @param doc [mixed] the name of object hashed to index
    @param subpath [string] the name of key of object hashed to index

    setDocumentHash: ({dataset, collection, id, doc, subpath})


## Testing

### Testing Locally

Set DB_HOST to localhost:9200 and

    npm run test-local

### Testing via docker

    docker-compose build 
    docker-compose up
