import * as os     from 'os';
import * as crypto from 'crypto';

const jsonfile = require('jsonfile');
import { spawn }   from 'child_process';

import config         from 'edgecommonconfig';
import DataSetManager from 'edgedatasetmanager';

import ApiProvider   from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
import EdgeError     from './EdgeError';

export default class ProviderOS extends ApiProvider {
    public serverDetail: any;
    private db: any;
    private internalJobPromise: Promise<any>;

    constructor(prototype: ApiProvider) {
        super(prototype);
    }

    onInit() {
        let pack;
        this.serverDetail           = {};
        this.serverDetail.host      = os.hostname();
        this.serverDetail.total_mem = os.totalmem();

        let packageFile = config.FindFileInPath('package.json', ['./', '../', '../../']);
        if (packageFile != null) {
            pack = jsonfile.readFileSync(packageFile);
        } else {
            pack = { version: 1.0, version_date: new Date(), version_text: 'OnDemand (no package.json)' };
        }

        this.serverDetail.version      = pack.version;
        this.serverDetail.version_date = pack.version_date;
        this.serverDetail.version_text = pack.version_text;
    }

    getBaseName() {
        return 'os';
    }

    async doGetServerInformation() {
        this.serverDetail.free_mem = os.freemem();
        this.serverDetail.load_Avg = os.loadavg();
        return this.serverDetail;
    }

    //  Execute a job and push the results back to the caller
    async doExecuteJob(user: ApiSocketUser, job_id: string) {
        config.status('ProviderOS.doExecuteJob');

        return new Promise((resolve, reject) => {

            console.log('doExecuteJob:', job_id);

            return this.doInternalGetJobCollection().then(collection => {

                return collection.doGetItem(job_id, false).then((job: any) => {

                    config.status('Found job:', job);
                    if (job == null) {
                        return false;
                    }

                    let commandLine = `./${ job.script }`;
                    if (/.coffee/.test(job.script)) {
                        commandLine = `/usr/local/bin/coffee ${ commandLine }`;
                    }

                    //  Job was not found, output an error
                    if (job == null) {
                        user.sendPush('job-output', {
                            job_id,
                            output: `Invalid job ${ job }`,
                        });

                        console.log(`Invalid Job: ${ job }`);
                        reject(new EdgeError(`Invalid job: ${ job }`, 'PROVIDER_OS'));
                        return;
                    }

                    //  Send initial job output when we start
                    let mid = __dirname.toString().split('/');
                    mid.pop();
                    mid.pop();
                    mid.push(job.folder);
                    let path = mid.join('/');

                    user.sendPush('job-output', {
                        job_id,
                        output: `Executing: ${ commandLine } ${ job.args } in ${ path }\n\n`,
                    });

                    let args = job.args.split(' ');
                    // tslint:disable-next-line:one-variable-per-declaration
                    for (let n = 0, end = args.length, asc = 0 <= end; asc ? n < end : n > end; asc ? n++ : n--) {
                        args[n] = args[n].replace(/^'([^']+)'$/, '$1');
                    }
                    args.splice(0, 0, job.script);

                    try {

                        let fullOutput = '';

                        //  Spawn the job
                        let envCopy = {
                            TERM: 'HTML',
                            HOME: '/Users/innovation',
                            LANG: 'en_US.UTF-8',
                            LOGNAME: 'innovation',
                            PATH: '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
                            SHELL: '/bin/bash',
                            SHLVL: '1',
                            TMPDIR: '/tmp/',
                            USER: 'innovation',
                        };

                        let jobExec = spawn('/usr/local/bin/coffee', args, {
                            cwd: path,
                            env: envCopy,
                        });

                        jobExec.stdout.on('data', data => {
                            if (data != null) {
                                fullOutput += data.toString();
                                return user.sendPush('job-output', {
                                    job_id,
                                    output: data.toString(),
                                });
                            }
                            return false;
                        });

                        jobExec.stderr.on('data', data => {
                            if (data != null) {
                                fullOutput += data.toString();
                                return user.sendPush('job-output', {
                                    job_id,
                                    output: data.toString(),
                                    is_err: true,
                                });
                            }
                            return false;
                        });

                        return jobExec.on('close', code => {
                            job.result  = fullOutput;
                            job.pending = false;
                            job.lastRun = new Date();
                            return collection.doUpsertOne(job_id, job).then(() => {
                                return resolve({ status: 'Job Complete', exit_code: code });
                            });
                        });
                    } catch (e) {

                        config.reportError('Spawn issue:', e);
                        user.sendPush('job-output', {
                            job_id,
                            output: `Exception in code:${ e.toString() }`,
                        });
                        reject(new EdgeError(`Exception in job`, 'PROVIDER_OS'));
                    }
                });
            });
        });
    }

    //  Shortcut to create a job that runs once a day
    doCreateDailyJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string) {
        return this.doCreateJob(title, 'hourly', 24, workingFolder, scriptName, commandLineArgs, 'system');
    }

    //  Shortcut to create a job that runs once an hour
    doCreateHourlyJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string) {
        return this.doCreateJob(title, 'hourly', 1, workingFolder, scriptName, commandLineArgs, 'system');
    }

    //  Shortcut to create a job that runs once and never again
    doCreateOnetimeJob(title: string, workingFolder: string, scriptName: string, commandLineArgs: string) {
        return this.doCreateJob(title, 'once', null, workingFolder, scriptName, commandLineArgs, 'system');
    }

    //  Create a job
    doCreateJob(title: string, freqType: string, freqRate: number, workingFolder: string, scriptName: string, commandLineArgs: string, jobOwner: string, npm: any = null) {

        return new Promise(async (resolve) => {

            let c = await this.doInternalGetJobCollection();

            let idHash = crypto.createHash('sha1');
            idHash.update(title);
            idHash.update(freqType);
            let job_id = idHash.digest('hex');

            if (freqType == null) {
                freqType = 'once';
            }

            if (freqRate == null || typeof freqRate !== 'number') {
                freqRate = 0;
            }

            if (workingFolder == null || typeof workingFolder !== 'string') {
                workingFolder = '';
            }

            if (scriptName == null || typeof scriptName !== 'string') {
                console.log('Can\'t create job, no script name');
                resolve();
            }

            if (commandLineArgs == null || typeof commandLineArgs !== 'string') {
                commandLineArgs = '';
            }

            let jobDoc: any = {
                id: job_id,
                title,
                freqType,
                freqRate,
                folder: workingFolder,
                script: scriptName,
                args: commandLineArgs,
                owner: jobOwner,
                enabled: true,
                pending: 1,
            };

            if (npm != null) {
                jobDoc.npm = npm;
            }

            let result = await c.doUpdate(`/job/${ job_id }`, jobDoc);
            resolve(result);
        });
    }

    //  Internal function that connects to the database
    doInternalGetJobCollection() {

        if (this.internalJobPromise) {
            return this.internalJobPromise;
        }
        return this.internalJobPromise = new Promise(async (resolve) => {

            this.db = new DataSetManager('os');
            await this.db.doVerifySimpleIndex('job', 'lastRun', null, false);
            await this.db.doVerifySimpleIndex('job', 'pending', null, false);
            await this.db.doVerifySimpleIndex('job', 'owner', null, false);
            resolve(this.db);
        });
    }
};
