import RuleProcessor from './RuleProcessor';
export default class RuleProcessorWorkqueue extends RuleProcessor {
    api: any;
    lastRecord: any;
    onInit(): void;
    onExecute(job: any, count?: number): void;
}
