/**
 * Handles configuration for a given DataSetManager
 * This classes is created as a member of a DataSetManager
 * to hold the available configurations for that specific dataset
 */
import DataSetManager from './DataSetManager';
export declare class DataSetConfig {
    ds: DataSetManager;
    tableName: string;
    settings: any;
    transformPromise: any;
    transformDateLoaded: any;
    settingsPromise: Promise<DataSetConfig>;
    transformCollection: any;
    transform: any;
    strDebug: any;
    configCollection: any;
    constructor(ds: DataSetManager, tableName: string);
    debug(line: string): string;
    check(flagName: string): any;
    doTransformObject(path: any, obj: any, isDebug?: any): Promise<boolean>;
    applyCommandCopy(obj: any, command: {
        subPath?: any[];
        sourceField?: string;
        targetField?: string;
    }): boolean;
    doGetTransformRule(ruleName: string): Promise<any>;
    doSetTransformRule(ruleName: string, ruleXml: string): Promise<any>;
    /**
     * Load the settings and translations
     */
    doLoadSettings(): Promise<DataSetConfig>;
    internalCheckTransformExpired(): boolean;
    doLoadTransforms(): any;
    internalVerifySettings(): boolean;
    save(): boolean;
}
export default DataSetConfig;
