"use strict";
//
// class for simpleobject data type
//
// @extends [DataFormatterType]
//
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatSimpleObject extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'simpleobject';
        this.clickable = true;
        this.align = 'left';
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null)) {
            return "Not set";
        }
        if ((data != null) && Array.isArray(data)) {
            if (data.length === 0) {
                return "Not set";
            }
            if (typeof data[0] === "string") {
                return data.sort().filter(a => a != null).join(", ");
            }
        }
        return "View";
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return data;
    }
    renderTooltip(row, value, tooltipWindow) {
        if ((value == null)) {
            return false;
        }
        let width = 0;
        let height = 20;
        let str = "<table>";
        for (let varName in value) {
            const val = value[varName];
            str += "<tr><td>";
            str += varName;
            str += "</td><td>";
            let temp = '';
            if (Array.isArray(val)) {
                temp = val.length + ' Records';
            }
            else if (typeof val === 'object') {
                temp = Object.keys(val).length + ' Properties';
            }
            else {
                temp = val;
            }
            str += temp;
            str += "</tr>";
            // @ts-ignore
            const tempWidth = textWidth(varName + temp) + 40;
            width = width > tempWidth ? width : tempWidth;
            height += 20;
        }
        str += "</table>";
        tooltipWindow.html(str);
        tooltipWindow.setSize(width, height);
        return true;
    }
    onFocus(e, col, data) {
        const key = data.id || e.path || e.rn;
        // @ts-ignore
        const path = DataMap.getDataMap().engine.parsePath(e.path);
        // @ts-ignore
        return doPopupTableView(data[col], `${path.collection} - ${col}`, `${path.collection} - ${col}`, 800, 400, view => 
        // console.log "Popup table view created in DataFormatTypes?"
        true);
    }
}
exports.DataFormatSimpleObject = DataFormatSimpleObject;
exports.default = DataFormatSimpleObject;
//# sourceMappingURL=DataFormatSimpleObject.js.map