# EdgeCommonPushNotification

Provides Push Notifications and SMS in the variety of forms such as apple/google push notification, WhatsApp and Twilio SMS.

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonPushNotification.git
    import EdgeCommonNotification from 'edgecommonpushnotification';
    
## Testing 
- Global testing

        npm run test
    
- Locally testing
        
        npm run test-local
    
- Specifically testing

        npm run test-apple
        npm run test-google
        npm run test-twilio
        npm run test-whatsapp (waiting for approval from twilio)
    
## Main Modules

EdgePushNotification is a method of sending a notification to a user through multiple methods. A notification may be in the form of:

- Apple Push Notification (APN)
- Google Push Notification
- WhatsApp Message
- SMS message using Twilio API
- Email notification using SendGrid and EdgeCommonMailer (Disabled this module, because `edgecommonmailer` exists)

        const notification = new EdgeCommonNotification();

### addContactDetails
> Set user contact's details using `config.setCredentials()` method

    notification.addContactDetails(userContactDetails: IContactInfo[]);

<details>
<summary>Show IContactInfo</summary>

```
export interface IContactInfo {
    type: string; // apple, google, sms, email, whatsapp
    priority: number;
    account: {
        deviceToken?: string;
        phoneNumber?: string;
        email?: string;
    }
}
```
</details>

<details>
<summary>Show Contact Example</summary>

```
const userContactDetails: IContactInfo[] = [
    {
        type: 'apple',
        priority: 0,
        account: {
            deviceToken: "your_deviceToken", // gets from mobile app
        }
    },
    {
        type: 'google',
        priority: 1,
        account: {
            deviceToken: "your_deviceToken", // gets from mobile app
        }
    },
    {
        type: 'whatsapp',
        priority: 2,
        account: {
            phoneNumber: 'your_whatsapp_number',
        }
    },
    {
        type: 'sms',
        priority: 3,
        account: {
            phoneNumber: 'your_phone_number'
        }
    }
];
```
</details>

    
* [Understanding Apple the key and certificate files](https://github.com/node-apn/node-apn/wiki/Preparing-Certificates)
* [Understanding APN(Apple Push Notification) Options Configuration](https://github.com/node-apn/node-apn/blob/master/doc/provider.markdown)

### doSendMessage
> Do send push notifications and sms to all of user's contacts

    await notification.doSendMessage(message: IMessage);
    
<details>
<summary>Show IMessage</summary>

```
export interface IMessage {
    alert       : string,
    title?      : string,
    topic?      : string,   // for only apple
    payload?    : any,      // for only apple
    sound?      : any,      // for only apple
    expiry?     : any,      // for only apple
    badge?      : any,      // for only apple
    priority?   : any,      // for only apple
    urlArgs?    : any[],    // for only apple
}
```
</details>

<details>
<summary>Show Message Example</summary>

```
const message: IMessage = {
    title: 'Testing Notification',
    alert: `\u2709 You have a new message(${new Date()})`,
    topic: 'com.test.Demo-PushNotification1',
};
```
> To send sms to WhatsApp, must have to use twilio template like `alert: 'Hi {{1}}, This is testing message from {{2}}'`.
</details>

#### doSendForApple(deviceToken: string, message: IMessage )
> Do send push notification to apple devices.

    await notification.dosendForApple('your_deviceToken', message);

#### doSendForGoogle(deviceToken: string, message: IMessage)
> Do send push notification to android devices.

    await notification.doSendForGoogle('your_deviceToken', message);

#### doSendWhatsApp(whatsAppNumber: string, message: IMessage)
> Do send push notifications to WhatsApp numbers (waiting for approval from twilio service).

    await notification.doSendWhatsApp('your_whatsapp_number', twilio_templ_msg);

#### doSendSMS(phoneNumber: string, message: IMessage)
> Do send SMS message to user's phone numbers. 

    await notification.doSendSMS('your_phone_number', message);

### Logging

    cd ~/EdgeData/logs/
    PushNotification-user-iMac.local-info.log
    PushNotification-user-iMac.local-error.log 
