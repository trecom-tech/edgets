import * as path              from "path";
import EdgeCommonNotification from '../src/EdgeCommonNotification';
import {expect}               from 'chai';
import { IContactInfo }       from '../src/IContactInfo';
import './helper/setup';

const notification = new EdgeCommonNotification();

const appleValidContactDetail: IContactInfo = {
    type: 'apple',
    priority: 0,
    account: {
        deviceToken: '079AF23E1A3620DB57D113ADF91F21DFB0E7D99244AD7BDCACEB4B59B6061151'
    }
};

const appleInvalidContactDetail: IContactInfo = {
    type: 'apple',
    priority: 0,
    account: {
        deviceToken: '079AF23E1A3620DB57D113ADF91F21DFB0E7D99244AD7BDCACEB4B59B60611'
    }
};

const message = {
    title: 'Testing Apple Push Notification',
    alert: `\u2709 You have a new message(${new Date()})`,
    topic: 'com.test.Demo-PushNotification1',
    sound: "chime.caf",
    badge: 10,
};

describe('Apple Push Notification', () => {
    it('doSendForApple() with valid token', async () => {
        const result = await notification.doSendForApple(appleValidContactDetail.account.deviceToken, message);
        expect(result).to.equal(true);
    });

    it('doSendForApple() with invalid token', async () => {
        const result = await notification.doSendForApple(appleInvalidContactDetail.account.deviceToken, message);
        expect(result).to.equal(false);
    });
});
