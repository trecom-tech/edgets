import { DataFormatText } from './DataFormatText';
export declare class DataFormatSourceCode extends DataFormatText {
    name: string;
    align: string;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    unformat(data: any, path: any): any;
}
export default DataFormatSourceCode;
