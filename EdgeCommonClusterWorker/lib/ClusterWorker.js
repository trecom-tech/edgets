"use strict";
//
//  Generic holder for calls that can be done as part of a threaded cluster
//
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
const edgecommonratecapture_1 = require("edgecommonratecapture");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgebulktablestorage_1 = require("edgebulktablestorage");
const edgeapi_1 = require("edgeapi");
const chalk = require('chalk');
const numeral = require('numeral');
const os = require('os');
//
//   Log to either SQL Server or Mongo DB
let globalLoggingMongo = false;
let globalLoggingStatus = false;
class ClusterWorker {
    constructor() {
        this.jobsRunning = 0;
        this.statsKey = {};
        this.currentTimestamp = new Date();
        // console.log = @reportText
        const logging = edgecommonconfig_1.default.getCredentials('Logging');
        if (logging != null) {
            if (logging.table != null) {
                globalLoggingStatus = true;
            }
        }
        //
        //  Once per second update the current timestamp, it's not critical to be more accurate
        //  this saves a lot of CPU on the native date object when inserting hundreds of records
        //  in a short amount of time.
        //
        setInterval(() => {
            return this.currentTimestamp = new Date();
        }, 1000);
        //
        //  Get a copy of the API before calling onInit from the subclass
        edgeapi_1.default.doGetApi()
            .then((api) => __awaiter(this, void 0, void 0, function* () {
            this.api = api;
            this.onInit();
        }));
    }
    // place holder function that need to be overrided in child class
    onInit() {
        return Promise.resolve(true);
    }
    //
    //  Helper function sends a message to the terminal to be displayed
    //  id is the worker id, message is any text
    reportText(...messages) {
        let str = '';
        for (const message of messages) {
            if (str.length > 0) {
                str += ' ';
            }
            if (typeof message === 'object') {
                str += chalk.blue(JSON.stringify(message));
            }
            else {
                str += message;
            }
        }
        console.info(str);
        return true;
    }
    onBeforeUpdate(vars) {
        // placeholder
    }
    //
    fixupJson(obj) {
        for (const i in obj) {
            let newName;
            const o = obj[i];
            if (i.indexOf('$') !== -1) {
                newName = i.toString().replace('$', 'SS');
                obj[newName] = o;
                delete obj[i];
                continue;
            }
            if (i.indexOf('.') !== -1) {
                // config.getLogger('ClusterWorker').error('ClusterWorker fixupJson invalid key '.'', { invalid: i, obj: obj })
                newName = i.toString().replace('.', '_dot_');
                obj[newName] = o;
                delete obj[i];
                continue;
            }
            if (typeof o === 'object') {
                this.fixupJson(o);
            }
            else if (typeof o === 'string') {
                if ((o.length === 24) && (o.charAt(10) === 'T') && (o.charAt(23) === 'Z')) {
                    obj[i] = new Date(o);
                }
            }
        }
        return true;
    }
    //
    //  Create a loop that updates the number of messages in the queue
    //  once per second.
    //
    setupCounterUpdateLoop(queueName, mqName, monitorChannel, callbackGetTotal) {
        //
        //  Track the rate of things which will also update time series data
        this.monitorChannel = monitorChannel;
        const queueRate = new edgecommonratecapture_1.default(queueName);
        return queueRate.onBeforeUpdate = (vars) => {
            this.onBeforeUpdate(vars);
            return this.monitorChannel.checkQueue(mqName)
                .then((result) => {
                // Result looks like this:
                // queue: 'item-updates', messageCount: 0, consumerCount: 0
                const { messageCount } = result;
                if (callbackGetTotal != null) {
                    queueRate.addSample(callbackGetTotal(queueName, messageCount), messageCount);
                }
                else {
                    edgecommonconfig_1.default.status('Warning: setupCounterUpdateLoop withouth callbackGetTotal');
                    queueRate.addSample(0, messageCount);
                }
                return true;
            });
        };
    }
    //
    //  Open a connection to the logging database
    setupLogging() {
        //
        //  Logging to mongodb process field
        if (globalLoggingStatus === true) {
            this.logDataset = new edgedatasetmanager_1.default('rets_raw');
            this.logDataset.doGetCollection('process')
                .then((collection) => {
                this.logCollection = collection;
            });
        }
        if (globalLoggingMongo !== true) {
            return false;
        }
        this.hostname = os.hostname();
        this.loggingCollection = new edgebulktablestorage_1.default('azureLogs', 'microservice');
        return true;
    }
    //
    //  Setup a loop to work a message queue
    //  Given a queue name and consumer count, the callback will be made
    //  passing in the job object.   The callback can return true/false,
    //  null, or a promise.
    //
    //  Currently there is no difference between true/false returned
    //
    processMessageQueue(logName, queueName, consumerCount, onMessageCallback) {
        const log = edgecommonconfig_1.default.getLogger(logName);
        if ((onMessageCallback == null) || (typeof onMessageCallback !== 'function')) {
            throw new Error('Expecting onMessageCallback to be a function');
        }
        const status = {
            totalProcessed: 0,
            totalPending: 0
        };
        //
        //  Correctly a long time spelling error that is difficult to remove
        if (globalLoggingMongo === true) {
            this.setupLogging();
        }
        edgecommonmessagequeue_1.default.doOpenMessageQueue(queueName)
            .then(mqItemsPending => {
            const onComplete = (msg) => {
                status.totalProcessed++;
                mqItemsPending.mqChannel.ack(msg);
                this.verifyLoadAverage();
                return true;
            };
            if (edgecommonconfig_1.default.traceEnabled) {
                consumerCount = 1;
            }
            mqItemsPending.consume(consumerCount, (msg, job) => {
                try {
                    //
                    // Execute the job and then if a boolean is
                    // not returned we wait for a promise.
                    //
                    const result = onMessageCallback(job);
                    if ((result != null) && (typeof result !== 'boolean')) {
                        return result.then(() => {
                            return onComplete(msg);
                        });
                    }
                    else {
                        return onComplete(msg);
                    }
                }
                catch (e) {
                    edgecommonconfig_1.default.status('Exception during message:', e, job);
                    log.error('Exception', {
                        job,
                        e
                    });
                    return onComplete(msg);
                }
            });
            return this.setupCounterUpdateLoop(logName, queueName, mqItemsPending.mqChannel, (mqName, mqPending) => {
                status.totalPending = mqPending;
            });
        })
            .catch((err) => {
            console.log(err);
        });
        return status;
    }
    //
    //  Internal function for use in watching Work Queues
    //  Similar to the
    setupMonitorProgressWorkQueue(queueName) {
        const bar = new edgecommonratecapture_1.default(`${queueName} WorkQueue`);
        const updateFunction = () => {
            let totalProcessed = 0;
            const now = new Date().getTime();
            for (let slot in this.workerList[queueName]) {
                const data = this.workerList[queueName][slot];
                let str = `${queueName} slot [${slot}] : `;
                if (data != null) {
                    totalProcessed = totalProcessed + data.total;
                    if (data.start != null) {
                        const diff = numeral((now - data.start) / 1000).format('#,###.###') + ' sec.';
                        str += diff;
                        str += ` id: ${data.id} `;
                        if ((now - data.start) > (1000 * 60 * 5)) {
                            console.log('Slot timeout!');
                            process.exit();
                        }
                        console.log(str);
                    }
                    else {
                        str += ' unknown or not started.';
                    }
                }
                else {
                    str += ' idle.';
                }
            }
            return this.api.workqueue_doGetCount(queueName)
                .then((totalPending) => {
                bar.addSample(totalProcessed, totalPending);
                return setTimeout(updateFunction, 5000);
            });
        };
        setTimeout(updateFunction, 5000);
        return this.bar = bar;
    }
    startRequest(slot, id, queueName) {
        const startTime = process.hrtime();
        const reqData = {
            id: `${queueName}${startTime[0]}${startTime[1] * 1000}_${slot}`.replace('-', ''),
            record_id: id,
            start_time: startTime,
            server: this.hostname,
            name: queueName
        };
        return reqData;
    }
    endRequest(reqData, result) {
        const diff = process.hrtime(reqData.start_time);
        const logData = {
            record_id: reqData.record_id,
            stamp: new Date(),
            wq: reqData.name,
            host: reqData.server,
            proctime: ((diff[0] * 1e9) + diff[1]) / 1000000,
            result
        };
        if (logData.host == null) {
            logData.host = os.hostname();
        }
        if ((globalLoggingStatus === true) && (this.logCollection != null)) {
            const logDataSub = {};
            logDataSub[reqData.name] = {
                stamp: logData.stamp,
                host: logData.host,
                proctime: logData.proctime,
                result
            };
            this.api.data_doUpdatePath('rets_raw', `/process/${reqData.record_id.replace(/,\./, '_')}`, logDataSub);
        }
        logData.id = reqData.id;
        if (globalLoggingMongo === true) {
            const partKey = reqData.record_id.toString().replace(/_.*/, '');
            edgecommonconfig_1.default.status(`ClusterWorker endRquest [${reqData.name}+${reqData.server}]=`, logData);
            this.loggingCollection.doInsertLogRecord(partKey, logData);
        }
        // @loggingCollection.doInsertOne logData
        return true;
    }
    //
    //  Hard exit if the server is overloaded
    verifyLoadAverage() {
        const loadAvg = os.loadavg();
        if ((loadAvg[0] > 40.0) || (loadAvg[1] > 30.0)) {
            //
            //  Server is too busy
            this.log.error(`Server is too busy: ${loadAvg[0]}, ${loadAvg[1]}, ${loadAvg[2]}`);
            process.exit(0);
        }
        return true;
    }
    //
    //  One slot worker for a workqueue to be processed
    //
    internalProcessWorkQueueWorker(slot, queueName, onMessageCallback) {
        if (this.workerList[queueName] == null) {
            this.workerList[queueName] = {};
        }
        if (this.workerList[queueName][slot] != null) {
            this.workerList[queueName][slot].start = null;
        }
        let req = null;
        this.verifyLoadAverage();
        //
        //  Wait for a message
        this.api.workqueue_doWaitForPending(queueName)
            .then((record_id) => {
            if (record_id == null) {
                //
                //  Somehow we got a null id?
                this.internalProcessWorkQueueWorker(slot, queueName, onMessageCallback);
                return false;
            }
            if ((typeof (record_id) !== 'string') || (record_id === '')) {
                //
                //  Somehow we got an empty or invalid ID
                this.internalProcessWorkQueueWorker(slot, queueName, onMessageCallback);
                return false;
            }
            //
            //  Track the various specific allocations
            if (this.workerList[queueName][slot] == null) {
                this.workerList[queueName][slot] = {
                    id: record_id,
                    total: 0
                };
            }
            this.workerList[queueName][slot].start = new Date().getTime();
            req = this.startRequest(slot, record_id, queueName);
            const result = onMessageCallback(record_id);
            this.workerList[queueName][slot].total++;
            if ((result != null) && (result.then != null) && (typeof result.then === 'function')) {
                return result.then((finalResult) => {
                    this.endRequest(req, finalResult);
                    return this.internalProcessWorkQueueWorker(slot, queueName, onMessageCallback);
                });
            }
            else {
                this.endRequest(req, result);
                return this.internalProcessWorkQueueWorker(slot, queueName, onMessageCallback);
            }
        }).catch((e) => {
            console.log(`internalProcessWorkQueueWorker ${queueName} Error processing record:`, e);
            this.endRequest(req, { err: e });
            return this.internalProcessWorkQueueWorker(slot, queueName, onMessageCallback);
        });
        return true;
    }
    //
    //  Setup a loop to work a workqueue
    //  Given a queue name and consumer count, the callback will be made
    //  passing in the job object.   The callback can return true/false,
    //  null, or a promise.
    //
    //  Currently there is no difference between true/false returned
    //
    processWorkQueue(logName, queueName, consumerCount, onMessageCallback) {
        if ((onMessageCallback == null) || (typeof onMessageCallback !== 'function')) {
            throw new Error('Expecting onMessageCallback to be a function');
        }
        if (edgecommonconfig_1.default.traceEnabled) {
            consumerCount = 1;
        }
        //
        // Workqueue Variables
        this.workerList = {};
        this.log = edgecommonconfig_1.default.getLogger(logName);
        if ((globalLoggingMongo === true) || (globalLoggingStatus === true)) {
            this.setupLogging();
        }
        edgecommonconfig_1.default.status('processWorkQueue Getting API');
        return edgeapi_1.default.doGetApi()
            .then((api) => {
            this.api = api;
            edgecommonconfig_1.default.status(`processWorkQueue Starting ${consumerCount} consumers`);
            if (consumerCount < 1) {
                consumerCount = 1;
            }
            if (consumerCount > 50) {
                consumerCount = 50;
            }
            const end = consumerCount;
            const asc = 0 <= end;
            for (let n = 0; asc ? n < end : n > end; asc ? n++ : n--) {
                this.internalProcessWorkQueueWorker(n, queueName, onMessageCallback);
            }
            return this.setupMonitorProgressWorkQueue(queueName);
        });
    }
}
exports.ClusterWorker = ClusterWorker;
exports.default = ClusterWorker;
//# sourceMappingURL=ClusterWorker.js.map