import * as storage     from 'node-persist';
import { createHash }   from 'crypto';
import { config }       from 'edgecommonconfig';
import EdgeApi          from 'edgeapi';

/**
 * This is the base class for a table that is stored on a remote
 * storage technology. Right now this is coded against the Edge API for loading / saving records.
 */
export default class TableBase {

    private api                 : any;
    private isConnected         : boolean;
    private marks               : any;
    
    id                          : string;
    data                        : any;
    dataSet                     : string;
    tableName                   : string;

    constructor(dataSet: string, tableName: string, id: string) {
        this.dataSet             = dataSet;
        this.tableName           = tableName;
        this.id                  = id;
        this.api                 = null;
        this.isConnected         = false;
        this.marks               = null;
    }

    isset(path: string) {
        let base  = this.data;
        let parts = path.split(/[\/\.]/);
        
        while (parts.length > 0) {
            const name = String(parts.shift());

            if (!base) { return false; }
            if (!base[name]) { return false; }

            base = base[name];
        }

        if (!base) { return false; }

        return true;
    }

    /**
     * Returns a value give a path such as /test/data/name or test.name
     */
    safeString(path: string) {
        let base  = this.data;
        let parts = path.split(/[\/\.]/);

        while (parts.length > 0) {
            const name = String(parts.shift());

            if (!base) { return ''; }
            if (!base[name]) { return ''; }

            base = base[name];
        }

        if (!base) { return ''; }

        return base.toString();
    }

    safeNumber(path: string) {
        let base  = this.data;
        let parts = path.split(/[\/\.]/);

        while (parts.length > 0) {
            const name = String(parts.shift());

            if (!base) { return 0; }
            if (!base[name]) { return 0; }

            base = base[name];
        }

        if (!base) { return 0; }
        if (typeof base === 'number') { return base; }

        return parseFloat(base);
    }

    safeBool(path: string) {
        let base  = this.data;
        let parts = path.split(/[\/\.]/);

        while (parts.length > 0) {
            const name = String(parts.shift());

            if (!base) { return false; }
            if (!base[name]) { return false; }

            base = base[name];
        }

        if (!base) { return false; }
        if (base === true ||
            base === 'Yes' || 
            base === 'Y' ||
            base === 'yes' ||
            base === 'true' ||
            base === '1' ||
            base === 1) { 
            return true; 
        }

        return false;
    }

    /**
     * Called after a connection is established for the first time.
     * Should be used to check indexes and such.
     */
    async onVerifyModel() {
        return true;
    }

    /**
     * You can overwrite this function to see what changed.
     */
    async onSaved(totalChanges: number, newData: any) {
        return true;
    }

    /**
     * Potentially subject changes to this record.
     */
    async doSave() {
        if (!this.data) {
            throw new Error(`Invalid call to doSave on ${this.tableName}, id=${this.id}, not loaded`);
        }

        config.status('doSave::doConnect()');
        await this.doConnect();

        if (this.marks) {
            // See what data has changed before submitting
            let newData: any = {};
            let totalChanges = 0;

            for (let varName in this.data) {
                if (varName === '_id' || varName === '_lastModified') {
                    continue;
                }
    
                const recordHash = createHash('sha1');
                EdgeApi.getDataHash(recordHash, this.data[varName]);
                const hashValue = recordHash.digest('hex');

                if (!this.marks[varName] || hashValue !== this.marks[varName]) {
                    newData[varName] = this.data[varName];
                    totalChanges ++;
                    config.status(`Change found ${this.dataSet}://${this.tableName}/${this.id} for ${varName}`);
                    this.marks[varName] = hashValue;
                } else {
                    config.status(`No change in ${this.dataSet}://${this.tableName}/${this.id} for ${varName} [${hashValue}]`);
                }
            }

            if (totalChanges === 0) {
                await this.onSaved(0, null);
                return 0;
            }

            newData._lastModified = new Date();
            config.status(`${this.dataSet}://${this.tableName}/${this.id} -- Found ${totalChanges} total changes`, newData);
            await this.api.data_doUpdatePath(this.dataSet, `/${this.tableName}/${this.id}`, newData);
            config.status('Calling on Saved');
            const result = this.onSaved(totalChanges, newData);

            if (result) {
                await result;
            }

            return totalChanges;
        } else {
            config.status(`doSave::data_doUpdatePath ${this.dataSet}, /${this.tableName}/${this.id}`);
            await this.api.data_doUpdatePath(this.dataSet, `/${this.tableName}/${this.id}`, this.data);
            await this.onSaved(10000, this.data);
        }

        config.status('Saved');
        return true;
    }

    async doVerifyLoaded() {
        config.status('TableBase doVerifyLoaded Starting');
        await this.doConnect();

        // Check to see if we have the hash
        try {
            const cacheKey       = `${this.dataSet}:${this.tableName}/${this.id}`;
            const existingRecord = storage.getItemSync(cacheKey);

            if (existingRecord) {
                // Check the hash
                config.status(`TableBase doVerifyLoaded Gettting If Newer /${this.tableName}/${this.id}`);
                const tmpData = await this.api.data_doGetItemIfNewer(this.dataSet, 
                        `/${this.tableName}/${this.id}`, 
                        existingRecord._hash);

                if (typeof tmpData === 'object') {
                    this.data = tmpData;
                } else {
                    this.data = existingRecord;
                }
            } else {
                config.status(`TableBase doVerifyLoaded Getting Item /${this.tableName}/${this.id}`);
                this.data = await this.api.data_doGetItem(this.dataSet, `/${this.tableName}/${this.id}`);
                await storage.setItem(cacheKey, this.data);
            }
        } catch (e) {
            config.status('Exception in doVerifyLoaded:', e);
            this.data = await this.api.data_doGetItem(this.dataSet, `/${this.tableName}/${this.id}`);
        }
        
        config.status('doVerifyLoaded Marking values');
        this.markDataValues();

        return this.data;
    }

    /**
     * Execute an aggregate
     */
    async doAggregate(aggList: any) {
        await this.doConnect();
        const result = await this.api.data_doAggregate(this.dataSet, `/${this.tableName}`, aggList);
        return result;
    }

    /**
     * Load a single record
     */
    async doFind(condition: any) {
        const all = await this.doFindAll(condition, null, 0, 1);
        if (!all) { return null; }
        if (all.length === 0) { return null; }

        return all[0];
    }

    async doRemove(condition: any) {
        if (!this.isConnected) {
            await this.doConnect();
        }

        await this.api.data_doRemove(this.dataSet, this.tableName, condition);
        
        return true;
    }

    async doFindAll(condition?: any, sortCondition?: any, offset?: number, limit?: number) {
        const className = this.constructor.name;

        if (!condition) {
            condition = '';
        }

        if (typeof condition === 'object') {
            let strCondition = '';
            
            for (let varName in condition) {
                if (strCondition.length > 0) {
                    strCondition += ',';
                }
                strCondition += `${varName}:${condition[varName]}`;
            }
            
            condition = strCondition;
        }

        sortCondition = sortCondition || { _id: 1 };
        offset        = offset || 0;
        limit         = limit || 1000;

        await this.doConnect();
        const all = await this.api.data_doGetItems(this.dataSet, 
                `/${this.tableName}/${condition}`, 
                null, 
                sortCondition, 
                offset, 
                limit);
        let results = [];

        for (let obj of all) {
            let oneItem     = new TableBase(this.dataSet, this.tableName, obj.id);
            oneItem.data    = obj;

            results.push(oneItem);
        }

        return results;
    }

    async doVerifyIndex(keyName: string, indexType: any, isUnique: boolean) {
        if (!this.isConnected) {
            await this.doConnect();
        }

        const result = await this.api.data_doVerifySimpleIndex(this.dataSet, 
                this.tableName, 
                keyName, 
                indexType, 
                isUnique);

        return result;
    }

    /**
     * Called when an object is loaded to create hash values from the top level attributes
     * this way we can avoid sending them to save if they don't change
     */
    private markDataValues() {
        config.status('TableBase::markDataValues Start');
        this.marks = {};

        for (let varName in this.data) {
            if (varName === '_id' || varName === '_lastModified') {
                continue;
            }

            const recordHash = createHash('sha1');
            EdgeApi.getDataHash(recordHash, this.data[varName]);
            this.marks[varName] = recordHash.digest('hex');
        }

        config.status('TableBase::markDataValues Complete');

        return true;
    }

    private async doConnect() {
        config.status('TableBase doConnect Starting');
        this.api         = await EdgeApi.doGetApi();
        this.isConnected = true;

        config.status('TableBase Verify Model Starting');
        const result = this.onVerifyModel();

        if (result && result.then) {
            await result;
        }

        config.status('TableBase doConnect Connected');

        return true;
    }

}