class ViewEditor extends View

    getLayout: () =>
        view: "DockedToolbar"
        items: [
            type: "button"
            text: "Save"
            icon: "fa fa-save"
            callback: @onSave
        ,
            type: "button"
            text: "Cancel"
            icon: "fa fa-close"
            callback: @onCancel
        ]
        main:
            view: "CodeEditor"
            name: "viewCodeEditor"

    onSave: (e) =>
        # console.log "Save is clicked...: ", e
        annotations = @viewCodeEditor?.getSession?().getAnnotations?()
        if annotations? and annotations.length
            for a in annotations
                if a.type is "error"
                    m = new ModalForm
                        title:        "Error"
                        content:      "Invalid code, please try again."
                        position:     "top"

        if @saveFunc? and typeof @saveFunc is "function" then @saveFunc(@viewCodeEditor.getContent())
        @closePopup()        

    onCancel: (e) =>
        # console.log "Cancel is clicked...: ", e
        @closePopup()

    ##
    ## Just a fake function to make all the code lines using this function still work
    showEditor: () => 
        true

    ## -------------------------------------------------------------------------------------------------------------
    ## function to get the created editor instance
    ##
    ## @return [CodeEditor] codeEditor
    ##
    getEditorInstance: ->
        return @codeEditor

    ## -------------------------------------------------------------------------------------------------------------
    ## clears the html of the editor that is used to remove the editor
    ##
    clear: ->
        @editorWidget.html ""

    ## -gao
    ## set function used to save content of editor
    ##
    setSaveFunc: (@saveFunc)=>

    ## -gao
    ## set editor's settings
    ##

    applyCodeEditorSettings: (@codeMode, @content, @theme, @popupMode) =>
        if !@viewCodeEditor? then return false
        if @codeMode then @viewCodeEditor.setMode @codeMode
        if @content then @viewCodeEditor.setContent @content
        if @theme then @viewCodeEditor.setTheme @theme

        if @popupMode? and typeof @popupMode is "boolean" then @viewCodeEditor.setPopupMode @popupMode

    setEditorTheme: (@theme)=>
        if @viewCodeEditor? and @theme then @viewCodeEditor.setTheme @theme

    setEditorMode: (@codeMode)=>
        if @viewCodeEditor? and @codeMode then @viewCodeEditor.setMode @codeMode

    setEditorPopupMode: (@popupMode)=>
        if @viewCodeEditor? and @popupMode? and typeof @popupMode is "boolean" then @viewCodeEditor.setPopupMode @popupMode
    
    setEditorOptions: (@_options)=>
        if @viewCodeEditor? and @_options then @viewCodeEditor.setOptions @_options
        this
    setEditorContent: (val) =>

        if !val
            @content = ''
        else if typeof val isnt 'string'
            @content = val.toString()
        else
            @content = val
        if @viewCodeEditor? and @content then @viewCodeEditor.setContent @content

    getEditorContent: ()=>
        @viewCodeEditor.getContent()

    serialize: () =>
        return 
            theme:     @theme
            mode:      @codeMode
            popupmode: @popupMode
            content:   @content
            options:   @_options
            savefunc:  @saveFunc

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? then reject "Invalid options passed."

            if options.theme?
                @setEditorTheme options.theme 

            if options.mode?
                @setEditorMode options.mode

            if options.popupmode?
                @setEditorPopupMode options.popupmode 

            if options.options?
                @setEditorOptions options.options 

            if options.content?
                @setEditorContent options.content

            if options.savefunc? and typeof options.savefunc is "function"
                @setSaveFunc options.savefunc 

            resolve true
