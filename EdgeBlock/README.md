# EdgeBlock
> This is the base class for all self-contained data transformation blocks in the Edge System.

    npm install --save git+ssh://git@gitlab.protovate.com:Edge/EdgeBlock.git
    import EdgeBlock from 'edgeblock';

## Code Sample

This is example code about how we extend EdgeBlock in AddressNormalizer
EdgeBlock defines some common methods we can use in child classes.

    class AddressNormalizer extends EdgeBlock {
    }

## Methods

### doGetApi
This method calls doGetApi method of EdgeApi and return the response

### doGetLink(link, qs)
This method download the link with query string

### dateFromObjectId(objectId)
Given a mongo object ID, return a date based on when it was created

### setError(message, optionalData)
Error function to report one or more errors

### sendToQueue(queueName, data)
Send a record to a queue for additional processing

### updateStatsToday(name, keys)
Add stats for the day key a key as an object or scalar

## What exactly does an Edge Block do?

The goal of the Edge Block system is to quickly create blocks of code that handle a single purpose on an object
in a way that is re-usable.   For example, suppose you want to validate a user's profile has a valid birthday
or need to add pricing information to an item from another source.

* Edge Blocks are self-documenting - Each must have a function that describes the parameters required

* Edge Blocks make assumings about incoming data based on model - For some common functions we have to
agree on a common model and those are described later in this document or agreed within a document in
the project using them.   For example, we say that an address should have a house number, street name,
state, city, and so on and we have named them as such.

* Edge Blocks have access to common based class commands to update stats, perform logging, and
otherwise interact with the rest of the Edge System as needed.

* Edge Blocks are self contained so that they can run on any node in the Edge Hub and in the future
run on nano-instances or docker instances or some other place in order to scale.

* Edge Blocks are self testable.   Each block should have a test function in place that is able to use
the agreed testing methods (currently Mocha, Should, and Expect) to verify it works.

* Edge Blocks can be global in that we host them for all Edge projects or specific to a use cases
and hosted locally in another project.

* Eventually there will be a GUI created that allows Edge Blocks to chain and test.   This is why
we have a specific documentation format for parameters in and out and specific ways to define
credentials needed.


# Common Class functions

TODO:  These need to be described and standardized

## getInputs
> Returns a list of the inputs

## getOutputs
> Returns a list of the outputs

## process
> Processes a block of data
