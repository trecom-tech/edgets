import * as os            from 'os';
import * as numeral       from 'numeral';
import chalk              from 'chalk';

import config                from 'edgecommonconfig';
import DataSetManager        from 'edgedatasetmanager';
import EdgeApi               from 'edgeapi';
import {
    EdgeLauncher,
    NpmModuleTools
}                            from 'edgecommonlauncher';
import StatusUpdateClient    from 'edgecommonstatusupdateclient';
import { IJob }              from "./interfaces/IJob";
import { DelayedWinstonLog } from "edgecommonconfig/lib/Config";

const wqName = 'queue_jobs';

interface IServerDetail {
    host: string;
    total_mem: number;
    free_mem: number;
    load_Avg: number[];
}

export class JobServer {
    private api                : EdgeApi;
    private basePath           : string;
    private dbLogs             : DataSetManager;
    private homePath           : string;
    private jobs               : { [key: string]: IJob };
    private jobDelay           : number;
    private log                : DelayedWinstonLog;
    private refreshing         : boolean;
    private serverDetail       : IServerDetail;
    private statusUpdateClient : StatusUpdateClient;
    private busyGetNextJob     : boolean | null;

    constructor() {
        this.dbLogs = new DataSetManager('logs');
        this.log    = config.getLogger('JobRunner');
        const pathParts = __dirname.split('/');
        pathParts.pop();
        this.basePath   = pathParts.join('/') + '/';
        this.homePath   = pathParts.join('/');
        this.refreshing = false;
        this.jobs = {};
    }

    async refreshStatus(): Promise<boolean> {
        try {
            this.jobDelay               = 0;
            this.serverDetail = {
                host: os.hostname(),
                total_mem: Math.ceil(os.totalmem()),
                free_mem: Math.ceil(os.freemem()),
                load_Avg: os.loadavg()
            };

            if (this.serverDetail.load_Avg[0] > 2) {
                this.jobDelay = 3000;
            } else if (this.serverDetail.load_Avg[0] > 4) {
                this.jobDelay = 30000;
            } else if (this.serverDetail.load_Avg[0] > 6) {
                this.jobDelay = 300000;
            } else if (this.serverDetail.load_Avg[0] > 8) {
                this.jobDelay = 3000000;
            }

            let str = `[Host = ${chalk.gray(this.serverDetail.host)}] `;
            str += `[Total Mem = ${numeral(this.serverDetail.total_mem / 1024000).format('#,###')} MB] `;
            str += `[Free Mem = ${chalk.green(numeral(this.serverDetail.free_mem / 1024000).format('#,###'))} MB] `;
            str += `[Load = ${chalk.magenta(numeral(this.serverDetail.load_Avg[0]).format('#.##'))} `;
            str += numeral(this.serverDetail.load_Avg[1]).format('#.##') + ' ';
            str += numeral(this.serverDetail.load_Avg[2]).format('#.##') + '] ';
            str += `[Delay = ${numeral(this.jobDelay).format()}] `;

            await JobServer.getPendingCount()
                .then((count: any) => {
                    return console.log(`JobServer ${str}{${count}}`);
                });

            return true;
        } catch(e) {
            return false;
        }
    }

    static async getPendingCount(): Promise<any> {
        try {
            const total = await EdgeApi.cacheGetSetLength(wqName);
            config.status('Currently pending in list:', total);

            return total;
        } catch (e) {
            return null;
        }
    }

    //  A really simple delay as a promise
    promiseDelayed(timeout?: any) {
        if (timeout == null) {
            timeout = 1000;
        }

        return new Promise((resolve) => {
            return setTimeout(resolve, timeout, `${timeout} delayed`);
        });
    }

    //  Queue job to job db
    async queueJob(job: IJob): Promise<any> {
        try {
            const db = new DataSetManager('os');
            await db.doUpdate(`/job/${job.id}`, job);
            return true;
        } catch (e) {
            return null;
        }
    }

    //  Load a list of jobs and save them into an array.
    async getJobsList(): Promise<IJob[]> {
        try {
            const db      = new DataSetManager('os');
            let jobs_list: IJob[] = await db.doGetItems('/job/', null, null, 0, 5000);

            //  Random order for the jobs list to go through
            jobs_list = jobs_list.sort(() => {
                return Math.random() - Math.random();
            });
            for (let job of jobs_list) {
                this.jobs[job.id] = job;
            }

            return jobs_list;

        } catch (e) {
            return [];
        }
    }

    async refreshJobList(): Promise<boolean> {
        try {
            //
            //  TODO CHanged @db from MongoGateway to DataSetManager
            //  need to change doFind to doGetItems
            const jobs_list: IJob[] = await this.getJobsList();

            if (this.refreshing) {
                await this.promiseDelayed();
                return true;
            }

            this.refreshing = true;

            this.log.info('Refreshing job list');
            config.status('Refreshing Job List');

            for (let job of jobs_list) {
                // if (job.npm == null) {
                //     continue;
                // }

                if (JobServer.shouldJobRun(job) !== false) {
                    EdgeApi.cacheSetAdd(wqName, job.id);
                }
            }

            let total = await JobServer.getPendingCount();
            console.log('total====>', total);
            this.log.info('Running jobs', {total});

            if (total < 5) {
                await this.promiseDelayed(1000 * 60);
            }

            if (!this.api) {
                this.api = await EdgeApi.doGetApi();
            }

            this.refreshing = false;

            return true;
        } catch (e) {
            return false;
        }
    }

    //  Returns true if a job should run
    static shouldJobRun(job: any): boolean | number {
        try {
            if (!job.enabled) {
                return false;
            }

            //  Never run before so run it now
            if (job.lastRun == null) {
                return 9999999;
            }

            //  See how old the job is
            const now = new Date().getTime();

            if (job.freqType === 'hourly') {
                let diff = now - new Date(job.lastRun).getTime();
                diff /= 1000;
                diff /= 60;
                diff /= 60;
                // console.log(`Job #{job.id} lastRun=${job.lastRun} Diff=`, diff, ' vs ', job.freqRate)

                if (diff > job.freqRate) {
                    return (diff - job.freqRate);
                }
            }

            if (job.pending != null && job.pending) {
                return new Date(job.lastRun).getTime();
            }

            return true;

        } catch (e) {

            return false;

        }
    }

    async getNextJob(): Promise<IJob | null> {
        try {
            if (this.busyGetNextJob != null) {
                await this.promiseDelayed(1000);
                return this.getNextJob();
            }

            this.busyGetNextJob = true;

            if (this.jobs == null) {
                await this.getJobsList();
            }

            const record:any = await EdgeApi.cacheSetPop(wqName);

            if (record == null) {
                config.status('getNextJob, no record returned');
                await this.refreshJobList();
                this.busyGetNextJob = null;
                return this.getNextJob();
            }

            let job: any;
            job = this.jobs[record];

            if (job == null) {
                config.status(`getNextJob, job ${record} missing`);
                await this.getJobsList();
                job = this.jobs[record];
            }

            if (job == null) {
                config.status(`getNextJob, job ${record} missing - final`);
                this.busyGetNextJob = null;
                return null;
            }

            if (!job.enabled) {
                config.status(`Job ${record} has been disabled`);
                this.busyGetNextJob = null;
                await this.promiseDelayed(1000);
                return this.getNextJob();
            }

            config.status(`getNextJob return ${record}:`, this.jobs[record]);
            this.busyGetNextJob = null;
            this.jobs[record].lastRun = new Date();

            return this.jobs[record];

        } catch (e) {

            return null;

        }
    }

    //  Make sure the NPM is installed
    static async doVerifyInstalled(job: IJob): Promise<boolean> {
        try {
            let npmList: any;
            if (job == null || job.npm == null) {
                return false;
            }

            if (npmList == null) {
                npmList = {};
            }

            const last_run  = npmList[job.npm] || 0;
            let diff_time   = new Date().getTime() - last_run;

            diff_time /= 60000;

            // console.log(`Job ID: ${job.id} time=`, diff_time)
            if (diff_time < 60) {
                return true;
            }
            //  Install or update the NPM
            config.status(`Installing ${job.npm} in ${job.folder}`);

            await NpmModuleTools.verifyInstalled(job.npm, job.folder);

            npmList[job.npm] = new Date().getTime();

            return true;

        } catch (e) {

            return false;

        }
    }

    async executeJobs(slot_id: number): Promise<any> {
        try {
            // console.log(`Starting job loop, slot=${slot_id}`);
            let jobLoop: any;
            const runner = new EdgeLauncher();
            // return (jobLoop = async () => {
            //     config.status('Calling getNextJob');
            // });

            config.status('Calling getNextJob');

            jobLoop = await this.getNextJob()
                .then((job: IJob) => {
                    config.status('executeJobs Got job to run', job);
                    // console.log('Job=', job);

                    if (job != null) {
                        // runner.statusUpdateClient = this.statusUpdateClient;
                        // runner.api = this.api;
                        return runner.runJob(job, slot_id)
                            .then((result: any) => {
                                config.status('executeJobs Result:', result);

                                return setTimeout(jobLoop, this.jobDelay);
                            });
                    } else {
                        config.status('No jobs');

                        return setTimeout(jobLoop, this.jobDelay);
                    }
                });

            return jobLoop;

        } catch (e) {

            return null;

        }

    }

    async startSystemStatusLoop(): Promise<any> {
        try {
            //  Overall system status loop
            await this.refreshStatus();

            const statusInterval = setInterval(async () => {
                    return this.refreshStatus();
                }
                , 5000);

            let statusUpdateConnected = false;
            this.statusUpdateClient = new StatusUpdateClient();

            return await this.statusUpdateClient.doOpenChangeMessageQueue();

        } catch (e) {

            return null;

        }
    }
}

export default JobServer;
