"use strict";
//
// class for Text datatype - Edit as HTML/Popup
//
// @extends [DataFormatterType
//
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatMemo extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'memo';
        this.input_width = 300;
        this.align = 'align';
    }
    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        const cx = left + (width / 2);
        const cy = top - 10;
        let w = $(window).width();
        let h = $(window).height();
        if (w > 1000) {
            w = 1000;
        }
        else if (w > 800) {
            w = 800;
        }
        else {
            w = 600;
        }
        if (h > 1000) {
            h = 1000;
        }
        else if (h > 800) {
            h = 800;
        }
        else if (h > 600) {
            h = 600;
        }
        else {
            h = 400;
        }
        let codeMode = "markdown";
        if (typeof this.options === "string") {
            codeMode = this.options;
        }
        //  Show a popup menu
        // @ts-ignore
        doPopupView("WysEditor", "MemoEditor", "memoeditor", w, h, view => {
            view.showEditor();
            view.wysEditor.setContent(currentValue);
            return view.wysEditor.createButton("Save", "Save editor's content", () => {
                return this.saveValue(view.wysEditor.getContent());
            });
        });
        return true;
    }
    //
    //  Attach to an existing text input.
    //  Hide the text input and add the div
    //  and then use Summernote Editor
    attachEditor(elSourceField, path) {
        super.attachEditor(elSourceField, path);
        const options = DataFormatCommon_1.DataFormatCommon.getOptions(elSourceField.data("options"));
        const config = {};
        // # Enable AirMode of summernote
        config.airMode = true;
        config.maxHeight = 100;
        config.height = 96;
        //
        //  Copy forward any config
        for (let name in options) {
            const value = options[name];
            config[name] = value;
        }
        //
        elSourceField.hide();
        elSourceField.elSummernoteWrapper = $("<div />");
        // # apply some styles
        elSourceField.elSummernoteWrapper.height(110);
        elSourceField.elSummernoteWrapper.css('padding', '5px');
        elSourceField.elSummernoteWrapper.css('border', '1px solid grey');
        elSourceField.elSummernoteWrapper.css('border-radius', '3px');
        elSourceField.elSummernoteWrapper.css('overflow-y', 'scroll');
        elSourceField.elSummerNoteBody = $("<div />");
        elSourceField.elSummernoteWrapper.append(elSourceField.elSummerNoteBody);
        elSourceField.parent().prepend(elSourceField.elSummernoteWrapper);
        // @ts-ignore
        const wysEditor = new WysEditor(elSourceField.elSummerNoteBody, config);
        // console.log "DataFormatMemo Created with options:", config
        elSourceField.val = (newValue) => {
            if ((newValue == null)) {
                return wysEditor.getText();
            }
            wysEditor.setText(newValue);
            return newValue;
        };
        return true;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null)) {
            return "";
        }
        if ((data.length === 0) || (data.length === undefined)) {
            return "";
        }
        return `<span class='memo'>${data.slice(0, 201)}</span><span class='fieldicon'><i class='si si-eyeglasses'></i></span>`;
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return data;
    }
    onFocus(e, col, data) {
        const text = data[col];
        if ((text != null) && (typeof text === "string") && (text.length > 0)) {
            const content = `<br><textarea style='width:100%; height: 600px; font-size: 16px; line-height: 20px; font-family: Consolas, monospaced, arial;'>${text}</textarea>`;
            // @ts-ignore
            const m = new ModalDialog({
                showOnCreate: true,
                content,
                title: "View Contents",
                ok: "Done",
                close: ""
            });
        }
        return true;
    }
}
exports.DataFormatMemo = DataFormatMemo;
exports.default = DataFormatMemo;
//# sourceMappingURL=DataFormatMemo.js.map