# EdgeMySQL

> Provides a standard interface to MySQL database

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonMySQL.git
    import EdgeCommonMySQL from 'edgecommonmysql';

## Install MySQL in ubuntu

    sudo apt update
    sudo apt install mysql-server
    sudo mysql_secure_installation

The detailed installation guide is here: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04

## Setup Credentials

1. The entire structure needs to be setup as a credential in the local configuration file.  
The person in charge of the credential file would need to edit save_creds_example.js in `~/EdgeConfig/`

```json
"MySQL": {
    host: "localhost",
    port: 3306,
    user: "root",
    password: "12345678",
    database: "test_trialserver",
    options: {
        connectionLimit: 3,
        connectTimeout: 2000,
        queueLimit: 0,
        debug: false,
        timezone: "UTC",
        waitForConnections: true
    }
}
```

2. Run this script in terminal:

    node save_creds_example.js

## Creating an instance

    const mysql = new EdgeCommonMySQL(configName); // "MySQL"

### Parameters

* configName: Pass in the config name as defined in the credential

## Helper function

### Run a query
    
* doRunQuery(sql ,callback): callback would be called per row fetched.

```
mysql.doRunQuery(sql, (err: any, result: any, fields: any) => {
    console.log(result);
});
```


## Run tests locally

Make sure you have installed MySQL. Also replace port, user, password in [test.ts](test/test.ts).

    npm install
    npm run test-local


