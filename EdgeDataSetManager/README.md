# DataSetManager
> The Edge interface into the data storage server

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeDataSetManager.git
    import EdgeDataSetManager from 'EdgeDataSetManager'


## Testing

### Testing Locally

Set DB_HOST to localhost:27017 and

    npm run test-local

### Testing via docker

Set DB_HOST to dockerized_mongo and

    docker-compose build 
    docker-compose up


## Classes in this package

### Understanding data interface

Client App ---> DataSetManager Class ---> Mongo Gateway ---> Mongo Model

| Class Name     | Description                                                |
|----------------|------------------------------------------------------------|
| DataSetManager | High level interface to work with Documents in database    |
| DataSetConfig  | Holds the configuration of each data set                   |
| DataSetPath    | Rules for converting a URL style path to a document        |
| MongoGateway   | Connection to a MongoDB Database                           |
| MongoModel     | A specific collection within the MongoDB Database          |


### Accessing data through a path

In the Edge system all data is mapped to a data set and then a path.   A data set is
a group of similar things and the path is the name of the collection and an ID.  So all
records in MongoDB will have a unique field called "id".

A path is usually a name and id and one or more sub documents, for example

* /user/brian
* /company/32
* /item/67/images/large

See [The Mocha Tests](test/test.coffee) to understand the options for a path.

## TODO: Document DataSetManager

@param path [string] the text path to parse or a DataSetPath object.
@param fieldList [mixed] An array of fields or an object in Mongo projection
@param sortCondition [mixed] An optional list of sorting with 1 or -1 as the field value
@param offset [number] An optional start index from the results
@param limit [number] An optional (defaulting to 1000) limit of the number of rows returned.

    doGetItems: (path, fieldList, sortCondition, offset, limit)

This one is same with above on function and return as array

    doGetItemsArray: (path, fieldList, sortCondition, offset, limit)

Return number of count search list with path on collection

@param path [string] the text path to parse or a DataSetPath object.

    doCount: (path)

Create or verify an index exists on a collection

@param tableName [string] The name of the table you want an index Mongo
@param keyField [string] The field name to make an index
@param indexType [string] The type of index if special such as geo
@param isUnqiue [boolean] Should the index be unique?

    doVerifySimpleIndex: (tableName, keyField, indexType, isUnique)


Save or update data to a path

@param path [String|object] Path can be a string path or a parsed path
@param newJson [mixed] the data to be added or updated
@param updateAgentID [string] the _id of an approved agent (optional)

    doUpdate: (path, newJson, updateAgentID)

Append the data to the path treating the path like an array.

@param path [String|object] Path can be a string path or a parsed path
@param newJson [mixed] the data to be added or updated
@param updateAgentID [string] the _id of an approved agent (optional)

    doAppend: (path, newJson, updateAgentID)

Delete data with path

@param path [String|object] Path can be a string path or a parsed path

    doDeletePath: (path)

Remove documents returned by search condition on collection

@param collectionName: [string] collection's name which has documents to remove
@param searchCondition: [string|object] condition to search documents

    doRemove: (collectionName, searchCondition)

Execute an aggregate against a table

@param path: [string] the text path to parse or a DataSetPath object.
@param aggList: [mixed] An object for aggregation options

    doAggregate: (path, aggList)

Open Database and Return Collection

@param databaseName: [string] database name to open with data set manager
@param collectionName: [string] collection name in database

    doOpen: (databaseName, collectionName)

Get a reference to the settings for given collection 

@param collectionName: [string] table name to get it

    doInternalGetSettings: (collectionName)

Get a list of collections and counts open

    doGetUpdatedStats: ()

DoFindNearby - Returns records that are nearby assuming that the destimation table has a "loc" field and that it   contains the 2dsphere index.
    
@param path [mixed] Can be a compiled path or string path
@param centerLocation [mixed] Should be an object with lat and lon definedor it can be a GeoJSON object with coordinates defined.
@param miles [number] the number of miles to search around
@param offset [number] Optional starting point in the result list (default 0)
@param limit [number] Optional the maximum number of results (default 1000)

    doFindNearby: (path, centerLocation, miles, offset, limit)



### Examples

Store a simple record


Get the last 100 records from a logs collection

    dmLogs = new DataSetManager("logs")
    @dmLogs.doGetItems "/jobrun", {}, { _id: -1 }, 0, 100
    .then (all)->

## TODO: Document DataSetPath

Parse path to understand what path options are possible

@param path: [string] DataSetPath Object
    
    parsePath: (path)


Return search condition for mongo with give condition string

@param basePath: [string] DataSetPath object
@param oneCondition: [string] condition string in 'DataSet' terms

    parsePathCondition: (basePath, oneCondition)


## TODO: Document DataSetConfig

Transform an object by applying the rules to it
    
@param path: [string] DataSetPath Object or path parsed by DataSetPath instance
@param object: [mixed] object for transform with path
@param isDebug: [boolean] set debug mode , if true debug mode and if false, not.

    doTransformObject: (path, object, isDebug)


Takes a value from one name and moves it directly to another (Copy command)
*required: sourceField, targetField

@param object: [mixed] object which has source and target field
@param command: [mixed] object for copy command

    applyCommandCopy: (object, command)


Add or Change transform rules

@param ruleName: [string] string to indicate rule's name
@param ruleXml: [xml] transform rules for adding or changing

    doSetTransformRule: (ruleName, ruleXml)


Load the settings and translations

    doLoadSettings: ()


Save to server

    save: ()

## TODO: Document how blockly works