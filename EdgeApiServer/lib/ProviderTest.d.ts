import ApiProvider from './ApiProvider';
import IApiUser from './interfaces/IApiUser';
export declare class ProviderTest extends ApiProvider {
    constructor(prototype: ApiProvider);
    onInit(): void;
    getBaseName(): string;
    /**
     * The most basic form of API call, /test/SimpleTest outputs the current system date.
     * http://localhost:8001/api/test/doSimpleTest.json
     */
    doSimpleTest(): string;
    doShowUser(user: IApiUser): {
        name: number;
        ip_address: string;
        authAttempts: number;
        timezoneOffset: number;
        browser: any;
        location: any;
    };
}
export default ProviderTest;
