class ViewSnapEditor extends View

    getDependencyList: ()=>
        return [ "/ace/ace.js", "/ace/ext-language_tools.js" ]

    onSetupButtons: () =>

    onResize: (pw, ph)=>
        h = @elHolder.parent().parent().height()
        true

    onShowScreen: ()=>

        navButtonSave = new NavButton "Save", "toolbar-btn navbar-btn btn-primary"
        navButtonSave.onClick = (e)=>
            console.log "CODE=", codeEditor.getContent()

        @addToolbar [ navButtonSave, navButtonCancel ]

        tag = $ "<div />",
            id     : "editor_" + GlobalValueManager.NextGlobalID()
            height : 5000

        @popup.windowScroll.append tag

        codeMode = "javacript"
        if typeof @options == "string" then codeMode = @options

        codeEditor = new CodeEditor tag
        codeEditor.popupMode().setTheme "tomorrow_night_eighties"
            .setMode codeMode

        console.log "CURRENT=", currentValue

        if !currentValue
            code = ''
        else if typeof currentValue isnt 'string'
            code = currentValue.toString()
        else
            code = currentValue

        codeEditor.setContent code