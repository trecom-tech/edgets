class ViewStepper extends View

	##|
	##| setSize (see NinjaContainer) is called when the holder wants this dynamic steps
	##| to take up exactly some amount of space.  By default it will resize the steps and
	##| then onResize is called once the size has actually changed
	##|
	# setSize: (w, h)=>

	onResize: (w, h)=>
		super(w, h)

		##|
		##|  Update the visible step
		@refreshWidgetsSize()

		true

	##|
	##|  total the badge values from all children
	getBadgeText: ()=>

		total = 0
		for tag, id in (@tags or [])
			if tag.body? and tag.body.getBadgeText?
				num = tag.body.getBadgeText()
				if typeof num != "number" then num = parseInt(num)
				total += num

		if total > 0 then return total
		return null

	## -------------------------------------------------------------------------------------------------------------
	## Add a new step data to array named "stepData"
	##
	## @param [String] stepName the name of the step to be adding
	## @param [String] defaultHtml optional html for the step content
	## @param [Integer] order (optional) order of the step that should >= 0, if not specified, set it to -1
	## @return [Step] the new step Object which is created
	##
	addStepData: (stepName, icon, defaultHtml, order) =>
		if !order? then order = -1
		step =
			stepName   : stepName
			icon       : icon
			defaultHtml: defaultHtml
			order      : order
		@stepData.push step
		step

	updateSteps: ()=>

		for tag, id in @tags

			@updateTagContent tag, id

			if @getOrder(id) == @getOrder(@activeStep)
				tag.body.show()

				if @getInsideHeight() == 0
					tag.body.hide()
				else if tag.body.move?
					tag.body.show()
					tag.body.move @outerPadding, @height() - @getInsideHeight() - @footerH - @innerPadding - @outerPadding, @getInsideWidth(), @getInsideHeight()

				try
					if tag isnt @lastActiveStep
						@internalCallStepHide(@lastActiveStep)
						@internalCallStepShow(tag)

					@lastActiveStep = tag
				catch e
					console.log "Error in Stepper - onStepShow or onStepHide with tag: ", e, tag
				
			else
				tag.body.hide()

		@updateBadges()
		@updateButtons()
		true


	##|
	##|  Internal helper function that calls onTabShow for a tag.
	internalCallStepShow: (tag)=>
		if !tag? then return false
		if !tag.body? then return false
		if tag.body.onStepShow?
			tag.body.onStepShow(tag);
			return true
		if tag.body.childView? and tag.body.childView.onStepShow?
			tag.body.childView.onStepShow(tag.body.childView)
			return true
		return false

	##|
	##|  Internal helper function that calls onTabShow for a tag.
	internalCallStepHide: (tag)=>
		if !tag? then return false
		if !tag.body? then return false
		if tag.body.onStepHide?
			tag.body.onStepHide(tag);
			return true
		if tag.body.childView? and tag.body.childView.onStepHide?
			tag.body.childView.onStepHide(tag.body.childView)
			return true
		return false		

	updateBadges: ()=>
		if !@tags? then return false
		for tag, id in @tags

			baseText = tag.badge
			if !baseText? and tag.body? and tag.body.getBadgeText?
				baseText = tag.body.getBadgeText()

			if baseText? and (typeof(baseText)=="number" or baseText.length > 0)
				tag.badgeText.html baseText
				tag.badgeText.show()
			else
				tag.badgeText.hide()

		true

	updateTagContent: (tag, id) =>
		if !tag? then return false
		tag.step.outerWidth(@getInsideWidth()/@tags.length)
		
		if !id? then id = @tags.indexOf(tag)
		if id < 0 then return false 
		
		if tag.icon? and tag.icon isnt ""
			tag.stepIcon.html "<div class='ninja-step-default-icon'><i class='#{tag.icon}'></i></div>"		
		else 
			tag.stepIcon.html "<div class='ninja-step-default-icon'>#{parseInt(tag.order)+1}</div>"
		
		if @getOrder(id) == @getOrder(@activeStep)
			tag.step.addClass "active"
		else if @getOrder(id) < @getOrder(@activeStep)
			tag.step.addClass "checked"
			tag.stepIcon.html "<div class='ninja-step-default-icon'><i class='fa fa-check'></i></div>"
		else
			tag.step.removeClass "active"

		true

	##|
	##|  Add a view to a step
	##|  Return (resolves) with the step
	##|  Calls callbackWithView with the new view
	##|  The promise is only complete after the callback completes.
	##|
	doAddViewStep : (viewName, stepText, icon, callbackWithView, sortOrder) =>

		new Promise (resolve, reject) =>

			gid = GlobalValueManager.NextGlobalID()
			step = @addStep stepText, icon, null, sortOrder
			step.body.setView viewName, callbackWithView
			.then (view)=>
				view.elHolder = step.body.el
				resolve(view)

			true

	##|
	##|  Add a table to a step which is a common function so
	##|  we have included management for steps with tables globally
	##|
	doAddTableStep : (tableName, stepText, icon, sortOrder = null, callbackWithTableView) =>

		new Promise (resolve, reject)=>

			@doAddViewStep "Table", stepText, (view, viewText)=>

				if !@tables? 
					@tables = {}
				table                   = view.loadTable tableName
				table.showCheckboxes    = true
				@tables[tableName]      = table
				@tables[tableName].step = @steps[stepText]
				@steps[stepText].table  = table

				if callbackWithTableView?
					callbackWithTableView(view)

				return true
			, sortOrder

			.then (step)=>

				# total = @steps[stepText].table.getTableTotalRows()
				# console.log "Setting Badge [#{stepText}] to #{total}:", @steps[stepText].table
				# @steps[stepText].setBadge(total)
				resolve(@steps[stepText])

	onCheckTableUpdateRowcount: (tableName, newRowCount)=>

		if !@tables? then return
		# console.log "onCheckTableUpdateRowcount table=#{tableName} new=#{newRowCount}"

		if @tables[tableName]?
			@tables[tableName].step.badgeText.html newRowCount
			@tables[tableName].step.badgeText.show()
			@tables[tableName].step.badge = newRowCount

		true

	## -------------------------------------------------------------------------------------------------------------
	## Add a new step to current instance
	##
	## @param [String] tabName the name of the step to be adding
	## @param [String] defaultHtml optional html for the step content
	## @return [Step] the new step Object which is created
	##
	addStep: (stepName, icon, defaultHtml, sortOrder = null, titleHTML) =>

		##|
		##|  Return existing step
		if @steps[stepName]? then return @steps[stepName]

		##|
		##|  Default sort order puts the step at the end
		if !sortOrder? or typeof sortOrder != "number"
			sortOrder = @tags.length

		##|
		##|  Find where to insert
		insertPosition   = 0
		for child in @stepList.getChildren()
			if sortOrder > child.sortOrder then insertPosition++

		id               = @stepCount++
		elStep           = @stepList.addAtPosition "li", "ninja-nav-step", insertPosition
		elStep.sortOrder = sortOrder

		elStep.setDataPath id
		elStep.on "click", @onClickStep
		elStepIcon   = elStep.add "div", "ninja-stepper-icon"
		if icon? and icon isnt ""
			elStepIcon.html "<div class='ninja-step-default-icon'><i class='#{icon}'></i></div>"
		else
			elStepIcon.html "<div class='ninja-step-default-icon'>#{sortOrder+1}</div>"
		
		elStepText       = elStep.add "div", "ninja-step-text"
		if titleHTML? and titleHTML isnt ""
			elStepText.html titleHTML
		else
			elStepText.html stepName

		elStepBadge      = elStep.addDiv "ninja-badge"

		elBody           = @stepContent.add "div", "ninja-nav-body"
		elBody.on "keyup", (e) =>
			if e.keyCode is 13
				# console.log "Stepper body Enter is clicked...", e
				@getActiveStep()?.goNext()
				return false

		if defaultHtml?
			elBody.html defaultHtml

		if insertPosition == 0
			@activeStep  = id

		@orderList.splice insertPosition, 0, id

		@tags.push
			name:      stepName
			id:        id
			title:     stepName
			icon:      icon
			html:      defaultHtml
			order:     sortOrder
			titlehtml: titleHTML
			parent:    this
			step:      elStep
			body:      elBody
			stepIcon:  elStepIcon
			stepText:  elStepText
			badgeText: elStepBadge
			setBadge:  @onSetBadge
			show: ()=>
				@activeStep = id
				@updateSteps()
			goPrev: ()=>
				if @checkFirstStep(id) then return false
				@moveStep id, -1
				true
			goNext: (bool)=>
				if @checkLastStep(id) then return @onSubmit()
				
				activeStep = @getActiveStep()
				f          = activeStep.body.childView?.wizardAllowContinue
				
				if f? and typeof f is "function"
					result = f(this, id)
					if result? and result.then? and typeof result.then == "function"
						return result.then (nextResult)=>
							@moveStep id, 1, nextResult
					return @moveStep id, 1, result
				
				@moveStep id, 1
				true
		##|
		##|  A reference to the data by name
		@steps[stepName] = @tags[id]
		@updateSteps()

		return @tags[id]

	moveStep: (current, offset, flag) =>
		if flag? and flag is false then return false
		# if !current? or !offset? then return false
		if !current? then current = @activeStep
		if !offset? then offset = 1
		pos = @orderList.indexOf(current) + offset
		if pos < 0 or pos > @orderList.length - 1 then return false
		@show @orderList[pos]
		true

	goToFirstStep: () =>
		@show @orderList[0]
		true

	goToLastStep: () =>
		@show @orderList[@orderList.length - 1]
		true

	onSetBadge: (num, classname)->
		id                     = this.id
		@parent.tags[id].badge = num

		if classname?
			@parent.tags[id].badgeText.addClass("badge-" + classname)

		return @parent.updateBadges()

	getOrder: (id) =>
		@orderList.indexOf id

	onClickStep: (e)=>
		# console.log "DynamicSteps onClickStep"
		if !e? or !e.path? then return false
		
		id = e.path
		if !@isEnablePrevious and @getOrder(id) < @getOrder(@activeStep)
			return false

		if !@isEnableSkip and @getOrder(id) > @getOrder(@activeStep)
			return false

		# if id is @activeStep - 1 then return @getActiveStep().goPrev()
		# if id is @activeStep + 1 then return @getActiveStep().goNext()
		@show id
		true

	getActiveStep: ()=>
		return @tags[@activeStep]

	show: (id, flag)=>
		if flag is false then return false
		if !id? then return false
		if typeof id == "object" and id.id? then id = id.id
		if @tags[id]?
			@emitEvent "showstep", [ id, @tags[id] ]
			@activeStep = id
			@updateSteps()

		return true

	getStep: (stepName) =>

		##|
		##|  Return existing step
		if @steps[stepName]? then return @steps[stepName]
		return null

	##|
	##|  Returns the size of the space available for the step content
	getInsideHeight: ()=>
		h = @height()
		h -= 2 * (@innerPadding + @outerPadding)
		h -= (@stepHeight+1)
		h -= @footerH
		if @title then h -= @titleH
		if @subTitle then h -= @subTitleH
		return h

	##|
	##|  Returns the size of the space available width
	getInsideWidth: ()=>
		return @getWidth() - 2 * @outerPadding

	onShowScreen: ()=>

		@tags       = []
		@steps      = {}
		@orderList  = []
		@stepCount  = 0
		@activeStep = null
		@stepHeight = 80
		@titleH 	= 80
		@subTitleH	= 40
		@footerH    = 80
		@buttonH	= 50
		@buttonW 	= 100
		@txtPrevBtn = "Previous"
		@txtNextBtn = "Next"
		@txtSubmit  = "Submit"

		## flags
		@isEnablePrevious = true
		@isEnableSkip     = false
		@isEnableSubmit   = true

		## paddings between sections - header, body, footer
		@innerPadding = 30

		## padding in four borders of the entire step view
		@outerPadding = 20

		@el.addClass "ninja-steps"
		@stepList    = @add "ul", "ninja-step-list", "ninja-step-list"
		@stepList.height(@stepHeight)
		@stepIconBar     = @stepList.add "div", "ninja-step-icon-bar"

		@addDiv "clr"
		@stepContent = @add "div", "ninja-step-content step-content"
		@stepData  	 = []

		## buttons
		@buttonContainer = @add "div", "ninja-stepper-button-container"
		
		@prevBtnWgt = @buttonContainer.add "button", "btn ninja-form-button ninja-stepper-button ninja-stepper-prev-button"
		@nextBtnWgt = @buttonContainer.add "button", "btn ninja-form-button ninja-stepper-button ninja-stepper-next-button"
		@updateButtonsPosSize()
		@prevBtnWgt.text @txtPrevBtn
		@nextBtnWgt.text @txtNextBtn
		@prevBtnWgt.bind "click", (e) =>
			# console.log "Previous button is clicked..."
			@getActiveStep().goPrev()
			true
		@nextBtnWgt.bind "click", (e) =>
			if @checkLastStep(@activeStep) and @isEnableSubmit
				@onSubmit e
			else 
				# console.log "Next button is clicked..."
				@getActiveStep().goNext()
			true
		##|
		##|  Allow this view to have events
		GlobalClassTools.addEventManager(this)

		##|
		##|  If someone updates the number for the badge, referesh the steps
		globalTableEvents.on "row_count", ()=>
			@updateBadges()

		true

	checkFirstStep: (id) =>
		if !id? then return false
		return @orderList.indexOf(id) is 0

	checkLastStep: (id) =>
		if !id? then return false
		return @orderList.indexOf(id) == @orderList.length - 1

	##
	## function to set title of this view
	setTitle: (@title, classes) =>
		if !@title then return false
		if typeof @title isnt "string" or @title.length is 0 then return false

		## apply css classes if any		
		@titleClasses = []
		if classes?
			if typeof classes is "string" then @titleClasses = classes.split " "
			if Array.isArray(classes) is true then @titleClasses = classes
		
		if !@titleWgt?
			@titleWgt = @addAtPosition "div", "ninja-stepper-title #{@titleClasses.join(" ")}", 0
			@refreshWidgetsSize()
		@titleWgt.html @title
		true

	##
	## function to set sub title of this view
	setSubTitle: (@subTitle, classes) =>
		if !@subTitle then return false
		if typeof @subTitle isnt "string" or @subTitle.length is 0 then return false
		
		## apply css classes if any		
		@subTitleClasses = []
		if classes?
			if typeof classes is "string" then @subTitleClasses = classes.split " "
			if Array.isArray(classes) is true then @subTitleClasses = classes
		
		if !@subTitleWgt?
			@subTitleWgt = @addAtPosition "div", "ninja-stepper-subtitle #{@subTitleClasses.join(" ")}", (order = if @title? then 1 else 0)
			@refreshWidgetsSize()
		h = if @title then @titleH else 0
		@subTitleWgt.html @subTitle
		true

	##
	## function to recalculate size(width and height) of title, subtitle and steps
	refreshWidgetsSize: () =>
		h = @outerPadding
		if @title
			@titleWgt.move @outerPadding, @outerPadding, @getInsideWidth(), @titleH
			h += (@titleH + @outerPadding)
		if @subTitle 
			@subTitleWgt.move @outerPadding, h, @getInsideWidth(), @subTitleH
			h += @subTitleH

		containerX = @outerPadding
		containerW = @getWidth() - 2 * @outerPadding
		@stepList.move @outerPadding, h, containerW, @stepHeight
		@updateSteps()
		@updateButtonsPosSize()
		true

	## 
	## function to set text of previous button
	setTextPrevious: (str) =>
		if !str? or typeof str isnt "string" or str.length is 0 then return false
		@txtPrevBtn = str
		@prevBtnWgt.text @txtPrevBtn
		true

	## 
	## function to set text of next button
	setTextNext: (str) =>
		if !str? or typeof str isnt "string" or str.length is 0 then return false
		@txtNextBtn = str
		@nextBtnWgt.text @txtNextBtn
		true

	##
	## function to set text of Submit
	setTextSubmit: (str) =>
		if !str? or typeof str isnt "string" or str.length is 0 then return false
		@txtSubmit = str
		@updateButtons()
		true

	##
	## set a flag to determine if it is possible to go back
	## default is true
	setEnablePrevious: (bool) =>
		@isEnablePrevious = bool
		if @isEnablePrevious and !@prevBtnWgt.visible
			@prevBtnWgt.show()
		else if !@isEnablePrevious and @prevBtnWgt.visible
			@prevBtnWgt.hide()
		true

	##
	## set a flag to determine if it's possible to skip the next step
	## default is false
	setEnableSkip: (bool) =>
		@isEnableSkip = bool

	##
	## set a flag to determine if it's possible to submit in the last step
	## default is true
	setEnableSubmit: (bool) =>
		@isEnableSubmit = bool

	##
	## show "Submit" instead of "Next" if enabled
	updateButtons: () =>
		if @checkLastStep(@activeStep) and @isEnableSubmit
			@nextBtnWgt.text @txtSubmit
		else
			@nextBtnWgt.text @txtNextBtn

		@prevBtnWgt.setClass "disabled", @checkFirstStep(@activeStep)  
		@nextBtnWgt.setClass "disabled", (@checkLastStep(@activeStep) and !@isEnableSubmit) or (@isEnableSubmit and @isSubmitting)
		# @updateButtonsPosSize()
		true

	updateButtonsPosSize: () =>
		buttonPadding = (@footerH - @buttonH) / 2
		@buttonContainer.move @outerPadding, @getHeight() - @footerH - @outerPadding, @getWidth() - 2 * @outerPadding, @footerH
		@prevBtnWgt.move @getWidth() - 2 * @outerPadding - 2 * @buttonW - 10, buttonPadding, @buttonW, @buttonH
		@nextBtnWgt.move @getWidth() - 2 * @outerPadding - @buttonW, buttonPadding, @buttonW, @buttonH
		true

	setSubmitFunction: (@onSubmitCallback) =>

	##
	## function to be called when "Submit" button is clicked
	onSubmit: (e) =>
		if @isSubmitting? and @isSubmitting is true then return false
		@isSubmitting = true
		result        = false
		if @onSubmitCallback?
			result = @onSubmitCallback this 
			if result? and result.then? and typeof result.then is "function"
				@updateButtons()
				return result.then (val) =>
					@isSubmitting = false
					@updateButtons()
					unless val is false
						@emitEvent "submit_steps", [ e ]
					return true

		unless result is false
			@emitEvent "submit_steps", [ e ]
		@isSubmitting = false
		true

	##
	## add custom classes to Button widgets
	addCustomButtonClasses: (classes) =>
		if !classes? then return false
		if !@buttonclasses? then @buttonclasses = []
		for cls in classes when !(cls in @buttonclasses)
			@prevBtnWgt.addClass cls 
			@nextBtnWgt.addClass cls 
			@buttonclasses.push cls
		true

	## No use right now
	setPadding: (value, key) =>
		if !value? then return false 

		## value is array just like CSS 'padding': [top, right, bottom, left]
		if Array.isArray(value) is true
			if value.length > 0 then @setPadding value[0], 'top'
			if value.length > 1 then @setPadding value[1], 'right'
			if value.length > 2 then @setPadding value[2], 'bottom'
			if value.length > 3 then @setPadding value[3], 'left'
			return true

		## value is Object like : { 'left': 10, 'right': 20, ...}
		if typeof value is "object"
			if value['top']? then @setPadding value['top'], 'top'
			if value['right']? then @setPadding value['right'], 'right'
			if value['bottom']? then @setPadding value['bottom'], 'bottom'
			if value['left']? then @setPadding value['left'], 'left'
			return true 

		## value is numeric, now key is required to be some valid string
		if isNaN(value) then return false
		if !key? or ['top', 'right', 'bottom', 'left'].indexOf(key) < 0 then return false 
		@bodyPadding[key] = value
		@updateSteps()
		return true

	setInnerPadding: (@innerPadding) =>
		@refreshWidgetsSize()

	setOuterPadding: (@outerPadding) =>
		@refreshWidgetsSize()

	setStepHeight: (@stepHeight) =>
		@refreshWidgetsSize()

	setFooterHeight: (@footerH) =>
		@refreshWidgetsSize()		

	setTitleHeight: (@titleH) =>
		@refreshWidgetsSize()

	setSubTitleHeight: (@subTitleH) =>
		@refreshWidgetsSize()

	setButtonHeight: (@buttonH) =>
		@refreshWidgetsSize()

	setButtonWidth: (@buttonW) =>
		@refreshWidgetsSize()

	addStepView: (id, options) =>
		if !@stepViews? then @stepViews = {}
		if @stepViews[id]? then return @stepViews[id]
		@stepViews[id] = JSON.stringify options
		@stepViews[id]

	serialize: () =>
		steps = []
		for name, step of @steps
			if @stepViews[step.id]?
				steps.push JSON.parse(@stepViews[step.id])
			else
				steps.push
					title: step.title
					icon: step.icon
					html: step.html
					order: step.order
					titlehtml: step.titlehtml
		return
			title: @title
			subtitle: @subTitle
			submitfunction: @onSubmitCallback
			enableskip: @isEnableSkip
			enableprevious: @isEnablePrevious
			textprevious: @txtPrevBtn
			textnext: @txtNextBtn
			textsubmit: @txtSubmit
			custombutton: @buttonclasses
			stepheight: @stepHeight
			footerheight: @footerH
			buttonheight: @buttonH
			buttonwidth: @buttonW
			titleheight: @titleH
			subtitleheight: @subTitleH
			innerpadding: @innerPadding
			outerpadding: @outerPadding
			steps: steps

	##|
	##|  Autolayout helper class -- called after the view is created, before returning to the main caller.
	##|  See ViewTestLayout7
	##|
	deserialize: (options)=>

		newPromise ()=>
			if options.title?
				@setTitle options.title 

			if options.subtitle? 
				@setSubTitle options.subtitle 

			if options.submitfunction? and typeof options.submitfunction is "function"
				@setSubmitFunction options.submitfunction

			if options.enableskip?
				@setEnableSkip options.enableskip

			if options.enableprevious?
				@setEnablePrevious options.enableprevious

			if options.textprevious?
				@setTextPrevious options.textprevious

			if options.textnext?
				@setTextNext options.textnext

			if options.textsubmit?
				@setTextSubmit options.textsubmit

			if options.custombutton?
				@addCustomButtonClasses options.custombutton

			if options.stepheight?
				@setStepHeight(options.stepheight)

			if options.footerheight?
				@setFooterHeight options.footerheight

			if options.buttonheight?
				@setButtonHeight(options.buttonheight)

			if options.buttonwidth?
				@setButtonWidth(options.buttonwidth)

			if options.titleheight?
				@setTitleHeight(options.titleheight)

			if options.subtitleheight?
				@setSubTitleHeight(options.subtitleheight)

			# if options.padding?
			# 	@setPadding options.padding
			if options.innerpadding?
				@setInnerPadding options.innerpadding

			if options.outerpadding?
				@setOuterPadding options.outerpadding

			if options.steps?

				for item in options.steps

					title = "Unknown"
					icon  = null 
					order = null
					html  = null
					if item.title? then title = item.title
					if item.icon? then icon = item.icon
					if item.html? then html = item.html
					if item.order? then order = item.order
					if item.titlehtml? then titlehtml = item.titlehtml else titlehtml = null
					step = @addStep title, icon, html, order, titlehtml 
					if item.view?
						@addStepView step.id, item
						yield step.body.internalProcessLayout(item)
						if item.wizardAllowContinue? and typeof item.wizardAllowContinue is "function"
							step.body.childView.wizardAllowContinue = item.wizardAllowContinue

					@internalCopyVariables(step.body)

			return true
