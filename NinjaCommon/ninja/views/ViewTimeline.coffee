##
## class to create groups of items in ViewTimeline
class TimelineGroup

    constructor: (@widgetHolder, optionData = {}) ->
        @wgtGroupDate = @widgetHolder.add "div", "ninja-timeline-group-date"
        @wgtCollapsed = @widgetHolder.add "div", "ninja-timeline-group-collapsed"
        @wgtItemList  = @widgetHolder.add "ul", "ninja-timeline-group-itemlist"
        
        @isCollapsed = if optionData.isCollapsed? then optionData.isCollapsed else true
        @format      = if optionData.format? then optionData.format else "MMM YYYY"
        @datetime    = optionData.datetime
        @items       = []
        @options     = {}

        @setDatetime @datetime
        @setCollapsed @isCollapsed

        @wgtCollapsed.on "click", (e) =>
            @setCollapsed false
            true

        @wgtGroupDate.on "click", (e) =>
            @setCollapsed !@isCollapsed
            true

        ## render widgettags
        #@widgetHolder.move 0, @widgtparent.height() + 10, @widgetHolder.width(), @widgetHolder.height()
        # @wgtGroupDate.width 60
        # @wgtGroupDate.setAbsolute()
        # @wgtGroupDate.move -60

        ##
        ##  Allow bypassing or changing any item functions or options
        for varName, value of optionData
            this.options[varName] = value		
        true		


    createItem: (datetime, text, sender, options = {}) =>
        if typeof datetime is "string" then datetime = new Date(datetime)
        if isNaN(datetime)
            console.log "TimelineGroup.createItem: invalid date time", datetime
            return false

        itemId     = "timeline-item-#{@options.id}-#{@items.length}"
        ## find where to insert this group by its year/month
        position = 0
        for item, index in @items
            comp = ViewTimeline.compareDates item.getDatetime(), datetime
            if comp is 1
                position++
                continue

            if comp is 0
                break

            if comp is -1
                position = index
                break

        if position < 0 then position = 0

        itemWidget    = @wgtItemList.addAtPosition "li", "ninja-timelineitem", position
        itemWidget.id = itemId

        options.parentGroup = this
        options.viewid      = @options.viewid
        options.id          = itemId
        options.datetime    = datetime
        options.text        = text
        options.sender      = sender

        timeLineItem = new TimelineItem itemWidget, options
        #@items.push item
        @items.splice position, 0, timeLineItem

        @setCollapsed @isCollapsed
        timeLineItem

    removeItem: (id) =>
        for item, index in @items
            if item.options.id is id
                @items.splice index, 1
                break
        if @items.length is 0
            @widgetHolder.hide()

        @setCollapsed @isCollapsed
        false

    setCollapsed: (bool) =>
        if !bool? then bool = true
        @isCollapsed = bool
        if @isCollapsed
            if @items.length is 1
                @wgtCollapsed.html "#{@items.length} message available, tap to view"
            else if @items.length > 1
                @wgtCollapsed.html "#{@items.length} messages available, tap to view"
            else
                @wgtCollapsed.html "No messages available."
            @wgtCollapsed.show()
            @wgtItemList.hide()
        else
            @wgtCollapsed.hide()
            @wgtItemList.show()
        bool

    setYearMonth: (year, month) =>
        if !year? or !month? then return false
        @year  = year
        @month = month
        true

    setDatetime: (datetime) =>
        if typeof datetime is "string" then datetime = new Date(datetime)
        if isNaN(datetime)
            console.log "TimelineGroup.setDatetime: invalid date time", datetime
            return false
        @datetime = datetime
        @wgtGroupDate.html "<span>#{@getFormattedDate()}</span>"
        datetime

    getDatetime: () =>
        @datetime


    ##
    ## set moment.js format of group's date
    setFormat: (@format) =>
        @formattedDate = moment(@datetime).format @format
        @setDatetime @formattedDate
        true

    ##
    ## returns formatted date string of the group
    getFormattedDate: (format) =>
        if format? then @setFormat format
        if @formattedDate? then return @formattedDate
        @formattedDate = moment(@datetime).format @format
        @formattedDate

##
## class to express items in ViewTimeline
class TimelineItem 
    
    @DEFAULT_SENDER: "System"

    constructor: (@widgetHolder, options) ->
        
        GlobalClassTools.addEventManager(this)
        @wgtIcon   = @widgetHolder.add "div", "ninja-timelineitem-icon"
        @wgtRightWrapper = @widgetHolder.add "div", "ninja-timeline-right"

        @wgtText   = @wgtRightWrapper.add "div", "ninja-timelineitem-text"
        @wgtFooter = @wgtRightWrapper.add "div", "ninja-timelineitem-footer"
        @wgtDate   = @wgtFooter.add "span", "ninja-timelineitem-date"
        @wgtSender = @wgtFooter.add "span", "ninja-timelineitem-sender"
        
        @options   = {}
        @setIcon options.icon
        @setText options.text
        @setDatetime options.datetime
        @setSender options.sender

        ##
        ##  Allow bypassing or changing any item functions or options
        for varName, value of options
            if varName in [ "icon", "text", "sender", "datetime" ] then continue
            this.options[varName] = value		
        true

    setIcon: (icon) =>
        if !icon then icon = "fa-circle"
        @options.icon  = icon
        @wgtIcon.html "<i class='#{@icon}'></i>"
        icon

    setText: (text) =>
        if !text? then return false
        if @options.text is text then return false
        @options.text = text
        @wgtText.html text
        true

    setDatetime: (dateObj) =>
        if !dateObj? or !dateObj.getTime? then return false
        if dateObj is @options.datetime then return false

        @options.datetime = dateObj
        year    = dateObj.getFullYear()
        month   = dateObj.getMonth()
        date    = dateObj.getDate()
        hours   = dateObj.getHours()
        minutes = dateObj.getMinutes()
        seconds = dateObj.getSeconds()

        ## render date time value
        str = ""
        now = new Date()
        if year == now.getFullYear()
            str = moment(dateObj).format('MMM Do, h:mm a')
        else
            str = moment(dateObj).format('MMM Do YYYY, h:mm a')

        timeAgo = @getTimeAgo dateObj
        if timeAgo
            str += " (#{@getTimeAgo(dateObj)})"

        str += " - "

        @wgtDate.html str
        true

    getDatetime: () =>
        @options.datetime

    getTimeAgo: (data) =>

        if !data? then return ""

        if typeof data == "string"
            stamp = new Date(data)
        else if typeof data == "number"
            stamp = new Date(data)
        else if typeof data == "object" and data.getTime?
            stamp = data
        else
            return ""

        age = new Date().getTime() - stamp.getTime()
        age /= 1000

        if age > 86400
            days = Math.floor(age / 86400)
            if days != 1 then daysTxt = "days" else daysTxt = "day"
            txt = "#{days} #{daysTxt}"
        else
            return null
        return "-" + txt

    setSender: (name) =>

        strBefore = ""
        strAfter  = ""
        if !name? or name is "" 
            @options.sender = ""
            name            = TimelineItem.DEFAULT_SENDER
            @wgtSender.html "#{TimelineItem.DEFAULT_SENDER} note"
        else
            if @options.sender is name then return false		
            @options.sender = name
            @wgtSender.html " <span class='timeline-sourceuser'>@#{name}</span>"
            $(".timeline-sourceuser").on "click", (e) =>
                #console.log "Sourceuser is clicked", e
                globalKeyboardEvents.emitEvent "click_user", [e, $(e.target).text()]
                true
        @options.sender

    updateContent: (data) =>
        if !data? then return false
        datetime = data.datetime
        if datetime? and typeof datetime is "string"
            datetime = new Date(datetime)
        isNewDate = @setDatetime datetime
        @setText data.text
        @setSender data.sender

        ##
        ## if datetime is changed, this item's order in the view/group 
        ## must be modified
        if isNewDate
            #console.log "TimeLineItem's datetime is changed", data
            viewid = @options.viewid
            @destroy()

            @emitEvent "timelineitem_changed", [ viewid, datetime, data.text, data.sender, data ]
            ev = new CustomEvent "timelineitem_changed", 
                    detail:
                        datetime: datetime
                        text:     data.text
                        sender:   data.sender
                        data:     data
            window.dispatchEvent ev
        true

    destroy: () =>
        @widgetHolder.destroy()
        @options.parentGroup?.removeItem @options.id
        true

## 
## class to present Timeline that can include TimelineGroups
class ViewTimeline extends View

    onShowScreen: () =>

        ##
        ## default value of left space in this view
        ## this value should be changed when the view gets resized
        @gid = "timeline_#{GlobalValueManager.NextGlobalID()}"
        @spaceLeft  = 100
        @wgtContent = @add "div", "view-timeline-content"
        @wgtGroups  = @wgtContent.add "ul", "view-timeline-groups"

        @groups             = []
        @groupCollapseOlder = true

        window.addEventListener "timelineitem_changed", @onItemChanged

    ##
    ## do not call this function directly outside of this view
    ## addItem should be called to add items/groups
    addGroup: (datetime, options = {}) =>
        if !datetime? or !datetime.getTime? then return false

        options.parentView = this
        options.viewid     = @gid
        options.id         = "timeline-group-#{@groups.length}"

        ## find where to insert this group by its year/month
        position = 0
        for group, index in @groups
            comp = ViewTimeline.compareDates group.getDatetime(), datetime
            if comp >= 2
                position++
                continue

            if comp is 0 or comp is 1 or comp is -1
                return group

            if comp <= -2
                position = index
                break

        if position < 0 then position = 0

        groupWidget        = @wgtGroups.addAtPosition "li", "ninja-timeline-group", position
        groupWidget.id     = options.id
        options.datetime   = datetime
        group              = new TimelineGroup groupWidget, options
        @groups.splice position, 0, group
        #@groups.push group
        group

    addItem: (datetime, text, sender, options = {}) =>
        if !datetime? then return false
        if typeof datetime is "string" 
            datetime = new Date(datetime)

        ## if datetime is invalid date/time format, just do nothing
        if isNaN(datetime) then return false

        ## try to create a new group based on this date info
        group            = @addGroup datetime
        item             = group.createItem datetime, text, sender, options
        if @groupCollapseOlder?
            @setCollapseOlder @groupCollapseOlder
        item

    onItemChanged: (e) =>
        # console.log "on timelineview item changed: ", e.detail
        if !e.detail? or typeof e.detail isnt "object" then return false
        if @gid isnt e.detail.viewid then return false
        @addItem e.detail.datetime, e.detail.text, e.detail.sender, e.detail.data
        @setCollapseOlder @groupCollapseOlder
        true

    setGroupBy: (format) =>
        if !format then return false
        @groupDateFormat = format
        for group in @groups
            group.setFormat format
        true

    setCollapseOlder: (bool) =>
        if !bool? then return false
        @groupCollapseOlder = bool
        setFirst            = false
        for group, index in @groups
            if !setFirst
                if group.items.length > 0
                    group.setCollapsed false
                    setFirst = true
            else 
                group.setCollapsed bool
        true

    ##
    ## compare 2 datetimes
    @compareDates: (date1, date2) =>
        if typeof date1 is "string" then date1 = new Date(date1)
        if typeof date2 is "string" then date2 = new Date(date2)

        if date1.getYear() > date2.getYear()
            return 3
        else if date1.getYear() < date2.getYear()
            return -3

        if date1.getMonth() > date2.getMonth()
            return 2
        else if date1.getMonth() < date2.getMonth() 
            return -2
        
        if date1.getTime() > date2.getTime()
            return 1
        else if date1.getTime() < date2.getTime()
            return -1
        return 0

    onResize: (w, h) =>
        super(w, h)
        # console.log "TimelineView is resized, ", w, h
        # @wgtContent.move @spaceLeft, 0, w - @spaceLeft - 1, h
        true

    getItems: (filter) =>
        if !filter? or typeof filter isnt "object"
            filter = {}

        items = []
        for group in @groups
            for item in group.items
                options = item.options
                if !options? or typeof options isnt "object" then continue
                matching = true
                for key, value of filter
                    if options[key] isnt value then matching = false
                if matching is true then items.push item
        items

    serialize: () =>
        items = []
        for group in @groups
            for item in group.items
                options = item.options
                if !options? or typeof options isnt "object" then continue
                items.push Object.assign({}, options)

        obj =
            items: items
            collapseolder: @groupCollapseOlde
            groupby: @groupDateFormat

        return obj

    deserialize: (data) =>
        new Promise (resolve, reject) =>
            if !data? or typeof data isnt "object"
                reject "ViewTimeline: invalid data passed"

            if data.items?
                for item in data.items
                    @addItem item.datetime, item.text, item.sender, item

            if data.collapseolder?
                @setCollapseOlder data.collapseolder

            if data.groupby?
                @setGroupBy data.groupby

            resolve true
