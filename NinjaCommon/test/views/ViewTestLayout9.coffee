class ViewTestLayout9 extends View

    getLayout: ()=>
        view: "DockedToolbar"
        main:
            view: "TestShowSize"
            name: "viewTest"

    onShowScreen: ()=>

        console.log "Automatically Created Toolbar:", @viewToolbar
        console.log "Specific test view:", @viewTest

        navButtonNew = new NavButton "Testing Button"
        navButtonNew.onClick = (e)=>
            console.log "TODO: New Job Not Implemented"

        navButtonNew2 = new NavButton "Testing Button"
        navButtonNew2.setIcon "fa fa-save"
        navButtonNew2.onClick = (e)=>
            console.log "TODO: New Job Not Implemented"

        @viewToolbar.addToolbar [ navButtonNew, navButtonNew2 ]
        true