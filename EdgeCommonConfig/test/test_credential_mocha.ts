import { config } from '../src/Config';

const should = require('should');

describe("Credential management", () =>

    it("Should be able to get the default test credentials", function () {

        //
        //  Get the credentials for a test host
        const mqHost = config.getCredentials("mqHost");
        mqHost.should.equal('amqp://edge:Edge000199@dev1.protovate.com:5672');
    })
);