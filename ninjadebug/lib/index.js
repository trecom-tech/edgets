"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = require("chalk");
const moment = require("moment");
const numeral = require("numeral");
/*
* Ninja main class which is exported as a general purpose set of debugging tools
* */
let globalNinjaInstance = null;
let slice = [].slice;
class Ninja {
    static dumpBoolean(value) {
        return chalk_1.default.green(value);
    }
    static dumpNull() {
        return chalk_1.default.gray("<null>");
    }
    static dumpString(value) {
        return "'" + chalk_1.default.magenta(value) + "'";
    }
    ;
    /*
    * Dump attempts to display a variable as if it were an assignment that you
    * can just copy/paste back into your code if needed.
    * */
    static dump(title, items) {
        if (!items) {
            items = title;
            title = "";
        }
        if (!title) {
            title = "";
        }
        let str = chalk_1.default.white(title) + " = ";
        for (const item in items) {
            str += Ninja.dumpVar(item, "");
        }
        console.log(str);
    }
    ;
    static dumpDate(value) {
        if (Ninja.debugDateFormat) {
            return chalk_1.default.yellow(moment(value).format(Ninja.debugDateFormat));
        }
        return chalk_1.default.yellow(value);
    }
    ;
    static dumpNumber(value) {
        if (isNaN(value)) {
            return chalk_1.default.gray("<isNaN>");
        }
        if (Ninja.debugNumberFormat != null) {
            if (!value) {
                return Ninja.dumpNull();
            }
            return chalk_1.default.cyan(numeral(value).format(Ninja.debugNumberFormat));
        }
        return chalk_1.default.cyan(value);
    }
    static dumpObject(items, indent) {
        let maxLen = 0;
        let str = "";
        for (const varName in items) {
            if (varName.length > maxLen) {
                maxLen = varName.length;
            }
        }
        for (const varName in items) {
            const value = items[varName];
            let name = varName;
            while (name.length < maxLen) {
                name = name + " ";
            }
            str += indent + name + " : ";
            if (varName === "_id") {
                str += value.toString();
            }
            else {
                str += Ninja.dumpVar(value, indent + "    ");
            }
            str += "\n";
        }
        return str.replace(/\n$/, "");
    }
    ;
    /*
    * Returns a text formatted value for a decimal that is a percent
    * */
    static dumpPercent(value) {
        if (!value) {
            value = 0;
        }
        return numeral(value).format("#,### %");
    }
    static dumpVar(value, indent) {
        let all;
        let prototype;
        let str = "";
        let type;
        type = typeof value;
        if (value == null) {
            return Ninja.dumpNull();
        }
        if (type === "string") {
            return Ninja.dumpString(value);
        }
        if (type === "number") {
            return Ninja.dumpNumber(value);
        }
        if (type === "boolean") {
            return Ninja.dumpBoolean(value);
        }
        if (value.getTime != null) {
            return Ninja.dumpDate(value);
        }
        prototype = value.constructor.toString();
        if (/function Object/.test(prototype)) {
            str = "\n";
            str += Ninja.dumpObject(value, indent + "    ");
            return str;
        }
        if (/function Array/.test(prototype)) {
            all = [];
            for (let subItem in value) {
                all.push(Ninja.dumpVar(subItem, indent));
            }
            str = chalk_1.default.bold("[") + all.join(", ") + chalk_1.default.bold("]");
            return str;
        }
        if (type === "function") {
            return "Function()";
        }
        str += Ninja.dumpObject(value, indent + "    ");
        return str;
    }
    /*
    *  Log will take any number of items and output them similar to console.log
    *  except that color is added automatically and objects are dummped nicely.
    * */
    static log(items) {
        let level = 0;
        for (const item in items) {
            if (typeof item === "string" && level === 0) {
                level++;
                process.stdout.write(item);
            }
            else {
                let str = Ninja.dumpVar(item, "");
                process.stdout.write(str);
                level--;
            }
        }
        process.stdout.write("\n");
    }
    ;
    /*
    * Output a string from a number padded right to a given string length
    *
    * */
    static pad(value, maxSpace, colorFunction, customFormat) {
        let str;
        if (!value) {
            value = "<null>";
        }
        if (typeof value === "number" && isNaN(value)) {
            value = "<isNaN>";
        }
        if (typeof value === "number") {
            if (customFormat) {
                str = numeral(value).format(customFormat);
            }
            else {
                str = numeral(value).format(Ninja.debugNumberFormat);
            }
            while (str.length < maxSpace) {
                str = " " + str;
            }
            if (colorFunction) {
                return colorFunction(str);
            }
            return chalk_1.default.cyan(str);
        }
        str = value.toString();
        while (str.length < maxSpace) {
            str = str + " ";
        }
        if (colorFunction) {
            return colorFunction(str);
        }
        return str;
    }
}
// Format to use to display numbers, set to null for no output
Ninja.debugNumberFormat = "#,###.[####]";
// Format to use to display dates, set to null for no output
Ninja.debugDateFormat = "dddd, MMMM Do YYYY, h:mm:ss a";
exports.Ninja = Ninja;
exports.default = Ninja;
//# sourceMappingURL=index.js.map