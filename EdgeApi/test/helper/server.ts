import config             from 'edgecommonconfig';
import * as EdgeApiServer from 'edgeapiserver';
import TestApiProvider    from './TestApiProvider';

const os       = require('os');
const fs       = require('fs');
const host     = os.hostname();

config.setCredentials('sendgrid', {
    type: 'sendgrid',
    apikey: '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'
});
config.setCredentials('redisReadHost', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHostWQ', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHost', `${process.env.REDIS_HOST}`);
config.setCredentials('influxdb', {
    type    : 'influxdb',
    host    : process.env.INFLUX_HOST,
    username: '',
    password: '',
});
config.setCredentials('MongoDB', {
    url    : `${process.env.DB_HOST}`,
    options: {
        connectTimeoutMS: 600000,
        poolSize        : 16,
        socketTimeoutMS : 600000,
    },
});
config.setCredentials('mqHost', process.env.MQHOST);

config.ApiServers = [process.env.API_SERVER];


export const server = EdgeApiServer.startServer();
server.webServer.gateway.addProvider(new TestApiProvider(TestApiProvider.prototype));

server.addRoute('/testForm(.*)', function(url: string, fields: any, files: any, matches: any) {
    console.log('TEST CALLBACK, URL=', url);
    console.log('TEST Fields       =', fields);
    console.log('TEST Files        =', files);
    console.log('TEST Matches      =', matches);
    return 'Test Result as a string';
});

server.addRoute('/postForm(.*)', function(url: any, fields: any, files: any, matches: any) {
    console.log('POST CALLBACK, URL=', url);
    console.log('POST Fields       =', fields);
    console.log('POST Files        =', files);
    console.log('POST Matches      =', matches);
    return { x: 1, y: 2, z: 3 };
});

export default server;