"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
//  The worker class is one connection to the message queue
//  it handles importing the message, processing it with the database
//  and then sends process messages back the parent thread
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgecommonclusterworker_1 = require("edgecommonclusterworker");
const edgeapi_1 = require("edgeapi");
const slug = require('slug');
const jsonfile = require('jsonfile');
const { argv } = require('yargs');
//  During testing, set this to false and the invalid message will stop running
//  the worker allowing you to see the error and not burn the queue.  During
//  production, set to true so that a single invalid message doesn't cause the
//  worker to stop responding.
const ACK_INVALID_MESSAGES = true;
const ENABLE_DEBUG_SAVE = false;
class Worker extends edgecommonclusterworker_1.default {
    constructor(...args) {
        super();
    }
    onInit() {
        this.reDoubleSlash = new RegExp("//", "g");
        return this.setup();
    }
    //  Incoming job type: message
    //  Log a message to the screen
    processJobMessage(job) {
        this.reportText(`Received Message: ${job.txt}`);
        return true;
    }
    //  Message queue related API calls
    //  These calls only push information or jobs to the server, there
    //  is no response.   The message queue we are pushing to is for monitoring changes
    doOpenChangeMessageQueue() {
        return __awaiter(this, void 0, void 0, function* () {
            this.changemqConnection = yield edgecommonmessagequeue_1.default.doOpenExchangeForWrite(edgecommonconfig_1.default.mqItemChanges, edgecommonconfig_1.default.mqExchangePathUpdates);
            return this.changemqConnection;
        });
    }
    //  Incoming job type: save
    //  Saves changes to an item's json data
    processJobInsertData(job) {
        const dataSet = slug(job.ds);
        const { path } = job;
        const { agent } = job;
        const newData = job.data;
        console.log("insert job");
        process.exit(0);
        if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
            console.log("Invalid JobSaveData", job);
            return false;
        }
        if (!this.dataStore[dataSet]) {
            this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
        }
        return this.dataStore[dataSet].doInsert(path, newData, agent);
    }
    //
    //  Delete a path
    //
    processJobDelete(job) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataSet = slug(job.ds);
            const { path } = job;
            if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
                console.log("Invalid JobDelete", job);
                return false;
            }
            if (!this.dataStore[dataSet]) {
                this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
            }
            yield this.dataStore[dataSet].doDeletePath(path);
            return true;
        });
    }
    //  Append a record to an array or sub path
    processJobAppendData(job) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataSet = slug(job.ds);
            const { path } = job;
            const { agent } = job;
            const { mode } = job;
            const newData = job.data;
            edgecommonconfig_1.default.status(`processJobAppendData path=${path}`);
            if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
                this.invalidJobLog.error("Invalid Append, missing dataset", { job });
                edgecommonconfig_1.default.status("processJobAppendData Invalid Job");
                return false;
            }
            if (!this.dataStore[dataSet]) {
                edgecommonconfig_1.default.status("processJobAppendData Connect to DataSetManager");
                this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
            }
            edgecommonconfig_1.default.status("processJobAppendData Starting doUpdate");
            yield this.dataStore[dataSet].doAppend(path, newData, agent, mode);
            return true;
        });
    }
    //  Incoming job type: save
    //  Saves changes to an item's json data
    processJobSaveData(job) {
        return __awaiter(this, void 0, void 0, function* () {
            const dataSet = slug(job.ds);
            const { path } = job;
            const { agent } = job;
            const { mode } = job;
            const newData = job.data;
            if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
                edgecommonconfig_1.default.status("ProcessJobSaveData Invalid Job");
                console.log("Invalid JobSaveData", job);
                return false;
            }
            // config.status "ProcessJobSaveData path=#{path}", newData
            // console.log "ProcessJobSaveData path=#{path}", newData
            if (!this.dataStore[dataSet]) {
                edgecommonconfig_1.default.status("ProcessJobSaveData Connect to DataSetManager");
                this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
            }
            if (ENABLE_DEBUG_SAVE) {
                this.sampleData.push(job);
            }
            edgecommonconfig_1.default.status("ProcessJobSaveData Starting doUpdate");
            yield this.dataStore[dataSet].doUpdate(path, newData, agent, mode);
            return true;
        });
    }
    //  General job router
    //  Job is an incoming object from the message queue
    //  it should have "job.type" which determines the
    //  rest of the contents and what should be done with it.
    processJob(job) {
        if (ENABLE_DEBUG_SAVE && (this.sampleData.length > 9)) {
            if (!this.nCounter) {
                this.nCounter = 0;
            }
            jsonfile.writeFileSync(`SampleData${this.nCounter}.json`, this.sampleData, { spaces: 4 });
            this.nCounter++;
            this.sampleData = [];
        }
        if (job.type === "savedata") {
            return this.processJobSaveData(job);
        }
        else if (job.type === "append") {
            return this.processJobAppendData(job);
        }
        else if (job.type === "insertdata") {
            return this.processJobInsertData(job);
        }
        else if (job.type === "deletepath") {
            return this.processJobDelete(job);
        }
        else if (job.type === "message") {
            return this.processJobMessage(job);
        }
        else {
            this.reportText(`Invalid job :${JSON.stringify(job)}`);
            return false;
        }
    }
    //  Initialize the worker
    setup() {
        return __awaiter(this, void 0, void 0, function* () {
            this.invalidJobLog = edgecommonconfig_1.default.getLogger("InvalidJobs");
            //  An array of ItemStorage containers for each store number
            //  that we get updates for
            this.dataStore = {};
            //
            //  Cache connection
            this.cache = edgeapi_1.default.getRedisConnection();
            edgecommonconfig_1.default.status("Worker setup opening exchange for write changes");
            this.changemqConnection = yield edgecommonmessagequeue_1.default.doOpenExchangeForWrite(edgecommonconfig_1.default.mqItemChanges, edgecommonconfig_1.default.mqExchangePathUpdates);
            //
            //  --test <filename> to process a single job for testing.
            //
            if (argv.test != null) {
                const job = jsonfile.readFileSync(argv.test);
                edgecommonconfig_1.default.status("Worker setup testing job: ", argv.test);
                this.fixupJson(job);
                edgecommonconfig_1.default.status("Worker setup test job data:", job);
                const result = this.processJob(job);
                edgecommonconfig_1.default.status("Worker setup test job result:", result);
                return false;
            }
            edgecommonconfig_1.default.status("Worker setup opening message queue read updates");
            // @mqItemUpdates     = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdates
            // @mqItemUpdatesHigh = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdatesHigh
            // @mqItemUpdatesLow  = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdatesLow
            this.sampleData = [];
            const statusHigh = this.processMessageQueue("UpdatesHigh", edgecommonconfig_1.default.mqItemUpdatesHigh, 6, (job) => {
                this.fixupJson(job);
                return this.processJob(job);
            });
            const statusNormal = this.processMessageQueue("Updates", edgecommonconfig_1.default.mqItemUpdates, 4, (job) => {
                this.fixupJson(job);
                // console.log "JOB:", job
                return this.processJob(job);
            });
            const statusLow = this.processMessageQueue("UpdatesLow", edgecommonconfig_1.default.mqItemUpdatesLow, 1, (job) => {
                this.fixupJson(job);
                return this.processJob(job);
            });
            return true;
        });
    }
}
exports.Worker = Worker;
exports.default = Worker;
//# sourceMappingURL=Worker.js.map