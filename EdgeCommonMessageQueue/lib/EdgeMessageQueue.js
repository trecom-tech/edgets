"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const zlib = require("zlib");
const amqplib = require("amqplib");
//
//  Wrap the actual amqp class
//
class EdgeAQMPMessageQueue {
    constructor(mqChannel, messageQueue, queueName) {
        this.drainWait = [];
        this.defaultPriority = 5;
        this.mqChannel = mqChannel;
        this.messageQueue = messageQueue;
        this.queueName = queueName;
        this.mqChannel.on('drain', () => {
            this.drainRequired = false;
            for (let callback of Array.from(this.drainWait)) {
                // console.log 'Drainwait called', callback
                callback();
            }
            this.drainWait = [];
        });
    }
    consume(prefetchNum, callback) {
        edgecommonconfig_1.config.status(`EdgeMessageQueue consume Starting with ${prefetchNum}`);
        this.mqChannel.prefetch(prefetchNum);
        this.mqChannel.consume(this.messageQueue.queue, (msg) => {
            try {
                let job;
                if (msg.content[0] === 123) {
                    //
                    //  First letter is { so it's text JSON
                    edgecommonconfig_1.config.status('EdgeMessageQueue consume message received in JSON', msg.content.toString());
                    job = JSON.parse(msg.content.toString());
                    callback(msg, job);
                }
                else {
                    //
                    //  Inflate the content
                    //
                    edgecommonconfig_1.config.status('EdgeMessageQueue consume message received in ZLIB');
                    zlib.inflate(msg.content, (err, str) => {
                        if (err != null) {
                            console.log('Error during inflate:', err);
                        }
                        else {
                            job = JSON.parse(str.toString());
                            callback(msg, job);
                        }
                    });
                }
            }
            catch (e) {
                console.log('Exception:', e);
                console.log('Error in onQueueMessage:', msg.content.toString());
                this.mqChannel.ack(msg);
                return false;
            }
        });
    }
    //
    //  Disconnect / disable the message queue
    doClose() {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.config.status('EdgeMessageQueue::doClose Start');
            edgecommonconfig_1.config.status('EdgeMessageQueue::doClose Complete');
            return true;
        });
    }
    //
    //  Internal wait until the write queue is complete if buffer is full
    doWaitForDrain() {
        return new Promise((resolve, reject) => {
            this.drainWait.push(() => {
                resolve(true);
            });
        });
    }
    // publish string to a channel
    doPublish(routingKey, str) {
        const { exchange } = this.messageExchange;
        return new Promise((resolve, reject) => {
            if (typeof str === 'object') {
                str = JSON.stringify(str);
            }
            if (typeof str === 'string') {
                zlib.deflate(str, (err, buffer) => {
                    if (err != null) {
                        edgecommonconfig_1.config.error('zlib.deflate error:', err);
                    }
                    else {
                        edgecommonconfig_1.config.status(`EdgeMessageQueue Publish ${buffer.length} bytes (from ${str.length})`);
                        const result = this.mqChannel.publish(exchange, routingKey, buffer);
                        if (!result) {
                            this.doWaitForDrain()
                                .then(() => {
                                resolve(true);
                            });
                        }
                        else {
                            resolve(true);
                        }
                    }
                });
            }
            else {
                const result = this.mqChannel.publish(exchange, routingKey, str);
                if (!result) {
                    this.doWaitForDrain()
                        .then(() => {
                        resolve(true);
                    });
                }
                else {
                    resolve(true);
                }
            }
        });
    }
    //
    //  Write either JSON text or a binary buffer to the message queue
    //  returns a promise write the write is complete.
    //
    doSubmitBuffer(str, messagePriority) {
        return new Promise((resolve, reject) => {
            if (typeof str === 'object') {
                str = JSON.stringify(str);
            }
            if (typeof str === 'string') {
                //
                // Compress data and submit it
                zlib.deflate(str, (err, buffer) => {
                    if (err != null) {
                        edgecommonconfig_1.config.error('zlib.deflate error:', err);
                    }
                    else {
                        edgecommonconfig_1.config.status(`EdgeMessageQueue [${this.messageQueue.queue}] doSubmitBuffer Sending Compressed ${str.length} => ${buffer.length}`);
                        // console.log 'HERE M=', @messageQueue
                        // result = @messageQueue.sendToQueue @queueName, buffer
                        if ((messagePriority == null)) {
                            messagePriority = this.defaultPriority;
                        }
                        const options = { priority: messagePriority };
                        const result = this.mqChannel.sendToQueue(this.messageQueue.queue, buffer, options);
                        if (!result) {
                            this.doWaitForDrain()
                                .then(() => {
                                resolve(true);
                            });
                        }
                        else {
                            resolve(true);
                        }
                    }
                });
            }
            else {
                const result = this.messageQueue.sendToQueue(this.queueName, str);
                if (!result) {
                    this.doWaitForDrain()
                        .then(() => {
                        resolve(true);
                    });
                }
                else {
                    resolve(true);
                }
            }
        });
    }
}
//
//  Main class that is returned when you require EdgeMessageQueue
//
class EdgeMessageQueueController {
    constructor() {
        this.log = edgecommonconfig_1.config.getLogger('MessageQueue');
    }
    doConnect() {
        if (this.openPromise != null) {
            return this.openPromise;
        }
        return this.openPromise = (() => __awaiter(this, void 0, void 0, function* () {
            const server = edgecommonconfig_1.config.getCredentials('mqHost');
            edgecommonconfig_1.config.status(`EdgeMessageQueue doConnect ${server}`);
            // tslint:disable-next-line
            this.conn = yield amqplib.connect(server);
            edgecommonconfig_1.config.status('EdgeMessageQueue Connected');
            return true;
        }))();
    }
    doOpenSubscription(queueName, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doConnect();
            //
            // RabbitMQ / AQMP 0.9
            edgecommonconfig_1.config.status('Creating subscription chan');
            const ch = yield this.conn.createChannel();
            const mq = yield ch.assertQueue('', { exclusive: true, durable: false });
            edgecommonconfig_1.config.status('Creating bind chan to', queueName);
            ch.bindQueue(mq.queue, queueName);
            ch.consume(mq.queue, (msg) => {
                try {
                    let json;
                    ch.ack(msg);
                    if (msg.content[0] === 123) {
                        //
                        //  First letter is { so it's text JSON
                        edgecommonconfig_1.config.status('doOpenSubscription consume message received in JSON Subscription:', msg.content.toString());
                        json = JSON.parse(msg.content.toString());
                        if ((json.stamp == null)) {
                            json.stamp = new Date();
                        }
                        callback(json);
                    }
                    else {
                        //
                        //  Inflate the content
                        //
                        // config.status 'doOpenSubscription consume message received in ZLIB'
                        zlib.inflate(msg.content, (err, str) => {
                            if (err != null) {
                                console.log('Error during inflate:', err);
                            }
                            else {
                                json = JSON.parse(str.toString());
                                if ((json.stamp == null)) {
                                    json.stamp = new Date();
                                }
                                callback(json);
                            }
                        });
                    }
                }
                catch (e) {
                    console.log('JSON Exception:', e);
                    if ((msg != null) && (msg.content != null)) {
                        console.log('JSON Content:', msg.content.length, ' = ', msg.content.toString());
                    }
                    const log = edgecommonconfig_1.config.getLogger(`EdgeMessageQueue-${queueName}`);
                    log.error('Exception during processing', { msg, e });
                }
            });
        });
    }
    doOpenMessageQueue(queueName, options = null) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doConnect();
            edgecommonconfig_1.config.status(`EdgeMessageQueue connecting to message queue ${queueName}`);
            if ((options == null)) {
                options = {};
            }
            options['x-max-priority'] = 10;
            options['durable'] = false;
            const mqChannel = yield this.conn.createChannel();
            const messageQueue = yield mqChannel.assertQueue(queueName, options);
            const wrappedQueue = new EdgeAQMPMessageQueue(mqChannel, messageQueue, queueName);
            edgecommonconfig_1.config.status(`EdgeMessageQueue connected to message queue ${queueName}`);
            return wrappedQueue;
        });
    }
    doOpenExchangeForWrite(queueName, exchangeName) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.config.status(`doOpenExchangeForWrite queueName=${queueName}, exchangeName=${exchangeName}`);
            yield this.doConnect();
            const mqChannel = yield this.conn.createChannel();
            const messageExchange = yield mqChannel.assertExchange(exchangeName, 'fanout', { durable: false });
            const messageQueue = yield mqChannel.assertQueue(queueName, { durable: false });
            mqChannel.bindQueue(messageQueue.queue, exchangeName, '');
            const wrappedQueue = new EdgeAQMPMessageQueue(mqChannel, messageQueue, queueName);
            wrappedQueue.messageExchange = messageExchange;
            return wrappedQueue;
        });
    }
}
exports.EdgeAMQPController = new EdgeMessageQueueController();
exports.default = exports.EdgeAMQPController;
//# sourceMappingURL=EdgeMessageQueue.js.map