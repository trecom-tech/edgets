import { config } from 'edgecommonconfig';

const should = require('should');
import EdgeApi from '../src/EdgeApi';

config.setCredentials("redisHost", process.env.REDIS_HOST);
config.setCredentials("redisHostWQ", process.env.REDIS_HOST);
config.setCredentials("redisReadHost", process.env.REDIS_HOST);
config.setCredentials("ApiServers", [process.env.API_SERVER]);

const wait = (timeout: number) => new Promise(resolve => setTimeout(() => resolve(), timeout));

describe("Cache", () => {
    it("Should work cacheSetAdd cacheGetSetLength", async () => {
        let result = EdgeApi.cacheSetAdd("wq_test", 123);
        result = EdgeApi.cacheSetAdd("wq_test", 1234);

        await EdgeApi.cacheGetSetLength("wq_test").then(async (result) => {
            result.should.equal(2);
        
            await EdgeApi.cacheSetPop("wq_test").then(val => (console.log(val)));
        
            await EdgeApi.cacheSetPop("wq_test").then(val => (console.log(val)));
        
            await EdgeApi.cacheSetPop("wq_test").then(val => (console.log(val), should.not.exist(val)));
        });
    });

    it('cacheSet cacheGet', async () => {
        let result = await EdgeApi.cacheSet('test', 'some_string');
        result.should.be.eql('OK');
        result = await EdgeApi.cacheGet('test');
        result.should.be.eql('some_string');
    });

    it('cacheSetExpireSeconds', async () => {
        let result = EdgeApi.cacheSetExpireSeconds('test_expire', 'some_string', 5);
        result.should.be.eql(true);
        let newResult = await EdgeApi.cacheGet('test_expire');
        newResult.should.be.eql('some_string');

        await wait(5000);
        newResult = await EdgeApi.cacheGet('test_expire');
        should.not.exist(newResult);
    });

    it('cacheHMSet', async () => {
        let result = EdgeApi.cacheHMSet('test_hm_set', { test: '1' });
        result.should.be.eql(true);

        let newResult = await EdgeApi.cacheHGetAll('test_hm_set');
        newResult.should.have.property('test', '1');
    });

    it('Should work cacheSetAdd cacheGetSetLength', async () => {

        let result = EdgeApi.cacheSetAdd('wq_test', 123);
        result     = EdgeApi.cacheSetAdd('wq_test', 1234);

        await EdgeApi.cacheGetSetLength('wq_test').then(async (result) => {
            result.should.equal(2);
            let newResult: any = await EdgeApi.cacheSetMembers('wq_test');
            newResult.length.should.be.eql(2);

            await EdgeApi.cacheSetPop('wq_test').then(val => (console.log(val)));

            await EdgeApi.cacheSetPop('wq_test').then(val => (console.log(val)));

            await EdgeApi.cacheSetPop('wq_test').then(val => (console.log(val), should.not.exist(val)));
        });

    });

    describe("Cloning an object", () => {

        describe("Cloning a scalar value", () => {
            it("Should return the same value as cloned", () => {
                let value = EdgeApi.cloneObject(999);
                return value.should.equal(999);
            });

            it("Should return the same string", () => {
                let value = EdgeApi.cloneObject('Testing');
                return value.should.equal("Testing");
            });
        });

        describe("Cloning a date", () => {

            let today = new Date();
            let dupe = EdgeApi.cloneObject(today);
            let time = dupe.getTime();
            // time.should.equal(today.getTime());

            today.setSeconds(1);
            // time.should.not.equal(today.getTime());
        });

        describe("Cloning an object", () => {

            let obj1 = { a: 10, b: 20, c: { d: 30 } };
            let obj2 = EdgeApi.cloneObject(obj1);

            it("Should return a deep clone from an object", () => {
                obj2.should.have.property('a', obj1.a);
                obj2.should.have.property('b', obj1.b);
                obj2.c.should.have.property('d', obj1.c.d);
            });

            it("Should not have the same value after edit", () => {
                obj2.b = 30;
                obj2.should.have.property('b', 30);
                obj1.should.have.property('b', 20);
            });
        });
        
    });

    describe("REDIS Cache", function() {
        this.timeout(10000);
        it('Should return a null value for a new key', async () => {
            let newKey = `TEST${ new Date().getTime() }`;
            const data = await EdgeApi.cacheGet(newKey);
            should.not.exist(data);
        });

        it('Should return a matching value after being set', async () => {
            let newKey = 'TESTNEW';
            let today = new Date().getTime();
            await EdgeApi.cacheSet(newKey, today as any);
            let data = await EdgeApi.cacheGet(newKey);
            data.should.equal(today + '');
        });
    });
});