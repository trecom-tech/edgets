#!/bin/sh

coffee -s <<"EOF"
config = require '../src/Config'
testValue = config.getCredentials "TestCreds"
if testValue == "123456"
	console.log "GETTING CREDENTIAL is successful (bad result)"
else
	console.log "GETTING CREDENTIAL is failed (which is correct!)"
EOF

##|
##|  Now export the value and run the code again
export TestCreds='123456'

coffee -s <<"EOF"
config = require '../src/Config'
testValue = config.getCredentials "TestCreds"
if testValue == "123456"
	console.log "GETTING CREDENTIAL is successful"
else
	console.log "GETTING CREDENTIAL is failed"
EOF