import { EdgeMQTTController } from "../src/EdgePubSub";
import config from 'edgecommonconfig';

config.setCredentials('mqttHost', process.env.MQTT_HOST || 'mqtt://localhost:1883');

describe("MQTT Connecting and sending a message", () => {

    describe("Submit messages to the message queue", () => {

        it("Should handle a string message", async () => {
            const result = await EdgeMQTTController.doPublish('topic', 'message');
            result.should.equal(true);
        });

        it("Should handle Buffer data", async () => {
            const result = await EdgeMQTTController.doSubmitBuffer('topic', Buffer.from('test'));
            result.should.equal(true);
        });
    });

    describe("subscribe to a topic", () => {
        it("Should receive a message", async () => {
            return new Promise(async (resolve, reject) => {
                await EdgeMQTTController.consume('topic', (data: any) => {
                    // This callback function is triggered when new message is received
                    console.log(data);
                });
                const result = await EdgeMQTTController.doPublish('topic', 'message');
                resolve();
            })

        });
    })
});


