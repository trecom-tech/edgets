import { ColumnBase } from './column_base';
export declare class Column extends ColumnBase {
    static defaultColumn: any;
    tableName: string;
    formater: any;
    parser: any;
    scope: any[];
    defaultColumn: any;
    data: any;
    clickable: boolean;
    isGrouped: boolean;
    visible: boolean;
    renderFunctionCache: any;
    colorFunctionCache: any;
    editFunctionCache: any;
    actualWidth: number;
    sort: number;
    tagSort: any;
    constructor(tableName?: string);
    init(): void;
    setScope(scope: any): void;
    /**
     * function to run excel statement,
     * run excel statement from parsing string if given expression is started with "="
     * @param expression [String] expression to parse into excel statement
     * @param scope [Object]
     */
    excelStatementRender(expression: string, scope: any[]): any;
    setValues(scope: any[], prefix?: string): boolean;
    internalMathRender(a: any, b: any, c: any): string;
    getName(): any;
    getSource(): any;
    getEditable(): any;
    getType(): any;
    getDefault(): any;
    getAlign(): any;
    getOrder(): any;
    getIsCalculation(): boolean;
    getRequired(): boolean;
    getVisible(): boolean;
    getAlwaysHidden(): boolean;
    getDeleted(): boolean;
    getClickable(): boolean;
    getOptions(): any;
    changeColumn(varName: string, value: any): boolean;
    renderTooltip(row: any, value: any, tooltipWindow: any): any;
    renderValue(value: any, keyValue: any, row: any): any;
    onFocus(e: any, col: any, data: any): boolean;
    getCellColor(value: any, keyValue: any, row: any): any;
    RenderHeader(parent: any, location: any): any;
    RenderHeaderHorizontal(parent: any, location: any): any;
    UpdateSortIcon(newSort: any): boolean;
    getRenderFunction(): any;
    getColorFunction(): any;
    getHasColorFunction(): boolean;
    getFormatter(): any;
    deduceInitialColumnType(): boolean;
    deduceColumnType(newData: any): boolean;
    getAutoSize(): boolean;
    getWidth(): any;
    getLinked(): any;
    getPii(): any;
    getBlind(): any;
    getLocked(): any;
    getEditPermissions(): any[];
    getViewPermissions(): any[];
    getFlagsRequiredView(): any;
    getFlagsRequriedEdit(): any;
    getVisibleOnNew(): any;
    getVisibleOnEdit(): any;
    getValidateOnCreate(): any;
    getValidateOnEdit(): any;
    getValidateErrorMessage(): any;
    getValidateWarningMessage(): any;
    getValidateOptions(): any;
    getValidationType(): any;
    getValidationDetails(): any;
    /**
     * function to edit the column
     */
    getEditFunction(): any;
    /**
     * function to get this column
     */
    serialize(): any;
    /**
     * function to set column value from configuration
     * @param obj - obj to set column's value
     */
    deserialize(obj: any): boolean;
}
export default Column;
