import { config }       from 'edgecommonconfig';
import BulkTableStorage from '../src/BulkTableStorage';

const should        = require('should');
const testRecord1   = {
    name    : 'Brian Pollack',
    age     : 42,
    birthday: new Date('1975-10-02 10: 04: 05'),
    income  : 17.64
};

config.setCredentials('azureTest', {connection: 'DefaultEndpointsProtocol=https;AccountName=obviotest;AccountKey=IRfy3COidoce4u7W3L6dQ1bI+XrSRVPjsGsIa46Fs+JxM8nOTuRt8escpcFIDWGnOCDrqV56FPXPGmA7qyCrWA==;EndpointSuffix=core.windows.net'});

describe('BulkTableStorage', () => {

    it('Should insert a simple record into the test table', async () => {
        const ts = new BulkTableStorage('azureTest', 'table1');
        let result = await ts.doInsertLogRecord('log1', testRecord1);
        result.should.equal(true);

        result = await ts.doInsertLogRecord('log1', null);
        result.should.equal(false);

        result = await ts.doInsertLogRecord('log1', 'test');
        result.should.equal(false);

        ts.enabled = false;
        result = await ts.doInsertLogRecord('log1', testRecord1);
        result.should.equal(false);

        ts.creds = null;
        result = await ts.doInsertLogRecord('log1', testRecord1);
        result.should.equal(false);
    });

    it('Create a query', () => {
        const ts = new BulkTableStorage('azureTest', 'table1');
        let query = ts.createQuery('name', 'Brian Pollack', 1);
        query._top.should.equal(1);
        query._where[0].should.equal('name eq \'Brian Pollack\'');

        query = ts.createQuery('name', 'Brian Pollack');
        should.not.exist(query._top);
        query._where[0].should.equal('name eq \'Brian Pollack\'');

        query = ts.createQuery('name', null);
        should.not.exist(query);

        query = ts.createQuery('', 'Brian Pollack', 1);
        should.not.exist(query);
    });

    it('Should get a record from the test table', async () => {
        let ts = new BulkTableStorage('azureTest', 'table1');

        let query = ts.createQuery('name', 'Brian Pollack', 1);
        let result:any = await ts.doQuery(query);
        result.rows[0].name.should.equal('Brian Pollack');
        result.rows[0].age.should.equal(42);
        result.rows[0].birthday.getTime().should.equal(new Date('1975-10-02 10: 04: 05').getTime());
        result.rows[0].income.should.equal(17.64);

        result = await ts.doQuery(null);
        result.should.equal(false);

        query = ts.createQuery('name', 'test', 1);
        result = await ts.doQuery(query);
        result.rows.length.should.equal(0);

        ts.creds = null;
        query = ts.createQuery('name', 'Brian Pollack', 1);
        result = await ts.doQuery(query);
        result.should.equal(false);
    });

    it('Test with invalid credentials', async () => {
        config.setCredentials('azureTest', {connection: ''});
        let ts = new BulkTableStorage('azureTest', 'table1');
        let query = ts.createQuery('name', 'Brian Pollack', 1);
        query._top.should.equal(1);

        let result = await ts.doInsertLogRecord('log1', testRecord1);
        result.should.equal(false);

        result = await ts.doQuery(query);
        result.should.equal(false);
    });

});