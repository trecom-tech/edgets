"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for distance data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const common = new DataFormatCommon_1.DataFormatCommon();
class DataFormatDistance extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'distance';
        this.width = 100;
        this.input_width = 100;
        this.align = 'left';
    }
    //
    // Takes meters in, returns a formatted string
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if (data === 0) {
            return 0;
        }
        //  DATA is in METERS
        const feet = 3.28084 * data;
        // feet = 5280 * data
        if (feet < 50) {
            return "< 50 ft";
        }
        if (feet < 1000) {
            return Math.ceil(feet) + " ft";
        }
        data = feet / 5280;
        return numeral(data).format('#,###.##') + " mi";
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        // console.log "Unformat distance doesn't work:", data
        const val = common.getNumber(data);
        return val;
    }
}
exports.DataFormatDistance = DataFormatDistance;
exports.default = DataFormatDistance;
//# sourceMappingURL=DataFormatDistance.js.map