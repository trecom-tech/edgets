import EdgeApi                           from 'edgeapi';
import { ui, prompt, Question, Answers } from 'inquirer';

const channelName = 'demoChannel';
const bottomBar = new ui.BottomBar();

async function start() {
    const nameQuestion: Question<Answers> = {
        message: 'Enter your name: ',
        name: 'name',
        type: 'input',
    };
    const { name } = await prompt([nameQuestion]);

    const connection = new EdgeApi();
    await connection.doConnect();

    connection.subscribe(channelName);

    connection.listen(channelName, (data: any) => {
        if (connection.socket.id !== data.id) {
            console.log(`Message from ${data.name}: ${data.message}`);
        }
    });

    for (let i = 0; i < 10; i ++) {
        const messageQuestion: Question<Answers> = {
            message: 'Enter message: ',
            name: 'message',
            type: 'input',
        };

        const { message } = await prompt([messageQuestion]);

        connection.data_doSendChannelMessage(channelName, {
            message,
            name,
            id: connection.socket.id
        });
    }
}

start()
    .then((res) => {
        console.log(res);
    })
    .catch((err) => {
        console.log(err);
    });
