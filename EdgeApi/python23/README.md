## Description

This is python module which is simple client to connect to EdgeApiServer using socketio such like my golang pacakge.

This needs to install socketio-client python package before launching module.

	pip install -U socketIO-client


 1. We should launch EdgeApiServer

* ...
* cd EdgeApiServer/
* coffee test/test.coffee

 2. Navigate to our python23 directoy to check module.

* ...
* cd python23/
* python test.py


## Important Notes

This is client side ony and needs to be launched with EdgeApi Server.

## Using module

If we import this package, we should create the instance of EdgeApiClient Class.

After creating instance, call setServer() with host and port of server to connect the server. And we will use connect() to check and confirm if server is available and try to connect server.

### connect to server.

	c = EdgeApiClient()
	c.setServer("localhost", 8001)
	c.connect()

## Api 

* get_list(callback) : Get the list of available apis from server using callback function.
* make_call(callName, callback) : Call to server by callName (string) and Get the result with callback function.