/// <reference types="node" />
export declare class TimeSeriesDatabase {
    parent: EdgeTimeseriesData;
    databaseName: string;
    pendingPoints: Array<object>;
    pendingTimer: NodeJS.Timeout;
    constructor(parent: EdgeTimeseriesData, databaseName: string);
    /**
     * Send the pending data
     */
    sendTimer(): boolean;
    /**
     * Change to doSaveMeasurement for those that are approved during the testing phase.
     * @param measurement
     * @param tags
     * @param data
     */
    doSaveMeasurement(measurement: string, tags: any, data: any): Promise<boolean>;
}
export default class EdgeTimeseriesData {
    static singleInstance: EdgeTimeseriesData;
    private log;
    private base;
    client: any;
    private dbCache;
    static getInstance(): EdgeTimeseriesData;
    constructor();
    /**
     * Creates database only if it doesn't exist, returns success
     * @param databaseName
     */
    doCreateDatabase(databaseName: string): Promise<any>;
    /**
     * Retrieves all database names
     */
    doGetDatabases(): Promise<any>;
    /**
     * Retrieves all measurements per database name
     * @param databaseName
     * @param measurement
     * @param tags
     * @param data
     */
    doWriteMeasurement(databaseName: string, measurement: string, tags: string, data: any): Promise<boolean>;
    /**
     * Retrieves all measurements per database name
     * @param databaseName
     */
    doGetMeasurements(databaseName: string): Promise<any>;
    /**
     * Execute a query and get result, use fields same as '*' or 'cpu, mem, disk',
     * as usual select operation in database queries
     * @param databaseName
     * @param sql
     */
    doQuery(databaseName: string, sql: string): Promise<any>;
    dropDatabase(databaseName: string): Promise<any>;
}
