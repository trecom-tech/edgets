class ViewMarkDown extends View

	getDependencyList: ()=>
		return [ "/vendor/markdown-it.min.js" ]

	onShowScreen: () =>
		@mdBody = @elHolder.addDiv "markdown-body"
		@mdBody.setAbsolute()
		@mdBody.setScrollable()
		@md = markdownit()
		true

	##
	## convert markdown content to HTML
	setHTML: (@mdContent) =>
		@htmlContent = @md.render @mdContent
		@mdBody.html @htmlContent
		true

	getHTMLContent: () =>
		@htmlContent

	getMDContent: () =>
		@mdContent

	showContent: () =>
		@mdBody.html @htmlContent

	serialize: () =>
		return {}

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? or typeof options isnt "object"
				reject "ViewMarkDown: invalid data passed"

			if options.content?
				@setHTML options.content

			resolve true
