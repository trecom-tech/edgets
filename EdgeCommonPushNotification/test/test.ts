import EdgeCommonNotification from '../src/EdgeCommonNotification';
import {expect}               from 'chai';
import { IContactInfo }       from '../src/IContactInfo';
import { IMessage }           from '../src/IMessage';
import './helper/setup';

const notification = new EdgeCommonNotification();

const message1: IMessage = {
    title: 'Testing Notification',
    alert: `\u2709 You have a new message(${new Date()})`,
    topic: 'com.test.Demo-PushNotification1',
};

const userContactDetails: IContactInfo[] = [
    {
        type: 'apple',
        priority: 0,
        account: {
            deviceToken: "079AF23E1A3620DB57D113ADF91F21DFB0E7D99244AD7BDCACEB4B59B6061151",
        }
    },
    {
        type: 'google',
        priority: 1,
        account: {
            deviceToken: "db57akN8B3A:APA91bHzQb6QbPF7gGbWUtnlj-zpGn5OXXOjW28W052BLAEq_WLlA2otSHOnTYoqq9qHdmaXAiYHiWkgfabscm_ThmDzP5UkRToq5XIHavs4GQn2aGTUANfUPvdpQoKCCtVqEx-DhPmt",
        }
    },
    {
        type: 'whatsapp',
        priority: 2,
        account: {
            phoneNumber: '8550967922181',
        }
    },
    {
        type: 'sms',
        priority: 3,
        account: {
            phoneNumber: '13609407548'
        }
    }
];


describe('Contact Details', () => {

    it('addContactDetails()', () => {
        const result = notification.addContactDetails(userContactDetails);
        expect(result).to.equal(true);
    });

});

describe('Push Notifications', () => {
    it('doSendMessage()', async () => {
        const result = await notification.doSendMessage(message1);
        expect(result).to.equal(true);
    });
});
