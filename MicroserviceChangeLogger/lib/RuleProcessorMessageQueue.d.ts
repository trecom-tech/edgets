import RuleProcessor from './RuleProcessor';
export default class RuleProcessorMessageQueue extends RuleProcessor {
    mq: any;
    onInit(): void;
    onExecute(job: any, count?: number): void;
}
