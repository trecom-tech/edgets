"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug = require('slug');
const jsonfile = require('jsonfile');
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgeapi_1 = require("edgeapi");
const ApiProvider_1 = require("./ApiProvider");
class ProviderData extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    /**
     * Get invoked when provider is added to gateway
     */
    onInit() {
        // Initialize the provider
        this.dataStore = {};
        this.log = edgecommonconfig_1.default.getLogger('ProviderData');
    }
    /**
     * Returns base name. {BaseName}_{MemberName}
     */
    getBaseName() {
        return 'data';
    }
    /**
     * Send a message to a channel
     *
     * @param channelName
     * @param message
     */
    doSendChannelMessage(channelName, message) {
        this.sendToChannel(channelName, { priority: 1 }, message);
    }
    doTestFormAuth(user) {
        if (user.hasRole('serviceUser')) {
            return 'Found';
        }
        return this.failedRoleCheck(user, 'seviceUser', 'doTestFormAuth');
    }
    /**
     * Dump formdat and returns true
     * @param user - [ApiSocketUser] ApiSocketUser instance who is calling the endpoint.
     * @param formData - [any] JSON parameter
     */
    doTestForm(user, formData) {
        console.log('doTestForm user=', user);
        console.log('formData=', formData);
        edgecommonconfig_1.default.dump('formData=', formData);
        return true;
    }
    /**
     * Write obj into logs/filename
     * @param filename - [string] FileName to write obj.
     * @param obj - [any] JSON to write to file.
     */
    doRecordFile(filename, obj) {
        console.log('Writing object to file: ', filename);
        jsonfile.writeFile(`./logs/${filename}`, obj, { spaces: 4 });
        return true;
    }
    /**
     * Returns cached DataSetManager instance
     * @param datasetName - [string] Dataset Name
     * @internal
     */
    internalGetDataset(datasetName) {
        const dataSet = slug(datasetName);
        if (!this.dataStore[dataSet]) {
            this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
        }
        return this.dataStore[dataSet];
    }
    /**
     * Save `newData` to `path` of `datasetName`
     * @param datasetName - [string] Dataset Name
     * @param path - [string] Path string to save newData
     * @param newData - [any] JSON object to save
     * @returns difference of new and old data or true when there's no reference.
     */
    internalDoSaveData(datasetName, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(datasetName);
            edgecommonconfig_1.default.status('internalDoSaveData Starting');
            const agent = '';
            const differences = yield ds.doUpdate(path, newData, agent);
            if (Array.isArray(differences)) {
                edgecommonconfig_1.default.status(`internalDoSaveData Complete:${differences.length}`);
            }
            if (differences != null) {
                return differences;
            }
            return true;
        });
    }
    /**
     * Create or verify an index on a collection
     * @param dataSet - [string] Data Set Name
     * @param tableName - [string] Collection Name
     * @param keyField - [string] Field name to index
     * @param indexType - [string] Index type: mongo field
     * @param isUnique - [string] Uniqueness to provide mongo indexing function
     */
    doVerifySimpleIndex(dataSet, tableName, keyField, indexType, isUnique) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            const result = yield ds.doVerifySimpleIndex(tableName, keyField, indexType, isUnique);
            return result;
        });
    }
    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param path - [mixed] Can be a compiled path or string path
     * @param centerLocation - [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles - [number] the number of miles to search around
     * @param offset - [number] Optional starting point in the result list (default 0)
     * @param limit - [number] Optional the maximum number of results (default 1000)
     */
    doFindNearby(dataSet, path, centerLocation, miles, offset, limit) {
        const ds = this.internalGetDataset(dataSet);
        return ds.doFindNearby(path, centerLocation, miles, offset, limit);
    }
    /**
     * DoFindNearbyIds - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * Difference to doFindNearby: it runs on mongo aggregation
     * @param path - [mixed] Can be a compiled path or string path
     * @param centerLocation - [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles - [number] the number of miles to search around
     * @param offset - [number] Optional starting point in the result list (default 0)
     * @param limit - [number] Optional the maximum number of results (default 1000)
     */
    doFindNearbyIds(dataSet, path, centerLocation, miles, offset, limit) {
        const ds = this.internalGetDataset(dataSet);
        return ds.doFindNearbyIds(path, centerLocation, miles, offset, limit);
    }
    /**
     * Do call doGetItems with limit 1000000
     * @param dataSet - [string] Data Set Name
     * @param path - [string] path to collect
     */
    doGetAllItems(dataSet, path) {
        return this.doGetItems(dataSet, path, null, null, 0, 1000000);
    }
    /**
     * Do search on `dataset` within `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param dataSet - [string] dataset name
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortOrder - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetItems(dataSet, path, fieldList, sortOrder, offset, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status(`doGetItems dataSet=${dataSet}, path=${path}, Sort=`, sortOrder);
            const ds = this.internalGetDataset(dataSet);
            const all = yield ds.doGetItems(path, fieldList, sortOrder, offset, limit);
            return all;
        });
    }
    /**
     * Same as doGetItems but stream the data
     * Do search on `dataset` within `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param user - [ApiSocketUser] socket user instance
     * @param dataSet - [string] dataset name
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortOrder - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetItemsStream(user, dataSet, path, fieldList, sortOrder, offset, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status(`doGetItemsStream dataSet=${dataSet}, path=${path}, Sort=`, sortOrder);
            const ds = this.internalGetDataset(dataSet);
            const stream_id = user.startStreamingList();
            ds.doStream(path, fieldList, offset, limit, (record) => {
                return user.sendPush(stream_id, record);
            }).then((result) => {
                return user.endStreamingList(stream_id);
            });
            return stream_id;
        });
    }
    /**
     * Returns 1 item for path inside dataset
     * @param dataSet - [string] datasetname
     * @param path - [string] path string
     * @returns cached data in path
     */
    doGetItem(dataSet, path) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            edgecommonconfig_1.default.status(`doGetItem [${dataSet}][${path}]`);
            const item = yield ds.doGetItem(path, false);
            if (item != null && item.base != null) {
                const key = `LU${dataSet}_${path}`;
                const recordHash = edgeapi_1.default.getHash(item.base);
                edgeapi_1.default.cacheSet(key, recordHash);
                item.base._hash = recordHash;
                return item.base;
            }
            edgecommonconfig_1.default.status(`doGetitem path=${path} returning null`);
            return null;
        });
    }
    /**
     * Lookup items within IdList
     * @param user - [ApiSocketUser]
     * @param dataSet - [string]
     * @param collection - [string]
     * @param idList - [hash|ObjectId]
     */
    doGetItemsIfNeeded(user, dataSet, collection, idList) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                edgecommonconfig_1.default.status(`doGetItemsIfNeeded path=${dataSet}/${collection}`, idList);
                if (dataSet == null || collection == null || idList == null) {
                    reject('Invalid call, missing parameter');
                }
                const stream_id = user.startStreamingList();
                resolve(stream_id);
                // Split out the records that we need for sure and the ones
                // we need to check with Redis to see if we have them already
                const databaseNeeded = [];
                const allTest = [];
                const allPromise = [];
                if (typeof idList === 'object' && !Array.isArray(idList)) {
                    idList = [idList];
                }
                for (let item of idList) {
                    if (typeof item === 'string') {
                        databaseNeeded.push(item);
                    }
                    else if (typeof item === 'number') {
                        databaseNeeded.push(item);
                    }
                    else {
                        if (!item.id) {
                            this.log.error('Invalid Call', {
                                api: 'doGetItemsIfNeeded',
                                dataSet,
                                collection,
                                item,
                                error: 'Missing ID',
                            });
                            continue;
                        }
                        // If there is no hash, we need it.
                        // if there is a hash, we have to check them
                        if (item.hash == null || item.hash === '') {
                            databaseNeeded.push(item.id);
                        }
                        else {
                            // Check the hash in Redis
                            allTest.push(item);
                            allPromise.push(edgeapi_1.default.cacheGet(`LU${dataSet}${`_/${collection}/${item.id}`}`));
                        }
                    }
                }
                if (allPromise.length === 0 && databaseNeeded.length === 0) {
                    edgecommonconfig_1.default.status('doGetItemsIfNeeded nothing to return');
                    // Make sure end streaming list after streamer is created in client.
                    setTimeout(() => {
                        return user.endStreamingList(stream_id);
                    }, 10);
                    resolve(null);
                }
                // We have 2 lists,
                // databaseNeeded - Confirmed records we need from the database
                // allTest - Items we just checked in redis and need to confirm
                // config.status "After Stage 1, databaseNeeded=", databaseNeeded.length, "allTest=", allTest.length
                Promise.all(allPromise).then(results => {
                    for (let result of Array.from(results)) {
                        let item = allTest.shift();
                        // if result? and item.hash == result
                        // 	user.sendPush stream_id, { id: item.id, useCache: true }
                        // else
                        // 	databaseNeeded.push item.id
                        // brian - force all to download fresh for testing
                        databaseNeeded.push(item.id);
                    }
                    if (databaseNeeded.length > 0) {
                        const ds = this.internalGetDataset(dataSet);
                        if (ds == null) {
                            return null;
                        }
                        const strList = databaseNeeded.join(',');
                        return ds.doStream(`/${collection}/in:${strList}`, {}, 0, null, (record) => {
                            const recordHash = edgeapi_1.default.getHash(record);
                            const key = `LU${dataSet}_/${collection}/${record.id}`;
                            edgecommonconfig_1.default.status(`doGetItemsIfNeeded Setting key=${key} to ${recordHash}`);
                            edgeapi_1.default.cacheSet(key, recordHash);
                            record._hash = recordHash;
                            return user.sendPush(stream_id, record);
                        }).then((result) => {
                            user.endStreamingList(stream_id);
                            return new Date().getTime();
                        }).catch((_err) => {
                            user.endStreamingList(stream_id);
                            throw _err;
                        });
                    }
                    else {
                        // Make sure end streaming list after streamer is created.
                        setTimeout(() => {
                            return user.endStreamingList(stream_id);
                        }, 10);
                    }
                });
                resolve(null);
            });
        });
    }
    /**
     * Lookup a record within a dataset
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @param currentHash - [string] hash string
     * @returns cached data unless data is not updated
     */
    doGetItemIfNewer(dataSet, path, currentHash) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const key = `LU${dataSet}_${path}`;
            const cacheResult = yield edgeapi_1.default.cacheGet(key);
            if (cacheResult != null && cacheResult === currentHash) {
                resolve(true);
            }
            const ds = this.internalGetDataset(dataSet);
            // Load the record from the database
            let recordHash = null;
            const item = yield ds.doGetItem(path, false);
            if (item != null && item.base != null) {
                recordHash = edgeapi_1.default.getHash(item.base);
                edgeapi_1.default.cacheSet(key, recordHash);
                edgecommonconfig_1.default.status(`doGetItemIfNewer Setting key=${key} to ${recordHash}`);
                item.base._hash = recordHash;
                resolve(item.base);
            }
            else {
                this.log.info('Path not found', { dataSet, path, currentHash });
                edgecommonconfig_1.default.status(`Path not found ${path}`);
                resolve(null);
            }
        }));
    }
    /**
     * Execute an aggregate. Excute doAggregate of DataSetManager with aggregation object
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @param aggList - [object] mongo aggregation object
     */
    doAggregate(dataSet, path, aggList) {
        const ds = this.internalGetDataset(dataSet);
        return ds.doAggregate(path, aggList);
    }
    /**
     * Delete collection found by path inside dataset
     * @param dataSet - [string] dataset name
     * @param path - [string] path string
     * @returns true whatever the case is
     */
    doDeletePath(dataSet, path) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            yield ds.doDeletePath(path);
            return true;
        });
    }
    /**
     * Act exactly like doUpate. Keeping just not to break old client
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePathHigh(dataSet, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.doUpdatePath(dataSet, path, newData);
        });
    }
    /**
     * Update data in path of dataset with newData
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePath(dataSet, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            // Check to see if we have an exact match
            const ds = slug(dataSet);
            const key = `LU${ds}_${path}`;
            const recordHash = edgeapi_1.default.getHash(newData);
            const oldHash = yield edgeapi_1.default.cacheGet(key);
            if (oldHash === recordHash) {
                return false;
            }
            edgecommonconfig_1.default.status('doUpdatePath dataSet=', dataSet, 'path=', path, 'newData=', newData);
            edgeapi_1.default.cacheSet(key, recordHash);
            return this.internalDoSaveData(dataSet, path, newData);
        });
    }
    /**
     * Act exactly like doUpate. Keeping just not to break old client
     * @param dataSet - [string] datasetname
     * @param path - [string] path name
     * @param newData - [any] object
     */
    doUpdatePathLow(dataSet, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.doUpdatePath(dataSet, path, newData);
        });
    }
    /**
     * Run mongo remove command
     * @param dataSet - [string]
     * @param tableName - [string]
     * @param searchCondition - [any] condition to delete
     */
    doRemove(dataSet, tableName, searchCondition) {
        const ds = this.internalGetDataset(dataSet);
        return ds.doRemove(tableName, searchCondition);
    }
    /**
     * Get a list of tables and counts open
     * @param dataSet
     */
    doGetUpdatedStats(dataSet) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            const result = yield ds.doGetUpdatedStats();
            return result;
        });
    }
    /**
     * Basically select path, count(*) group by path
     * @param dataSet - [string]
     * @param path - [string]
     * @param matchCondition - [any]
     */
    doGetUniqueValues(dataSet, path, matchCondition) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            const result = yield ds.doGetUniqueValues(path, matchCondition);
            return result;
        });
    }
    /**
     * Given a path and some json data, append the data to the path
     * treating the path like an array. if the path is not an array then
     * we find the next id and add it that way.
     * @param dataSet - [string]
     * @param path - [string]
     * @param newData - [any] object
     */
    doAppendPath(dataSet, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            const result = yield ds.doAppend(path, newData);
            return result;
        });
    }
    /**
     * Api for calculating count for given path
     * @param dataSet - [string]
     * @param path - [string]
     */
    doCount(dataSet, path) {
        return __awaiter(this, void 0, void 0, function* () {
            const ds = this.internalGetDataset(dataSet);
            const result = yield ds.doCount(path);
            return result;
        });
    }
}
exports.default = ProviderData;
;
//# sourceMappingURL=ProviderData.js.map