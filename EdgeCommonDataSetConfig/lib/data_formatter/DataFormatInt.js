"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for int data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const common = new DataFormatCommon_1.DataFormatCommon();
class DataFormatInt extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'int';
        this.width = 90;
        this.input_width = 90;
        this.align = 'align';
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null)) {
            return "";
        }
        if ((data === null) || ((typeof data === "string") && (data.length === 0))) {
            return "";
        }
        if ((options != null) && (options !== null)) {
            return numeral(common.getNumber(data)).format(options);
        }
        return numeral(common.getNumber(data)).format("#,###");
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        const num = common.getNumber(data);
        if (isNaN(num)) {
            return "";
        }
        return Math.round(num);
    }
    attachEditor(elSourceInput, path) {
        // #
        // # set type of this input as "number"
        return elSourceInput.css("text-align", "right");
    }
}
exports.DataFormatInt = DataFormatInt;
exports.default = DataFormatInt;
//# sourceMappingURL=DataFormatInt.js.map