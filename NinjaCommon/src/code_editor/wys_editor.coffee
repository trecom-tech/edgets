##| WYSIWYG Editor Widget
##|
##| example usage:
##| we = new WysEditor($('#test'));
##| @param [jQuery Element] elementHolder in which the code editor will be rendered
##|

class WysEditor

	constructor: (@elementHolder, optionData) ->
		if !@elementHolder.length
			throw new Error "The specified element #{@elementHolder.selector} not found"

		##| check if summernote is loaded
		if typeof $.summernote == "undefined"
			throw new Error "Summernote is not loaded on which this component depends, so summernote must be loaded first"

		@gid = GlobalValueManager.NextGlobalID()
		@statusbarHeight = 8
		options =
			## You can use airMode that provides hidden toolbar by following line
			## airMode: true
			placeholder: 'Type here...'
			tabsize: 2
			height: 500
			minHeight: null
			maxHeight: null
			dialogsInBody: true			
			toolbar:[
				['style',['style']],
				['font',['bold','italic','underline','clear']],
				['fontname',['fontname']],
				['color',['color']],
				['para',['ul','ol','paragraph']],
				['height',['height']],
				['table',['table']],
				['insert',['media','link','hr']],
				['view',['fullscreen','codeview']],
				['help',['help']]
			]
			buttons: {}
			focus: true
			hint: 
				mentions: []
				match: /\B@(\w*)$/
				search: (keyword, callback) ->
					# console.log "In search function: ", keyword, @mentions, callback 
					callback($.grep(@mentions, (item) =>
						return item.indexOf(keyword) == 0
					))
				content: (item) =>
					return '@' + item

		@options = Object.assign {}, options, optionData
		@elementHolder.summernote @options

	## get content of editor
	getContent: () =>
		@elementHolder.summernote 'code'

	getText: () =>
		$(@elementHolder.summernote('code')).text()

	## set content of editor
	## @param [HTML Markup] strMarkUp
	setContent: (strMarkUp) =>
		@elementHolder.summernote 'code', strMarkUp

	setText: (str) =>
		if !str then return false
		@elementHolder.summernote 'code', "<p>#{str}</p>"

	## set event handler for editor
	## for example: 
	## wysEditor.setEventHandler 'change', (event) => ...
	## valid event types are: 
	## 'init', 'enter', 'focus', 'blur', 'keyup/down', 'paste', 'image.upload', 'change'
	setEventHandler: (handlerName, callback) =>
		@elementHolder.on "summernote.#{handlerName}", callback
	
	## set references for hint to mention
	## you can use this by typing '@'
	## @param [Array] refList: list of references
	## @return [Boolean] true: if successfully set
	setReferenceList: (refList) => 
		if !Array.isArray(refList) then return false
		@options.hint.mentions = refList
		@elementHolder.summernote @options
		true

	## create custom button
	## @param [String] title: title of button
	## @param [String] tooltip: tooltip of button
	## @param [Function] callback: callback binded to button
	createButton: (title, tooltip, callback, icon) =>
		if @options.buttons[title] then return @options.buttons[title]
		newButton =	(context) =>
			ui = $.summernote.ui
			# create button
			button = ui.button
				contents: "#{if icon? then icon + " " else ""} #{title}"
				tooltip: "#{tooltip}"
				click:  callback
			## render the new button
			button.render()
		## update SummerNote Options to add this new button to toolbar
		@options.toolbar.push ["#{title}", ["#{title}"]]
		@options.buttons[title] = newButton
		@updateEditorWithHeight()

	## Set toolbar items
	setToolbarItems: (items) =>
		if !items then return false
		@options.toolbar = items
		@updateEditorWithHeight()

	## function to resize editor
	## @param [Number] w: width of parent view
	## @param [Number] h: height of parent view
	onResize: (w, h) =>
		@toolbarHeight = @elementHolder.next('.note-editor').find('.note-toolbar').outerHeight()
		newHeight = h - @toolbarHeight - @statusbarHeight
		if @options.height isnt newHeight
			@options.height = newHeight
			@elementHolder.summernote 'destroy'
			@elementHolder.summernote @options

	##
	## Update summernote's options as well as its height possibly
	updateEditorWithHeight: () =>
		tbHeight = @elementHolder.next('.note-editor').find('.note-toolbar').outerHeight()
		if @toolbarHeight isnt tbHeight
			@options.height = @options.height + (@toolbarHeight - tbHeight)
			@toolbarHeight  = tbHeight
			@elementHolder.summernote 'destroy'
		@elementHolder.summernote @options

	##
	## set AirMode of the editor
	setAirMode: (bool) =>
		currentMode = if @options.airMode? then @options.airMode else false
		if bool is currentMode then return false
		@options.airMode = bool
		@elementHolder.summernote 'destroy'
		@elementHolder.summernote @options
		true

	##
	## Set a hotkey in this editor
	setHotkey: (keyboard, callback) =>
		if !keyboard or !callback or typeof(callback) isnt "function" then return false
		if !@hotkeyList then @hotkeyList = {}
		@hotkeyList[keyboard] = callback
		@elementHolder.on 'summernote.keydown', (we, e) ->
			e = e || window.event 
			if e.ctrlKey and (e.keyCode is keyboard.toUpperCase().charCodeAt(0) or e.keyCode is keyboard.toLowerCase().charCodeAt(0))
				e.preventDefault()
				e.stopPropagation()
				callback(we, e)
		true

	##
	## Set focus in this Summernote editor 
	setFocus: () =>
		@elementHolder.summernote "focus"


			