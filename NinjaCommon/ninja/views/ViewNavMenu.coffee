class ViewNavMenu extends View
	onShowScreen: () =>
		@wgtGroups = @add "ul", "view-navmenu-groups"
		@groups = []
		@boundaryWidth = 150

	setTitle: (title) =>
		if !title? or title is "" then return false
		@title = title
		if !@wgtTitle?
			@wgtTitle = @addAtPosition "div", "view-navmenu-title", 0		
		@wgtTitle.html "<span>#{@title}</span>"

	setBoundaryWidth: (w) =>
		if w <= 0 then return false
		@boundaryWidth = w
		@updateviewOptions()

	##|
	##| Remove selected status on all child items in all groups
	deselectAll: ()=>
		for group in @groups
			group.deselectAll()
		true

	addGroup: (options) =>
		for g in @groups
			if g.options.title is options.title
				return g

		options.parentView = this
		options.id         = GlobalValueManager.NextGlobalID()
		groupWidget        = @wgtGroups.add 'li', 'ninja-nav-group collapsed', 'nav-group-' + options.id,
			'data-toggle': 'collapse'
			'data-target': '#ninja-nav-group-items-' + options.id
		group = new NavGroup groupWidget, options
		@groups.push group
		group

	resetGroups: () =>
		@wgtGroups.destroy()
		@wgtGroups = @add "ul", "view-navmenu-groups"
		@groups = []

	onResize: (w, h) =>
		@narrowedMode = (@width() < @boundaryWidth)
		for group in @groups
			group.onResize(w, h, @narrowedMode)

	serialize: () =>
		groups = @groups.map (g) ->
			items = g.items?.map (i) ->
				return
					options: i.options
			return
				options: g.options
				items: items
		return
			groups: groups

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? or typeof options isnt "object"
				reject "ViewNavMenu: invalid data passed"

			if options.groups?
				for g in options.groups
					group = @addGroup g.options

					for i in (g.items or [])
						item = group.addItem i.options

			resolve true