"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var process = require("process");
var edgecommonconfig_1 = require("edgecommonconfig");
// we can not use import syntax for these libraries because they are not not typescript ready
var azureStorage = require('azure-storage');
var entGen = azureStorage.TableUtilities.entityGenerator;
var maxDateTime = new Date(2100, 1, 1).getTime();
var globalCounter = 0;
var BulkTableStorageResult = /** @class */ (function () {
    function BulkTableStorageResult(result, tableSvc, tableName, query) {
        this.tableSvc = tableSvc;
        this.tableName = tableName;
        this.query = query;
        this.rows = [];
        if (result && result.entries) {
            for (var _i = 0, _a = result.entries; _i < _a.length; _i++) {
                var rec = _a[_i];
                this.rows.push(this.entToObject(rec));
            }
        }
        this.length = this.rows.length;
        this.continuationToken = result.continuationToken;
        // continuationToken:
        // { nextPartitionKey: '1!32!Y29sb255LW5lZWRlZGxndjNub2RlMTA-',
        //     nextRowKey: '1!28!MDI2MDIzODQzODAzNjctMTI5NTc-',
        //     targetLocation: 0 } }
    }
    /**
     * Convert from record to JSON object
     */
    BulkTableStorageResult.prototype.entToObject = function (rec) {
        var newData = {};
        for (var varName in rec) {
            if (!rec[varName]) {
                continue;
            }
            newData[varName] = (varName === '.metadata') ? rec[varName] : rec[varName]._;
        }
        return newData;
    };
    return BulkTableStorageResult;
}());
var BulkTableStorage = /** @class */ (function () {
    /**
     * Pass in the config name as defined in the credential like this:
     * azureTableStorage =
     *      connection: AZURE_STORAGE_CONNECTION_STRING
     */
    function BulkTableStorage(configName, tableName) {
        this.log = edgecommonconfig_1.config.getLogger("BulkTableStorage-" + tableName);
        this.isReady = false;
        this.enabled = true;
        this.configName = configName;
        this.tableName = tableName;
        this.creds = edgecommonconfig_1.config.getCredentials(this.configName);
    }
    /**
     * Insert a record using an inverse date time where lower values are newer so they query
     * at the top of the azure table results
     */
    BulkTableStorage.prototype.doInsertLogRecord = function (partKey, newData) {
        if (!newData) {
            return false;
        }
        if (typeof newData !== 'object') {
            return false;
        }
        globalCounter++;
        var rowkey = "0" + (maxDateTime - new Date().getTime());
        rowkey += "-" + globalCounter.toString(16);
        rowkey += "-" + process.pid;
        return this.doInsertRecord(partKey, rowkey, newData, true);
    };
    BulkTableStorage.prototype.createQuery = function (fieldName, value, limit) {
        if (limit === void 0) { limit = null; }
        if (!value) {
            return null;
        }
        if (fieldName.length === 0) {
            return null;
        }
        var query = new azureStorage.TableQuery();
        if (limit) {
            query.top(limit);
        }
        if (fieldName && value) {
            query.where(fieldName + " eq ?", value);
        }
        return query;
    };
    BulkTableStorage.prototype.doQuery = function (query) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!query) {
                resolve(false);
            }
            _this.doInternalOpenTable()
                .then(function (status) {
                if (!status) {
                    resolve(false);
                }
                _this.tableSvc.queryEntities(_this.tableName, query, null, function (err, result, response) {
                    if (err) {
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " data=", query);
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " err=", err);
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " result=", result);
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " response=", response);
                        _this.log.error('doQuery', { tableName: _this.tableName, err: err });
                        resolve(new BulkTableStorageResult(null, _this.tableSvc, _this.tableName, query));
                    }
                    else {
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " result=", result);
                        edgecommonconfig_1.config.status("BulkTableStorage - doQuery " + _this.tableName + " response=", response);
                        resolve(new BulkTableStorageResult(result, _this.tableSvc, _this.tableName, query));
                    }
                });
            }).catch(function () {
                resolve(false);
            });
        });
    };
    BulkTableStorage.prototype.doInternalOpenTable = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.creds || !_this.creds.connection) {
                edgecommonconfig_1.config.status("BulkTableStorage doInternalOpenTable no credentials for " + _this.configName);
                _this.enabled = false;
                resolve(false);
            }
            // Open / Create the table and leave room for later async initialization
            edgecommonconfig_1.config.status("BulkTableStorage - Initialize Table " + _this.tableName);
            _this.tableSvc = azureStorage.createTableService(_this.creds.connection);
            _this.tableSvc.createTableIfNotExists(_this.tableName, function (err, result, reponse) {
                // config.status 'BulkTableStorage - createTable #{@tableName} result=', result
                // config.status 'BulkTableStorage - createTable #{@tableName} response=', response
                if (err) {
                    edgecommonconfig_1.config.status("BulkTableStorage - createTable " + _this.tableName + " err=", err);
                    _this.enabled = false;
                    _this.log.error('createTableIfNotExists', { talbeName: _this.tableName, err: err });
                    resolve(false);
                }
                else {
                    _this.isReady = true;
                    resolve(true);
                }
            });
        });
    };
    BulkTableStorage.prototype.internalSetupTabledata = function (partKey, rowKey, newData) {
        var tableData = {};
        tableData.PartitionKey = entGen.String(partKey);
        tableData.RowKey = entGen.String(rowKey);
        var objToEnt = function (obj, prefix) {
            // See https://azure.github.io/azure-storage-node/TableUtilities.html
            for (var varName in obj) {
                var value = obj[varName];
                var safeName = varName.replace(/[^a-zA-Z0-9]/g, '_');
                safeName = safeName.replace(/^_/, '');
                safeName = "" + prefix + safeName;
                if (typeof value === 'string') {
                    tableData[safeName] = entGen.String(value);
                }
                else if (typeof value === 'object' && typeof value.getTime === 'function') {
                    tableData[safeName] = entGen.DateTime(value);
                }
                else if (typeof value === 'number') {
                    if (Math.floor(value) === value) {
                        tableData[safeName] = entGen.Int32(value);
                    }
                    else {
                        tableData[safeName] = entGen.Double(value);
                    }
                }
                else if (typeof value === 'boolean') {
                    tableData[safeName] = entGen.Boolean(value);
                }
                else if (typeof value === 'object') {
                    objToEnt(value, safeName + "_");
                }
            }
            return true;
        };
        objToEnt(newData, '');
        return tableData;
    };
    /**
     * see https://github.com/Azure/azure-storage-node/blob/master/lib/services/table/tableservice.js
     */
    // private doInsertOrReplace(partKey: any, rowKey: any, newData: any) {
    //     return new Promise((resolve, reject) => {
    //         if (!this.enabled) {
    //             resolve(false);
    //             return;
    //         }
    //         let tableData = this.internalSetupTabledata(partKey, rowKey, newData);
    //         this.doInternalOpenTable()
    //         .then((status) => {
    //             if (!status) {
    //                 resolve(false);
    //                 return;
    //             }
    //             config.status(`BulkTableStorage doInsertOrReplace ${this.tableName}:`, newData);
    //             let options: any = {};
    //             this.tableSvc.insertOrReplaceEntity(this.tableName, tableData, options, (err: any, result: any, response: any) => {
    //                 if (err) {
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} data=`, tableData);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} err=`, err);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} result=`, result);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} response=`, response);
    //                     this.log.error('insertOrReplaceEntity', {tableName: this.tableName, data: tableData, err: err});
    //                     resolve(false);
    //                 } else {
    //                     resolve(true);
    //                 }
    //             });
    //         }).catch(() => {
    //             resolve(false);
    //         });
    //     });
    // }
    BulkTableStorage.prototype.doInsertRecord = function (partKey, rowKey, newData, ignoreFailed) {
        var _this = this;
        if (ignoreFailed === void 0) { ignoreFailed = true; }
        return new Promise(function (resolve, reject) {
            if (!_this.enabled) {
                resolve(false);
                return;
            }
            var tableData = _this.internalSetupTabledata(partKey, rowKey, newData);
            _this.doInternalOpenTable()
                .then(function (status) {
                if (!status) {
                    resolve(false);
                }
                edgecommonconfig_1.config.status("BulkTableStorage doInsertRecord " + _this.tableName + ":", newData);
                _this.tableSvc.insertEntity(_this.tableName, tableData, { echoContent: false }, function (err, result, response) {
                    if (result) {
                        result.rowkey = rowKey;
                        result.partKey = partKey;
                    }
                    if (err) {
                        edgecommonconfig_1.config.status("BulkTableStorage - doInsertRecord " + _this.tableName + " data=", tableData);
                        edgecommonconfig_1.config.status("BulkTableStorage - doInsertRecord " + _this.tableName + " err=", err);
                        edgecommonconfig_1.config.status("BulkTableStorage - doInsertRecord " + _this.tableName + " result=", result);
                        edgecommonconfig_1.config.status("BulkTableStorage - doInsertRecord " + _this.tableName + " response=", response);
                        _this.log.error('intsertEntity', { tableName: _this.tableName, data: tableData, err: err });
                        if (ignoreFailed) {
                            resolve(false);
                        }
                        else {
                            reject(err);
                        }
                    }
                    else {
                        resolve(true);
                    }
                });
            }).catch(function () {
                resolve(false);
            });
        });
    };
    return BulkTableStorage;
}());
exports.default = BulkTableStorage;
//# sourceMappingURL=BulkTableStorage.js.map