/**
 * Class to normalized EdgeMailer Error.
 */
export class EdgeCommonMailerError extends Error {
    public message  :string;
    public code     :string;
    public options  :any;

    constructor(message: string, code: string = 'Error', options: any = {}) {

        super(message);

        this.message    = message;
        this.code       = code;
        this.options    = options;

        Error.captureStackTrace(this, this.constructor);
    }
}

export default EdgeCommonMailerError;
