
#
# global variable ioserver is defined in index.jade
#
class SocketConnection extends EdgeApiBase

    onCmdReplyComplete: ()=>

        if @elBusy?
            @elBusy.hide()

    #
    # Event received from DataMap on all data changes, make this code small or it will be very slow
    onGlobalNewData: (e)=>
        table_name = e.detail.tablename
        record_id = e.detail.id

        if !@monitor_table[table_name]?
            # console.log "onGlobalNewData not watching #{table_name}"
            return false

        if record_id in @monitor_table[table_name].list
            # console.log "onGlobalNewData already #{table_name} / #{record_id}"
            return false

        # console.log "subscribing /#{@monitor_table[table_name].tableName}/#{record_id}"
        @subscribe @monitor_table[table_name].dataSet, "/#{@monitor_table[table_name].tableName}/#{record_id}"

        true

    #
    # Once connected, setup events to watch for DataMap changes or server side changes and map them
    # dataSet / tableName -> Database settings
    # subPath -> optional sub path within the database object
    # map to the base of a table in the DataMap
    #
    setupChangeEvents: (dataSet, tableName, dataMapTable)=>

        if !dataMapTable? then dataMapTable = tableName

        if !@monitor_list?
            @monitor_list = {}
            @monitor_table = {}
            window.addEventListener "new_data", @onGlobalNewData, false

        if !@monitor_list[dataSet]
            @monitor_list[dataSet] = {}

        if !@monitor_list[dataSet][tableName]?
            @monitor_list[dataSet][tableName] = {}
            @monitor_list[dataSet][tableName].table = dataMapTable
            @monitor_table[dataMapTable] =
                dataSet  : dataSet
                tableName: tableName
                list     : []

        true

    removeChangeEvents: (dataSet, tableName, rec_id)=>

        if !@monitor_list? then return
        if !@monitor_list[dataSet]? then return
        if !@monitor_list[dataSet][tableName]? then return

        if rec_id?

            newList = []
            for id in @monitor_list[dataSet][tableName].list
                if id == rec_id
                    @unsubscribe "/#{tableName}/#{id}"
                else
                    newList.push id

            @monitor_list[dataSet][tableName].list = newList

        return true


    #
    # When a change record comes in, find the associated element
    # change that using the correct data formatters.   Change records
    # are sent from the server to the connection when the database changes.
    # We only get them if we use "subscribe" function to watch
    # for a change.
    #
    updateValue: (e) =>

        dataSet = e.ds
        #
        # Assume we are getting the data change event because we are
        # subscribed and thus DataMap has some data already loaded.
        # note that this doesn't update locallyCachedData in the browser
        # cache at this time.
        #

        parts = e.path.split "/"
        if !parts? or parts.length < 4
            console.log "ERROR: sock.updateValue parts not long enough:", e
            return

        parts.shift()
        table_name = parts.shift()
        record_id  = parts.shift()

        #
        # Loop through sub paths
        newData = {}
        basePointer = newData
        while parts.length > 1
            name = parts.shift()
            basePointer[name] = {}
            basePointer = basePointer[name]

        #
        # Specific field change
        name = parts.shift()
        basePointer[name] = e.rhs

        if e.kind != "E"
            DataMap.newDataEvent(table_name, record_id, newData, e)
            # console.log "TODO: sock.updateValue kind=#{e.kind}:", e
            return

        # console.log "Server edit dataSet=#{dataSet} table=#{table_name} record=#{record_id} save=", newData

        if @monitor_list? and @monitor_list[dataSet]? and @monitor_list[dataSet][table_name]?
            DataMap.addDataUpdateTable @monitor_list[dataSet][table_name].table, record_id, newData
        else
            DataMap.addDataUpdateTable table_name, record_id, newData

        true

    doGetSocket: () =>

        new Promise (resolve, reject) =>

            do ioLoadedCheck = ()=>

                if !io?
                    console.log "IO Not yet loaded"
                    setTimeout ioLoadedCheck, 1000
                else
                    @socket = io.connect()
                    @socket.on "connect", (a, b)=>
                        # console.log "SOCKET connect event"
                        @setupChangeEvents()
                        true

                    @socket.on "connect_error", (a, b)=>
                        # console.log "SOCKET connect_error a=", a, "b=", b
                        true

                    @socket.on "connect_timeout", (a, b)=>
                        # console.log "SOCKET connect_timeout a=", a, "b=", b
                        true

                    @socket.on "reconnecting", (a, b)=>
                        # console.log "SOCKET reconnecting a=", a, "b=", b
                        true

                    @socket.on "reconnect_error", (a, b)=>
                        # console.log "SOCKET reconnect_error a=", a, "b=", b
                        true

                    @socket.on "reconnect_failed", (a, b)=>
                        # console.log "SOCKET reconnect_failed a=", a, "b=", b
                        true

                    resolve(@socket)


    setBusy : (message) =>

        if !@elBusy?
            @elBusy = $("#busyMessage")

        if typeof message == "undefined" or message == null
            @elBusy.hide()
        else
            @elBusy.html "<i class='fa fa-asterisk fa-spin'></i> " + message
            @elBusy.show()

        true


$ ->

    window.sock = new SocketConnection()

