import {expect}               from 'chai';
import { config }             from 'edgecommonconfig';
import EdgeCommonNotification from '../src/EdgeCommonNotification';
import { IContactInfo }       from '../src/IContactInfo';
import './helper/setup';

const notification = new EdgeCommonNotification();
const { twilioNumber } = config.getCredentials('twilioCreds');

const whatsAppValidContactDetail: IContactInfo = {
    type: 'whatsapp',
    priority: 2,
    account: {
        phoneNumber: '8550967922181', // valid token
    }
};

const whatsAppInvalidContactDetail: IContactInfo = {
    type: 'whatsapp',
    priority: 2,
    account: {
        phoneNumber: '+855096792218', // invalid token
    }
};


const message = {
    title: 'Testing WhatsApp Notification',
    alert: 'Your testing pin code is 123456.',
};

describe('WhatsApp Notification', () => {
    it('doSendWhatsApp() with valid number', async () => {
        const result = await notification.doSendWhatsApp(whatsAppValidContactDetail.account.phoneNumber, message);
        expect(result).to.equal(true);
    });

    it('doSendWhatsApp() with invalid number', async () => {
        const result = await notification.doSendWhatsApp(whatsAppInvalidContactDetail.account.phoneNumber, message);
        expect(result).to.equal(true);
    });
});
