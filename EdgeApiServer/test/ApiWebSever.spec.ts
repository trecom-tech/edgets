import EdgeApi               from 'edgeapi';
import { EdgeRequest }       from 'edgecommonrequest';
import { safeEscapeAppName } from '../src/ApiWebServer';
import { expect }            from 'chai';

const baseUrl         = process.env.API_SERVER;
const httpsServerBase = `https://127.0.0.1:8443`;

// Force http(s).request ignore certificate.
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';


describe('ApiWebServer', () => {

    let api: EdgeApi;
    let request: EdgeRequest;
    
    before(async () => {
        api = await EdgeApi.doGetApi();
        request = new EdgeRequest();
    });


    it('Make initial data', async () => {
        let data = {
            test: 'def',
        };

        return api.data_doUpdatePath('lookup', '/table/test', data).then(function(result: any) {
            console.log(result);
        });
    });

    describe('HTTPS', () => {
        it('findAppNameMiddleware', async () => {
            const result = await request.doGetLink(`${httpsServerBase}/Code`);
            result.should.match(/^<!DOCTYPE/);
        });
        
        it('apiPathMiddleware json', async () => {
            const result = await request.doGetLink(`${httpsServerBase}/api/os/doGetServerInformation.json`);
            
            result.should.have.property('free_mem');
        });
    });
    
    it('safeEscapeAppName', async () => {
        let result = safeEscapeAppName('test.website.com');
        expect(result).to.equal('test_website');

        result = safeEscapeAppName('www.test.website.com');
        expect(result).to.equal('test_website');

        result = safeEscapeAppName('test.WEBsite.edu');
        expect(result).to.equal('test_website');

        result = safeEscapeAppName('www.__test.com');
        expect(result).to.equal('__test');

        result = safeEscapeAppName('test..cool.com');
        expect(result).to.equal('test__cool');
        
        result = safeEscapeAppName('localhost');
        expect(result).to.equal('localhost');

        result = safeEscapeAppName('127.0.0.1');
        expect(result).to.equal('127_0_0_1');
    });

    it('hostRedirectMiddleware', async () => {
        const result = await request.doGetLink(`${httpsServerBase}/`);
        result.should.match(/127.0.0.1\/127_0_0_1/);
    });

    it('apiPathMiddleware xml', async () => {
        const result = await request.doGetLink(`${baseUrl}/api/os/doGetServerInformation.xml`);
        result.should.have.property('root');
    });

    it('apiPathMiddleware json', async () => {
        const result = await request.doGetLink(`${baseUrl}/api/os/doGetServerInformation.json`);
        result.should.have.property('free_mem');
    });

    // TODO should figure out why this is failing in gitlab builder
    it.skip('dataPathMiddleware xml', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/data/lookup/table.xml`);
        result.should.have.property('root');
    });

    // TODO should figure out why this is failing in gitlab builder
    it.skip('dataPathMiddleware json', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/data/lookup/table.json`);
        const objs = result.filter((item: any) => item.id === 'test');
        objs.length.should.be.greaterThanOrEqual(1);
        objs[0].should.have.property('_id');
    });

    it('findAppNameMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/Code`);
        result.should.match(/^<!DOCTYPE/);
    });

    it('findStaticFilePath', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/fonts/usesf.css`);
        result.should.match(/^\/\* Generat/);
    });

    it('findStaticFilePath', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/vendor/bootstrap.min.css`);
        result.should.match(/Bootstrap v3\.3\.6/);
    });

    it('findStaticFilePath', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/vendor/prism.css`);
        result.indexOf('http://prismjs.com/download').should.not.be.eql(-1);
    });

    it('findAppNameMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/css/baseedgeweb.css`);
        result.indexOf('margin-bottom: 30px').should.not.be.eql(-1);
    });

    it('loadLessFiles', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/ninja/ninja.css`);
        result.indexOf('.pleaseWaitDialog').should.not.be.eql(-1);
    });

    it('loadLessFiles', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/none/none.css`);
        result.indexOf('Unknown call to /none/none.css').should.not.be.eql(-1);
    });

    it('screenMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/views/ViewNavMenu.js`);
        result.indexOf('var ViewNavMenu').should.not.be.eql(-1);
    });

    it('finalMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/xyz/abc/def`);
        result.indexOf('Unknown call to /xyz/abc/def').should.not.be.eql(-1);
    });

    it('addRouteMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/testForm.json`);
        result.indexOf('Test Result as a string').should.not.be.eql(-1);
    });

    it('banHackerMiddleWare', async () => {
        const url = new URL(baseUrl);

        let result: any = await request.doInternalSendRequest({
            host: url.hostname,
            port: url.port,
            path: '/test.php'
        });
        result.res.statusCode.should.be.eql(403);
        result.body.should.be.eql('Permission error');

        result = await request.doInternalSendRequest({
            host: url.hostname,
            port: url.port,
            path: '/test.aspx'
        });
        result.res.statusCode.should.be.eql(403);
        result.body.should.be.eql('Permission error');
    });

    it('screenMiddleware', async () => {
        const result: any = await request.doGetLink(`${baseUrl}/views/NotExisting.js`);
        result.indexOf('NotExisting.prototype.template').should.not.be.eql(-1);
    });
    
});