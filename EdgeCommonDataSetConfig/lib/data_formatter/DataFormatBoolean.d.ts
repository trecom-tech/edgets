import DataFormatterType from './DataFormatterType';
export declare class DataFormatBoolean extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    textYes: string;
    textNo: string;
    textNotSet: string;
    align: string;
    constructor();
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options: any, path: any): string;
    unformat(data: any, path: any): boolean;
}
export default DataFormatBoolean;
