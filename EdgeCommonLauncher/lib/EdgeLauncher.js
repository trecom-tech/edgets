"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const fs = require("fs");
const moment = require("moment");
const chalk_1 = require("chalk");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const child_process_1 = require("child_process");
const ninjadebug_1 = require("ninjadebug");
const shell_quote_1 = require("shell-quote");
const NpmModuleTools_1 = require("./NpmModuleTools");
if (process.env.NODE_ENV === 'test') {
    edgecommonconfig_1.config.setCredentials('redisReadHost', `${process.env.REDIS_HOST}`);
    edgecommonconfig_1.config.setCredentials('redisHostWQ', `${process.env.REDIS_HOST}`);
    edgecommonconfig_1.config.setCredentials('redisHost', `${process.env.REDIS_HOST}`);
    edgecommonconfig_1.config.setCredentials('MongoDB', {
        url: `${process.env.DB_HOST}`,
        options: {
            connectTimeoutMS: 600000,
            poolSize: 16,
            socketTimeoutMS: 600000
        }
    });
}
class EdgeLauncher {
    constructor() {
        let pathParts = __dirname.split("/");
        pathParts.pop();
        //  Set this in the calling code if you want stats to be logged
        this.api = null;
        this.basePath = pathParts.join("/") + "/";
        // this.db                 = new DataSetManager('os');
        // this.dbLogs             = new DataSetManager('logs');
        this.homePath = process.env.HOME;
        this.log = edgecommonconfig_1.config.getLogger("JobRunner");
        //  Set this in the calling code if you want status updates
        this.statusUpdateClient = null;
        //  The result of the job from the server, set after the job is doUpsertOne
        // this.saveResult         = null;
    }
    /**
     *
     * Any job can output one or more "Results=<Number>" lines and this
     * will total up those numbers and add them to the record for reporting.
     *
     * @param job
     * @param command
     * @param fullOutput
     * @param slot_id
     */
    insertJobLog(job, command, fullOutput, slot_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let db = new edgedatasetmanager_1.default('os');
            let dbLogs = new edgedatasetmanager_1.default('logs');
            let total;
            const reResult = new RegExp("Results*=(.*)", "g");
            let m = reResult.exec(fullOutput);
            edgecommonconfig_1.config.status(`JobRunner::insertJobLog saving slot ${slot_id}`);
            if (fullOutput.indexOf("JobRunner: Disable") !== -1) {
                //  Job should be disabled in the future
                job.enabled = false;
                try {
                    yield db.doUpdate(`/job/${job.id}`, job);
                }
                catch (err) {
                    this.log.error(err);
                }
            }
            total = 0;
            while (m != null) {
                // config.dump "Match Result=", m
                if (m[1] != null) {
                    let amount = parseFloat(m[1]);
                    if (!isNaN(amount)) {
                        total += amount;
                    }
                }
                m = reResult.exec(fullOutput);
            }
            if (total == null) {
                total = "";
            }
            if (/Exception in/.test(fullOutput)) {
                total = "Exception";
            }
            //  Save a job log record
            const job_id = job.id.slice(0, 5) + new Date().getTime();
            try {
                this.saveResult = yield dbLogs.doInsert(`/jobrun/${job_id}`, {
                    id: job_id,
                    title: job.title,
                    command: command,
                    duration: job.lastDuration,
                    output: fullOutput,
                    total: total,
                    job_date: new Date(),
                    _lastModified: new Date()
                }, 1);
                return true;
            }
            catch (err) {
                console.log(err);
                return false;
            }
        });
    }
    /**
     *
     * job is an object as defined in "UnderstandingJobs.md"
     * slot_id is any number that is used for reporting and also
     * used to help the real-time status boards keep track of jobs
     *
     * @param job
     * @param slot_id
     */
    runJob(job, slot_id) {
        return __awaiter(this, void 0, void 0, function* () {
            let clearTimer;
            let jobExec;
            let jobStatus;
            let stat;
            let timeout;
            let totalBytes;
            let args;
            let db = new edgedatasetmanager_1.default('os');
            let dbLogs = new edgedatasetmanager_1.default('logs');
            edgecommonconfig_1.config.status("JobRunner::runJob starting");
            if (this.api != null) {
                //
                //  Log that we're running this job today
                this.api.stats_doAddStats("today", "RunJob", job.title, 1);
            }
            //
            //  Mark the job so it doesn't restart in another thread
            job.pending = false;
            job.lastRun = new Date();
            job.lastDuration = 0;
            //  remember this is running async
            yield db.doUpdate(`/job/${job.id}`, job);
            let commandLine = "node";
            let path = this.basePath + job.folder;
            if (/npm/.test(job.script)) {
                //  handle npm scripts
                //  TODO: use path instead of undefined, cannot
                //  resolve exception: "Error: spawn npm ENOENT" right now
                console.log("INSTALLING:", job.script);
                const result = yield NpmModuleTools_1.NpmModuleTools.verifyInstalled(job.npm, job.folder);
                return result;
            }
            else {
                args = shell_quote_1.parse(job.args);
                for (let n = 0; n < args.length; n++) {
                    args[n] = args[n].replace(/^'([^']+)'$/, "$1");
                }
                if (/\.sh/.test(job.script)) {
                    //  handle shell scripts
                    commandLine = "/bin/sh";
                    args.splice(0, 0, job.script);
                }
                else if (/\.pl/.test(job.script)) {
                    //  handle perl scripts
                    commandLine = "perl";
                    args.splice(0, 0, job.script);
                }
                else if (/\.js/.test(job.script)) {
                    commandLine = "node";
                    args.splice(0, 0, job.script);
                    args.splice(0, 0, "--max-old-space-size=8192");
                    args.splice(0, 0, "--nouse-idle-notification");
                }
                else {
                    // everything else is a node command
                    args.splice(0, 0, job.script);
                    args.splice(0, 0, "/usr/local/bin/coffee");
                    args.splice(0, 0, "--max-old-space-size=8192");
                    args.splice(0, 0, "--nouse-idle-notification");
                }
                if (this.statusUpdateClient != null) {
                    //
                    //  Tell the realtime server we are starting the job
                    jobStatus = this.statusUpdateClient.startJob(job.title, args, slot_id);
                }
                this.log.info("Starting job", {
                    job: job,
                    slot: slot_id,
                    path: this.basePath + job.folder,
                    args: args
                });
                try {
                    let fullOutput = "";
                    let jobFilenameRaw = os.hostname() + "_" + moment().format("hh-mm-ss") + "_" + slot_id + "_" + job.title;
                    jobFilenameRaw = jobFilenameRaw.replace(/[^a-zA-Z0-9]+/g, "_");
                    jobFilenameRaw = jobFilenameRaw.replace(/__+/g, "_");
                    jobFilenameRaw = jobFilenameRaw.replace(/_+$/, "");
                    fullOutput = edgecommonconfig_1.config.getDataPath(`logs/job_output_${moment().format("YYYY-MM-DD")}/${jobFilenameRaw}.txt`);
                    let jobOutput = fs.createWriteStream(fullOutput);
                    totalBytes = 0;
                    jobOutput.write("Path: " + path + "\n");
                    jobOutput.write("Command : " + commandLine + " " + "\n\n");
                    let startTime = new Date().getTime();
                    //  Spawn the job
                    const envCopy = {
                        TERM: "HTML",
                        HOME: this.homePath,
                        LANG: "en_US.UTF-8",
                        LOGNAME: "www",
                        PATH: "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin",
                        SHELL: "/bin/bash",
                        SHLVL: "1",
                        TMPDIR: "/tmp/",
                        USER: "www",
                        TOOL: "job"
                    };
                    stat = null;
                    try {
                        stat = fs.lstatSync(path);
                    }
                    catch (error) {
                        edgecommonconfig_1.config.reportError('Invalid path 1:', error);
                        edgecommonconfig_1.config.status("Invalid path 1:" + error.toString());
                        stat = null;
                    }
                    if (stat === null || !stat.isDirectory()) {
                        //
                        //  Invalid folder skip the job
                        fullOutput = "Invalid folder: " + path;
                        job.pending = false;
                        job.lastRun = new Date();
                        job.lastDuration = job.lastRun.getTime() - startTime;
                        yield db.doUpdate(`/job/${job.id}`, job);
                        let command;
                        command = commandLine + " " + shell_quote_1.quote(args);
                        this.insertJobLog(job, command, fullOutput, slot_id);
                        edgecommonconfig_1.config.status(`Invalid folder [${path}]`);
                        return true;
                    }
                    if (slot_id != null) {
                        //
                        // Output to the console the status of the job every 5 seconds
                        this.statusInterval = setInterval(function () {
                            let currentTime = new Date().getTime();
                            let duration = currentTime - startTime;
                            duration /= 1000;
                            let lastData = 0;
                            let lastDataTime = currentTime - job.lastDataReceived.getTime();
                            lastDataTime = Math.round(lastDataTime / 1000);
                            const customFormat = '#,###.[####]';
                            let txt = ninjadebug_1.default.pad(totalBytes, 10, chalk_1.default.white, customFormat) + " bytes | ";
                            txt += ninjadebug_1.default.pad(Math.floor(duration), 6, chalk_1.default.cyan, customFormat) + " sec | ";
                            if (lastDataTime > 20) {
                                txt += ninjadebug_1.default.pad(lastDataTime, 6, chalk_1.default.cyan, customFormat) + " sec ago | ";
                            }
                            else {
                                txt += ninjadebug_1.default.pad(lastDataTime, 6, chalk_1.default.cyan, customFormat) + " sec ago | ";
                            }
                            if (lastDataTime > 60 * 20) {
                                console.log("** JOB TIMEOUT **");
                                jobExec.kill('SIGHUP');
                            }
                            if (job.timeout != null) {
                                let timeoutRemaining = job.timeout - lastDataTime;
                                if (timeoutRemaining < 100) {
                                    //
                                    //  Kill job
                                    console.log("** JOB TIMEOUT **");
                                    jobExec.kill('SIGHUP');
                                }
                            }
                            if (job.args) {
                                return console.log(`${slot_id} : ` + txt + " " + job.script + " " + job.args);
                            }
                            else {
                                return console.log(`${slot_id} : ` + txt + " " + job.script);
                            }
                        }, 5000);
                    }
                    jobExec = null;
                    // args.push "--trace"
                    jobExec = child_process_1.spawn(commandLine, args, {
                        cwd: path,
                        env: envCopy
                    });
                    if (!job.timeout) {
                        job.timeout = 1800000;
                    }
                    this.timer = null;
                    timeout = function () {
                        return job.lastDataReceived = new Date();
                    };
                    // console.log "-* timeout() on ", job.id
                    // ## -1 is used to never timeout
                    // if job.timeout != -1
                    //     clearTimeout(@timer)
                    //     close = () ->
                    //         console.log "-* timeout() CLOSE CLOSE CLOSE on ", job.id
                    //         config.status "TIMEOUT on Job ", job.id
                    //         jobExec.kill 'SIGINT'
                    //         resolve(false)
                    //     @timer = setTimeout close, job.timeout
                    /* tslint:disable:no-empty */
                    clearTimer = function () {
                    };
                    // if job.timeout != -1
                    // clearTimeout(@timer)
                    //
                    //  Mark the job as running
                    job.pending = false;
                    job.lastRun = new Date();
                    job.lastDataReceived = new Date();
                    job.lastDuration = -1;
                    // this.db.doUpdate(`/job/${job.id}`, job);
                    return new Promise((resolve, reject) => {
                        jobExec.on("error", (err) => {
                            clearTimer();
                            console.log(`-> JobError in command=${commandLine} args=`, args);
                            console.log("-> Path  : ", path);
                            console.log("-> Error : ", err);
                            resolve(false);
                        });
                        jobExec.stdout.on("data", (data) => {
                            if (data != null) {
                                timeout();
                                totalBytes += data.length;
                                jobOutput.write(data);
                                if (jobStatus != null) {
                                    jobStatus.data.numproc = totalBytes;
                                }
                                if (edgecommonconfig_1.config.traceEnabled) {
                                    console.log("[[" + data.toString() + "]]");
                                }
                            }
                            return true;
                        });
                        jobExec.stderr.on("data", (data) => {
                            if (data != null) {
                                timeout();
                                fullOutput += "\n";
                                fullOutput += data.toString();
                                jobOutput.write(data);
                                totalBytes += data.length;
                                if (edgecommonconfig_1.config.traceEnabled) {
                                    console.log("*[" + data.toString() + "]*");
                                }
                            }
                            return true;
                        });
                        jobExec.on("close", (code) => {
                            clearTimer();
                            jobOutput.close();
                            job.pending = false;
                            job.lastRun = new Date();
                            job.lastDuration = job.lastRun.getTime() - startTime;
                            if (this.statusInterval != null) {
                                clearInterval(this.statusInterval);
                                this.statusInterval = null;
                            }
                            if (jobStatus != null) {
                                jobStatus.stop();
                            }
                            return db.doUpdate(`/job/${job.id}`, job)
                                .then(() => {
                                let command;
                                command = commandLine + " " + shell_quote_1.quote(args);
                                this.insertJobLog(job, command, fullOutput, slot_id);
                                resolve(true);
                            });
                        });
                    });
                }
                catch (error) {
                    console.log("Spawn Issue:", error);
                    edgecommonconfig_1.config.reportException("Spawn issue:", error);
                    return {
                        error: "Exception in job",
                        e: error
                    };
                }
            }
        });
    }
}
exports.EdgeLauncher = EdgeLauncher;
exports.default = EdgeLauncher;
//# sourceMappingURL=EdgeLauncher.js.map