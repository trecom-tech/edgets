import config           from 'edgecommonconfig';
import EdgeMessageQueue from 'edgecommonmessagequeue';

export class JobQueueSubmit {
    priorityType: string;
    messageQueueItemUpdates: any = null;
    internalConnectionPromise: any;

    //  priorityType default is null or 'normal'
    //  priorityType options: 'High', 'Low', 'high', 'low'
    constructor(priorityType: string) {
        this.priorityType = priorityType;
    }

    //  Submit data to the Edge Server to be saved to the item
    doSubmitAppend(dataSet: any, path: string, newJson: any) {

        const str = JSON.stringify({
            type: 'append',
            ds: dataSet,
            path,
            data: newJson
        });

        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doSubmitInsert not connected');
        }

        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }

    //  Submit data to the Edge Server to be saved to the item
    doSubmitInsert(dataSet: any, path: string, data: any) {

        const str = JSON.stringify({
            type: 'insertdata',
            ds: dataSet,
            path,
            data
        });

        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doSubmitInsert not connected');
        }

        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }

    //  Submit data to the Edge Server to be saved to the item
    doDeletePath(dataSet: any, path: string) {

        const str = JSON.stringify({
            type: 'deletepath',
            ds: dataSet,
            path
        });

        if (this.messageQueueItemUpdates == null) {
            throw new Error('JobQueueSubmit doDeletePath not connected');
        }

        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }

    //  Submit data to the Edge Server to be saved to the item
    async doSubmitData(dataSet: any, path: string, data: any) {

        const str = JSON.stringify({
            type: 'savedata',
            ds: dataSet,
            path,
            data
        });

        if (this.messageQueueItemUpdates == null) {
            config.status('JobQueueSubmit Warning, not already connected.');
            await this.doInternalConnectItemUpdateMessageQueue();
            config.status(`JobQueueSubmit Connected not, submitting ${path}`);
            return this.messageQueueItemUpdates.doSubmitBuffer(str);
        }

        return this.messageQueueItemUpdates.doSubmitBuffer(str);
    }

    doInternalConnectItemUpdateMessageQueue() {

        if (this.internalConnectionPromise != null) {
            return this.internalConnectionPromise;
        }

        return this.internalConnectionPromise = new Promise((resolve, reject) => {

            config.status('JobQueueSubmit doInternalConnectItemUpdateMessageQueue Connecting');

            let p = null;
            if ((this.priorityType === 'High') || (this.priorityType === 'high')) {
                p = EdgeMessageQueue.doOpenMessageQueue(config.mqItemUpdatesHigh);
            } else if ((this.priorityType === 'Low') || (this.priorityType === 'low')) {
                p = EdgeMessageQueue.doOpenMessageQueue(config.mqItemUpdatesLow);
            } else {
                p = EdgeMessageQueue.doOpenMessageQueue(config.mqItemUpdates);
            }

            p.then(mq => {
                this.messageQueueItemUpdates = mq;
                config.status('JobQueueSubmit doInternalConnectItemUpdateMessageQueue Connected');
                resolve(this.messageQueueItemUpdates);
            }).catch((err: Error) => {
                reject(err);
            })
        });
    }
}

export default JobQueueSubmit;