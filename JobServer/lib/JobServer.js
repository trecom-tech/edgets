"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const numeral = require("numeral");
const chalk_1 = require("chalk");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgeapi_1 = require("edgeapi");
const edgecommonlauncher_1 = require("edgecommonlauncher");
const edgecommonstatusupdateclient_1 = require("edgecommonstatusupdateclient");
const wqName = 'queue_jobs';
class JobServer {
    constructor() {
        this.dbLogs = new edgedatasetmanager_1.default('logs');
        this.log = edgecommonconfig_1.default.getLogger('JobRunner');
        const pathParts = __dirname.split('/');
        pathParts.pop();
        this.basePath = pathParts.join('/') + '/';
        this.homePath = pathParts.join('/');
        this.refreshing = false;
        this.jobs = {};
    }
    refreshStatus() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.jobDelay = 0;
                this.serverDetail = {
                    host: os.hostname(),
                    total_mem: Math.ceil(os.totalmem()),
                    free_mem: Math.ceil(os.freemem()),
                    load_Avg: os.loadavg()
                };
                if (this.serverDetail.load_Avg[0] > 2) {
                    this.jobDelay = 3000;
                }
                else if (this.serverDetail.load_Avg[0] > 4) {
                    this.jobDelay = 30000;
                }
                else if (this.serverDetail.load_Avg[0] > 6) {
                    this.jobDelay = 300000;
                }
                else if (this.serverDetail.load_Avg[0] > 8) {
                    this.jobDelay = 3000000;
                }
                let str = `[Host = ${chalk_1.default.gray(this.serverDetail.host)}] `;
                str += `[Total Mem = ${numeral(this.serverDetail.total_mem / 1024000).format('#,###')} MB] `;
                str += `[Free Mem = ${chalk_1.default.green(numeral(this.serverDetail.free_mem / 1024000).format('#,###'))} MB] `;
                str += `[Load = ${chalk_1.default.magenta(numeral(this.serverDetail.load_Avg[0]).format('#.##'))} `;
                str += numeral(this.serverDetail.load_Avg[1]).format('#.##') + ' ';
                str += numeral(this.serverDetail.load_Avg[2]).format('#.##') + '] ';
                str += `[Delay = ${numeral(this.jobDelay).format()}] `;
                yield JobServer.getPendingCount()
                    .then((count) => {
                    return console.log(`JobServer ${str}{${count}}`);
                });
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
    static getPendingCount() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const total = yield edgeapi_1.default.cacheGetSetLength(wqName);
                edgecommonconfig_1.default.status('Currently pending in list:', total);
                return total;
            }
            catch (e) {
                return null;
            }
        });
    }
    //  A really simple delay as a promise
    promiseDelayed(timeout) {
        if (timeout == null) {
            timeout = 1000;
        }
        return new Promise((resolve) => {
            return setTimeout(resolve, timeout, `${timeout} delayed`);
        });
    }
    //  Queue job to job db
    queueJob(job) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = new edgedatasetmanager_1.default('os');
                yield db.doUpdate(`/job/${job.id}`, job);
                return true;
            }
            catch (e) {
                return null;
            }
        });
    }
    //  Load a list of jobs and save them into an array.
    getJobsList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = new edgedatasetmanager_1.default('os');
                let jobs_list = yield db.doGetItems('/job/', null, null, 0, 5000);
                //  Random order for the jobs list to go through
                jobs_list = jobs_list.sort(() => {
                    return Math.random() - Math.random();
                });
                for (let job of jobs_list) {
                    this.jobs[job.id] = job;
                }
                return jobs_list;
            }
            catch (e) {
                return [];
            }
        });
    }
    refreshJobList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                //
                //  TODO CHanged @db from MongoGateway to DataSetManager
                //  need to change doFind to doGetItems
                const jobs_list = yield this.getJobsList();
                if (this.refreshing) {
                    yield this.promiseDelayed();
                    return true;
                }
                this.refreshing = true;
                this.log.info('Refreshing job list');
                edgecommonconfig_1.default.status('Refreshing Job List');
                for (let job of jobs_list) {
                    // if (job.npm == null) {
                    //     continue;
                    // }
                    if (JobServer.shouldJobRun(job) !== false) {
                        edgeapi_1.default.cacheSetAdd(wqName, job.id);
                    }
                }
                let total = yield JobServer.getPendingCount();
                console.log('total====>', total);
                this.log.info('Running jobs', { total });
                if (total < 5) {
                    yield this.promiseDelayed(1000 * 60);
                }
                if (!this.api) {
                    this.api = yield edgeapi_1.default.doGetApi();
                }
                this.refreshing = false;
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
    //  Returns true if a job should run
    static shouldJobRun(job) {
        try {
            if (!job.enabled) {
                return false;
            }
            //  Never run before so run it now
            if (job.lastRun == null) {
                return 9999999;
            }
            //  See how old the job is
            const now = new Date().getTime();
            if (job.freqType === 'hourly') {
                let diff = now - new Date(job.lastRun).getTime();
                diff /= 1000;
                diff /= 60;
                diff /= 60;
                // console.log(`Job #{job.id} lastRun=${job.lastRun} Diff=`, diff, ' vs ', job.freqRate)
                if (diff > job.freqRate) {
                    return (diff - job.freqRate);
                }
            }
            if (job.pending != null && job.pending) {
                return new Date(job.lastRun).getTime();
            }
            return true;
        }
        catch (e) {
            return false;
        }
    }
    getNextJob() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (this.busyGetNextJob != null) {
                    yield this.promiseDelayed(1000);
                    return this.getNextJob();
                }
                this.busyGetNextJob = true;
                if (this.jobs == null) {
                    yield this.getJobsList();
                }
                const record = yield edgeapi_1.default.cacheSetPop(wqName);
                if (record == null) {
                    edgecommonconfig_1.default.status('getNextJob, no record returned');
                    yield this.refreshJobList();
                    this.busyGetNextJob = null;
                    return this.getNextJob();
                }
                let job;
                job = this.jobs[record];
                if (job == null) {
                    edgecommonconfig_1.default.status(`getNextJob, job ${record} missing`);
                    yield this.getJobsList();
                    job = this.jobs[record];
                }
                if (job == null) {
                    edgecommonconfig_1.default.status(`getNextJob, job ${record} missing - final`);
                    this.busyGetNextJob = null;
                    return null;
                }
                if (!job.enabled) {
                    edgecommonconfig_1.default.status(`Job ${record} has been disabled`);
                    this.busyGetNextJob = null;
                    yield this.promiseDelayed(1000);
                    return this.getNextJob();
                }
                edgecommonconfig_1.default.status(`getNextJob return ${record}:`, this.jobs[record]);
                this.busyGetNextJob = null;
                this.jobs[record].lastRun = new Date();
                return this.jobs[record];
            }
            catch (e) {
                return null;
            }
        });
    }
    //  Make sure the NPM is installed
    static doVerifyInstalled(job) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let npmList;
                if (job == null || job.npm == null) {
                    return false;
                }
                if (npmList == null) {
                    npmList = {};
                }
                const last_run = npmList[job.npm] || 0;
                let diff_time = new Date().getTime() - last_run;
                diff_time /= 60000;
                // console.log(`Job ID: ${job.id} time=`, diff_time)
                if (diff_time < 60) {
                    return true;
                }
                //  Install or update the NPM
                edgecommonconfig_1.default.status(`Installing ${job.npm} in ${job.folder}`);
                yield edgecommonlauncher_1.NpmModuleTools.verifyInstalled(job.npm, job.folder);
                npmList[job.npm] = new Date().getTime();
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
    executeJobs(slot_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(`Starting job loop, slot=${slot_id}`);
                let jobLoop;
                const runner = new edgecommonlauncher_1.EdgeLauncher();
                // return (jobLoop = async () => {
                //     config.status('Calling getNextJob');
                // });
                edgecommonconfig_1.default.status('Calling getNextJob');
                jobLoop = yield this.getNextJob()
                    .then((job) => {
                    edgecommonconfig_1.default.status('executeJobs Got job to run', job);
                    // console.log('Job=', job);
                    if (job != null) {
                        // runner.statusUpdateClient = this.statusUpdateClient;
                        // runner.api = this.api;
                        return runner.runJob(job, slot_id)
                            .then((result) => {
                            edgecommonconfig_1.default.status('executeJobs Result:', result);
                            return setTimeout(jobLoop, this.jobDelay);
                        });
                    }
                    else {
                        edgecommonconfig_1.default.status('No jobs');
                        return setTimeout(jobLoop, this.jobDelay);
                    }
                });
                return jobLoop;
            }
            catch (e) {
                return null;
            }
        });
    }
    startSystemStatusLoop() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                //  Overall system status loop
                yield this.refreshStatus();
                const statusInterval = setInterval(() => __awaiter(this, void 0, void 0, function* () {
                    return this.refreshStatus();
                }), 5000);
                let statusUpdateConnected = false;
                this.statusUpdateClient = new edgecommonstatusupdateclient_1.default();
                return yield this.statusUpdateClient.doOpenChangeMessageQueue();
            }
            catch (e) {
                return null;
            }
        });
    }
}
exports.JobServer = JobServer;
exports.default = JobServer;
//# sourceMappingURL=JobServer.js.map