$ ->

    testLocations =
        washington_monument:
            lat : 38.889484
            lon : -77.035278
        statue_of_liberty:
            lat: 40.689247
            lon: -74.044502
        house1:
            lat: 35.588249
            lon: -80.8881666
        starbucks1:
            lat: 35.586227
            lon: -80.875916
        park:
            lat: 35.528495
            lon: -80.871598

    ##|
    ##|  Show a filled layer in red on the map from GeoJSON Data
    addTestButton "Load GeoJSON Record", "Start", ()->

        $.get "js/test_data/zip28115.json", (allData)->
            console.log "Loaded:", allData
            addHolder().setView "MapBox", (view)->
                view.setViewLocation testLocations.house1.lat, testLocations.house1.lon, 16

                loc = view.initGeoJson()
                loc.geoJsonAdd allData

                view.addFilledLayer "zipcodes", loc, "#ff0000"
                view.fit(loc)

    ##|
    ##|  Simple colored map showing the Washington Monument at zoom 16
    addTestButton "Simple MapBox", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            console.log "Map is showing, zoom level 16 on washington Monument"

    ##|
    ##|  Enable drawing mode on the map.  Check the console for
    ##|  polygon output.
    addTestButton "Simple MapBox - Drawing Mode", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            view.setDrawingMode true
            console.log "Map is showing, zoom level 16 on washington Monument as drawing mode"

            view.on "update", (data)=>
                console.log "Got Gobal Event with Map Draw: ", data

    ##|
    ##|  Enable drawing mode and then turn it off again every 10 seconds.
    addTestButton "Simple MapBox - Drawing Mode every 10 seconds", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            console.log "Map is showing, zoom level 16 on washington Monument as drawing mode"

            onesec = false
            window.setInterval ()->
                onesec = not onesec
                view.setDrawingMode onesec
                true
            , 10000

    ##|
    ##|  Enable building level detail from MapBox
    addTestButton "Simple MapBox - Buildings Layer", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            view.addBuildingsLayer()
            console.log "Map is showing, zoom level 16 on washington Monument"

    ##|
    ##|  Switch to Satellite mode
    addTestButton "Simple MapBox - Satellite", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            view.setStyle("satellite")

            setTimeout ()=>
                view.setPitch 45
            , 5000

            setTimeout ()=>
                view.setStyle "dark"
            , 10000

    ##|
    ##|  Enable search by location
    addTestButton "Simple MapBox - Search", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setSearchControlEnabled()
            view.setNavigationControl()
            view.on "moved", (bounds)=>
                console.log "Moved to:", bounds

    ##
    ##  Enable context menu
    addTestButton "Simple MapBox - Context Menu", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.house1.lat, testLocations.house1.lon
            view.on "contextmenu", (bounds)=>
                console.log "contextmenu: ", bounds
            view.on "zoom", (bounds)=>
                console.log "zoomed: ", bounds                

    ##|
    ##|  Display a map in a popup
    addTestButton "Simple MapBox Popup", "Open", ()->

        doPopupView "MapBox", "Map showing the Statue of Liberty", "testpopup", 600, 600, (view) ->
            true

    ##|
    ##|  GeoJSON layer with a single house point
    addTestButton "Map a house address", "Open", ()->
        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.house1.lat, testLocations.house1.lon
            view.addCustomSymbol "house1", "/images/IconHousePurple.png"
            .then ()->

                loc = view.initGeoJson()
                loc.addPoint testLocations.house1.lat, testLocations.house1.lon,
                    title: "Test Title"
                    description: "This is a test description"
                    icon: "house1"

                view.addSymbolLayer "points", loc

    ##|
    ##|  Map multiple locations on a single layer
    addTestButton "Map multiple locations", "Open", ()->
        addHolder().setView "MapBox", (view)->
            # view.setViewLocation testLocations.house1.lat, testLocations.house1.lon, 15
            view.setViewLocation 32.958984, -5.353521, 12

            all = []
            all.push view.addCustomSymbol "house1", "/images/IconHousePurple.png"
            all.push view.addCustomSymbol "house2", "/images/IconHousePink.png"
            all.push view.addCustomSymbol "house3", "/images/IconHouseDarkBlue.png"

            Promise.all(all)
            .then ()->

                loc = view.initGeoJson()
                loc.addPoint testLocations.house1.lat, testLocations.house1.lon,
                    title: "House"
                    icon: "house1"

                loc.addPoint testLocations.starbucks1.lat, testLocations.starbucks1.lon,
                    title: "Starbucks"
                    icon: "house2"

                loc.addPoint testLocations.park.lat, testLocations.park.lon,
                    title: "Demo Park"
                    icon: "park-15"

                view.addSymbolLayer "points", loc
                view.fit(loc)

                line = view.initGeoJson()
                line.addLine [ testLocations.house1, testLocations.starbucks1 ],
                    color: "#903090"

                view.addLinesLayer "testingline", line


                setTimeout ()=>
                    view.removeLayer "testingline"
                , 3000


    ##|
    ##|  Show simple tooltip on the symbol layer.
    addTestButton "Map Tooltips on locations", "Open", ()->
        addHolder().setView "MapBox", (view)->
            view.setViewLocation 32.958984, -5.353521, 12

            all = []
            all.push view.addCustomSymbol "house1", "/images/IconHousePurple.png"
            all.push view.addCustomSymbol "house2", "/images/IconHousePink.png"
            all.push view.addCustomSymbol "house3", "/images/IconHouseDarkBlue.png"

            Promise.all(all)
            .then ()->

                loc = view.initGeoJson()
                loc.addPoint testLocations.house1.lat, testLocations.house1.lon,
                    title: "House"
                    icon: "house1"
                    description: "<h2>House1</h2> <p>This is description for house 1</p>"

                loc.addPoint testLocations.starbucks1.lat, testLocations.starbucks1.lon,
                    title: "Starbucks"
                    icon: "house2"
                    description: "<h2 style='color: red'>House2</h2> <p>This is description for house 2</p>"

                loc.addPoint testLocations.park.lat, testLocations.park.lon,
                    title: "Demo Park"
                    icon: "park-15"
                    description: "<h2>Demo Park</h2> <p>This is description for <b>Demo Park</b></p>"

                view.addSymbolLayer "points", loc, {}, true
                view.fit(loc)

    ##|
    ##|   Adding labels layer example
    ##|
    addTestButton "MapBox with Label", "Open", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation testLocations.washington_monument.lat, testLocations.washington_monument.lon, 16
            labels = [
                {'lat':38.889484,'lon':-77.035278,'label':'345','color':'blue'},
                {'lat':38.879484,'lon':-77.025278,'label':'90','color':'red'},
                {'lat':38.899484,'lon':-77.015278,'label':'7632','color':'blue'},
                {'lat':38.899484,'lon':-77.005278,'label':'73632','color':'red'},
                {'lat':38.879484,'lon':-77.005278,'label':'432','color':'blue'},
            ]
            view.addLabels labels
            console.log "Map is showing with Labels, on washington Monument"

    addTestButton "Map Serialize/Deserialize", "Open", ()->
        addHolder().setView "MapBox", (view) ->
            view.deserialize
                center: testLocations.house1
                zoomlevel: 12
                drawingmode: true
                style: "satellite"
                enablenavigationcontrol: true
                symbols: [
                    title: "house1"
                    image: "/images/IconHousePurple.png"
                ,
                    title: "house2"
                    image: "/images/IconHousePink.png"
                ,
                    title: "house3"
                    image: "/images/IconHouseDarkBlue.png"
                ]
                layers: [
                    name: "points"
                    type: "symbol"
                    # fit:  true
                    showtooltip: true
                    points: [
                        lat: testLocations.house1.lat
                        lon: testLocations.house1.lon
                        options:
                            title: "House"
                            icon: "house1"
                            description: "<h2>House1</h2> <p>This is description for house 1</p>"
                    ,
                        lat: testLocations.starbucks1.lat
                        lon: testLocations.starbucks1.lon
                        options:
                            title: "Starbucks"
                            icon: "house2"
                            description: "<h2 style='color: red'>House2</h2> <p>This is description for house 2</p>"
                    ,
                        lat: testLocations.park.lat
                        lon: testLocations.park.lon
                        options:
                            title: "Demo Park"
                            icon: "park-15"
                            description: "<h2>Demo Park</h2> <p>This is description for <b>Demo Park</b></p>"
                    ]
                ,
                    name: "lines"
                    type: "line"
                    lines: [
                        start: testLocations.house1
                        end: testLocations.starbucks1
                        options:
                            color: "#903090"
                    ]
                ]

            setTimeout () ->
                serializedOpts = view.serialize()
                console.log "Mapbox serialized: ", serializedOpts
                doPopupView "MapBox", "Map Serielized", null, 1400, 900, (viewPopup) ->
                    viewPopup.deserialize serializedOpts
            , 5000

    go()
