import ClusterWorker from '../src/ClusterWorker';
import config        from 'edgecommonconfig';

const delay = require('util').promisify(setTimeout);

config.setCredentials("redisHost", process.env.REDIS_HOST);
config.setCredentials("redisHostWQ", process.env.REDIS_HOST);
config.setCredentials("redisReadHost", process.env.REDIS_HOST);
config.setCredentials("ApiServers", [process.env.API_SERVER]);


class ServiceWorker extends ClusterWorker {

    constructor(...args: any[]) {
        super();
    }

    onTestCallback(data: any) {

        if (Math.random() < 0.1) {
            throw new Error('This is an exception');
        }

        if (Math.random() < 0.5) {
            console.log('onTestCallback Data=', data);
            if (Math.random() < 0.5) {
                return `Value ${data}`;
            }
            return true;
        }

        return new Promise((resolve, reject) => {
            console.log('onTestCallback Promise Data=', data);
            const amount = Math.random() * 3000;
            return setTimeout(resolve, amount, amount);
        });
    }

    async onInit() {
        //
        //  Add a bunch of messages for testing
        console.log('Adding test messages');
        for (let n = 0; n < 200; n++) {
            await this.api.workqueue_doAddRecord('test-queue', n);
        }

        console.log('Test messages added');

        await this.processWorkQueue('ClusterWorkerTest', 'test-queue', 4, this.onTestCallback);

        console.log('Test complete');
        process.exit(0);
        return true;
    }
}

describe("cluster worker test", () => {
    it('should test ClusterWorker class', async () => {
        const worker = new ServiceWorker();
        await delay(5000);
    });
});
