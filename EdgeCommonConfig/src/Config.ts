import * as fs              from 'fs';
import * as os              from 'os';
import * as winston         from 'winston';
import * as path            from 'path';
import * as yargs           from 'yargs';
import { encrypt, decrypt } from './Encrypter';
import ninja                from 'ninjadebug';

// we can not use import syntax for these libraries because they are not not typescript ready
const chalk                  = require('chalk');
const logzioWinstonTransport = require('winston-logzio');
const exreport               = require('edgecommonexceptionreport');
const humanize               = require('humanize-duration');
const argv                   = yargs.argv;

class EdgeTimer {
    name: string;
    lastTime: any = process.hrtime();
    totalTime: number;
    lastStatus: string;
    timerNumber: number;
    traceLogFile: any;
    traceEnabled: boolean;

    constructor(name: string, traceEnabled: boolean, timersCount: number, traceLogFile: any) {
        this.traceEnabled = traceEnabled;
        this.name         = name;
        this.lastStatus   = `Starting ${name}`;
        this.totalTime    = 0;
        this.timerNumber  = timersCount;
        this.traceLogFile = traceLogFile;
    }

    log(status?: string) {

        if (!this.traceEnabled) {
            return 0;
        }

        if (status == null) {
            status = 'Undefined status';
        }

        const segment           = process.hrtime(this.lastTime);
        const segmentns: number = (segment[0] * 1e9) + segment[1];
        const segmentms: number = segmentns / 1e6;

        this.totalTime = this.totalTime + segmentms;

        this.traceLogFile.info({
            timer   : this.name,
            segmentms,
            totalms : this.totalTime,
            timer_id: this.timerNumber,
            lastCall: this.lastStatus,
            thisCall: status
        });

        this.lastStatus = status;
        this.lastTime   = process.hrtime();
        return segmentms;
    };

    status(...statusAll: any[]) {

        if (!this.traceEnabled) {
            return false;
        }

        let status;
        if ((statusAll != null) && (statusAll.length === 1) && (typeof statusAll[0] === 'string')) {
            status = statusAll[0];
        } else {
            status = '';
            for (let obj of statusAll) {

                if (typeof obj === 'string') {
                    status = status + obj;
                } else {
                    status = status + ninja.dumpVar(obj, '');
                }
            }
        }

        const segmentms: number = this.log(status);
        let str                 = '';

        if (segmentms > 5000) {
            str = `[${ninja.pad(segmentms / 1000, 13, chalk.red)} sec] `;
        } else if (segmentms > 1000) {
            str = `[${ninja.pad(segmentms / 1000, 13, chalk.yellow)} sec] `;
        } else {
            str = `[${ninja.pad(segmentms, 14)} ms] `;
        }

        return console.log(`${str} ${this.name} | ${status}`);
    };
}

class DelayedWinstonLog {
    name: string;
    config: any;
    logger: any;

    constructor(name: string, config: any) {
        this.name   = name;
        this.config = config;
    }

    //
    // Setup winston logs using Logz.io
    // https://github.com/logzio/winston-logzio
    internalGetLoggerLogz() {

        const logzio = this.config.getCredentials('logzio');
        if (logzio == null) {
            return false;
        }

        let consoleLevel = 'log';
        if (this.config.traceEnabled) {
            consoleLevel = 'error';
        }

        const transportList = {
            transports : [
                new winston.transports.Console({
                    level      : consoleLevel,
                    colorize   : true,
                    prettyPrint: true,
                    depth      : 4,
                    timestamp  : true,
                    showLevel  : false
                })
                ,
                new logzioWinstonTransport({
                    token: logzio,
                    level: 'error',
                    name : this.name
                })
            ],
            exitOnError: false
        };

        this.logger = new winston.Logger(transportList);
        return true;
    }

    //
    // Setup winston loggin and transports for local files
    //
    internalGetLoggerFiles() {

        const host: string = os.hostname();
        const infoLogFile  = this.config.getDataPath(`logs/${this.name}-${host}-info.log`);
        const errorLogFile = this.config.getDataPath(`logs/${this.name}-${host}-error.log`);

        let consoleLevel = 'log';
        if (this.config.traceEnabled) {
            consoleLevel = 'error';
        }

        try {

            const transportList = {
                transports : [
                    new winston.transports.Console({
                        level      : consoleLevel,
                        colorize   : true,
                        prettyPrint: true,
                        depth      : 4,
                        timestamp  : true,
                        showLevel  : false
                    }),
                    new winston.transports.File({
                        name         : 'info',
                        level        : 'info',
                        filename     : infoLogFile,
                        json         : true,
                        timestamp    : true,
                        maxsize      : 1024 * 1024 * 40,
                        maxFiles     : 10,
                        depth        : 4,
                        tailable     : true,
                        zippedArchive: true,
                        showLevel    : false
                    }),
                    new winston.transports.File({
                        name         : 'error',
                        level        : 'error',
                        filename     : errorLogFile,
                        json         : true,
                        timestamp    : true,
                        maxsize      : 1024 * 1024 * 40,
                        maxFiles     : 10,
                        depth        : 4,
                        tailable     : true,
                        zippedArchive: true,
                        showLevel    : false
                    })
                ],
                exitOnError: false
            };

            this.logger = new winston.Logger(transportList);

        } catch (e) {

            // Fail silently and log error
            console.log('Logger error:', e);
        }

        return true;
    }

    realSetupLogger() {
        //
        // Setup logging
        if (this.logger != null) {
            return true;
        }

        const logzio = this.config.getCredentials('logzio');

        if (logzio != null) {
            this.internalGetLoggerLogz();
        } else {
            this.internalGetLoggerFiles();
        }

        return this.logger != null;
    }

    info(...args: any[]) {

        try {

            if (this.realSetupLogger()) {
                return this.logger.info(args);
            }

        } catch (ignoreException) {
            // fail silently
        }
    }

    //
    // Ignore writing to the error log

    log(...args: any[]) {

        try {

            if (this.realSetupLogger()) {
                return this.logger.log(args);
            }

        } catch (ignoreException) {
            // fail silently
        }
    }

    //
    // Ignore writing to the error log

    error(...args: any[]) {

        try {

            console.log(`Error in log [${this.name}]:`, args);

            if (this.realSetupLogger()) {
                this.logger.error(args);
            }

        } catch (ignoreException) {

            //
            // Ignore writing to the error log
            console.log('IGNORED MESSAGE:', ignoreException);
        }

        return true;
    }
}

class EdgeAppConfig {
    devMode: boolean;
    traceEnabled: boolean;
    recordIncomingMessages: boolean;
    debugAggregateCalls: boolean;
    useNinjaDebug: boolean;
    useExceptionReport: boolean;
    WebserverPort: number;
    WebserverSSLPort: number;
    mqExchangePathUpdates: string;
    mqExchangeStatusUpdates: string;
    mqItemUpdates: string;
    mqItemUpdatesHigh: string;
    mqItemUpdatesLow: string;
    mqItemChanges: string;
    mqRetsRawData: string;
    ConfigPath: string[];
    CredentialPath: string[];
    logPath: string;
    imagePath: string;
    importPath: string;
    PrivateToken: string;
    ProjectId: number;
    FilePathInGit: string;
    argv: any;
    appRunningName: string;
    traceLogFile: any;
    timersRunning: number;
    timersCount: number;
    mainTimer: any;
    startTime: Date;
    appTitle: string;
    logger: any;
    _credentials: any;
    _logs: any;

    // index signature for loading credentials
    [k: string]: any;

    static initClass() {

        //
        // TODO:  These things should be easier to change, load from environment
        // or otherwise work from the command line the way trace does.
        //

        this.prototype.devMode = (process.env.DEVMODE === 'true') || false;

        this.prototype.traceEnabled           = false;
        this.prototype.recordIncomingMessages = false;
        this.prototype.debugAggregateCalls    = false;
        this.prototype.useNinjaDebug          = true;
        this.prototype.useExceptionReport     = true;

        this.prototype.WebserverPort    = 8001;
        this.prototype.WebserverSSLPort = 8443;

        this.prototype.mqExchangePathUpdates   = 'all-updates';
        this.prototype.mqExchangeStatusUpdates = 'status-updates';
        this.prototype.mqItemUpdates           = 'item-updates';
        this.prototype.mqItemUpdatesHigh       = 'item-updates-high';
        this.prototype.mqItemUpdatesLow        = 'item-updates-low';
        this.prototype.mqItemChanges           = 'item-changes';
        this.prototype.mqRetsRawData           = 'rets-raw';

        this.prototype.ConfigPath     = [
            './',
            `${process.env.HOME}/EdgeConfig/`,
            `/usr/local/etc/EdgeConfig/`,
            __dirname,
            `${__dirname}/node_modules/edgecommonconfig/EdgeConfig/`,
            `${__dirname}/../EdgeConfig/`
        ];
        this.prototype.CredentialPath = ['/tmp/', process.env.tmp];
        this.prototype.logPath        = `${process.env.HOME}/EdgeData/logs/`;
        this.prototype.imagePath      = `${process.env.HOME}/EdgeData/images/`;
        this.prototype.importPath     = `${process.env.HOME}/EdgeData/import/`;

        this.prototype.PrivateToken  = '8V8nPmPjvvPTF7C9BJki';
        this.prototype.ProjectId     = 19;
        this.prototype.FilePathInGit = 'config_files/';
    }

    ensureExistsSync(path: string) {
        try {
            fs.mkdirSync(path);
            return true;
        } catch (e) {
            return e.code !== 'EEXIST';
        }
    }

    //
    // Return and verify a data path
    getDataPath(pathName: string) {

        let strPath = `${process.env.HOME}/EdgeData/`;
        this.ensureExistsSync(strPath);
        for (let part of pathName.split('/')) {
            if (part.indexOf('.') !== -1) {
                return strPath + part;
            }

            strPath += part + '/';
            this.ensureExistsSync(strPath);
        }

        return strPath;
    }

    //
    // Return and verify a credential path
    getCredentialPath() {
        const osType = os.type();
        if (/^window/i.test(osType)) {
            return this.CredentialPath[1];
        }
        return this.CredentialPath[0];
    }

    //
    // Given a filename and path list, resolve with the full path that exists.
    // @param filename [string] A filename to find
    // @param pathList [array/string] A list of one or more paths to find
    // @return [string] the path and filename that was found.
    //
    FindFileInPath(filename: string, pathList: string | string[], returnPath?: any) {

        if (returnPath == null) {
            returnPath = false;
        }
        if (typeof pathList === 'string') {
            pathList = [pathList];
        }
        for (let pathName of pathList) {

            try {
                if (pathName == null) {
                    continue;
                }
                if (pathName.charAt(pathName.length - 1) !== '/') {
                    pathName += '/';
                }

                const filenameTest = pathName + filename;
                // console.log 'CHECKING[#{filenameTest}]'
                const stat         = fs.statSync(filenameTest);
                if ((stat != null) && stat.size && (returnPath === false)) {
                    return filenameTest;
                }
                if ((stat != null) && stat.size && (returnPath === true)) {
                    // test for absolute path names
                    if (filenameTest.charAt(0) === '/') {
                        return path.dirname(filenameTest) + '/';
                    }

                    return (path.dirname(path.join(process.cwd(), filenameTest))) + '/';
                }
            } catch (e) {
                // fail silently
            }
        }
        // ...

        return null;
    }

    constructor() {
        this.argv = argv;

        //
        // Put --dev on the command line to use credentials_dev.json
        if (this.argv.dev != null) {
            this.devMode = true;
        } else {
            this.devMode = false;
        }

        // General command line flags
        if (this.argv.trace != null) {
            this.traceEnabled = true;
        }

        // General command line flags
        if (process.env.EDGE_DEBUG === 'trace') {
            this.traceEnabled = true;
        }

        // Currently running app
        this.appRunningName = argv['$0'];
        this.appRunningName = this.appRunningName.replace(/.*\\/, '');
        this.appRunningName = this.appRunningName.replace('.coffee', '');
        this.appRunningName = this.appRunningName.replace('coffee', '');
        this.appRunningName = this.appRunningName.trim();
        this.setTitle(this.appRunningName);

        //
        // If we are tracing the app, create a trace logfile.
        //
        if (this.traceEnabled) {

            this.log('Launching ', this.appRunningName, ' with traceEnabled: ', argv._);

            const host         = os.hostname();
            const traceLogFile = this.getDataPath(`logs/${this.appRunningName.replace('/', '_')}-${host}-trace.log`);

            this.traceLogFile = new winston.Logger({
                transports: [
                    new winston.transports.File({
                        level    : 'info',
                        filename : traceLogFile,
                        json     : true,
                        timestamp: false,
                        depth    : 4,
                        tailable : true,
                        showLevel: false
                    })
                ]
            });

            this.timersRunning = 0;
            this.timersCount   = 0;
            this.mainTimer     = this.timerStart('Main');

        } else {

            this.mainTimer = null;
        }

        this.startTime = new Date();

        (process as NodeJS.EventEmitter).on('exit', this.onExitFunction);

        // if ((require.main.__configTrackingEnabled == null)) {
        //   require.main.__configTrackingEnabled = true;
        //
        //   process.on('warning', warning => {
        //     console.warn(warning.name);
        //     console.warn(warning.message);
        //     return console.warn(warning.stack);
        //   });
        // }

        this.getCredentials();
    }

    onExitFunction(code: string) {

        if (this.traceEnabled) {
            console.log('Total duration:', humanize(new Date().getTime() - this.startTime.getTime()));
            console.log(`Application exit code=${code}`);
        }

        // this.setTitle('');
        return true;
    }

    //
    // Log an internal exception
    reportException(message: string, e: any) {

        this.internalSetupLogger();
        this.logger.error('Exception', { message, e });
        this.reportError(message, e);
        return true;
    }

    //
    // Log an exception or error that may be fatal
    reportError(message: string, e: any) {

        if (e == null) {
            e       = message;
            message = '';
        }

        exreport.reportError([message, e]);
        return false;
    }

    //
    // Change the terminal title in iTerm2 and Byobu
    setTitle(title: string) {
        if (!Boolean(process.stdout.isTTY)) {
            return;
        }

        if ((title === null) || (title === '')) {
            process.stdout.write(String.fromCharCode(27) + ']6;1;bg;*;default' + String.fromCharCode(7));
            process.stdout.write(String.fromCharCode(27) + ']0;' + os.hostname() + String.fromCharCode(7));  //  iTerm2 on Mac
            process.stdout.write(String.fromCharCode(27) + ']2;' + 'bash$' + String.fromCharCode(27) + '\\');
        } else {
            process.stdout.write(String.fromCharCode(27) + ']0;' + title + String.fromCharCode(7));
            process.stdout.write(String.fromCharCode(27) + ']2;' + title + String.fromCharCode(27) + '\\');
        }

        this.appTitle = title;
        return true;
    }

    //
    // Report a database error different than other errors
    reportDatabaseError(name: string, action: any, document: any, e: any) {

        this.logger = this.getLogger(`Database-${name}`);
        this.logger.error('Database error', {
                name,
                action,
                document,
                error: e
            }
        );

        console.info('--> Database Error');
        console.info(chalk.blue(`DataSet   :${chalk.yellow(name)}`));
        console.info(chalk.blue(`Action    :${chalk.yellow(action)}`));
        console.info(chalk.blue(`Document  :${JSON.stringify(document)}`));
        console.info(chalk.blue(`Exception :${chalk.green(e.toString())}`));
        return false;
    }

    jsonParseOrReturn(credential: string) {
        try {
            return JSON.parse(credential);
        } catch (err) {
            // If we can not parse the string, we just return it
            return credential;
        }
    }

    private _loadCredential(credentialFile: string) {
        const keyString = this.getKey();
        if (keyString == null)
        {
            // No key was found, can't continue loading anything.
            return null;
        }

        // Classic method to use ~/EdgeConfig/credentials.json
        const configFile         = this.FindFileInPath(credentialFile, this.ConfigPath);

        if (configFile == null) {
            // console.log(`Error:  Unable to find ${credentialFile} in `, this.ConfigPath);
            return null;
        }
        const jsonText = fs.readFileSync(configFile);
        const hex      = this.jsonParseOrReturn(jsonText.toString());

        try {
            const decryptedConfig:string = decrypt(hex, keyString);
            return this.jsonParseOrReturn(decryptedConfig);
        } catch (err) {
            // if we failed to parse the config file, log error and exit
            console.log('Error: failed to decrypt the configuration file ${credentialFile}', err);
            process.exit(1);
        }
    }

    getKey() {
        if (process.env.EDGE_KEY) {
            return process.env.EDGE_KEY;
        }

        const configFile = this.FindFileInPath('keyv2.txt', this.ConfigPath);
        if (configFile == null) {
            console.log('Warning: Unable to find keyv2.txt in ', this.ConfigPath);
            return null;
        }

        let key         = fs.readFileSync(configFile);
        return key.toString();
    }
    //
    // Look for the credential in following sequence:
    // - First look for the credential in environment variable and return it
    // - Load credentials from config file
    // which can be anything that needs very basic security
    // when stored locally on disk or in a git repo.
    // Note: the key.txt file should not be stored in the repo.
    //
    getCredentials(serverCode?: string) {

        //
        // Check for an environment variable
        if ((serverCode) && process.env[serverCode]) {
            return this.jsonParseOrReturn(process.env[serverCode]);
        }

        if ((serverCode) && process.env[serverCode.toLowerCase()]) {
            return this.jsonParseOrReturn(process.env[serverCode.toLowerCase()]);
        }

        if ((serverCode) && process.env[serverCode.toUpperCase()]) {
            return this.jsonParseOrReturn(process.env[serverCode.toUpperCase()]);
        }

        if (serverCode) {
            const credentialFile = `credentials_${serverCode.toLowerCase()}.json`;
            const credential = this._loadCredential(credentialFile);
            if (credential) {
                return credential;
            }
        }

        //
        // Load the credentials file if not found
        if (this._credentials == null) {

            if ((process.env.EDGE_KEY != null) && (process.env.EDGE_KEY !== '')) {

                console.log('Support for EDGE_KEY Disabled');
                return false;

            } else {
                const credentialFileAll = 'credentialsv2.json';
                this._credentials = this._loadCredential(credentialFileAll);
                if (this._credentials == null) {
                    return null;
                }
            }
        }

        if (serverCode == null) {
            for (const varName in this._credentials) {
                this[varName] = this._credentials[varName];
            }
            return;
        }

        if (this._credentials[serverCode] == null) {
            // console.error 'Warning: requested credentials to unknown site #{serverCode}'
            return null;
        }

        return this._credentials[serverCode];
    }

    //
    // Set the credentials to credentials.json if given the server and data related to server
    // So this is a shortcut that apps can use for testing.
    //
    setCredentials(serverName: string, object: any) {

        // load it so it loads the main config file
        this.getCredentials(serverName);

        // now store a new value
        this._credentials[serverName] = object;
    }


    //
    // Returns a reference to a logger object
    // given a specific name which is cached across multiple calls.
    //
    getLogger(name: string) {

        if (!this._logs) {
            this._logs = {};
        }

        if (!this._logs[name]) {
            this._logs[name] = new DelayedWinstonLog(name, this);
        }

        return this._logs[name];
    }

    //
    // Display an object for debugging purposes
    //
    dump(...args: any[]) {
        return ninja.dump(args);
    }

    //
    // Simple output to the screen
    log(...message: any[]) {
        try {
            this.internalSetupLogger();
            this.logger.log(message);
        } catch (ignoreException) {
            // Ignore writing to the error log
            console.log('IGNORED MESSAGE:', ignoreException);
        }
    }

    //
    // Open the info log and record an information message
    info(...args: any[]) {

        try {

            this.internalSetupLogger();
            this.logger.info(args);

        } catch (ignoreException) {
            // Ignore writing to the error log
            console.log('IGNORED MESSAGE:', ignoreException);
        }

        return true;
    }

    //
    // Open the error log and record an error message
    error(...args: any[]) {

        try {

            this.internalSetupLogger();
            this.logger.error(args);

        } catch (ignoreException) {
            // Ignore writing to the error log
            console.log('IGNORED MESSAGE:', ignoreException);
        }

        return true;
    }

    status(...message: any[]) {

        try {

            if (this.mainTimer != null) {
                this.mainTimer.status(...message);
            }

        } catch (e) {

            console.log('Status error:', e);
        }

        return true;
    }

    //
    // Start a timer (benchmark) and return the benchmark details
    //
    timerStart(name: string) {
        const data = new EdgeTimer(name, this.traceEnabled, this.timersCount++, this.traceLogFile);
        return data;
    }

    internalSetupLogger() {

        if (this.logger != null) {
            return;
        }

        const appName = this.appRunningName.replace('/', '_');
        this.logger   = this.getLogger(appName);

        return true;
    }
}

EdgeAppConfig.initClass();

export const config: EdgeAppConfig = new EdgeAppConfig();
export default config;

// TODO - do we relaly need this code?
// const MYKEY = Symbol.for('edgeserver.config');
// const globalSymbols = Object.getOwnPropertySymbols(global);
// const hasMyKeyAlraedy = (globalSymbols.indexOf(MYKEY) > -1);
// if (!hasMyKeyAlraedy) {
//   global[MYKEY] = config;
// }

//
// Return a reference to the app configuration
// module.exports = new EdgeAppConfig()