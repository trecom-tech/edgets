class ViewShowTableEditor extends View

    getDependencyList: ()=>
        return [ "/ace/ace.js", "/ace/ext-language_tools.js" ]

    getLayout: ()=>
        view: "DockedToolbar"
        items: [
            type: "button"
            text: "New Columns"
            icon: "fa fa-plus"
            callback: @onButtonNewColumn
        ,
            type: "button"
            text: "Add Form"
            icon: "fa fa-plus"
            callback: @onButtonAddForm
        ,
            type: "button"
            text: "Remove Form"
            icon: "fa fa-minus"
            callback: @onButtonRemoveForm            
        ]
        main:
            view: "DynamicTabs"
            name: "viewTabs"
            tabs: [
                view: "Table"
                name: "viewTable"
                title: "Table Fields"
            ,
                view: "Table"
                name: "viewTablePermissions"
                title: "Permissions"
            ,
                view: "Table"
                name: "viewTableValidation"
                title: "Validations"
            ,
                view: "TableRearrange"
                name: "viewArrangement"
                title: "Arrangement"
            ]

    onButtonNewColumn: ()=>

        m = new ModalForm
            title:        "New Field"
            content:      "Add a new field"
            position:     "top"
            ok:           "Save"
            inputs: [
                name: "name"
                label: "Name"
            ,
                name: "source"
                label: "Source"
            ]
            onsubmit: (form) =>
                source = form.source.getValue()
                name   = form.name.getValue()
                if !source or source.length is 0 
                    source = name.replace(" ", "_").replace(/[^a-zA-Z0-9]/g, "_").toLowerCase()
                data =
                    name       : name
                    source     : source
                    visible    : true
                    editable   : true
                    type       : "text"
                    "autosize" : true

                DataMap.addColumn @editedTableKey, data
                DataMap.changeColumnAttribute @editedTableKey, source, "type", "text"
                DataMap.changeColumnAttribute @editedTableKey, source, "autosize", true
                @updateData()
                m.hide()
                true

    onButtonAddForm: () =>

        m = new ModalForm
            title:        "New Form"
            content:      "Add a new form"
            position:     "top"
            ok:           "Save"
            inputs: [
                name: "formname"
                label: "Form Name"
            ]
            onsubmit: (form) =>
                formName = form.formname.getValue()
                tabName = formName.replace(/ /g, "_")
                @addFormTab formName
                m.hide()
                true

    addFormTab: (formName, formConfig) =>
        @viewTabs.doAddViewTab "CreateForm", "<i class='far fa-window'></i>  #{formName}", (viewForm) =>
            viewForm.setTableName @editedTableKey
            viewForm.setFormName formName
            if formConfig? and formConfig.validfields? and formConfig.validfields.length
                viewForm.setValidColumns formConfig.validfields
                viewForm.buildForm @editedTableKey, formName, formConfig.validfields
            true


    updateData: ()=>
        @rowsList = DataMap.getColumnsFromTable(@editedTableKey, null)
        @internalSetDataTypes()
        @internalSetDataTypesPermissions()
        @internalSetDataTypesValidations()
        true

    ##|
    ##|  Show what the create record form will look like
    updateCreateForm: ()=>
        formConfigs = DataMap.getFormInTable @editedTableKey
        if !formConfigs then return false
        newPromise () =>
            for fn, fc of formConfigs
                yield @addFormTab(fn, JSON.parse(fc))
        .then () ->
            # console.log "TableEditor forms are created."
            true

    onButtonRemoveForm: () => 
        activeTab = @viewTabs.activeTab
        if [ 'tab0', 'tab1', 'tab2', 'tab3' ].indexOf(activeTab) >= 0
            m = new ModalForm
                title:        "Remove Form"
                content:      "You cannot remove default tabs. Please select another."
                position:     "top"
                ok:           "OK"

        tab = @viewTabs.internalFindTab(@viewTabs.activeTab)
        viewForm = tab.body.childView
        if !viewForm? or !viewForm.formName? then return false

        m = new ModalForm
            title:        "Remove Form"
            content:      "Are you sure you would like to delete form '#{viewForm.formName}'? This action can not be undone."
            position:     "top"
            ok:           "Confirm"
            onSubmit: () =>
                @viewTabs.showClosestTab tab
                tab.close()
                m.hide()
                true

    ## -------------------------------------------------------------------------------------------------------------
    ## constructor to create new tableEditor
    ##
    showTableEditor: (@editedTableKey) =>

        @viewArrangement.setTable(@editedTableKey)

        dm = DataMap.getDataMap()
        DataMap.removeTable "_editor"
        DataMap.removeTable "_editorperm"
        DataMap.removeTable "_editorvalidate"

        DataMap.getTypes(@editedTableKey).verifyOrderIsUnique()

        @rowsList = DataMap.getColumnsFromTable(@editedTableKey, null)
        @internalSetDataTypes()
        @internalSetDataTypesPermissions()
        @internalSetDataTypesValidations()

        @validatetable = @viewTableValidation.addTable "_editorvalidate"
        @validatetable.showConfigTable = false
        @validatetable.showFilters = false
        @validatetable.setAutoFillWidth()
        @validatetable.addSortRule("order", 1)
        @validatetable.moveTitleColumn "order"
        @validatetable.moveTitleColumn "name"
        @validatetable.moveTitleColumn "source"

        @permtable = @viewTablePermissions.addTable "_editorperm"
        @permtable.showConfigTable = false
        @permtable.showFilters = false
        @permtable.setAutoFillWidth()
        @permtable.addSortRule("order", 1)

        @editorTable = @viewTable.addTable "_editor"
        @editorTable.showConfigTable = false
        @editorTable.showFilters = true
        @editorTable.setAutoFillWidth()

        @editorTable.addActionColumn
            width  : 80
            source : "options",
            name   : "Options"
            render : (data) =>
                return "Edit"

            callback: (row) =>
                # console.log "Options:", row
                options = row.options
                # if !options then return false

                if typeof options is "string" and options.includes(",")
                    options = options.split ","

                if Array.isArray(options)
                    opts =
                        title: "Edit options"
                        content: ""
                        inputs: [
                            type    : "tags"
                            label   : "Value"
                            name    : "input1"
                            options : options
                            value   : options
                        ]
                        buttons: [
                            type: "submit"
                            text: "Save"
                        ]
                        onSubmit: (form)=>
                            # @saveValue form.input1.value
                            console.log "Saving options: ", form.input1.value
                            DataMap.changeColumnAttribute @editedTableKey, row.source, "options", form.input1.value
                            m.hide()
                            false

                    m = new ModalForm opts

                else if typeof options is "object" and Object.keys(options).length
                    optionsTableName = "#{@editedTableKey}_#{row.source}"
                    doPopupTableView options, optionsTableName, "", 800, 600
                    .then (view) =>
                        # console.log "#{@editedTableKey}_#{row.source}"
                        DataMap.makeColumnsEditable optionsTableName

                else
                    opts =
                        title: "Edit options"
                        content: ""
                        inputs: [
                            type    : "text"
                            label   : "Value"
                            name    : "input1"
                            value   : options
                        ]
                        buttons: [
                            type: "submit"
                            text: "Save"
                        ]
                        onSubmit: (form)=>
                            console.log "Saving options: ", form.input1.value
                            DataMap.changeColumnAttribute @editedTableKey, row.source, "options", form.input1.value
                            m.hide()
                            false

                    m = new ModalForm opts

                true

        @editorTable.addActionColumn
            width  : 80
            source : "delete",
            name   : "Delete"
            callback: (row)=>
                # console.log "Delete on:", row
                DataMap.changeColumnAttribute @editedTableKey, row.source, "deleted", true
                true

        @editorTable.addSortRule("order", 1)

        ##|
        ##|  Save callback from the data map
        DataMap.setSaveCallback "_editor", (id, field, oldValue, newValue)=>
            # console.log @editedTableKey, "CHANGE id=#{id} field=#{field} newValue=#{newValue}"
            DataMap.changeColumnAttribute @editedTableKey, id, field, newValue

            ## -gao
            ## additional actions to set default values
            if field is "type"
                ## set width to DataFormatter's default value
                col       = DataMap.getTypes(@editedTableKey).getColumn(id)
                formatter = col.getFormatter()
                if formatter? and formatter.width? and formatter.width > 0
                    DataMap.changeColumnAttribute @editedTableKey, id, "width", formatter.width
                ## new type isn't "text" then set "autosize" to false
                if newValue isnt "text"
                    DataMap.changeColumnAttribute @editedTableKey, id, "autosize", false
                ## new type is "text" then set "autosize" to true, but only currently it's "not set"
                ## default value of "autosize" is true
                else if col.getAutoSize() is true
                    # console.log "Set Autosize to true", @editedTableKey, id
                    DataMap.changeColumnAttribute @editedTableKey, id, "autosize", true

            true

        DataMap.setSaveCallback "_editorperm", (id, field, oldValue, newValue)=>
            # console.log @editedTableKey, "CHANGE id=#{id} field=#{field} newValue=#{newValue}"
            DataMap.changeColumnAttribute @editedTableKey, id, field, newValue
            true

        DataMap.setSaveCallback "_editorvalidate", (id, field, oldValue, newValue)=>
            # console.log @editedTableKey, "CHANGE id=#{id} field=#{field} newValue=#{newValue}"
            DataMap.changeColumnAttribute @editedTableKey, id, field, newValue
            true

        ##|
        ##|  Custom change event
        globalTableEvents.on "table_change", (tableName, source, field, newValue)=>
            # console.log "Editor Custom #{tableName}/#{source}"
            if tableName == @editedTableKey
                path = "/_editor/#{source}/#{field}"
                # console.log "Global....", path, newValue
                # @editorTable.updateRowData()
                @updateData()
                @updateCreateForm()

        @updateCreateForm()


    ## -------------------------------------------------------------------------------------------------------------
    ## function to get the created table instance
    ##
    ## @return [TableView] editorTable
    ##
    getTableInstance: ->
        return @editorTable

    ## -------------------------------------------------------------------------------------------------------------
    ## clears the html of the table used to remove the table
    ##
    clear: ->
        @tableHolder.html ""

    internalSetDataTypesValidations: ()->

        DataMap.removeTable("_editorvalidate")

        DataMap.setDataTypes "_editorvalidate", [
                name:     ""
                source:   "order"
                visible:  true
                type:     "text"
                width:    36
                editable: false
                autosize: false
                render: (val)->
                    return "#{val}"
            ,
                name     : "Name"
                source   : "name"
                visible  : true
                type     : "text"
                editable : false
                required : true
                width    : 140
                autosize : false
            ,
                name     : "Source"
                source   : "source"
                visible  : true
                type     : "text"
                editable : false
                width    : 120
                autosize : false
            ,
                name        : "Calculation"
                source      : "render"
                visible     : true
                type        : "sourcecode"
                editable    : true
                width       : 120
                autosize    : true
                calculation : false
                render      : (val)=>
                    if val? then return "Edit source"
                    return ""
            # ,
            #     name     : "Validate on create"
            #     source   : "validateoncreate"
            #     visible  : true,
            #     type     : "boolean"
            #     editable : true
            #     width    : 130
            # ,
            #     name     : "Validate on edit"
            #     source   : "validateonedit"
            #     visible  : true,
            #     type     : "boolean"
            #     editable : true
            #     width    : 130
            # ,
            #     name     : "Error message"
            #     source   : "validate_err_message"
            #     visible  : true,
            #     type     : "text"
            #     editable : true
            #     width    : 300
            # ,
            #     name     : "Warning message"
            #     source   : "validate_warn_message"
            #     visible  : true,
            #     type     : "text"
            #     editable : true
            #     width    : 300
            ,
                name     : "Validation type"
                source   : "validation_type"
                visible  : true
                type     : "enum"
                default  : "None"
                options  : ["None", "Not Empty", "RegEx", "Code"]
                editable : true
                width    : 100
            ,
                name     : "Validation Details"
                source   : "validation_details"
                visible  : true
                type     : "sourcecode"
                editable : true
                autosize : true
            ,
                name     : "Message"
                source   : "validate_err_message"
                visible  : true
                type     : "text"
                editable : true
                autosize : true
        ]

        for row in @rowsList
            DataMap.addData "_editorvalidate", row.getSource(), row.serialize()


    internalSetDataTypesPermissions: ()->

        DataMap.removeTable("_editorperm")

        DataMap.setDataTypes "_editorperm", [
                name:     ""
                source:   "order"
                visible:  true
                type:     "text"
                width:    36
                editable: false
                autosize: false
                render: (val)->
                    return "#{val}"
            ,
                name     : "Name"
                source   : "name"
                visible  : true,
                type     : "text"
                editable : false
                required : true
                width    : 140
                autosize : false
            ,
                name     : "Source"
                source   : "source"
                visible  : true,
                type     : "text"
                editable : false
                width    : 120
                autosize : false
            ,
                name     : "Flags to view"
                source   : "flagsview"
                visible  : true,
                type     : "tags"
                options  : ""
                editable : true
                width    : 200
            ,
                name     : "Flags to edit"
                source   : "flagsedit"
                visible  : true,
                type     : "tags"
                options  : ""
                editable : true
                width    : 200
            ,
                name     : "Visible on new record"
                source   : "visiblenewform"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 130
            ,
                name     : "Visible on edit record"
                source   : "visibleeditform"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 130
        ]

        for row in @rowsList
            DataMap.addData "_editorperm", row.getSource(), row.serialize()

    ## -------------------------------------------------------------------------------------------------------------
    ## internal function to sets the data type in the the datamap for the editor table
    ##
    internalSetDataTypes: () ->

        DataMap.removeTable("_editor")

        ##| These data type will be same for all the table editor
        DataMap.setDataTypes "_editor", [
                name     : ""
                source   : "order"
                visible  : true
                type     : "text"
                width    : 36
                editable : false
                autosize : false
                order    : 0
                render   : (val)->
                    #return "<i class='dragHandle fa fa-list-ul '></i> #{val}"
                    return "#{val}"
            ,
                name     : "Name"
                source   : "name"
                visible  : true,
                type     : "text"
                editable : true
                required : true
                width    : 140
                autosize : true
                order    : 1
            ,
                name     : "Source"
                source   : "source"
                visible  : true,
                type     : "text"
                editable : true
                width    : 120
                autosize : true
                order    : 2
            ,
                name     : "Visible"
                source   : "visible"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 70
                order    : 3
            ,
                name     : "Ignored"
                source   : "hideable"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 70
                order    : 4
            ,
                name     : "Editable"
                source   : "editable"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 70
                order    : 5
            ,
                name     : "Required"
                source   : "required"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 70
                order    : 6
            ,
                name     : "PII"
                source   : "pii"
                visible  : true
                type     : "boolean"
                default  : false
                editable : true
                width    : 70
                order    : 7
            ,
                name     : "Blind"
                source   : "blind"
                visible  : true
                type     : "boolean"
                default  : false
                editable : true
                width    : 70
                order    : 8
            ,
                name     : "Autosize"
                source   : "autosize"
                visible  : true,
                type     : "boolean"
                editable : true
                width    : 70
                order    : 9
            ,
                name     : "Type"
                source   : "type"
                visible  : true,
                type     : "enum"
                editable : true
                required : true
                width    : 70
                element  : "select"
                options  : Object.keys globalDataFormatter.formats
                order    : 10
            # ,
            #     name     : "Calculation"
            #     source   : "calculation"
            #     visible  : true,
            #     type     : "boolean"
            #     editable : true
            #     width    : 70
            #     order    : 8
            ,
                name     : "Align"
                source   : "align"
                visible  : true,
                type     : "enum"
                editable : true
                required : false
                width    : 70
                element  : "select"
                options  : [ '', 'left', 'right', 'center' ]
                order    : 11
            ,
                name     : "Width"
                source   : "width"
                visible  : true
                type     : "int"
                editable : true
                width    : 60
                order    : 12
            # ,
            #     name     : "Options"
            #     source   : "options"
            #     visible  : true
            #     type     : "text"
            #     editable : true
            #     width    : 120
            #     autosize : true
            #     order    : 13
            ,
                name     : "Text Color"
                source   : "cellColor"
                visible  : true
                type     : "sourcecode"
                editable : true
                width    : 60
                order    : 13
                render   : (val)=>
                    if val? then return "Edit source"
                    return ""
            # ,
            #     name        : "Formula/Code"
            #     source      : "render"
            #     visible     : true
            #     type        : "sourcecode"
            #     editable    : true
            #     width       : 120
            #     autosize    : true
            #     calculation : false
            #     order       : 14
            #     render      : (val)=>
            #         if val? then return "Edit source"
            #         return ""
        ]

        ##| add row data to dataMap about current column configurations
        for row in @rowsList
            DataMap.addData "_editor", row.getSource(), row.serialize()

        true

    serialize: () =>
        return
            table: @editedTableKey

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewShowTableEditor: invalid data passed"

            if options.table?
                @showTableEditor options.table

            resolve true
