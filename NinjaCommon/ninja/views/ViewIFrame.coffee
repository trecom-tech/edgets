class ViewIFrame extends View

	constructor : () ->
		super()
		@optionData = {
			id: "iframe_" + GlobalValueManager.NextGlobalID()
		}

	setData: (options) =>
		if options?
			$.extend @optionData, options, true

		@widgetIframe = @add "iframe", @optionData.classes || '', @optionData.id
		@iframe = @widgetIframe.el			

	setSource: (srcData) =>
		if !srcData then return false
		@optionData.src = srcData
		@iframe.attr 'src', srcData
		true

	onResize: (w, h) =>
		if !@iframe then return false
		@iframe.css "width", w
		@iframe.css "height", h
		@widgetIframe.setSize w, h

	serialize: () =>
		return @optionData

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? or typeof options isnt "object"
				reject "ViewIFrame: invalid data passed"

			if options.src 
				@setSource options.src 
			resolve true
