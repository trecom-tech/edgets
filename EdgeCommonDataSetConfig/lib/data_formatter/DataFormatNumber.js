"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for number data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const common = new DataFormatCommon_1.DataFormatCommon();
class DataFormatNumber extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'number';
        this.input_width = 90;
        this.align = 'right';
    }
    //
    // funtion to open editor as flatpickr
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if ((this.options != null) && (this.options['min'] != null) && (this.options['max'] != null)) {
            let jRangeValue = currentValue;
            const pathStr = path.split('/').join('-');
            // @ts-ignore
            doPopupView("Docked", "Number Range Selector", `popup-jrange-${pathStr}`, 400, 150, view => {
                view.setDockSize(40);
                return view.getFirst().setView("NavBar", (viewNavbar) => {
                    // @ts-ignore
                    const navButtonCancel = new NavButton("<i class='fa fa-window-close'></i> Cancel", "flat-toolbar-btn");
                    navButtonCancel.onClick = (e) => {
                        return view.popup.close();
                    };
                    // @ts-ignore
                    const navButtonSave = new NavButton("<i class='fa fa-save'></i> Save", "flat-toolbar-btn");
                    navButtonSave.onClick = (e) => {
                        this.saveValue(jRangeValue);
                        return view.popup.close();
                    };
                    viewNavbar.addToolbar([navButtonCancel, navButtonSave]);
                    view.getBody().css("padding", "30px 25px");
                    const elInput = view.getBody().add("input", "input-jrange", `input-jrange-${path}`, {
                        type: "hidden",
                        value: currentValue
                    });
                    elInput.getTag().jRange({
                        from: this.options['min'],
                        to: this.options['max'],
                        step: this.options['step'],
                        scale: this.options['scale'],
                        format: '%s',
                        width: 350,
                        snap: true,
                        onstatechange: (val) => {
                            // console.log val
                            return jRangeValue = val;
                        }
                    });
                    return elInput.getTag().jRange("setValue", currentValue);
                });
            });
        }
        else {
            if (!this.elEditor) {
                this.elEditor = $("<input />", {
                    type: "text",
                    class: "dynamic_edit form-control"
                });
                this.appendEditor();
            }
            this.elEditor.css({
                position: "absolute",
                "z-index": 5001,
                top,
                left,
                width,
                height
            });
            this.elEditor.val(currentValue);
            this.elEditor.show();
            this.elEditor.focus();
            this.elEditor.select();
            // @ts-ignore
            globalKeyboardEvents.once("global_mouse_down", this.onGlobalMouseDown);
        }
        return true;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null)) {
            return "";
        }
        const num = common.getNumber(data);
        if ((data === null) || ((typeof data === "string") && (data.length === 0))) {
            return "";
        }
        if (isNaN(num)) {
            return `[${num}]`;
        }
        // if !options? or (typeof options is "string" and options.length == 0)
        const optionObj = {};
        if ((options != null) && (typeof options === "string") && (options.length > 0)) {
            const iterable = options.split(';');
            for (let index = 0; index < iterable.length; index++) {
                let key;
                let val;
                const option = iterable[index];
                if (option.indexOf('=') < 0) {
                    key = 'format';
                    const value = option;
                }
                else {
                    key = option.split('=')[0];
                    val = option.split('=')[1];
                }
                optionObj[key] = val;
            }
        }
        this.options = Object.assign({
            format: "#,###.[##]",
            step: 1
        }, optionObj);
        try {
            return numeral(num).format(this.options['format']);
        }
        catch (e) {
            console.log(`Exception formatting number [${num}] using [${options}]`);
            return `[${num}]`;
        }
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return common.getNumber(data);
    }
    attachEditor(elSourceInput, path) {
        //
        // set type of this input as "number"
        return elSourceInput.css("text-align", "right");
    }
}
exports.DataFormatNumber = DataFormatNumber;
exports.default = DataFormatNumber;
//# sourceMappingURL=DataFormatNumber.js.map