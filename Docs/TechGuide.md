# directory structure of edge modules

```
├── lib                    # compiled files (important to push d.ts file)
├── src                    # source files
├── test                   # test files
│   ├── mocha.opts         # mocha test configuration
│   └── test.ts            # test file (can add mutliple files if required)
├── doc                    # Documentation files (if we need more detailed documentation)
├── README.md              # Documentation file
├── package.json           # dependency
├── tsconfig.json          # tsconfig for development
├── tsconfig.release.json  # tsconfig for release
├── Dockerfile             # Dockerfile for generating docker image
├── docker-compose.yml     # Yaml configuration for running test with just one command
├── tslint.json            # tslint configuration
└── update.sh              # shell script for compiling ts to js and pushing to repo
```


# tsconfig

This is the current tsconfig rule we have (open for suggestions)

```
{
  "compilerOptions": {
    "outDir": "lib/",
    "sourceMap": true,
    "module": "commonjs",
    "moduleResolution": "node",
    "noEmitOnError": true,
    "target": "es6",
    "declaration": true,
    "removeComments": false,
    "noImplicitAny": true,
    "forceConsistentCasingInFileNames": true,
    "pretty": true,
    "strict": true
  },
  "include": [
    "src**/*",
    "test**/*"
  ]
}
```


# tslint

This is the current tslint rule we have and should be improved to have more strict rules (open for suggestions)

```
{
  "extends": [
    "tslint-config-standard",
    "tslint-config-prettier"
  ],
  "rules": {
    "variable-name": [true, "ban-keywords", "allow-leading-underscore"],
    "strict-type-predicates": false
  }
}
```

# Node/Npm version
Always use latest LTS version
Currently node10 is the latest TLS version

# Testing
Use mocha/chai for testing framework and use nyc for generating test coverage.

Ideally all modules should have atleast 80% test coverage and we will consider the test fail if the coverage is below 80%.

For any third party modules, we should mock as much as possible using sinon.

If we can not mock the third party services (like database, caches etc), we should use *docker* to run the service.

Make sure all codes are passing tslint rules.

*Avoid using should, instead please use chai. Right now, we are using should in almost all modules and we should change assertion library to chai

# Coding Style

*Here's some important coding style guides for EdgeTS*


- Tabs

Use 4 spaces for Tab


- Spacing

A single space follows commas, colons, semicolons, operators and parenthesis.

```
import { redis } from 'redis';
const sum = a + b;
for(let i = 0; i < 10; i += 1) {
    // ...
}
...
```


- Quotes

Use single-quotes (') for all strings, and use double-quotes (") for strings within strings.

Use template literals(`) only when using expression interplation ${}


- Promises

Alway use async/await wherever possible.



- Alignments

Use vertical alignments for all variable definitions

```
const a                = 10;
const mySampleVariable = 'test';
```


- Use of var, let, and const

Use const everywhere possible (we can use const for almost all cases)

Use let if we need to reassign.

Avoid using var


- Types

Always favor type inference over explicit type declaration except for function return types

Always define the return type of functions. This can help catch errors as the functions evolve.

Types should be used whenever necessary (no implicit any).

Arrays should be defined as type[] instead of Array<type> .

Use the any type sparingly, it is always better to define an interface.

- devDependencies

All npm dependencies for development should be defined in devDependencies.

For example, `@types/`, mocha, chai, typescript, ts-node etc should be defined in devDependencies.



References:

https://github.com/labs42io/clean-code-typescript


