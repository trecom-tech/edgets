class ScreenLogin extends Screen

    windowTitle      : "Login"
    windowSubTitle   : "Login to the Edge system"

    #
    # onSetupButtons is called the first time the screen is created and only called once
    onSetupButtons: ()=>
        $("#btnToggleLeftSide").hide()

    #
    # Called when the login screen is shown to the user.
    #
    onShowScreen : () =>
        @loginCorrectLoadData()

    setupUserEvents: ()=>

        #
        # Global events from NinjaCommon based UI controls
        #
        globalTableEvents.on "open_editor", (tableName)=>
            console.log "Opening editor:", tableName
            # doPopupView "ShowTableEditor", "Editing table: #{tableName}", null, 1400, 800
            # .then (view)=>
            #     view.showTableEditor tableName
            true

        DataMap.getDataMap().on "table_change", (tableName, config)->
            console.log "ScreenLogin on DataMap.table_change tableName=#{tableName} config=", config

            # if tableName.charAt(0) == '_' then return
            # if /Stats/.test tableName then return
            # if window["dataChangeTimer#{tableName}"]? then clearTimeout(window["dataChangeTimer#{tableName}"])

            # window["dataChangeTimer#{tableName}"] = setTimeout (tableName, config)=>

            #     console.log "DataMap.table_change tableName=#{tableName} config=", config
            #     delete window["dataChangeTimer#{tableName}"]

            #     sock.data_doUpdatePath "lookup", "/table/#{tableName}/layout", config
            #     .then (r)=>
            #         # console.log "Result of doSetupChangeTableEvent, DataMap.table_change: ", r

            # , 1000, tableName, config

            true

        globalTableEvents.on "table_change", (tableName, source, field, newValue)=>
            console.log "ScreenLogin on globalTableEvents.table_change tableName=#{tableName} config=", config
            # if tableName.charAt(0) == '_' then return
            # if /Stats/.test tableName then return

            # console.log "set custom: table=#{tableName} source=#{source} field=#{field} new=", newValue
            # settings = {}
            # settings[field] = newValue

            # if globalTableBaseCache["lookup"]? and globalTableBaseCache["lookup"]["table"]?
            #     globalTableBaseCache["lookup"]["table"].doInvalidateRecord(tableName)

            # sock.data_doUpdatePath "lookup", "/table/#{tableName}/layout/#{source}", settings
            # .then (r)=>
            #     console.log "Result of Admin ScreenLogin, table_change: /table/#{tableName}/layout/#{source} ", r

            return

    preloadRequiredData: ()=>

        if @preloadPromise? then return @preloadPromise
        @preloadPromise = new Promise (resolve, reject)=>

            sock.doConnect()
            .then ()=>

                #
                #Load available tables
                resolve(true)


    #
    # Execute a timer every 50ms to check to see if the navbar and all
    # system data such as clients list and searches have loaded
    loginCorrectLoadData: () =>

        #
        # Setup menu
        if !window.catLayerOptions?

            @parentTag = $("#sidebarContent")
            holder = new WidgetTag(@parentTag)
            holder.setView "LeftDock", (view)=>
                #
                # View is setup

        @preloadRequiredData()
        .then ()=>
            @setupUserEvents()
            $("#page-container").show()
            $(@classid).hide()

            startScreen = "MainMenu"
            if window.hashHistory? and window.hashHistory.length > 0
                startScreen = window.hashHistory.pop()

            if startScreen == "Login" then startScreen = "MainMenu"
            showScreen startScreen

