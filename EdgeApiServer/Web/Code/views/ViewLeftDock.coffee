class ViewLeftDock extends View

	setNavSize: ()=>
		if !@viewMenu? then return
		winHeight = $(window).height()
		width     = @width()
		yTop      = 60
		@viewMenu.move(0, yTop, width, winHeight-yTop)
		true

	onShowScreen: ()=>

		holder = new WidgetTag(@navHolder)
		holder.setView "NavMenu", (@viewMenu)=>
			@setNavSize()
			@viewMenu.setTitle "Available Modules"
			@groups = {}
			setTimeout ()=>
				@refreshModuleList()
			, 2000

		@move(0, 0, 230, $(window).height())

		#
		# Context menu
		@navHolder.bind "contextmenu", (e) =>
			e.originalEvent.preventDefault()
			e.originalEvent.stopPropagation()
			coords = GlobalValueManager.GetCoordsFromEvent(e.originalEvent)
			menu = new PopupMenu "Modules", coords.x, coords.y
			menu.addItem "Add new module", @onButtonNewModule

	#
	# Context menu -> Add new module
	onButtonNewModule: (data)=>

		options =
			title: "Create a new module"
			inputs: [
				type: "text"
				label: "Category"
				name: "category"
			,
				type: "text"
				label: "Module Name"
				name: "modname"
			]
			buttons: [
				type: "submit"
				text: "Create and edit"
			]
			onSubmit: (form)=>
				showScreen "CodeEdit", "#{form.category.value}_#{form.modname.value}"
				true

		m = new ModalForm(options)

	genClickFunction: (mod)=>
		return ()=>
			showScreen "CodeEdit", "#{mod.category}_#{mod.name}"

	refreshModuleList: ()=>

		sock.code_doListAvailableModules(null)
		.then (allModules)=>
			for mod in allModules
				if !@groups[mod.category]?
					@groups[mod.category] =
						holder : @viewMenu.addGroup
							title:     mod.category
							isEnabled: true
							isOpen:    true
						items: {}

				if !@groups[mod.category].items[mod.name]?
					@groups[mod.category].items[mod.name] = @groups[mod.category].holder.addItem
						title:      mod.name
						isEnabled:  true
						isHidden:   false
						isSelected: false
						icon:       "fa fa-play"

					@groups[mod.category].items[mod.name].setClickFunction @genClickFunction(mod)

			@viewMenu.show()


	onResize: (w, h)=>
		@setNavSize()

	nav: ()=>
		return @viewMenu