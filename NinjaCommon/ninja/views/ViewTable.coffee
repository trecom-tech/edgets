class ViewTable extends View

	getDependencyList: ()=>
		return [ "/vendor/tableexport/FileSaver.js", "/vendor/tableexport/cpexcel.js", "/vendor/tableexport/xlsx.full.min.js", "/vendor/selectize.js", "/vendor/selectize.css", "/vendor/summernote/summernote.min.js", "/vendor/summernote/summernote.css", "/vendor/jquery.multi-select.js", "/vendor/multi-select.css" ]

	onSetupButtons: () =>

	onShowScreen: ()=>

	##|
	##|  Keep a list of functions that need to be passed to the table

	internalPassthrough: (functionName, params...)=>

		##|
		##| if table already exists, just call it.
		if @table?
			if !@table[functionName]? then throw "Internal error: #{functionName} not in table"
			@table[functionName].apply(@table, params)
			@resetSize()
			return true

		##|
		##|  Table doesn't exist, to queue calls in order
		if !@tableDelayFunctions?
			@tableDelayFunctions = []

		@tableDelayFunctions.push { name: functionName, params: params }
		return false


	##|
	##|  Add an action column (see TableView.coffee)
	addActionColumn: (data)=>
		@internalPassthrough "addActionColumn", data
		true

	setActionColumn: (columnName, isAdd = true)=>
		@internalPassthrough "moveActionColumn", columnName, isAdd
		true

	moveActionColumn: (columnName, isAdd = true) =>
		@setActionColumn columnName, isAdd
		true

	##
	##  Add a title column
	addTitleColumn: (data)=>
		@internalPassthrough "addTitleColumn", data
		true

	setTitleColumn: (columnName, isAdd = true)=>
		@internalPassthrough "moveTitleColumn", columnName, isAdd
		true

	moveTitleColumn: (columnName, isAdd = true)=>
		@setTitleColumn columnName, isAdd
		true

	setAutoFillWidth: ()=>
		@internalPassthrough "setAutoFillWidth", true
		true

	setLeftWidth: (num)=>
		@internalPassthrough "setLeftWidth", num
		true

	##|
	##| newLimit is the optional number of checkboxes that can be enabled, default to 1
	setEnableCheckboxes: (enabled, newLimit)=>
		if !enabled? then enabled = true
		if !newLimit? or typeof newLimit is not "number"
			newLimit = 1

		@internalPassthrough "setEnableCheckboxes", enabled, newLimit
		true

	##
	## newLimit is the optional number of flagged rows that can be enabled
	## default to 0(unlimited)
	setEnableFlags: (enabled, newLimit)=>
		if !enabled? then enabled = true
		@internalPassthrough "setEnableFlags", enabled, newLimit
		return true

	##
	## Add a flag object
	addFlag: (obj) =>
		if !obj? then return false
		@internalPassthrough "addFlag", obj
		true

	setTitle: (title) =>
		@internalPassthrough "setTitle", title
		return true

	setEnableMouseover: (isEnabled)=>
		@internalPassthrough "setEnableMouseover", isEnabled
		true

	##|
	##|  Show or hide the user input filters
	setShowFilter: (showFilters)=>
		@internalPassthrough "setShowFilter", (showFilters == true)
		true

	setStatusBarEnabled: (isEnabled)=>
		@internalPassthrough "setStatusBarEnabled", isEnabled
		true

	##|
	##|  Execute a callback that receives a row record for each visible row
	forAll: (callback) =>
		@internalPassthrough "forAll", callback
		true

	##|
	##|  Execute a callback for all checked records
	forAllChecked: (isChecked = true, callback) =>
		@internalPassthrough "forAllChecked", isChecked, callback
		true

	##|
	##|  Execute a callback for all visible records
	forAllVisible: (isVisible = true, callback) =>
		@internalPassthrough "forAllVisible", isVisible, callback
		true

	##|
	##|  Execute a callback for all filtered records
	forAllFiltered: (isFiltered = true, callback) =>
		@internalPassthrough "forAllFiltered", isFiltered, callback
		true

	##|
	##|  Add grouping by a given column name
	groupBy: (columnName)=>
		@internalPassthrough "groupBy", columnName
		true

	## set html content of `No Data` message
	setNoDataMessage: (content) =>
		@internalPassthrough "setNoDataMessage", content
		true

	##
	## Toggle a group visible or invisible
	## `groupName` can be presented by either of 2 ways:
	## - Object: { columnName1: value1, columnName2: value2, ...}
	## - String: `columnName1: value1, columnName2: value2...`
	##
	toggleGroupVisible: (groupName, mode = null) =>
		@internalPassthrough "toggleGroupVisible", groupName, mode
		true

	##
	## Remove a row from table
	removeRow: (rowId) =>
		if @tableName?
			DataMap.removeItem @tableName, rowId
		true

	##
	## set filter content: filter's comparator('=' or '<' etc) and value to compare
	## possible keys: 'comparator', 'dataFormatter', 'value', 'comparatorFunc'
	setFilter: (columnName, key, value) =>
		if !@tableName?
			console.log "Warning: called to setFilter #{columnName} before table name was set"
			return false

		@internalPassthrough "updateFilter", @tableName, columnName, key, value
		true

	##
	## set row class by its content
	setRowClass: (className, callback) =>
		@internalPassthrough "setRowClass", className, callback
		true

	##
	## move focus to cell by field value
	setFocusByValue: (sourceName, value) =>
		@internalPassthrough "setFocusByValue", sourceName, value
		true

	##
	## set name of SavedSearch
	setSavedSearch: (name) =>
		@internalPassthrough "setSavedSearch", name
		true

	##
	## save current filters to local storage
	saveCurrentFilters: (saveName, objName) =>
		@internalPassthrough "saveCurrentFilters", saveName, objName

	##
	## apply saved filters in local storage
	applySavedSearches: (name, objName) =>
		@internalPassthrough "applySavedSearches", name, objName

	##
	## remove saved filters in local storage
	removeSearch: (name, objName) =>
		@internalPassthrough "removeSearch", name, objName

	##|
	##|  Lock a row to the top of the list
	addLock: (rowID)=>
		@internalPassthrough "addLock", rowID
		true

	removeLock: (rowID)=>
		@internalPassthrough "removeLock", rowID
		true

	getLocked: ()=>
		if @table? then return @table.getLocked()
		return {}

	highlightRows: (className, callback) =>
		@internalPassthrough "highlightRows", className, callback

	addComputedRow: (mathTypes, mathCols) =>
		@internalPassthrough "addComputedRow", mathTypes, mathCols

	setEnableMathRow: (enabled) =>
		@internalPassthrough "setEnableMathRow", enabled

	exportTable: (tableName, fileName, reducer) =>
		@internalPassthrough "exportTable", tableName, fileName, reducer

	resetCurrentFilters: () =>
		@internalPassthrough "resetCurrentFilters"

	resetFilters: () =>
		@internalPassthrough "resetCurrentFilters"

	editRow: (rowId) =>
		@internalPassthrough "editRow", rowId

	##
	## reset data of table
	resetView: () =>
		if !@table? then return true
		@table.resetTable()
		true

	sort: (sortData) =>
		data = sortData.split " "
		column = data[0]
		if !column? or column.length is 0
			console.log "Invalid column name for ViewTable Sorting...", sortData

		if data.length > 1 then direction = data[1] else direction = "a"

		if direction is "a" or direction is "asc"
			direction = TableView.SORT_ASC
		else if direction is "d" or direction is "desc"
			direction = TableView.SORT_DESC
		else
			direction = TableView.SORT_NONE

		@internalPassthrough "addSortRule", column, direction
		true

	##
	## function to set width and height of View
	##
	setSize: (w, h)=>
		super(w, h)

		if @table and (w * h > 0)
			@table.show()
			@table.onResize(w, h)

		true

	onResize: (w, h)=>
		super(w, h)

		if globalDebugResize
			console.log @element, "ViewTable onResize(#{w}, #{h})"

		if w == 0 or h == 0
			if @table? then @table.hide()
			return

		true

	getBadgeText: ()=>
		if !@table? then return null

		if @isDetailed
			num = @table.getTableTotalCols()
			if isNaN(num) or num == 0 then return 0
			return num - 1

		return @table.getTableTotalRows()

	##|
	##|  Make sure to call this before addTable/loadTable
	##|  Display the table as a "Detailed" view
	##|
	setDetailed: ()=>
		@isDetailed = true

	##|
	##|  simple add table function
	addTable: (@tableName, colFilterFunction, rowFilterFunction)=>

		@table = null

		if @isDetailed
			@table = new TableViewDetailed @el
		else
			@table = new TableView @el

		@table.addTable @tableName, colFilterFunction, rowFilterFunction
		@table.setFixedHeaderAndScrollable()
		@table.render()
		@table.setStatusBarEnabled()
		@table.updateRowData()

		##|
		##|  Any pending actions?
		if @tableDelayFunctions?
			for delayedFunction in @tableDelayFunctions
				if @table[delayedFunction.name]?
					@table[delayedFunction.name].apply(@table, delayedFunction.params)

		return @table

	##|
	##|   Altername call name due to legacy code
	loadTable: (@tableName, colFilterFunction, rowFilterFunction)=>
		@addTable @tableName, colFilterFunction, rowFilterFunction

	##
	## Remove/Destroy TableView of this View
	removeExtraThings: () =>
		if !@table then return true 
		@table.destroy()

	##
	## Start loading Table
	startLoading: (text, promiseFunction) =>
		DataMap.removeTableData @tableName
		if !text? or text is "" then text = "Loading data..."
		@table?.setLoadingMessage(text)
		if promiseFunction? and typeof promiseFunction is "function"
			return new Promise (resolve, reject)=>
				promiseFunction()
				.then (result) =>
					@endLoading()
					resolve(result)

		true

	##
	## End loading table
	endLoading: () =>
		@table?.unsetLoadingMessage()
		true

	##
	## Set a function to render custom Tooltips
	setCustomTooltip: (bool, callback) =>
		@internalPassthrough "setCustomTooltip", bool, callback
		true

	serialize: () =>
		tableOptions = @table.serialize()
		defaultOptions =
			detailed: @isDetailed
			collection: @tableName
		return Object.assign({}, defaultOptions, tableOptions)

	##|
	##|  Autolayout helper class -- called after the view is created, before returning to the main caller.
	##|  See ViewTestLayout7
	##|
	deserialize: (options)=>

		if options.detailed? and options.detailed == true
			@setDetailed()

		if options.collection?
			@addTable options.collection, options.colfilter, options.rowfilter

		if options.tabletitle?
			@setTitle options.tabletitle

		if options.savedsearch?
			@setSavedSearch options.savedsearch

		if options.autofill?
			@setAutoFillWidth options.autofill

		if options.showfilter?
			@setShowFilter options.showfilter

		if options.leftwidth? and options.leftwidth > 0
			@setLeftWidth options.leftwidth

		if options.enablecheckboxes? and options.enablecheckboxes == true
			@setEnableCheckboxes(true, options.checkboxlimit)

		if options.flags? 
			for flag in options.flags 
				@addFlag flag
			@setEnableFlags(true, options.flaglimit)

		if options.rowcsses?
			for css in options.rowcsses
				@setRowClass css.classname, css.callback

		if options.highlightselector? and typeof options.highlightselector is "function"
			@highlightRows options.highlightclassname, options.highlightselector

		if options.hascustomtooltip?
			@setCustomTooltip options.hascustomtooltip, options.tooltiprenderer

		## apply sort data - format: "column", "column a[sc]", "column d[esc]"
		if options.sort?
			@sort options.sort

		if options.groupby
			@groupBy options.groupby

		if options.filters?
			for colName, obj of options.filters
				for key, value of obj
					@setFilter colName, key, value

		# Move table columns to action columns
		if options.actioncolumns?
			for actioncol in options.actioncolumns
				if typeof actioncol is "string"
					@setActionColumn actioncol
				else if typeof actioncol is "object"
					@setActionColumn actioncol.source, actioncol.isadd

		## Add new action columns that will act like buttons
		if options.newactioncolumns?
			for newactioncol in options.newactioncolumns
				@addActionColumn newactioncol

		# Move table columns to title columns
		if options.titlelocks?
			for titlecol in options.titlelocks
				if typeof titlecol is "string"
					@setTitleColumn titlecol
				else if typeof titlecol is "object"
					@setTitleColumn titlecol.source, titlecol.isadd

		## Add new title columns that will act like buttons
		if options.newtitlelocks?
			for newtitlecol in options.newtitlelocks
				@addTitleColumn newtitlecol

		true
