"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql_1 = require("mysql");
var edgecommonconfig_1 = require("edgecommonconfig");
var DEFAULT_HOST = 'localhost';
var DEFAULT_PORT = 3306;
var DEFAULT_CONNECTION_LIMIT = 3;
var DEFAULT_CONNECT_TIMEOUT = 2000;
var DEFAULT_QUEUE_LIMIT = 0;
var DEFAULT_TIMEZONE = 'UTC';
var DEFAULT_DEBUG = false;
var DEFAULT_WAIT_FOR_CONNECTIONS = true;
var EdgeCommonMySQL = /** @class */ (function () {
    /**
     * Creates an instance
     * @param configName name of config as defined in the credential
     * @param options options for mysql connection
     */
    function EdgeCommonMySQL(configName) {
        this.configName = configName;
        this.creds = edgecommonconfig_1.config.getCredentials(configName);
        this.logger = edgecommonconfig_1.config.getLogger('MySQL');
        this.pool = null;
    }
    /**
     * Execute a SQL query
     * @param sql Query string
     * @param callbackOnRow callback function
     */
    EdgeCommonMySQL.prototype.doRunQuery = function (sql, callbackOnRow) {
        var _this = this;
        if (!callbackOnRow) {
            return false;
        }
        if (typeof callbackOnRow !== 'function') {
            return false;
        }
        if (!this.pool) {
            this.pool = this.createPool();
        }
        this.pool.query(sql, function (err, results, fields) {
            if (err) {
                _this.logger.error('doRunQuery', { sql: sql, err: err });
                callbackOnRow(err, results, fields);
            }
            if (results instanceof Array) {
                if (results.length === 0) {
                    callbackOnRow(err, results, fields);
                }
                else {
                    for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
                        var result = results_1[_i];
                        for (var _a = 0, fields_1 = fields; _a < fields_1.length; _a++) {
                            var field = fields_1[_a];
                            if (field.type === 1 /* TINY */) {
                                result[field.name] = result[field.name] === 1;
                            }
                        }
                        callbackOnRow(err, result, fields);
                    }
                }
            }
            else if (results.affectedRows) {
                callbackOnRow(err, results, fields);
            }
        });
    };
    /**
     * Create a connection pool
     */
    EdgeCommonMySQL.prototype.createPool = function () {
        if (!this.creds) {
            edgecommonconfig_1.config.status("EdgeCommonMySQL no credentials for " + this.configName);
            throw new Error("EdgeCommonMySQL no credentials for " + this.configName);
        }
        if (!this.creds.options) {
            this.creds.options = {};
        }
        var options = {};
        options.host = this.creds.host || DEFAULT_HOST;
        options.port = this.creds.port || DEFAULT_PORT;
        options.user = this.creds.user;
        options.password = this.creds.password;
        options.database = this.creds.database;
        options.connectionLimit = this.creds.options.connectionLimit || DEFAULT_CONNECTION_LIMIT;
        options.connectTimeout = this.creds.options.connectTimeout || DEFAULT_CONNECT_TIMEOUT;
        options.queueLimit = this.creds.options.queueLimit || DEFAULT_QUEUE_LIMIT;
        options.debug = this.creds.options.debug || DEFAULT_DEBUG;
        options.timezone = this.creds.options.timezone || DEFAULT_TIMEZONE; // acceptable values: local, Z, +HH:MM, -HH:MM
        options.waitForConnections = this.creds.options.waitForConnections || DEFAULT_WAIT_FOR_CONNECTIONS;
        if (!options.user) {
            throw new Error('EdgeCommonMySQL username must be specified');
        }
        else if (!options.password) {
            throw new Error('EdgeCommonMySQL password must be specified');
        }
        else if (!options.database) {
            throw new Error('EdgeCommonMySQL database must be specified');
        }
        options.timezone = options.timezone === 'UTC' ? '+00:00' : options.timezone;
        return mysql_1.createPool(options);
    };
    return EdgeCommonMySQL;
}());
exports.EdgeCommonMySQL = EdgeCommonMySQL;
exports.default = EdgeCommonMySQL;
//# sourceMappingURL=EdgeCommonMySQL.js.map