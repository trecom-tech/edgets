# CoffeeNinjaCommon

Common modules for use in Coffeescript related projects

Read these documents:

- [Understanding the data model](UnderstandingData.md)
- [Undrestanding the source](UnderstandingNinja.md)
- [Understanding the widget](UndrestandingWidget.md)
- [Understanding the docs](UnderstandingDocs.md)

Description
--------------------------------------------------
Each of the pages on the left are designed to demonstrate or test specific web GUI controls
they are added as a pug template in test/views, a javascript page in test/js/, and possibly data.

## Using Docker
### Pulling and Running Docker image
1. Pull docker image from docker-hub by: `docker pull xgao69/coffee-ninja:latest`
2. Run docker image by: `docker run -it -p 9000:9000 -e BRANCH=<branch-name> xgao69/coffee-ninja`
e.g. `docker run -it -p 9000:9000 -e BRANCH='30_GridStack_Bugs' xgao69/coffee-ninja`
3. Go to browser and then `localhost:9000` to see Ninja Test pages.

### Copying CoffeeNinjaCommon Files from Docker Container
1. Copy file 'dockerCopy.sh' from this repository.
2. Paste this file to where you want to copy this entire project folder.
3. Run 'sh dockerCopy.sh'. 

### Building & pushing Docker image for CoffeeNinjaCommon
1. Move to folder of CoffeeNinjaCommon Project.
2. Run 'sh dockerBuild.sh <username-of-docker-hub>'.

### Steps to Setup in Windows OS
1. Install Git (https://git-scm.com/downloads )
2. Install Node (https://nodejs.org/en/download/ )
3. Generate​ your​ public SSH key​ and Generate SSH
- Run ​ssh-keygen command.
- Press enter key whenever needed

https://drive.google.com/file/d/15VlhA8TvSf2AYrmt-aEjXQDWWbsAmRIM/view?usp=sharing

- Find SSH key from local system (Generally its located at C:\Users\YOUR_SYSTEM_USER_PROFILE_NAME\.ssh )
- Open id_rsa.pub in notepad/sublime or any text editor.

https://drive.google.com/file/d/1ZBaLaZW5rLxTN3d4GSmwRJQgfpGg4S7s/view?usp=sharing

4. Add your public SSH Key in Git profile (https://gitlab.protovate.com/profile/keys
- Copy key

https://drive.google.com/file/d/1Wn25mOWVuKmuESlfTfImxTx95aEzve5B/view?usp=sharing

5. Then ​ open File Explorer​ and location of the directory where want to clone Project.
6. Within the folder ​ right click ​ and select ​"Git Bash here"​ to open Git Terminal.
7. Write following command 
git clone git@gitlab.protovate.com:EdgeTS/NinjaCommon.git
Hit the enter key from keyboard whenever required
8. Once the above process done run another command
npm install​
9. Setup Done

### Steps to Test In Windows OS
1. Open project directory, right click in white area
2. Seect "Git bash here" option to open terminal
3. Run following command
nom start
4. After few seconds project is ready to run
5. Open browser and following URL : http://localhost:9000
