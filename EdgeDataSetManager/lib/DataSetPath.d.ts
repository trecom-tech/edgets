import { ObjectID } from 'mongodb';
export declare class DataSetPath {
    [key: string]: any;
    keyName: string;
    value: any;
    condition: any;
    subPath: any;
    collection: string;
    rawPath: any;
    canSave: boolean;
    static checkForHexRegExp: RegExp;
    static checkForNumber: RegExp;
    toString(): any;
    constructor(rawPath: string | any);
    /**
     * Set the test file to understand what path options are possible
     * @param path
     */
    parsePath(path: string): boolean;
    /**
     * Special condition when a key is in a group of values
     * /item/in:1,2,3
     * @param fieldName
     * @param subPathText
     * @param pathText
     */
    parseInGroupCondition(fieldName: string, subPathText: string, pathText: string): boolean;
    /**
     *
     * @param subPathText
     * @param pathText
     */
    parsePathSubkey(subPathText: string, pathText: string): boolean;
    /**
     * Given a condition in "DataSet" terms, return a viable
     * Mongo search condition.
     * @param basePath
     * @param oneCondition
     */
    parsePathCondition(basePath: string, oneCondition: string): any;
    /**
     * Get the value in the correct data type
     * @param value
     */
    getConditionValue(value: string): string | number | RegExp | ObjectID | {
        '$exists': boolean;
        '$gt'?: undefined;
        '$lt'?: undefined;
    } | {
        '$gt': number;
        '$lt': number;
        '$exists'?: undefined;
    } | {
        '$gt': number;
        '$exists'?: undefined;
        '$lt'?: undefined;
    };
}
export default DataSetPath;
