"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgedatasetmanager_1 = require("edgedatasetmanager");
const ApiProvider_1 = require("./ApiProvider");
class ProviderLogs extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
        return __awaiter(this, void 0, void 0, function* () {
            // Initialize the provider
            this.dmLogs = new edgedatasetmanager_1.default('logs');
            // TODO roy: dbLogs is not defined.
            // let c = this.dbLogs.collection("jobrun");
            // await c.doInternalGetCollection();
            // return c;
            return true;
        });
    }
    getBaseName() {
        return 'logs';
    }
    // The /jobrun collection stores the output from jobs, this will return
    // the last number of records from that log.
    doGetLatestJobs(limit) {
        if (limit == null || typeof limit !== 'number') {
            limit = 100;
        }
        return this.dmLogs.doGetItems('/jobrun', {}, { _id: -1 }, 0, limit);
    }
}
exports.default = ProviderLogs;
;
//# sourceMappingURL=ProviderLogs.js.map