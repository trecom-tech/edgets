import { IUser }           from './IUser';
import { EdgeApiProvider } from 'edgeapi';
import ApiProvider         from '../ApiProvider';
import * as passwordUtil   from '../utility/Password';

export abstract class IApiUser extends EdgeApiProvider{
    public myConnectionID: number;
    public myName: number;
    public ipAddress: string;
    public rec: IUser;
    public authAttempts: number;
    public timezoneOffset: number = 0;
    public system: string;

    /**
     * Run one-time initial code after the user object is created.
     */
    public async doInitUser()
    {
        console.log("Default INit User");
    }

    public addRoles(roles: string[] = [], system: string = 'global'): boolean {
        if (roles && roles.length > 0) {
            if (!this.rec.roles[system]) {
                this.rec.roles[system] = roles;
            } else {
                this.rec.roles[system] = {
                    ...this.rec.roles[system],
                    ...roles
                };
            }
        }
        return true;
    }

    public removeRoles(roles: string[] = [], system: string = 'global'): boolean {
        if (roles && roles.length > 0) {
            if (this.rec.roles[system]) {
                this.rec.roles[system] = this.rec.roles[system].filter((x: string) => !roles.includes(x));
            }
        }

        return true;
    }

    public hasRole(roleName: string, targetSystem?: string): boolean {
        if (this.rec && this.rec.roles) {
            const userSystem = this.system;
            if (targetSystem) {
                const systemRoles: string[] = this.rec.roles[targetSystem] || [];
                if (systemRoles.includes(roleName)) {
                    return true;
                }
            } else if (userSystem) {
                const systemRoles: string[] = this.rec.roles[userSystem] || [];
                if (systemRoles.includes(roleName)) {
                    return true;
                }
            }

            const globalRoles: string[] = this.rec.roles['global'] || [];
            return globalRoles.includes(roleName);
        }
        return false;
    }

    public doChangePassword(newPassword: string) {
        this.rec.password = passwordUtil.generatePasswordHash(newPassword);
        return true;
    }

    public async doAddTimeline(provider: ApiProvider, timelineData: any) {
        return provider.doCall('timeline_doAddTimeline', {
            participant_id: this.rec.id,
            eventStamp: new Date(),
            eventOffset: this.timezoneOffset,
            ...timelineData
        }, this);
    }

    public abstract addSubscription(data: any): boolean;
}

export default IApiUser;