import { config } from 'edgecommonconfig';
import EdgeApi    from '../src/EdgeApi';
import { expect } from 'chai';

config.setCredentials("redisHost", process.env.REDIS_HOST);
config.setCredentials("redisHostWQ", process.env.REDIS_HOST);
config.setCredentials("redisReadHost", process.env.REDIS_HOST);
config.setCredentials("ApiServers", [process.env.API_SERVER]);

const should = require('should');

const wait = (timeout: number) => new Promise(resolve => setTimeout(() => resolve(), timeout));

describe('Edge API', function() {
    let api: EdgeApi;

    before(async () => {
        api = await EdgeApi.doGetApi();
    });


    it('data_doUpdatePath', async () => {
        const data = {
            test: 'def',
        };

        const result = await api.data_doUpdatePath('lookup', '/table/test', data);
    });

    it('data_doTestForm', async () => {
        const result = await api.data_doTestForm({test: true});
        result.should.be.eql(true);
    });

    it('data_doRecordFile', async () => {

        const result = await api.data_doRecordFile('test.json', {write: true, test: true});
        result.should.be.eql(true);
    });

    it('Should connect and disconnnect', async () => {
        api.dynamicApiCallsList.should.have.property('code_doRunCode');
        let result = await api.code_doRunCode();
        result.should.have.property('total_ms');
    });

    it('doRegisterApiCommand', async () => {
        await api.doRegisterApiCommand({
            base: 'test',
            command: 'sendMeThis',
            params: {},
            target: function() {
                return 'test';
            }
        });

        await wait(2000);
        let result = await api.test_sendMeThis();
        
        result.should.be.eql('test');
    });

    it('data_doGetItems', async () => {
        const allData = await api.data_doGetItems('lookup', '/table');
        allData.length.should.be.eql(1);
    });

    it('Should handle basic number conversion', async () => {
        api = await EdgeApi.doGetApi();

        let testDataset = 'lookup';
        let testPath    = '/table';
        let fieldList   = { _id: 1, id: 1, _lastModified: 1 };
        let sortOrder   = {};
        let offset      = 0;
        let limit       = 1000;
        let allData     = await api.data_doGetItems(testDataset, testPath, fieldList, sortOrder, offset, limit);

        let r;
        let list = [];
        for (let rec of allData) {
            list.push({ id: rec.id });
        }

        console.log(`Starting server stream for ${list.length} records.`);

        let stream = await api.data_doGetItemsIfNeeded('lookup', 'table', list);

        console.log('Stream starting', stream);

        stream.onData = (record: any) => {
            return console.log('Stream received:', record);
        };

        await stream.doWaitUntilFinished();
    });

    it('doCall should succeed', async () => {
        const params = {
            test: true
        };

        const result = await api.test_doTestdoCall(params);
        result.should.match(/as sent request with/);
    });

    it('os_doGetServerInformation', async () => {
        let result = await api.os_doGetServerInformation();
        result.should.have.property('free_mem');
    });


    it('doQuickFind', async () => {
        const testDataset        = 'lookup';
        const testPath           = 'table';
        const allResults: any[]  = [];

        const result = await EdgeApi.doQuickFind(testDataset, testPath, {}, null, (item: any) => {
            allResults.push(item);
        });

        result.should.be.eql(true);
    });

    it('doAggregate', async () => {
        const testDataset        = 'lookup';
        const testPath           = 'table';
        const allResults: any[]  = [];
        const aggList = [{
            '$match': {
                'test': 'def'
            }
        }];

        const result: any = await EdgeApi.doAggregate(testDataset, testPath, aggList);
        console.log(result, 'sssssssss')
        result.length.should.be.greaterThanOrEqual(1);
    });
    
    it('doAggregateCount', async () => {
        const testDataset        = 'lookup';
        const testPath           = 'table';
        const allResults: any[]  = [];
        const aggList = [{
            '$match': {
                'id': 'test'
            }
        }];

        const result: any = await EdgeApi.doAggregateCount(testDataset, testPath, null, null, null, null);
        result[0].count.should.be.eql(1);
    });

    it('flattenObject', async () => {
        const complexObj = {
            parent1: {
                prarent2: {
                    child1: 'child1',
                    child2: 'child2',
                }
            },
            parent2: {
                child1: 'child1'
            }
        };
        const result = await EdgeApi.flattenObject(complexObj, '', null);
        result.should.have.property('parent1.prarent2.child1', 'child1');
    });

    it('getRedisReadonlyConnection', async () => {
        const conn = EdgeApi.getRedisReadonlyConnection();
        conn.should.have.property('spop');
    });

    it('getRedisConnection', async () => {
        const conn = EdgeApi.getRedisConnection();
        conn.should.have.property('spop');
    });

    it('getRedisConnectionWQ', async () => {
        const conn = EdgeApi.getRedisConnectionWQ();
        conn.should.have.property('spop');
    });
    

    // ROY TODO: Please skip this one because it crashed the server on purpose. 
    // Not sure how the server recovers because I see no recovering logic
    it('Crash Recovery', async () => {
        try {
            let api         = await EdgeApi.doGetApiForServer('http://localhost:8001');
            let allPromises = [];
            for (let x = 0; x <= 5; x++) {
                allPromises.push(new Promise((resolve, reject) => {
                    config.status(`Sending API for ${x}`);
                    api.test_doTestCommandCrash(x)
                        .then(function(result: any) {
                            config.status(`Result from ${x} = ${result}`);
                            return resolve(result);
                        })
                        .catch(function(e: Error) {
                            console.log(`Exception from ${x}:`, e);
                            return reject();
                        });
                }));
            }

            return Promise.all(allPromises).then(function(allResults) {
                config.status('All Results:', allResults);
                console.log(allResults);
            }).catch(ea => {
                return console.log('Catch One:', ea);
            });
        } catch (err) {
            console.error(err);
        }
    });
});