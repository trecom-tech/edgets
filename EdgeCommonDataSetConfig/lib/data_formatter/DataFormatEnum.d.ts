import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatEnum extends DataFormatterType {
    name: string;
    align: string;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options?: any, path?: any): any;
    unformat(data: any, path: any): any;
    getValueInTooltip(value: any): any;
}
export default DataFormatEnum;
