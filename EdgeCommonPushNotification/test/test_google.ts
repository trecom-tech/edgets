import EdgeCommonNotification from '../src/EdgeCommonNotification';
import {expect}               from 'chai';
import { IContactInfo }       from '../src/IContactInfo';
import './helper/setup';

const notification = new EdgeCommonNotification();

const googleValidContactDetail: IContactInfo = {
    type: 'google',
    priority: 1,
    account: {
        deviceToken: 'db57akN8B3A:APA91bHzQb6QbPF7gGbWUtnlj-zpGn5OXXOjW28W052BLAEq_WLlA2otSHOnTYoqq9qHdmaXAiYHiWkgfabscm_ThmDzP5UkRToq5XIHavs4GQn2aGTUANfUPvdpQoKCCtVqEx-DhPmt', // valid token
    }
};

const googleInvalidContactDetail: IContactInfo = {
    type: 'google',
    priority: 1,
    account: {
        deviceToken: 'db57akN8B3A:APA91bHzQb6QbPF7gGbWUtnlj-zpGn5OXXOjW28W052BLAEq_WLlA2otSHOnTYoqq9qHdmaXAiYHiWkgfabscm_ThmDzP5UkRToq5XIHavs4GQn2aGTUANfUPvdpQoKCCtVqEx-DhP'
    }
};


const message = {
    title: 'Testing Google Notification',
    alert: `\u2709 You have a new message(${new Date()})`,
};

describe('Google Push Notification', () => {
    it('doSendForGoogle() with valid token', async () => {
        const result = await notification.doSendForGoogle(googleValidContactDetail.account.deviceToken, message);
        expect(result).to.equal(true);
    });

    it('doSendForGoogle() with invalid token', async () => {
        const result = await notification.doSendForGoogle(googleInvalidContactDetail.account.deviceToken, message);
        expect(result).to.equal(false);
    });
});
