import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';

let dummyUser = {
    streamingListNumber: 999,
    send               : (cmd: string, obj: any) => {
        console.log('send cmd=', cmd, 'obj=', obj);
        return true;
    },
    // Send this back to the client and they know to expect data
    // then use sendPush to send the actual data.
    startStreamingList : () => {
        if (!dummyUser.streamingListNumber) {
            dummyUser.streamingListNumber = 999;
        }

        dummyUser.streamingListNumber++;
        console.log('startStreamingList id=', dummyUser.streamingListNumber);
        return `STREAM:${dummyUser.streamingListNumber}`;
    },

    // Terminate the streaming list by sending a special command
    // to the receiver
    endStreamingList: (stream_id: any) => {
        console.log(`endStreamingList id=${stream_id}`);
        return true;
    },

    // Generic push notification message
    sendPush: (cmd: string, obj: any) => {
        console.log(`sendPush cmd=${cmd} obj=`, obj);
        return true;
    },

    // Push message but delayed to avoid sending too many at once
    sendPushDelayed: (cmd: string, obj: any) => {
        console.log(`sendPushDelayed cmd=${cmd} obj=`, obj);
        return true;
    },
};

describe('test.spec', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();


    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
    });


    it('StatusUpdate test', async () => {

        api.sendCommand('admin_status', true);
        api.on('status-update', (data: any) => {
            if (data.type !== 'workqueue') {
                return;
            }
            return console.log('DATA=', data);
        });
    });

    it('Custom URL test', async () => {
        try {
            // let result = await api._testForm(5, 10);
            // result.should.be.eql('X5Y10');
        } catch (err) {
            console.error(err);
        }
    });


    it('Workqueue members', async () => {

        let statusLoop = () => api.workqueue_doGetWorkqueueList().then(function(result: any) {
            console.log('Work queue workqueue_doGetWorkqueueList :', result);
            // return setTimeout(statusLoop, 1000);
        });

        statusLoop();

        // tslint:disable-next-line:one-variable-per-declaration
        for (let i = 0, n = i; i <= 5; i++, n = i) {
            api.workqueue_doWaitForPending('test_wq').then((message: any) => console.log(`Message found ${n--} in test_wq:`, message));
        }

        console.log('Adding message 123 to test_wq and test_wq2');
        api.workqueue_doAddRecord('test_wq', 123);
        api.workqueue_doAddRecord('test_wq_2', 123);

        let result = [];
        for (let a = 0; a <= 5; a++) {
            let num = 1000;
            result.push(setTimeout(function() {
                api.workqueue_doAddRecord('test_wq', num++);
                return api.workqueue_doAddRecord('test_wq_2', num++);
            }, 1500 + a * 500));
        }
    });

    it('Code members', async () => {
        console.log('** Should return all code modules');
        let allModules = await api.code_doListAvailableModules(dummyUser, null);
        console.log('Result=', allModules);

        console.log('** Should return just the single module');
        allModules = await api.code_doListAvailableModules(dummyUser, 'name:Demo2');
        console.log('Result=', allModules);

        console.log('** Loading a single module in dev mode');
    });

    it('Simple client test', async () => {

        api        = await EdgeApi.doGetApi();
        let socket = require('socket.io-client')(process.env.API_SERVER);

        return new Promise((resolve: any, reject: any) => {
            socket.on('connect', function() {
                console.log('onConnect');

                resolve();
                socket.emit('config_sendflat');

                socket.emit('cmd', {
                    callName: 'os_doGetServerInformation',
                    uuid    : '1',
                });

            });

            // |
            // |  The reply to the os_doGetServerInformation comes here.
            socket.on('cmd-reply', function(data: any) {
                console.log('DATA:', data);
            });

            socket.on('event', function(data: any) {
                console.log('onEvent', data);
            });

            socket.on('disconnect', function() {
                console.log('onDisconnect');
            });
        });

    });

});




