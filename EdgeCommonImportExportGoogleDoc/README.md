# EdgeCommonImportExportGoogleSheet
> Provides a standard interface to sync data with a google doc

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonImportExportGoogleDoc.git
    import EdgeCommonImportExportGoogleDoc from 'edgecommonimportexportgoogledoc';

## Getting Google Credentials

`client_id` and `client_secret` are available at [google developer console](https://console.developers.google.com/).

1. Navigate [google developer console](https://console.developers.google.com/) and create or pick a project.
2. Choose "API & Services" and click "Credentials" in the left sidebar.
3. Click "Create credentials" and chose "OAuth client ID" in the dropdown menu.
4. Select "Other" in the "Application type" that will be shown in the screen, and click "Create" button.
5. Copy `client_id` and `client_secret` in the presented dialog.
6. Choose "Dashboard" in the left sidebar, and click "ENABLE APIS AND SERVICES".
7. In the screen - "API Library", type "Google Sheets API" in the search APIs field.
8. Click `Google Sheets API`, and then you will be navigated to the detailed page.
9. In the detailed page, click "Enable" button.

## Setup Credentials

1. Update `CLIENT_ID` and `CLIENT_SECRET` variables in [get_oauth2_permissions.js](util/get_oauth2_permissions.js).
2. Run following script in terminal:
    `node util/get_oauth2_permissions.js`
3. Visit the URL printed in your browser, authenticate the google user, grant the permission.
4. Copy the authorization code and paste it at the terminal
5. The `refresh_token` you get is needed with `client_id` and `client_secret`.
6. The entire structure needs to be setup as a credential in the local configuration file. The person in charge of the credential file would need to edit save_creds_example.js in `~/EdgeConfig/`

    ```
    GoogleDocs1: {
        oauth2: {
            client_id:   client_id,
            client_secret: client_secret,
            refresh_token: refresh_token
        }
    }
    ```
7. Run this script in terminal:
    `node save_creds_example.js`

## Creating an instance

    let doc = new EdgeCommonImportExportGoogleDoc(configName, sheetId, optionalCacheFile, optionalCacheLimit);

### Parameters

* configName: Pass in the config name as defined in the credential
* sheetId: Google Spreadsheet Id
* optionalCacheFile(optional): Cache file name
* optionalCacheLimit(optional): Cache limt time. Default is 300 seconds

### Usage

    doc.doReadSheets()
    .then((allSheets: any) => {
        const sheet = allSheets["Sheet1"];
        const cell = sheet.get(0, 0);
        console.log(cell.value());
    });

## Run tests locally

    npm install
    npm run test


