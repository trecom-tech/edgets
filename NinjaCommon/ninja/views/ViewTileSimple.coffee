class ViewTileSimple extends View

    setData: (options = {}) =>
        if options.tileWidth?
            @setTileWidth options.tileWidth
        
        if options.tileHeight?
            @setTileHeight options.tileHeight

        GlobalClassTools.addEventManager(this)

        @subTitleHeight = 20
        @tileHeight     = 150
        @titleHPercent  = 0.2
        @iconWPercent   = 0.5

        @padding         = 10
        @margin          = 0
        @firstTopPadding = 0
        @viewTitleH      = 100

    setTileWidth: (w) =>
        if !w? then return false
        if @tileWidth is w then return false
        @tileWidth = w
        @resizeAllTiles()
        true

    setTileHeight: (h) =>
        if !h? then return false 
        if @tileHeight is h then return false
        @tileHeight = h
        @resizeAllTiles()
        true

    removeAllTiles: () =>
        for tile, index in @tileList
            tile.elTitle?.destroy()
            tile.elDescription?.destroy()
            tile.elSubTxt?.destroy()
            tile.elTile?.destroy()
        @tileList = []

    resizeAllTiles: () =>
        # if @tileWidth? and @viewWidth > (2 * @tileWidth)
        #     @horizontal = true
        #     @el.css "overflow-x", "auto"
        #     @el.css "overflow-y", "hidden"
        # else 
        #     @horizontal = false
        #     @el.css "overflow-x", "hidden"
        #     @el.css "overflow-y", "auto"

        for tile, index in @tileList
            @resizeTile tile, index
        true

    addRow: () =>
        row = @mainWrapper.addDiv "ninja-tile-row", "tile-row#{@rowCount}"
        @rowCount++
        @rowList.push row
        row

    resizeTile: (tile, index) =>
        if !tile then return false
        if !@viewWidth? then return false
        ##
        ## Calculate Width of this tile
        ## @tileWidth can be an array in format of [100, 200, 300] or [ 0.3, 0.3, 0.4 ](by percent)
        ## or an Integer like 200 which means all the tiles have the same width of 200px
        totalWidth = parseInt(@viewWidth)

        if !@tileWidth?
            tileWidth = totalWidth
        else if typeof @tileWidth is "number"
            tileWidth = @tileWidth
        else if Array.isArray(@tileWidth) is true ## in this case Param Index is a must
            if !index? or index < 0 then return false
            tileWidth = @tileWidth[index] or totalWidth

        if tileWidth <= 1 ## set by percent like 0.6
            tileWidth = parseInt(totalWidth * tileWidth) - 2 ## border is considered here

        tileWidth -= 2 * @padding

        tileHeight = @tileHeight
        if tileHeight <= 1 
            tileHeight = parseInt(@viewHeight * @tileHeight)

        tileHeight -= 2 * @padding

        if tile.elTile?
            # tileWidth = @tileWidth or (@viewWidth - 2 * @leftPadding)
            tile.elTile.outerWidth(tileWidth + 2 * @padding + 2)
            tile.elTile.outerHeight(tileHeight + 2 * @padding)
        if tile.elIcon?
            tile.elIcon.outerWidth(tileWidth * @iconWPercent)
        if tile.subtxt?
            titleH       = (tileHeight - @subTitleHeight) * @titleHPercent
            descriptionH = tileHeight - @subTitleHeight - titleH - 2

            tile.elTitle.outerWidth tileWidth
            tile.elTitle.outerHeight titleH
            tile.elTitle.css "line-height", "#{titleH}px"

            tile.elDescription.outerWidth tileWidth * (1 - @iconWPercent)
            tile.elDescription.outerHeight descriptionH

            tile.elSubTxt.outerWidth tileWidth * (1 - @iconWPercent)
            tile.elSubTxt.outerHeight @subTitleHeight
        else 
            titleH       = tileHeight * @titleHPercent
            descriptionH = tileHeight - titleH
            tile.elTitle.outerWidth tileWidth
            tile.elTitle.outerHeight titleH
            tile.elTitle.css "line-height", "#{titleH}px"

            tile.elDescription.outerWidth tileWidth * (1 - @iconWPercent)
            tile.elDescription.outerHeight descriptionH

    addTile: (rowNum, name, title, icon, description, subtxt, theme) =>
        if !name? then name = @tileCount
        if !title? then title = ""
        if !description? then description = ""
        if !rowNum? then rowNum = @rowCount

        if @rowList.length is 0 then @addRow()
        if rowNum > @rowCount - 1 then @addRow()

        row = @rowList[rowNum]

        if !row? then return console.log "Invalid row number for this tile: #{rowNum}"

        tileContainer = row.addDiv "ninja-tile-container", "tile-#{@gid}-#{@tileCount}"

        tileContainer.css "margin-left", "#{@margin}px"
        tileContainer.css "margin-top", "#{@margin}px"
        tileContainer.css "padding", "#{@padding}px"

        elTitle       = tileContainer.addDiv "ninja-tile-title"
        elContent     = tileContainer.addDiv "ninja-tile-content"
        elIcon        = elContent.addDiv "ninja-tile-icon"
        elText        = elContent.addDiv "ninja-tile-text"
        elDescription = elText.addDiv "ninja-tile-description"

        tile =
            id : @tileCount++
            name: name
            title: title
            description: description
            elTile : tileContainer
            elTitle: elTitle 
            elIcon: elIcon
            elDescription: elDescription

        if icon?
            elIcon.html icon

        if subtxt?
            elSubTxt  = elText.addDiv "ninja-tile-subtxt" 
            tile.subtxt = subtxt 
            tile.elSubTxt = elSubTxt

            elTitle.html "<div class='ninja-tile-title-text'>#{title}</div>"
            elDescription.html "<div class='ninja-tile-description-text'>#{description}</div>"
            elSubTxt.html "<div class='ninja-tile-subtxt-text'>#{subtxt}</div>"
        else 
            elTitle.html "<div class='ninja-tile-title-text'>#{title}</div>"
            elDescription.html "<div class='ninja-tile-description-text'>#{description}</div>"

        if theme?
            tile.theme = theme
        
        @tiles[name] = tile 
        @tileList.push tile 

        if theme?
            @setTileBGColor name, theme

        @resizeTile tile, @tileList.length - 1
        @tiles[name]

    ##  Set the background color of the tile
    ##
    ##  @see (Theme style file)[theme.styl] for a reference to the colors available
    ##  @param color {number|string} The color either a number 0 through 9 or the text "tilecolor0" through "tilecolor9"
    setTileBGColor: (tileName, color) =>

        if !color? then color = "tilecolor0"
        if !isNaN(color) then color = "tilecolor" + color
        @setTileClass tileName, color, true
        true

    ##
    ##  Add a css class name to a tile
    ##  @param tileName {mixed} The title of the tile or the tile object, see internalFindTile
    ##  @param className {string} The css class to add or remove
    ##  @param addClass {boolean} Will add the class (true|default) or remove the class (false)
    ##
    setTileClass: (tileName, className, addClass = true)=>
        tile = @internalFindTile(tileName)
        if !tile? then return false
        tile.class = className
        tile.elTile.setClass className, addClass
        true

    setTitleClass: (tileName, className, addClass = true)=>
        tile = @internalFindTile(tileName)
        if !tile? or !tile.elTitle? then return false
        tile.elTitle.setClass className, addClass
        true

    setDescriptionClass: (tileName, className, addClass = true)=>
        tile = @internalFindTile(tileName)
        if !tile? or !tile.elDescription? then return false
        tile.elDescription.setClass className, addClass
        true

    setSubTxtClass: (tileName, className, addClass = true)=>
        tile = @internalFindTile(tileName)
        if !tile? or !tile.elSubTxt? then return false
        tile.elSubTxt.setClass className, addClass
        true

    setTileTitle: (tileName, str, callback) =>
        tile = @internalFindTile(tileName)
        if !tile? then return false
        tile.title = str
        tile.elTitle.html "<div class='ninja-tile-title-text'>#{str}</div>"
        if callback? and typeof callback is "function"
            tile.elTitle.on "click", callback
            tile.elTitle.setClass "ninja-tile-clickable", true
        else
            tile.elTitle.setClass "ninja-tile-clickable", false
        true

    setTileSubTitle: (tileName, str, callback) =>
        tile = @internalFindTile(tileName)
        if !tile? then return false
        tile.subtxt = str
        if !str? or str is ""
            tile.elSubTxt?.hide()
            @resizeAllTiles()
        else if !tile.elSubTxt?
            tile.elSubTxt = tile.elTile.addDiv "ninja-tile-subtxt"
            @resizeAllTiles()
        if tile.elSubTxt? and callback? and typeof callback is "function"
            tile.elSubTxt.on "click", callback
            tile.elSubTxt.setClass "ninja-tile-clickable", true
        else
            tile.elSubTxt?.setClass "ninja-tile-clickable", false

        tile.elSubTxt?.html "<div class='ninja-tile-subtxt-text'>#{str}</div>"
        true

    setTileDescription: (tileName, str, callback) =>
        tile = @internalFindTile(tileName)
        if !tile? or !tile.elDescription? then return false
        tile.description = str
        tile.elDescription.html "<div class='ninja-tile-description-text'>#{str}</div>"
        if callback? and typeof callback is "function"
            tile.elDescription.on "click", callback
            tile.elDescription.setClass "ninja-tile-clickable", true
        else
            tile.elDescription.setClass "ninja-tile-clickable", false
        true

    setTitle: (title) =>
        if !title then return false
        @title = title
        @wgtTitle.show()
        @wgtTitle.html title
        true

    setMargin: (@margin) =>
        for tile in @tileList
            tile.elTile?.css "margin-left", "#{@margin}px"
            tile.elTile?.css "margin-top", "#{@margin}px"
        true

    setPadding: (@padding) =>
        true

    ##
    ##  Given a tile name or tile object, return the tile
    ##  @param tileNameOrObject {string|object} Either text of a tile name, ID, or the tile object itself
    ##  @return null or a tile object
    ##
    internalFindTile: (tileNameOrObject)=>
        if !tileNameOrObject? then return null

        if typeof tileNameOrObject == "object" and tileNameOrObject.id?
            return tileNameOrObject

        if typeof tileNameOrObject == "string" and @tiles[tileNameOrObject]?
            return @tiles[tileNameOrObject]

        for tileName, tile of @tiles
            if tile.id == tileNameOrObject then return tile
            if tile.name? and tile.name == tileNameOrObject then return tile

            ##
            ##  Regex match attempt if tileNameOrObject is /something/
            if typeof tileNameOrObject == "object" and tileNameOrObject.test?
                if tileNameOrObject.test(tile.name) then return tile

        return null

    onResize: (w, h) =>
        super(w, h)
        # @mainWrapper.outerWidth w 
        # @mainWrapper.outerHeight h 
        @viewWidth  = w
        @viewHeight = if @wgtTitle.getVisible() then h - @viewTitleH else h
        # @mainWrapper.height @viewHeight
        @resizeAllTiles()
        
    onShowScreen: () =>
        @gid         = GlobalValueManager.NextGlobalID()
        @wgtTitle    = @addDiv "ninja-tile-title"
        @mainWrapper = @addDiv "ninja-tilelist-wrapper", "viewtiles-#{@gid}"
        @rowList     = []
        @rowCount    = 0
        @tiles       = {}
        @tileList    = []
        @tileCount   = 0

        ## By default, there's no title
        @wgtTitle.hide()

        true

    serialize: () =>
        tiles = @tileList.map (item) ->
            return
                name: item.name
                title: item.title
                description: item.description
                subtxt: item.subtxt
                theme: item.theme
                class: item.class
        return
            title: @title
            tiles: tiles
            tilewidth: @tileWidth
            tileheight: @tileHeight

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewTiles: invalid data passed"

            if options.title?
                @setTitle options.title

            for tile in options.tiles or []
                @addTile tile.name, tile.title, tile.description, tile.subtxt, tile.theme
                if tile.class?
                    @setTileClass (tile.name or @tileCount - 1), tile.class, tile.addclass

            if options.tilewidth?
                @setTileWidth options.tilewidth

            if options.tileheight?
                @setTileHeight options.tileheight

            resolve true
