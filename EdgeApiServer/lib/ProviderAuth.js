"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgedatasetmanager_1 = require("edgedatasetmanager");
const ApiProvider_1 = require("./ApiProvider");
const EdgeError_1 = require("./EdgeError");
const IApiUserOffline_1 = require("./interfaces/IApiUserOffline");
const passwordUtil = require("./utility/Password");
const _ = require("lodash");
const uuidv4 = require("uuid/v4");
const LRU = require("lru-cache");
const slug = require('slug');
const crypto = require('crypto');
class ProviderAuth extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
        this.globalAuthTokensCache = new LRU({ max: 500 });
    }
    onInit() {
        this.internalCollections = {};
        this.dataStore = {};
    }
    getBaseName() {
        return 'auth';
    }
    /**
     *
     * Authenticate the user and set values in the persistant user object for future use.
     * @see [Data Model:Master / User](https://brians.atlassian.net/wiki/spaces/OBVIO/pages/258342984/Data+Model+Master+User)
     *
     * @param user - The IApiUser object of a connected user
     * @param username - A username to validate
     * @param passwordHash - A hashed version of the user's password for comparison
     * @param system - The name of the system the user is trying to login to.  Used to validate the user is a member of that system and load Roles.
     * @return object - authentication information
     */
    doAuthUser(user, username, passwordHash, system) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!system) {
                system = "global";
            }
            if (!system) {
                throw new EdgeError_1.EdgeError('System is required', 'MISSING_SYSTEM', 401);
            }
            user.system = system;
            if (user.authAttempts > 6) {
                yield new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve();
                    }, 1000 * user.authAttempts);
                });
                throw new EdgeError_1.EdgeError('Too many authentication attempts', 'INVALID_ROLE', 401);
            }
            const correctPassword = passwordUtil.generatePasswordHash(passwordHash);
            /*
            TODO Phase 1:
            Load record (rec) from collection where id='username'.
            Check that rec..password == correctPassword
            Check that user has roles defined for this system.  That is rec.roles[system] is an array, non empty
            return { error: 'text' } upon any failure
    
            update rec.last_login with current system time.
            update rec.last_login_ip with current connection IP (maybe in ApiSocketUser?)
            update collection with user.doUpdateOne setting login/ip
    
            return rec object to caller
            */
            const targetUser = yield this.doInternalLoadUser(username);
            if (!targetUser.isLoaded()) {
                throw new EdgeError_1.EdgeError('Can not find user with provided username', 'INVALID_USERNAME', 401);
            }
            if (targetUser.rec.password === correctPassword) {
                if (!((Array.isArray(targetUser.rec.roles[system]) && targetUser.rec.roles[system].length > 0) ||
                    (Array.isArray(targetUser.rec.roles['global']) && targetUser.rec.roles['global'].length > 0))) {
                    this.authLogger.error({
                        userId: targetUser.rec.id,
                        ipAddress: targetUser.rec.last_login_ip,
                        username,
                        system,
                    });
                    // throw new EdgeError('Invalid role', 'INVALID_ROLE', 401);
                }
                targetUser.rec.last_login = new Date();
                delete targetUser.rec.password;
                const allGroups = yield this.doInternalGetAllGroups();
                const assignedRoles = targetUser.rec.roles;
                for (const userSystem in targetUser.rec.roles) {
                    assignedRoles[userSystem] = [];
                }
                for (const group of allGroups) {
                    if (group.members && group.members.includes(username)) {
                        this.doInternalMergeRoles(assignedRoles, group.roles);
                    }
                }
                this.doCall('data_doUpdatePath', {
                    dataSet: 'master',
                    path: `/user/${username}`,
                    newData: {
                        last_login: new Date(),
                        roles: assignedRoles,
                    },
                }, user);
                targetUser.rec.roles = assignedRoles;
                user.rec = targetUser.rec;
                yield user.doAddTimeline(this, {
                    participant_id: username,
                    category: 'Account Change',
                    subcategory: 'Login',
                    text: 'User has logged in'
                });
                user.rec.authToken = uuidv4();
                this.globalAuthTokensCache.set(user.rec.authToken, user.rec);
                return user.rec;
            }
            user.authAttempts = user.authAttempts + 1;
            throw new EdgeError_1.EdgeError('Incorrect user password', 'INVALID_PASSWORD', 401);
        });
    }
    /**
     *
     * Authenticate the user using token.
     * @see [Data Model:Master / User](https://brians.atlassian.net/wiki/spaces/OBVIO/pages/258342984/Data+Model+Master+User)
     *
     * @param user - The IApiUser object of a connected user
     * @param strToken - auth token to login
     * @return object - authentication information
     */
    doAuthFromToken(user, strToken) {
        return __awaiter(this, void 0, void 0, function* () {
            // @ts-ignore
            const cachedUser = this.globalAuthTokensCache.get(strToken);
            if (!cachedUser) {
                throw new EdgeError_1.EdgeError('Can not find user with provided token', 'INVALID_TOKEN', 401);
            }
            user.rec = cachedUser;
            return user.rec;
        });
    }
    doTestUser(user) {
        return user.rec.id;
    }
    /**
     * Change the password for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param newPasswordHash - True to indicate that the password has been changed or an error message
     */
    doChangePassword(user, username, newPasswordHash) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, username, 'adminManageUsers');
            const correctPassword = passwordUtil.generatePasswordHash(newPasswordHash);
            //
            // TODO, Verify that the doUpdateOne call worked and return the correct result.
            this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/user/${username}`,
                newData: { password: correctPassword },
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Account Change',
                subcategory: 'Password Change',
                text: 'Password has been changed'
            });
            return true;
        });
    }
    /**
     * Change the password for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     */
    doCreateUser(user, username) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.hasRole('adminManageUsers')) {
                throw new EdgeError_1.EdgeError('Not allowed to make this api call', 'NO_PERMISSION', 403);
            }
            if (username !== slug(username)) {
                throw new EdgeError_1.EdgeError('Username is not valid', 'INVALID_USERNAME', 400);
            }
            const targetUser = yield this.doInternalLoadUser(username);
            if (targetUser.isLoaded()) {
                throw new EdgeError_1.EdgeError('Username already exists', 'INVALID_USERNAME', 400);
            }
            const newPasswordHash = this.internalGeneratePassword();
            const correctPassword = passwordUtil.generatePasswordHash(newPasswordHash);
            const newUser = {
                password: correctPassword
            };
            this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/user/${username}`,
                newData: newUser,
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Account Change',
                subcategory: 'Create',
                text: 'User has been created'
            });
            return {
                id: username,
                password: newPasswordHash
            };
        });
    }
    /**
     * Update the information for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param newData - The user data to update
     */
    doUpdateUser(user, username, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, username, 'adminManageUsers');
            const fieldsToExclude = ['Password.ts', 'id', '_id', 'roles'];
            for (const field of fieldsToExclude) {
                delete newData[field];
            }
            for (const key in newData) {
                if (newData.hasOwnProperty(key) && key.startsWith('_')) {
                    delete newData[key];
                }
            }
            this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/user/${username}`,
                newData: newData,
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Account Change',
                subcategory: 'Profile Updated',
                text: 'User profile has been updated',
                options: newData
            });
            return true;
        });
    }
    /**
     * Update the information for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param system - The system to locate and update
     * @param rolesAdd - The username to locate and update
     * @param rolesRemove - The user data to update
     */
    doUpdateUserRoles(user, username, system, rolesAdd, rolesRemove) {
        return __awaiter(this, void 0, void 0, function* () {
            // only admin can change role of users
            if (!user.hasRole('adminManageUsers')) {
                throw new EdgeError_1.EdgeError('Not allowed to make this api call', 'NO_PERMISSION', 403);
            }
            const targetUser = yield this.doInternalLoadUser(username);
            if (!targetUser.isLoaded()) {
                throw new EdgeError_1.EdgeError('Can not find user with provided username', 'INVALID_USERNAME', 401);
            }
            targetUser.addRoles(rolesAdd, system);
            targetUser.removeRoles(rolesRemove, system);
            this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/user/${username}`,
                newData: targetUser.rec,
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Account Change',
                subcategory: 'Role Updated',
                text: 'User role has been updated',
                options: { rolesAdd, rolesRemove }
            });
            return true;
        });
    }
    /**
     * return list of groups based on prefix filter
     * @param user - currently logged in user
     * @param prefix - prefix to filter group
     */
    doGroupList(user, prefix) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.doInternalGetAllGroups(prefix);
        });
    }
    /**
     * create a new group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param description - description of the group to create
     */
    doGroupCreate(user, name, description) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, null, 'adminGroupManager');
            const existingGroup = yield this.doInternalLoadGroup(name);
            if (existingGroup) {
                throw new EdgeError_1.EdgeError(`Group with name(${name}) already exists`, 'GROUP_ALREADY_EXISTS', 400);
            }
            const newGroup = {
                id: name,
                name,
                description,
                created_by: user.rec.name,
                first_modified: new Date()
            };
            yield this.doCall('data_doUpdatePath', {
                dataSet: 'lookup',
                path: `/groups/${name}`,
                newData: newGroup,
            }, user);
            return newGroup;
        });
    }
    /**
     * add new role(s) to a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param strRole - role string or array of string
     */
    doGroupAddRole(user, name, system, strRole) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, null, 'adminGroupManager');
            const existingGroup = yield this.doInternalLoadGroup(name);
            if (!existingGroup) {
                throw new EdgeError_1.EdgeError(`Can not find group with name(${name})`, 'NOT_FOUND', 404);
            }
            const roles = typeof strRole === 'string' ? [strRole] : strRole;
            const existingRoles = _.get(existingGroup, `roles.${system}`, []);
            const rolesToUpdate = {
                [system]: [
                    ...roles,
                    ...existingRoles
                ]
            };
            yield this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/groups/${name}`,
                newData: rolesToUpdate
            }, user);
            return true;
        });
    }
    /**
     * remove roles for a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param strRole - role string or array of string
     */
    doGroupRemoveRole(user, name, system, strRole) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, null, 'adminGroupManager');
            const existingGroup = yield this.doInternalLoadGroup(name);
            if (!existingGroup) {
                throw new EdgeError_1.EdgeError(`Can not find group with name(${name})`, 'NOT_FOUND', 404);
            }
            const roles = typeof strRole === 'string' ? [strRole] : strRole;
            const existingRoles = _.get(existingGroup, `roles.${system}`, []);
            const rolesToUpdate = {
                [system]: _.difference(existingRoles, roles)
            };
            yield this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/groups/${name}`,
                newData: rolesToUpdate,
            }, user);
            return true;
        });
    }
    /**
     * add user to a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param username- username to add to a group
     */
    doGroupAddUser(user, name, system, username) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, null, 'adminGroupManager', system);
            const existingGroup = yield this.doInternalLoadGroup(name);
            if (!existingGroup) {
                throw new EdgeError_1.EdgeError(`Can not find group with name(${name})`, 'NOT_FOUND', 404);
            }
            const membersToUpdate = [
                ...(existingGroup.members || []),
                username
            ];
            yield this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/groups/${name}`,
                newData: { members: membersToUpdate }
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Group Change',
                subcategory: 'User Added',
                text: 'Group members has been updated',
                options: { username }
            });
            return true;
        });
    }
    /**
     * remove user from a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param username- username to add to a group
     */
    doGroupRemoveUser(user, name, system, username) {
        return __awaiter(this, void 0, void 0, function* () {
            this.verifyUserRole(user, null, 'adminGroupManager', system);
            const existingGroup = yield this.doInternalLoadGroup(name);
            if (!existingGroup) {
                throw new EdgeError_1.EdgeError(`Can not find group with name(${name})`, 'NOT_FOUND', 404);
            }
            const membersToUpdate = _.pull(existingGroup.members || [], username);
            yield this.doCall('data_doUpdatePath', {
                dataSet: 'master',
                path: `/groups/${name}`,
                newData: { members: membersToUpdate }
            }, user);
            yield user.doAddTimeline(this, {
                participant_id: username,
                category: 'Group Change',
                subcategory: 'User Removed',
                text: 'Group members has been updated',
                options: { username }
            });
            return true;
        });
    }
    /**
     *
     * @param user - logged in user
     * @param targetUsername - username to update
     * @param role - role name to verify
     */
    verifyUserRole(user, targetUsername, role, system) {
        if (user.rec.id !== targetUsername && !user.hasRole(role, system)) {
            this.authLogger.error({
                userId: user.rec.id,
                ipAddress: user.rec.last_login_ip,
                role,
                system,
                targetUsername
            });
            // TODO should enable this code later
            // throw new EdgeError(`User ${user.rec.id} not authorized for API Call assessing user ${targetUsername}`, 'NO_PERMISSION', 403);
        }
    }
    internalGetCollection(dataset = 'master', collection = 'user') {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.internalCollections[`${dataset}_${collection}`]) {
                return this.internalCollections[`${dataset}_${collection}`];
            }
            this.internalCollections[`${dataset}_${collection}`] = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                let dsMaster = this.internalGetDataset(dataset);
                let userCollection = yield dsMaster.doGetCollection(collection);
                resolve(userCollection);
            }));
            return this.internalCollections[`${dataset}_${collection}`];
        });
    }
    /**
     * Get the reference to the data set
     * @param datasetName
     */
    internalGetDataset(datasetName) {
        let dataSet = slug(datasetName);
        if (!this.dataStore[dataSet]) {
            this.dataStore[dataSet] = new edgedatasetmanager_1.default(dataSet);
        }
        return this.dataStore[dataSet];
    }
    /**
     * Get the random password
     * @param length
     */
    internalGeneratePassword(length = 8) {
        const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        let retVal = '';
        for (let i = 0; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * charset.length));
        }
        return retVal;
    }
    doInternalLoadUser(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const userCollection = yield this.internalGetCollection('master', 'user');
            const userObject = new IApiUserOffline_1.default();
            userObject.rec = yield userCollection.doFindByID(username);
            return userObject;
        });
    }
    doInternalLoadGroup(name) {
        return __awaiter(this, void 0, void 0, function* () {
            const groupCollection = yield this.internalGetCollection('lookup', 'groups');
            return groupCollection.doFindByID(name);
        });
    }
    doInternalGetAllGroups(prefix) {
        return __awaiter(this, void 0, void 0, function* () {
            const groupCollection = yield this.internalGetCollection('lookup', 'groups');
            if (prefix) {
                return groupCollection.doFind({
                    name: { $regex: `/^${prefix}/` }
                });
            }
            else {
                return groupCollection.doFind({});
            }
        });
    }
    doInternalMergeRoles(original, target) {
        return __awaiter(this, void 0, void 0, function* () {
            const customizer = (objValue, srcValue) => {
                if (_.isArray(objValue)) {
                    return objValue.concat(srcValue);
                }
            };
            _.mergeWith(original, target, customizer);
        });
    }
}
exports.default = ProviderAuth;
;
//# sourceMappingURL=ProviderAuth.js.map