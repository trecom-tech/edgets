import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatText extends DataFormatterType {
    name: string;
    input_width: number;
    align: string;
    constructor(...args: any[]);
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
}
export default DataFormatText;
