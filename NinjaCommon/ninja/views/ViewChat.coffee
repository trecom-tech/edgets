
class ViewChat extends View
    NEW_THRESHOLD: 120

    onShowScreen: () =>
        # @mainWrapper = @addDiv
        # @mainWrapper.html "This is chat view"
        @setupWidgets()
        @setHeaderTitle "Messages"
        @setFooterText "Need help? Tap here to talk to a trial coordinator"

        @setupEventHandlers()

        @messageCnt      = 0
        @messages        = []
        @lastMsgStamp    = null
        @inputLineHeight = 16
        @inputMinHeight  = 40
        @inputMaxLines   = 6
        true

    ##
    ## Set user of mine in this chat
    setMe: (@me) =>
        true

    setHeaderTitle: (@headerTitle) =>
        @wgtHeaderTitle.html @headerTitle
        true

    setFooterText: (@footerText) =>
        @wgtFooterText.html @footerText
        true

    addMessage: (id = @messageCnt, content, incoming, from = @me?.name, fromIcon, stamp = new Date(), type = "string") =>
        if typeof id is "object"
            content = id.content
            incoming = id.incoming
            from = id.from or @me.name
            fromIcon = id.fromIcon
            stamp = id.stamp or new Date()
            type = id.type or "string"
            id = id.id or @messageCnt

        # console.log("Adding message", id, content)

        if !content then return null
        newMsg = false
        momStamp = moment(stamp)
        if !@lastMsgStamp or momStamp.diff(@lastMsgStamp) > @NEW_THRESHOLD * 1000
            newMsg = true
            momNow = moment()
            dayDiff = momNow.diff(momStamp, 'days', true)
            dayStr = momStamp.format("dddd, MMM DD YYYY")
            if dayDiff < 2
                if momNow.date() - momStamp.date() is 0
                    dayStr = "Today"
                else if momNow.date() - momStamp.date() is 1
                    dayStr = "Yesterday"

            timeLine = @wgtMessagesWrapper.addDiv "msg-time-line"
            timeLine.html "#{dayStr} #{momStamp.format('hh:mm a')}"

        className = if incoming then "msg-from-op" else "msg-from-me"
        messageWrapper = @wgtMessagesWrapper.addDiv "#{className}", "viewchat-msg#{@messageCnt}"
        if incoming and newMsg
            messageFromDiv = messageWrapper.addDiv "message-from", "viewchat-msg-title#{@messageCnt}"
            messageFromDiv.html from

        messageDiv     = messageWrapper.addDiv "message", "viewchat-msg#{@messageCnt}"
        messageDiv.html content

        @scrollToBottom @wgtMessagesWrapper

        message =
            id: id
            content: content
            incoming: incoming
            from: from
            fromIcon: fromIcon
            stamp: stamp
            type: type

        @lastMsgStamp = stamp
        @messageCnt++
        @messages.push message
        message

    sendMessage: (data) =>
        # console.log "Sending message: ", data, data.content.trim()
        @clearInput()
        if !@validateInput(data) then return false
        @requestSendMessage(data)
        .then (res) =>
            if res
                @addMessage null, data.content, false, @me.name, null, data.stamp, data.type
        .catch (err) =>
            console.log "Error sending message: ", err

    requestSendMessage: (content) =>
        return new Promise (resolve, reject) =>
            setTimeout () =>
                resolve true
            , 500

    setupWidgets: () =>
        @wgtMainWrapper = @addDiv "mainWrapper", "mainWrapper"
        @wgtHeader = @wgtMainWrapper.addDiv "header", "header"
        @wgtHeaderTitle = @wgtHeader.addDiv "header-title"

        @wgtMessagesWrapper = @wgtMainWrapper.addDiv "messages-wrapper", "messagesWrapper"
        @wgtInputWrapper = @wgtMainWrapper.addDiv "input-wrapper", "inputWrapper"
        @wgtTextInputWrapper = @wgtInputWrapper.addDiv "text-input-wrapper"
        @wgtTextInput = @wgtTextInputWrapper.add "textarea", "text-input", "text-input#{@messageCnt}", { placeholder: "Type your message here."}
        @wgtBtnSend = @wgtInputWrapper.addDiv "icon-btn", "btnSend"
        @wgtBtnSend.html "<i class='far fa-paper-plane' />"
        @wgtBtnCamera = @wgtInputWrapper.addDiv "icon-btn", "btnCamera"
        @wgtBtnCamera.html "<i class='far fa-camera' />"
        @wgtBtnFilm = @wgtInputWrapper.addDiv "icon-btn", "btnFilm"
        @wgtBtnFilm.html "<i class='far fa-film' />"

        @wgtFooter = @wgtMainWrapper.addDiv "footer", "footer"
        @wgtFooterText = @wgtFooter.addDiv "footer-text", "footerText"
        true

    validateInput: (data) =>
        if !data? then return false

        if data.type is "string"
            if !data.content? or data.content.trim() is ""
                return false

        true

    clearInput: () =>
        if @wgtTextInput?
            @wgtTextInput.getTag().val("")
            @wgtTextInput.setAttribute "rows", Math.floor(@inputMinHeight / @inputLineHeight)
            @wgtTextInput.css "overflow-y", "hidden"

    setupEventHandlers: () =>
        if @wgtBtnSend?
            @wgtBtnSend.on "click", (e) =>
                inputVal = if @wgtTextInput.getTag() then @wgtTextInput.getTag().val() else null
                @sendMessage
                    content: inputVal
                    stamp: new Date()
                    type: "string"
                true

        if @wgtTextInput?
            @wgtTextInput.on "keyup", (e) =>
                if e.key is "Enter"
                    if e.shiftKey is true
                        el = @wgtTextInput.getTag()
                        if el
                            contentH = el[0].scrollHeight - el[0].scrollTop
                            if contentH > @inputMinHeight
                                rowCnt = Math.ceil(contentH / @inputLineHeight)
                                if rowCnt <= @inputMaxLines
                                    @wgtTextInput.setAttribute "rows", rowCnt
                                    @wgtTextInput.css "overflow-y", "hidden"
                                else
                                    @wgtTextInput.css "overflow-y", "auto"
                    else
                        @sendMessage
                            content: e.target.value
                            stamp: new Date()
                            type: "string"

                true

    scrollToBottom: (widget) =>
        if widget? and widget.getTag() and widget.getTag().length > 0
            el = widget.getTag()[0]
            el.scrollTop = el.scrollHeight - el.clientHeight
            return true
        false

    onResize: (w, h) =>
        this.width w
        this.height h
        true