"use strict";
//
// class for datetime data type
//
// @extends [DataFormatterType]
//
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const common = new DataFormatCommon_1.DataFormatCommon();
const flatpickr = require('flatpickr');
class DataFormatDateTime extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'datetime';
        this.input_width = 250;
        this.align = 'left';
    }
    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                type: "text",
                class: "dynamic_edit"
            });
            this.appendEditor();
        }
        this.elEditor.css({
            position: "absolute",
            "z-index": 5001,
            top,
            left,
            width,
            height
        });
        this.attachEditor(this.elEditor, path);
        // this.elEditor.on("change", (val: any) => {
        //     // console.log "DATE CHANGE=", val
        //     this.editorShowing = false;
        //     return this.elEditor.hide();
        // });
        this.elEditor.val(this.format(currentValue, path));
        this.elEditor.show();
        return this.elEditor.focus();
    }
    attachEditor(elSourceField, path) {
        super.attachEditor(elSourceField, path);
        const options = JSON.parse(elSourceField.data("options") || "{}");
        const df = (options != null) && (options.dateonly != null) && (options.dateonly === true) ? "D, M J, Y" : "D, M J, Y h:i:S K";
        const et = (options != null) && (options.dateonly != null) && (options.dateonly === true) ? false : true;
        const currentValue = this.unformat(elSourceField.val(), path);
        // #|
        // #|  Create 3rd party date picker control
        this.datePicker = flatpickr(elSourceField[0], {
            allowInput: true,
            dateFormat: df,
            parseDate(dateStr) {
                const mom = common.getMoment(dateStr, 'MM/DD/YYYY HH:mm:ss');
                if (mom != null) {
                    return mom.toDate();
                }
                return null;
            },
            onChange: (dateObj, dateStr) => {
                if ((dateObj != null) && dateObj.length) {
                    this.saveValue(dateObj);
                    elSourceField.val(this.format(dateObj[0], options, path));
                    return elSourceField.change();
                }
            },
            onOpen: (dateObj, dateStr, instance) => {
                const time = __guard__(common.getMoment(this.unformat(elSourceField.val(), path)), (x) => x.format('MM/DD/YYYY HH:mm:ss'));
                if (time != null) {
                    return instance.setDate(time);
                }
            },
            onClose: (dateObj, dateStr, instance) => {
                if ((dateObj != null) && dateObj.length) {
                    const date = dateObj[0];
                    const time = __guard__(common.getMoment(date), (x) => x.format('MM/DD/YYYY HH:mm:ss'));
                    if (time != null) {
                        instance.setDate(date);
                    }
                }
                this.editorShowing = false;
                if (this.elEditor != null) {
                    return this.elEditor.hide();
                }
            },
            enableTime: true,
            time_24hr: true
        });
        this.elDataEditor = this.datePicker.calendarContainer;
        // #|
        // #|  Attach any keyboard event in the text editor to open the date picker
        elSourceField.on('keydown', (e) => {
            // if editor is closed then close datepicker
            if (!this.editorShowing) {
                return this.datePicker.close();
            }
        });
        elSourceField.val(this.format(currentValue));
        return true;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((typeof options === "string") && (options[0] === "{")) {
            options = JSON.parse(options);
        }
        let format = "ddd, MMM Do, YYYY h:mm:ss A";
        if ((options != null) && (options.dateonly != null) && (options.dateonly === true)) {
            format = "ddd, MMM Do, YYYY";
        }
        const m = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        return m.format(format);
    }
    //
    // funtion to unformat the currently unformatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        const format = "YYYY-MM-DD HH:mm:ss";
        const m = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        return new Date(m.format(format));
    }
    getValueInTooltip(value) {
        const format = "YYYY-MM-DD";
        const m = common.getMoment(value, format);
        if (!m) {
            return "";
        }
        return m.format(format);
    }
}
exports.DataFormatDateTime = DataFormatDateTime;
exports.default = DataFormatDateTime;
function __guard__(value, transform) {
    return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}
//# sourceMappingURL=DataFormatDateTime.js.map