# EdgeCommonTimeseriesData
> Interface to InfluxDB

    yarn add git+ssh://git@gitlab.protovate.com:EdgTS/EdgeCommonTimeseriesData.git
    import EdgeTimeseriesData from 'edgecommontimeseriesdata';

> Get singleton

    let timeseriesData = EdgeTimeseriesData.getInstance(credentials);

## What is EdgeTimeseriesData

This class is an interface to InfluxDB for timeseries data. Currently supports only InfluxDB

@see [InfluxDB Tutorial](https://www.youtube.com/watch?v=d99oCnOyA4k)

Events have four types of data:

- Type 1 - Key/Value Data that should be indexed for searching
- Type 2 - Key/Value Data that is used for reporting but not searching
- Type 3 - The time of the event
- Type 4 - The name or category of the event

## Usage

### Create database

You can create database with databaseName, it will check if already exists before creating

    @param databaseName [string] name of the database to create

    doCreateDatabase: (databaseName)


### Get list of databases

Returns all database names
    
    doGetDatabases: ()


### Get measurements 

Returns all measurements for specific database

    @param databaseName [string] name of database

    doGetMeasurements: (databaseName)


### Query

Executes a query and returns array result

    @param databaseName [string]
    @param fields [string] use fields as "*" or "field1, field2"
    @param measurement [string] measurement name is used as a table in select

    doQuery: (databaseName, fields, measurement)


### Write measurement

Writes a measurement to existing database

    @param databaseName [string]
    @param measurement [string] measurement name
    @param tags [object] tags as key, value pairs
    @param data [object] data as fieldName: fieldValue 

    doWriteMeasurement: (databaseName, measurement, tags, data)

{"databaseName":"rates","tags":{},"data":{"avg":3090,"total":665911,"remain":1034,"name":"ChangeLog","host":"mslabhv1.lowes.com","memusage":110415264,"type":"status","slot":0,"uuid":"mslabhv1.l
owes.comChangeLog1507643882558","first_seen":"Tue Oct 10 2017 09:58:02 GMT-0400 (EDT)","proctime":2001.27465,"numproc":6785,"pid":52937},"level":"info","message":"measurement","timestamp":"2017-10-10T14:01:43.685Z"}

{"databaseName":"rates","tags":{},"data":{"avg":11,"total":2536,"remain":35140,"name":"Updates","host":"mslabhv1.lowes.com","memusage":116361528,"type":"status","slot":0,"uuid":"mslabhv1.lowes.
comUpdates1507643883706","first_seen":"Tue Oct 10 2017 09:58:03 GMT-0400 (EDT)","proctime":2081.500858,"numproc":17,"pid":52912},"level":"info","message":"measurement","timestamp":"2017-10-10T1
4:01:43.695Z"}



## Testing

### Testing Locally

Set INFLUX_HOST, INFLUX_USERNAME, IFLUX_PASSWORD environment variable and  run

    npm run test

### Testing via docker

    docker-compose build --build-arg SSH_KEY="$(cat ~/.ssh/id_rsa)" --build-arg SSH_KEY_PASSPHRASE="passphrase" --build-arg SSH_KEY_PUBLIC="$(cat ~/.ssh/id_rsa.pub)"
    docker-compose up -d
    docker-compose exec app npm run test

