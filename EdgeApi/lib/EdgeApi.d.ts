/// <reference types="socket.io-client" />
/// <reference types="socket.io" />
import * as redis from 'redis';
import EdgeApiBase from './EdgeApiBase';
export { default as EdgeApiProvider } from './EdgeApiProvider';
export { default as DeepDiff } from './DeepDiff';
export interface EdgeApiDefinition {
    base: string;
    command: string;
    params: {
        [varName: string]: any;
    };
    target: Function;
}
export interface EdgeSubscriptionItem {
    dataset: string;
    path: string;
}
export interface EdgeSubscription {
    channel: string;
}
export interface EdgeSocketRequestData {
    uuid: number;
}
export interface EdgeSocketResponseData {
    error: boolean;
    message: string;
}
/**
 * EdgeSocketClient is type of mixed of socket server and client type.
 * This is used in EdgeApiProvider, where it provide the underlying socket common logic
 * for both EdgeApiServer and EdgeApi
 */
export interface EdgeSocketClient extends SocketIOClient.Socket, SocketIO.Client {
    myConnectionID: number;
}
export declare class EdgeApi extends EdgeApiBase {
    static globalCacheReadonly: redis.RedisClient;
    static globalCache: redis.RedisClient;
    static globalCacheSets: redis.RedisClient;
    static globalCachedInstanceRemote: {
        [serverName: string]: Promise<EdgeApi>;
    };
    static globalCachedInstance: Promise<EdgeApi>;
    private socketConnectCallbackList;
    private currentServer;
    private serverList;
    socket: EdgeSocketClient;
    constructor();
    /**
     * Get EdgeApi instance for specific server
     * @param serverName string[] | string server url
     */
    static doGetApiForServer(serverName: string): Promise<EdgeApi>;
    static cacheGet: (keyname: string) => Promise<string>;
    static cacheSetExpireSeconds: (keyname: string, value: any, expireTime: number) => boolean;
    /**
     *
     * @param keyname
     * @param value
     */
    static cacheSet: (keyname: string, value: string) => Promise<unknown>;
    static cacheHGetAll: (keyname: string) => Promise<unknown>;
    /**
     * Send command to redis https://redis.io/commands
     * @param keyname
     * @param data
     */
    static cacheHMSet: (keyname: string, data: any) => boolean;
    /**
     *
     */
    static cacheSetPop: (keyname: string) => Promise<unknown>;
    static cacheSetAdd: (keyname: string, data: any) => boolean;
    static cacheGetSetLength: (keyname: string) => Promise<unknown>;
    static cacheSetMembers: (keyname: string) => Promise<unknown>;
    /**
     *
     * Quick utility function to query a table
     *
     * @param dataSet
     * @param tableName
     * @param condition
     * @param sort
     * @param callback (item, index, total) => Promise | booilean :  process each item callback
     */
    static doQuickFind: (dataSet: string, tableName: string, condition: any, sort: any, callback: Function) => Promise<boolean>;
    static doAggregate: (dataSet: string, tableName: string, aggregateQuery: any, options?: {}) => Promise<any[]>;
    /**
     * A simple way to run a query such as SELECT field, count(*) FROM table GROUP BY field
     * This runs a mongo aggregate against the "fieldList" array
     * @param dataSet [string] Data set name
     * @param tableName [string] Name of the table
     * @param fieldList [Object or Array or Comma String] A list of fields to group
     * @param matchCondition [object] Can be null, only count if a certain condition exists
     * @param sortCondition [Object] Optional sort condition
     * @param limit [number] Optional maximum records to return
     *
     */
    static doAggregateCount: (dataSet: string, tableName: string, fieldList: any, matchCondition: any, sortCondition: any, limit: number) => Promise<any[]>;
    static fixupJson: (obj: any) => any;
    static flattenObject: (obj: any, prefix: string, result: any) => any;
    static doGetApi(): Promise<EdgeApi>;
    /**
     * Returns Redis Readonly connection
     */
    static getRedisReadonlyConnection(): redis.RedisClient;
    static getRedisConnectionWQ(): redis.RedisClient;
    static getRedisConnection(): redis.RedisClient;
    doClose(): Promise<unknown>;
    connectToHost(): boolean;
    /**
     * Returns a valid socket-io based class
     * @param socketConnectCallbackList
     */
    doGetSocket(socketConnectCallbackList: Function[]): Promise<boolean>;
    /**
     * Internal helper function that recursively hashes a variable
     * @param recordHash
     * @param value
     */
    static getDataHash(recordHash: any, value: any): boolean;
    static getHash(value: any): string;
    /**
     * Removed undefined and null values in an object
     * @param obj
     */
    static cleanupObject(obj: any): boolean;
    static deepDiff(object1: any, object2: any, basePath?: any): any[];
    /**
     * General helper function to deep copy / clone an object
     * @param objTarget
     * @param objSrc
     * @param addAttributes
     * @param deleteAttributes
     */
    static deepMergeObject(objTarget: any, objSrc: any, addAttributes?: any, deleteAttributes?: any): any;
    /**
     * General helper function to deep copy / clone an object
     * @param obj
     */
    static cloneObject(obj: any): any;
    /**
     * Helper function to record the contents of an item object
     * to a text file in a way to can be used to debug or view the data easily.
     * @param obj
     * @param saveFile
     * @param theName
     */
    static dumpObject(obj: any, saveFile: any, theName: any): Promise<unknown>;
}
export default EdgeApi;
