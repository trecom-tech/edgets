import * as fs from 'fs';

const coffeeScript = require('coffeescript');

export class DynamicProvider {
    params: any[];
    fn: Function;
    resources: any[];

    constructor() {
        this.onInit                 = this.onInit.bind(this);
        this.serialize              = this.serialize.bind(this);
        this.initRunOptions         = this.initRunOptions.bind(this);
        this.verifyParamValue       = this.verifyParamValue.bind(this);
        this.logFunction            = this.logFunction.bind(this);
        this.doCallFunction         = this.doCallFunction.bind(this);
        this.parseCodeComments      = this.parseCodeComments.bind(this);
        this.addParam               = this.addParam.bind(this);
        this.createFunctionFromCode = this.createFunctionFromCode.bind(this);
        this.doCompileCoffeeToCode  = this.doCompileCoffeeToCode.bind(this);
        this.doCompileCoffeeFile    = this.doCompileCoffeeFile.bind(this);
        this.params                 = []; // # A list of parameters accepted
        this.resources              = [];
    }

    onInit() {
    }

    serialize() {
        let result = {
            params: this.params,
        };
        return result;
    }

    //  Initialize an object with default run options
    initRunOptions() {
        let options: any = {};

        //  Possible options the caller can modify before running
        options.trace        = false; // make a callback for console
        options.onConsoleLog = null; // callback function for console logs
        options.params       = {}; // key/value function params
        options.userObject   = {}; // user object for permissions/etc

        //  Storage for output
        options.output = {};

        return options;
    }

    verifyParamValue(paramNum: string, value: any) {
        //  TODO:  Check data types, etc
        return value;
    }

    logFunction(executeOptions: any, ...values: any) {

        if (executeOptions.onConsoleLog != null && typeof executeOptions.onConsoleLog === 'function') {
            let segment   = process.hrtime(executeOptions.startTime);
            let segmentns = segment[0] * 1e9 + segment[1];
            let segmentms = segmentns / 1e6;
            executeOptions.onConsoleLog(segmentms, values);
        } else {
            console.log('DYNAMIC LOG FUNCTION:', executeOptions, values);
        }

        return true;
    }

    //  execute the function as loaded by the dynamic code
    //  this may return a promise if the function itself returns a promise
    //  @param code {Mixed} Possible array of parameters in order or object with paramters by name
    //  @return {Mixed} Returns a promise or value based on the function
    doCallFunction(executeOptions: any) {

        return new Promise(async (resolve, reject) => {

            try {

                //  For reporting purposes
                if (executeOptions.trace) {
                    executeOptions.output.trace = [];
                }

                let args = [];

                //  User is always the first parameter
                if (executeOptions.userObject == null) {
                    executeOptions.userObject = {};
                }

                // Debug
                console.log('CALLING WITH:', executeOptions);
                console.log('CODE:', this.fn);
                executeOptions.startTime     = process.hrtime();
                executeOptions.output.result = this.fn.apply(this, [this.logFunction, executeOptions]);

                if (executeOptions.output.result != null && executeOptions.output.result.then != null && typeof executeOptions.output.result.then === 'function') {
                    executeOptions.output.result = await executeOptions.output.result;
                }

                let segment   = process.hrtime(executeOptions.startTime);
                let segmentns = segment[0] * 1e9 + segment[1];
                let segmentms = segmentns / 1e6;

                executeOptions.output.total_ms = segmentms;

                resolve(executeOptions.output);
            } catch (e) {
                console.log('doCallFunction Exception:', e);
                console.log('doCallFunction Exception str:', e.toString());

                executeOptions.output.error = e.toString();
                reject(executeOptions.output);
            }
        });
    }

    //  Parse the comments in the code.  See the README file
    //  to understand the jsdoc format
    //  @see http:// usejsdoc.org/tags-param.html
    parseCodeComments(code: any) {

        let all;
        let reParamWithType = new RegExp('@param {([^}]+)} ([a-zA-Z0-9\_\$\.]+)[\t \-]*(.*)', 'ig');
        while (reParamWithType.exec(code)) 
        {
            all = reParamWithType.exec(code);
            this.params.push({
                type: all[1],
                name: all[2],
                comment: all[3],
                optional: false,
                default: null,
            });
        }

        reParamWithType = new RegExp('@param \\[([^}]+)\\] ([a-zA-Z0-9\_\$\.]+)[\t \-]*(.*)', 'ig');
        while (reParamWithType.exec(code)) {
            all = reParamWithType.exec(code);
            this.params.push({
                type: all[1],
                name: all[2],
                comment: all[3],
                optional: true,
                default: null,
            });
        }

        let reSqlServer = new RegExp('@sql ([a-zA-Z0-9\_\$\.]+)[\t \-]*(.*)', 'ig');
        while (reParamWithType.exec(code)) {
            all = reParamWithType.exec(code);
            this.resources.push({
                type: 'sql',
                name: all[1],
                comment: all[2],
            });
        }

        let reSqlServerWithDb = new RegExp('@sql \\[([^}]+)\\] ([a-zA-Z0-9\_\$\.]+)[\t \-]*(.*)', 'ig');
        while (reParamWithType.exec(code)) {
            all = reParamWithType.exec(code);
            this.resources.push({
                type: 'sql',
                db: all[1],
                name: all[2],
                comment: all[3],
            });
        }

        return true;
    }

    //  Add a parameter to the list that this code requires
    //  TODO: Add validation on parameters
    addParam(name: any, comment: any) {
        this.params.push({
            name,
            comment,
        });

        return true;
    }

    createFunctionFromCode(code: any) {

        //  Rewrite the console.log statements to logFunction
        code = code.replace(new RegExp('console.log\\(', 'g'), 'logFunction(args, ');

        //  strCodePrefix = "var args = arguments[1];\n"
        let strCodePrefix = '';
        strCodePrefix += 'console.log(\'ARGS=\', args);\n';

        //  Dynamically add functions that can be called
        strCodePrefix += 'print = this.logFunction;\n';

        //  Assign the user object first as it's always there.
        strCodePrefix += 'var user = args.userObject;\n';
        strCodePrefix += 'console.log(\'PARAMS=\', args.params);\n';

        for (let item of this.params) {
            strCodePrefix += `var ${ item.name } = args.params['${ item.name }'];\n`;
            if (item.type === 'number' || item.type === 'int') {
                strCodePrefix += `if ((typeof(${ item.name }) != 'number') || isNaN(${ item.name }) || isInfinite(${ item.name })) ${ item.name } = 0;\n`;
            }
            strCodePrefix += `console.log('SET ${ item.name } = ', ${ item.name });\n`;
        }

        try {
            this.fn = new Function('logFunction', 'args', strCodePrefix + code);
            return true;
        } catch (e) {
            console.log('Error creating function, code=', strCodePrefix + code);
            console.log('Error:', e);
            return e.toString();
        }
    }

    doCompileCoffeeToCode(coffeeText: any) {

        let compiled = coffeeScript.compile(coffeeText, { bare: true });

        this.parseCodeComments(coffeeText);

        return compiled;
    }

    doCompileCoffeeFile(filename: any) {

        return new Promise((resolve, reject) => {

            return fs.readFile(filename, 'utf8', (err, content) => {
                if (err) {
                    throw err;
                }
                
                try {

                    let compiled = coffeeScript.compile(content, { bare: true });

                    this.parseCodeComments(content);
                    return resolve(compiled);
                } catch (ex) {

                    console.log('Content=', content);
                    console.log('E=', ex);
                    return reject(ex);
                }
            });
        });
    }
};

export default DynamicProvider;