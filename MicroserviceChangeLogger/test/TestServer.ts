import * as EdgeApiServer from 'edgeapiserver';
import config             from 'edgecommonconfig';

const ELASTIC_HOST = process.env.ELASTIC_HOST   || 'http://localhost:9200';
const DB_HOST      = process.env.DB_HOST ? 
                    `'mongodb://${process.env.DB_HOST}/admin'` : 
                    'mongodb://localhost/admin';
const REDIS_HOST   = process.env.REDIS_HOST     || 'redis://localhost:6379';
const MQHOST       = process.env.MQHOST         || 'amqp://guest:guest@localhost:5672';
const API_SERVER   = process.env.API_SERVER     || 'http://127.0.0.1:8001';

config.setCredentials('redisReadHost', `${REDIS_HOST}`);
config.setCredentials('redisHostWQ', `${REDIS_HOST}`);
config.setCredentials('redisHost', `${REDIS_HOST}`);

config.setCredentials('MongoDB', {
    url    : `${DB_HOST}`,
    options: {
        connectTimeoutMS: 600000,
        poolSize        : 16,
        socketTimeoutMS : 600000,
    },
});
config.setCredentials('elasticsearch', {
    type: 'es',
    host: ELASTIC_HOST,
});
config.setCredentials('mqHost', MQHOST);

config.ApiServers = [API_SERVER];


export const server = EdgeApiServer.startServer();
