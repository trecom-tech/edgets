class ViewTestShowZipcode extends View

    ##|
    ##|  Simple test that expects to show data on a single zipcode
    ##|  Assumes zipcode data is loaded into table=zipcodes already
    ##|

    setZipcode: (@id)=>
        @txtCurrentZipcode.html @id

    onShowScreen: ()=>
        console.log "ViewTestShowZipcode onShowScreen ID=", @id

    onResize: (w, h)=>
        super(w, h)

        @txtWidth.html        w
        @txtHeight.html       h
        @txtOuterWidth.html   @outerWidth()
        @txtOuterHeight.html  @outerHeight()

