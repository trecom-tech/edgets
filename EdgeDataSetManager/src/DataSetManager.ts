const geolib = require('geolib');

import { config } from 'edgecommonconfig';
import EdgeApi from 'edgeapi';

import DataItem from './DataItem';
import MongoGateway from './MongoGateway';
import DataSetConfig from './DataSetConfig';
import DataSetPath from './DataSetPath';
import MongoModel from './MongoModel';

export class DataSetManager {
    // Not used
    // api;
    changeCollection         : MongoModel;
    changeCollectionPromise  : Promise<MongoModel>;
    settingsPromise          : { [tableName     : string]: Promise<DataSetConfig> };
    collectionPromise        : { [collectionName: string]: Promise<MongoModel> };
    collections              : { [collectionName: string]: MongoModel };
    alternateConnectionConfig: any;
    db                       : MongoGateway;
    log                      : any;
    databaseName             : string;

    /**
     * Static member function opens a database set manager and returns the named collection
     * @param databaseName
     * @param collectionName
     */
    static async doOpen(databaseName: string, collectionName: string) {
        // TODO wondering how to implement cache? use node-promise-cache module?

        // return newPromise.cached(`DataSetManager_${ databaseName }_${ collectionName }`, function* () {

        //     let ds = new DataSetManager(databaseName);
        //     let collection = await ds.doGetCollection(collectionName);
        //     return collection;
        // });
        let ds = new DataSetManager(databaseName);
        let collection = await ds.doGetCollection(collectionName);
        return collection;
    }

    constructor(databaseName: string, alternateConnectionConfig: any = null) {
        this.databaseName              = databaseName;
        this.alternateConnectionConfig = alternateConnectionConfig;

        this.onInit();
        this.log                       = config.getLogger(`DataSetManager-${this.databaseName}`);
    }

    onInit() {
        this.db = new MongoGateway(
            this.databaseName,
            this.alternateConnectionConfig,
        );
        this.collections             = {};
        this.collectionPromise       = {};
        return (this.settingsPromise = {});
    }

    doGetChangeCollection(): Promise<MongoModel> {
        if (this.changeCollectionPromise != null) {
            return this.changeCollectionPromise;
        }

        return (this.changeCollectionPromise = new Promise(
            async (resolve, reject) => {
                await this.db.doConnectToDatabase();
                let c = this.db.collection('changes');
                await c.doCreateIndex({ stamp: -1 }, { unique: false });
                await c.doCreateIndex({ path: 1 }, { unique: false });
                await c.doCreateIndex({ cat: 1, key: 1 }, { unique: false });
                this.changeCollection = c;
                resolve(this.changeCollection);
            },
        ));
    }

    /**
     * Get a reference to the settings for this table
     * @param tableName
     */
    doInternalGetSettings(tableName: string) {
        if (this.settingsPromise[tableName] != null) {
            return this.settingsPromise[tableName];
        }

        this.settingsPromise[tableName] = new Promise(async (resolve: any, reject: any) => {
            await this.db.doConnectToDatabase();
            let settings = new DataSetConfig(this, tableName);
            await settings.doLoadSettings();
            resolve(settings);
        });

        return this.settingsPromise[tableName];
    }

    /**
     * Get a list of tables and counts open
     */
    async doGetUpdatedStats() {
        await this.doGetChangeCollection();

        let results: any = {};
        for (let name in this.collections) {
            let col = this.collections[name];
            results[name] = await col.theCollection.find().count();
        }

        if (this.changeCollection) {
            results[
                'change'
            ] = await this.changeCollection.theCollection.find().count();
        }

        return results;
    }

    /**
     * Create or verify an index 
     * @param tableName - [string] Collection Name
     * @param keyField - [string] Field name to index
     * @param indexType - [string] Index type: mongo field
     * @param isUnique - [string] Uniqueness to provide mongo indexing function
     */
    async doVerifySimpleIndex(tableName: string, keyField: any, indexType?: any, isUnique?: any) {
        let indexFields: any;
        if (!isUnique) {
            isUnique = false;
        }

        if (!indexType) {
            indexType = 1;
        }

        if (indexType === 'geo') {
            indexType = '2dsphere';
        }

        if (typeof keyField === 'object') {
            indexFields = keyField;
        } else {
            indexFields = {};
            indexFields[keyField] = indexType;
        }

        let collection = await this.doGetCollection(tableName);
        let result = await collection.doCreateIndex(indexFields, {
            unique: isUnique,
        });
        return result;
    }

    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit 
     */
    async doGetItems(
        path          : any,
        fieldList?    : any,
        sortCondition?: any,
        offset?       : any,
        limit?        : any,
    ): Promise<any[]> {
        let collection;
        path = new DataSetPath(path);

        // Don't wait for the collection if possible used the known value
        if (this.collections[path.collection] != null) {
            collection = this.collections[path.collection];
        } else {
            collection = await this.doGetCollection(path.collection);
        }

        return collection.doFind(
            path.condition,
            fieldList,
            sortCondition,
            offset,
            limit,
        );
    }

    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit 
     */
    async doStream(path: any, fieldList: any, offset: any, limit: any, callback: any) {
        path = new DataSetPath(path);
        let collection = await this.doGetCollection(path.collection);

        config.status('DataSetManager doStream path=', path);
        return collection.doTail(
            path.condition,
            fieldList,
            offset,
            limit,
            callback,
        );
    }

    /**
     * Exactly the same as doGetItems except that records are returned
     * as an array with the id as the key field
     * @param path 
     * @param fieldList 
     * @param sortCondition 
     * @param offset 
     * @param limit 
     */
    async doGetItemsArray(path: string, fieldList?: any, sortCondition?: any, offset?: number, limit?: number) {
        const results: any[] = await this.doGetItems(
            path,
            fieldList,
            sortCondition,
            offset,
            limit,
        );

        const output: any = {};
        for (const result of results) {
            output[result.id] = result;
        }

        return output;
    }

    /**
     * Count some search
     * @param path 
     */
    async doCount(path: any) {
        let collection;
        path = new DataSetPath(path);
        if (this.collections[path.collection] != null) {
            collection = this.collections[path.collection];
        } else {
            collection = await this.doGetCollection(path.collection);
        }

        return collection.doCount(path.condition);
    }

    getDistanceFromPolygon(origin: any, loc: { coordinates: string[] }) {
        let lowest = 9999999;
        if (loc == null) {
            return 0;
        }

        try {
            let allPoints = [];
            for (let lineString of Array.from(loc.coordinates)) {
                for (let p of lineString) {
                    let target = { latitude: p[1], longitude: p[0] };
                    allPoints.push(target);
                    let distance = geolib.getDistance(origin, target);
                    if (distance < lowest) {
                        lowest = distance;
                    }
                }
            }

            if (geolib.isPointInside(origin, allPoints)) {
                return 0;
            }
        } catch (e) {
            console.log('getDistanceFromPolygon Source:', origin, 'loc=', loc);
            console.log('getDistanceFromPolygon Error :', e);

            return 999;
        }

        return lowest;
    }

    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param path [mixed] Can be a compiled path or string path
     * @param centerLocation [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles [number] the number of miles to search around
     * @param offset [number] Optional starting point in the result list (default 0)
     * @param limit [number] Optional the maximum number of results (default 1000)
     */
    async doFindNearby(path: any, centerLocation: any, miles: any, offset: number, limit: number) {
        path = new DataSetPath(path);
        let collection = await this.doGetCollection(path.collection);
        let result: any = await collection.doFindNearby(
            path.condition,
            centerLocation,
            miles,
            offset,
            limit,
        );
        if (result == null) {
            return null;
        }

        if (centerLocation.coordinates != null) {
            let origin = {
                latitude: centerLocation.coordinates[1],
                longitude: centerLocation.coordinates[0],
            };
            for (let r of result) {
                let target;
                if (r.loc == null) {
                    continue;
                }

                if (r.loc.type === 'Polygon') {
                    r.distance = this.getDistanceFromPolygon(origin, r.loc);
                    r.distance /= 1609.34;
                } else if (r.loc.type === 'Point') {
                    target = {
                        latitude: r.loc.coordinates[1],
                        longitude: r.loc.coordinates[0],
                    };
                    r.distance = geolib.getDistance(origin, target);
                    r.distance /= 1609.34;
                } else {
                    let distance;
                    try {
                        let lowest = 9999999;
                        let base = r.loc.coordinates;
                        if (Array.isArray(base)) {
                            base = base[0];
                        }
                        if (Array.isArray(base)) {
                            base = base[0];
                        }

                        for (let p of base) {
                            if (typeof p[0] === 'number') {
                                target = {
                                    latitude: p[1],
                                    longitude: p[0],
                                };
                                distance = geolib.getDistance(
                                    origin,
                                    target,
                                );
                                if (distance < lowest) {
                                    lowest = distance;
                                }
                            }
                        }

                        r.distance = lowest / 1609.34;
                    } catch (e) {
                        console.log(
                            'doFindNearby Source:',
                            origin,
                            'r=',
                            r.loc.coordinates,
                        );
                        console.log('doFindNearby Error :', e);
                        r.distance = 999;
                        throw e;
                    }
                }
            }
        }

        return result;
    }

    async doFindNearbyIds(
        path: any,
        centerLocation: any,
        miles: any,
        offset: number,
        limit: number,
        fieldList?: any,
    ) {
        path = new DataSetPath(path);
        const collection = await this.doGetCollection(path.collection);
        return collection.doFindNearbyIds(
            path.condition,
            centerLocation,
            miles,
            offset,
            limit,
            fieldList,
        );
    }

    async doGetUniqueValues(path: any, matchCondition: any) {
        console.log(
            'DataSetManager path=',
            path,
            ' match=',
            matchCondition,
        );

        path = new DataSetPath(path);
        let collection = await this.doGetCollection(path.collection);

        let pathField = path.rawPath.replace(`/${path.collection}/`, '');
        pathField = pathField.replace(/\//g, '.');

        let result = await collection.doGetUniqueValues(
            pathField,
            matchCondition,
        );
        console.log('Found:', result);
        return result;
    }

    /**
     * Lookup a record within a dataset. 
     * @param path - [string] path
     * @param createIfNeeded - [string] if element does not exist on path, create one
     * @returns first element in that search criteria
     */
    async doGetItem(path: any, createIfNeeded: any = null): Promise<any> {
        if (createIfNeeded == null) {
            createIfNeeded = true;
        }

        let collection;
        path = new DataSetPath(path);

        // Don't wait for the collection if possible used the known value
        if (this.collections[path.collection] != null) {
            collection = this.collections[path.collection];
        } else {
            collection = await this.doGetCollection(path.collection);
        }

        // Search for the item directly if possible
        let item: any = null;
        if (path.condition['id'] != null) {
            let itemResult = await collection.doFindByID(
                path.condition.id,
                createIfNeeded,
                path.subPath,
                true,
            );
            if (itemResult != null) {
                item = new DataItem(path, collection, itemResult.value);
                item.isNew = itemResult.isNew;
            } else {
                console.log('Invalid, collection.doFindByID() null', path);
                console.log('Path=', path);
            }
        } else {
            let results = await collection.doFind(path.condition);
            if (results != null && results.length) {
                item = new DataItem(path, collection, results[0]);
            } else {
                console.log('Invalid, collection.doFind() null', path);
                console.log('Path=', path);
            }
        }

        return item;
    }

    /**
     * 
     * @param path 
     * @param newJson 
     * @param updateAgentID 
     */
    async doInsert(path: any, newJson: any, updateAgentID?: any) {
        let collection;
        path = new DataSetPath(path);
        if (!path.canSave) {
            throw new Error(`Can't update using this path: ${path}`);
        }

        //
        // Don't wait for the collection if possible used the known value
        if (this.collections[path.collection] != null) {
            collection = this.collections[path.collection];
        } else {
            collection = await this.doGetCollection(path.collection);
        }

        //
        // Check to see if the record has a sub path
        // or a direct insert into the table
        // TODO: Check SubPath
        //

        return collection.doUpsertOne(newJson.id, newJson);
    }

    /**
     * Given a path, delete everything under it
     * @param path 
     */
    async doDeletePath(path: any) {
        path = new DataSetPath(path);
        if (!path.canSave) {
            throw new Error(`Can't delete using this path: ${path}`);
        }

        console.log('DELETE USING PATH:', path);

        let settings = await this.doInternalGetSettings(path.collection);

        // If we are not tracking changes, merge and save quickly.
        if (!settings.check('track_changes')) {
            console.log(
                'TODO:  Not implemented delete with path not checked',
            );
            return false;
        }

        let item = await this.doGetItem(path);
        if (item == null || item.data == null) {
            console.log("Can't delete a path that doesn't exist");
            return false;
        }

        //
        // BasePointer now references the object at end the base path
        // so we can compare/insert the change data
        let basePointer = item.base;
        let numChanges = 0;
        let differences = EdgeApi.deepDiff(basePointer, {});

        let collection = await this.doGetCollection(path.collection);
        await collection.doRemove(path.condition);

        return differences;
    }

    /**
     * 
     * @param path 
     * @param newJson 
     */
    async doCheckUpdateHash(path: any, newJson: any) {
        const myHash = EdgeApi.getHash(newJson);
        const key = `DSM:${this.databaseName}:${path.collection}/${path.condition.id}/${path.subPath.join('/')}`;

        const result = await EdgeApi.cacheGet(key);
        return (result != null && result === myHash);
    }

    /**
     * 
     * @param path 
     * @param newJson 
     */
    async doSaveUpdateHash(path: any, newJson: any) {
        let myHash = EdgeApi.getHash(newJson);
        let key = `DSM:${this.databaseName}:${path.collection}/${
            path.condition.id
        }/${path.subPath.join('/')}`;
        // tslint:disable-next-line:no-floating-promises
        EdgeApi.cacheSet(key, myHash);

        // config.dump "doSaveUpdateHash",
        //     dataset    : @databaseName
        //     collection : path.collection
        //     path       : path.condition
        //     subPath    : path.subPath
        //     myHash     : myHash
        //     key        : key

        // theDoc = {}
        // basePointer = theDoc
        // for sub in path.subPath
        //     basePointer[sub] = {}
        //     basePointer = basePointer[sub]

        // EdgeApi.deepMergeObject basePointer, newJson, {}, {}
        // config.dump "Elastic Save", theDoc

        // result = await @elastic.doUpdate @databaseName, path.collection, path.condition.id, theDoc
        // config.dump "Elastic Result", result

        // result = await @elastic.setDocumentHash @databaseName, path.collection, path.condition.id, newJson, path.subPath.join("/")
        return true;
    }

    /**
     * Given a path and some json data, append the data to the path
     * treating the path like an array. if the path is not an array then
     * we find the next id and add it that way.
     * @param path 
     * @param newJson 
     * @param updateAgentID 
     */
    async doAppend(path: any, newJson: any, updateAgentID?: any) {
        config.status(`doAppend path=${path}`);
        path = new DataSetPath(path);
        let collection = await this.doGetCollection(path.collection);
        let result: any = await collection.doAppend(
            path.condition,
            path.subPath,
            newJson,
        );

        if (path.condition != null && path.condition.id != null) {
            let key = `LU${this.databaseName}_${path.collection}_${
                path.condition.id
            }`;
            config.status(
                `DatasetManager doAppend Starting erase cache key ${key}`,
            );
            // tslint:disable-next-line:no-floating-promises
            EdgeApi.cacheSet(key, null);
            return [
                { kind: 'P', path: path.subPath, lhs: '', rhs: newJson },
            ];
        }

        if (result != null && result.result != null) {
            return {
                ok: result.result.ok,
                nModified: result.result.nModified,
                n: result.result.n,
            };
        }

        return [];
    }

    /**
     * Given a path and some new JSON data, update the object
     * save the results back to the database
     * @param path - [String|object] Path can be a string path or a parsed path
     * @param newJson - [any] the data to be added or updated
     * @param updateAgentID - [string] the _id of an approved agent (optional)
     */
    async doUpdate(path: string | any, newJson: any, updateAgentID?: string): Promise<any[]|boolean> {
        let collection;
        config.status('doUpdate path=', path);

        path = new DataSetPath(path);
        if (!path.canSave) {
            this.log.error("Can't update using path", {
                path,
                newJson,
                updateAgentID,
            });

            console.log("Can't update using this path:", path);
            return false;
        }

        //
        // If this newjson is an exact match for the path already known
        // according to the cached hash values, then skip

        // TODO roy: theres no case doCheckUpdateHash returns true because its promise
        // Just uncomment when edge api cacheGet is working.

        let result = await this.doCheckUpdateHash(path, newJson);

        if (result != null && result === true) {
            this.log.info("Not updating", {
                path,
                newJson
            });
            return true;
        }

        let settings = await this.doInternalGetSettings(path.collection);
        let item = null;

        config.status('Starting doUpdate::settings.doTransformObject');
        let objDidTransform: any = await settings.doTransformObject(
            path,
            newJson,
        );

        if (path.subPath.length === 0) {
            //
            // Special case, quick modification
            collection = await this.doGetCollection(path.collection);
            config.status('Starting doFindAndInsert');
            let result = await collection.doFindAndInsert(
                path.condition,
                newJson,
            );
            if (result == null) {
                return false;
            }

            //
            // If it's new then it's already added
            if (result.isNew) {
                return [];
            }

            item = new DataItem(path, collection, result.value);

            if (item.data.id == null) {
                this.log.error('Invalid DataItem', {
                    condition: path.condition,
                    item: item.data,
                    result,
                });

                return false;
            }
        } else {
            config.status('Starting doUpdate::doGetItem for path');
            item = await this.doGetItem(path);
            if (item == null || item.data == null) {
                console.log('doGetItem should not return null');
                return false;
            }
        }

        //
        // BasePointer now references the object at end the base path
        // so we can compare/insert the change data
        config.status('Starting doUpdate::diff()');
        let basePointer = item.base;
        let numChanges = 0;

        // console.log "DIFF A:", JSON.stringify(basePointer), JSON.stringify(newJson)
        let differences: any[] = EdgeApi.deepDiff(basePointer, newJson);

        let allDifferences = [];
        for (let d of Array.from(differences)) {
            if (d.path != null && /comps/.test(d.path)) {
                continue;
            }

            // if d.kind == "N" and d.lhs == null then continue

            if (d.path != null && d.path[0] === 'processed') {
                continue;
            }

            if (d.path != null && d.path[0] === '_lastModified') {
                continue;
            }

            allDifferences.push(d);
        }

        // if differences.length == 0

        //     console.log "Starting:", basePointer
        //     console.log "Target  :", newJson

        if (differences.length > 0) {
            // config.dump "#{item.data.id} DIFF", differences

            if (basePointer['_id'] != null && newJson['_id'] == null) {
                delete newJson._id;
            }

            config.status('Starting EdgeApi.deepMergeObject');
            EdgeApi.deepMergeObject(basePointer, newJson);

            config.status('Starting item.doSaveUpdates');
            await item.doSaveUpdates();

            //
            // Cache connection
            if (path.condition.id != null) {
                let key = `LU${this.databaseName}_${path.collection}_${
                    path.condition.id
                }`;
                config.status(`Starting erase cache key ${key}`);
                // tslint:disable-next-line:no-floating-promises
                EdgeApi.cacheSet(key, null);
                // tslint:disable-next-line:no-floating-promises
                this.doSaveUpdateHash(path, newJson);
            }
        }

        config.status(
            `doUpdate returning ${allDifferences.length} differences`,
        );
        return allDifferences;
    }

    //
    // Save a change record
    //
    async doRecordDataChanges(changeLogs: any[]) {
        //
        // Defined in Worker.coffee's version of doRecordDataChange

        // # type   : "change"
        // # ds     : dataSet
        // # path   : path
        // # stamp  : new Date()
        // # kind   : change.kind
        // # lhs    : change.lhs
        // # rhs    : change.rhs
        if (changeLogs == null || changeLogs.length < 1) {
            return true;
        }

        if (!this.changeCollection) {
            await this.doGetChangeCollection();
        }

        let changeLogsFinal = [];
        for (let changeLog of Array.from(changeLogs)) {
            delete changeLog.ds;
            delete changeLog.type;
            let parts = changeLog.path.split('/');

            if (parts[1] != null && parts[2] != null) {
                changeLog.cat = parts[1];
                changeLog.key = parts[2];
                changeLogsFinal.push(changeLog);
            }
        }

        if (changeLogsFinal.length < 1) {
            return true;
        }

        await this.changeCollection.doInsertMany(changeLogsFinal);
        return true;
    }

    /**
     * Run mongo remove with search Criteria
     * @param tableName 
     * @param searchCondition 
     */
    async doRemove(tableName: string, searchCondition?: any) {
        //
        // TODO:  Track history on Deletes
        //

        let collection = await this.doGetCollection(tableName);
        let result = await collection.doRemove(searchCondition);
        return result;
    }

    /**
     * Execute an aggregate against a table
     * @param path 
     * @param aggList 
     */
    async doAggregate(path: any, aggList: any) {
        path = new DataSetPath(path);
        let collection = await this.doGetCollection(path.collection);
        let r = await collection.doAggregate(aggList);
        return r;
    }

    /**
     * Get the collection for a given category
     * @param name collection name
     */
    doGetCollection(name: string) {
        if (this.collectionPromise[name] != null) {
            return this.collectionPromise[name];
        }

        return (this.collectionPromise[name] = new Promise(
            async (resolve, reject) => {
                try {
                    await this.db.doConnectToDatabase();
                    let c = this.db.collection(name);
                    await c.doCreateIndex({ id: 1 }, { unique: true });
                    // await c.doCreateIndex { _lastModified: -1 }, { unique: false }
                    this.collections[name] = c;
                    resolve(this.collections[name]);
                } catch (err) {
                    reject(err);
                }
            },
        ));
    }
}

export default DataSetManager;