from lib.client import EdgeApiClient

def on_list(data):
	for key in data:
		print key + " : "
		print data[key]
		print "				\n"

def on_api_call(data):
	for key in data:
		print key + " : "
		print data[key]
		print "				\n"

c = EdgeApiClient()
c.setServer("localhost", 8001)
c.connect()

c.get_list(on_list)
c.make_call("os_doGetServerInformation", on_api_call)