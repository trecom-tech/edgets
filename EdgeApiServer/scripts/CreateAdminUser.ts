import {
    addServiceUser,
    DEFAULT_SERVICE_USER
} from '../src/utility/AddServiceUser';

//
// Create admin user
let args = {
    apiUser: "admin",
    apiPassword: "scott123"
};

addServiceUser(args)
.then((res) => {
    console.log('Added or confirmed admin user');
}).catch((err) => {
    console.log(err);
});
