const crypto = require('crypto');
import { config }       from 'edgecommonconfig';

export function generatePasswordHash(password: string): string {
    const ha1 = new crypto.createHash('md5');
    ha1.update(config.getCredentials('secretKey') + password);
    return ha1.digest("hex");
}