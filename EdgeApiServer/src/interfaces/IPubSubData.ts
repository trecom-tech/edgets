export interface IPubSubData {
    channel: string;
    stamp: Date;
    data: any;
}