#!/bin/bash

npm version minor --force
npm run build
git add .
git commit -a -m "$*"
git push
