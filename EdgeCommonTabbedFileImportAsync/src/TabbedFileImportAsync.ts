import * as fs from 'fs';

export default class TabbedFileImportAsync {
    /**
     * Has a header line
     */
    hasHeader: boolean;

    /**
     * The decimal ascii code for the field seperator
     */
    seperator: number;

    /**
     * The number of bytes to read from the file at once
     */
    sLength: number;
    
    /**
     * The number of lines to skip
     */
    skipCount: number;

    handleEscaped: boolean;

    /**
     * Filename to import
     */
    private filename: string;

    private buffer: any;
    
    private fd: any;

    private readPos: number;

    private readSize: number;

    private callback: any;

    private headers: any;

    constructor(filename: string) {
        this.filename      = filename;
        this.hasHeader     = false;
        this.seperator     = 9;
        this.sLength       = 1024 *120;
        this.skipCount     = 0;
        this.handleEscaped = true;
        this.buffer        = Buffer.alloc(this.sLength);
        this.fd            = fs.openSync(filename, 'r');
        this.readPos       = 0;
        this.readSize      = 0;
        this.callback      = null;
        this.headers       = null;
    }

    /**
     * Shortcut function to configure for a CSV file
     */
    setFormatCSVWithHeaders() {
        this.hasHeader     = true;
        this.seperator     = 44;
        this.handleEscaped = true
        this.skipCount     = 0;
    }

    /**
     * Returns a promise that completes once the file completes
     * @param callback - If callback returns a promise then we pause reading until it's done
     */
    async readLine(callback: any) {
        this.callback = callback;
        this.headers  = this.hasHeader ? null : this.headers;

        // Current items read in from this line
        let currentArray    = [];
        let currentValue    = '';
        let currentLength   = 0;
        let currentSpaces   = 0;
        let currentLine     = 0;
        let isQuoteString   = false;
        let absoluteNext    = false;

        if (typeof this.callback !== 'function') {
            return false;
        }

        while (true) {
            if (this.readSize <= this.readPos) {
                const result = await this.doReadBuffer();
                if (!result) {
                    break;
                }
            }

            const ch = this.buffer[this.readPos++];
            if (absoluteNext) {
                // Add this ch no matter what because it was excaped with \
                while (currentSpaces > 0) {
                    currentValue += ' ';
                    currentSpaces --;
                }

                if (ch === 10) {
                    currentValue += '\n';
                } else if (ch === 13) {
                    currentValue += '';
                } else {
                    currentValue += String.fromCharCode(ch);
                }

                absoluteNext = false;
            } 
            // check for new line
            else if ((ch === 10 || ch === 13) && !isQuoteString) { 
                if (currentValue) {
                    currentArray.push(currentValue);
                }

                if (currentLine >= this.skipCount) {
                    const result = this.callbackWithData(currentArray);

                    // Check for a promise
                    if (result && 
                        typeof result === 'object' && 
                        result.next && 
                        typeof result.next === 'function') {
                        console.log(`TabbedFileImportAsync at line ${currentLine} received promise`);
                        await result;
                    }
                }

                currentArray  = [];
                currentValue  = '';
                currentLength = 0;
                currentSpaces = 0;
                currentLine ++;
            } 
            // ignore everything if we are just skipping lines
            else if (currentLine < this.skipCount) {
                continue;
            } 
            // If it's a quote and starts a field then we're in a string
            else if (ch === 34) { 
                if (currentValue.length === 0) {
                    isQuoteString = !isQuoteString;
                } else {
                    if (isQuoteString) {
                        isQuoteString = false;
                    } else {
                        // Single quote in the middle of a string
                        // just add it to the current value
                        currentValue += '"';
                    }
                }
            } 
            // Check for \ which means always add the next char to the current value
            else if (ch === 92 && this.handleEscaped) { 
                absoluteNext = true;
            }
            // Check for a tab
            else if (ch === this.seperator && !isQuoteString) {
                currentArray.push(currentValue);
                currentValue  = '';
                currentLength = 0;
                currentSpaces = 0;
            }
            // All others can be added to the current value
            else {
                // Don't add spaces at the start of the string
                if (ch === 32) {
                    if (currentLength === 0) {
                        continue;
                    }
                    currentSpaces ++;
                    continue;
                }
                
                while (currentSpaces > 0) {
                    currentValue += ' ';
                    currentSpaces --;
                }

                currentValue += String.fromCharCode(ch);
                currentLength ++;
            }
        }

        fs.close(this.fd, () => {/**/});

        if (currentArray.length > 0 && currentLine >= this.skipCount) {
            this.callbackWithData(currentArray);
        }

        // At the end of file, send null to let the client know it's end of the file.
        this.callback(null);

        return true;
    }

    private doReadBuffer() {
        return new Promise((resolve) => {
            fs.read(this.fd, this.buffer, 0, this.sLength, null, (err, readSize, buffer) => {
                this.readPos = 0;
                this.readSize = readSize;

                if (err || !readSize || readSize < 1) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    }

    /**
     * Make the callback with either an array of values
     * or a dictionary of values if hasHeader is set
     */
    private callbackWithData(currentArray: any[]) {
        if (currentArray.length === 0) {
            return [];
        }
        
        if (!this.hasHeader) {
            return this.callback(currentArray);
        }
        
        let result: any;

        if (!this.headers) {
            this.headers = [];
            
            // Check to see if we got the delim wrong
            if (currentArray.length === 1 && /,/.test(currentArray[0])) {
                const reCSVHeader = /(?:\s*(?:\"([^\"]*)\"|([^,]+))\s*,?)+?/g;
                let m = reCSVHeader.exec(currentArray[0]);

                while(m) {
                    if (m[1]) {
                        // matched something in quotes
                        this.headers.push(m[1]);
                    } else {
                        // matched something without quotes
                        this.headers.push(m[2]);
                    }
                    m = reCSVHeader.exec(currentArray[0]);
                }
            } else {
                this.headers = currentArray;
            }

            // Switch to CSV Comma format
            this.seperator = this.headers.length > 0 ? 44 : this.seperator;
        } else {
            let currentValues: any = {};

            for (let x in this.headers as any[]) {
                currentValues[this.headers[x]] = currentArray[x];
            }

            result = this.callback(currentValues);
        }

        return result;
    }
}