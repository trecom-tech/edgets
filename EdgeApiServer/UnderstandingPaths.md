## URL Paths

### Accessing specific apps

Most paths are based on an "Application" or App.   The app is the first part of the URL and should be in a folder called ./Web/*App*,  for example

    http://localhost:8001/Portal/  is  ./Web/Portal/

Within the App folder you need to have:

* screens - The folder that contains the screen definition files
* views - The folder that contains the views for the app

Within a shared ./Web/ folder you can also have a screens and views folder the contains shared views or shared screens that are common to all apps on this server.

### Accessing data

Loading data is based on /dataset/collection/*search condition*.*extension* where the search condition is anything understood by DataSetPath and extensions should be json or xml.

