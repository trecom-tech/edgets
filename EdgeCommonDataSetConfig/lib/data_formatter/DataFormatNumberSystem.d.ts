import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatNumberSystem extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    constructor(...args: any);
    format(data: any, options: any, path: any): string;
    unformat(data: any, path: any): any;
}
export default DataFormatNumberSystem;
