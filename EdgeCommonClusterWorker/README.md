# Cluster Worker
> This is the base class that all other microservice workers should use

    npm install --save "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonClusterWorker"
    import ClusterWorker from 'EdgeCommonClusterWorker';

    class myWorker extends ClusterWorker {
    }


## Internal Functions

### processMessageQueue
Setup a loop to work a message queue.
Given a queue name and consumer count, the callback will be made passing in the job object.
The callback can return true/false, null, or a promise.
Currently there is no difference between true/false returned.

	processMessageQueue(logName, queueName, consumerCount, onMessageCallback);

### verifyLoadAverage
Hard exit if the server is overloaded

    verifyLoadAverage();

### processWorkQueue
Setup a loop to work a workqueue
Given a queue name and consumer count, the callback will be made passing in the job object.
The callback can return true/false, null, or a promise.
Currently there is no difference between true/false returned

    processWorkQueue(logName: string, queueName: string, consumerCount: number, onMessageCallback: any);

### setupLogging
Open a connection to the logging database

    setupLogging();

### setupCounterUpdateLoop
Create a loop that updates the number of messages in the queue once per second.

    setupCounterUpdateLoop();

### reportText
Helper function sends a message to the terminal to be displayed
id is the worker id, message is any text

    reportText(...messages: any[]);

### setupAzureLogging
Logging to the Azure SQL Database

    await setupAzureLogging();

