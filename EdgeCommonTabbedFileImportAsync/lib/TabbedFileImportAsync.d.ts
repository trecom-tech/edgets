export default class TabbedFileImportAsync {
    /**
     * Has a header line
     */
    hasHeader: boolean;
    /**
     * The decimal ascii code for the field seperator
     */
    seperator: number;
    /**
     * The number of bytes to read from the file at once
     */
    sLength: number;
    /**
     * The number of lines to skip
     */
    skipCount: number;
    handleEscaped: boolean;
    /**
     * Filename to import
     */
    private filename;
    private buffer;
    private fd;
    private readPos;
    private readSize;
    private callback;
    private headers;
    constructor(filename: string);
    /**
     * Shortcut function to configure for a CSV file
     */
    setFormatCSVWithHeaders(): void;
    /**
     * Returns a promise that completes once the file completes
     * @param callback - If callback returns a promise then we pause reading until it's done
     */
    readLine(callback: any): Promise<boolean>;
    private doReadBuffer;
    /**
     * Make the callback with either an array of values
     * or a dictionary of values if hasHeader is set
     */
    private callbackWithData;
}
