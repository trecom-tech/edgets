##|
##|  To help with automated testing

class ViewMiller extends View

    getDependencyList: ()=>
        return ["/vendor/miller.min.js"]

    onSetupButtons: () ->

    onShowScreen: ()=>
        @millerColumn = new MillerColumns @elMiller, true

    ##|
    ##|  Add a Data as object to miller columns
    setData: (@optionsData)=>
        if !@millerColumn then return false
        @millerColumn.setData @optionsData
        @millerColumn.render()
        true

    onResize : (w, h)=>
        return

    setOnSelected: (onSelected) =>
        if !@millerColumn then return false
        if onSelected? and typeof onSelected is "function"
            @onSelected = onSelected
            @millerColumn.onSelected = @onSelected
            return true
        false

    setSize: (w, h)=>
        @elHolder.width(w)
        @elHolder.height(h)

    show: (name)=>

    serialize: () =>
        return
            settings: @optionsData
            onSelected: @onSelected

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewMiller: invalid data passed"

            if options.settings?
                @setData options.settings

            if options.onSelected? or options.onselected?
                @setOnSelected (options.onSelected or options.onselected)

            resolve true
