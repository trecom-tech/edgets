$ ->

    addTestButton "Simple Diagram 1", "Open", ()->

        addHolder()
        .setView "Diagram", (viewDiagram)->
            viewDiagram.createRect "Testing"
            console.log "Diagram is setup"

    addTestButton "Simple Diagram with resize option", "Open", ()->

        addHolder()
        .setView "Diagram", (viewDiagram)->
            viewDiagram.createRect "Testing 1"
            viewDiagram.createRect "Testing 2"
            viewDiagram.setAutoFitAfterResize true
            console.log "Diagram is setup"

    addTestButton "Hello World Example", "Open", ()->

        addHolder()
        .setView "Diagram", (viewDiagram)->
            r1 = viewDiagram.createRect "Hello"
            r2 = viewDiagram.createRect "World"
            viewDiagram.createLink r1, r2

    go()



