//  class TableViewCol to create and render single column for the table
//  global functions required to use tables. the cell id is a counter
//  used to create elements with a new unique ID
//

import { DataFormatter } from '../data_formatter/DataFormatter';

const dataFormatter = new DataFormatter();

export class ColumnBase {
    reDate1: RegExp   = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T00.00.00.000Z/;
    reDate2: RegExp   = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9]Z/;
    reNumber: RegExp  = /^[\-1-9][0-9]{1,10}$/;
    reDecimal: RegExp = /^[\-1-9\.][0-9\.]{1,11}\.[0-9]+$/;
    formatter: any;
    data: any;

    constructor() {
        // write constructor code
    }

    getName() {
        return "No name";
    }

    getSource() {
        return "";
    }

    getOrder() {
        return 999;
    }

    getOptions(): null {
        return null;
    }

    getClickable() {
        return false;
    }

    getEditable() {
        return false;
    }

    getAlign(): null {
        return null;
    }

    getWidth() {
        return 0;
    }

    RenderHeader(parent: any, location: any) {
        return parent.html("No RenderHeader");
    }

    RenderHeaderHorizontal(parent: any, location: any) {
        return parent.html("No RenderHeaderHorizontal");
    }

    UpdateSortIcon(newSort: any): null | boolean {
        return null;
    }

    getVisible() {
        return true;
    }

    getType() {
        return "text";
    }

    getFormatter() {
        if (this.formatter) {
            return this.formatter;
        }
        return this.formatter = dataFormatter.getFormatter(this.getType());
    }

    getFormatterName() {
        const f = this.getFormatter();
        if (f != null) {
            return f.name;
        }
        return null;
    }

    onFocus(e: any, col: any, data: any) {
        return true;
    }

    getRequired() {
        return false;
    }

    getAlwaysHidden() {
        return false;
    }

    getSystemColumn() {
        return false;
    }

    getAutoSize() {
        return false;
    }

    getIsCalculation() {
        return false;
    }

    //  -gao
    //  function to get linked field
    // 
    getLinked(): null {
        return null;
    }

    //
    // Return html to display
    // @param value [mixed] Whatever the current value is in database format
    // @param value [mixed] The key value on the row for this record if any
    // @param value [object] The row object if any
    renderValue(value: any, keyValue: any, row: any) {
        return value;
    }

    //
    // Update configuration of the column
    // The varName / value should be the same as defined
    // in the "serialize" function but we allow one update
    // function we can adjust the related fields in the subclasses.
    changeColumn(varName: any, value: any) {
        return true;
    }

    getRenderFunction(): null {
        return null;
    }

    renderTooltip(row: any, value: any, tooltipWindow: any) {
        return false;
    }

    //
    // Given some new data, see if we need to automatically change
    // the data type on this column.
    deduceColumnType(newData: any): null | boolean {
        return null;
    }

    //
    // Called once when the column is created to see if the
    // class wants to update the information on the column type
    deduceInitialColumnType(): null | boolean {
        return null;
    }

    //
    // Flags (permisssions) that are required to view the field.   Should be validated
    // against the current user.   One of the flags in the array should be valid.
    getFlagsRequiredView(): any[] {
        return [];
    }

    //
    // Flags (permissions) that are required to edit the field assuming editing is enabled.
    getFlagsRequriedEdit(): any[] {
        return [];
    }

    //
    // If true (default) show this field in the new form
    getVisibleOnNew() {
        return true;
    }

    getVisibleOnEdit() {
        return true;
    }

    //
    // ------------------------------------ Properties that deal with validation -----------------------------------

    /**
     *     Should a value be validated prior to allowing a new record to be created?
     */
    getValidateOnCreate() {
        return true;
    }

    /**
     * Should a valid be validated prior to allowing a change to the record?
     */
    getValidateOnEdit() {
        return true;
    }

    /**
     * An optional message to show if validation fails.   Only Error or Warning should be returned.
     * an error message should stop edit/create but a wanring should not.
     */
    getValidateErrorMessage(): null {
        return null;
    }

    getValidateWarningMessage(): null {
        return null;
    }

    /**
     * Could be either javascript code or number or a range of numbers of whatever is required for validation.
     * format and value to be determined.
     */
    getValidateOptions(): null {
        return null;
    }

    /**
     *     function used to edit the column
     */
    getEditFunction(): null {
        return null;
    }

    serialize() {

        const obj: any = {};
        obj.name       = this.getName();
        obj.type       = this.getType();
        obj.width      = this.getWidth();
        obj.options    = this.getOptions();
        obj.editable   = this.getEditable();
        obj.visible    = this.getVisible();
        obj.clickable  = this.getClickable();
        obj.align      = this.getAlign();
        obj.source     = this.getSource();
        obj.required   = this.getRequired();
        obj.hideable   = this.getAlwaysHidden();
        obj.system     = this.getSystemColumn();
        obj.autosize   = this.getAutoSize();
        obj.order      = this.getOrder();
        obj.render     = this.getRenderFunction();
        obj.calculate  = this.getIsCalculation();

        // Permissions settings
        //
        obj.flagsview       = this.getFlagsRequiredView();
        obj.flagsedit       = this.getFlagsRequriedEdit();
        obj.visiblenewform  = this.getVisibleOnNew();
        obj.visibleeditform = this.getVisibleOnEdit();

        // Validation settings
        //
        obj.validateoncreate      = this.getValidateOnCreate();
        obj.validateonedit        = this.getValidateOnEdit();
        obj.validate_err_message  = this.getValidateErrorMessage();
        obj.validate_warn_message = this.getValidateWarningMessage();
        obj.validate_options      = this.getValidateOptions();

        // console.log "SERIALIZE:", @data

        if ((this.data.render != null) && (typeof this.data.render === "string") && (this.data.render.charAt(0) === '=')) {
            obj.render = this.data.render;
        }

        return obj;
    }

    deserialize(obj: any) {

        for (const varName in obj) {
            const value = obj[varName];
            this.changeColumn(varName, value);
        }

        return true;
    }
}

export default ColumnBase;