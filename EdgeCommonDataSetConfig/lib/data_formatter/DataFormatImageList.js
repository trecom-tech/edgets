"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatImageList extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'imagelist';
        this.width = 60;
        this.align = 'center';
        this.option = [];
    }
    //
    // funtion to open multiselect editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        let title;
        if ((currentValue == null)) {
            return false;
        }
        let w = $(window).width();
        let h = $(window).height();
        if (w > 1000) {
            w = 1000;
        }
        else if (w > 800) {
            w = 800;
        }
        else {
            w = 600;
        }
        if (h > 1000) {
            h = 1000;
        }
        else if (h > 800) {
            h = 800;
        }
        else if (h > 600) {
            h = 600;
        }
        else {
            h = 400;
        }
        if (typeof currentValue === "string") {
            currentValue = currentValue.split("||");
        }
        const imgCount = currentValue.length;
        if ((imgCount === 1) && (currentValue[0] === "")) {
            return false;
        }
        else if (imgCount === 1) {
            title = "View Image";
        }
        else {
            title = `View ${imgCount} Images`;
        }
        // @ts-ignore
        return doPopupView('ImageStrip', title, 'imagestrip_popup', w, h, (view) => Array.from(currentValue).map((img) => view.addImage(img)));
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(currentValue, options, path) {
        this.options = options;
        let formattedValue = "";
        if (typeof currentValue === "string") {
            currentValue = currentValue.split("||");
        }
        else if ((currentValue == null)) {
            return formattedValue;
        }
        const imgCount = currentValue.length;
        if ((imgCount > 0) && (currentValue[0] !== "")) {
            formattedValue = `${imgCount} <i class='fal fa-image'></i>`;
        }
        return formattedValue;
    }
    //
    // funtion to unformat the currently unformatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(currentValue, path) {
        if (typeof currentValue === "string") {
            currentValue = currentValue.split('||');
        }
        return currentValue;
    }
    //
    // render a tooltip when getting mouse-hovered
    renderTooltip(row, value, tooltipWindow) {
        let height;
        let width;
        let str = "No Images";
        if (typeof value === "string") {
            value = value.split("||");
        }
        if ((value.length > 0) && (value[0] !== "")) {
            str = value[0];
            width = 200;
            height = 200;
            tooltipWindow.html(`<div class='ninjaImageTooltip'><img src='${str}' width=200 /></div>`);
        }
        else {
            // @ts-ignore
            width = textWidth(str) + 20;
            height = 40;
            tooltipWindow.html(`<div class='text'>${str}</div>`);
        }
        tooltipWindow.setSize(width, height);
        return true;
    }
}
exports.DataFormatImageList = DataFormatImageList;
exports.default = DataFormatImageList;
//# sourceMappingURL=DataFormatImageList.js.map