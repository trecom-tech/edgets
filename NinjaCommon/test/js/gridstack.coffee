$ ->

	addTest "Loading Zipcodes", ()->
		loadZipcodes()
		
	addTestButton "GridStack basic", "Open", (e)->
		addHolder().setView "Grid", (view) ->
			view.addWidget()

		return true

	addTestButton "GridStack SetName", "Open", (e)->
		addHolder().setView "Grid", (view) ->
			view.addWidget()
			view.setName "test1" 

		return true

	addTestButton "GridStack Set Padding", "Open", (e)->
		addHolder().setView "Grid", (view) ->
			view.setPadding 50
			view.addWidget()
			view.setName "test1"

		return true

	addTestButton "GridStack Disable Resize", "Open", (e)->
		options =
			name: "GridStack1"
			width: 6
			height: 6

		addHolder().setView "Grid", (view) ->
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize is added", viewTestShowSize
			.then () ->
				view.enableResize options.name, false

		return true

	addTestButton "GridStack AddView with Options", "Open", (e)->
		options = {
			name: "GridStack1"
			width: 6
			height: 6
		}
		addHolder().setView "Grid", (view) ->
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize is added", viewTestShowSize
		return true

	addTestButton "GridStack Add 2 Views", "Open", (e)->
		options =
			name: "GSwithOptions"
			x: 4
			y: 2
			width: 4
			height: 4

		addHolder().setView "Grid", (view) ->
			view.addView "TestShowSize", (viewTestShowSize) ->
				console.log "ViewTestShowSize is added"
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize with Options is added"
		return true

	addTestButton "GridStack Cell Height", "Open", () ->
		options =
			name: "GSwithOptions"
			x: 4
			y: 2
			width: 4
			height: 4

		addHolder().setView "Grid", (view) ->
			view.addView "TestShowSize", (viewTestShowSize) ->
				console.log "ViewTestShowSize is added"
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize with Options is added"
			console.log "Grid Cell Width, Height: ", view.getCellWidth(), view.getCellHeight()
			setTimeout () ->
				view.setCellHeight 100
			, 3000
		return true

	addTestButton "GridStack Grid Width", "Open", () ->
		options =
			name: "GSwithOptions"
			x: 4
			y: 2
			width: 4
			height: 4

		addHolder().setView "Grid", (view) ->
			view.addView "TestShowSize", (viewTestShowSize) ->
				console.log "ViewTestShowSize is added"
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize with Options is added"
			console.log "Grid Cell Width, Height: ", view.getCellWidth(), view.getCellHeight()
			setTimeout () ->
				view.setGridWidth 6
			, 3000
		return true


	addTestButton "GridStack containing Table", "Open", (e) ->
		option1 = 
			name: "ZipcodeTable"
			x: 4
			y: 2
			width: 6
			height: 6

		option2 =
			name: "Empty Table"
			x: 0
			y: 0

		addHolder().setView "Grid", (view) ->
			view.addView "Table", option1, (viewTable) ->
				viewTable.addTable "zipcode"
				viewTable.setTitle "Zipcode"
				true
			view.addView "Table", option2, (viewTable) ->
				viewTable.addTable "invalid"
				viewTable.setTitle "Empty Table"
				true
		true

	addTestButton "GridStack in PopupWindow", "Open", (e)->
		options =
			name: "GridStack1"
			width: 6
			height: 6

		doPopupView "Grid", "Grid Stack View", "PopupGridView1", 1000, 800, (view) ->
			view.addView "TestShowSize", options, (viewTestShowSize) ->
				console.log "ViewTestShowSize is added onto PopupWindow", viewTestShowSize

		return true

	addTestButton "GridStack in tabs", "Open", (e)->
		options = {
			name: "GridStack1"
			width: 6
			height: 6
		}
		addHolder()
		.setView "DynamicTabs", (viewTabs) ->
			viewTabs.doAddViewTab "Grid", "GridStackTab", (view)->
				view.addView "TestShowSize", options, (viewTestShowSize) ->
					console.log "ViewTestShowSize is added in Tab", viewTestShowSize
			viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
		return true

	addTestButton "GridStack deserialize", "Open", () ->
		addHolder()
		.setView "Grid", (view) ->
			view.deserialize
				padding: 50
				handlericon: "<i class='far fa-lightbulb'></i>"
				grids: [
					title: "Grid 1"
					x: 4
					y: 2
					width: 4
					height: 4
					view: "TestShowSize"
				,
					title: "Grid 2"
					x: 6
					y: 6
					width: 6
					height: 6
					disable_resize: true
				]

			setTimeout () ->
				console.log "Serialized data: ", view.serialize()
				doPopupView "Grid", "Grid Stack View", "popup-gridview-2", 1000, 800, (viewPopup) ->
					viewPopup.deserialize view.serialize()
			, 2000

	go()
