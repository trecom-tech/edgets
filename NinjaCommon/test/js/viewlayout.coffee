$ ->

    addTest "Loading Data", ()->
        DataMap.setDataTypes "offer_table", [
                name: "Create Date"
                source: "create_date"
                visible: true
                type: "datetime"
                editable: true
            ,
                name: "Record Type"
                source: "record_type"
                visible: true
                editable: true
                type: "enum"
                options: ["Counter Received", "Offer Sent", "Something else"]
            ,
                name: "Documented"
                source: "documented"
                visible: true
                editable: true
                type: "enum"
                options: ["Dropbox", "GDrive", "OneDrive"]
            , 
                name: "Offer Notes"
                source: "offer_notes"
                visible: true
                editable: true
                type: "text"
            ,
                name: "Offer Amount"
                source: "offer_amount"
                visible: true
                editable: true
                type: "decimal"
            ,
                name: "Other Field1"
                source: "other1"
                visible: true
                editable: true
                type: "text"
            ,
                name: "Other Field2"
                source: "other2"
                visible: true
                editable: false
                type: "text"
            ,
                name: "Other Field3"
                source: "other3"
                visible: true
                editable: false
                type: "text"
            ]
            
        DataMap.addData "offer_table", 1, 
            create_date: "2018-03-01"
            record_type: "Offer Sent"
            documented : "Dropbox"
            offer_notes: "This offer is the first one"
            offer_amount: 1.234
            other1: "This is the first other field"
            other2: "This is the second other field"
            other3: "This is the third other field"
        true    

    ##|
    ##|  This is just for diagnostics,  you don't need to verify the data map is
    ##|  loaded normally.  The data types should be loaded upon startup.
    addTest "Confirm Zipcodes datatype loaded", () ->
        dm = DataMap.getDataMap()
        if !dm? then return false

        zipcodes = dm.types["zipcode"]
        if !zipcodes? then return false
        if !zipcodes.col["code"]? then return false

        true

    ##|
    ##|  Load the zipcodes JSON file.
    ##|  This will insert the zipcodes into the global data map.
    addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

    addTestButton "Simple 'String' view name", "Open", ()->
        addHolder()
        .setView "TestLayout1", (view)->
            console.log "Simulate getLayout return single string"
            view.runTest "TestShowSize"

    addTestButton "Error, doesn't have a view name", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                badName: "TestShowSize"


    addTestButton "Simple 'Object' view name", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view: "TestShowSize"
    addTestButton "Table with Sort", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view:       "Table"
                collection: "zipcode"
                sort:       "city a"

    addTestButton "Table with Sort and Groupby", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view:       "Table"
                collection: "zipcode"
                sort:       "city a"
                groupby:    "county"     

    addTestButton "Table Set Checkbox and Flags", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view:             "Table"
                collection:       "zipcode"
                enablecheckboxes: true 
                checkboxlimit:    5 
                flags: [
                    name: "FlagBook"
                    icon: "fa fa-book"
                ,
                    name: "FlagBan"
                    icon: "fa fa-ban"
                ]

    addTestButton "DataBound Form on Grid", "Open", () ->
        addHolder()
        .setView "TestLayout1", (view) ->
            view.runTest
                view: "Grid"
                grids: [
                    title: "Grid Form"
                    width: 6
                    height: 6
                    disable_resize: true
                    view: "Form"
                    render_from_table: "offer_table"
                    idvalue: 1
                    valid_fields: ["create_date", "record_type", "documented", "offer_notes", "offer_amount", "new_text_field"]
                ]

    go()
