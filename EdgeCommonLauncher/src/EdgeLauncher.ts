import * as os            from 'os';
import * as fs            from 'fs';
import * as moment        from 'moment';
import chalk              from 'chalk';
import {
    config,
    DelayedWinstonLog
}                         from 'edgecommonconfig';
import DataSetManager     from 'edgedatasetmanager';
import { spawn }          from 'child_process';
import NinjaDebug         from 'ninjadebug';
import { parse, quote }   from 'shell-quote';
import { NpmModuleTools } from './NpmModuleTools';

if (process.env.NODE_ENV === 'test') {
    config.setCredentials('redisReadHost', `${process.env.REDIS_HOST}`);
    config.setCredentials('redisHostWQ', `${process.env.REDIS_HOST}`);
    config.setCredentials('redisHost', `${process.env.REDIS_HOST}`);
    config.setCredentials('MongoDB', {
        url    : `${process.env.DB_HOST}`,
        options: {
            connectTimeoutMS: 600000,
            poolSize        : 16,
            socketTimeoutMS : 600000
        }
    });
}

export class EdgeLauncher {
    private api: any;
    private basePath: string;
    // private db                 : any;
    // private dbLogs             : any;
    private emitter: any;
    private homePath: string;
    private log: DelayedWinstonLog;
    private statusInterval: NodeJS.Timeout;
    private statusUpdateClient: any;
    private saveResult: any;
    private timer: any;
    private static saveResult: any;

    constructor() {
        let pathParts = __dirname.split("/");
        pathParts.pop();
        //  Set this in the calling code if you want stats to be logged
        this.api      = null;
        this.basePath = pathParts.join("/") + "/";
        // this.db                 = new DataSetManager('os');
        // this.dbLogs             = new DataSetManager('logs');
        this.homePath           = process.env.HOME;
        this.log                = config.getLogger("JobRunner");
        //  Set this in the calling code if you want status updates
        this.statusUpdateClient = null;
        //  The result of the job from the server, set after the job is doUpsertOne
        // this.saveResult         = null;
    }

    /**
     *
     * Any job can output one or more "Results=<Number>" lines and this
     * will total up those numbers and add them to the record for reporting.
     *
     * @param job
     * @param command
     * @param fullOutput
     * @param slot_id
     */
    async insertJobLog(job: any, command: any, fullOutput: any, slot_id: any): Promise<boolean> {
        let db         = new DataSetManager('os');
        let dbLogs     = new DataSetManager('logs');
        let total: string | number;
        const reResult = new RegExp("Results*=(.*)", "g");
        let m          = reResult.exec(fullOutput);

        config.status(`JobRunner::insertJobLog saving slot ${slot_id}`);

        if (fullOutput.indexOf("JobRunner: Disable") !== -1) {
            //  Job should be disabled in the future
            job.enabled = false;
            try {
                await db.doUpdate(`/job/${job.id}`, job);
            } catch (err) {
                this.log.error(err);
            }
        }
        total = 0;
        while (m != null) {
            // config.dump "Match Result=", m
            if (m[1] != null) {
                let amount = parseFloat(m[1]);
                if (!isNaN(amount)) {
                    total += amount;
                }
            }
            m = reResult.exec(fullOutput);
        }
        if (total == null) {
            total = "";
        }
        if (/Exception in/.test(fullOutput)) {
            total = "Exception";
        }

        //  Save a job log record
        const job_id = job.id.slice(0, 5) + new Date().getTime();
        try {
            this.saveResult = await dbLogs.doInsert(
                `/jobrun/${job_id}`,
                {
                    id           : job_id,
                    title        : job.title,
                    command      : command,
                    duration     : job.lastDuration,
                    output       : fullOutput,
                    total        : total,
                    job_date     : new Date(),
                    _lastModified: new Date()
                },
                1);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }

    }

    /**
     *
     * job is an object as defined in "UnderstandingJobs.md"
     * slot_id is any number that is used for reporting and also
     * used to help the real-time status boards keep track of jobs
     *
     * @param job
     * @param slot_id
     */
    async runJob(job: any, slot_id: any) {
        let clearTimer: any;
        let jobExec: any;
        let jobStatus: any;
        let stat: any;
        let timeout: any;
        let totalBytes: any;
        let args: any;

        let db     = new DataSetManager('os');
        let dbLogs = new DataSetManager('logs');

        config.status("JobRunner::runJob starting");
        if (this.api != null) {
            //
            //  Log that we're running this job today
            this.api.stats_doAddStats("today", "RunJob", job.title, 1);
        }
        //
        //  Mark the job so it doesn't restart in another thread
        job.pending      = false;
        job.lastRun      = new Date();
        job.lastDuration = 0;

        //  remember this is running async
        await db.doUpdate(`/job/${job.id}`, job);
        let commandLine = "node";
        let path        = this.basePath + job.folder;
        if (/npm/.test(job.script)) {
            //  handle npm scripts
            //  TODO: use path instead of undefined, cannot
            //  resolve exception: "Error: spawn npm ENOENT" right now
            console.log("INSTALLING:", job.script);
            const result = await NpmModuleTools.verifyInstalled(job.npm, job.folder);
            return result;
        } else {
            args = parse(job.args);
            for (let n = 0; n < args.length; n++) {
                args[n] = args[n].replace(/^'([^']+)'$/, "$1");
            }
            if (/\.sh/.test(job.script)) {
                //  handle shell scripts
                commandLine = "/bin/sh";
                args.splice(0, 0, job.script);
            } else if (/\.pl/.test(job.script)) {
                //  handle perl scripts
                commandLine = "perl";
                args.splice(0, 0, job.script);
            } else if (/\.js/.test(job.script)) {
                commandLine = "node";
                args.splice(0, 0, job.script);
                args.splice(0, 0, "--max-old-space-size=8192");
                args.splice(0, 0, "--nouse-idle-notification");
            } else {
                // everything else is a node command
                args.splice(0, 0, job.script);
                args.splice(0, 0, "/usr/local/bin/coffee");
                args.splice(0, 0, "--max-old-space-size=8192");
                args.splice(0, 0, "--nouse-idle-notification");
            }
            if (this.statusUpdateClient != null) {
                //
                //  Tell the realtime server we are starting the job
                jobStatus = this.statusUpdateClient.startJob(job.title, args, slot_id);
            }
            this.log.info("Starting job", {
                job : job,
                slot: slot_id,
                path: this.basePath + job.folder,
                args: args
            });
            try {
                let fullOutput     = "";
                let jobFilenameRaw = os.hostname() + "_" + moment().format("hh-mm-ss") + "_" + slot_id + "_" + job.title;
                jobFilenameRaw     = jobFilenameRaw.replace(/[^a-zA-Z0-9]+/g, "_");
                jobFilenameRaw     = jobFilenameRaw.replace(/__+/g, "_");
                jobFilenameRaw     = jobFilenameRaw.replace(/_+$/, "");
                fullOutput         = config.getDataPath(`logs/job_output_${moment().format("YYYY-MM-DD")}/${jobFilenameRaw}.txt`);
                let jobOutput      = fs.createWriteStream(fullOutput);
                totalBytes         = 0;
                jobOutput.write("Path: " + path + "\n");
                jobOutput.write("Command : " + commandLine + " " + "\n\n");
                let startTime = new Date().getTime();
                //  Spawn the job
                const envCopy = {
                    TERM   : "HTML",
                    HOME   : this.homePath,
                    LANG   : "en_US.UTF-8",
                    LOGNAME: "www",
                    PATH   : "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin",
                    SHELL  : "/bin/bash",
                    SHLVL  : "1",
                    TMPDIR : "/tmp/",
                    USER   : "www",
                    TOOL   : "job"
                };

                stat = null;
                try {
                    stat = fs.lstatSync(path);
                } catch (error) {
                    config.reportError('Invalid path 1:', error);
                    config.status("Invalid path 1:" + error.toString());
                    stat = null;
                }
                if (stat === null || !stat.isDirectory()) {
                    //
                    //  Invalid folder skip the job
                    fullOutput       = "Invalid folder: " + path;
                    job.pending      = false;
                    job.lastRun      = new Date();
                    job.lastDuration = job.lastRun.getTime() - startTime;
                    await db.doUpdate(`/job/${job.id}`, job);

                    let command;
                    command = commandLine + " " + quote(args);
                    this.insertJobLog(job, command, fullOutput, slot_id);

                    config.status(`Invalid folder [${path}]`);

                    return true;
                }
                if (slot_id != null) {
                    //
                    // Output to the console the status of the job every 5 seconds
                    this.statusInterval = setInterval(function () {
                        let currentTime    = new Date().getTime();
                        let duration       = currentTime - startTime;
                        duration /= 1000;
                        let lastData       = 0;
                        let lastDataTime   = currentTime - job.lastDataReceived.getTime();
                        lastDataTime       = Math.round(lastDataTime / 1000);
                        const customFormat = '#,###.[####]';
                        let txt            = NinjaDebug.pad(totalBytes, 10, chalk.white, customFormat) + " bytes | ";
                        txt += NinjaDebug.pad(Math.floor(duration), 6, chalk.cyan, customFormat) + " sec | ";
                        if (lastDataTime > 20) {
                            txt += NinjaDebug.pad(lastDataTime, 6, chalk.cyan, customFormat) + " sec ago | ";
                        } else {
                            txt += NinjaDebug.pad(lastDataTime, 6, chalk.cyan, customFormat) + " sec ago | ";
                        }
                        if (lastDataTime > 60 * 20) {
                            console.log("** JOB TIMEOUT **");
                            jobExec.kill('SIGHUP');
                        }
                        if (job.timeout != null) {
                            let timeoutRemaining = job.timeout - lastDataTime;
                            if (timeoutRemaining < 100) {
                                //
                                //  Kill job
                                console.log("** JOB TIMEOUT **");
                                jobExec.kill('SIGHUP');
                            }
                        }
                        if (job.args) {
                            return console.log(`${slot_id} : ` + txt + " " + job.script + " " + job.args);
                        } else {
                            return console.log(`${slot_id} : ` + txt + " " + job.script);
                        }
                    }, 5000);
                }
                jobExec = null;
                // args.push "--trace"
                jobExec = spawn(commandLine, args, {
                    cwd: path,
                    env: envCopy
                });
                if (!job.timeout) {
                    job.timeout = 1800000;
                }
                this.timer = null;
                timeout    = function () {
                    return job.lastDataReceived = new Date();
                };
                // console.log "-* timeout() on ", job.id
                // ## -1 is used to never timeout
                // if job.timeout != -1
                //     clearTimeout(@timer)
                //     close = () ->
                //         console.log "-* timeout() CLOSE CLOSE CLOSE on ", job.id
                //         config.status "TIMEOUT on Job ", job.id
                //         jobExec.kill 'SIGINT'
                //         resolve(false)

                //     @timer = setTimeout close, job.timeout

                /* tslint:disable:no-empty */
                clearTimer = function () {
                };
                // if job.timeout != -1
                // clearTimeout(@timer)

                //
                //  Mark the job as running
                job.pending          = false;
                job.lastRun          = new Date();
                job.lastDataReceived = new Date();
                job.lastDuration     = -1;
                // this.db.doUpdate(`/job/${job.id}`, job);

                return new Promise((resolve: Function, reject: Function) => {
                    jobExec.on("error", (err: any) => {
                        clearTimer();
                        console.log(`-> JobError in command=${commandLine} args=`, args);
                        console.log("-> Path  : ", path);
                        console.log("-> Error : ", err);
                        resolve(false);
                    });
                    jobExec.stdout.on("data", (data: any) => {
                        if (data != null) {
                            timeout();
                            totalBytes += data.length;
                            jobOutput.write(data);
                            if (jobStatus != null) {
                                jobStatus.data.numproc = totalBytes;
                            }
                            if (config.traceEnabled) {
                                console.log("[[" + data.toString() + "]]");
                            }
                        }
                        return true;
                    });
                    jobExec.stderr.on("data", (data: any) => {
                        if (data != null) {
                            timeout();
                            fullOutput += "\n";
                            fullOutput += data.toString();
                            jobOutput.write(data);
                            totalBytes += data.length;
                            if (config.traceEnabled) {
                                console.log("*[" + data.toString() + "]*");
                            }
                        }

                        return true;
                    });

                    jobExec.on("close", (code: any) => {
                        clearTimer();
                        jobOutput.close();
                        job.pending      = false;
                        job.lastRun      = new Date();
                        job.lastDuration = job.lastRun.getTime() - startTime;
                        if (this.statusInterval != null) {
                            clearInterval(this.statusInterval);
                            this.statusInterval = null;
                        }
                        if (jobStatus != null) {
                            jobStatus.stop();
                        }
                        return db.doUpdate(`/job/${job.id}`, job)
                            .then(() => {
                                let command;
                                command = commandLine + " " + quote(args);
                                this.insertJobLog(job, command, fullOutput, slot_id);
                                resolve(true);
                            });
                    });
                })
            } catch (error) {
                console.log("Spawn Issue:", error);
                config.reportException("Spawn issue:", error);

                return {
                    error: "Exception in job",
                    e    : error
                };
            }
        }
    }

}

export default EdgeLauncher;
