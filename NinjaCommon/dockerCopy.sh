#!/bin/sh
echo Copying CoffeeNinja from container...

# docker build -t CoffeeNinja:build . -f Dockerfile.build
docker create --name extract xgao69/coffee-ninja
docker cp extract:root/app/CoffeeNinjaCommon ./CoffeeNinjaCommon  
docker rm -f extract
