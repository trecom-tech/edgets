# Edge Server Setup
> This document provides detailed server install instructions for a secure Ubuntu Linux server with configuation to run the Edge Server software.

## Setting up the new Linux Ubuntu Server

I'm using Ubuntu Linux 16.x LTS (Long Term Support).  This is in the most common server configurations for the Internet and available on every major cloud provider.     Prior to following this document, make sure you can login to your virtual server and upload your SSH private key file.

The first commands you should run on any new server:

	sudo -s
	apt-get update 
	apt-get upgrade -y
	
Install basic build tools

	apt-get install -y git g\+\+ make build-essential
	
Install the basic fail2ban server that blocks automated password crackers

	apt-get install -y fail2ban

## MongoDB

Deny everyone access to port 27017
	
	ufw deny from 0.0.0.0 to any port 27017

Open the firewall for specific IPs - not limited to port because of other services we bring up
By default we added Azure VPN, so everyone connected through company VPN will have access

	ufw allow from 1.1.10.11

Check firewall status

	ufw status
	
Ubuntu requires an authentic PGP Key so add that:

	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

Now add the repo from the official source

	echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
	
Install the package

	sudo apt-get update
	sudo apt-get install -y mongodb-org
	
We need to enable MongoDB as a service so edit the config file and paste in the configuration

	sudo vim /etc/systemd/system/mongodb.service
		
	[Unit]
	Description=High-performance, schema-free document-oriented database
	After=network.target
	
	[Service]
	User=mongodb
	ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf
	
	[Install]
	WantedBy=multi-user.target
	
Start the MongoDB manually

	sudo systemctl start mongodb
	
Check that it's running

	sudo systemctl status mongodb
	
	● mongodb.service - High-performance, schema-free document-oriented database
	   Loaded: loaded (/etc/systemd/system/mongodb.service; disabled; vendor preset: enabled)
	   Active: active (running) since Fri 2017-05-05 12:41:06 UTC; 33s ago
	 Main PID: 35197 (mongod)
	    Tasks: 19
	   Memory: 30.0M
	      CPU: 168ms
	   CGroup: /system.slice/mongodb.service
	           └─35197 /usr/bin/mongod --quiet --config /etc/mongod.conf
	
Finally we can tell the system to enable it by default on reboot

	sudo systemctl enable mongodb
	
To secure the server, use the "mongo" command line tool with the following commands:

	# mongo admin	
	MongoDB shell version: 3.2.13
	connecting to: admin
	
	> var user = { "user" : "sa", "pwd" : "edge5516789", roles: [ { "role" :    "userAdminAnyDatabase", "db" : "admin" }]}
	> db.createUser(user)
	
	Successfully added user: {
		"user" : "sa",
		"roles" : [
			{
				"role" : "userAdminAnyDatabase",
				"db" : "admin"
			}
		]
	}
	
	> db.auth("sa", "edge5516789")
	1
	
	> db.grantRolesToUser("sa", [ "root" ]);
	> db.createUser({ user: "edgeserver", pwd: "edgeserver554412", roles: [ { role: "readWriteAnyDatabase", db: "admin" } ] })
	
	> exit
	
	# service mongodb restart
	
Now you can try the connection with the new credentials

	mongo admin -u edgeserver -p edgeserver554412
	> use testdata
	> db.stats()
	
	{
		"db" : "testdata",
		"collections" : 1,
		"objects" : 1,
		"avgObjSize" : 75,
		"dataSize" : 75,
		"storageSize" : 16384,
		"numExtents" : 0,
		"indexes" : 1,
		"indexSize" : 16384,
		"ok" : 1
	}

### Security

After we created admin user, uncomment security in /etc/mongod.conf and add authorization: enabled
	
	security:
  		authorization: enabled

### Custom configuration
> These are optional configurations based on how Edge suggests them.

	vim /etc/mongod.conf
	
The default storage path is /var/lib/mongodb, if you have another data drive as 
suggested, mount it as /data and use

	dbpath=/data/
	
Edge is designed for replication, so setup the replication:

	replySet=edge01
	oplogSize=4096
	
It's best to change the port to allow outside connections or debugging and installing will be very difficult.

	bindip=0.0.0.0
	
For replication you'll want a key files

	keyFile=/etc/mongodb-keyfile
	
	mv ~/mongodb-keyfile /etc/
	chmod 600 /etc/mongodb-keyfile
	mkdir /data/db
	chown mongod.mongod /data/db
	chown mongod.mongod /etc/mongodb-keyfile

### Disable transparent Huge Page on Ubuntu

> Follow documentation: 

	https://docs.mongodb.com/manual/tutorial/transparent-huge-pages/

## RabbitMQ

Install the Rabbit Message Queue server

	apt-get install -y rabbitmq-server

Setup the web management plugin

	rabbitmq-plugins enable rabbitmq_management

Edge uses RabbitMQ a lot so open the config file and uncomment or edit the ulimit line

	vim /etc/default/rabbitmq-server
	ulimit -S -n 4096

Start the server

	systemctl start rabbitmq-server

Add 2 users, the "sa" or master user and the "edge" user for accessing through the API.

	rabbitmqctl add_user sa sa0000001
	rabbitmqctl set_user_tags sa administrator
	rabbitmqctl set_permissions -p / sa ".*" ".*" ".*"
	rabbitmqctl add_user edge Edge000199
	rabbitmqctl set_permissions -p / edge ".*" ".*" ".*"

	ln -s /var/lib/rabbitmq/mnesia/rabbit@lgv3mq-plugins-expand/rabbitmq_management-3.5.7/priv/www/cli/rabbitmqadmin /usr/local/bin
	rabbitmqadmin declare exchange name=status-updates type=fanout durable=false
	rabbitmqadmin declare exchange name=all-updates type=fanout durable=false

Change the bind host if needed, the test server was ipv6 so I forced ipv4

	vim /etc/rabbitmq/rabbitmq-env.conf
	
	NODE_IP_ADDRESS=0.0.0.0

Check to make sure it's live:

	 rabbitmqctl status
	 
Accessing from the web

RabbitMQ has a web interface available once you run the above command to enable the management plugin.  This will be on port 15672.    Given the firewall,  I use an SSH tunnel on my local machine to be able to access the web admin port.

* [Codinn's SSH Tunnel for Max](https://codinn.com/)
* [SSH Tunnel with Putty for Windows](https://www.electrictoolbox.com/putty-create-ssh-port-tunnel/)

	-L 127.0.0.1:15677 127.0.0.1:15672

Open the management console in your local browser.  Note that that "sa" account and password where created above with the rabbitmqctl command.

	http://localhost:15677/
	user: sa
	pass: sa0000001
		 
##  Install Elastic 

> Elastic Search is an awesome document based search engine that can be used with
Edge Server to allow auto complete searches and other types of searching.

Elastic requires a Java Runtime so install it this way

	sudo apt-get install default-jre
	
Elastic needs to be downloaded and installed
	
	wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.0.deb 
	dpkg -i elasticsearch-5.2.0.deb

Enable the service config it comes with

	systemctl enable elasticsearch.service

Open the config file and change a few things:

	vim /etc/elasticsearch/elasticsearch.yml
	 
	cluster.name: edgeserver
	node.name: edge01
	
Install the nginx server as a front end for Elastic and Edge

	apt-get install -y nginx
	
Edit the config

	vim /etc/nginx/sites-enabled/default	

	server {
	        listen *:9201;
	        access_log /var/log/nginx/elastic.log;
	        error_log /var/log/nginx/elastic.error.log;
	        location / {
	                proxy_pass http://127.0.0.1:9200;
	                proxy_set_header Host $host;
	                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	                auth_basic "Restricted";
	                auth_basic_user_file "/etc/nginx/elastic_users.htpasswd";
	        }
	}	
	
Set a password

	sudo apt-get install apache2-utils
	htpasswd -c /etc/nginx/elastic_users.htpasswd sa
	
Restart servers

	service elasticsearch restart
	service nginx restart	
	
Check that it's running

	systemctl status elasticsearch
	
Check that Elastic is running from localhost on port 9200 (no auth needed)

	curl http://localhost:9200/
	
	{
	  "name" : "edge01",
	  "cluster_name" : "edgeserver",
	  "cluster_uuid" : "JV5fa926S_GrXP50tv6Edw",
	  "version" : {
	    "number" : "5.2.0",
	    "build_hash" : "24e05b9",
	    "build_date" : "2017-01-24T19:52:35.800Z",
	    "build_snapshot" : false,
	    "lucene_version" : "6.4.0"
	  },
	  "tagline" : "You Know, for Search"
	}
	
	curl http://localhost:9201/
	
	<html>
	<head><title>401 Authorization Required</title></head>
	
## Install InfluxDB
> InfluxDB is a time series database 

Add apt repository

	curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
	source /etc/lsb-release
	echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list

Install influxdb

	apt-get update && sudo apt-get install influxdb

Enable to run service on boot and start

	systemctl enable influxdb.service
	systemctl start influxdb

Edit auth-enabled under /etc/influxdb/influxdb.conf to true:

	vim /etc/influxdb/influxdb.conf

	[http]
	# Determines whether HTTP authentication is enabled.
	auth-enabled = true

Now we need to add new admin user:

	influx
	> create user admin with password 'password' with all privileges
	> create user admin with password '5Ssb1ARQWo' with all privileges
	> create user sa with password '5Ssb1ARQWo' with all privileges

Restart service

	systemctl restart influxdb

## Install REDIS

Install the default redis package 

	apt-get install -y redis-server

Configure it to listen on all ports and add a password

	vim /etc/redis/redis.conf
	
	tcp-keepalive 60
	bind 0.0.0.0
	requirepass 8d1b846a8b9c46e9e1562733cd483f19
	maxmemory-policy allkeys-lru
	appendonly yes
	appendfilename redis-staging-ao.aof
	
If you are going to setup a slave you'll need:

	slaveof somehostname.com 6379
	repl-backlog-size 10mb
	masterauth 8d1b846a8b9c46e9e1562733cd483f19
	
Restart the service

	service redis-server restart

	● redis-server.service - Advanced key-value store
	   Loaded: loaded (/lib/systemd/system/redis-server.service; enabled; vendor preset: enabled)
	   Active: active (running) since Fri 2017-05-05 13:04:57 UTC; 13s ago
	     Docs: http://redis.io/documentation,
	           man:redis-server(1)
	  Process: 43682 ExecStopPost=/bin/run-parts --verbose /etc/redis/redis-server.post-down.d (code=exited, status=0/SUCCESS)
	  Process: 43677 ExecStop=/bin/kill -s TERM $MAINPID (code=exited, status=0/SUCCESS)
	  Process: 43672 ExecStop=/bin/run-parts --verbose /etc/redis/redis-server.pre-down.d (code=exited, status=0/SUCCESS)
	  Process: 43699 ExecStartPost=/bin/run-parts --verbose /etc/redis/redis-server.post-up.d (code=exited, status=0/SUCCESS)
	  Process: 43695 ExecStart=/usr/bin/redis-server /etc/redis/redis.conf (code=exited, status=0/SUCCESS)
	  Process: 43690 ExecStartPre=/bin/run-parts --verbose /etc/redis/redis-server.pre-up.d (code=exited, status=0/SUCCESS)
	 Main PID: 43698 (redis-server)
	    Tasks: 3
	   Memory: 6.2M
	      CPU: 26ms
	   CGroup: /system.slice/redis-server.service
	           └─43698 /usr/bin/redis-server 0.0.0.0:6379
	           
## Install Node JS

	apt-get install nodejs
	
What you have now is a really old version, something like v4.2.6 and it's named
"nodejs" instead of node.   So we'll upgrade

	apt-get install npm
	npm install -g n
	n stable
	
Verify it is installed correctly

	node --version
	v7.10.0	
	
Now install common global packages

	npm install -g coffee-script

	/usr/local/bin/coffee -> /usr/local/lib/node_modules/coffee-script/bin/coffee
	/usr/local/bin/cake -> /usr/local/lib/node_modules/coffee-script/bin/cake
	/usr/local/lib
	└── coffee-script@1.12.5

Setup a shortcut command called "c"

	cat > /usr/local/bin/c
	#!/bin/bash

	ulimit -n 9999
	if [[ "$1" =~ coffee$ ]];
	then
	    node --max-old-space-size=8192 --nouse-idle-notification /usr/local/bin/coffee "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
	else
	    node --max-old-space-size=8192 --nouse-idle-notification /usr/local/bin/coffee "$1.coffee" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
	fi

	chmod 755 /usr/local/bin/c
		
## Setting up the Edge user

Create the new user "edge" on the server and give it a strong password.

	adduser edge
	
Switch to the edge user, ignore the "no job control" message

	su edge -
	id
	
	uid=1001(edge) gid=1001(edge) groups=1001(edge)
	
Generate your SSH Key

	ssh-keygen -t rsa
	
	Generating public/private rsa key pair.
	Enter file in which to save the key (/home/edge/.ssh/id_rsa):
	Created directory '/home/edge/.ssh'.
	Enter passphrase (empty for no passphrase):
	Enter same passphrase again:
	Your identification has been saved in /home/edge/.ssh/id_rsa.
	Your public key has been saved in /home/edge/.ssh/id_rsa.pub.

Now that you have the SSH key, you need to add it to the GitHub account that you
want to be the master service account.  

> Make sure to copy the entire output without line breaks.   GitLab requires the full and complete SSH key.

	cat ~/.ssh/id_rsa.pub

Login to your GitLab services account, select the profile image in the top right.  Select Settings and then in the next screen select SSH Keys.

The primary tool required to run Edge is called "Jobs".   The "Jobs" tool will automatically download and install the appropriate sub packages from Git as needed and update them automatically.    Initial setup:

	cd
	mkdir ~/EdgeConfig
	mkdir ~/EdgeData
	mkdir ~/EdgeData/logs
	mkdir ~/EdgeData/images
	
Setting up credentials for Edge must be done off-server.   This is a semi-secure file
that is encrypted locally and contains all the logins and passwords for the given services
to connect to.  This avoids having to store credentials to databases and other things
in a git server or with specific developers.

> Instructions and sample configuration files are in the "EdgeConfig" folder
in the "EdgeCommonConfig" sub project.

	scp key.txt credentials.json edge@server:EdgeConfig/

Once the credentials are installed you are ready to clone the main Jobs project.

> Pro tip:  I suggest running the Edge app in the byobu shell (screens) until
final production.  This allows logout and seamless resume with console.

	byobu
	byobu-enable
	
	Ctrl+a c  -- Creates a new screen
	Ctrl+a n  -- Toggles between screens
	
Clone the code

	git clone git@gitlab.protovate.com:Edge/Jobs.git Jobs
	cd Jobs
	npm install
	cp AppLauncherConfig-example.json ~/EdgeConfig/AppLauncherConfig.json
	
You now have 3 commands available:

	-rwxrwxr-x   1 edge edge  154 May  5 13:27 jobtool
	-rwxrwxr-x   1 edge edge  166 May  5 13:27 jobserver
	-rwxrwxr-x   1 edge edge  168 May  5 13:27 appserver
	
To start the main server simple use:

	./appserver
	
	Now you will see the NPM packages get installed, all the git packages
	that are required get installed and everything will start.
	
## Adding test data

For quick testing, let's add a record to mongo from the command line in a data set named "testdata" and a collection named "customers".

	~/Jobs⟫ mongo testdata
	MongoDB shell version: 3.2.13
	connecting to: testdata
	Welcome to the MongoDB shell.
	For interactive help, type "help".
	
	> db.customers.insert({id:"brian", ranking: 1, city:"Mooresville"});
	WriteResult({ "nInserted" : 1 })
	
	> exit
	
Setup an SSH tunnel on your local machine using port 8007 to port 8001 on the 
Edge server in order to access the generic API.   You can also use a local
curl command to test the server response.

	curl http://localhost:8001/testdata/customers/brian.json
	[{"_id":"590c9bddf6fb9e9ae2232d05","id":"brian","ranking":1,"city":"Mooresville"}]
	
	


	

	
	
	
	


	
