# EdgeCommonRequest
> Makes complicated HTTP and HTTPS Requests

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonRequest.git
    const { EdgeRequest } = require('edgecommonrequest'); // for js
    import { EdgeRequest } from 'edgecommonrequest'; // for typescript

* When an API returns XML, it's automatically parsed and converted into a clean object
* When an API returns JSON, it's parsed into a clean object with fixed dates and types
* Supports Multipart attachments
* Supports several types of compression automatically
* Supports Chuncked and normal Requests
* Supports Windows NT Digest Authentication
* Supports Basic Authentication
* Supports Cookies and custom headers

## Making a Requests

    const request = new EdgeRequest();
    const queryString = { age: 40, name: "Brian Pollack" };
    const result = await request.doGetLink(`${baseUrl}/get`, queryString); //Some JSON Object is returned

## Public methods for configuration

    request.setMethod(method); // set request method. paramter can be GET, POST, PUT, PATCH, DELETE
    request.setUseragent(userAgent); // set user agent for http request
    request.setUsername(username, password); // set username/password for the request
    request.addHeader(key, value); // add custome header to the request

## Running Tests / Updating

This system is using Mocha and Should so run "npm test"

    npm test

To update the module in GitLab use the script update.sh which has the command to rebuild the js and submit.

    sh update.sh "Comments for the submit"

## SSL/TLS Errors

If you get an error like this:

Exception text : Error: Hostname/IP doesn't match certificate's altnames:
You would need to turn off "rejectUnauthorized" which may be a security issue!

    const req = new EdgeRequest();
    req.requestOptions.rejectUnauthorized = false;








