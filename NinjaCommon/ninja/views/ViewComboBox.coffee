class ViewComboBox extends View

    getDependencyList: () =>
        return [ "/vendor/selectize.js", "/vendor/selectize.css" ]

    onShowScreen: () =>
        GlobalClassTools.addEventManager(this)

        @filterFunc = null
        @wgtSelectizeWrapper = @addDiv "selectize-wrapper"
        @wgtSelect = @wgtSelectizeWrapper.add "select", "combo-select"
        @select = @wgtSelect.el.selectize
            maxItems: 1
            # caret: 1
            valueField: 'id',
            labelField: null,
            searchField: []
            placeholder: 'Select...'
            create: false
            options: []
            onInitialize: (args...) ->
                true
            onChange: @onChangeValue
            score: () =>
                ##
                ## Disable auto-search when typing in the text input
                return (item) =>
                    return 1

            render:
                ##
                ## Initial item and option render functions
                item: (item, escape) ->
                    return "<div>#{item.id}</div>"
                option: (item, escape) ->
                    return "<div>#{item.id}</div>"

            load: @onType

        @selectize = @select[0].selectize

    ##
    ## Filter data when you type into the box
    onType: (query, callback) =>
        if !@selectize? then return false
        # console.log "Combobox load: ", query
        if !@filterFunc? then return null
        @selectize.clearCache()
        @selectize.clearOptions()
        newOptions = (@tableData or []).filter (item) =>
            @filterFunc item
        callback newOptions

    ##
    ## Load new data from data map with no filters
    loadData: () =>
        @tableData = DataMap.getValuesFromTable @collectionName
        # console.log "adding new data: ", @tableData, @selectize
        @selectize.clearCache()
        @selectize.clearOptions()
        @selectize.load (callback) =>
            callback @tableData
        @emitEvent "data_load", [@tableData]
        true

    ##
    ## Event handler for selections you make
    onChangeValue: (@value) =>
        row = @tableData.filter (item) => item.id is @value
        # console.log "Combobox onChangeValue: ", @value, row[0]
        @emitEvent "change_value", [@value, row[0]]
        true

    ##
    ## Refresh data as table data in data map changes
    onTableNewData: (e) =>
        # console.log "Table new data: ", e
        if !e? or e.detail.tablename is @collectionName

            if @resetTimer?
                clearTimeout(@resetTimer)

            @resetTimer = setTimeout ()=>
                delete @resetTimer
                @loadData()
            , 50
        true

    ##
    ## Returns "id" value of currently selected item
    getValue: () =>
        @value

    ##
    ## Set "id" value of the selected item
    setValue: (value) =>
        if !@selectize? then return false
        @value = value
        @selectize.setValue @value
        true

    ##
    ## Change placeholder of the box
    setPlaceHolder: (@placeholder) =>
        if !@selectize? then return false
        @selectize.settings.placeholder = @placeholder
        @selectize.updatePlaceholder()
        true

    ##
    ## Change collection name of data source
    setCollectionName: (@collectionName) =>
        if !@selectize? then return false
        if !@collectionName?
            console.log "ComboBox: invalid collection name: ", @collectionName
            return false

        @loadData()
        window.addEventListener "new_data", @onTableNewData, false
        true

    ##
    ## Change item render function
    setItemRenderer: (renderer) =>
        if !@selectize? then return false
        if !renderer? or typeof renderer isnt "function"
            console.log "ComboBox: invalid item render function: ", renderer
            return false

        @itemRenderer                   = renderer
        @selectize.settings.render.item = @itemRenderer
        @selectize.clearCache()
        true

    ##
    ## Change option render function
    setOptionRenderer: (renderer) =>
        if !@selectize? then return false
        if !renderer? or typeof renderer isnt "function"
            console.log "ComboBox: invalid option render function: ", renderer
            return false

        @optionRenderer = renderer
        @selectize.settings.render.option = @optionRenderer
        @selectize.clearCache()
        true

    ##
    ## Change filter function
    setFilterer: (filterer) =>
        if !@selectize? then return false
        if !filterer? or typeof filterer isnt "function"
            console.log "ComboBox: invalid filter function: ", filterer
            return false

        @filterer   = filterer
        @filterFunc = @filterer
        true

    ##
    ## Change width of box to a fixed value
    setWidth: (w) =>
        if !width? then return false
        @w = w
        @wgtSelectizeWrapper.width @w
        true

    onResize: (w, h) =>
        # console.log "Combobox resize: ", w, h
        true

    serialize: () =>
        return
            collection:     @collectionName
            placeholder:    @placeholder
            value:          @value
            filterer:       @filterer
            itemRenderer:   @itemRenderer
            optionRenderer: @optionRenderer
            width:          @w

    deserialize: (optionData) =>
        new Promise (resolve, reject)=>
            if !optionData? then reject("Combobox: invalid data to deserailize: ", optionData)

            if optionData.collection? or optionData.table?
                @setCollectionName(optionData.collection or optionData.table)

            if optionData.placeholder?
                @setPlaceHolder optionData.placeholder

            if optionData.filterer?
                @setFilterer optionData.filterer

            if optionData.itemRenderer?
                @setItemRenderer optionData.itemRenderer

            if optionData.optionRenderer?
                @setOptionRenderer optionData.optionRenderer

            if optionData.value?
                @setValue optionData.value

            if optionData.width?
                @setWidth optionData.width
            resolve true
