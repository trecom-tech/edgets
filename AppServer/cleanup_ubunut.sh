#!/bin/sh

sudo npm -g install npm
sudo npm -g install yarn
yarn cache clean
rm -rf ~/.npm/
echo 'db.adminCommand( { logRotate : 1 } )' | ma
sudo find /var/log/mongodb -name "*T*" -ls -exec rm {} \;