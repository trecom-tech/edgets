import { config }   from 'edgecommonconfig';

const LIMIT = 10;

export default class RuleProcessor {
    
    rule            : any;
    test_condition  : any;
    log             : any;
    active          : boolean;
    readonly limit  : number = LIMIT;

    constructor(rule: any) {
        this.rule           = rule;
        this.test_condition = null;
        this.active         = true;

        if (!rule.condition) {
            console.log('Invalid rule, missing condition:', this.rule);
            this.log.error('Invalid rule, missing condition', {rule: this.rule});

            return;
        }

        this.test_condition = new RegExp(this.rule.condition, 'i');

        // If the rule is not active in the database then it's not active here.
        this.active         = this.rule.active === false ? false : this.active;
        this.active         = this.rule.active === 0 ? false : this.active;
        this.onInit();
    }

    /**
     * Report an error and deactive the rule 
     */
    setError(txt: string) {
        config.status('Rule error', this.rule, txt);
        console.log(`Rule error in ${this.rule.title}:`, txt);
        this.active = false;
    }

    /**
     * Check to see if a change matches any rules
     */
    check(job: any) {
        if (!this.active) { return false; }
        if (!this.test_condition) { return false; }
        if (!job) { return false; }
        if (!job.path) { return false; }

        if (this.test_condition.test(job.path)) {
            config.status('RuleProcessor check passed:', {
                condition: this.test_condition,
                path: job.path,
                dataset: job.dataset
            });

            this.onExecute(job);
            return true;
        }

        return false;
    }

    onExecute(job: any) {
        // put this in the extended class
    }

    onInit() {
        // put this in the extended class
    }

}