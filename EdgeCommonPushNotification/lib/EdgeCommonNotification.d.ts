import { IContactInfo } from './IContactInfo';
import { IMessage } from './IMessage';
export declare class EdgeCommonNotification {
    private contacts;
    private logger;
    private readonly guid;
    constructor();
    addContactDetails(userContactDetails?: IContactInfo[]): boolean;
    doSendMessage(messageDetails: IMessage): Promise<any>;
    doSendForApple(deviceToken: string, message: IMessage): Promise<boolean>;
    doSendForGoogle(deviceToken: string, message: IMessage): Promise<boolean>;
    doSendWhatsApp(whatsAppNumber: string, message: IMessage): Promise<boolean>;
    doSendSMS(phoneNumber: string, message: IMessage): Promise<boolean>;
    private static validatePhoneNumber;
    private static generateGUID;
}
export default EdgeCommonNotification;
