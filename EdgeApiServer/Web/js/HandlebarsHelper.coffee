$ ->

    Handlebars.registerHelper 'formatNumber', (context, options)->
        return numeral(context).format('#,###.[##]')