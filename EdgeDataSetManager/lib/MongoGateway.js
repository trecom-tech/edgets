"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const edgecommonconfig_1 = require("edgecommonconfig");
const MongoModel_1 = require("./MongoModel");
// true when mongo is being connected at the moment
let globalBusyConnecting = false;
// Cache for collections
let globalConnection = {};
// dbAdmin from MongoClient.connect
let globalAdmin = {};
class MongoGateway {
    /**
     * Connect alternateConnectionConfig first and try to get MongoDB_{dbName}
     * If they are are all null, fall back to use MongoDB
     * Use Azure Cosmos db from when dbName is logs and changes, read config 'CosmosDB'
     * @param dbName db name
     * @param alternateConnectionConfig this db connection config is used when is not null rather than config.getCredential('MongoDB')
     */
    constructor(dbName, alternateConnectionConfig) {
        this.dbName = dbName;
        this.alternateConnectionConfig = alternateConnectionConfig;
        this.databaseReady = false;
    }
    //
    // Close the database
    close() {
        if (this.connectPromise != null && this.databaseReady) {
            // tslint:disable-next-line:no-floating-promises
            this.dbAdmin.close();
        }
        return true;
    }
    /**
     * Get the collection from the database
     * @param name [string] collection name
     */
    collection(name) {
        return new MongoModel_1.default(name, this);
    }
    static displayStatus() {
        edgecommonconfig_1.config.status(`Getting connection status ${Object.keys(globalAdmin).length}`);
        return (() => {
            let result = [];
            for (let name in globalConnection) {
                let db = globalConnection[name];
                edgecommonconfig_1.config.status(`Getting connection for ${name}`);
                // tslint:disable-next-line:handle-callback-err
                result.push(db.admin().serverStatus((err, status) => edgecommonconfig_1.config.dump("Server status:", {
                    host: status.host,
                    uptime: status.uptime,
                    connections: status.connections,
                    locks: status.globalLock.currentQueue,
                    ops: status.opcounters
                })));
            }
            return result;
        })();
    }
    getConnectionStatus() {
        return __awaiter(this, void 0, void 0, function* () {
            let info = yield this.db.admin().serverStatus();
            return edgecommonconfig_1.config.dump("MongoDB Connection Data", info);
        });
    }
    /**
     * Connect to the database, should be called before every
     * call as it returns a promise
     *
     */
    doConnectToDatabase() {
        if (!this.connectPromise) {
            this.connectPromise = new Promise((resolve, reject) => {
                if (globalConnection[this.dbName] != null) {
                    this.db = globalConnection[this.dbName];
                    resolve();
                    return;
                }
                globalBusyConnecting = true;
                try {
                    let mongoServer = null;
                    if (this.alternateConnectionConfig != null) {
                        mongoServer = edgecommonconfig_1.config.getCredentials(this.alternateConnectionConfig);
                    }
                    else {
                        edgecommonconfig_1.config.status(`Looking for credentials MongoDB_${this.dbName}`);
                        mongoServer = edgecommonconfig_1.config.getCredentials(`MongoDB_${this.dbName}`);
                        if (mongoServer == null) {
                            mongoServer = edgecommonconfig_1.config.getCredentials("MongoDB");
                        }
                    }
                    // Certain tables are forced to Cosmos
                    let wantAzureDatabase = false;
                    if (this.dbName === "logs") {
                        wantAzureDatabase = true;
                    }
                    if (this.dbName === "changes") {
                        wantAzureDatabase = true;
                    }
                    if (wantAzureDatabase) {
                        let testCreds = edgecommonconfig_1.config.getCredentials("CosmosDB");
                        if (testCreds != null && testCreds.url != null) {
                            edgecommonconfig_1.config.status(`MongoGateway switching to CosmosDB for ${this.dbName}`);
                            mongoServer = testCreds;
                        }
                    }
                    edgecommonconfig_1.config.status("Connecting to", mongoServer);
                    return mongodb_1.MongoClient.connect(mongoServer.url, mongoServer.options, (err, dbAdmin) => {
                        this.dbAdmin = dbAdmin;
                        if (err) {
                            edgecommonconfig_1.config.status("Error connecting to database: ", err);
                            this.databaseReady = false;
                            edgecommonconfig_1.config.status(`ERROR Connecting to Database: ${err.toString()}`);
                            globalBusyConnecting = false;
                            reject(err);
                            return;
                        }
                        this.dbAdmin.on("timeout", (e) => {
                            return edgecommonconfig_1.config.status("dbAdmin timeout:", e);
                        });
                        this.dbAdmin.on("error", (e) => {
                            return edgecommonconfig_1.config.status("dbAdmin error:", e);
                        });
                        this.dbAdmin.on("close", e => {
                            return edgecommonconfig_1.config.status("dbAdmin close:", e);
                        });
                        this.dbAdmin.on("fullsetup", e => {
                            return edgecommonconfig_1.config.status("dbAdmin fullsetup", e);
                        });
                        this.dbAdmin.on("authenticated", e => {
                            return edgecommonconfig_1.config.status("dbAdmin authenticated:", e);
                        });
                        this.db = this.dbAdmin.db(this.dbName);
                        this.db.on("timeout", e => {
                            return edgecommonconfig_1.config.status("db timeout:", e);
                        });
                        this.db.on("error", e => {
                            return edgecommonconfig_1.config.status("db error:", e);
                        });
                        this.db.on("close", e => {
                            return edgecommonconfig_1.config.status("db close:", e);
                        });
                        // tslint:disable-next-line:no-empty
                        this.db.on("fullsetup", e => { });
                        // config.status "db fullsetup:", e
                        this.db.on("authenticated", e => {
                            return edgecommonconfig_1.config.status("db authenticated:", e);
                        });
                        // config.status "Connection established to #{@dbName}"
                        this.databaseReady = true;
                        globalBusyConnecting = false;
                        globalConnection[this.dbName] = this.db;
                        globalAdmin[this.dbName] = this.dbAdmin;
                        return resolve();
                    });
                }
                catch (error) {
                    let e = error;
                    edgecommonconfig_1.config.status("UNABLE TO CONNECT:", e);
                    return reject(e);
                }
            });
        }
        return this.connectPromise;
    }
    doGetStats() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doConnectToDatabase();
            return this.db.stats();
        });
    }
}
exports.MongoGateway = MongoGateway;
;
exports.default = MongoGateway;
//# sourceMappingURL=MongoGateway.js.map