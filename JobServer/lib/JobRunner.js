"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const JobServer_1 = require("./JobServer");
if (process.env.NODE_ENV === 'local') {
    process.env.MongoDB = '{"url": "mongodb://localhost:27017/admin", "options": {"poolSize": 8,"autoReconnect": true, "noDelay": true,"forceServerObjectId": true,"ignoreUndefined": true,"authSource": "admin" } }';
    process.env.redisHostWQ = 'redis://127.0.0.1:6379';
    process.env.redisReadHost = 'redis://127.0.0.1:6379';
    process.env.redisHost = 'redis://127.0.0.1:6379';
    process.env.mqHost = 'amqp://127.0.0.1:5672';
    process.env.sendgrid = '{"type": "sendgrid", "apikey": "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"}';
}
const runner = new JobServer_1.default();
class JobRunner {
    static index() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield runner.startSystemStatusLoop();
                yield runner.getJobsList();
                const all = [];
                all.push(runner.executeJobs(0));
                all.push(runner.executeJobs(1));
                all.push(runner.executeJobs(2));
                all.push(runner.executeJobs(3));
                yield Promise.all(all);
                console.log("All jobs complete");
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
}
exports.JobRunner = JobRunner;
JobRunner.index();
exports.default = JobRunner;
//# sourceMappingURL=JobRunner.js.map