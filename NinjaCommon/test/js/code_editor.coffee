$ ->

	addTestButton "Basic Code Editor", "Open", ()->
		addHolder().setView "CodeEditor", (view)->
			true

		true

	addTestButton "Basic Code Editor Events", "Open", ()->
		addHolder().setView "CodeEditor", (view)->
			##|
			##|  Any time input is fired
			view.on "change", (editor)=>
				console.log "Input Event"

			##|
			##|  If document changes from last saved
			view.on "dirty_changed", (editor, isDirty)=>
				console.log "Dirty Change Event:", isDirty
			true
		true

	addTestButton "Code Editor with theme", "Open", ()->

		addHolder().setView "CodeEditor", (view)->
			# make sure the js of the theme is included
			view.setTheme("tomorrow_night_eighties")
			true
		true

	addTestButton "Code Editor with theme and options and content api", "Open", ()->

		addHolder().setView "CodeEditor", (view)->
			# make sure the js of the theme is included
			view.setTheme("tomorrow_night_eighties").setOptions
				enableBasicAutocompletion: true
				enableSnippets: true
				enableLiveAutocompletion: true
			.setContent "select * from users"
			true
		true

	addTestButton "Code Editor with change event", "Open", ()->
		addHolder().setView "Docked", (dockedView)->
			dockedView.getFirst().html '<div id="preview" style="padding: 10px;"></div>'
			dockedView.getSecond().setView "CodeEditor", (editorView)->
				editorView.getEditorInstance().setTheme("tomorrow_night_eighties").onChange (content, editor) ->
					$("div#preview").html content
				true
		true

	addTestButton "Code Editor with mongoDB(javascript) mode", "Open", ()->
		addHolder().setView "CodeEditor", (view)->
			# make sure the js of the theme is included
			view.setTheme("tomorrow_night_eighties").setMode('javascript')
			true
		true

	addTestButton "Code Editor with histories", "Open", ()->
		addHolder("renderHistories");
		addHolder("renderTest1");
		##| to make little bit space it should be done using css
		addHolder().setView "Docked", (dockedView)->
			dockedView.setDockSize 150
			divHistories = dockedView.getFirst().addDiv "", "renderHistories"
			divHistories.css "margin-bottom", "30px"
			button = dockedView.getFirst().add "button", "btn btn-primary", "add"
			button.text "Add To History"
			dockedView.getBody().setView "CodeEditor", (editorView) ->
				editor = editorView.getEditorInstance()
				editor.setTheme("tomorrow_night_eighties")
				editor.addToHistory "select * from users"
				editor.addToHistory "select * from abc"
				editor.renderHistories $('#renderHistories'), (updatedValue,element) ->
					editor.setContent updatedValue
					element.val ''
				button.bind "click", () ->
					editor.addToHistory editor.getContent()
					editor.refreshHistories()
				true
		true

	addTestButton "Code Editor Add Custom HotKey", "Open", ()->
		addHolder().setView "CodeEditor", (view)->
			view.showEditor()
			bindKey = {win: 'Ctrl-M', mac: 'Command-M'}
			view.setHotKey "my_hot_key", bindKey, () =>
				console.log "My Hot Key is clicked ", bindKey

		true

	addTestButton "Code Editor Deserialize", "Open", () ->
		addHolder()
		.setView "CodeEditor", (view) ->
			view.deserialize 
				theme: "tomorrow_night_eighties"
				content: "{ key: 'This is initial content for this Code Editor' }"
				popupmode: true 
				mode: "json"
				options: 
					enableBasicAutocompletion: true
					enableSnippets: true
					enableLiveAutocompletion: true
			true

	addTestButton "Basic WYSIWYG Editor", "Open", ()->
		addHolder().setView "WysEditor", (view)->
			true
		true

	addTestButton "WYSIWYG Editor Set References", "Open", ()->
		refList = ['apple', 'banana', 'cherry', 'dolphin', 'elephant']
		addHolder().setView "WysEditor", (view)->
			view.wysEditor.setReferenceList refList
		true

	addTestButton "WYSIWYG Editor Set Event Handler", "Open", ()->
		addHolder().setView "WysEditor", (view)->
			view.wysEditor.setEventHandler 'change', (we, contents, $editable) =>
  				console.log 'summernote\'s content is changed: ', contents
		true

	addTestButton "WYSIWYG Editor Custom Button", "Open", ()->
		addHolder().setView "WysEditor", (view)->
			view.wysEditor.createButton 'myButton', 'Click to see magic!', (event) =>
  				console.log 'My button is clicked!... ', event
  			, "<i class='fa fa-save'></i>"
		true		

	addTestButton "WYSIWYG Editor Air Mode", "Open", ()->
		addHolder().setView "WysEditor", (view)->
			view.setAirMode true
		true

	addTestButton "WYSIWYG Editor on Popup", "Open", ()->
		refList = ['apple', 'banana', 'cherry', 'dolphin', 'elephant']
		doPopupView "WysEditor", "Popup with WYSIWYG Editor", null, 800, 600, (view) ->
			view.wysEditor.setReferenceList refList
		true

	addTestButton "WYSIWYG Editor in Tab", "Open", () ->
		addHolder()
		.setView "DynamicTabs", (tabs)->
			tabs.doAddViewTab "WysEditor", "Tab WysEditor", (view) ->
				view.setContent "<p>Hello world!</p>"
				view.wysEditor.setEventHandler 'keyup', (we, e) =>
					console.log 'WysEditor Key Up- keycode: ', e.keyCode

			tabs.addTab "Empty Tab", "<p style='font-size:xx-large;'>--- Another Tab ---</p>"

		true

	addTestButton "WYSIWYG Editor with custom Toolbar and buttons", "Open" , () ->
		items = [
			['font',['bold','italic','underline','clear']],
			['fontname',['fontname']],
			['color',['color']],
			['insert',['media','link','hr']]
		]

		btnObj = 
			title: "Save Note"
			tooltip: "Click to save the note"
			callback: (e) =>
				console.log "Save Note button is clicked... ", e 
			icon: "<i class='fa fa-save'></i>"

		doPopupView "WysEditor", "New Note", null, 800, 600, (view) ->
			view.setToolbarItems items
			view.createButton btnObj


	addTestButton "WYSIWYG Editor Custom Hotkey", "Open", ()->
		addHolder().setView "WysEditor", (view)->
			view.setHotkey 's', (we, e) =>
  				console.log 'Hotkey is clicked: Ctrl -', e.ctrlKey, ' Key -', e.keyCode
		true

	addTestButton "WYSIWYG Editor Auto Focused", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input", "Example Input"
			view.addButton "button", "Button Label", "Button", 
				onClick: (form) ->
					doPopupView "WysEditor", "Pouup Editor", null, 800, 600, (editor) ->
						editor.setFocus()
						true
			view.addSubmit "submit", "Submit Label", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.show()
			true

	addTestButton "WYSIWYG Editor Deserialize", "Open", () ->
		addHolder()
		.setView "WysEditor", (view) ->
			view.deserialize
				content: "Default content"
				airMode: false
				button:
					title: "New Button"
					tooltip: "This is a new button"
					callback: (e) =>
						console.log "New button is clicked"
					icon: "<i class='fa fa-pencil'></i>"
				toolbaritems: [
					['font',['bold','italic','underline','clear']],
					['fontname',['fontname']],
					['color',['color']],
					['insert',['media','link','hr']]
				]

			setTimeout () ->
				options = view.serialize()
				console.log "WysEditor serialized data = ", options
				doPopupView "WysEditor", "WysEditor Serialized", null, 1200, 800, (viewPopup) ->
					viewPopup.deserialize options
			, 3000

	go()
