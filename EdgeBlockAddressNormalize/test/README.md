In order to test the AddressNormalizer I used SmartyStreets in some cases to produce valid results.

https://us-street.api.smartystreets.com/street-address?auth-id=123&auth-token=abc

From their website:

    curl -v 'https://us-street.api.smartystreets.com/street-address?
                auth-id=YOUR+AUTH-ID+HERE&
                auth-token=YOUR+AUTH-TOKEN+HERE&
                street=1600+amphitheatre+pkwy&
                city=mountain+view&
                state=CA&
                candidates=10'

My test credentials:

    auth-id=79017c92-b367-2ed9-12ae-781f2f72f03f
    auth-token=mlekVZKyfcaGZ5aNa4cZ

Example of their data

    "delivery_line_1": "250 N Banana River Dr Apt C6",
    "last_line": "Merritt Island FL 32952-2553",
    "delivery_point_barcode": "329522553365",
    "components": {
      "primary_number": "250",
      "street_predirection": "N",
      "street_name": "Banana River",
      "street_suffix": "Dr",
      "secondary_number": "C6",
      "secondary_designator": "Apt",
      "city_name": "Merritt Island",
      "state_abbreviation": "FL",
      "zipcode": "32952",
      "plus4_code": "2553",
      "delivery_point": "36",
      "delivery_point_check_digit": "5"
    },

## Redis

If you get an error like this:

    Error: Redis connection to 127.0.0.1:6379 failed - connect ECONNREFUSED 127.0.0.1:6379

You need to be running REDIS locally.

    brew install redis
    brew services start redis