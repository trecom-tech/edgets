import EdgeBlock from 'edgeblock';
interface Address {
    number?: number;
    display?: string;
    city?: string;
    state?: string;
    street?: string;
    county?: string;
    zip?: number;
    unit?: number;
    hash?: string;
    [key: string]: any;
}
interface AddressResult {
    _lastModified: Date;
    source: string;
    confidence: number;
    lat: number;
    lon: number;
    address?: Address;
    display?: string;
    other: any;
}
export declare class GeoCode extends EdgeBlock {
    static useEdgeApi: boolean;
    constructor(...args: any[]);
    getInputs(): string[];
    initResult(): AddressResult;
    internalConvertAddressToObject(strAddress: any): Address;
    doGetCensus(address: any): Promise<AddressResult | null>;
    doGetOpenCage(address: any): Promise<AddressResult>;
    doGetGeocodio(address: any): Promise<{}>;
    doGetMapbox(address: any): Promise<AddressResult>;
    doGetMapboxReverse(lat: number, lon: number): Promise<AddressResult>;
    process(address?: any): Promise<any>;
}
export default GeoCode;
