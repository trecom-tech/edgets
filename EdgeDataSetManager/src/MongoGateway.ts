
import { MongoClient, MongoError, Db } from 'mongodb';
import { config }  from 'edgecommonconfig';
import MongoModel from './MongoModel';

// true when mongo is being connected at the moment
let globalBusyConnecting = false;
// Cache for collections
let globalConnection: {[dbName: string]: Db}          = {};
// dbAdmin from MongoClient.connect
let globalAdmin     : {[dbName: string]: MongoClient} =  {};

export class MongoGateway {
    connectPromise: Promise<void>;
    dbAdmin       : MongoClient;
    public db     : Db;
    databaseReady : boolean = false;

    /**
     * Connect alternateConnectionConfig first and try to get MongoDB_{dbName}
     * If they are are all null, fall back to use MongoDB 
     * Use Azure Cosmos db from when dbName is logs and changes, read config 'CosmosDB'
     * @param dbName db name
     * @param alternateConnectionConfig this db connection config is used when is not null rather than config.getCredential('MongoDB')
     */
    constructor(public dbName: string, public alternateConnectionConfig: any) {

    }

    //
    // Close the database
    close() {

        if (this.connectPromise != null && this.databaseReady) {
            // tslint:disable-next-line:no-floating-promises
            this.dbAdmin.close();
        }

        return true;
    }

    /**
     * Get the collection from the database
     * @param name [string] collection name
     */
    collection(name: string): MongoModel {
        return new MongoModel(name, this);
    }

    static displayStatus() {

        config.status(`Getting connection status ${ Object.keys(globalAdmin).length }`);
        return (() => {
            let result = [];
            for (let name in globalConnection) {
                let db = globalConnection[name];
                config.status(`Getting connection for ${ name }`);
                // tslint:disable-next-line:handle-callback-err
                result.push(db.admin().serverStatus((err, status) => config.dump("Server status:", {
                    host: status.host,
                    uptime: status.uptime,
                    connections: status.connections,
                    locks: status.globalLock.currentQueue,
                    ops: status.opcounters
                })));
            }
            return result;
        })();
    }

    async getConnectionStatus() {

        let info = await this.db.admin().serverStatus();
        return config.dump("MongoDB Connection Data", info);
    }

    /**
     * Connect to the database, should be called before every
     * call as it returns a promise
     * 
     */
    doConnectToDatabase() {

        if (!this.connectPromise) {
            this.connectPromise = new Promise((resolve, reject) => {

                if (globalConnection[this.dbName] != null) {
                    this.db = globalConnection[this.dbName];
                    resolve();
                    return;
                }

                globalBusyConnecting = true;

                try {
                    let mongoServer = null;
                    if (this.alternateConnectionConfig != null) {
                        mongoServer = config.getCredentials(this.alternateConnectionConfig);
                    } else {
                        config.status(`Looking for credentials MongoDB_${ this.dbName }`);
                        mongoServer = config.getCredentials(`MongoDB_${ this.dbName }`);
                        if (mongoServer == null) {
                            mongoServer = config.getCredentials("MongoDB");
                        }
                    }

                    // Certain tables are forced to Cosmos
                    let wantAzureDatabase = false;
                    if (this.dbName === "logs") {
                        wantAzureDatabase = true;
                    }
                    if (this.dbName === "changes") {
                        wantAzureDatabase = true;
                    }

                    if (wantAzureDatabase) {
                        let testCreds = config.getCredentials("CosmosDB");
                        if (testCreds != null && testCreds.url != null) {
                            config.status(`MongoGateway switching to CosmosDB for ${ this.dbName }`);
                            mongoServer = testCreds;
                        }
                    }

                    config.status("Connecting to", mongoServer);

                    return MongoClient.connect(mongoServer.url, mongoServer.options, (err: MongoError, dbAdmin: MongoClient) => {

                        this.dbAdmin = dbAdmin;
                        if (err) {
                            config.status("Error connecting to database: ", err);
                            this.databaseReady = false;
                            config.status(`ERROR Connecting to Database: ${ err.toString() }`);
                            globalBusyConnecting = false;
                            reject(err);
                            return;
                        }

                        this.dbAdmin.on("timeout", (e: Error) => {
                            return config.status("dbAdmin timeout:", e);
                        });

                        this.dbAdmin.on("error", (e: Error) => {
                            return config.status("dbAdmin error:", e);
                        });

                        this.dbAdmin.on("close", e => {
                            return config.status("dbAdmin close:", e);
                        });

                        this.dbAdmin.on("fullsetup", e => {
                            return config.status("dbAdmin fullsetup", e);
                        });

                        this.dbAdmin.on("authenticated", e => {
                            return config.status("dbAdmin authenticated:", e);
                        });

                        this.db = this.dbAdmin.db(this.dbName);

                        this.db.on("timeout", e => {
                            return config.status("db timeout:", e);
                        });

                        this.db.on("error", e => {
                            return config.status("db error:", e);
                        });

                        this.db.on("close", e => {
                            return config.status("db close:", e);
                        });

                        // tslint:disable-next-line:no-empty
                        this.db.on("fullsetup", e => {});
                        // config.status "db fullsetup:", e

                        this.db.on("authenticated", e => {
                            return config.status("db authenticated:", e);
                        });

                        // config.status "Connection established to #{@dbName}"
                        this.databaseReady = true;
                        globalBusyConnecting = false;
                        globalConnection[this.dbName] = this.db;
                        globalAdmin[this.dbName] = this.dbAdmin;


                        return resolve();
                    });
                } catch (error) {
                    let e = error;
                    config.status("UNABLE TO CONNECT:", e);
                    return reject(e);
                }
            });
        }

        return this.connectPromise;
    }

    async doGetStats() {
        await this.doConnectToDatabase();
        return this.db.stats();
    }
};

export default MongoGateway;
