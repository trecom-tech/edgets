"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redis = require("redis");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonratecapture_1 = require("edgecommonratecapture");
const edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
const ApiSocketUser_1 = require("./ApiSocketUser");
const edgeapi_1 = require("edgeapi");
const EdgeError_1 = require("./EdgeError");
exports.REDIS_PUBSUB_CHANNEL = 'MESSAGES';
class ApiSocketLog {
    constructor(user, callName, data) {
        this.user = user;
        this.callName = callName;
        this.data = data;
        this.start = process.hrtime();
    }
    save(log, result) {
        let diff = process.hrtime(this.start);
        let ns = diff[0] * 1e9 + diff[1];
        let ms = ns / 1e6;
        if (this.user != null && this.user.myConnectionID != null) {
            console.log('API Log:', {
                user: this.user.myName,
                callName: this.callName,
                total_time: ms,
            });
        }
        if (!edgecommonconfig_1.default.traceEnabled) {
            return null;
        }
        log.info('MakeDynamicCall', {
            user: this.user.myName,
            callName: this.callName,
            data: this.data,
            total_time: ms,
            result,
        });
        return true;
    }
}
class ApiSocketFunction {
    constructor(path, apiDefinition) {
        this.path = path;
        this.apiDefinition = apiDefinition;
        this.connections = [];
        this.index_num = 0;
    }
    getUser() {
        // Todo, if there are multiple provider, cycle through them
        return this.connections[0];
    }
    addConnection(user) {
        this.connections.push(user);
        return console.log(`User ${user.myConnectionID} accepting `, this.path);
    }
    removeConnection(user) {
        let list = [];
        for (let c of Array.from(this.connections)) {
            if (c === user) {
                continue;
            }
            list.push(c);
        }
        this.connections = list;
        return console.log(`User ${user.myConnectionID} no longer accepting `, this.path);
    }
}
class ApiSocketServer {
    constructor(apiWebServer) {
        this.bar = null;
        this.log = edgecommonconfig_1.default.getLogger('ApiGateway');
        this.statusChanges = [];
        this.statusMessages = [];
        this.statusUpdates = [];
        this.localApiCallList = {};
        this.registerdCalls = {};
        // uniqueNumber         : socket
        this.allClients = {};
        this.globalUnique = 0;
        this.socketCount = 0;
        this.webServer = null;
        this.total_api_calls_done = 0;
        this.total_api_calls = 0;
        this.bar = new edgecommonratecapture_1.default('ApiSocketServer');
        this.bar.onBeforeUpdate = (varList, status) => {
            this.bar.addSample(this.total_api_calls, this.total_api_calls_done - this.total_api_calls);
            return true;
        };
        this.redisPubsubQueue = edgeapi_1.EdgeApi.getRedisConnectionWQ();
        this.webServer = apiWebServer;
        this.onInit(apiWebServer);
    }
    static getRedisReadonlyConnection() {
        const redisHost = edgecommonconfig_1.default.getCredentials('redisReadHost');
        edgecommonconfig_1.default.status(`getRedisReadonlyConnection to ${redisHost}`);
        return redis.createClient(redisHost, { no_ready_check: true });
    }
    static getRedisConnection() {
        const redisHost = edgecommonconfig_1.default.getCredentials('redisHost');
        return redis.createClient(redisHost, { no_ready_check: true });
    }
    /**
     *
     * @param apiWebServer
     */
    onInit(apiWebServer) {
        this.setupSocketServer(apiWebServer.getHttpServer(), apiWebServer.getHttpsServer());
        //  Maintain a connection REDIS Pub/Sub for "hearing" the changes
        edgecommonconfig_1.default.status('ApiSocketServer onInit Connecting to cache');
        this.cache = ApiSocketServer.getRedisConnection();
        this.cache.subscribe('changelog');
        return this.cache.on('message', (channel, message) => {
            const data = JSON.parse(message);
            return this.onIncomingChangeNotification(data);
        });
    }
    /**
     * Add a custom URL or Path
     * @param pathPattern {string|regex} The pattern to match
     * @param callback {function}
     */
    addRoute(pathPattern, callback) {
        this.webServer.addRoute(pathPattern, callback);
    }
    /**
     * Remove any registrations that the user is setup for
     * @param user
     */
    removeRegistrations(user) {
        if (user.registerdCalls != null) {
            for (const path of user.registerdCalls) {
                this.registerdCalls[path].removeConnection(user);
                delete this.localApiCallList[path];
            }
        }
        return true;
    }
    //  Register a command that a connection is able to handle
    onRegisterApiFunction(apiDefinition, user) {
        let description;
        if (apiDefinition.base == null) {
            console.log('Invalid API Definition 1:', apiDefinition);
            return false;
        }
        if (apiDefinition.command == null) {
            console.log('Invalid API Definition 2:', apiDefinition);
            return false;
        }
        // A given connection can now support an API call
        const path = apiDefinition.base + '_' + apiDefinition.command;
        if (this.registerdCalls[path] == null) {
            this.registerdCalls[path] = new ApiSocketFunction(path, apiDefinition);
        }
        if (user.registerdCalls == null) {
            user.registerdCalls = [];
        }
        user.registerdCalls.push(path);
        const args = [];
        for (const varName in apiDefinition.params) {
            description = apiDefinition.params[varName];
            args.push(varName);
        }
        user.addDynamicApiCall(path, args);
        console.log('User registered command: ', path);
        this.registerdCalls[path].addConnection(user);
        this.localApiCallList[path] = [];
        for (const varName in apiDefinition.params) {
            description = apiDefinition.params[varName];
            this.localApiCallList[path].push(varName);
        }
        this.resendApiList();
        return true;
    }
    /**
     * Handles incoming channel messages
     * @param data
     */
    onIncomingChannelMessage(data) {
        for (const idx in this.allClients) {
            const client = this.allClients[idx];
            client.filterChange(data);
        }
    }
    /**
     * Handles incoming change messages
     * @param data
     */
    onIncomingChangeNotification(data) {
        data.type = 'change';
        for (const idx in this.allClients) {
            const client = this.allClients[idx];
            client.filterChange(data);
        }
        for (const idx in this.allClients) {
            const client = this.allClients[idx];
            if (client.wantStatusUpdate) {
                client.sendPushDelayed('status-update', data);
            }
        }
        return true;
    }
    setupChangeEventServer() {
        //  Connect to the MQ server, bind to the changes and watch
        if (this.setupChangeEventServerPromise != null) {
            return this.setupChangeEventServerPromise;
        }
        return this.setupChangeEventServerPromise = new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            // subscribe to generic channel in order to have only one subscribe callback per instance
            this.redisPubsubQueue.subscribe(exports.REDIS_PUBSUB_CHANNEL);
            this.redisPubsubQueue.on('message', (channel, message) => {
                // message should be in IPubSubData format
                if (channel === exports.REDIS_PUBSUB_CHANNEL) {
                    const data = JSON.parse(message);
                    return this.onIncomingChannelMessage(data);
                }
            });
            resolve(true);
        }));
    }
    /**
     * Setup an internal server that waits for remote status messages
     */
    setupStatusServer() {
        //  Connect to the MQ server, bind to the changes and watch
        if (this.statusSetupServerPromise != null) {
            return this.statusSetupServerPromise;
        }
        return this.statusSetupServerPromise = new Promise((resolve) => {
            edgecommonmessagequeue_1.default.doOpenSubscription(edgecommonconfig_1.default.mqExchangeStatusUpdates, (message) => {
                // config.status "STATUS Update Message:", message
                if (this.statusUpdates == null) {
                    this.statusUpdates = [];
                }
                if (this.statusMessages == null) {
                    this.statusMessages = [];
                }
                if (message.name != null) {
                    this.statusUpdates.push(message);
                    if (this.statusUpdates.length > 1000) {
                        this.statusUpdates.shift();
                    }
                }
                else {
                    this.statusMessages.push(message);
                    if (this.statusMessages.length > 100) {
                        this.statusMessages.shift();
                    }
                }
                return (() => {
                    const result = [];
                    for (const idx in this.allClients) {
                        const client = this.allClients[idx];
                        if (client.wantStatusUpdate) {
                            result.push(client.sendPush('status-update', message));
                        }
                        else {
                            result.push(undefined);
                        }
                    }
                    return result;
                })();
            });
            resolve(true);
        });
    }
    /**
     *
     * @param user
     * @param uuid
     * @param newResult
     */
    doSendFlatResponse(user, uuid, newResult) {
        let result;
        if (user.configSendFlat) {
            if (newResult == null) {
                result = { uuid };
                user.send('cmd-reply', result);
                return true;
            }
            //
            //  Already an object, we can just add the uuid and send
            if (typeof newResult === 'object') {
                newResult.uuid = uuid;
                user.send('cmd-reply', newResult);
                return true;
            }
            //  Not an object so we have to merge
            result = { uuid };
            result.value = newResult;
            user.send('cmd-reply', result);
            return true;
        }
        else {
            user.send('cmd-reply', {
                uuid,
                result: newResult,
            });
        }
        return true;
    }
    /**
     *
     * @param user
     * @param callName
     * @param data
     * @constructor
     */
    MakeDynamicCall(user, callName, data) {
        //  Check to see if this is a registered call that a connected socket
        //  would like to handle.  If so, we send the message to the connection
        //  and then when the response comes in, we'll deal with it, but not
        //  at the moment.
        let result;
        this.total_api_calls++;
        const apiLog = new ApiSocketLog(user, callName, data);
        let { uuid } = data;
        delete data.uuid;
        const handleError = (e) => {
            const logger = edgecommonconfig_1.default.getLogger('ApiCalls');
            if (e instanceof EdgeError_1.default) {
                logger.error('EdgeError thrown in call', {
                    callName,
                    callData: data,
                    error: e,
                    user: user.myName,
                });
                // TODO put custom logic here for handling errors based on error code
                this.doSendFlatResponse(user, uuid, { error: e.message, e: e.toString() });
            }
            else {
                logger.error('Unexpected Error in call', {
                    callName,
                    callData: data,
                    error: e,
                    user: user.myName,
                });
                edgecommonconfig_1.default.reportError(`API CALL [${callName}]`, e);
                this.doSendFlatResponse(user, uuid, { error: 'Internal Error', e: e.toString() });
                apiLog.save(this.log, {
                    type: 'exception',
                    ex: e,
                });
            }
            this.total_api_calls_done++;
            return this.bar.incVar('Errors', 1);
        };
        const handleSuccess = (res) => {
            this.doSendFlatResponse(user, uuid, res);
            this.total_api_calls_done++;
            apiLog.save(this.log, res);
        };
        if (this.registerdCalls[callName] != null) {
            //  Send the command to the connected socket
            try {
                result = this.registerdCalls[callName].getUser().MakeDynamicCall(callName, data);
            }
            catch (e) {
                return handleError(e);
            }
        }
        else {
            try {
                result = this.webServer.gateway.doProcessApiCall(callName, data, user);
            }
            catch (e) {
                return handleError(e);
            }
        }
        if (result != null && result.then != null && typeof result.then === 'function') {
            // Handle the promise by waiting for the answer and then
            // sending that answer back to the client
            result.then(newResult => {
                handleSuccess(newResult);
            }).catch(e => {
                handleError(e);
            });
        }
        else {
            handleSuccess(result);
        }
        return true;
    }
    sendApiList(user) {
        // Send a list of available API calls
        user.send('api-list', this.localApiCallList);
        this.log.info('SendAPIList', { apiList: this.localApiCallList });
        return true;
    }
    resendApiList() {
        for (const id in this.allClients) {
            const user = this.allClients[id];
            this.sendApiList(user);
        }
        return true;
    }
    setupSocketServer(httpApp, httpsApp) {
        // @setupStatusServer()
        // @setupChangeEventServer()
        this.io = require('socket.io')(httpApp);
        httpApp.listen(edgecommonconfig_1.default.WebserverPort);
        if (httpsApp != null) {
            httpsApp.listen(edgecommonconfig_1.default.WebserverSSLPort);
            this.io.attach(httpsApp);
            console.log(`Listening on port ${edgecommonconfig_1.default.WebserverSSLPort}`);
        }
        this.io.on('connection', (socket) => {
            edgecommonconfig_1.default.status('setupSocketServer new incoming connection');
            this.socketCount++;
            // Create a placeholder for the user
            socket.myConnectionID = ++this.globalUnique;
            this.allClients[socket.myConnectionID] = new ApiSocketUser_1.default(socket.myConnectionID, socket);
            this.allClients[socket.myConnectionID].doInitUser();
            // Send the list that doesn't change
            this.allClients[socket.myConnectionID].send('api-list', this.webServer.gateway.apiCallList);
            this.sendApiList(this.allClients[socket.myConnectionID]);
            socket.on('register', (apiDefinition) => {
                try {
                    console.log(socket.myConnectionID, 'Register:', apiDefinition);
                    this.onRegisterApiFunction(apiDefinition, this.allClients[socket.myConnectionID]);
                }
                catch (e) {
                    edgecommonconfig_1.default.reportError('Error in register command:', e);
                }
                return true;
            });
            socket.on('noadmin_status', (data) => {
                this.allClients[socket.myConnectionID].setNoAdmin();
                return true;
            });
            socket.on('config_sendflat', (data) => {
                console.log('Enable sendflat mode for user');
                this.allClients[socket.myConnectionID].setConfigFlat();
                return true;
            });
            socket.on('admin_status', (data) => {
                //  Admin mode enabled, get status pushes
                console.log('Received admin mode');
                this.setupStatusServer();
                this.allClients[socket.myConnectionID].setAdmin();
                this.allClients[socket.myConnectionID].setWantStatusUpdates();
                for (const msg of Array.from(this.statusUpdates)) {
                    this.allClients[socket.myConnectionID].sendPush('status-update', msg);
                }
                for (const msg of Array.from(this.statusMessages)) {
                    this.allClients[socket.myConnectionID].sendPush('status-update', msg);
                }
                for (const msg of Array.from(this.statusChanges)) {
                    this.allClients[socket.myConnectionID].sendPush('status-update', msg);
                }
                return true;
            });
            socket.on('publish', (data) => {
                console.log('Received publish');
                for (const idx in this.allClients) {
                    const client = this.allClients[idx];
                    if (client.wantStatusUpdate) {
                        console.log('Sending publish data to client');
                        client.sendPush('publish', data);
                    }
                }
                return true;
            });
            socket.on('cmd', (data) => {
                try {
                    this.allClients[socket.myConnectionID].logIncoming(data.callName, data);
                    let { callName } = data;
                    if (callName != null) {
                        delete data.callName;
                        this.MakeDynamicCall(this.allClients[socket.myConnectionID], callName, data);
                    }
                }
                catch (e) {
                    edgecommonconfig_1.default.reportError(`Error in cmd:${JSON.stringify(data)}`, e);
                }
                return true;
            });
            socket.on('subscribe', (data) => {
                // subscribe to a path for changes
                this.setupChangeEventServer();
                this.allClients[socket.myConnectionID].addSubscription(data);
                return true;
            });
            socket.on('disconnect', () => {
                // Decrease the socket count on a disconnect, emit
                this.socketCount--;
                this.removeRegistrations(this.allClients[socket.myConnectionID]);
                // console.log "Remove client ", socket.myConnectionID
                delete this.allClients[socket.myConnectionID];
                return true;
            });
            return true;
        });
        return true;
    }
}
exports.default = ApiSocketServer;
//# sourceMappingURL=ApiSocketServer.js.map