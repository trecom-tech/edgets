class ViewNodeDocs extends View

	getLayout: () =>
		return
			view     : "Docked"
			size     : 200
			location : "top"
			docked   :
				view: "Form"
				name: "viewForm"
				inputs: [
					name: "select_key"
					label: "Choose Search Key"
				]
				buttons: [
					type: "submit"
					text: "Search"
				]
				onSubmit: @setSrcIframe
			main    :
				view: "IFrame"
				name: "viewIFrame"
				src: "https://npms.io/search"

	constructor: () ->
		super()

		# list of search keys
		@moduleList = []

		@gid = 'nodedocs-' + GlobalValueManager.NextGlobalID()

	onShowScreen: () =>
		@createSelectBox()
		true

	setData: (options = {}) =>
		if options.id?
			@gid = options.id
		if options.modules?
			for module in options.modules
				@addModule module.key, module.url

		true

	createSelectBox: () =>
		if @getModules().length > 0
			@viewForm.getValues()
			keys = @moduleList.map (m) ->
				return m.key
			if @viewForm.select_key.dataFormatter? then return false
			@viewForm.select_key.setOptions(keys).setDataFormatter("enum")
			return true		
		false

	getModules: () =>
		keys = localStorage.getItem @gid
		if keys? and keys isnt ""
			@moduleList = JSON.parse keys
		else
			@moduleList = []
		@moduleList

	addModule: (key, url) =>
		if !@moduleList? then @moduleList = @getModules()
		if !key? then return @moduleList
		if !url? or url is "" then url = "https://npms.io/search?q=#{key}"

		obj = 
			key: key
			url: url
		@moduleList.push obj

		# store at most 10 keys
		if @moduleList.length > 10 
			@moduleList.shift()		
		localStorage.setItem @gid, JSON.stringify(@moduleList)
		obj

	setSrcIframe: (form) =>
		if !@viewIFrame? then return false

		key = form.select_key.getValue()
		for module in @moduleList
			if module.key is key 
				@viewIFrame.setSource module.url
				return true

		module = @addModule key 
		@viewIFrame.setSource module.url 

		true

	serialize: () =>
		return
			id: @gid
			modules: @moduleList

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? or typeof options isnt "object"
				reject "ViewNodeDocs: invalid data passed"

			if options.id?
				@gid = options.id

			if options.modules?
				for module in options.modules
					@addModule module.key, module.url

			@createSelectBox()

			resolve true
