import ApiSocketServer from './ApiSocketServer';
export declare function startServer(isBasicAuthEnabled?: boolean, strRealm?: string): ApiSocketServer;
export { default as ApiProvider } from './ApiProvider';
export { default as ApiSocketUser } from './ApiSocketUser';
export { EdgeError } from './EdgeError';
