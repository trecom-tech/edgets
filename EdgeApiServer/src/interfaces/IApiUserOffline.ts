import IApiUser  from './IApiUser';

export class IApiUserOffline extends IApiUser{

    public addSubscription(data: any): boolean {
        return true;
    };

    public isLoaded(): boolean {
        return !!this.rec;
    }
}

export default IApiUserOffline;