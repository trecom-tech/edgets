#!/bin/sh

##
##  Testing against Azure server carpg1
##  Using local SSH Tunnel for connections
##

export EDGE_DEBUG=

export mongodb='{"url": "mongodb://sa:edge5516789@localhost/admin", "options": {"poolSize": 8,"autoReconnect": true, "noDelay": true,"forceServerObjectId": true,"ignoreUndefined": true,"authSource": "admin" } }'
export influxdb="{type:'influxdb',host:'127.0.0.1',username:'admin',password:'5Ssb1ARQWo'}"
export redisHost="redis://:8d1b846a8b9c46e9e1562733cd483f19@localhost:6379" 
export redisHostWQ="redis://:8d1b846a8b9c46e9e1562733cd483f19@localhost:6379" 
export redisReadHost="redis://:8d1b846a8b9c46e9e1562733cd483f19@localhost:6379" 
export mqHost="amqp://edge:Edge000199@127.0.0.1:5672"

export API_SERVER=http://127.0.0.1:8001 
ts-node server.ts


                
