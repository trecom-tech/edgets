## JobServer

The [Job Server](src/JobServer.coffee) is meant to run jobs on a schedule.

* JobServer looks at the dataSet "os" in the collection "jobs"
* JobServer logs to the dataSet "logs" in the collection "jobrun"
* JobServer runs npm install or npm update for all jobs before launching them.
* Jobs run not more often than desired but can run less often if the server
is too busy.   There are n slots available (4 currently) to run jobs.  Adding
slots will take more CPU, more database, and more resources but will make jobs
run more often.

## Tools

* [Job Tool](UnderstandingJobTool.md) - A command line tool to help with jobs