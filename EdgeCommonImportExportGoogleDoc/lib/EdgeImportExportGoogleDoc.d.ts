export default class EdgeImportExportGoogleDoc {
    sheetId: string;
    cacheFile: any;
    maxCacheAge: any;
    credentials: any;
    constructor(credentialKey: string, sheetId: string, cacheFile?: any, maxCacheAge?: any);
    /**
     * Batch updates document
     * https://developers.google.com/sheets/api/guides/batchupdate
     */
    doReadSheets(ranges?: string): Promise<{}>;
    doReadDoc(range: string, dataFormat?: string): Promise<{}>;
    /**
     * Read a part of the document using the range passed in
     * Range examples: "A1" - Default sheet, cell A1 only.
     */
    doReadDocFormulas(range: string): Promise<{}>;
    /**
     * Get the OAuth2 object based on the stored credentials
     * See the util/README file for information on how to generate the values.
     */
    private getAuth;
}
