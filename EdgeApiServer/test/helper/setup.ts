// Do setup for testing.
import EdgeApi         from 'edgeapi';

// Start server for testing
import './server';

const cache = EdgeApi.getRedisConnection();
cache.flushall();