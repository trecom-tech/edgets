#!/usr/bin/env ts-node

import * as yargs       from 'yargs';
import { quote }        from 'shell-quote';
import { config }       from 'edgecommonconfig';
import LGBotLib         from '../src/LGBotLib';

const titleCase         = require('title-case');
const EasyTable         = require('easy-table');

const args = process.argv;
args.shift(); // remove ts-node
args.shift(); // remove name of script lg.ts

if (args.length === 0) {
    console.log('Missing command, try lg help');
    process.exit();
}

const argv         = yargs.argv; // Arguments to string
const search       = quote(argv._);
let flagShowJson   = (argv.json)    ? true : false;
let flagShowTable  = (argv.table)   ? true : false;
let flagShowNormal = (argv.normal)  ? true : false;
let flagShowIdList = (argv.idlist)  ? true : false;

const bot = new LGBotLib();
bot.doProcess(search, argv).then((result: any) => {
    if (!result) {
        console.log('Internal error: got nothing from bot');
        process.exit();
    }

    if (result.error !== false) {
        console.log('Error:', result.error);
        process.exit();
    }

    if (result.type === 'text') {
        console.log(result.data);
        process.exit();
    }

    if (!flagShowJson && !flagShowIdList && !flagShowTable && !flagShowNormal) {
        flagShowTable  = result.suggest === 'table';
        flagShowJson   = result.suggest === 'json';
        flagShowIdList = result.suggest === 'id';
    }

    // Output JSON if command line has --json
    if (flagShowJson) {
        console.log(JSON.stringify(result.data, null, 4));
    } else if (flagShowIdList) {
        // Output result as a set of IDs
        // This is best used with xargs to fee another command in a bash script.
        for (let record of result.data) {
            console.log(record.id);
        }
    } else if (flagShowTable) {
        // Output result as clean table
        const t = new EasyTable();
        for (let record of result.data) {
            for (let name in record) {
                const value = record[name];
                name = titleCase(name);

                if (typeof value === 'number') {
                    t.cell(name, value, EasyTable.number(0));
                } else {
                    t.cell(name, value);
                }
            }

            t.newRow();
        }

        console.log(t.toString());
    } else {
        // Nicely format using ninjadump
        if (Array.isArray(result.data)) {
            for (let idx in result.data) {
                const record = result.data[idx];
                config.dump(`Result ${idx}`, record);
            }
        } else {
            config.dump('Result:', result.data);
        }
    }

    process.exit();
}).catch(() => { /**/});