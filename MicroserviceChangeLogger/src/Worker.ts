import * as jsonfile                from 'jsonfile';
import { MongoClient }              from 'mongodb';
import BulkTableStorage             from 'edgebulktablestorage';
import DataSetManager               from 'edgedatasetmanager';
import { config }                   from 'edgecommonconfig';
import RuleProcessorMessageQueue    from './RuleProcessorMessageQueue';
import RuleProcessorWorkQueue       from './RuleProcessorWorkqueue';

const reDate1       = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T00.00.00.000Z/;
const reDate2       = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9]Z/;
const reDate3       = / [0-9][0-9] [12][0-9][0-9][0-9] /;
const pipeline      = [
    {
        $project: { documentKey: false }
    }
];

export default class Worker {
    
    log                 : any;
    callback            : any;
    changeStream        : any;
    changeLogsTable     : any;
    loggingTableName    : string;
    csvLogEnabled       : boolean;
    azureLogEnabled     : boolean;
    pathSkip            : any[];
    dmLookup            : any;
    rules               : any[];

    constructor(azureConfigName: string, azureTableName: string) {
        this.log              = config.getLogger('ChangeWorker');
        this.changeLogsTable  = new BulkTableStorage(azureConfigName, azureTableName);
        this.loggingTableName = azureTableName;
        this.dmLookup         = new DataSetManager('lookup');
        this.csvLogEnabled    = false;
        this.azureLogEnabled  = false;
        this.pathSkip         = [];
        this.rules            = [];
    }

    doRun(callback?: any) {

        if (callback && typeof callback === 'function') {
            this.callback = callback;
        }

        this._doSetupMongoChangeStream()
        .then(async () => {
            await this._doReloadRules();

            this.changeStream.on('change', async (change: any) => {
                const changeRec = this._processChange(change);
                this._doSaveChangesDatabase(changeRec)
                .then((success: any) => {
                    if (success && this.callback) {
                        this.callback(null, changeRec);
                    }
                })
                .catch((err: Error) => {
                    if (this.callback) {
                        this.callback(err, changeRec);
                    }
                });
            });
        })
        .catch((err) => {
            console.log(err);
            this.log.error('doRun', { err: err });

            if (this.callback) {
                this.callback(err, null);
            }
        });
    }

    setupSkipList() {
        const pathSkipList = jsonfile.readFileSync(config.FindFileInPath('skip_list.json', ['~/EdgeConfig/']));
        for (const str in pathSkipList) {
            try {
                const re = new RegExp(str, 'i');
                this.pathSkip.push(re);
            } catch (e) {
                console.log('Invalid Regexp:', str);
                this.log.error('Invalid Regexp:', { re: str });
            }
        }
    }

    private _doSaveChangesDatabase(changeRec: any) {
        return this.changeLogsTable.doInsertLogRecord(this.loggingTableName, changeRec);
    }

    private async _doReloadRules() {
        config.status('doReloadRules Reading lookup/change_rules');

        const result = await this.dmLookup.doGetItems('/change_rules', {});
        config.status('change_rules query result:', result);
        for (const rule of result) {
            config.status('doReloadRules Adding:', rule);

            try {
                if (rule.action === 'workqueue') {
                    this.rules.push(new RuleProcessorWorkQueue(rule));
                } else if (rule.action === 'messagequeue') {
                    this.rules.push(new RuleProcessorMessageQueue(rule));
                } else {
                    this.log.error('Invalid rule', { rule: rule });
                }
            } catch (err) {
                this.log.error('Error while initializing rules', err);
            }

        }

        return true;
    }

    private _processChange(change: any) {
        const changeRec: any    = {};
        changeRec.dataset     = change.ns.db;
        changeRec.collection  = change.ns.coll;
        changeRec.change_date = new Date();
        changeRec.kind        = change.operationType;
        changeRec.record_id   = String(change.fullDocument.id) || String(change.fullDocument._id);
        changeRec.path = `${change.ns.coll}/${changeRec.record_id}`;

        if (change.fullDocument.name) {
            changeRec.path += `/${change.ns.coll}/${change.fullDocument.name}`;
        }

        if (changeRec.record_id.length >= 64) {
            console.log(`Length error in record_id, '${changeRec.record_id}' > 64`);
            changeRec.record_id = changeRec.record_id.substring(0, 64);
        }

        if (changeRec.dataset.length > 32) {
			console.log(`Length error in dataset, '${changeRec.dataset}' > 32`);
            changeRec.dataset = changeRec.dataset.substring(0, 32);
        }

        if (changeRec.collection.length > 64) {
			console.log(`Length error in collection, '${changeRec.collection}' > 64`);
            changeRec.collection = changeRec.collection.substring(0, 64);
        }

        changeRec.stamp = new Date();
        
        for (const rule of this.rules) {
            if (!rule.active) {
                continue;
            }

            if (rule.ds_name && rule.ds_name !== changeRec.dataset) {
                continue;
            }

            rule.check(changeRec);
        }

        return changeRec;
    }

    private async _doSetupMongoChangeStream() {
        const mongoServer = config.getCredentials('MongoDB');

        if (!mongoServer || typeof mongoServer !== 'object') {
            throw new Error('Error: failed to load credentials');
        }

        const client = await MongoClient.connect(mongoServer.url_replicaSet, { useNewUrlParser: true });
        this.changeStream = client.watch(pipeline);
    }

}