/**
 * Class to normalized EdgeMailer Error.
 */
export declare class EdgeCommonMailerError extends Error {
    message: string;
    code: string;
    options: any;
    constructor(message: string, code?: string, options?: any);
}
export default EdgeCommonMailerError;
