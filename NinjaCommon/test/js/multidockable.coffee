$ ->

    addTestButton "Multi Docked Views Vertical", "Open", () ->
        addHolder()
        .setView "DockedMulti", (view) ->
            view.setSizes [100, 200, 0.4, 0.6]
            for i in [0..3]
                view.getHolder(i).setView "TestShowSize"

    addTestButton "Multi Docked Views Horizontal MinWidth", "Open", () ->
        addHolder()
        .setView "DockedMulti", (view) ->
            view.setDirection "H"
            view.setSizes [100, 200, 0.4, 0.6]
            view.getHolder(2).setMinWidth 300
            for i in [0..3]
                view.getHolder(i).setView "TestShowSize"

    addTestButton "Deserialize Multi Docked Views", "Open", () ->
        addHolder()
        .setView "DockedMulti", (view) ->
            view.deserialize
                direction: "H"
                docks: [
                    size: 100
                    view: "TestShowSize"
                ,
                    size: 200
                    view: "TestShowSize"
                ,
                    size: 0.4
                    view: "TestShowSize"
                ,
                    size: 0.6
                    view: "TestShowSize"
                ]    

    addTestButton "Multi Docked View Serialize/Deserialize", "Open", () ->
        addHolder()
        .setView "DockedMulti", (view) ->
            view.setDirection "H"
            view.setSizes [100, 200, 0.4, 0.6]
            view.getHolder(2).setMinWidth 300
            for i in [0..3]
                view.getHolder(i).setView "TestShowSize"

            setTimeout () ->
                obj = view.serialize()
                console.log "Serialized DockedMulti: ", obj
                doPopupView "DockedMulti", "DockedMulti Deserialize", null, 800, 600, (view) ->
                    view.deserialize obj
            , 2000

    go()
