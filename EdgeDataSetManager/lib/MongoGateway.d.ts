import { MongoClient, Db } from 'mongodb';
import MongoModel from './MongoModel';
export declare class MongoGateway {
    dbName: string;
    alternateConnectionConfig: any;
    connectPromise: Promise<void>;
    dbAdmin: MongoClient;
    db: Db;
    databaseReady: boolean;
    /**
     * Connect alternateConnectionConfig first and try to get MongoDB_{dbName}
     * If they are are all null, fall back to use MongoDB
     * Use Azure Cosmos db from when dbName is logs and changes, read config 'CosmosDB'
     * @param dbName db name
     * @param alternateConnectionConfig this db connection config is used when is not null rather than config.getCredential('MongoDB')
     */
    constructor(dbName: string, alternateConnectionConfig: any);
    close(): boolean;
    /**
     * Get the collection from the database
     * @param name [string] collection name
     */
    collection(name: string): MongoModel;
    static displayStatus(): void[];
    getConnectionStatus(): Promise<void>;
    /**
     * Connect to the database, should be called before every
     * call as it returns a promise
     *
     */
    doConnectToDatabase(): Promise<void>;
    doGetStats(): Promise<any>;
}
export default MongoGateway;
