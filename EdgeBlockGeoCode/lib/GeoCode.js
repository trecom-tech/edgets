"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgeblock_1 = require("edgeblock");
const edgecommonconfig_1 = require("edgecommonconfig");
const crypto = require("crypto");
const edgeapi_1 = require("edgeapi");
const Geocodio = require('geocodio');
const MapboxClient = require('mapbox');
class GeoCode extends edgeblock_1.default {
    constructor(...args) {
        super(...args);
    }
    getInputs() {
        return ["Normalized Address"];
    }
    //
    //  Initialize the results that we are going to return
    //  this is mostly a reminder of the template
    initResult() {
        let result = {
            _lastModified: new Date(),
            source: "unknown",
            confidence: 0,
            lat: null,
            lon: null,
            address: {},
            other: {}
        };
        return result;
    }
    //
    //  Convert a string address to an address object which can be used for lookup functions
    internalConvertAddressToObject(strAddress) {
        const address = {
            display: "",
            number: 0,
            street: "",
            city: "",
            state: "",
        };
        if (strAddress == null) {
            return null;
        }
        if (typeof strAddress === "string") {
            address.display = strAddress;
        }
        else if (typeof strAddress === "object") {
            for (const varName in strAddress) {
                const value = strAddress[varName];
                address[varName] = value;
            }
        }
        return address;
    }
    //
    //
    doGetCensus(address) {
        return __awaiter(this, void 0, void 0, function* () {
            if (GeoCode.useEdgeApi) {
                this.updateStatsToday("GeoCode", { "CensusData": 1 });
            }
            address = this.internalConvertAddressToObject(address);
            const host = "https://geocoding.geo.census.gov/geocoder/locations/onelineaddress";
            const result = yield this.doGetLink(host, {
                address: address.display,
                format: "json",
                benchmark: 8
            });
            if ((result != null) && (result.result != null) && (result.result.addressMatches != null)) {
                const matches = result.result.addressMatches;
                if ((matches[0] != null) && (matches[0].coordinates != null)) {
                    edgecommonconfig_1.default.status("Found Census");
                    const output = this.initResult();
                    output.source = "CensusData";
                    output.lon = matches[0].coordinates.x;
                    output.lat = matches[0].coordinates.y;
                    output.address.display = matches[0].matchedAddress;
                    output.confidence = 100;
                    if (matches[0].addressComponents != null) {
                        output.address.city = matches[0].addressComponents.city;
                        output.address.state = matches[0].addressComponents.state;
                        output.address.zip = matches[0].addressComponents.zip;
                    }
                    return output;
                }
            }
            return null;
        });
    }
    //
    //  Location lookup with OpenCage
    doGetOpenCage(address) {
        return __awaiter(this, void 0, void 0, function* () {
            address = this.internalConvertAddressToObject(address);
            if (GeoCode.useEdgeApi) {
                this.updateStatsToday("GeoCode", { "OpenCage": 1 });
            }
            const result = yield this.doGetLink("http://api.opencagedata.com/geocode/v1/json", {
                q: address.display,
                key: "d2ed58adefe9edb46a80739394f5a82f"
            });
            if ((result != null) && (result.results != null) && (result.results.length > 0)) {
                const bestResult = result.results.sort((a, b) => b.confidence - a.confidence).shift();
                // config.dump "OpenCage:", bestResult
                if ((bestResult.formatted != null) && (bestResult.components._type === "building")) {
                    const output = this.initResult();
                    output.address.display = bestResult.formatted.replace(", United States of America", "");
                    output.address.number = bestResult.components.house_number;
                    output.address.zip = bestResult.components.postcode;
                    output.address.street = bestResult.components.road;
                    output.address.state = bestResult.components.state;
                    output.address.city = bestResult.components.town;
                    output.address.county = bestResult.components.county;
                    output.lat = bestResult.geometry.lat;
                    output.lon = bestResult.geometry.lng;
                    output.source = "OpenCage";
                    output.confidence = bestResult.confidence;
                    output.other.annotations = bestResult.annotations;
                    edgeapi_1.default.cleanupObject(output);
                    return output;
                }
            }
            return null;
        });
    }
    //
    //  Location lookup with GeoCodio
    doGetGeocodio(address) {
        return new Promise((resolve, reject) => {
            address = this.internalConvertAddressToObject(address);
            if (GeoCode.useEdgeApi) {
                this.updateStatsToday("GeoCode", { "GeoCodio": 1 });
            }
            const geocodio = new Geocodio({ api_key: "5878d6de6d26e7e8f2de2c37e36f2ed368556d1" });
            // geocodio.get 'parse', {q: address.display }, (err, result)=>
            //     console.log "PARSE ERR:", err
            //     console.log "PARSE RESULT:", result
            // console.log "ADDR:", address.display
            return geocodio.get("geocode", { q: address.display }, (err, location) => {
                if (err) {
                    this.log.error("Geocodio Error", err);
                    resolve(null);
                }
                if (typeof location === "string") {
                    location = JSON.parse(location);
                }
                // config.dump "Geocodia Census Result:", err, location
                // config.inspect location
                const output = this.initResult();
                if ((location != null) && (location.results != null)) {
                    for (let result of location.results) {
                        if (result.accuracy >= 0.9) {
                            output.address.number = result.address_components.number;
                            output.address.street = result.address_components.formatted_street;
                            output.address.city = result.address_components.city;
                            output.address.state = result.address_components.state;
                            output.address.zip = result.address_components.zip;
                            output.address.county = result.address_components.county;
                            if (result.formatted_address != null) {
                                output.display = result.formatted_address;
                            }
                            output.lat = result.location.lat;
                            output.lon = result.location.lng;
                            output.confidence = result.accuracy * 10;
                            output.source = "GeoCodio";
                            resolve(output);
                        }
                    }
                }
                resolve(null);
            });
        });
    }
    doGetMapbox(address) {
        return __awaiter(this, void 0, void 0, function* () {
            address = this.internalConvertAddressToObject(address);
            if (GeoCode.useEdgeApi) {
                this.updateStatsToday("GeoCode", { "Mapbox": 1 });
            }
            const client = new MapboxClient('pk.eyJ1IjoiYnJpYW5wb2xsYWNrIiwiYSI6IjUwZDQ0OWFkMDNhNTgyNjdlZmMyOWM1YTRhMzVmYjRjIn0.K0Pbg-Jo9bHGyxNvhtfVOQ');
            const result = yield client.geocodeForward(address.display);
            if ((result == null) || (result.entity == null)) {
                return null;
            }
            const output = this.initResult();
            output.source = "Mapbox";
            let seenAddress = false;
            for (let item of result.entity.features) {
                if ((item.place_type[0] === "address") && (seenAddress === false)) {
                    output.address.display = item.place_name;
                    output.lat = item.center[1];
                    output.lon = item.center[0];
                    output.confidence = item.relevance;
                    for (let c of item.context) {
                        if (/region/.test(c.id)) {
                            output.address.state = c.text;
                        }
                        if (/neighborhood/.test(c.id)) {
                            output.address.subdivision = c.text;
                        }
                        if (/place/.test(c.id)) {
                            output.address.city = c.text;
                        }
                        if (/postcode/.test(c.id)) {
                            output.address.zip = c.text;
                        }
                    }
                    seenAddress = true;
                }
                if (item.place_type[0] === "postcode") {
                    output.address.zip = item.text;
                }
            }
            return output;
        });
    }
    doGetMapboxReverse(lat, lon) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = new MapboxClient('pk.eyJ1IjoiYnJpYW5wb2xsYWNrIiwiYSI6IjUwZDQ0OWFkMDNhNTgyNjdlZmMyOWM1YTRhMzVmYjRjIn0.K0Pbg-Jo9bHGyxNvhtfVOQ');
            const result = yield client.geocodeReverse({ latitude: lat, longitude: lon });
            //
            //  Same exact processing as above Mapbox Formward request
            if ((result == null) || (result.entity == null)) {
                return null;
            }
            const output = this.initResult();
            output.source = "Mapbox";
            let seenAddress = false;
            for (let item of result.entity.features) {
                if ((item.place_type[0] === "address") && (seenAddress === false)) {
                    output.address.display = item.place_name;
                    output.lat = item.center[1];
                    output.lon = item.center[0];
                    output.confidence = item.relevance;
                    for (let c of item.context) {
                        if (/region/.test(c.id)) {
                            output.address.state = c.text;
                        }
                        if (/neighborhood/.test(c.id)) {
                            output.address.subdivision = c.text;
                        }
                        if (/place/.test(c.id)) {
                            output.address.city = c.text;
                        }
                        if (/postcode/.test(c.id)) {
                            output.address.zip = c.text;
                        }
                    }
                    seenAddress = true;
                }
                if (item.place_type[0] === "postcode") {
                    output.address.zip = item.text;
                }
            }
            return output;
        });
    }
    //
    //  Convert an address text into address parts
    //
    process(address) {
        return __awaiter(this, void 0, void 0, function* () {
            if (module.exports.devMode) {
                this.log.info("GeoCoding address", address);
            }
            let api, hash;
            let location;
            address = this.internalConvertAddressToObject(address);
            //
            //  Lookup the Geocode information
            // if !address or !address.number or isNaN(address.number)
            if (!address || !address.number) {
                return { error: "Invalid address, no house number" };
            }
            address.display = address.display.replace("  ", " ");
            if (!/,/.test(address.display)) {
                address.display = [address.display, address.city, address.state, address.zip].join(", ");
            }
            //
            //  Step 1- Attempt to locate the address's hash in the existing
            //  database.   If so, don't need to lookup
            if (address.hash == null) {
                hash = crypto.createHash('md5');
                hash.update(address.display);
                address.hash = hash.digest('hex');
            }
            if (GeoCode.useEdgeApi) {
                api = yield this.doGetApi();
                const items = yield api.data_doGetItems("lookup", `/GeoCode/${address.hash}`, {}, {}, 0, 1);
                if ((items != null) && (items.length > 0)) {
                    location = items[0];
                    if (location.source != null) {
                        return location;
                    }
                    //
                    // Old hash from RR Portal 2.0
                    if ((location.street != null) && (location.number != null)) {
                        const output = this.initResult();
                        output.lat = parseFloat(location.lat);
                        output.lon = parseFloat(location.lon);
                        return output;
                    }
                }
            }
            //
            //  Step 2- Loop through different options
            //
            location = null;
            if (location == null) {
                location = yield this.doGetCensus(address);
            }
            if (location == null) {
                //
                //  Lookup with GeoCodio
                location = yield this.doGetGeocodio(address);
            }
            if (location == null) {
                //
                //  Attempt to find the address using OpenCage API
                location = yield this.doGetOpenCage(address);
            }
            if (location == null) {
                //
                //  Attempt to use Mapbox
                location = yield this.doGetMapbox(address);
            }
            //
            //  If we found a location, update the cache and return it
            //
            if ((location != null) && location.lon && location.lat) {
                // console.log "Saving to /GeoCode/#{address.hash}"
                if (GeoCode.useEdgeApi && (address.hash != null)) {
                    yield api.data_doUpdatePath("lookup", `/GeoCode/${address.hash}`, location);
                }
                return location;
            }
            else {
                // console.log "Error Saving to /GeoCode/#{address.hash}", location
                if (GeoCode.useEdgeApi) {
                    yield api.data_doUpdatePath("lookup", `/GeoCode/${address.hash}`, { error: "Unable to find location" });
                }
                return { error: "Unable to find location" };
            }
        });
    }
}
GeoCode.useEdgeApi = true;
exports.GeoCode = GeoCode;
exports.default = GeoCode;
//# sourceMappingURL=GeoCode.js.map