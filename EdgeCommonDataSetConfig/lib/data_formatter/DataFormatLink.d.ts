import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatLink extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    clickable: boolean;
    align: string;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): null;
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
    onFocus(e: any, col: any, data: any): boolean;
}
export default DataFormatLink;
