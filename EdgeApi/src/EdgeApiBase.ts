/**
 * This is the base class for calling the Edge API Server
 * The api can expose RPC functions and it can consume functions
 */
import * as EvEmitter           from 'events';
import { config }               from 'edgecommonconfig';
import EdgeApiProvider          from './EdgeApiProvider';
import { EdgeSubscription } from './EdgeApi';

const DEFAULT_API_USERNAME = 'edgeApi';
const DEFAULT_API_PASSWORD = 'Hr^YQ5yh!2eM;QbV';

export abstract class EdgeApiBase extends EdgeApiProvider {
    subscriptionList: EdgeSubscription[]                   = [];
    registeredList: any[]                                  = [];
    connectPromise: Promise<boolean>                       = null;
    waitForPromises: { [index: string]: Promise<boolean> } = null;
    emitter                                                = new EvEmitter();
    isVerbose                                              = false;
    commandCallback                                        = {};
    listeners: any                                         = {};

    constructor() {
        super();
    }

    /**
     * Establish a socket connection to the server
     * Allow registering for push message actions
     * @param pushMessage
     * @param callback
     */
    on(pushMessage: string, callback: any) {
        return this.emitter.on(pushMessage, callback);
    }

    listen(channel: string, callback: any) {
        this.listeners[channel] = callback;
    }

    /**
     * Stop a push message action
     * @param pushMessage
     * @param callback
     */
    off(pushMessage: string, callback: any) {
        return this.emitter.off(pushMessage, callback);
    }

    /**
     * Single callback for an event
     * @param pushMessage
     * @param callback
     */
    once(pushMessage: string, callback: any) {
        return this.emitter.on(pushMessage, callback);
    }

    /**
     *
     * @param socket
     */
    onConnect(socket: SocketIOClient.Socket) {
        // Nothing
        config.status('EdgeApiBase::onConnect');
        return true;
    }

    /**
     *
     */
    onDisconnect() {
        // Nothing
        config.status('EdgeApiBase::onDisconnect');
        return true;
    }

    onReady() {
        // Nothing
        config.status('EdgeApiBase::onReady');
        return true;
    }

    /**
     *
     * @param socketConnectCallbackList
     */
    doGetSocket(socketConnectCallbackList: Function[]): Promise<any> {
        throw new Error('Parent must implement');
    }

    // tslint:disable-next-line:no-empty
    onCmdReplyComplete() {
    }

    processChangeEvent(data: any) {
        if (this.listeners[data.channel]) {
            this.listeners[data.channel](data.data);
        }
        return true;
    }

    onInitializeSocket() {
        return new Promise((resolve, reject) => {
            this.socket.on('connect', () => {
                config.status('EdgeApiBase::onInitializeSocket \'connect\'');

                // Send/Resend current subscriptions
                for (const sub of this.subscriptionList) {
                    this.socket.emit('subscribe', sub);
                }

                resolve(true);
                this.onConnect(this.socket);
                this.onReconnect(this.socket);
                return true;
            });

            this.socket.on('disconnect', () => {
                config.status('EdgeApiBase::onInitializeSocket \'disconnect\'');
                this.onDisconnect();
                this.waitForPromises = {};
                return true;
            });

            this.socket.on('change', (data: any) => {
                config.status('EdgeApiBase::onInitializeSocket \'change\'');
                // console.log "CHANGE MATCH=", data
                this.processChangeEvent(data);
                return true;
            });

            return this.socket.on('cmd-push', (data: any) => {
                config.status('EdgeApiBase::onInitializeSocket \'cmd-push\'');
                // incoming push message
                let command = data.command.toString();
                // console.log "Push received [#{command}] = ", data
                this.emitter.emit(command, [data.data]);
                return true;
            });
        });
    }

    /**
     * Wait until callName gets available
     * @param callName string
     */
    async doWaitForApi(callName: string): Promise<boolean> {
        if (!this.waitForPromises) {
            this.waitForPromises = {};
        }

        if (this.waitForPromises[callName]) {
            return this.waitForPromises[callName];
        }

        this.waitForPromises[callName] = new Promise((resolve, reject) => {
            let checkAvailable: Function;
            return (checkAvailable = () => {
                if (this[callName] != null) {
                    return resolve(true);
                } else {
                    return setTimeout(checkAvailable, 10);
                }
            })();
        });

        return this.waitForPromises[callName];
    }

    /**
     * Create socket and wait to be connected.
     */
    doConnect() {
        config.status('EdgeApiBase doConnect Starting');
        if (this.connectPromise != null) {
            return this.connectPromise;
        }

        return (this.connectPromise = this.doConnectPromise());
    }

    async doConnectPromise() {
        await this.doGetSocket([ this.onSetupSocket, this.onInitializeSocket, ]);
        await this.doWaitForApi('os_doGetServerInformation');
        config.status('EdgeApiBase doConnect Connected');

        // Authenticate using serviceUser
        const apiUsername = config.getCredentials('apiUsername') || process.env.API_USERNAME || DEFAULT_API_USERNAME;
        const apiPassword = config.getCredentials('apiPassword') || process.env.API_PASSWORD || DEFAULT_API_PASSWORD;

        const result: any = await this.auth_doAuthUser(apiUsername, apiPassword);
        if (result.error) {
            throw new Error(result.error);
        }

        return true;
    }

    /**
     * Subscribe to a given path
     * @param channel
     */
    subscribe(channel: string) {
        // Prevent re-subsubscribing
        for (const sub of this.subscriptionList) {
            if (sub.channel === channel) {
                return true;
            }
        }

        const sub = { channel };

        this.socket.emit('subscribe', sub);
        this.subscriptionList.push(sub);
        return true;
    }

    /**
     * Send a command to the server
     * @param command
     * @param options
     */
    sendCommand(command: string, options: any): boolean {
        if (typeof options !== 'object') {
            options = {};
        }

        this.socket.emit(command, options);
        return true;
    }
}

export default EdgeApiBase;
