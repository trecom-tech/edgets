class ViewFileManager extends View

	getLayout: ()=>
		view: "DockedToolbar"
		items: [
			type:     "button"
			text:     "Upload file"
			icon:     "cloud-upload"
			callback: @onButtonUploadFile
		,
			type:     "button"
			text:     "Download"
			icon:     "cloud-download"
			callback: @onButtonDownloadFile
		,
			type:     "button"
			text:     "Delete"
			icon:     "trash-alt"
			callback: @onButtonDeleteFile
		,
			type:     "button"
			text:     "Refresh"
			icons:    "refresh"
			callback: @onButtonRefresh
		]
		main:
			view     : "Docked"
			size     : 80
			location : "top"
			name     : "mainWrapper"
			docked   :
				view: "DragFileTarget"
				name: "viewDragFileTarget"

	constructor: () ->
		super()
		@fileList     = []
		@folderList   = []
		@username     = '@guest'
		@dataPath     = ''
		@baseDataPath = ''
		@currentPath  = '/'
		@history      = []

		## common icon list for file types
		@iconList = 
			"none" 	: "<img src='/images/fileicons/fileicon_bg.png' width='64' height='64' alt='Default FileType' />"
			"js" 	: "<img src='/images/fileicons/developer.png' width='64' height='64' alt='JavaScript FileType' />"
			"coffee": "<img src='/images/fileicons/developer.png' width='64' height='64' alt='CoffeeScript FileType' />"
			"ts" 	: "<img src='/images/fileicons/developer.png' width='64' height='64' alt='TypeScript FileType' />"
			"java" 	: "<img src='/images/fileicons/developer.png' width='64' height='64' alt='Java FileType' />"
			"cpp" 	: "<img src='/images/fileicons/developer.png' width='64' height='64' alt='C++ FileType' />"
			"html" 	: "<img src='/images/fileicons/html.png' width='64' height='64' alt='HTML FileType' />"
			"htm" 	: "<img src='/images/fileicons/html.png' width='64' height='64' alt='HTML FileType' />"
			"html5" : "<img src='/images/fileicons/html.png' width='64' height='64' alt='HTML5 FileType' />"
			"css"	: "<img src='/images/fileicons/css.png' width='64' height='64' alt='CSS FileType' />"
			"jpg"	: "<img src='/images/fileicons/image.png' width='64' height='64' alt='JPEG FileType' />"
			"bmp" 	: "<img src='/images/fileicons/image.png' width='64' height='64' alt='BMP FileType' />"
			"jpeg"	: "<img src='/images/fileicons/image.png' width='64' height='64' alt='JPEG FileType' />"
			"png"	: "<img src='/images/fileicons/image.png' width='64' height='64' alt='PNG FileType' />"
			"psd"	: "<img src='/images/fileicons/photoshop.png' width='64' height='64' alt='Photoshop FileType' />"
			"gif"	: "<img src='/images/fileicons/image.png' width='64' height='64' alt='GIF FileType' />"
			"pdf"	: "<img src='/images/fileicons/pdf.png' width='64' height='64' alt='PDF FileType' />"
			"mp3"	: "<img src='/images/fileicons/music.png' width='64' height='64' alt='MP3 FileType' />"
			"wav"	: "<img src='/images/fileicons/music.png' width='64' height='64' alt='WAV FileType' />"
			"aac"	: "<img src='/images/fileicons/music.png' width='64' height='64' alt='AAC FileType' />"
			"ogg"	: "<img src='/images/fileicons/music.png' width='64' height='64' alt='OGG FileType' />"
			"xls"	: "<img src='/images/fileicons/excel.png' width='64' height='64' alt='Excel FileType' />"
			"xlt"	: "<img src='/images/fileicons/excel.png' width='64' height='64' alt='Excel FileType' />"
			"xlm"	: "<img src='/images/fileicons/excel.png' width='64' height='64' alt='Excel FileType' />"
			"xlsx"	: "<img src='/images/fileicons/excel.png' width='64' height='64' alt='Excel FileType' />"
			"csv"	: "<img src='/images/fileicons/excel.png' width='64' height='64' alt='Excel FileType' />"
			"doc"	: "<img src='/images/fileicons/word.png' width='64' height='64' alt='Word FileType' />"
			"docx"	: "<img src='/images/fileicons/word.png' width='64' height='64' alt='Word FileType' />"
			"docm"	: "<img src='/images/fileicons/word.png' width='64' height='64' alt='Word FileType' />"
			"rtf"	: "<img src='/images/fileicons/word.png' width='64' height='64' alt='Word FileType' />"
			"ppt"	: "<img src='/images/fileicons/powerpoint.png' width='64' height='64' alt='PowerPoint FileType' />"
			"pot"	: "<img src='/images/fileicons/powerpoint.png' width='64' height='64' alt='PowerPoint FileType' />"
			"pps"	: "<img src='/images/fileicons/powerpoint.png' width='64' height='64' alt='PowerPoint FileType' />"
			"zip"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='ZIP FileType' />"
			"rar"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='Rar FileType' />"
			"tar"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='TAR FileType' />"
			"7z"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='7z FileType' />"
			"iso"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='ISO FileType' />"
			"arj"	: "<img src='/images/fileicons/compressed.png' width='64' height='64' alt='ARJ FileType' />"
			"txt"	: "<img src='/images/fileicons/text.png' width='64' height='64' alt='Text FileType' />"
			"avi"   : "<img src='/images/fileicons/movie.png' width='64' height='64' alt='AVI FileType' />"
			"mp4"   : "<img src='/images/fileicons/movie.png' width='64' height='64' alt='MP4 FileType' />"
			"mov"   : "<img src='/images/fileicons/movie.png' width='64' height='64' alt='MOV FileType' />"
			"dir"   : "<img src='/images/fileicons/folder.png' width='64' height='64' alt='Folder' />"

		GlobalClassTools.addEventManager(this)

	##|
	##|  Show the file manager
	show: ()=>

	onShowScreen: ()=>
		##
		## set up events for the view
		@fileContainer     = @mainWrapper.getBody()
		@fileContainer.css "overflow-y", "auto"
		@folderListWrapper = @fileContainer.add "ul", "ninja-fm-filelist"
		@fileListWrapper   = @fileContainer.add "ul", "ninja-fm-filelist"

		@goBackWrapper     = @folderListWrapper.add "li", "ninja-fm-goback"
		@goBackText        = @goBackWrapper.add "div", "ninja-fm-goback-text"
		@goBackText.html "<span><i class='far fa-level-up'></i>   Previous Folder</span>"
		@goBackText.on "click", @goBack
		@goBackText.on "dblclick", @goBack

		@selectedFiles     = []

		@viewDragFileTarget.on "file_complete", (fileList)=>
			@onButtonRefresh()

		@onButtonRefresh()
		true

	## set name of file list
	setName: (tableName) =>
		true

	## set path of upload files
	setPath: (dataPath, updateBasePath = true) =>
		dataPath = @getValidDataPath dataPath
		if updateBasePath is true
			@baseDataPath = dataPath
		@dataPath     = dataPath
		@viewDragFileTarget.setPath @dataPath

	## set username
	setUsername: (username) =>
		@username = username
		@viewDragFileTarget.setUsername @username

	## file download button
	onButtonDownloadFile: (number)=>
		if number? and typeof number isnt "object"
			# console.log @fileList[number]
			@downloadFile @fileList[number]
		else if number?
			for id in @selectedFiles
				# console.log @fileList[id]
				@downloadFile @fileList[id]

	downloadFile: (fobj)=>
#		template = document.createElement('template')
#		html = '<form target="_blank" action="/filedownload/" method="post"><input name="fpath" value="'+fobj.path+'"><input name="fmime" value="'+fobj.mime_type+'"></form>'.trim()
#		template.innerHTML = html;
#		form = template.content.firstChild
#		document.body.appendChild form
#		form.submit()

		a = document.createElement("a")
		# strPath = "/filemanager/filedownload/" + @dataPath + "/" + fobj.path
		# strPath = strPath.replace("//", "/")
		# strPath = strPath.replace("//", "/")
		strPath = fobj.link
		# console.log "DOWNLOAD:", strPath
		a.href = strPath
		a.download = fobj.filename
		a.click()

	## file upload button
	onButtonUploadFile: ()=>
		@viewDragFileTarget.openFileUpload()

	##|
	##|  Auto refresh on tab show
	onTabShow: ()=>
		setTimeout @onButtonRefresh, 100

	## Refresh button callback
	onButtonRefresh: ()=>

		##|
		##| brian - Don't request from the server if there is no data path
		if (!@baseDataPath? or @baseDataPath == "") and (!@dataPath? or @dataPath == "")
			return true

		formData = new FormData
		formData.append 'datapath', @baseDataPath or @dataPath
		$.ajax
			xhr: ()=>
				xhr = new window.XMLHttpRequest()
				return xhr
			url: '/filemanager/getfilelist'
			method: 'post'
			data: formData
			processData: false
			contentType: false
			success: (response)=>
				if response? and typeof response == "string"
					response = JSON.parse(response)

				##|
				##|  Remove old list
				$.each @fileListWrapper.children, (i, ele)->
					$(ele.element).remove()

				@reset()

				console.log "ViewFileManager, onButtonRefresh response data=", response
				if response? and response.data?
					for file in response.data
						@addFile
							name:        file.original_filename
							path:        file.filename
							mime_type:   file.mime_type
							size:        file.file_size
							user:        file.username
							date:        file.timestamp
							description: file.description
							link:        file.link
							subpath:     file.subpath

	## 
	## Add a file to FileList
	## @param [Object] fileData: should have fields - name, created date, size of file
	## @return [Boolean] true if the new file is successfully added into DataMap
	addFile: (fileData) =>
		if !fileData or typeof(fileData) isnt "object" then return false
		unless fileData.name then return false

		## if this file is in subfolder
		## add it to the subfolder
		subpath         = fileData.subpath
		if subpath? and subpath[0] isnt "/" then subpath = "/" + subpath
		if subpath? and subpath isnt ""
			@addSubFolder
				path: subpath

		nameSplitted 	= fileData.name.split('.')
		ext 			= if nameSplitted.length > 1 then nameSplitted[nameSplitted.length - 1] else ""
		name 			= nameSplitted.join('.')

		## assign icon from iconlist
		icon = @iconList[ext] or @iconList["none"]
		type = "<i class='#{icon}'></i> &nbsp #{ext}"
		##
		## Render File Data
		fileId   = @fileList.length
		fileItem = @fileListWrapper.add "li", "ninja-fm-fileitem", "ninja-fm-fileitem-#{fileId}",
			"data-number": fileId
		iconWidget = fileItem.addDiv "ninja-fm-file-icon", "ninja-fm-file-icon-#{fileId}",
			"data-number": fileId
		dataWidget = fileItem.addDiv "ninja-fm-file-data"
		nameWidget = dataWidget.addDiv "ninja-fm-file-name", "ninja-fm-file-name-#{fileId}",
			"data-number": fileId
		dateWidget = dataWidget.addDiv "ninja-fm-file-date"
		descWidget = dataWidget.addDiv "ninja-fm-file-desc"

		iconWidget.html icon

		if fileData.size?
			humanSize = fileData.size
			if humanSize > 1024 * 1024
				humanSize = numeral(fileData.size/1024000).format("#,###.##") + " MB"
			else if humanSize > 1024
				humanSize = numeral(humanSize/1024).format("#,###.##") + " KB"

			nameWidget.html "#{name} (#{humanSize})"
		else 
			nameWidget.html name 

		date = fileData.date or "unknown"
		user = fileData.user or ""
		if user? and user.length > 0
			dateWidget.html "#{moment(date).format('MMM Do YYYY, h:mm a')} by @#{user}"
		else
			dateWidget.html "#{moment(date).format('MMM Do YYYY, h:mm a')}"

		descWidget.html fileData.description

		##
		## Link event listeners
		fileItem.el.on "click", (e) =>
			fileItem.el.toggleClass "ninja-fm-selected"
			number = parseInt(fileItem.el.data("number"))
			if isNaN(number) or number < 0 then return false 
			index = @selectedFiles.indexOf(number)
			if index < 0
				@selectedFiles.push number 
			else
				@selectedFiles.splice index, 1
			#console.log "Selected Items: ", @selectedFiles
			true

		fileItem.on "dblclick", (e) =>
			#console.log "File item double clicking..."
			# if !@prevent? then @prevent = {}
			# @prevent[fileId] = true
			# clearTimeout @timers[fileId]
		
		iconWidget.on "click", (e) =>
			e.stopPropagation()
			number = parseInt(iconWidget.el.data("number"))
			# console.log "File icon is selected: ", number
			if isNaN(number) or number < 0 then return false
			if @onButtonDownloadFile? and typeof @onButtonDownloadFile is "function"
				@onButtonDownloadFile number
			
			true

		nameWidget.on "click", (e) =>
			e.stopPropagation()
			number = parseInt(nameWidget.el.data("number"))
			# console.log "File name is selected: ", number
			if isNaN(number) or number < 0 then return false
			if @onButtonDownloadFile? and typeof @onButtonDownloadFile is "function"
				@onButtonDownloadFile number
			true

		file = Object.assign {}, fileData, 
			name	        : name
			icon	        : icon
			ext		        : ext
			subpath         : subpath or "/"
			containerWidget : fileItem
			iconWidget      : iconWidget
			dataWidget      : dataWidget
			nameWidget      : nameWidget
			dateWidget      : dateWidget
			descWidget      : descWidget

		@fileList.push file
		globalTableEvents.emitEvent "file_count", [ @fileList.length, this ]
		@showCurrentFolder()
		true

	addSubFolder: (folderData) =>
		if !folderData? then return false
		if typeof folderData is "string" 
			folderData = 
				path: folderData

		path = folderData.path
		path = @getValidDataPath path

		if folderData.name?
			name = folderData.name
		else
			name = path.split('/').pop()

		if !@folderList? then @folderList = {}
		if @folderList[path]? then return true

		icon = @iconList["dir"] or @iconList["none"]
		folderItem = @folderListWrapper.add "li", "ninja-fm-fileitem ninja-fm-folderitem", "ninja-fm-folder-#{path}",
			"data-path": path
		iconWidget = folderItem.addDiv "ninja-fm-file-icon", "ninja-fm-folder-icon-#{path}",
			"data-path": path
		dataWidget = folderItem.addDiv "ninja-fm-file-data"
		nameWidget = dataWidget.addDiv "ninja-fm-file-name", "ninja-fm-folder-name-#{path}",
			"data-path": path
		dateWidget = dataWidget.addDiv "ninja-fm-file-date"
		descWidget = dataWidget.addDiv "ninja-fm-file-desc"

		iconWidget.html icon
		nameWidget.html name
		descWidget.html "#{@getFolderCount(path)} Folders, #{@getFileCount(path)} Files"

		dateWidget.hide()

		folderItem.on "click", (e) =>
			# console.log "folder item double clicking...", folderItem
			path = folderItem.el.data("path")
			if !path then return false 
			@goToFolder path

		folder = Object.assign {}, folderData, 
			name	        : name
			path            : path
			containerWidget : folderItem
			iconWidget      : iconWidget
			dataWidget      : dataWidget
			nameWidget      : nameWidget
			dateWidget      : dateWidget
			descWidget      : descWidget

		@folderList[path] = folder
		@showCurrentFolder()
		true

	##
	## Get number of files in the folder/path
	getFileCount: (path) =>
		result = 0
		for file in @fileList
			if file.subpath is path
				result++

		result

	##
	## Get number of folders in the folder/path
	getFolderCount: (path) =>
		result = 0
		for key, folder of @folderList
			if @checkSubFolder(path, folder.path) is true
				result++
		result

	##
	## Move to folder with Path of param
	goToFolder: (path) =>
		@history.push @currentPath
		# console.log "Moving to this path: ", path
		@currentPath = path
		@showCurrentFolder()
		@setPath(@baseDataPath + @currentPath, false)
		true
	##
	## Go back to the parent folder
	goBack: (e) =>
		e.stopPropagation()
		e.preventDefault()
		if @history.length is 0 then return false
		@currentPath = @history.pop()
		# console.log "Go back is clicked: ", @currentPath, @history
		@showCurrentFolder()
		@setPath(@baseDataPath + @currentPath, false)		
		true

	##
	## Show files in current folder
	showCurrentFolder: () =>
		# console.log "Current path: ", @currentPath
		if @currentPath is "/"
			@goBackWrapper.hide()
		else
			@goBackWrapper.show()

		for file in @fileList
			if file.subpath is @currentPath
				file.containerWidget.show()
			else
				file.containerWidget.hide()

		for key, folder of @folderList
			if @checkSubFolder(@currentPath, folder.path) is true
				folder.containerWidget.show()
				folder.descWidget.html "#{@getFolderCount(folder.path)} Folders, #{@getFileCount(folder.path)} Files"
			else
				folder.containerWidget.hide()

	##
	## Helper function to get valid datapath
	getValidDataPath: (path) ->
		if path[0] isnt '/' then path = '/' + path
		if path[path.length - 1] is '/' 
			path = path.slice(0, path.length - 1)
		path

	##
	## Helper function to check if one path is subpath of another
	checkSubFolder: (path1, path2) =>
		path1 = @getValidDataPath path1
		path2 = @getValidDataPath path2
		paths1 = path1.split '/'
		paths2 = path2.split '/'
		if paths1.length >= paths2.length then return false
		if paths2.length > paths1.length + 1 then return false
		for path, idx in paths1
			if path isnt paths2[idx]
				return false
		true

	##
	## get badge text from number of files in the view
	getBadgeText: () =>
		if !@fileList? then return null
		return @fileList.length

	##
	## reset file list - remove table from DataMap
	reset: () =>
		@fileList = []
		true

	##
	## set data for this view automatically
	setData: (@optionData = {}) =>
		true


	serialize: () =>
		files = @fileList?.map (file) ->
			return
				name:        file.name
				subpath:     file.subpath
				size:        file.size
				date:        file.date
				user:        file.user
				description: file.description
		return 
			basedatapath: @baseDataPath
			username:     @username
			files:        files

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? then reject "Invalid options passed."

			if options.basedatapath? or options.baseDataPath?
				@setPath(options.basedatapath or options.baseDataPath)

			if options.username?
				@setUsername options.username

			if options.files? and typeof options.files is "object"
				for index, file of options.files
					@addFile file

			resolve true
