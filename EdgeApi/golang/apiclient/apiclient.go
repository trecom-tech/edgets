package apiclient

import (
	"fmt"
	"github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
	"log"
	"math/rand"
	"runtime"
	"time"
)

type EdgeApiClient struct {
	serverUrl  string
	serverPort int
	sockClient *gosocketio.Client
	apiList    map[string]interface{}
}

func SetServer(host string, port int) (*EdgeApiClient, error) {
	runtime.GOMAXPROCS(runtime.NumCPU())

	if host == "" && port == 0 {
		log.Fatal("Please set server url and port")
	} else {
		curClient := &EdgeApiClient{serverUrl: host, serverPort: port}
		return curClient, nil
	}

	return nil, nil
}

/**
Initialize the client and dial to server
This is used for checking server and confirming if socket communication is available.
*/
func (ec *EdgeApiClient) Init() {
	if ec.serverPort == 0 && ec.serverUrl == "" {
		log.Fatal("Please set server url and port")
	} else {
		c, err := gosocketio.Dial(
			gosocketio.GetUrl(ec.serverUrl, ec.serverPort, false),
			transport.GetDefaultWebsocketTransport())
		if err != nil {
			log.Fatal(err)
		}

		ec.sockClient = c
	}
}

/**
If connected to socket server,
Connect to socket server
*/
func (ec *EdgeApiClient) Connect() {
	err := ec.sockClient.On(gosocketio.OnConnection, func(h *gosocketio.Channel) {
		log.Println("Connected")
	})
	if err != nil {
		log.Fatal(err)
	}

	err = ec.sockClient.On(gosocketio.OnDisconnection, func(h *gosocketio.Channel) {
		log.Fatal("Disconnected")
	})
	if err != nil {
		log.Fatal(err)
	}
}

/**
Close the connection to socket server
*/
func (ec *EdgeApiClient) CloseConnect() {
	ec.sockClient.Close()
}

/**
Get api list from server
*/
func (ec *EdgeApiClient) GetApiList() map[string]interface{} {
	result := make(chan map[string]interface{}, 1)

	err := ec.sockClient.On("api-list", func(h *gosocketio.Channel, args map[string]interface{}) {
		if len(args) != 0 {
			ec.apiList = args
			result <- args
			close(result)
		}
	})
	if err != nil {
		log.Fatal(err)
	}

	return <-result
}

/**
Get the Uuid by current date
*/
func (ec *EdgeApiClient) GetFastUuid() string {
	unixSeconds := time.Now().Round(time.Millisecond).UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
	randomNumber := rand.Intn(100000)
	return fmt.Sprint(unixSeconds) + "-" + fmt.Sprint(randomNumber)
}

/**
Make a call with callName
*/
func (ec *EdgeApiClient) MakeCall(callName string) map[string]interface{} {
	result := make(chan map[string]interface{}, 1)
	if len(ec.apiList) == 0 {
		ec.apiList = ec.GetApiList()
	}
	dynamicList := make(map[string]string)

	data := make(map[string]string)

	data["callName"] = callName
	data["uuid"] = ec.GetFastUuid()

	dynamicList[data["uuid"]] = data["callName"]

	ec.sockClient.Emit("cmd", data)
	ec.sockClient.On("cmd-reply", func(h *gosocketio.Channel, args map[string]interface{}) {
		_, check := dynamicList[args["uuid"].(string)]
		if check == false {
			return_result := make(map[string]interface{})
			result <- return_result
			close(result)
		} else {
			result <- args["result"].(map[string]interface{})
			close(result)
			delete(dynamicList, args["uuid"].(string))
		}
	})
	return <-result
}
