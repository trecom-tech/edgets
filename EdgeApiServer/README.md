# EdgeApiServer

> Provides all the work to create a web server, sock server, REST server, etc that works with the Edge protocol.

    npm install --save "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeApiServer"
    import EdgeApiServer from 'EdgeApiServer'

# Testing

 - start ElasticSearch, RabbitMQ, Redis, InfluxDB using docker
 
    docker-compose up
    
 - run the test
 
    npm run test-local

# Start independent server

    npm start

## The basic server

* Create an empty project
* Add the EdgeApiServer
* TODO: Document adding ApiProvider references
* call startServer()

## Core Documents

* [Understanding Server Paths](UnderstandingPaths.md)
* [Understanding API Providers](UnderstandingApiProviders.md)
* [Understanding Status Server](TODO UnderstandingStatusServer.md)
* [Understanding Change Messages](TODO UnderstandingChangeMessages.md)

Built in providers

* [Provider: Work Queues](UnderstandingWorkQueues.md) - Management of work queues
* [Provider: Data](TodoCreateDocument.md) - Primary means of accessing data
* [Provider: Mail](TodoCreatedocument.md) - General purpose E-mail functions
* [Provider: OS](TodoCreatedocument.md) - OS Specific API functions such as jobs
* [Provider: Rules](TodoCreatedocument.md) - Management of tables and rules logic
* [Provider: Logs](TodoCreateDocument.md) - General purpose logging as an API
* [Provider: Stats](TodoCreatedocument.md) - Internal stats tracking and monitoring
* [Provider: FileManager](TodoCreatedocument.md) - File manager

## Exports

EdgeApiServer.startServer
> Creates a web server / socket server

EdgeApiServer.ApiProvider
> Base class for an API Provider

## The web server

The web server is created with (ApiWebServer)[src/ApiWebServer.ts] which listens on port 8001 by default.
The web server uses Socket.io for Websockets to communicate which is (ApiSocketServer)[src/ApiSocketServer.ts]
The socket server creates a user for each connection which is (ApiSocketUser)[src/ApiSocketUser.ts]

The GUI Toolkit for the web server comes mainly from NinjaCommon which is another project.

## Add service user to database

`./node_modules/.bin/ts-node scripts/AddServiceUser.ts -u username -p password`

If you dont specify username or password, it will add default user to database.


## How to generate api documentation

    npm run doc