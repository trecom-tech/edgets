"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for SourceCode data type
//
// @extends [DataFormatText]
//
const DataFormatText_1 = require("./DataFormatText");
class DataFormatSourceCode extends DataFormatText_1.DataFormatText {
    constructor(...args) {
        super(...args);
        this.name = 'sourcecode';
        this.align = 'left';
    }
    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        //  Show a popup menu
        const w = $(window).width();
        const h = $(window).height();
        width = 800;
        height = 600;
        if (width > w) {
            width = w - 10;
        }
        if (height > h) {
            height = h - 10;
        }
        top = (h - height) / 2;
        left = (w - width) / 2;
        let codeMode = "javascript";
        if (typeof this.options === "string") {
            codeMode = this.options;
        }
        //  Show a popup menu
        // @ts-ignore
        doPopupView("Editor", "SourceCodeEditor", "codeeditor", w, h, view => {
            view.applyCodeEditorSettings(codeMode, currentValue, "tomorrow_night_eighties", true);
            view.setSaveFunc(this.saveValue.bind(this));
            return true;
        });
        return true;
    }
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return data;
    }
}
exports.DataFormatSourceCode = DataFormatSourceCode;
exports.default = DataFormatSourceCode;
//# sourceMappingURL=DataFormatSourceCode.js.map