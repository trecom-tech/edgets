//
// class for Number System data type
//
// @extends [DataFormatterType]
//

import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

const numeral = require('numeral');
const common  = new DataFormatCommon();

export class DataFormatNumberSystem extends DataFormatterType {
    name: string        = 'number_system';
    width: number       = 100;
    input_width: number = 100;
    align: string       = 'right';

    constructor(...args: any) {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if ((data == null)) {
            return "";
        }

        const num = common.getNumber(data);

        if ((data === null) || ((typeof data === "string") && (data.length === 0))) {
            return "";
        }

        if (isNaN(num)) {
            return "isNaN";
        }

        if ((options == null) || (options === "")) {
            options = "#,###.[##]";
        }

        try {
            if (num < 1024) {
                return numeral(num).format(options) + ' b';
            } else if ((1023 < num) && (num < 1024000)) {
                return numeral(num / 1024).format(options) + ' kb';
            } else if ((1024000 <= num) && (num < 1024000000)) {
                return numeral(num / 1024000).format(options) + ' mb';
            } else if ((1024000000 <= num) && (num < 1024000000000)) {
                return numeral(num / 1024000000).format(options) + ' gb';
            } else if (1024000000000 <= num) {
                return numeral(num / 1024000000000).format(options) + ' tb';
            }
        } catch (e) {
            console.log(`Exception formatting number system [${num}]`);
            return `[${num}]`;
        }
    }


    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        // console.log "unformat number system:", data
        const data_type = data.split(" ");

        if (data_type === 'b') {
            return common.getNumber(data);
        } else if (data_type === 'kb') {
            return common.getNumber(data * 1024000);
        } else if (data_type === 'mb') {
            return common.getNumber(data * 1024000000);
        } else if (data_type === 'gb') {
            return common.getNumber(data * 1024000000000);
        } else if (data_type === 'tb') {
            return common.getNumber(data * 1024000000000000);
        }
    }
}

export default DataFormatNumberSystem;