$ ->
	$("body").append '''
		<style type="text/css">
		.custom-button1 {
			border : 1px solid #bbbbdd;
			color  : red;
		}
		.custom-button2 {
			background-color: green;
		}
		.custom-input1 {
			border: none;
			border-bottom: 1px solid black;
			box-shadow: none;
			background-color: transparent;
		}
		.custom-input2 {
			color: red;
		}
		</style>
	'''		

	# Simple test cases for the ViewForm container
	# These tests do not use data bound to DataMap
	#

	addTestButton "Simple Form View", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.show()

	addTestButton "Simple Form View Submit Label", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addSubmit "submit", "Submit Label", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.show()

	addTestButton "Simple Form View Buttons and Submit", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addButton "button1", "Button Label1", "Button1", 
				onClick: (form) ->
					alert "Button1 is clicked...", form

			view.addButton "button2", "Button Label2", "Button2", 
				onClick: (form) ->
					alert "Button2 is clicked...", form

			view.addSubmit "submit", "Submit Label", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.show()

	addTestButton "Inline Form View", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.setSize 1400, 99

	addTestButton "Form View Set Focus", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput("input2", "Example Input 2").setFocus()
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"

	addTestButton "Set Value and Readonly", "Open", () ->
		addHolder().setView "Form", (view) ->

			view.addInput "input1", "Example Text"
			.setValue "Teting"
			.setReadonly(true)

			view.addInput "input2", "Example Number"
			.setValue 1234
			.setFocus()

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"

	addTestButton "Form View Set Header Content", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addButton "button", "Button Label", "Button", 
				onClick: (form) ->
					alert "Button is clicked...", form
			view.addSubmit "submit", "Submit Label", "Submit"
			view.setContentHeader "This is a <b>Header content</b>"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"

	addTestButton "Form Hide Inputs", "Open", () ->
		addHolder().setView "Form", (view) ->
			input1 = view.addInput "input1", "Example Input 1"
			input2 = view.addInput "input2", "Example Input 2"
			view.addButton "button", "Button Label", "Button", 
				onClick: (form) ->
					alert "Button is clicked...", form
			view.addSubmit "submit", "Submit Label", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			
			setTimeout () ->
				input1.hide()
			, 3000
			true			

	addTestButton "Render value in label", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Value is {value}, again {value}"

			view.addInput "input3", "Example Date: {value}"
			.setDataFormatter "datetime"
			.setValue(new Date())

			inputBool = view.addInput "input4", "Example Boolean: {value}"
			.setDataFormatter "boolean"
			.setValue(true)

			setTimeout () ->
				inputBool.setValue 0
			, 2000

			setTimeout () ->
				inputBool.setValue "Yes"
			, 4000

			view.addInput "input5", "Example enum: {value}"
			.setValue("Blue")
			.setOptions "Red#ff0000,White,Blue,Green"
			.setDataFormatter "enum"

			view.addInput "input6", "Example tags: {value}"
			.setValue("Apple, Pear")
			.setOptions "Apple, Orange, Pear, Grape"
			.setDataFormatter "tags"

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input3.value}"
		true

	addTestButton "Change Values Dynamically", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Value is {value}, again {value}"

			inputDate = view.addInput "input3", "Example Date: {value}"
			.setDataFormatter "datetime"
			.setValue(new Date())

			setTimeout () ->
				inputDate.setValue(new Date('2018-01-01'))
			, 2000

			inputBool = view.addInput "input4", "Example Boolean: {value}"
			.setDataFormatter "boolean"
			.setValue(true)

			setTimeout () ->
				inputBool.setValue 0
			, 2000

			setTimeout () ->
				inputBool.setValue "Yes"
			, 4000

			inputEnum = view.addInput "input5", "Example enum: {value}"
			.setValue("Blue")
			.setOptions "Red,White,Blue,Green"
			.setDataFormatter "enum"

			setTimeout () ->
				inputEnum.setValue "Red"
			, 2000

			inputTag = view.addInput "input6", "Example tags: {value}"
			.setValue("Apple, Pear")
			.setOptions "Apple, Orange, Pear, Grape"
			.setDataFormatter "tags"

			setTimeout () ->
				inputTag.setValue "Grape, Pear, Orange"
			, 2000

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input3.value}"
		true

	addTestButton "Form View Validation", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 1",
				validateFunction: (val)->
					console.log "Validation function got value:", val
					if val is "123"
						return @setValid()

					if isNaN(val)
						return @setWarningMsg "This value is not numeric..."

					return @setErrorMsg "Value should be '123'..."

			view.addInput "input2", "Example Input 2"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"

			view.show()

	addTestButton "Form View Hint", "Open", () ->
		addHolder().setView "Form", (view) ->
			view.addInput "input1", "Example Input 2"
			view.addInput "input2", "Example Input 1",
				validateFunction: (val)->
					console.log "Validation function got value:", val
					if isNaN(val)
						return @setWarningMsg "This value is not numeric..."

					else if val != "123" 
						@setErrorMsg "Value should be '123'..."
					true
			.setHintMsg "Hint on this input: please enter '123'"
			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"

			view.show()			

	addTestButton "Using data formatter", "Open", () ->
		addHolder().setView "Form", (view) ->


			view.addInput "input1", "Example Text", { defaultValue: "default value" }
			.setDataFormatter "text"
			.setValue null

			view.addInput "input2", "Example Number"
			.setDataFormatter "number"
			.setValue null

			view.addInput "input_money", "Example Money"
			.setDataFormatter "money"
			.setValue 1000

			view.addInput "input3", "Example Date"
			.setDataFormatter "datetime"
			.setValue(new Date())

			view.addInput "input3_1", "Example Date"
			.setOptions { dateonly: true }
			.setDataFormatter "datetime"
			.setValue(new Date())

			view.addInput "input3_2", "Example Date 1"
			.setDataFormatter "datetime"

			view.addInput "input3-3", "Example TimeAgo"
			.setDataFormatter "timeago"
			.setValue(new Date())

			view.addInput "input4", "Example Boolean"
			.setDataFormatter "boolean"
			.setValue(true)

			view.addInput "input5", "Example enum", { defaultValue: "White" }
			.setValue()
			.setOptions "Red,White,Blue,Green"
			.setDataFormatter "enum"

			view.addInput "input51", "Example enum 1"
			.setOptions "A,B,C,D"
			.setDataFormatter "enum"
			.setValue("C")

			view.addInput "input6", "Example tags"
			.setValue("Apple, Pear")
			.setOptions "Apple, Orange, Pear, Grape"
			.setDataFormatter "tags"

			view.addInput "input61", "Example tags 1"
			.setOptions "1, 2, 3, 4, 5"
			.setDataFormatter "tags"
			.setValue("1")

			view.addInput "input7", "Example password"
			.setValue("password")
			.setOptions({hasOriginPassword: true})
			.setDataFormatter("password")

			view.addInput "input8", "Example memo"
			.setDataFormatter "memo"
			.setValue "Memo text value..."

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				msg = ""
				msg = "Text Value: " + form.input1.value
				msg += "\nNumber Value: " + form.input2.value
				msg += "\nDate Value: " + form.input3.value
				msg += "\nBoolean Value: " + form.input4.value
				msg += "\nEnum Value: " + form.input5.value
				msg += "\nTags Value: " + form.input6.value
				msg += "\nMemo Value: " + form.input8.value
				alert(msg)

	addTestButton "Input Formatter", "Open", () ->
		addHolder().setView "Form", (view) ->
			input1 = view.addInput "input1", "Example Enum"
			.setOptions "Not set, Yes, No"
			.setDataFormatter "enum"
			.setInputFormatter (val) ->
				if val is 0 then return "No"
				if val is 1 then return "Yes"
				val
			.setValue 1

			setTimeout () ->
				input1.setValue 0
			, 2000

	addTestButton "Numeric Formatters", "Open", () ->
		addHolder().setView "Form", (view) ->

			view.addInput "input1", "Example Number"
			.setDataFormatter "number"
			.setValue 123456.7891234

			view.addInput "input2", "Example Number 3 Dec",
				options: "#,###.###"
			.setDataFormatter "number"
			.setValue 123456.7891234

			view.addInput "input3", "Example Number no markup",
				options: "####"
			.setDataFormatter "number"
			.setValue 123456.7891234

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				msg = ""
				msg = "Value 1: " + form.input1.value
				msg += "\nValue 2: " + form.input2.value
				msg += "\nValue 3: " + form.input3.value
				alert(msg)

	addTestButton "Modal Dialog", "Open", ()->

		m = new ModalDialog
            title        : "Example of a form in a modal dialog"
            ok           : "Save"

		m.getForm (formView)->
			formView.addInput "input1", "Example Input 1"
			formView.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}"

	addTestButton "Form on Popup", "Open", () ->
		doPopupView "Form", "Form on Popup", "form-popup1", 399, 300, (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"

			view.addInput "input3", "Example Date"
			.setDataFormatter "timeago"
			.setValue(new Date())

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.show()
		true

	addTestButton "Form on Popup with Many Fields", "Open", () ->
		doPopupView "Form", "Form on Popup with Many Fields", "form-popup2", 399, 300, (view) ->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addInput "input3", "Example Input 3"
			view.addInput "input4", "Example Input 4"
			view.addInput "input5", "Example Input 5"
			view.addInput "input6", "Example Input 6"
			view.addInput "input7", "Example Input 7"
			view.addInput "input8", "Example Input 8"

			view.addSubmit "submit", "", "Submit"
			view.setSubmitFunction (form) =>
				alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
			view.setScrollable()
			view.show()
		.then (popupView) ->
			console.log "PopupView: ", popupView
			popupView.getBody().setScrollable()
		true

	addTestButton "Form in Tab", "Open", () ->
		addHolder().setView "DynamicTabs", (tabs)->
			tabs.doAddViewTab("Form", "FormViewTab", (view)->
				view.addInput "input1", "Example Input 1"
				view.addInput "input2", "Example Input 2"
				view.addSubmit "submit", "Click this button to submit", "Submit"
				view.setSubmitFunction (form) =>
					alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value},  Test Value2 = #{form.input2.value}"
				view.show()
			)
			tabs.addTab "EmptyTab", 'Another tab'
		true

	addTestButton "Modal Form Class", "Open", ()->
		options =
			title: "This is a test form"
			content: "Please fill out this form"
			inputs: [
				type: "text"
				label: "Parameter Name"
				name: "input1"
			,
				type: "enum"
				label: "Parameter Type"
				name: "ptype"
				options: ["Apple", "Orange", "Grape", "Ball"]
			]
			buttons: [
				type: "submit"
				text: "Save"
			]
			onSubmit: (form)=>
				alert "SUBMIT FORM:", form.input1.value, " and ", form.ptype.value

		m = new ModalForm(options)
		true

	addTestButton "Forms - Deserialize 1", "Open", ()->
		addHolder().setView "Form", (view)->
			view.deserialize
				contentheader: "Content Header"
				values_from_object: 
					input1: "First Input"
					input2: "Second Input"
					ptype: 1
				inputs: [
					type: "text"
					label: "Parameter Name"
					name: "input1"
					validateFunction: (val) ->
						if !val or val is ""
							return @setErrorMsg "This field is required..."
						true
				,
					type:        "text"
					label:       "Field with Hint"
					name:        "input2"
					hintMessage: "Hint for the second input field..."	
					focus: 1
				,
					type: "enum"
					label: "Parameter Type"
					name: "ptype"
					options: ["Apple", "Orange", "Grape", "Ball"]
					inputFormatter: (val) ->
						if val is 1 then return "Ball"
						val
				]
				inputcustomstyles: [
					"custom-input1"
					"custom-input2"
				]
				buttoncustomstyles: [
					"custom-button1"
					"custom-button2"
				]
				buttons: [
					type: "submit"
					text: "Save"
				]

			setTimeout () ->
				doPopupView "Form", "Deserialized Popup Form", "form-deserialized1", 1000, 600, (viewFormPopup) ->
					options = view.serialize()
					console.log "Form serialized: ", options
					viewFormPopup.deserialize options
			, 2000

	addTestButton "Forms - Deserialize 2", "Open", ()->
		addHolder().setView "Form", (view)->
			view.deserialize
				rows: [
					inputs: [
						type: "text"
						label: "Parameter Name"
						name: "input1"
					,
						type:        "text"
						label:       "Field with Hint"
						name:        "input2"
						hintMessage: "Hint for the second input field..."				
					,
						type: "enum"
						label: "Parameter Type"
						name: "ptype"
						options: ["Apple", "Orange", "Grape", "Ball"]
					]
				,
					inputs: [
						type: "text"
						label: "Parameter Name"
						name: "input4"
					,
						type:        "text"
						label:       "Field with Hint"
						name:        "input5"
						hintMessage: "Hint for the second input field..."				
					]
				]
				buttons: [
					type: "submit"
					text: "Save"
				]
				onSubmit: (form) ->
					alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}\nTest Value2 = #{form.input2.value}\nParameter Type = #{form.ptype.value}\nTest Value 4 = #{form.input4.value}\nTest Value 5 = #{form.input5.value}"

			setTimeout () ->
				doPopupView "Form", "Deserialized Popup Form", "form-deserialized1", 1000, 600, (viewFormPopup) ->
					options = view.serialize()
					console.log "Form serialized: ", options
					viewFormPopup.deserialize options
			, 2000

	addTestButton "Forms - Deserialize 3", "Open", ()->
		addHolder().setView "Form", (view)->
			view.deserialize
				inputs: [
					type: "text"
					label: "Parameter Name"
					name: "input1"
				,
					type:        "text"
					label:       "Field with Hint"
					name:        "input2"
					hintMessage: "Hint for the second input field..."				
				,
					type: "enum"
					label: "Parameter Type"
					name: "ptype"
					options: ["Apple", "Orange", "Grape", "Ball"]
				,
					type: "text"
					label: "Parameter Name"
					name: "input4"
				,
					type:        "text"
					label:       "Field with Hint"
					name:        "input5"
					hintMessage: "Hint for the second input field..."
				]
				arrangement:
					defaultInputCount: 2
				buttons: [
					type: "submit"
					text: "Save"
				]
				onSubmit: (form) ->
					alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}\nTest Value2 = #{form.input2.value}\nParameter Type = #{form.ptype.value}\nTest Value 4 = #{form.input4.value}\nTest Value 5 = #{form.input5.value}"

			setTimeout () ->
				doPopupView "Form", "Deserialized Popup Form", "form-deserialized1", 1000, 600, (viewFormPopup) ->
					options = view.serialize()
					console.log "Form serialized: ", options
					viewFormPopup.deserialize options
			, 2000

	addTestButton "Forms - Deserialize 4", "Open", ()->
		addHolder().setView "Form", (view)->
			view.deserialize
				inputs: [
					type: "text"
					label: "Parameter Name"
					name: "input1"
				,
					type:        "text"
					label:       "Field with Hint"
					name:        "input2"
					hintMessage: "Hint for the second input field..."				
				,
					type: "enum"
					label: "Parameter Type"
					name: "ptype"
					options: ["Apple", "Orange", "Grape", "Ball"]
				,
					type: "text"
					label: "Parameter Name"
					name: "input4"
				,
					type:        "text"
					label:       "Field with Hint"
					name:        "input5"
					hintMessage: "Hint for the second input field..."
				]
				arrangement:
					defaultInputCount: 1
					rows: [
						inputCount: 1
					,
						inputCount: 2
					]
				buttons: [
					type: "submit"
					text: "Save"
				]
				onSubmit: (form) ->
					alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}\nTest Value2 = #{form.input2.value}\nParameter Type = #{form.ptype.value}\nTest Value 4 = #{form.input4.value}\nTest Value 5 = #{form.input5.value}"

	addTestButton "Password Change", "Open", () ->
		addHolder().setView "PasswordChange", (view) ->				
			console.log "View PasswordChange"

	go()
