import { expect }                       from 'chai';
import { EdgeLauncher, NpmModuleTools } from 'edgecommonlauncher';
import JobServer                        from '../src/JobServer';
import JobRunner                        from '../src/JobRunner';
import { IJob }                         from "../src/interfaces/IJob";

const jobServer = new JobServer();

const dataTestJob1: IJob = {
    id          : 'TESTJOB00000001',
    title       : 'One-time test job for npm modules',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'test_job_1.sh',
    args        : 'arg1 arg2 arg3',
    owner       : 'System',
    enabled     : true,
    pending     : false,
    lastRun     : null,
    lastDuration: null,
};

const dataTestJob2: IJob = {
    id          : 'TESTJOB00000002',
    title       : 'One-time test job',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'timeout.sh',
    args        : '',
    owner       : 'System',
    enabled     : true,
    pending     : false,
    lastDuration: null,
    lastRun     : null,
};

const dataTestJob3: IJob = {
    id          : 'TESTJOB00000003',
    title       : 'One-time test job',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'test_job_1.sh',
    args        : 'arg1 arg2 arg3',
    owner       : 'System',
    enabled     : false,
    pending     : true,
    lastDuration: null,
    lastRun     : null,
    timeout     : 2000,
};

const dataTestJob4: IJob = {
    id              : 'TESTJOB00000004',
    title           : 'One-time test job for missing module and wrong path',
    freqType        : 'hourly',
    freqRate        : 1,
    folder          : 'wrong_path',
    args            : '',
    owner           : '',
    enabled         : true,
    pending         : false,
    lastDuration    : null,
    lastRun         : new Date(),
    lastDataReceived: new Date(),
    timeout         : 2000,
};

const dataTestJob5: IJob = {
    id              : 'TESTJOB00000005',
    title           : 'One-time test job for JS script',
    freqType        : 'once',
    freqRate        : 1,
    folder          : 'test',
    script          : 'test_job_2.js',
    args            : '',
    owner           : '',
    enabled         : true,
    pending         : false,
    lastDuration    : null,
    lastRun         : null,
    lastDataReceived: new Date(),
    timeout         : 2000,
};

const dataTestJob6: IJob = {
    id          : 'TESTJOB00000006',
    title       : 'One-time test job for JS script',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'test_job_3.pl',
    args        : '',
    owner       : '',
    enabled     : true,
    pending     : false,
    lastDuration: null,
    lastRun     : null,
    timeout     : 2000,
};

const dataTestJob7: IJob = {
    id          : 'TESTJOB00000007',
    title       : 'One-time test job for coffee script',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'test_job_4.coffee',
    args        : '',
    owner       : '',
    enabled     : true,
    pending     : false,
    lastDuration: null,
    lastRun     : null,
    timeout     : 2000,
};

const dataTestJob8: IJob = {
    id          : 'TESTJOB00000008',
    title       : 'One-time test job for shell script',
    freqType    : 'once',
    freqRate    : 1,
    folder      : 'test',
    script      : 'test_job_1.sh',
    args        : 'arg1 arg2 arg3',
    owner       : 'System',
    enabled     : true,
    pending     : false,
    lastRun     : null,
    lastDuration: null,
};

describe('Job Server', () => {
   it('refreshStatus()', async () => {
       const result = await jobServer.refreshStatus();
       expect(result).to.equal(true);
   });

    it('getPendingCount()', async () => {
        const result = await JobServer.getPendingCount();
        const jobsList = await jobServer.getJobsList();
        expect(jobsList.length).to.equal(0);
    });

    it('promiseDelayed(null)', async () => {
        const result = await jobServer.promiseDelayed();
        expect(result).to.equal('1000 delayed');
    });

    it('promiseDelayed(2500)', async () => {
        const delay = 2500; // ms
        const result = await jobServer.promiseDelayed(delay);
        expect(result).to.equal('2500 delayed');
    });

    it('getJobsList()', async () => {
        const runner = new EdgeLauncher();
        const response = await runner.runJob(dataTestJob1, 1);
        if (response) {
            const result = await jobServer.getJobsList();
            const jobData = await result.filter((job: any) => job.id === dataTestJob1.id);
            expect(jobData.length).to.equal(1);
        }
    });

    it('refreshJobList()', async () => {
        const result = await jobServer.refreshJobList();
        expect(result).to.equal(true);
    });

    it('shouldJobRun() as lastRun = null', () => {
        const result = JobServer.shouldJobRun(dataTestJob2);
        expect(result).to.equal(9999999);
    });

    it('shouldJobRun() as enabled = false', () => {
        const result = JobServer.shouldJobRun(dataTestJob3);
        expect(result).to.equal(false);
    });

    it('shouldJobRun() as pending = true', () => {
        const result = JobServer.shouldJobRun(dataTestJob3);
        expect(result).to.equal(false);
    });

    it('getNextJob()', async () => {
        await jobServer.queueJob(dataTestJob8);
        const result = await jobServer.getNextJob();
        console.log('result===>', result);
        // expect(result.length).to.equal(1);
    });

    it('doVerifyInstalled()', async () => {
        const result = await JobServer.doVerifyInstalled(dataTestJob1);
        expect(result).to.equal(false);
    });

    it('doVerifyInstalled() as last_run diff', async () => {
        const result = await JobServer.doVerifyInstalled(dataTestJob4);
        expect(result).to.equal(false);
    });

    it('executeJobs()', async () => {
        const result = await jobServer.executeJobs(1);
        console.log('====>', result);
    });

    it('startSystemStatusLoop', async () => {
        const result = await jobServer.startSystemStatusLoop();
        expect(result).to.equal(false);
    });
});

describe('JobRunner', () => {
    it('index()', async () => {
        // const result = await JobRunner.index();
        // console.log('====>', result);
    });
});
