"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is the base class for calling the Edge API Server
 * The api can expose RPC functions and it can consume functions
 */
const EvEmitter = require("events");
const edgecommonconfig_1 = require("edgecommonconfig");
const EdgeApiProvider_1 = require("./EdgeApiProvider");
const DEFAULT_API_USERNAME = 'edgeApi';
const DEFAULT_API_PASSWORD = 'Hr^YQ5yh!2eM;QbV';
class EdgeApiBase extends EdgeApiProvider_1.default {
    constructor() {
        super();
        this.subscriptionList = [];
        this.registeredList = [];
        this.connectPromise = null;
        this.waitForPromises = null;
        this.emitter = new EvEmitter();
        this.isVerbose = false;
        this.commandCallback = {};
        this.listeners = {};
    }
    /**
     * Establish a socket connection to the server
     * Allow registering for push message actions
     * @param pushMessage
     * @param callback
     */
    on(pushMessage, callback) {
        return this.emitter.on(pushMessage, callback);
    }
    listen(channel, callback) {
        this.listeners[channel] = callback;
    }
    /**
     * Stop a push message action
     * @param pushMessage
     * @param callback
     */
    off(pushMessage, callback) {
        return this.emitter.off(pushMessage, callback);
    }
    /**
     * Single callback for an event
     * @param pushMessage
     * @param callback
     */
    once(pushMessage, callback) {
        return this.emitter.on(pushMessage, callback);
    }
    /**
     *
     * @param socket
     */
    onConnect(socket) {
        // Nothing
        edgecommonconfig_1.config.status('EdgeApiBase::onConnect');
        return true;
    }
    /**
     *
     */
    onDisconnect() {
        // Nothing
        edgecommonconfig_1.config.status('EdgeApiBase::onDisconnect');
        return true;
    }
    onReady() {
        // Nothing
        edgecommonconfig_1.config.status('EdgeApiBase::onReady');
        return true;
    }
    /**
     *
     * @param socketConnectCallbackList
     */
    doGetSocket(socketConnectCallbackList) {
        throw new Error('Parent must implement');
    }
    // tslint:disable-next-line:no-empty
    onCmdReplyComplete() {
    }
    processChangeEvent(data) {
        if (this.listeners[data.channel]) {
            this.listeners[data.channel](data.data);
        }
        return true;
    }
    onInitializeSocket() {
        return new Promise((resolve, reject) => {
            this.socket.on('connect', () => {
                edgecommonconfig_1.config.status('EdgeApiBase::onInitializeSocket \'connect\'');
                // Send/Resend current subscriptions
                for (const sub of this.subscriptionList) {
                    this.socket.emit('subscribe', sub);
                }
                resolve(true);
                this.onConnect(this.socket);
                this.onReconnect(this.socket);
                return true;
            });
            this.socket.on('disconnect', () => {
                edgecommonconfig_1.config.status('EdgeApiBase::onInitializeSocket \'disconnect\'');
                this.onDisconnect();
                this.waitForPromises = {};
                return true;
            });
            this.socket.on('change', (data) => {
                edgecommonconfig_1.config.status('EdgeApiBase::onInitializeSocket \'change\'');
                // console.log "CHANGE MATCH=", data
                this.processChangeEvent(data);
                return true;
            });
            return this.socket.on('cmd-push', (data) => {
                edgecommonconfig_1.config.status('EdgeApiBase::onInitializeSocket \'cmd-push\'');
                // incoming push message
                let command = data.command.toString();
                // console.log "Push received [#{command}] = ", data
                this.emitter.emit(command, [data.data]);
                return true;
            });
        });
    }
    /**
     * Wait until callName gets available
     * @param callName string
     */
    doWaitForApi(callName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.waitForPromises) {
                this.waitForPromises = {};
            }
            if (this.waitForPromises[callName]) {
                return this.waitForPromises[callName];
            }
            this.waitForPromises[callName] = new Promise((resolve, reject) => {
                let checkAvailable;
                return (checkAvailable = () => {
                    if (this[callName] != null) {
                        return resolve(true);
                    }
                    else {
                        return setTimeout(checkAvailable, 10);
                    }
                })();
            });
            return this.waitForPromises[callName];
        });
    }
    /**
     * Create socket and wait to be connected.
     */
    doConnect() {
        edgecommonconfig_1.config.status('EdgeApiBase doConnect Starting');
        if (this.connectPromise != null) {
            return this.connectPromise;
        }
        return (this.connectPromise = this.doConnectPromise());
    }
    doConnectPromise() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doGetSocket([this.onSetupSocket, this.onInitializeSocket,]);
            yield this.doWaitForApi('os_doGetServerInformation');
            edgecommonconfig_1.config.status('EdgeApiBase doConnect Connected');
            // Authenticate using serviceUser
            const apiUsername = edgecommonconfig_1.config.getCredentials('apiUsername') || process.env.API_USERNAME || DEFAULT_API_USERNAME;
            const apiPassword = edgecommonconfig_1.config.getCredentials('apiPassword') || process.env.API_PASSWORD || DEFAULT_API_PASSWORD;
            const result = yield this.auth_doAuthUser(apiUsername, apiPassword);
            if (result.error) {
                throw new Error(result.error);
            }
            return true;
        });
    }
    /**
     * Subscribe to a given path
     * @param channel
     */
    subscribe(channel) {
        // Prevent re-subsubscribing
        for (const sub of this.subscriptionList) {
            if (sub.channel === channel) {
                return true;
            }
        }
        const sub = { channel };
        this.socket.emit('subscribe', sub);
        this.subscriptionList.push(sub);
        return true;
    }
    /**
     * Send a command to the server
     * @param command
     * @param options
     */
    sendCommand(command, options) {
        if (typeof options !== 'object') {
            options = {};
        }
        this.socket.emit(command, options);
        return true;
    }
}
exports.EdgeApiBase = EdgeApiBase;
exports.default = EdgeApiBase;
//# sourceMappingURL=EdgeApiBase.js.map