import StatusUpdateClient from 'edgecommonstatusupdateclient';
import ApiProvider from './ApiProvider';
export default class ProviderWorkQueue extends ApiProvider {
    cacheTime: number;
    memoryCache: any;
    loadTooHigh: boolean;
    statusUpdateClient: StatusUpdateClient;
    workQueueStatus: {
        [workqueueName: string]: any;
    };
    knownListChanged: boolean;
    knownWorkQueues: string[];
    memoryCacheDelayed: {};
    cacheTimeDelayed: number;
    constructor(prototype: ApiProvider);
    onInit(): void;
    /**
     * Called on an interval to push anything in the cache array
     * to REDIS, otherwise keep it in memory
     */
    clearCache(): boolean;
    internalGetStatus(workQueueName: string): boolean;
    /**
     * Update the number of items for a workqueue to keep status
     * @param workQueueName
     * @param amountRecv
     * @param amountProc
     */
    internalStatusInc(workQueueName: string, amountRecv: any, amountProc: any): boolean;
    internalStatusCacheHit(workQueueName: string): boolean;
    internalStatusSetTotal(workQueueName: string, totalAmount: any): boolean;
    /**
     * Send updates to the API based on the number processed
     */
    internalStatusSendUpdates(): boolean;
    getBaseName(): string;
    /**
     * Return a safe and REDIS compatible name for a work queue
     * @param workQueueName
     */
    internalGetName(workQueueName: string): string;
    doAddRecordDelayed(workQueueName: string, record_id: number): boolean;
    /**
     * Add a record id to a work queue
     * @param workQueueName
     * @param record_id
     */
    doAddRecord(workQueueName: string, record_id: any): boolean;
    /**
     * Return the number of items pending in a queue
     * @param workQueueName
     */
    doGetCount(workQueueName: string): Promise<unknown>;
    /**
     * Get the next available or return null
     * This will go very quickly and return null when there
     * is nothing in REDIS
     * @param workQueueName
     */
    doGetOnePending(workQueueName: string): Promise<unknown>;
    /**
     * Get array of all known work queues and number of items
     * pending per specific work queue
     */
    doGetWorkqueueList(): Promise<{
        workqueue: string;
        pending: unknown;
    }[]>;
    /**
     * Don't return until a pending job is available
     * @param workQueueName
     */
    doWaitForPending(workQueueName: string): Promise<unknown>;
}
