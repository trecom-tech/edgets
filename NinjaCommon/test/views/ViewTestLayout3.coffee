class ViewTestLayout3 extends View

    onSplitterSetup: (viewSplitter, layoutOptions, resultFromDesserialize)=>
        console.log "Callback called"
        console.log "viewSplitter=", viewSplitter
        console.log "layoutOptions=", layoutOptions
        console.log "resultFromDesserialize=", resultFromDesserialize
        true

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view:     "Splittable"
        percent:  40
        callback: "onSplitterSetup"
        top:      "TestShowSize"
        bottom:   "TestShowSize"



