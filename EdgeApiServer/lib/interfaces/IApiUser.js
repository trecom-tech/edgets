"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgeapi_1 = require("edgeapi");
const passwordUtil = require("../utility/Password");
class IApiUser extends edgeapi_1.EdgeApiProvider {
    constructor() {
        super(...arguments);
        this.timezoneOffset = 0;
    }
    /**
     * Run one-time initial code after the user object is created.
     */
    doInitUser() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Default INit User");
        });
    }
    addRoles(roles = [], system = 'global') {
        if (roles && roles.length > 0) {
            if (!this.rec.roles[system]) {
                this.rec.roles[system] = roles;
            }
            else {
                this.rec.roles[system] = Object.assign(Object.assign({}, this.rec.roles[system]), roles);
            }
        }
        return true;
    }
    removeRoles(roles = [], system = 'global') {
        if (roles && roles.length > 0) {
            if (this.rec.roles[system]) {
                this.rec.roles[system] = this.rec.roles[system].filter((x) => !roles.includes(x));
            }
        }
        return true;
    }
    hasRole(roleName, targetSystem) {
        if (this.rec && this.rec.roles) {
            const userSystem = this.system;
            if (targetSystem) {
                const systemRoles = this.rec.roles[targetSystem] || [];
                if (systemRoles.includes(roleName)) {
                    return true;
                }
            }
            else if (userSystem) {
                const systemRoles = this.rec.roles[userSystem] || [];
                if (systemRoles.includes(roleName)) {
                    return true;
                }
            }
            const globalRoles = this.rec.roles['global'] || [];
            return globalRoles.includes(roleName);
        }
        return false;
    }
    doChangePassword(newPassword) {
        this.rec.password = passwordUtil.generatePasswordHash(newPassword);
        return true;
    }
    doAddTimeline(provider, timelineData) {
        return __awaiter(this, void 0, void 0, function* () {
            return provider.doCall('timeline_doAddTimeline', Object.assign({ participant_id: this.rec.id, eventStamp: new Date(), eventOffset: this.timezoneOffset }, timelineData), this);
        });
    }
}
exports.IApiUser = IApiUser;
exports.default = IApiUser;
//# sourceMappingURL=IApiUser.js.map