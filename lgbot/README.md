# LGBotLib - Library that takes text and uses NLP to create a search for request and then returns the result.

## Design Ideas

LGBot is a command line tool and library to access Edge API Services and perform useful functions from the command line or another location that acts like a command line.

The library returns results in a JSON format that is standardized with certain fields in a given way as described in this document.

The caller of the library can then change the result to a format is best for the output.   For example,  a website may way to display things in a nice table control.  A slack bot may want to format for slack and a command line tool might want to format with ANSI.

## Install Note

This tool is designed to install globally.   It is suggested that you link the lg binary using a command like this:

    chmod 755 ./bin/lg.ts
    sudo ln -s `pwd`/lg.ts /usr/local/bin/lg

## Usage on the command line

Execute the lg command with parameters.   Help will show you the valid command examples

    lg help

Modify the output

    --json          Sets output format to JSON
    --table         Sets output format to a table 
    --normal        Sets output format to normal / unchanged.
                    Normal mode is used when a function suggests a format such as table and you want to ignore that.
                    Normal mode uses NinjaDump for pretty color output.
    --idlist        Will output just the ID values found
    --trace         Add debugging output to the request

## Usage as a library

Create an instance of a bot and a search session.
    import LGBotLib from 'lgbotlib';

    const bot = new LGBotLib();
    bot.doProcess("search waterlynn")
    .then((results) => {
        console.log("r=", results);
        process.exit();
    });

Examples

    lg workqueues --table

## Return JSON

All JSON output is in this format:

    {
    type : "text|row_type"
    error: false|text error message
    data : null|array of json rows|text
    }

## Connecting to a server

This library uses the EdgeApi library to connect to a server.  This will check the following

3- Check v2 Environtment

    export apiservers="http://127.0.0.1:8001"

2- Check v2 credentials

    ~/EdgeApi/credentialsv2.json

Some functions access Azure File share 

    GlobalShareName = process.env.AZURESHARE || "edgedatalogs"
    GlobalShareCreds = process.env.AZURECONN || config.getCredentials("azureLogs").connection

## Testing

    docker-compose up --build
    npm run start-test-server
    npm run test

