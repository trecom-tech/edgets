import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatDateTime extends DataFormatterType {
    name: string;
    input_width: number;
    align: string;
    editorShowing: boolean;
    elEditor: any;
    datePicker: any;
    elDataEditor: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options?: any, path?: any): any;
    unformat(data: any, path: any): Date | "";
    getValueInTooltip(value: any): any;
}
export default DataFormatDateTime;
