package main

import (
	"./apiclient"
	"fmt"
	"log"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	/**
	connect to socket server
	*/
	ec, _ := apiclient.SetServer("localhost", 8001)
	ec.Init()
	ec.Connect()

	list := ec.GetApiList()
	log.Println("***************** API List *******************")
	for k, v := range list {
		fmt.Printf("%s -> %s\n", k, v)
	}

	info := ec.MakeCall("os_doGetServerInformation")
	log.Println("***************** OS Info *******************")
	for k, v := range info {
		fmt.Printf("%s -> %s\n", k, v)
	}

	time.Sleep(10 * time.Second)
	ec.CloseConnect()
}
