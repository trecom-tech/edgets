$ ->

    console.log "LOADING HERE"

    p1 = new Promise (resolve, reject) ->
        ds  = new DataSet "zipcodes"
        ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
        ds.doLoadData()
        .then (dsObject)->
            resolve(true)
        .catch (e) ->
            console.log "Error loading zipcode data: ", e
            resolve(false)

    p2 = new Promise (resolve, reject) ->
        ds  = new DataSet "people"
        ds.setAjaxSource "/js/test_data/mockdata_people1.json", null, "id"
        ds.doLoadData()
        .then (dsObject)->
            resolve(true)
        .catch (e) ->
            console.log "Error loading zipcode data: ", e
            resolve(false)

    Promise.all([p1, p2])
    .then ()->

        ##|
        ##|  For testing purposes
        DataMap.getDataMap().on "table_change", (tableName, config)->
            console.log "NEW CONFIG for #{tableName}:", config

        addTestButton "Linked Zipcode ReadOnly", "Open", ()->

            ##|
            ##|  Sample data is loaded into table zipcodes and people

            DataMap.changeColumnAttribute "people", "zipcode", "linked", "zipcodes"

            addHolder()
            .setView "Table", (view)->
                table = view.addTable "people"

        addTestButton "Linked Zipcode Editable", "Open", ()->

            ##|
            ##|  Sample data is loaded into table zipcodes and people

            DataMap.changeColumnAttributes "people", "zipcode", 
                linked: "zipcodes"
                editable: true

            addHolder()
            .setView "Table", (view)->
                table = view.addTable "people"

        go()