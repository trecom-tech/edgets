# ChangeLogServer
> Reads change records from Mongo's Change Log Stream and log the changes to Azure Table Storage
    
    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/MicroserviceChangeLogger.git
    import Worker from 'microservicechangelogger';


## Important

This module works on MongoDB 4.0 or greater, so make sure you have installed MongoDB 4.0 at least.  
Reference: https://docs.mongodb.com/manual/changeStreams/

Also this module needs to have EdgeApiServer running.
So when you run EdgeApiServer, make sure you start mongodb like:  

    mongod --port 27017 --dbpath /var/lib/mongodb --replSet rs0  

Once you run the above command, start mongo shell, and type following command:  

    db.adminCommand( { setFeatureCompatibilityVersion: "4.0" } )

## Credential

The credential for this module should be like:
```
MongoDB: {
    url: "mongodb://localhost:27017/admin?readPreference=primary",
    url_replicaSet: "mongodb://localhost:27017?replicaSet=rs0",
    options: {
        poolSize: 16,
        socketTimeoutMS: 600000,
        connectTimeoutMS: 600000
    }
}
```    

## Creating an instance

    const worker = new Worker(azureConfigName, azureTableName);

* azureConfigName : config name as defined in the credential for azure table storage
* azureTableName  : Indicate a azure table for logging

## Hepler functions

### doRun(callback?: any)

Run worker for watching Mongo's Change Log Stream

    worker.doRun((err, rec) => {
        console.log(rec);
    });

## Testing

    docker-compose up --build
    npm run start-test-server
    npm run test




