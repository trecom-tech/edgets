"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var TabbedFileImportAsync = /** @class */ (function () {
    function TabbedFileImportAsync(filename) {
        this.filename = filename;
        this.hasHeader = false;
        this.seperator = 9;
        this.sLength = 1024 * 120;
        this.skipCount = 0;
        this.handleEscaped = true;
        this.buffer = Buffer.alloc(this.sLength);
        this.fd = fs.openSync(filename, 'r');
        this.readPos = 0;
        this.readSize = 0;
        this.callback = null;
        this.headers = null;
    }
    /**
     * Shortcut function to configure for a CSV file
     */
    TabbedFileImportAsync.prototype.setFormatCSVWithHeaders = function () {
        this.hasHeader = true;
        this.seperator = 44;
        this.handleEscaped = true;
        this.skipCount = 0;
    };
    /**
     * Returns a promise that completes once the file completes
     * @param callback - If callback returns a promise then we pause reading until it's done
     */
    TabbedFileImportAsync.prototype.readLine = function (callback) {
        return __awaiter(this, void 0, void 0, function () {
            var currentArray, currentValue, currentLength, currentSpaces, currentLine, isQuoteString, absoluteNext, result, ch, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.callback = callback;
                        this.headers = this.hasHeader ? null : this.headers;
                        currentArray = [];
                        currentValue = '';
                        currentLength = 0;
                        currentSpaces = 0;
                        currentLine = 0;
                        isQuoteString = false;
                        absoluteNext = false;
                        if (typeof this.callback !== 'function') {
                            return [2 /*return*/, false];
                        }
                        _a.label = 1;
                    case 1:
                        if (!true) return [3 /*break*/, 9];
                        if (!(this.readSize <= this.readPos)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.doReadBuffer()];
                    case 2:
                        result = _a.sent();
                        if (!result) {
                            return [3 /*break*/, 9];
                        }
                        _a.label = 3;
                    case 3:
                        ch = this.buffer[this.readPos++];
                        if (!absoluteNext) return [3 /*break*/, 4];
                        // Add this ch no matter what because it was excaped with \
                        while (currentSpaces > 0) {
                            currentValue += ' ';
                            currentSpaces--;
                        }
                        if (ch === 10) {
                            currentValue += '\n';
                        }
                        else if (ch === 13) {
                            currentValue += '';
                        }
                        else {
                            currentValue += String.fromCharCode(ch);
                        }
                        absoluteNext = false;
                        return [3 /*break*/, 8];
                    case 4:
                        if (!((ch === 10 || ch === 13) && !isQuoteString)) return [3 /*break*/, 7];
                        if (currentValue) {
                            currentArray.push(currentValue);
                        }
                        if (!(currentLine >= this.skipCount)) return [3 /*break*/, 6];
                        result = this.callbackWithData(currentArray);
                        if (!(result &&
                            typeof result === 'object' &&
                            result.next &&
                            typeof result.next === 'function')) return [3 /*break*/, 6];
                        console.log("TabbedFileImportAsync at line " + currentLine + " received promise");
                        return [4 /*yield*/, result];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6:
                        currentArray = [];
                        currentValue = '';
                        currentLength = 0;
                        currentSpaces = 0;
                        currentLine++;
                        return [3 /*break*/, 8];
                    case 7:
                        if (currentLine < this.skipCount) {
                            return [3 /*break*/, 1];
                        }
                        // If it's a quote and starts a field then we're in a string
                        else if (ch === 34) {
                            if (currentValue.length === 0) {
                                isQuoteString = !isQuoteString;
                            }
                            else {
                                if (isQuoteString) {
                                    isQuoteString = false;
                                }
                                else {
                                    // Single quote in the middle of a string
                                    // just add it to the current value
                                    currentValue += '"';
                                }
                            }
                        }
                        // Check for \ which means always add the next char to the current value
                        else if (ch === 92 && this.handleEscaped) {
                            absoluteNext = true;
                        }
                        // Check for a tab
                        else if (ch === this.seperator && !isQuoteString) {
                            currentArray.push(currentValue);
                            currentValue = '';
                            currentLength = 0;
                            currentSpaces = 0;
                        }
                        // All others can be added to the current value
                        else {
                            // Don't add spaces at the start of the string
                            if (ch === 32) {
                                if (currentLength === 0) {
                                    return [3 /*break*/, 1];
                                }
                                currentSpaces++;
                                return [3 /*break*/, 1];
                            }
                            while (currentSpaces > 0) {
                                currentValue += ' ';
                                currentSpaces--;
                            }
                            currentValue += String.fromCharCode(ch);
                            currentLength++;
                        }
                        _a.label = 8;
                    case 8: return [3 /*break*/, 1];
                    case 9:
                        fs.close(this.fd, function () { });
                        if (currentArray.length > 0 && currentLine >= this.skipCount) {
                            this.callbackWithData(currentArray);
                        }
                        // At the end of file, send null to let the client know it's end of the file.
                        this.callback(null);
                        return [2 /*return*/, true];
                }
            });
        });
    };
    TabbedFileImportAsync.prototype.doReadBuffer = function () {
        var _this = this;
        return new Promise(function (resolve) {
            fs.read(_this.fd, _this.buffer, 0, _this.sLength, null, function (err, readSize, buffer) {
                _this.readPos = 0;
                _this.readSize = readSize;
                if (err || !readSize || readSize < 1) {
                    resolve(false);
                }
                else {
                    resolve(true);
                }
            });
        });
    };
    /**
     * Make the callback with either an array of values
     * or a dictionary of values if hasHeader is set
     */
    TabbedFileImportAsync.prototype.callbackWithData = function (currentArray) {
        if (currentArray.length === 0) {
            return [];
        }
        if (!this.hasHeader) {
            return this.callback(currentArray);
        }
        var result;
        if (!this.headers) {
            this.headers = [];
            // Check to see if we got the delim wrong
            if (currentArray.length === 1 && /,/.test(currentArray[0])) {
                var reCSVHeader = /(?:\s*(?:\"([^\"]*)\"|([^,]+))\s*,?)+?/g;
                var m = reCSVHeader.exec(currentArray[0]);
                while (m) {
                    if (m[1]) {
                        // matched something in quotes
                        this.headers.push(m[1]);
                    }
                    else {
                        // matched something without quotes
                        this.headers.push(m[2]);
                    }
                    m = reCSVHeader.exec(currentArray[0]);
                }
            }
            else {
                this.headers = currentArray;
            }
            // Switch to CSV Comma format
            this.seperator = this.headers.length > 0 ? 44 : this.seperator;
        }
        else {
            var currentValues = {};
            for (var x in this.headers) {
                currentValues[this.headers[x]] = currentArray[x];
            }
            result = this.callback(currentValues);
        }
        return result;
    };
    return TabbedFileImportAsync;
}());
exports.default = TabbedFileImportAsync;
//# sourceMappingURL=TabbedFileImportAsync.js.map