class ViewTestForms2 extends View

    getLayout: ()=>
        view: "Docked"
        docked:
            view: "NavMenu"
            name: "viewMenu"
        main:
            view: "Form"
            name: "viewForm"
        location: "right"
        size: 200

    setClickFunction: (code)=>
        return ()=>
            @viewForm.setPath "zipcode", code
            console.log "SET CODE:", code

    onShowScreen: ()=>

        ##|  setup form
        @viewForm.createFromTable "zipcode", false
        @viewForm.setSubmitFunction (form) =>
            alert "Form Submitted Successfully!\nTest value1 = #{form.input1.value}"

        ##|
        ##|  Setup menu

        @viewMenu.setTitle "Test List"

        group = @viewMenu.addGroup
            title:     "Zipcodes"
            isEnabled: true
            isOpen:    true

        for code in ["01014", "01020", "01022", "01344", "01520", "01746", "01965", "02171", "02460", "02841"]

            item = group.addItem
                title:      code
                isEnabled:  true
                callback:   @setClickFunction(code)

        @viewMenu.show()

        true