import { MongoClient }  from 'mongodb';
import { config }       from 'edgecommonconfig';
import Worker           from '../src/Worker';
import DataSetManager   from 'edgedatasetmanager';

const should    = require('should');
const host      = process.env.DB_HOST || 'localhost';

config.setCredentials('azureTest', {connection: 'DefaultEndpointsProtocol=https;AccountName=obviotest;AccountKey=IRfy3COidoce4u7W3L6dQ1bI+XrSRVPjsGsIa46Fs+JxM8nOTuRt8escpcFIDWGnOCDrqV56FPXPGmA7qyCrWA==;EndpointSuffix=core.windows.net'});
config.setCredentials('MongoDB', {
    url: `mongodb://${host}:27017/?replicaSet=rs0`,
    url_replicaSet: `mongodb://${host}:27017?replicaSet=rs0`,
    options: {
        poolSize: 16,
        socketTimeoutMS: 600000,
        connectTimeoutMS: 600000
    }
});
config.setCredentials('mqHost', process.env.AMQP_HOST || 'amqp://localhost:5672');

describe('Worker', () => {
    const worker = new Worker('azureTest', 'testChangeLogs');
    const testRecord   = {
        name    : 'Brian Pollack',
        age     : 42,
        birthday: new Date('1975-10-02 10: 04: 05'),
        income  : 17.64,
        id      : 'status'
    };

    before(async () => {
        if (host === 'localhost') {
            await setupReplica();
        }

        const config_rules: any[] = [{
            title    : 'Reprocessing due to raw change',
            condition: 'raw/[a-z]',
            action   : 'workqueue',
            wq_name  : 'reprocessing'
        }, {
            title    : 'Reprocessing due to property change',
            condition: 'property/.*(status|price|loc)',
            action   : 'workqueue',
            wq_name  : 'reprocessing'
        }, {
            title    : 'Reprocessing due to property change',
            condition: 'property/.*(status|price|loc)',
            action   : 'workqueue',
        }, {
            title    : 'List price change',
            condition: 'list.price',
            action   : 'messagequeue',
            mq_name  : 'price-change'
        }, {
            title    : 'Listing status change',
            condition: 'status',
            action   : 'messagequeue',
            mq_name  : 'price-change'
        }, {
            title    : 'Listing status change',
            condition: 'status',
            action   : 'messagequeue',
        }];

        const dm = new DataSetManager('lookup');

        let i = 1;
        for (const rule of config_rules) {
            rule.id = i;
            console.log(`adding rule to change_rules collection`, rule);
            await dm.doInsert(`/change_rules/${i}`, rule);
            i = i + 1;
        }

    });

    it('Should save change record for logging', async () => {
        const dt = new Date();
        dt.setSeconds(dt.getSeconds() + 1);

        const expected = { 
            dataset: 'test',
            collection: 'property',
            change_date: dt,
            kind: 'insert',
            stamp: dt 
        };

        setTimeout(async () => {
            const collection: any = await getCollection('test', 'property');
            await collection.insertOne(testRecord);
        }, 7000);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                worker.doRun((err: any, changeRec: any) => {
                    console.log('ChangeRec', changeRec);
                    if (err) {
                        return reject(err);
                    }
    
                    should.not.exist(err);
                    changeRec.dataset.should.equal(expected.dataset);
                    changeRec.collection.should.equal(expected.collection);
                    changeRec.kind.should.equal(expected.kind);
                    resolve(changeRec);
                    process.exit(0);
                });
            }, 5000);
        })
    });

});

async function setupReplica() {
    const client: any = await MongoClient.connect(`mongodb://${host}:27017`, { useNewUrlParser: true });
    const rsconf = {
        '_id' : 'rs0',
        'members': [
            { '_id' : 0, 'host' : `${host}:27017` }
        ]
    };
    const adminDb = client.db('test').admin();

    return adminDb.command({ replSetInitiate: rsconf });
}

async function getCollection(dbName: string = 'test', tableName: string = 'test') {
    const client: any = await MongoClient.connect(`mongodb://${host}:27017/${dbName}?replicaSet=rs0`, { useNewUrlParser: true });
    const db = client.db(dbName);
    let collection = db.collection(tableName);

    if (!collection) {
        await db.createCollection(tableName);
    }

    return collection;
}