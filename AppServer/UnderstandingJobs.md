# Example Job Data

Data for Jobs is located in the following location:

	dataSet = "os"
	collection = "jobs"
	
Example of some jobs:

This example shows a job that is to run "hourly" with a frequency of 24 which means it should run 
no more often then 1 time every 24 hours.   If the "lastRun" is older than 24 hours it will
attempt to run again.

The "lastDuration" is the number of second it took for this job to execute.

enabled - Boolean that indicates the job is active and should be run.

timeout - number of milliseconds to wait for timeout once data received, gets reseted when data received

	Job 'eb22a42d4e2139109a27dc05b3e5b749837ebb11' =
	    _id          : 57d1871fe7c6647f81e730e9
	    id           : 'eb22a42d4e2139109a27dc05b3e5b749837ebb11'
	    title        : 'Import T085 Full 2352'
	    freqType     : 'hourly'
	    freqRate     : 24
	    folder       : 'EdgeImportScripts'
	    script       : 'import_mainframe.coffee'
	    args         : '-t T085 -o T063_LCT_NBR\=2352 -s 2352'
	    owner        : 'System'
	    enabled      : true
	    pending      : false
	    lastRun      : Monday, February 6th 2017, 4:38:31 pm
	    lastDuration : 511,449
	    timeout      : 30000

