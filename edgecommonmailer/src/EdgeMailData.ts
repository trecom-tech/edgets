/**
 * Mail Data Interface that extends MailData of sendgrid
 */
import { MailData } from '@sendgrid/helpers/classes/mail';

export default interface EdgeMailData extends MailData {
    // Markdown Template
    mdTemplate?: string
} 