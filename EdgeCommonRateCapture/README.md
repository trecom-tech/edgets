# EdgeCommonRateCapture
> Provides a common class to capture the rate that something is happening and send it to the API / Logging server for reporting as well as the terminal for display if required. For example Downloading x bytes per second or reading y records per second.

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonRateCapture.git

## Creating a basic EdgeRateCapture

Passing in a name will create a rate capture, setup a timer to update the display and allow everything to run.  

    import EdgeRateCapture from 'edgecommonratecapture';
    let rateCapture = new EdgeRateCapture("name")

You just need to pass in the total amount processed like below:

    rateCapture.addSample(total);

If you want an ETA and percent complete, pass in the number remaining

    rateCapture.addSample(totalSoFar, totalRemaining);

## Additional methods

### Add variable

Add a variable to track

    rateCapture.addVar("name", "title", 10);

### Increase variable

    rateCapture.incVar("name", 10);

### Set variable

    rateCapture.setVar("name", 10);

### Reset tracking

Reset all tracking

    rateCapture.reset();

### Stop tracking

Stop all tracking

    rateCapture.stop();

