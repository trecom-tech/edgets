# AddressNormalizer
> Takes an address and normalizes the parts and returns an object

    npm install git+ssh://git@gitlab.protovate.com:Edge/EdgeBlockAddressNormalize
    import AddressNormalizer from 'edgeblockaddressnormalize';

## Input format

    AddressNormalizer.process(addressText, city, state, zip);

## Output format

The normalizer returns the following fields as an example:

    {
        number:  '293',
        street:  'Montibello Dr',
        city:    'Mooresville',
        state:   'NC',
        zip:     '28177',
        display: '293 Montibello Dr, Mooresville, NC, 28177',
        unit:    ''
        lot:     ''
        hash:    'aab4c769f75773f93e47363d64265cc3'
    }

## Details

* State names are converted to the short form names
* All names are converted to mixed case
* Missing data such as zipcode, city, and state are attempted to fill in

## Test

* `npm install` to install dependencies
* `npm test` to run tests