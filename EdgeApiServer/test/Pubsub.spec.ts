import './helper/setup';
import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';

describe('Pubsub Feature', () => {

    let apiConnection1: EdgeApi;
    let apiConnection2: EdgeApi;

    // Act before assertions
    before(async () => {
        apiConnection1 = new EdgeApi();
        await apiConnection1.doConnect();

        apiConnection2 = new EdgeApi();
        await apiConnection2.doConnect();
        apiConnection2.subscribe('testChannel');
    });

    describe('pubsub functionality', () => {
        it('should test pubsub', async () => {
            // apiConnection1.do
            setTimeout(function() {
                apiConnection1.data_doSendChannelMessage('testChannel', {
                    message: 'testMessage'
                });
            }, 2000);

            return new Promise((resolve, reject) => {
                apiConnection2.listen('testChannel', (data: any) => {
                    expect(data.message).to.equal('testMessage');
                    resolve();
                });
            });
        });
    });

});