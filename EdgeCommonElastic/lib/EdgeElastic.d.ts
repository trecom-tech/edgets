export default class EdgeElastic {
    static singleInstance: EdgeElastic;
    private cache;
    private log;
    private base;
    private client;
    /**
     * @returns singleton for the elastic search
     */
    static getElastic(): EdgeElastic;
    /**
     * Create an instance of EdgeElastic
     */
    constructor();
    /**
     * get mappings for index/type
     * @param params {index, type}
     */
    doGetMapping(params: {
        index: string;
        type: string;
    }): Promise<{}>;
    /**
     * delete index
     * @param params {index}
     */
    doDeleteIndex(params: {
        index: string;
    }): Promise<string>;
    /**
     * retrives all indices
     */
    doGetIndexes(): Promise<any>;
    /**
     * update dataset index, collection type with id and body (doc)
     * @param options {dataset, collection, id, doc}
     */
    doUpdate(options: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
    }): Promise<any>;
    /**
     * get cached dataset index, colllection type
     * @param options {dataset: index, colleciton: type, body: search options}
     */
    doGet(options: {
        dataset: string;
        collection: string;
        body: any;
    }): Promise<any>;
    /**
     * Search for collection
     * @param params {dataset, collection, query, sourceFilter, limit}
     */
    doSearch(params: {
        dataset: string;
        collection: string;
        query: string;
        sourceFilter: any[];
        limit: number;
    }): Promise<{}>;
    /**
     * set index dataset index with id and doc.
     * @param params {dataset, collection, id, doc}
     */
    doIndex(params: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
    }): Promise<any>;
    /**
     * Returns the hash values as stored in Elastic
     * @param params {dataset, collection ,id}
     */
    doGetDocumentHash(params: {
        dataset: string;
        collection: string;
        id: string;
    }): Promise<any>;
    setDocumentHash(params: {
        dataset: string;
        collection: string;
        id: string;
        doc: any;
        subpath: string;
    }): Promise<any>;
}
