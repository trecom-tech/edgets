import {expect}       from 'chai';
import * as fs        from 'fs';
import JobRunner      from '../src/EdgeLauncher';
import NpmModuleTools from '../src/NpmModuleTools';

const runner = new JobRunner();

interface Job {
    id: string;
    title: string;
    freqType?: string | null;
    freqRate?: number | null;
    folder?: string | null;
    owner?: string | null
    enabled?: boolean;
    pending?: boolean;
    lastRun?: Date | null;
    lastDuration?: number | null;
    npm?: string | null;
    args?: string | null;
    script?: string | null
    lastDataReceived?: Date | null;
    timeout?: number | null;
}

const dataTestJob1: Job = {
    "id": "TESTJOB00000001",
    "title": "One-time test job for npm modules",
    "freqType": "once",
    "freqRate": 1,
    "npm": "git+ssh://git@gitlab.protovate.com:EdgeTS/DummyJob.git",
    "folder": "node_modules/dummyjob",
    "script": "npm start",
    "owner": "System",
    "enabled": true,
    "pending": false,
    "lastRun": null,
    "lastDuration": null,
};

const dataTestJob2: Job = {
    "id": "TESTJOB00000002",
    "title": "Timeout test job for Jobs module",
    "freqType": "once",
    "freqRate": 1,
    "folder": "test",
    "script": "timeout.sh",
    "args": "",
    "owner": "System",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
};

const dataTestJob3: Job = {
    "id": "TESTJOB00000003",
    "title": "One-time test job for Jobs module",
    "freqType": "once",
    "freqRate": 1,
    "folder": "test",
    "script": "test_job_1.sh",
    "args": "arg1 arg2 arg3",
    "owner": "System",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
    "timeout": 2000,
};

const dataTestJob4: Job = {
    "id": "TESTJOB00000004",
    "title": "One-time test job for missing module and wrong path",
    "freqType": "once",
    "freqRate": 1,
    "npm": "npm start",
    "folder": "wrong_path",
    "args": "",
    "owner": "",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
    "lastDataReceived": new Date(),
    "timeout": 2000,
};

const dataTestJob5: Job = {
    "id": "TESTJOB00000005",
    "title": "One-time test job for JS script",
    "freqType": "once",
    "freqRate": 1,
    "folder": "test",
    "script": "test_job_2.js",
    "args": "",
    "owner": "",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
    "lastDataReceived": new Date(),
    "timeout": 2000,
};

const dataTestJob6: Job = {
    "id": "TESTJOB00000006",
    "title": "One-time test job for JS script",
    "freqType": "once",
    "freqRate": 1,
    "folder": "test",
    "script": "test_job_3.pl",
    "args": "",
    "owner": "",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
    "timeout": 2000,
};

const dataTestJob7: Job = {
    "id": "TESTJOB00000007",
    "title": "One-time test job for coffee script",
    "freqType": "once",
    "freqRate": 1,
    "folder": "tests",
    "script": "test_job_4.coffee",
    "args": "",
    "owner": "",
    "enabled": true,
    "pending": false,
    "lastDuration": null,
    "lastRun": null,
    "timeout": 2000,
};

const dataTestJob8: Job = {
    id: "TESTJOB00000008",
    title: "empty job",
    npm: "npm",
    args: "arg1, arg2",
};

describe('JobRunner', () => {
    /*
    * Add a simple one time job
    * Execute the job
    * Notice this record has "npm" and not "script"
    * */
    it('Add a simple one time job', async () => {
        await runner.runJob(dataTestJob1, 1)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    /*
    * Execute the job timeout.sh that sleeps for 20 seconds
    * timeout.sh contains 4 sleep calls: 10, 15, 10 and 25 seconds
    * at 25 seconds it fails
    * */
    it('Execute the job timeout.sh that sleeps for 20 seconds', () => {
        runner.runJob(dataTestJob2, 1)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    /*
    * Add a simple one time job
    * Execute the job
    * */
    it('One-time test job for Jobs module', async () => {
        await runner.runJob(dataTestJob3, 1)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('One-time test job for missing module and wrong path', async () => {
        await runner.runJob(dataTestJob4, null)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('One-time test job for JS script', async () => {
        await runner.runJob(dataTestJob5, 5)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('One-time test job for Perl script', async () => {
        await runner.runJob(dataTestJob6, 1)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('One-time test job for coffee script', async () => {
        await runner.runJob(dataTestJob7, null)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('Empty job',  done => {
        runner.runJob(dataTestJob8, 8)
            .then(result => {
                expect(result).to.equal(true);
                done();
            })
            .catch(error => console.log(error));
    });

    it('Disable JobRunner to insert a job.', async () => {
        const cmd = 'npm start';
        const fullOutput = "JobRunner: Disable";
        const result = await runner.insertJobLog(dataTestJob2, cmd, fullOutput, 1);
        expect(result).to.equal(true);
    });

    it('Check npm modules and update as latest version.', async () => {
        await NpmModuleTools.verifyInstalled('chalk', process.cwd())
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });

    it('Missing module source in the wrong path.', async () => {
        await NpmModuleTools.verifyInstalled('missing/moduleSource', `${process.cwd()}/wrong_path/`)
            .then(result => {
                expect(result).to.equal(true);
            })
            .catch(error => console.log(error));
    });
});
