"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Class to normalized EdgeMailer Error.
 */
class EdgeCommonMailerError extends Error {
    constructor(message, code = 'Error', options = {}) {
        super(message);
        this.message = message;
        this.code = code;
        this.options = options;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.EdgeCommonMailerError = EdgeCommonMailerError;
exports.default = EdgeCommonMailerError;
//# sourceMappingURL=EdgeCommonMailerError.js.map