// 
//  class for boolean data type
// 
//  @extends [DataFormatterType]
// 
import DataFormatterType from'./DataFormatterType';

export class DataFormatBoolean extends DataFormatterType {
    name: string        = 'boolean';
    width: number       = 70;
    input_width: number = 70;
    textYes: string     = "<i class='fas fa-circle'></i> Yes";
    textNo: string      = "<i class='far fa-circle'></i> No";
    textNotSet: string  = "<i class='far fa-ban'></i> Not Set";
    align: string       = 'left';

    constructor() {
        super();
    }

    //
    //  funtion to open editor including ace code editor
    //
    //  @param [JqueryObject] elParent parent element
    //  @param [Integer] left left position offset
    //  @param [Integer] top top position offset
    //  @param [Integer] width width of the editor
    //  @param [Integer] height height of the editor
    //  @param [Object] currentValue current value of the cell
    //  @param [String] path path where the value is being edited
    //  @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        if (currentValue) {
            currentValue = false;
        } else {
            currentValue = true;
        }

        this.saveValue(currentValue);
        return true;
    }

    //
    //  Inline boolean editor
    attachEditor(elSourceField: any, path: any) {
        super.attachEditor(elSourceField, path);

        const valFunction = elSourceField.val;
        elSourceField.val = (newValue: any) => {
            //
            //  If ViewForms or someone tries to val() a checkbox
            //  intercept and set the checked property
            if ((newValue == null)) {
                return elSourceField.prop('checked');
            }
            if (this.unformat(newValue, path)) {
                return elSourceField.bootstrapToggle('on');
            } else {
                return elSourceField.bootstrapToggle('off');
            }
        };

        //
        //  Hide the text editor and just make it text
        const currentValue = elSourceField.val();
        // console.log "On Create Checkbox:", currentValue
        elSourceField.attr("type", "checkbox");
        elSourceField.bootstrapToggle({
            on: "Yes",
            off: "No"
        });

        if (currentValue === true) {
            elSourceField.bootstrapToggle('on');
        } else {
            elSourceField.bootstrapToggle('off');
        }
        return true;
    }


    //
    //  funtion to format the currently passed data
    //
    //  @param [Object] data data to be formatted
    //  @param [Object] options additonal options defined for the datatype
    //  @param [String] path path where the value is being edited
    //  @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if ((data == null)) {
            return this.textNotSet;
        }
        if (data === "") {
            return this.textNotSet;
        }
        if ((data === null) || (data === 0) || (data === false) || (data === "false") || (data === "0")) {
            return this.textNo;
        }
        return this.textYes;
    }

    //
    //  funtion to unformat the currently formatted data
    //
    //  @param [Object] data data to be unformatted
    //  @param [Object] options additonal options defined for the datatype
    //  @param [String] path path where the value is being edited
    //  @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        if ((data == null)) {
            return false;
        }

        if (typeof(data) === "boolean") {
            return data;
        }
        if (data === this.textYes) {
            return true;
        }
        if (data === this.textNo) {
            return false;
        }
        if (data === "0") {
            return false;
        }
        if (data === "1") {
            return true;
        }
        if ((data === "Y") || (data === "Yes") || (data === "y") || (data === "yes")) {
            return true;
        }
        if ((data === "N") || (data === "No") || (data === "n") || (data === "no")) {
            return true;
        }
        if ((data === "T") || (data === "True") || (data === "t") || (data === "true")) {
            return true;
        }
        if ((data === "F") || (data === "False") || (data === "f") || (data === "false")) {
            return false;
        }
        if ((typeof(data) === "number") && (data >= 1)) {
            return true;
        }
        if ((typeof(data) === "number") && (data === 0)) {
            return false;
        }

        if (typeof data === "boolean") {
            if (data) {
                return true;
            }
            return false;
        }

        if ((data === null) || (data === 0)) {
            return false;
        }
        if ((data === "No") || (data === "no") || (data === "false") || (data === "off")) {
            return false;
        }
        return true;
    }
}

export default DataFormatBoolean;