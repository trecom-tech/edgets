/**
 * A class that models a single record
 */
import { config }  from 'edgecommonconfig';
import { ObjectID } from 'mongodb';
import DataSetPath from './DataSetPath';

export class DataItem{
    basePath: string = null;
    base    : any    = null;
    id      : string;

    /**
     * Initialize the Item requires an item id and store id
     * @param path
     * @param db
     * @param data
     */
    constructor(public path: DataSetPath, public db: any, public data: any) {

        if (this.data == null) {
            this.data = { id: this.id };
        }

        // Set the base pointer based on the sub path if needed

        this.base = this.data;
        this.basePath = this.path.subPath.join('.');

        let counter = 0;
        while (this.path.subPath != null && this.path.subPath.length > counter) {
            let targetPath = this.path.subPath[counter];
            if (this.base[targetPath] != null) {
                this.base = this.base[targetPath];
            } else {
                this.base[targetPath] = {};
                this.base = this.base[targetPath];
            }
            counter++;
        }
    }

    /**
     * Internal virtual function that saves items or updates
     */
    async doSaveUpdates(): Promise<boolean> {

        // Item has been saved, update the last modified and sub path
        const setCondition: any = {};
        if (this.basePath.length > 0) {

            setCondition[this.basePath] = this.base;
            setCondition._lastModified = new Date();
            try {
                let result = await this.db.doUpdateOne({ _id: this.data._id }, { '$set': setCondition })
                return true;
            } catch(e) {
                console.log("UpdateOne Error:", e);
                throw e;
            }
        } else {

            if (typeof this.data._id === "string" && this.data._id.length === 24) {
                this.data._id = ObjectID.createFromHexString(this.data._id);
            }

            this.data._lastModified = new Date();
            try {
                await this.db.doUpdateOne({ _id: this.data._id }, { '$set': this.data });
                return true;
            } catch(e) {
                // config.reportError "UpdateOne Saving Error:", e
                config.inspect(setCondition);
                console.log("UpdateOne Saving Error", e.toString());
                throw e;
            }
        }
    }
}

export default DataItem;