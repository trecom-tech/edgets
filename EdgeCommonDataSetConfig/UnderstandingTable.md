# Table Class of EdgeCommonDataSetConfig
> This Table Class exposes a collection of columns that can be saved or loaded using serialize functions.

We can create a collection named 'todo' , for example:

    Table = new DataSetConfig.Table('todo')

# Class Members

After creating a table like it above, we SHOULD configure the columns in table.
> If there is no configuration for columns in table, all processing will be failed.

So we can use _unserialize()_ member function and this method require the settings objects for columns.

    Table.unserialize(settingsObject).

For example, we can configure with with this configuration object.

    settingsObject = 
        id:
            name: 'ID'
            source: 'id'
            type: 'int'
            width: 80
            tooltip: ''
            ...
        name:
            name: 'Title'
            source: 'server_name'
            type: 'text'
            width: null
            tooltip: ''
            ...
        username:
            name: 'User Name'
            source: 'username'
            type: 'text'
            width: 130
            ...
        ...

    Table.unserialize(settingsObject)

In this case, we can create columns for table with the given settings object which has id, name, username fields or something like that.


Also we can get JSON objects for current columns in Table.

    columns = Table.serialize()


Especially in order to get the value of the specific column, we can use _getColumn()_ member function.
This method require the string of the column. For example, we suppose to get the value of username field.
    
    username = Table.getColumn('username')
