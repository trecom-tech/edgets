##|
##|  To help with automated testing
globalChartCounter = 0

class ViewGraphTime extends View

    getDependencyList: () =>
        return [ "/vendor/canvasjs.min.js" ]

    onSetupButtons: () =>
        console.log "ViewGraphTime onSetupButtons holder=", @elHolder

    onShowScreen: () =>
        console.log "ViewGraphTime onShowScreen holder=", @elHolder
        @chartOptions = {}
        @axisX  = new DataAxis()
        @axisY  = new DataAxis()
        @chartOptions.backgroundColor = "#EEEEEE"
        @chartOptions.animationEnabled = true
        @chartOptions.zoomEnabled = true
        @chartOptions.theme = "light2"

    addAxisY: ()=>
        if !Array.isArray(@axisY)
            tmp = [ @axisY, new DataAxis() ]
            @axisY = tmp
        else
            @axisY.push new DataAxis()

        return @axisY[@axisY.length-1]

    toUcfirst: (str)=>
        return str.charAt(0).toUpperCase() + str.slice(1);

    setTitle: (title)=>
        @chartOptions.title =
            text            : title
            fontSize        : 30
            horizontalAlign : "right"
        return @chartOptions.title

    ##|     Add a sweep line
    ##|     Params
    ##|         lineName: sweep line title
    ##|         lineValue: sweep line y-axis value

    addSweepLine: (lineName, lineValue) =>
        if @axisY.data.stripLines?
            @axisY.data.stripLines.push {value: Number(lineValue), label: lineName}
        else
            @axisY.data.stripLines = [{value: Number(lineValue), label: lineName}]
        true

    ##|     Add a Series
    ##|     Params
    ##|         title: GraphTime Chart Title
    ##|         tableName: Tablename for Chart Data
    ##|         dateColume: x-axis date colume
    ##|         valueColume: y-axis value Colume

    addSeries: (title, tableName, dateColume, valueColume=null)=>
        @axisX.data.title = @toUcfirst dateColume
        if valueColume isnt null
            @axisY.data.title = @toUcfirst valueColume

        ds = new DataSeries()
        ds.setSeriesType "line"
        ds.indexLabel = "{y[#index]}"
        ds.markerSize = 5
        data = DataMap.getValuesFromTable(tableName)

        data.sort (a,b)->
            if a[dateColume] > b[dateColume]
                return 1
            else if b[dateColume] > a[dateColume]
                return -1
            else
                return 0

        sum = 0
        for i, obj of data
            dat = new Date(obj[dateColume.toLowerCase()])
            if dat isnt "Invalid Date"
                mnt = moment(obj[dateColume.toLowerCase()]).format('MMM D, h:m')
                if valueColume is null
                    ds.addPoint mnt, 1
                    sum += 1
                else if obj[valueColume.toLowerCase()]?
                    val = Number(obj[valueColume.toLowerCase()])
                    if val?
                        ds.addPoint mnt, val
                        sum += val
        sum = Math.floor(sum*100) / 100
        @setTitle(title+" (#{sum})").horizontalAlign = "center"
        @setData ds


##|
##|  Add a DataSeries object
    setData: (dataSeries)=>

        if !dataSeries?
            return

        if !@chartOptions?
            return

        if !@chartOptions.data?
            @chartOptions.data = []

        if dataSeries.data.type != "doughnut"
            dataSeries.setIndexThemeColor(@chartOptions.data.length)

        @chartOptions.data.push dataSeries.getData()
        return @chartOptions

    onResize : (w, h)=>
        super(w, h)
        if @chart?
            return @onRender()
        return

    onRender: ()=>
        @chart.render()
        true

    showGraphTime: (name)=>
        if @chart? then return @onRender()

        @chartOptions.axisX = @axisX.data

        if Array.isArray(@axisY)
            @chartOptions.axisY = []
            for a in @axisY
                @chartOptions.axisY.push a.data
        else
            @chartOptions.axisY = @axisY.data

        @gid = globalChartCounter++
        if name? then @gid = name
        @elHolder.find(".graphTimeHolder").html("<div id='chart#{@gid}'/>")
        @chart = new CanvasJS.Chart("chart" + @gid, @chartOptions)
        @chart.render()

        true
