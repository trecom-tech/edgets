newImage1 = new Image()
newImage1.src = "./js/test_Data/images/1.jpg"
newImage2 = new Image()
newImage2.src = "./js/test_Data/images/2.jpg"
newImage3 = new Image()
newImage3.src = "./js/test_Data/images/3.jpg"
newImage4 = new Image()
newImage4.src = "./js/test_Data/images/4.jpg"
newImage5 = new Image()
newImage5.src = "./js/test_Data/images/5.jpg"
newImage6 = new Image()
newImage6.src = "./js/test_Data/images/6.jpg"
newImage7 = new Image()
newImage7.src = "./js/test_Data/images/7.jpg"
newImage8 = new Image()
newImage8.src = "./js/test_Data/images/8.jpg"
newImage9 = new Image()
newImage9.src = "./js/test_Data/images/9.jpg"
newImage10 = new Image()
newImage10.src = "./js/test_Data/images/10.jpg"
newImage11 = new Image()
newImage11.src = "./js/test_Data/images/1.jpg"

$ ->

    addTestButton "Simple splitter", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view: "Splittable"
                left: "TestShowSize"
                right: "TestShowSize"

    addTestButton "Simple splitter with size", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view: "Splittable"
                top: "TestShowSize"
                bottom: "TestShowSize"
                percent: 30

    addTestButton "Multiple Splitters with variable names", "Open", ()->

        addHolder()
        .setView "TestLayout4", (view)->

    addTestButton "Splitters and Docks", "Open", ()->

        addHolder()
        .setView "TestLayout5", (view)->

    addTestButton "Splitters, Docks, Tabs", "Open", ()->

        addHolder()
        .setView "TestLayout6", (view)->

    addTestButton "Testing callback function", "Open", ()->

        addHolder()
        .setView "TestLayout3", (view)->
            console.log "If this worked, the view will split top and bottom at 40%"

    addTestButton "Automatic Layout Using Helpers", "Open", ()->

        addHolder()
        .setView "TestLayout2", (view)->
            console.log "Console.log should have stuff"

    addTestButton "Customized Tabs in Splitter", "Open", ()->

        addHolder()
        .setView "TestLayout7", (view)->

    addTestButton "Multi Layer Tabs in Splitter", "Open", ()->

        addHolder()
        .setView "TestLayout8", (view)->

    addTestButton "GridStack with autolayout", "Open", ()->

        addHolder()
        .setView "TestLayout11", (view)->  

    addTestButton "Table & TableDetailed in Splittable", "Open", () ->

        loadZipcodes()
        .then ()->
            doPopupView "TestLayout1", "Testing Fast Layout", "popup_splittableWidget3", 1200, 800, (view)->
                view.runTest
                    view: "Splittable"
                    percent: 99
                    left:
                        view: "Splittable"
                        percent: 50
                        top:
                            view: "Table"
                            collection: "zipcode"
                        bottom:
                            view: "DynamicTabs"
                            tabs: [
                                title: "Table 1"
                                view: "Table"
                                collection: "zipcode"
                            ,
                                title: "Table 2"
                                view: "Table"
                                collection: "zipcode"
                            ,
                                title: "Table 3"
                                view: "Table"
                                collection: "zipcode"
                            ]
                    right:
                        minwidth: 240
                        view: "Table"
                        detailed: true
                        collection: "zipcode"

    addTestButton "Popup Splittable-Widget : Text/Tabs(Table & ImageStrip)", "Open", () ->
        doPopupView "Splittable", "Splittable Widget", "popup_splittableWidget2", 900, 600, (view)->

            view.getFirst().html '<p style="font-size:30px;">Dummy Text</p>'

            view.getSecond().setView "DynamicTabs", (tabsView)->
                newPromise () ->
                    yield loadZipcodes()

                    tabsView.doAddViewTab "Table", "Table", (view) ->
                        view.loadTable "zipcode"

                    tabsView.doAddViewTab "ImageStrip", "Images", (view) ->
                        view.addImage newImage1
                        view.addImage newImage2
                        view.addImage newImage3
                        view.addImage newImage4
                        view.addImage newImage5
                        view.addImage newImage6
                        view.addImage newImage7
                        view.addImage newImage8
                        view.addImage newImage9
                        view.addImage newImage10
                        view.addImage newImage1.src
                        view.addImage newImage2.src
                        view.addImage newImage3.src
                        view.addImage newImage4.src
                        view.addImage newImage5.src
                        view.addImage newImage6.src
                        view.addImage newImage7.src
                        view.addImage newImage8.src
                        view.addImage newImage9.src
                        view.addImage newImage10.src

    addTestButton "Simple Vertical 50/50", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setPercent(50)
            splitter.getFirst().html "Left side should be 50%"
            splitter.getSecond().html "Right side should be 50%"

    addTestButton "Use LocalStrage by setting configName", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setPercent 50
            splitter.setConfigName "test1"
            splitter.getFirst().setView "TestShowSize"
            splitter.getSecond().setView "TestShowSize"

    addTestButton "Simple Vertical 50/50 - Left Min Size", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setPercent(50)
            splitter.getFirst().html "Left side should be 50%, min 200px"
            splitter.getSecond().html "Right side should be 50%"
            splitter.getFirst().setMinWidth(200)

    addTestButton "Simple Horizontal 50/50 - Top Min Size", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setHorizontal()
            splitter.setPercent(50)
            splitter.getFirst().html "Top should be 50%"
            splitter.getFirst().setMinHeight 300
            splitter.getSecond().html "Bottom should be 50%"

    addTestButton "Simple Horizontal 50/50 with Events", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setHorizontal()
            splitter.setPercent(50)
            splitter.getFirst().html "Top should be 50%"
            splitter.getSecond().html "Bottom should be 50%"

            superResize = splitter.onResize
            splitter.onResize = (w, h)->
                superResize(w,h)
                PercentTop = Math.ceil(splitter.getPercent())
                PercentBot = 100 - PercentTop
                splitter.getFirst().html "Top should be #{PercentTop}% or #{splitter.size1} px"
                splitter.getSecond().html "Bottom should be #{PercentBot}% or #{splitter.size2} px"

    addTestButton "Vertical & Horizontal Splittable Widgets", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setHorizontal()
            splitter.setPercent(40)
            splitter.getFirst().html "Top should be 40%"
            splitter.getSecond().setView "Splittable", (splitter2)->
                splitter2.setPercent(40)
                splitter2.getFirst().html "Bottom Left side should be 40%"
                splitter2.getSecond().html "Bottom Right side should be 60%"

    addTestButton "Tab Splittable Widget", "Open", () ->
        addHolder()
        .setView "DynamicTabs", (tabs)->
            tabs.doAddViewTab "Splittable", "Tab Splittable Widget", (splitter) ->
                splitter.setPercent(25)
                splitter.getFirst().html "Left side should be 25%"
                splitter.getSecond().html "Right side should be 75%"

            tabs.addTab "Empty Tab", "<p style='font-size:xx-large;'>--- Another Tab ---</p>"

        true

    addTestButton "Timeline with autolayout", "Open", ()->

        addHolder()
        .setView "TestLayout12", (view)->

    addTestButton "Tabs in Multi-level Splitter", "Open", ()->

        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view: "DynamicTabs"
                activetab: "Second Tab"
                tabs: [
                    title: "First Tab"
                    view: "Splittable"
                    percent: 50
                    top:
                        view: "TestShowSize"
                    bottom:
                        view: "Splittable"
                        percent: 50
                        left: 
                            view: "DynamicTabs"
                            tabs: [
                                title: "Tab1"
                                view: "DynamicTabs"
                                tabs: [
                                    title: "Tab11"
                                    view: "TestShowSize"
                                    ontabshow: (tab) ->
                                        console.log "Tab11 is showing: ", tab
                                ,
                                    title: "Tab12"
                                    view: "TestShowSize"
                                    ontabshow: (tab) ->
                                        console.log "Tab12 is showing: ", tab
                                ]
                                ontabshow: (tab) ->
                                    console.log "Tab1 is showing: ", tab
                            ,
                                title: "Tab2"
                                view: "TestShowSize"
                                ontabshow: (tab) ->
                                    console.log "Tab2 is showing: ", tab
                            ]
                        right:
                            view: "TestShowSize"
                ,
                    title: "Second Tab"
                    view: "TestShowSize"
                ]

    addTestButton "Splitter with Tables", "Open", () ->

        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setHorizontal()
            splitter.setPercent(40)

            loadZipcodes().then ->
                splitter.getFirst().setView "Table", (viewTable1)->
                    viewTable1.loadTable "zipcode"

                splitter.getSecond().setView "Table", (viewTable2)->
                    viewTable2.loadTable "zipcode"
        true


    addTestButton "Table & TableDetailed in Splittable", "Open", () ->

        doPopupView "Splittable", "Splittable Widget", "popup_splittableWidget3", 1500, 800, (view)->
            view.setPercent(70)
            view.getSecond().setMinWidth(300)

            loadZipcodes().then ->

                view.getFirst().setView "Table", (viewTable1)->
                    viewTable1.loadTable "zipcode"

                view.getSecond().setView "Table", (viewTable2)->
                    viewTable2.setDetailed()
                    viewTable2.loadTable "zipcode"

        true

    ##|
    ##|  Demonstrates opening a view that contains several sub views.   The top view
    ##|  will be a table that switches locked rows once you check a different row.
    ##|  The bottom tables use "#element" tags in pug files and demonstrates and tests
    ##|  that the screen manager loads them differently.   The cascade function is used
    ##|  to pass changes to the sub views.
    ##|
    addTestButton "Param test 1", "Open", ()->

        addHolder().setView "Params1", (view)->
            console.log "Params1 Loaded:", view


    ##|
    ##|  Place a view and replace it
    addTestButton "Live Replace", "Open", ()->

        holder = addHolder()
        holder.setView "TestShowMessage1", (view)->
            console.log "TestShowMessage1 is loaded"

            ##|
            ##|  Should print
            globalTableEvents.emitEvent "test1", []
            globalTableEvents.emitEvent "test2", []
            globalKeyboardEvents.emitEvent "test3", []
            globalTableEvents.emitEvent "test_table_zipcode", []
            globalTableEvents.emitEvent "test_table_zipcode_invalid", []

            setTimeout ()=>
                holder.removeView()
                holder.setView "TestShowSize", (newView)->
                    console.log "NewView=", newView

                    ##|
                    ##|  Should not print
                    globalTableEvents.emitEvent "test1", []
                    globalTableEvents.emitEvent "test2", []
                    globalKeyboardEvents.emitEvent "test3", []
                    globalTableEvents.emitEvent "test_table_zipcode", []

            , 2000

    addTestButton "Show/Hide Child of ViewSplittable", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            splitter.setHorizontal()
            splitter.setPercent(40)
            splitter.getFirst().setView "TestShowSize"
            splitter.getSecond().html "Second Splitter"
            first  = splitter.getFirst()
            second = splitter.getSecond()
            setTimeout () =>
                second.hide()
                true
            , 2000
            setTimeout () =>
                second.show()
                true
            , 5000
        true

    addTestButton "Show/Hide ImagePopup Child of ViewSplittable", "Open", () ->
        addHolder("")
        .setView "Splittable", (splitter)->
            # splitter.setHorizontal()
            splitter.setPercent(40)
            splitter.getFirst().setView "TestShowSize"
            splitter.getSecond().setView "ImageStripPopups", (view)->
                view.setTitle "Test-ImagePopup1"
                view.setDataPath "zipcode", "/zipcode/03105/images"
                view.addImage newImage1
                view.addImage newImage2
                view.addImage newImage3
                view.addImage newImage4
                view.addImage newImage5
                view.addImage newImage6
                view.addImage newImage11
            true
            first  = splitter.getFirst()
            second = splitter.getSecond()
            setTimeout () =>
                second.hide()
                # first.hide()
                true
            , 2000
            setTimeout () =>
                second.show()
                true
            , 5000
        true

    addTestButton "Splitter serialize/deserialize", "Open", () ->
        newPromise () ->
            yield loadZipcodes()
            yield addHolder()
            .setView "Splittable", (view) =>
                view.deserialize
                    view: "Splittable"
                    percent: 99
                    left:
                        view: "Splittable"
                        percent: 50
                        top:
                            view: "Table"
                            collection: "zipcode"
                        bottom:
                            view: "DynamicTabs"
                            tabs: [
                                title: "Table 1"
                                view: "Table"
                                collection: "zipcode"
                            ,
                                title: "TestShowSize"
                                view: "TestShowSize"
                            ,
                                title: "Table 3"
                                view: "Table"
                                detailed: true
                                collection: "zipcode"
                            ]
                    right:
                        minwidth: 100
                        view: "TestShowSize"

                setTimeout () ->
                    options = view.serialize()
                    console.log "ViewSplittable serialized data = ", options
                    doPopupView "Splittable", "Splittable Serialized", null, 1200, 800, (viewPopup) ->
                        viewPopup.deserialize options
                        true
                , 3000
            return true

    go()
