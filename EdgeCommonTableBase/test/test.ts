import { MongoClient }  from 'mongodb';
import TableBase        from '../src/TableBase';
import { config }       from 'edgecommonconfig';

const should = require('should');
const host   = process.env.MONGODB_HOST ? process.env.MONGODB_HOST : 'localhost';
const port   = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : '27017';

config.setCredentials('ApiServers', [
    'http://127.0.0.1:8001',
    'http://127.0.0.1:8001'
]);

describe('TableBase', () => {

    const dataSet   = 'rets_raw';
    const tableName = 'raw';
    const id        = '36_Listing_5049080';
    const tb        = new TableBase(dataSet, tableName, id);

    before(() => {
        return MongoClient.connect(`mongodb://${host}:${port}/${dataSet}`, { useNewUrlParser: true })
        .then(async (client) => {
            const db = client.db(dataSet);
            const collection = db.collection(tableName);

            if (!collection) {
                await db.createCollection(tableName);
            }
            
            await collection.insertOne({
                id: '36_Listing_5049080',
                name: 'brian',
                age: 43,
                is_verified: true
            });
        }).catch((err) => {
            console.log('Error while connecting MongoDB:', err);
        });
    });

    it('Verify loaded', async () => {
        let result = await tb.doVerifyLoaded();
        result.id.should.equal(id);
    });

    it('Check if value has been set', () => {
        let result = tb.isset('test');
        result.should.equal(false);

        result = tb.isset('id');
        result.should.equal(true);
    });

    it('SafeString', async () => {
        let result = tb.safeString('test');
        result.should.equal('');

        result = tb.safeString('name');
        result.should.equal('brian');

        tb.data = null;
        result = tb.safeString('name');
        result.should.equal('');
        await tb.doVerifyLoaded();
    });

    it('SafeNumber', async () => {
        let result = tb.safeNumber('test');
        result.should.equal(0);

        result = tb.safeNumber('age');
        result.should.equal(43);

        tb.data = null;
        result = tb.safeNumber('age');
        result.should.equal(0);
        await tb.doVerifyLoaded();
    });

    it('SafeBool', async () => {
        let result = tb.safeBool('test');
        result.should.equal(false);

        result = tb.safeBool('is_verified');
        result.should.equal(true);

        tb.data = null;
        result = tb.safeBool('is_verified');
        result.should.equal(false);
        await tb.doVerifyLoaded();
    });

    it('Save', async () => {
        tb.data.name = 'test';
        let result = await tb.doSave();
        result.should.equal(1);

        tb.data.name = 'brian';
        result = await tb.doSave();
        result.should.equal(1);

        tb.data.name = 'brian'
        result = await tb.doSave();
        result.should.equal(0);
    });

    it('Aggregate', async () => {
        const result = await tb.doAggregate({ $match: { age: 43 } })
        result[0].id.should.equal(id);
    });

    it('Find a single record', async () => {
        let result: any = await tb.doFind({ age: 43 });
        result.id.should.equal(id);

        result = await tb.doFind({age: 999});
        should.not.exist(result);
    });

    it('Find all', async () => {
        let result = await tb.doFindAll();
        result[0].id.should.equal(id);

        result = await tb.doFindAll({ age: 43 }, { _id: 1 });
        result[0].id.should.equal(id);

        result = await tb.doFindAll({ age: 43 }, null, 0, 1);
        result[0].id.should.equal(id);

        result = await tb.doFindAll({ age: 999 }, null, 0, 1);
        result.length.should.equal(0);

        result = await tb.doFindAll(null, null, 0, 1);
        result[0].id.should.equal(id);
    });

    it('Verify index', () => {
        return tb.doVerifyIndex('simple_index', 'geo', true)
        .then((result: any) => {
            result.should.equal('simple_index_2dsphere');
        });
    });

    it('Remove', async () => {
        const result = await tb.doRemove({id: id});
        result.should.equal(true);
    });

});