##|
##|  To help with automated testing
globalChartCounter = 0

class ViewDataChart extends View

    getDependencyList: ()=>
        return ["/vendor/canvasjs.min.js"]

    onSetupButtons: () =>
        console.log "ViewDataChart onSetupButtons holder=", @elHolder

    onShowScreen: ()=>
        console.log "ViewDataChart onShowScreen holder=", @elHolder
        @chartOptions = {}
        @axisX  = new DataAxis()
        @axisY  = new DataAxis()
        @chartOptions.backgroundColor = "#EEEEEE"
        @chartOptions.axisX = @axisX.data

        if Array.isArray(@axisY)
            @chartOptions.axisY = []
            for a in @axisY
                @chartOptions.axisY.push a.data
        else
            @chartOptions.axisY = @axisY.data

        @gid = globalChartCounter++
        @elHolder.find(".chartHolder").html("<div id='chart#{@gid}'/>")
        # console.log "Setting chart to ", @elHolder.find(".chartHolder"), " opt=", @chartOptions
        @chart = new CanvasJS.Chart("chart" + @gid, @chartOptions)
        @renderChart()
        true

    addAxisY: ()=>

        if !Array.isArray(@axisY)
            tmp = [ @axisY, new DataAxis() ]
            @axisY = tmp
        else
            @axisY.push new DataAxis()

        return @axisY[@axisY.length-1]

    setTitle: (title)=>
        @chartOptions.title =
            text            : title
            fontSize        : 18
            horizontalAlign : "right"

        if @chart?
            @chart.options.title = @chartOptions.title

        @renderChart()
        return @chartOptions.title

    ##|
    ##|  Add a DataSeries object
    setData: (dataSeries)=>

        if !dataSeries?
            return

        if !@chartOptions?
            return

        if !@chartOptions.data?
            @chartOptions.data = []

        if dataSeries.data.type != "doughnut"
            dataSeries.setIndexThemeColor(@chartOptions.data.length)

        @chartOptions.data.push dataSeries.getData()
        @chart.options.data = @chartOptions.data
        @renderChart()

        return @chartOptions.data

    enableAnimation: (bool = true) =>
        @animationEnabled = bool
        @chartOptions.animationEnabled = @animationEnabled
        @renderChart()
        return bool

    enableZoom: (bool = true) =>
        @zoomEnabled = bool
        @chartOptions.zoomEnabled = @zoomEnabled
        @renderChart()
        return bool

    setAxisX: (title, format, range, options) =>
        if title? and typeof title is "object"
            $.extend @chartOptions.axisX, title
        else
            if title?
                @chartOptions.axisX.title = title
            if format?
                @chartOptions.axisX.valueFormatString = format
            if range? and range.length > 1
                @chartOptions.axisX.minimum = range[0]
                @chartOptions.axisX.maximum = range[1]
            if options? and typeof options is "object"
                $.extend @chartOptions.axisX, options
        $.extend @chart.options.axisX, @chartOptions.axisX
        @renderChart()
        return @chartOptions.axisX

    setAxisY: (title, format, range, options) =>
        if title? and typeof title is "object"
            $.extend @chartOptions.axisY, title
        else
            if title?
                @chartOptions.axisY.title = title
            if format?
                @chartOptions.axisY.valueFormatString = format
            if range? and range.length > 1
                @chartOptions.axisY.minimum = range[0]
                @chartOptions.axisY.maximum = range[1]
            if options? and typeof options is "object"
                $.extend @chartOptions.axisY, options
        $.extend @chart.options.axisY, @chartOptions.axisY
        @renderChart()
        return @chartOptions.axisY

    renderChart: () =>
        if @renderTimer?
            clearTimeout @renderTimer

        @renderTimer = setTimeout () =>
            @chart.render()
        , 50

    onResize : (w, h)=>
        super(w, h)
        if @chart?
            return @onRender()
        return

    onRender: ()=>
        @chart.render()
        true

    ## dummy function for legacy code
    show: (name)=>

    serialize: () =>
        return @chartOptions

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options or typeof options isnt "object"
                reject "ViewDataChart: invalid data is passed"

            if options.title?
                @setTitle options.title

            if options.data?
                @setData options.data

            if options.animationEnabled?
                @enableAnimation options.animationEnabled

            if options.zoomEnabled?
                @enableZoom options.zoomEnabled

            resolve true
