
class ViewFormInput

	formGroupExtra : ""
	fieldSize      : ""
	fieldCss       : ""
	fieldType      : "text"
	fieldGid	   : 0

	##|
	##|  Create the input tag
	createField: (formTag)=>
		@input = $ "<input autocomplete='off' type='#{@fieldType}' id='#{@fieldName}#{@fieldGid}' name='#{@fieldName}#{@fieldGid}' class='form-control #{@fieldCss}' />"
		@input.on "change", (e)=>
			# @startValidation()
			@updateLabel()
			@updatePathValue()
			if @onChange? then @onChange(e)
			true

		@input.on "blur", (e)=>
			@startValidation()
			@setValue @getValue()
			@formGroup.removeClass "form-group-focused"
			true

		@input.on "focus", (e) =>
			if @hintMessage?
				@setHintMsg @hintMessage

			@formGroup.addClass "form-group-focused"

		@input.on "keydown", (e) =>
			if e.keyCode == 9
				## Tab key handles save
				# @setValue @getValue()
				# @updatePathValue()
				return true

			if e.keyCode == 13
				## Enter key, handles save
				@setValue @getValue()
				if !@updatePathValue()
					@formTag.submit()
				return false

			if e.keyCode == 27
				## Esc key, go back to the original value
				# @updateValueFromPath()
				@setValue @value
				return true

	createStatusMessage: ()=>
		@message = $ "<div class='help-block'></div>"

	createLabel: ()=>
		@renderedLabel = @fieldLabel or ""
		if @fieldLabel? and @fieldLabel.includes('{value}')
			@renderedLabel = @fieldLabel.replace /{value}/g, @getValue()
			
		@label = $ "<label for='#{@fieldName}'>#{@renderedLabel}</label>"

	updateLabel: () =>
		if !@fieldLabel? or !@fieldLabel.includes('{value}') then return false
		@renderedLabel = @fieldLabel.replace /{value}/g, @getValue()
		@label.html @renderedLabel
		@renderedLabel 

	createFormGroup: ()=>

		@ninjaFormGroup = $ "<div class='ninja-form-group #{@fieldSize}' />"
		@ninjaFormGroup.append @input

		if @message?
			@ninjaFormGroup.append @message

		if @label?
			@labelGroup = $ "<div class='ninja-form-label' />"
			@labelGroup.append @label

		@formGroup = $ "<div class='form-group #{@formGroupExtra}' />"
		if @labelGroup?
			@formGroup.append @labelGroup

		if !@noInput? or @noInput is false
			@formGroup.append @ninjaFormGroup
		true

	appendFormGroup: ()=>
		@formTag.inputRow[@rowNumber].append @formGroup
		true

	constructor: (@formTag, @fieldName, @fieldLabel, options, @rowNumber)->

		@fieldGid = GlobalValueManager.NextGlobalID()

		##|
		##|  Allow bypassing or changing any form field functions or options
		for varName, value of options
			this[varName] = value
		
		if @rowNumber < 0 then @rowNumber = 0
		if @rowNumber > @formTag.inputRow.length - 1 then @rowNumber = @formTag.inputRow.length - 1

		@createField()
		@createLabel()
		@createStatusMessage()
		@createFormGroup()
		@appendFormGroup()
		@setReadonly(@isReadonly or false)

		true

	##|
	##|  Update the value from the path
	##|  may need to refresh something?
	updateValueFromPath: ()=>
		if !@path then return false
		parts = @path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		fieldName = parts[3]		
		value = DataMap.getDataField tableName, keyValue, fieldName
		@setValue(value, false)
		true

	##
	## Update value in database by form field's value
	updatePathValue: () =>
		if !@path then return false
		DataMap.getDataMap().updatePathValueEvent @path, @getValue()
		true


	getValue: ()=>
		formattedValue = @input.val()
		if @dataFormatter?
			@value = @dataFormatter.unformat(formattedValue)
			# console.log "getValue unformatted=[", formattedValue, "] to unformatted=", @value, " type=", typeof(@value)
		else
			@value = formattedValue
		return @value

	##|
	##|  Set data-options for possible use by formatters
	setOptions: (newOptions)=>

		if !newOptions? then return

		if typeof newOptions == "object"
			newOptions = JSON.stringify(newOptions)

		@options = newOptions

		if @input?
			@input.data("options", newOptions)

		this

	##|
	##|  Set value and return "this" to allow chaining
	setValue: (newValue, skipValidation = false)=>
		if !newValue? then newValue = @defaultValue or ""
		@value = newValue

		## Format input value
		if @inputFormatter?
			@value = @inputFormatter(@value)

		formattedValue = @value
		if @dataFormatter?
			formattedValue = @dataFormatter.format(@value, @options)
		else if typeof newValue != "string"
			formattedValue = newValue.toString()

		@input.val(formattedValue)
		# @input.change()
		@updateLabel()
		# if skipValidation then return this
		if !skipValidation and @value and @value isnt ""
			@startValidation()
		return this

	##|
	##|  Sets a field as read only (static)
	##|  Returns this for chaining
	setReadonly: (@isReadonly = true)=>
		if @isReadonly? and @isReadonly is true
			@formGroup.addClass("ninja-readonly")
		else
			@formGroup.removeClass("ninja-readonly")

		if @input?
			@input.prop("disabled", @isReadonly)

		this

	##|
	##|  @param dataFormatter {string or DataFormatter} Can be the name or object for a data formatter
	##|  see repo EdgeCommonDataSetConfig DataFormatter
	##|
	setDataFormatter: (dataFormatter)=>

		try
			if dataFormatter? and typeof dataFormatter.attachEditor == "function"
				@dataFormatter = dataFormatter
			else
				@dataFormatter = globalDataFormatter.getFormatter(dataFormatter)

			if @input? then @dataFormatter.attachEditor @input, @path
			@setValue @getValue()
		catch e
			console.log "Error setting format for #{dataFormatter}:", e

		return this

	##
	## Set path for this field in database
	setPath: (@path) =>
		true

	##|
	##|  Set the focus and return this object to allow chaining
	setFocus: ()=>
		if @input?
			@input.focus()
		return this

	##|
	##|  Set this input into a warning state
	##|  Returns a value that can be returned from validation function
	setWarningMsg: (message)=>
		@message.html message
		@message.show()
		@formGroup.addClass("has-warning")
		@formGroup.removeClass("has-error")
		return ViewForm.WARNING

	##|
	##|  Set this input into an error state
	##|  Returns a value that can be returned from validation function
	setErrorMsg: (message)=>
		@message.html message
		@message.show()
		@formGroup.removeClass("has-warning")
		@formGroup.addClass("has-error")
		# if @input? and @input.focus?
		# 	@input.focus()
		return ViewForm.ERROR

	##|
	##|  Set this input into an hint state
	##|  Returns a value that can be returned from validation function
	setHintMsg: (@hintMessage = "")=>
		if @message?
			@message.html @hintMessage
			@message.show()
		@formGroup.removeClass("has-warningr")
		@formGroup.removeClass("has-error")
		return ViewForm.HINT		

	##|
	##|  Set this input into a normal state
	##|  Returns a value that can be returned from validation function
	setValid: (message = "")=>
		if @message?
			@message.html message 
			@message.show()
		else
			@message.html ""
			@message.hide()

		@formGroup.removeClass("has-warningr")
		@formGroup.removeClass("has-error")
		return ViewForm.SUCCESS

	##|
	##|  Perform validation on the value
	##|  returns the validation result (see ViewForm.SUCCESS, ERROR or WARNING)
	##|
	startValidation: ()=>
		@getValue()
		@setHintMsg @hintMessage
		result = 0
		if @validateFunction?
			result = @validateFunction(@value, this)

		return result

	##
	## Set a function to format input value: e.g. 1 -> "Yes", 0 -> "No"
	setInputFormatter: (inputFormatter) =>
		if inputFormatter? and typeof inputFormatter is "function"
			@inputFormatter = inputFormatter
		this

	hide: () =>
		if !@formGroup? then return false
		@formGroup.hide()

	show: () =>
		if !@formGroup? then return false
		@formGroup.show()

	##
	## Get ViewForm instance for this field
	getViewFormInstance: () =>
		return @formTag?.form

class ViewFormInputButton extends ViewFormInput

	constructor: (@formTag, @fieldName, @fieldLabel, @buttonText, options)->
		if !@fieldLabel? then @fieldLabel = ""
		if !@buttonText? then @buttonText = "Submit"
		@gid = GlobalValueManager.NextGlobalID()
		# @fieldName = @fieldName + @gid
		##|
		##|  Force the button on a row of it's own
		##|  TODO:  Create a button group in the form and add it
		# @formGroupExtra = "col"
		# @fieldSize      = ""
		super(@formTag, @fieldName, @fieldLabel, options)

	appendFormGroup: ()=>
		@formTag.buttonRow.append @formGroup
		true

	createField: ()=>
		@input = $ "<button name='#{@fieldName}' class='btn ninja-form-button' id='#{@fieldName}#{@gid}' >#{@buttonText}</button>"
		@input.on "click", (e)=>
			@formTag.form.onButton(this, e)

		true

class ViewForm extends View

	boundaryValueToFullWidth: 1000
	boundaryValueToInlineForm: 300

	@SUCCESS 	: 1
	@WARNING 	: 2
	@ERROR		: 3
	@HINT       : 4

	getDependencyList: ()=>
		return [
			"/vendor/selectize.js", "/vendor/selectize.css", 
			"/vendor/summernote/summernote.min.js", "/vendor/summernote/summernote.css", 
			"/vendor/jquery.multi-select.js", "/vendor/multi-select.css",
			"/vendor/bootstrap-toggle.min.js", "/vendor/bootstrap-toggle.min.css"
		]

	##|
	##|  Called when the form is first shown
	onShowScreen: ()=>
		GlobalClassTools.addEventManager(this)

	##|
	##|  Read all the values and save them as variables
	getValues: ()=>

		validation = 0

		for item in @fields
			name = item.fieldName
			item.getValue()
			this[name] = item

			result = item.startValidation()
			if result > validation then validation = result

		if validation == ViewForm.ERROR
			return false

		true

	onSubmit: ()=>
		try
			if @getValues() == false
				return false

			if @onSubmitCallback?
				result = @onSubmitCallback(this)
				if result == true or result == ViewForm.SUCCESS or !result?
					@emitEvent "close", [this]
				return false

			@emitEvent "close", [this]

		catch e
			console.log "Form Submit Error: ", e
		
		return false

	onButton: (button, e)=>
		result = @getValues()
		if button.onClick? 
			button.onClick(this)
		
		if button.fieldName is "submitButton"
			return true
		false

	##|
	##|  Initialize the options array for this form
	##|  extend the values from localOptions (optional) into the class options.
	##|
	initOptions: (localOptions)=>

		if !@options?
			@options =
				formClass	: "ninja-form"
				isInline	: false
				isFullWidth	: false

		if !@fields?
			@fields = []

		if localOptions?
			for varName, value of localOptions
				if @options[varName]? then @options[varName] = value


		##|
		##|  Create the form holder
		@gid = "form" + GlobalValueManager.NextGlobalID()
		@wgtForm = $ "<form id='form#{@gid}' class='#{@options['formClass']}' />"

		##|
		##|  Looks like this
		##|  <form ninja-form form-horizontal>
		##|  <div class='row'>
		##|    ... content header	
		##|  <div class='row'>
		##|    ... all inputs
		##|  <div class='row'>
		##|    ... all buttons
		##|

		@wgtForm.textRow   = $ "<div class='row ninja-form-text-row'></div>"
		@wgtForm.body      = $ "<div class='ninja-form-body'></div>"
		@wgtForm.inputRow  = []	
		@wgtForm.inputRow.push $("<div class='row ninja-form-inputs-row' id='ninja-input-row-#{@wgtForm.inputRow.length}''></div>")
		@wgtForm.buttonRow = $ "<div class='row ninja-form-buttons-row'></div>"
		@wgtForm.append @wgtForm.textRow
		@wgtForm.textRow.hide()
		@wgtForm.body.append @wgtForm.inputRow[0]
		@wgtForm.append @wgtForm.body
		@wgtForm.append @wgtForm.buttonRow

		@wgtForm.form = this
		@wgtForm.form.fields = @fields
		@wgtForm.on "submit", @onSubmit

		@wgtContainer = $ "<div class='block-content' />"
		@wgtContainer.append @wgtForm

		@elFormContainer.append @wgtContainer

		true

	clearForm: ()=>
		@wgtForm.textRow.html ""
		@wgtForm.textRow.hide()
		@wgtForm.body.html ""
		@wgtForm.inputRow  = []	
		@wgtForm.inputRow.push $("<div class='row ninja-form-inputs-row' id='ninja-input-row-#{@wgtForm.inputRow.length}''></div>")
		@wgtForm.body.append @wgtForm.inputRow[0]
		@wgtForm.buttonRow.html ""
		true

	## ----------------------------------------------------------------
	## Initialization function
	## Create instance of FormWrapper which would present form
	##
	setData: (localOptions)=>
		@initOptions(localOptions)

	setSubmitFunction: (@onSubmitCallback)=>

	##|
	##|  If true, don't resize the form to flow
	setNoResize: ()=>
		@boundaryValueToFullWidth = 1000000
		@boundaryValueToInlineForm = -1000000
		@wgtForm.removeClass("wide")
		@wgtForm.removeClass("short")

	##
	## Set content header of the form
	setContentHeader: (strHtml) =>
		if !strHtml? or strHtml is ""
			return @wgtForm.textRow.hide()
		@contentheader = strHtml
		@wgtForm.textRow.html strHtml
		return @wgtForm.textRow.show()

	## -----------------------------------------------------------------
	## Function to regulate "responsiveness" of form elements while resizing
	## When width of form gets shorter than boundary value, elements get wider
	## so that they have full width
	## Also when width of form gets longer than boundary value, elements get narrower
	##
	onResize : (w, h)=>
		if w == 0 or h == 0 then return

		# if @wgtForm?
		# 	if w > @boundaryValueToFullWidth
		# 		@wgtForm.addClass("wide")
		# 	else
		# 		@wgtForm.removeClass("wide")

		# 	if h < @boundaryValueToInlineForm
		# 		@wgtForm.addClass("short")
		# 	else
		# 		@wgtForm.removeClass("short")
	##|
	##|  This will bind the form to a specified table in the database.
	##|  If a column doesn't exist in the datamap it will be created only
	##|  after the first values are saved.
	##|
	setPath: (tableName, idValue) =>

		@bindTableName = tableName
		@bindValue     = idValue

		if !@bindSetup

			##|
			##|  Set the formatter on all fields the first time
			for idx, col of DataMap.getTypes(tableName).col
				for field in @fields
					if field.fieldName != col.getSource() then continue

					formatter  = col.getFormatter()
					field.setDataFormatter formatter
					# console.log "Setting path: ", field.fieldName, col.getSource(), field.fieldName is col.getSource()
					field.setPath "/#{tableName}/#{idValue}/#{col.getSource()}"

			@bindSetup = true

		@updatePathValues()
		##
		## Get synced by table's new data events
		window.addEventListener "new_data", @onTableNewData, false
		true

	onTableNewData: (e) =>
		if !e? or e.detail.tablename == @bindTableName

			if @resetTimer?
				clearTimeout(@resetTimer)

			@resetTimer = setTimeout ()=>
				delete @resetTimer
				@updatePathValues()
			, 50
		true

	##|
	##|  For Forms that are bound to a table and path,  this will loop through
	##|  all form fields and update the values if the value have changed.   It will
	##|  also revalidate the fields.
	##|
	updatePathValues: ()=>
		##|
		##|  Loop through all form fields, find ones that match the names
		for field in @fields
			field.setPath "/#{@bindTableName}/#{@bindValue}/#{field.fieldName}"
			field.updateValueFromPath()

		# for idx, col of DataMap.getTypes(@bindTableName).col
		# 	for field in @fields
		# 		if field.fieldName != col.getSource() then continue
		# 		field.setPath "/#{@bindTableName}/#{@bindValue}/#{field.fieldName}"
		# 		field.updateValueFromPath()

		true

	addSubmit: (buttonName, labelText, buttonText, options)=>

		field = new ViewFormInputButton(@wgtForm, "submitButton", labelText, buttonText, options)
		@fields.push field

	##
	## Add a button 
	##
	addButton: (buttonName, labelText, buttonText, options)=>

		field = new ViewFormInputButton(@wgtForm, buttonName or "button-#{@fields.length}", labelText, buttonText, options)
		@fields.push field

	## -------------------------------------------------------------------------------------------------------------
	## Add a general input field
	##
	## @param [String] fieldName name of the input field
	## @param [String] label label to be displayed infornt of text input
	## @param [String] value default value to be filled
	## @param [String] type type of the input it can be any valid type attribute value default is text
	## @param [Object] attrs object as attributes that will be included in the html
	## @param [Function] fnValidate a validation function can be passed if it returns true value will be valid else invalid
	##
	addInput: (fieldName, label, options, rowNumber) =>
		@buildArrangement()
		if !rowNumber? then rowNumber = @wgtForm.inputRow.length - 1
		field = new ViewFormInput(@wgtForm, fieldName, label, options, rowNumber)
		if @fields.length == 0 then field.setFocus()
		@fields.push(field)
		return field

		# type = if type is "boolean" then "checkbox" else type
		# if type is "checkbox" and value is 1
			# attrs.checked = "checked"
		# value = if type is "checkbox" then 1 else value

	##
	## Add a row wrapping inputs in it to the form's body
	##
	addRow: (position) =>
		if !position? then position = @wgtForm.inputRow.length
		row = $("<div class='row ninja-form-inputs-row' id='ninja-input-row-#{@wgtForm.inputRow.length}'></div>")
		@wgtForm.inputRow.splice position, 0, row
		@wgtForm.body.append row
		return row

	##
	## Automatically adds rows by @layout
	buildArrangement: () =>
		if !@layout? then return false
		inputTotal   = @fields?.length or 0
		inputCurrent = 0
		if @layout.rows?
			for rowLayout, idx in @layout.rows
				inputCurrent += (rowLayout.inputCount or layout.defaultInputCount)
				if inputCurrent is inputTotal
					return @addRow()

		defaultInputCount = @layout.defaultInputCount
		if defaultInputCount? && defaultInputCount > 0 && inputTotal > inputCurrent && (inputTotal - inputCurrent) % defaultInputCount is 0
			@addRow() 

		true

	setArrangement: (obj) =>
		@layout = $.extend {}, obj

	getArrangement: () =>
		@layout

	##
	## Get an input field by its name
	getFieldByName: (name) =>
		if !name? or name is "" then return false
		for field in @fields
			if field.fieldName is name
				return field
		null

	##|
	##|  Create a new form based on the fields in a table config
	##|  isCreateForm - true if the form is for creating a record,
	##|  false if you are creating an edit form.
	##|
	createFromTable: (tableName, isCreateForm, onModal = false)=>

		##|
		##| TODO:  getTypes() / col doesn't return columns in sorted order
		##|        so we need to sort this list before we create the fields.
		##|

		@clearForm()
		@bindTableName     = tableName
		@bindSetup         = true
		@createdFromTable  = true
		@renderedFromTable = false
		@editedFromTable   = !isCreateForm

		colNames = Object.keys(DataMap.getTypes(tableName).col)
		colNamesSorts = colNames.sort (a, b)=>
			if DataMap.getTypes(tableName).col[a].getOrder() < DataMap.getTypes(tableName).col[b].getOrder()
				return -1
			return 1

		for idx in colNamesSorts
			col = DataMap.getTypes(tableName).col[idx]

			if isCreateForm and not col.getVisibleOnNew() then continue
			if not isCreateForm and not col.getVisibleOnEdit() then continue

			options = {}
			field = @addInput col.getSource(), col.getName(), options
			field.setOptions(col.getOptions())
			field.setDataFormatter(col.getFormatter())

			if !isCreateForm and !col.getEditable()
				field.setReadonly(true)
		if onModal is false
			if isCreateForm
				@addSubmit "submit", "", "Create"
			else
				@addSubmit "submit", "", "Save Changes"

		true

	##
	##  Create a form based on fields in a table config
	##  validFields[Array]: contains field names to render, if null or undefined, render all fields
	##
	renderFromTable: (tableName, validFields, onModal = false) =>
		@clearForm()
		@bindTableName     = tableName
		@bindSetup         = true
		@createdFromTable  = false
		@renderedFromTable = true
		@editedFromTable   = false

		columns  = DataMap.getTypes(tableName).col
		colNames = Object.keys(columns)
		colNamesSorts = colNames.sort (a, b)=>
			if columns[a].getOrder() < columns[b].getOrder()
				return -1
			return 1

		if !validFields?
			validFields = colNamesSorts
		else if typeof validFields is "string"
			validFields = [ validFields ]
		else if validFields instanceof RegExp is true
			re = validFields
			validFields = colNames.filter (colName) ->
				return re.test(colName)

		##
		## make a new column's name from its source name
		makeColName = (sourceName) ->
			if !sourceName? or !sourceName.replace? then return null
			parts      = sourceName.replace(/_/g, " ").split(" ")
			camelParts = parts.map (part) ->
				return part.charAt(0).toUpperCase() + part.substr(1, part.length - 1)
			camelParts.join(" ")

		for fname in validFields
			position = colNamesSorts.indexOf(fname)
			if position >= 0
				col = columns[fname]

				options = {}
				if col.getRequired() 
					options.validateFunction = (val) ->
						if val is ""
							return @setErrorMsg "This field is required."

				field = @addInput col.getSource(), col.getName(), options
				field.setOptions(col.getOptions())
				field.setDataFormatter(col.getFormatter())

				if !col.getEditable()
					field.setReadonly(true)

			else 
				colName = makeColName field
				if !colName then continue
				options = 
					name   : colName
					source : field

				newColumn = DataMap.addColumn tableName, options			
				field = @addInput newColumn.getSource(), newColumn.getName()
				field.setDataFormatter(newColumn.getFormatter())

				# console.log "New column is created: ", tableName, options, newColumn

		if onModal is false
			@addSubmit "submit", "", "Save Changes"
		true

	##
	## Set values of form fields from an Object
	setValuesFromObject: (obj) =>
		if !obj? or typeof obj isnt "object" then return false
		for field in @fields when obj[field.fieldName]?
			field.setValue obj[field.fieldName]
		true

	##
	## Add custom styles to element
	addCustomStyles: (el, classes) =>
		if !el? or !classes? then return false
		for cls in classes
			el.addClass cls 

	##
	## Add custom styles to all input fields
	addCustomStylesToInputs: (classes) =>
		if !classes? then return false
		@inputcustomstyles = classes
		for field in @fields when field.constructor.name is "ViewFormInput"
			@addCustomStyles field.input, classes

		true

	##
	## Add custom styles to all input fields
	addCustomStylesToButtons: (classes) =>
		if !classes? then return false
		@buttoncustomstyles = classes
		for field in @fields when field.constructor.name is "ViewFormInputButton"
			@addCustomStyles field.input, classes

		true

	setBindSetup: (@bindSetup) =>

	serialize: () =>
		rows    = null
		inputs  = null
		buttons = []
		if @wgtForm.inputRow.length > 1
			rows = new Array(@wgtForm.inputRow.length)
		else
			inputs = []

		for field in @fields
			if field.constructor.name is "ViewFormInput"
				input =
					name: field.fieldName
					label: field.fieldLabel
					options: field.options
					value: field.value
					isReadonly: field.isReadonly
					type: field.dataFormatter?.name
					path: field.path
					hintMessage: field.hintMessage
				$.extend input, field.options
				if rows? and rows.length
					if !rows[field.rowNumber]?
						rows[field.rowNumber] = { inputs: [] }
					rows[field.rowNumber].inputs.push input
				else
					inputs.push input

			else if field.constructor.name is "ViewFormInputButton"
				button =
					name: field.fieldName
					text: field.buttonText
					label: field.fieldLabel
				$.extend button, field.options
				buttons.push button

		return
			arrangement:        @layout
			onsubmit:           @onSubmitCallback
			inputcustomstyles:  @inputcustomstyles
			buttoncustomstyles: @buttoncustomstyles
			contentheader:      @contentheader
			bindsetup:          @bindSetup
			create_from_table: (@createdFromTable and @bindTableName)
			edit_from_table:   (@editedFromTable and @bindTableName)
			render_from_table: (@renderedFromTable and @bindTableName)
			table:             @bindTableName
			idvalue:           @bindValue
			rows:              rows 
			inputs:            inputs
			buttons:           buttons


	##|
	##|  Called from getLayout() and can quickly setup the form
	##|  see test page forms.coffee
	deserialize: (options)=>
		addInputs = (inputs, rowNumber) =>
			for input in inputs
				fieldName = input.name
				fieldLabel = input.label
				delete input.name
				delete input.label
				inputControl = @addInput fieldName, fieldLabel, input, rowNumber

				if input.options?
					inputControl.setOptions(input.options)

				if input.value?
					inputControl.setValue(input.value)

				if input.type? and input.type != ""
					inputControl.setDataFormatter(input.type)
					if input.value?
						inputControl.setValue(input.value)

				if input.hintMessage and input.hintMessage != ""
					inputControl.setHintMsg input.hintMessage

				if input.hidden? and input.hidden is true
					inputControl.hide()

				if input.path?
					inputControl.setPath input.path

				if input.focus? and !(!input.focus)
					inputControl.setFocus input.focus

		new Promise (resolve, reject)=>

			if options.arrangement? and typeof options.arrangement is "object"
				@setArrangement options.arrangement

			if options.rows? and Array.isArray(options.rows)
				for row, index in options.rows
					position = row.position or index
					if index > @wgtForm.inputRow.length - 1
						@addRow position
					if row.inputs? and Array.isArray(row.inputs)
						addInputs row.inputs, position

			if options.inputs? and Array.isArray(options.inputs)
				addInputs options.inputs

			if options.buttons? and Array.isArray(options.buttons)
				for button in options.buttons
					if button.type == "submit"
						labelText = button.label
						buttonText = button.text
						delete button.label
						delete button.text						
						@addSubmit "submit", labelText, buttonText, button
					else
						labelText = button.label
						buttonText = button.text
						delete button.label
						delete button.text
						@addButton button.name, labelText, buttonText, button

			if options.onSubmit? and typeof options.onSubmit == "function"
				@setSubmitFunction(options.onSubmit)

			if options.onsubmit? and typeof options.onsubmit == "function"
				@setSubmitFunction(options.onsubmit)

			if options.inputcustomstyles? 
				@addCustomStylesToInputs options.inputcustomstyles

			if options.buttoncustomstyles?
				@addCustomStylesToButtons options.buttoncustomstyles

			if options.contentheader?
				@setContentHeader options.contentheader

			if options.bindsetup?
				@setBindSetup options.bindsetup

			if options.create_from_table?
				@createFromTable(options.create_from_table, true)
				if options.idvalue?
					@setPath options.create_from_table, options.idvalue
			else if options.edit_from_table?
				@createFromTable(options.edit_from_table, false)
				if options.idvalue?
					@setPath options.edit_from_table, options.idvalue
			else if options.render_from_table?
				@renderFromTable(options.render_from_table, options.valid_fields)
				if options.idvalue?
					@setPath options.render_from_table, options.idvalue
			else if options.table? and options.idvalue?
				@setPath options.table, options.idvalue

			if options.values_from_object?
				@setValuesFromObject options.values_from_object

			resolve(true)
