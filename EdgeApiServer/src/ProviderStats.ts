import * as url from 'url';

import EdgeTimeseriesData, { TimeSeriesDatabase } from 'edgecommontimeseriesdata';
import config                 from 'edgecommonconfig';
import EdgeRequest            from 'edgecommonrequest';

import ApiProvider from './ApiProvider';

export default class ProviderStats extends ApiProvider {
    timeseriesData: TimeSeriesDatabase;
    statDatabases: { [dbName: string]: TimeSeriesDatabase };

    constructor(prototype: ApiProvider) {
        super(prototype);

    }

    onInit() {
        // init timeseries data, create database if it doesn't exist
        let timeseriesData          = EdgeTimeseriesData.getInstance();
        timeseriesData.doCreateDatabase('general').then((db: any) => {
            if (db != null && db !== false) {
                return this.timeseriesData = db;
            } else {
                return config.status('ProviderStats onInit: No time series database.   Unable to initialize.', db);
            }
        });

        return true;
    }

    doWriteStatisticalData(databaseName: string, categoryName: string, dataTags: any, dataFields: any) {

        return new Promise(async (resolve) => {

            if (this.statDatabases == null) {
                this.statDatabases = {};
            }

            if (this.statDatabases[databaseName] == null) {

                let timeseriesData               = EdgeTimeseriesData.getInstance();
                this.statDatabases[databaseName] = await timeseriesData.doCreateDatabase(databaseName);
            }

            if (this.statDatabases[databaseName] != null && this.statDatabases[databaseName]) {
                let result = await this.statDatabases[databaseName].doSaveMeasurement(categoryName, dataTags, dataFields);
                resolve(result);
            }

            resolve(null);
        });
    }

    // Connect to RabbitMQ Using the management API and return Queue status
    doGetMessageQueueStats() {

        let mqHost   = config.getCredentials('mqAdmin');
        let urlParts = url.parse(mqHost);

        let { auth } = urlParts;
        let host     = urlParts.hostname;
        let port     = urlParts.port || 15672;

        let dataUrl = `http://${ auth }@${ host }:${ port }/api/queues`;
        let qs      = {};

        let r = new EdgeRequest();
        return r.doGetLink(dataUrl, qs);
    }

    getBaseName() {
        return 'stats';
    }

    doGetStats(intervalName: string, categoryName: string): void {
        return null;
    }

    // Add many factors to the stats
    doAddStatsMany(intervalName: string, categoryName: string, keys: string[]) {

        for (let keyName in keys) {
            let keyValue = keys[keyName];
            this.doAddStats(categoryName, keyName, keyValue);
        }

        return true;
    }

    // Add a single record to stats
    async doAddStats(intervalName: string, categoryName: string, counterName: string, count: number = 1) {

        config.status(`doAddStats intervalName=${ intervalName }, category=${ categoryName }, counter=${ counterName }, count=${ count }`);

        // intervalName is usually "today" or something similar to make sure we're
        // making it easier for mongo but EdgeCommonTimeSeries with Influx is better
        // and doesn't care about that so we can ignore the interval.

        // tags are indexed
        let tags = { key: counterName };

        // no fields of data
        let fields = { num_proc: count || 1 };

        if (this.timeseriesData == null) {
            config.status('ProviderStats doAddStats not connected');
            console.log('ProviderStats doAddStats not connected');
            return true;
        }
        // TODO ROY: doWriteMeasurement is not defined on timeseries
        return this.timeseriesData.doSaveMeasurement(categoryName, tags, fields);
        // return this.timeseriesData.doWriteMeasurement(categoryName, counterName, counterName, fields);
    }
};
