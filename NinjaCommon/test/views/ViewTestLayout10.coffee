class ViewTestLayout10 extends View

    getLayout: ()=>
        view: "DockedToolbar"
        items: [
            text: "New"
        ,
            type: "button"
            text: "Save"
            icon: "fa fa-save"
            callback: "eventTest1"
        ,
            text: "List"
            icon: "fa fa-list"
            callback: @onTestCallback
        ,
            type: "toggle"
            text: "Toggle"
            checked: true
            store_name: "toggle_button"
            initial_callback: true
            callback: @onTestCallbackToggle
        ]
        main:
            view: "TestShowSize"
            name: "viewTest"

    onTestCallback: ()=>
        console.log "TEST CALLBACK"
    
    onTestCallbackToggle: (e, isChecked) =>
        console.log "Test callback on Toggle button"
        console.log "Event: ", e 
        console.log "Checked value: ", isChecked

    onShowScreen: ()=>

        console.log "Automatically Created Toolbar:", @viewToolbar
        console.log "Specific test view:", @viewTest

        @viewToolbar.on "eventTest1", ()=>
            console.log "Callback from event on save button"

        true