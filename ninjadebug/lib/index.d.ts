export declare class Ninja {
    static debugNumberFormat: string;
    static debugDateFormat: string;
    static dumpBoolean(value: any): string;
    static dumpNull(): string;
    static dumpString(value: any): string;
    static dump(title: any, items?: any): void;
    static dumpDate(value: any): string;
    static dumpNumber(value: any): string;
    static dumpObject(items: any, indent: any): string;
    static dumpPercent(value?: any): string;
    static dumpVar(value: any, indent: any): string;
    static log(items: any): void;
    static pad(value: any, maxSpace: number, colorFunction?: any, customFormat?: string): string;
}
export default Ninja;
