$ ->
    pdfData = atob(
        'JVBERi0xLjcKCjEgMCBvYmogICUgZW50cnkgcG9pbnQKPDwKICAvVHlwZSAvQ2F0YWxvZwog' +
        'IC9QYWdlcyAyIDAgUgo+PgplbmRvYmoKCjIgMCBvYmoKPDwKICAvVHlwZSAvUGFnZXMKICAv' +
        'TWVkaWFCb3ggWyAwIDAgMjAwIDIwMCBdCiAgL0NvdW50IDEKICAvS2lkcyBbIDMgMCBSIF0K' +
        'Pj4KZW5kb2JqCgozIDAgb2JqCjw8CiAgL1R5cGUgL1BhZ2UKICAvUGFyZW50IDIgMCBSCiAg' +
        'L1Jlc291cmNlcyA8PAogICAgL0ZvbnQgPDwKICAgICAgL0YxIDQgMCBSIAogICAgPj4KICA+' +
        'PgogIC9Db250ZW50cyA1IDAgUgo+PgplbmRvYmoKCjQgMCBvYmoKPDwKICAvVHlwZSAvRm9u' +
        'dAogIC9TdWJ0eXBlIC9UeXBlMQogIC9CYXNlRm9udCAvVGltZXMtUm9tYW4KPj4KZW5kb2Jq' +
        'Cgo1IDAgb2JqICAlIHBhZ2UgY29udGVudAo8PAogIC9MZW5ndGggNDQKPj4Kc3RyZWFtCkJU' +
        'CjcwIDUwIFRECi9GMSAxMiBUZgooSGVsbG8sIHdvcmxkISkgVGoKRVQKZW5kc3RyZWFtCmVu' +
        'ZG9iagoKeHJlZgowIDYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMDEwIDAwMDAwIG4g' +
        'CjAwMDAwMDAwNzkgMDAwMDAgbiAKMDAwMDAwMDE3MyAwMDAwMCBuIAowMDAwMDAwMzAxIDAw' +
        'MDAwIG4gCjAwMDAwMDAzODAgMDAwMDAgbiAKdHJhaWxlcgo8PAogIC9TaXplIDYKICAvUm9v' +
        'dCAxIDAgUgo+PgpzdGFydHhyZWYKNDkyCiUlRU9G')

    url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf'

    addTestButton "Basic PDF Viewer", "Open", ()->
        addHolder().setView "DocumentPDF", (view)->
            true
        true

    addTestButton "Load Multi Page PDF", "Open", ()->
        addHolder().setView "DocumentPDF", (view)->
            view.setPDFSource url
            true
        true

    addTestButton "Load PDF Data", "Open", ()->
        addHolder().setView "DocumentPDF", (view)->
            view.setPDFSource
                data: pdfData
            true
        true

    addTestButton "PDF Viewer on Popup", "Open", ()->
        doPopupView "DocumentPDF", "popup-document-pdf", null, 800, 600, (view) ->
            view.setPDFSource url
            true
        true

    addTestButton "PDF Viewer Serialize/deserialize", "Open", ()->
        addHolder().setView "DocumentPDF", (view)->
            view.setPDFSource url
            view.setScale 1.2

            setTimeout () ->
                options = view.serialize()
                console.log "Serialized data: ", options
                doPopupView "DocumentPDF", "popup-document-pdf-deserialized", null, 800, 600, (view) ->
                    view.deserialize options
            , 5000

        true

    go()
