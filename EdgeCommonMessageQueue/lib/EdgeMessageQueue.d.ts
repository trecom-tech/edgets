declare class EdgeAQMPMessageQueue {
    mqChannel: any;
    messageQueue: any;
    queueName: string;
    drainRequired: boolean;
    messageExchange: any;
    drainWait: any[];
    defaultPriority: number;
    constructor(mqChannel: any, messageQueue: any, queueName: string);
    consume(prefetchNum: number, callback: any): void;
    doClose(): Promise<boolean>;
    doWaitForDrain(): Promise<{}>;
    doPublish(routingKey: string, str: any): Promise<{}>;
    doSubmitBuffer(str: any, messagePriority: any): Promise<{}>;
}
declare class EdgeMessageQueueController {
    log: any;
    openPromise: any;
    conn: any;
    constructor();
    doConnect(): any;
    doOpenSubscription(queueName: string, callback: any): Promise<void>;
    doOpenMessageQueue(queueName: string, options?: any): Promise<EdgeAQMPMessageQueue>;
    doOpenExchangeForWrite(queueName: string, exchangeName: string): Promise<EdgeAQMPMessageQueue>;
}
export declare const EdgeAMQPController: EdgeMessageQueueController;
export default EdgeAMQPController;
