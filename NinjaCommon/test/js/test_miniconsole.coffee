$ ->

    addTestButton "Simple ConsoleOutput", "Open", ()->

        addHolder()
        .setView "ConsoleOutput", (view)->
            view.addOutput "Testing"

    ##|
    ##|  Console should output time delayed information
    ##|  reporting on the time automatically
    addTestButton "Test output over time", "Open", ()->

        addHolder()
        .setView "ConsoleOutput", (view)->
            for x in [0..5]
                setTimeout (x)=>
                    view.addOutput "Testing line #{x}"
                , x*500, x

    ##|
    ##|  Console should output time delayed information
    ##|  reporting on the time automatically, this uses
    ##|  the specific call format to addOutput
    ##|
    addTestButton "Test output with types", "Open", ()->

        addHolder()
        .setView "ConsoleOutput", (view)->
            for x in [0..5]
                setTimeout (x)=>
                    obj =
                        type:    "c"
                        values:  ["Testing line text", x]
                        time_ms: 100*x

                    view.addOutput obj
                , x*500, x

    ##|
    ##|  Test the end run output with internal timer
    ##|
    addTestButton "End run after 5 seconds", "Open", ()->

        addHolder()
        .setView "ConsoleOutput", (view)->
            for x in [0..20]
                setTimeout (x)=>
                    if x == 20
                        view.endRun()
                    else
                        obj =
                            type:    "c"
                            values:  ["Testing line text", x]
                        view.addOutput obj
                , x*250, x

    ##|
    ##|  Test the end run output with external data
    ##|
    addTestButton "End run with result", "Open", ()->
        addHolder()
        .setView "ConsoleOutput", (view)->
            for x in [0..20]
                setTimeout (x)=>
                    if x == 20
                        obj =
                            total_ms: 100*x
                            result: true
                        view.endRun obj
                    else
                        obj =
                            type:    "c"
                            values:  ["Testing line text", x]
                            total_ms: 100*x
                        view.addOutput obj
                , x*250, x

    addTestButton "ConsoleOutput in popup", "Open", ()->
        doPopupView 'ConsoleOutput', 'Console Output', 'console_output', 1000, 800, (view)->
            for x in [0..20]
                setTimeout (x)=>
                    if x == 20
                        obj =
                            total_ms: 100*x
                            result: true
                        view.endRun obj
                    else
                        obj =
                            type:    "c"
                            values:  ["Testing line text", x]
                            total_ms: 100*x
                        view.addOutput obj
                , x*250, x

    addTestButton "Popup ConsoleOutput", "Open", ()->
        doPopupViewOnce "ConsoleOutput", "Test window title", "dummySettings", 800, 400, "Console Output", (view, tabText)->
            for x in [0..20]
                setTimeout (x)=>
                    if x == 20
                        obj =
                            total_ms: 100*x
                            result: true
                        view.endRun obj
                    else
                        obj =
                            type:    "c"
                            values:  ["Testing line text", x]
                            total_ms: 100*x
                        view.addOutput obj
                , x*250, x

    ##|
    ##|  Test the end run output with internal timer
    ##|
    addTestButton "Delayed Actions", "Open", ()->

        addHolder()
        .setView "ConsoleOutput", (view)->
            view.addOutput "Simple line of text"
            output = view.startOutput "This is a long action"
            setTimeout ()=>
                console.log "Here, output=", output
                output.finish "Task is complete after 5000 ms timeout"
            , 5000

    go()