TableTestdata = []
TableTestdata.push
    name        : "ID"
    source      : "id"
    editable    : false
    required    : true
TableTestdata.push
    name        : "InitialPrice"
    source      : "initialPrice"
    options     :
    	type     : "money"
    	default  : 1000
    	max      : 3000
    	min      : 100
    editable    : false
TableTestdata.push
    name        : "CurrentPrice"
    source      : "currentPrice"
    editable    : true
TableTestdata.push
    name        : "Date"
    source      : "date"
    options     : "Date options"
    editable    : true
TableTestdata.push
    name        : "Distance"
    source      : "distance"
    options     : "Very close, Close, Normal, Far, Very far"
    editable    : true
TableTestdata.push
    name        : "IsNew"
    source      : "isNew"
    editable    : true
TableTestdata.push
    name        : "ImageList"
    source      : "imagelist"
    options     : ["Small", "Medium", "Large", "Extra Large"]
    editable    : true
$ ->

##|
##|  This is just for diagnostics,  you don't need to verify the data map is
##|  loaded normally.  The data types should be loaded upon startup.
	addTest "Confirm Zipcodes datatype loaded", () ->
		dm = DataMap.getDataMap()
		if !dm? then return false

		zipcodes = dm.types["zipcode"]
		if !zipcodes? then return false
		if !zipcodes.col["code"]? then return false

		true

	##|
	##|  Load the zipcodes JSON file.
	##|  This will insert the zipcodes into the global data map.
	addTest "Loading Zipcodes", () ->

		new Promise (resolve, reject) ->
			ds  = new DataSet "zipcode"
			ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
			ds.doLoadData()
			.then (dsObject)->
				resolve(true)
			.catch (e) ->
				console.log "Error loading zipcode data: ", e
				resolve(false)

	addTest "Loading Test data", () ->

        loadDatafromJSONFile "testData"
        .then ()->
            DataMap.setDataTypes 'testData', TableTestdata
            return true

        true

	addTestButton "Show Table Editor", "Open", ()->

		tableName = "zipcode"
		doPopupView "ShowTableEditor", "Editing table: #{tableName}", "tableditor", 1300, 800, (view)->
			view.deserialize { table: tableName }
		true

	addTestButton "Tabs in Tabs with Table", "Open", ()->
		addHolder().setView "DynamicTabs", (viewTabs1)->

			newPromise ()->

				yield loadZipcodes()
				yield loadStockData()
				yield loadDatafromJSONFile("testData")
				yield loadDatafromJSONFile("SaveZipcodeData")

				DataMap.changeColumnAttribute "testData", "imagelist", "editable", true

			.then ()->

				viewTabs1.doAddViewTab "DynamicTabs", "Top Tab 1", (viewTabs1A)->
					viewTabs1A.doAddTableTab "zipcode", "Zipcodes", null, (table) ->
						table.setTitle "Zipcode Table"
						table.setSavedSearch "zipcode_saved_search"
					viewTabs1A.doAddTableTab "testData", "testData", null, (table) ->
						table.setTitle "Test Data Table"
						table.setSavedSearch "testtable_saved_search"
						setTimeout () ->
							DataMap.addData "testData", 123456,
								"id": "123456"
								"currentPrice": "10000"
						, 3000

				viewTabs1.doAddViewTab "DynamicTabs", "Top Tab 2", (viewTabs1B)->
					viewTabs1B.doAddTableTab "zipcode", "Zipcodes2"
					viewTabs1B.doAddTableTab "SaveZipcodeData", "SaveZipcodeData", null, (table) ->
						table.setTitle "Saved Zipcode Data Table"
						table.setSavedSearch "szdt_saved_search"

					# console.log "HERE Adding TOp2", viewTabs1B
					# viewTabs1B.doAddTableTab "SaveZipcodeData", "SaveZipcodeData"

					# viewTabs1B.doAddTableTab "testData", "TestData"
					# .then (viewTable)->
					# 	viewTable.table.moveActionColumn "distance"

		return

	addTestButton "TabsLeft in TabsLeft with Table", "Open", ()->
		addHolder().setView "DynamicTabsLeft", (viewTabs1)->

			newPromise ()->

				yield loadZipcodes()
				yield loadStockData()
				yield loadDatafromJSONFile("testData")
				yield loadDatafromJSONFile("SaveZipcodeData")

				DataMap.changeColumnAttribute "testData", "imagelist", "editable", true

			.then ()->

				viewTabs1.doAddViewTab "DynamicTabsLeft", "Top Tab 1", (viewTabs1A)->
					viewTabs1A.doAddTableTab "zipcode", "Zipcodes", null, (table) ->
						table.setTitle "Zipcode Table"
						table.setSavedSearch "zipcode_saved_search"
					viewTabs1A.doAddTableTab "testData", "testData", null, (table) ->
						table.setTitle "Test Data Table"
						table.setSavedSearch "testtable_saved_search"
						setTimeout () ->
							DataMap.addData "testData", 123456,
								"id": "123456"
								"currentPrice": "10000"
						, 3000

				viewTabs1.doAddViewTab "DynamicTabsLeft", "Top Tab 2", (viewTabs1B)->
					viewTabs1B.doAddTableTab "zipcode", "Zipcodes2"
					viewTabs1B.doAddTableTab "SaveZipcodeData", "SaveZipcodeData", null, (table) ->
						table.setTitle "Saved Zipcode Data Table"
						table.setSavedSearch "szdt_saved_search"

		return		


	addTestButton "simpleobject data type test", "Open", ()->

		##| set the address as object in data map, to manipulate address field as simple object
		for key,obj of DataMap.getDataMap().engine.export("zipcode")
			obj.address = {city:obj.city,state:obj.state,county:obj.county}

		## -gao
		## is "options.compile" needed or used?
		DataMap.setDataTypes "zipcode", [
			name    : "Address"
			source  : "address"
			visible : true
			type    : "simpleobject"
			width   : 200,
			options:
				compile: "{{city}}, {{state}}, {{county}}"
		]

		addHolder().setView "Table", (table)->
			table.addTable "zipcode"

	addTestButton "simple array data type test", "Open", ()->

		##| set the address as object in data map, to manipulate address field as simple object
		for key,obj of DataMap.getDataMap().engine.export("zipcode")
			obj.addressArray = [{city:obj.city,state:obj.state,county:obj.county}]

		DataMap.setDataTypes "zipcode", [
			name    : "Address"
			source  : "addressArray"
			visible : true
			type    : "array"
			width   : 200
		]

		addHolder().setView "Table", (table)->
			table.addTable "zipcode"

	addTestButton "complicated array data type test", "Open", ()->

		##| set the address as object in data map, to manipulate address field as simple object
		for key,obj of DataMap.getDataMap().engine.export("zipcode")
			obj.address = [{ "code": "00501", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "lat": "40.922326", "lon": "-72.637078", "arrayData": [1,2,3] },
	        		{"code": "00544", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "lat": "40.922326", "lon": "-72.637078", "arrayData": [1,2,3] },
	        		{"code": "01001", "city": "Agawam", "state": "MA", "county": "HAMPDEN", "lat": "42.140549", "lon": "-72.788661", "arrayData": [1,2,3] },
			        {"code": "01002", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.367092", "lon": "-72.464571", "arrayData": [1,2,3] },
	        		{"code": "01003", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.369562", "lon": "-72.63599", "arrayData": [1,2,3] },
	        		{"code": "01004", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.384494", "lon": "-72.513183", "arrayData": [1,2,3]}]

		DataMap.setDataTypes "zipcode", [
			name    : "Address"
			source  : "address"
			visible : true
			type    : "array"
			width   : 200
		]

		addHolder().setView "Table", (table)->
			table.addTable "zipcode"


	addTestButton "editable popup on click", "Open", ()->
		wgtHolder = addHolder()
		## create a TableView with a NavBar above it
		wgtHolder.doSetViewWithNavbar "Table", (tableView, navbarView) =>
			## TableView: add a table as well as callback for row
			table = tableView.addTable "zipcode"
			table.rowCallback = (data,e) ->
				if data.id
					new PopupForm('zipcode', 'code', data.id)
			
			## Add a NavButton with click event listener to NavBar
			navButton = new NavButton "Create New","btn btn-info navbar-btn"
			navButton.onClick = ()->
				p = new PopupForm('zipcode', 'code')
				p.onCreateNew = (tableName, data) ->
					console.log tableName, data
					##| apply filter or sorting to update the newly create row
					setTimeout () ->
						table.applyFilters()
						table.updateRowData()
					, 100
					true
			navbarView.addToolbar [navButton]
		true

	addTestButton "editable popup on click with custom columns", "Open", ()->
		addHolder().setView "Table", (table)->
			table.addTable "zipcode"

			_columns = []
			_columns.push
				name       : 'State'
				source     : 'state'
				type       : 'text'
				required   : true
			_columns.push
				name       : 'County'
				source     : 'county'
				type       : 'text'
				required   : true
			table.rowCallback = (data,e) ->
				if data.key
					new PopupForm('zipcode', 'code', data.key, _columns)
			true

	addTestButton "popup table with array data", "Open", ()->
		arrayData = [{ "code": "00501", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" },
	        		{"code": "00544", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" },
	        		{"code": "01001", "city": "Agawam", "state": "MA", "county": "HAMPDEN", "area_code": "", "lat": "42.140549", "lon": "-72.788661" },
			        {"code": "01002", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.367092", "lon": "-72.464571" },
	        		{"code": "01003", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.369562", "lon": "-72.63599" },
	        		{"code": "01004", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.384494", "lon": "-72.513183"}]

		doPopupTableView arrayData , "Show Table With Array", "showTableClasses", 800, 400
		.then (view) ->
			console.log "Table With Array data"

		true

	addTestButton "popup table edit function serialize", "Open", ()->
		arrayData = [{ "code": "00501", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" },
	        		{"code": "00544", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" },
	        		{"code": "01001", "city": "Agawam", "state": "MA", "county": "HAMPDEN", "area_code": "", "lat": "42.140549", "lon": "-72.788661" },
			        {"code": "01002", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.367092", "lon": "-72.464571" },
	        		{"code": "01003", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.369562", "lon": "-72.63599" },
	        		{"code": "01004", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "area_code": "", "lat": "42.384494", "lon": "-72.513183"}]
		id = 0
		for rec in arrayData
			DataMap.addDataUpdateTable "zipcode2", ++id, rec
		
		##
		## Add an edit function to "code"
		DataMap.changeColumnAttribute "zipcode2", "code", "editFunction", (el, val, path, saveCallback) =>
			m = new ModalDialog
				showOnCreate: false
				content:      "Type a new code in the following box."
				position:     "top"
				title:        "Code Editor"
				ok:           "OK"

			m.getForm().addTextInput "input1", "New Code"
			m.getForm().onSubmit = (form) =>
				console.log "New code=", form.input1.value
				saveCallback path, form.input1.value
				console.log "Data is updated: path = #{path}, new value = #{form.input1.value}"
				m.hide()
			m.show()
			true

		obj = DataMap.getTypes("zipcode2").serialize()
		table3 = DataMap.setTypes("zipcode3")
		table3.unserialize obj
		id = 0
		for rec in arrayData
			DataMap.addDataUpdateTable "zipcode3", ++id, rec		

		doPopupView "Table", "PopupView with EditFunction", "popuptable_editFunction", 800, 400, (view) ->
			view.addTable "zipcode3"
			true

	addTestButton "popup table with complicated array data", "Open", ()->
		arrayData = [{ "code": "00501", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "lat": "40.922326", "lon": "-72.637078", "arrayData": [1,2,3] },
	        		{"code": "00544", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "lat": "40.922326", "lon": "-72.637078", "arrayData": [1,2,3] },
	        		{"code": "01001", "city": "Agawam", "state": "MA", "county": "HAMPDEN", "lat": "42.140549", "lon": "-72.788661", "arrayData": [1,2,3] },
			        {"code": "01002", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.367092", "lon": "-72.464571", "arrayData": [1,2,3] },
	        		{"code": "01003", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.369562", "lon": "-72.63599", "arrayData": [1,2,3] },
	        		{"code": "01004", "city": "Amherst", "state": "MA", "county": "HAMPSHIRE", "lat": "42.384494", "lon": "-72.513183", "arrayData": [1,2,3]}]

		doPopupTableView arrayData , "Show Table With Array", "showTableClasses", 800, 400
		.then (view) ->
			console.log "Table With Array data"

		true

	addTestButton "popup table with object data", "Open", ()->
		objectData = {
					1:{ "code": "00501", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" },
					2:{"code": "00544", "city": "Holtsville", "state": "NY", "county": "SUFFOLK", "area_code": "", "lat": "40.922326", "lon": "-72.637078" }
					};

		doPopupTableView objectData , "Show Table With Object", "showTableClasses", 800, 400
		.then (view) ->
			console.log "Table with object data"
		true

	addTestButton "table with fixed header and scrollable", "Open", ()->
		holder = addHolder()
		holder.height(350) ##| to add scroll the height is fix
		holder.setView "Table", (table)->
			table.loadTable "zipcode"
		true

	addTestButton "Table column filter", "Open", () ->
		addHolder().setView "Table", (view) ->
			view.deserialize
				tabletitle: "Zipcode"
				collection: "zipcode"
				colfilter: (col) ->
					console.log "Column filter: ", col
					validcols = [ "city", "state", "code" ]
					source = col.getSource()
					if  validcols.indexOf(source) >= 0
						return true
					false
		true

	addTestButton "Table Column with Options", "Open", () ->
		addHolder()
		.setView "Table", (view) ->
			view.addTable "testData"
			true
		true
		
	go()
