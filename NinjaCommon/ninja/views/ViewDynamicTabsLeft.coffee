class ViewDynamicTabsLeft extends ViewDynamicTabs
    tabWidth : 120
    tabHeight: 120

    getInsideWidth: () =>
        @getWidth() - @tabWidth

    getInsideHeight: () =>
        @getHeight()

    refreshTagPosition: (tag) =>
        if !tag? or !tag.body? or !tag.body.move? then return false
        tag.body.move @tabWidth, 0, @getInsideWidth(), @getInsideHeight()
        true

    createTabList: () =>
        @tabList    = @add "ul", "ninja-tab-list ninja-tab-list-left", "ninja-tab-list"
        @rightTab   = @tabList.add "li", "ninja-nav-tab ninja-right-tab"
        @tabList.width(@tabWidth)
        true

    updateRightTab: () =>
        tabsHeight = 0
        for id, tag of @tags 
            if tag.tab? and tag.tab.getVisible() is true 
                tabsHeight += tag.tab.getTag().outerHeight()

        @rightTab.move 0, tabsHeight, @tabWidth, @getInsideHeight() - tabsHeight
