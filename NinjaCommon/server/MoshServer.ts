// Requires mosh - "sudo apt install mosh"
// if firewall enabled: add "iptables -I INPUT 1 -p udp --dport 60000:61000 -j ACCEPT"

const pty    = require('pty.js');
const server = require('socket.io');
const fs     = require('fs');
let path     = require('path');
const http   = require('http');
const https  = require('https');
path         = require('path');
const serve  = require('koa-static');
const koa    = require('koa');

const { argv } = require('yargs')
    .usage('Usage $0 <cmd> [options]');

const port   = 3000;
let runhttps = false;
let sshuser  = 'root';
let sshhost  = 'localhost';

if (argv.sshhost != null) {
    ({ sshhost } = argv);
}

if (argv.sshuser != null) {
    ({ sshuser } = argv);
}

if ((argv.sslkey != null) && (argv.sslcert != null)) {
    runhttps         = true;
    this.ssl['key']  = fs.readFileSync(path.resolve(argv.sslkey));
    this.ssl['cert'] = fs.readFileSync(path.resolve(argv.sslcert));
}

process.on('uncaughtException', err => console.log('Error', err));

const app = koa();
app.use(function* (ctx: any) {
    return yield serve(path.join(__dirname, '../ninja/vendor/terminal'));
});

let httpserv;

if (runhttps) {
    httpserv = https.createServer(this.ssl, app.callback()).listen(port, () => console.log('HTTPS on port ', port));
} else {
    httpserv = http.createServer(app.callback()).listen(port, () => console.log('HTTP on port', port));
}

const io = server(httpserv, { path: '/mosh' });
io.on('connection', function(socket: any) {

    if (!/@/.test(sshuser)) {
        sshuser = sshuser + '@';
    }

    const term = pty.spawn('mosh', [sshuser + sshhost, '--', 'tmux'], {
            name: 'xterm-256color',
            cols: 80,
            rows: 30,
        },
    );

    term.on('data', (data: any) => socket.emit('output', data));

    socket.on('resize', (data: any) => term.resize(data.col, data.row));

    socket.on('input', (data: any) => term.write(data));

    return socket.on('disconnect', () => term.end());
});
