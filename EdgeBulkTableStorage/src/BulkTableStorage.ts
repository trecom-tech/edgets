import * as process from 'process';
import { config }   from 'edgecommonconfig';

// we can not use import syntax for these libraries because they are not not typescript ready
const azureStorage = require('azure-storage');

const entGen       = azureStorage.TableUtilities.entityGenerator;
const maxDateTime  = new Date(2100, 1, 1).getTime();
let globalCounter  = 0;

class BulkTableStorageResult {
    
    tableSvc            : any;
    tableName           : string;
    query               : string;
    rows                : any[];
    length              : number;
    continuationToken   : string;
    
    constructor(result:any, tableSvc:any, tableName:string, query:string) {
        this.tableSvc  = tableSvc;
        this.tableName = tableName;
        this.query     = query;
        this.rows      = [];

        if (result && result.entries) {
            for (let rec of result.entries) {
                this.rows.push(this.entToObject(rec));
            }
        }

        this.length            = this.rows.length;
        this.continuationToken = result.continuationToken;
        // continuationToken:
        // { nextPartitionKey: '1!32!Y29sb255LW5lZWRlZGxndjNub2RlMTA-',
        //     nextRowKey: '1!28!MDI2MDIzODQzODAzNjctMTI5NTc-',
        //     targetLocation: 0 } }
    }

    /**
     * Convert from record to JSON object
     */
    entToObject(rec: any) {
        let newData:any = {};
        
        for (let varName in rec) {
            if (!rec[varName]) {
                continue;
            }

            newData[varName] = (varName === '.metadata') ? rec[varName] : rec[varName]._;
        }

        return newData;
    }
}


export default class BulkTableStorage {
    
    configName      : string;
    tableSvc        : any;
    tableName       : string;
    log             : any;
    isReady         : boolean;
    enabled         : boolean;
    openTablePromise: any;
    creds           : any;

    /**
     * Pass in the config name as defined in the credential like this:
     * azureTableStorage =
     *      connection: AZURE_STORAGE_CONNECTION_STRING
     */
    constructor(configName: string, tableName: string) {
        this.log = config.getLogger(`BulkTableStorage-${tableName}`);
        this.isReady = false;
        this.enabled = true;
        this.configName = configName;
        this.tableName = tableName;
        this.creds = config.getCredentials(this.configName);
    }

    /**
     * Insert a record using an inverse date time where lower values are newer so they query
     * at the top of the azure table results
     */
    doInsertLogRecord(partKey: string, newData: any) {
        if (!newData) { return false; }
        if (typeof newData !== 'object') { return false; }
        
        globalCounter++;
        let rowkey = `0${maxDateTime - new Date().getTime()}`;
        rowkey += `-${globalCounter.toString(16)}`;
        rowkey += `-${process.pid}`;

        return this.doInsertRecord(partKey, rowkey, newData, true);
    }

    createQuery(fieldName: string, value: any, limit: any = null) {
        if (!value) {
            return null;
        }

        if (fieldName.length === 0) {
            return null;
        }

        const query = new azureStorage.TableQuery();

        if (limit) {
            query.top(limit);
        }

        if (fieldName && value) {
            query.where(`${fieldName} eq ?`, value);
        }

        return query;
    }

    doQuery(query: any) {
        return new Promise((resolve, reject) => {
            if (!query) {
                resolve(false);
            }

            this.doInternalOpenTable()
            .then((status) => {
                if (!status) {
                    resolve(false);
                }

                this.tableSvc.queryEntities(this.tableName, query, null, (err: any, result: any, response: any) => {
                    if (err) {
                        config.status(`BulkTableStorage - doQuery ${this.tableName} data=`, query);
                        config.status(`BulkTableStorage - doQuery ${this.tableName} err=`, err);
                        config.status(`BulkTableStorage - doQuery ${this.tableName} result=`, result);
                        config.status(`BulkTableStorage - doQuery ${this.tableName} response=`, response);

                        this.log.error('doQuery', { tableName: this.tableName, err: err });
                        resolve(new BulkTableStorageResult(null, this.tableSvc, this.tableName, query));
                    } else {
                        config.status(`BulkTableStorage - doQuery ${this.tableName} result=`, result);
                        config.status(`BulkTableStorage - doQuery ${this.tableName} response=`, response);
                        resolve(new BulkTableStorageResult(result, this.tableSvc, this.tableName, query));
                    }
                });
            }).catch(() => {
                resolve(false);
            })
        });
    }

    private doInternalOpenTable() {
        return new Promise((resolve, reject) => {
            if (!this.creds || !this.creds.connection) {
                config.status(`BulkTableStorage doInternalOpenTable no credentials for ${this.configName}`);
                this.enabled = false;
                resolve(false);
            }

            // Open / Create the table and leave room for later async initialization
            config.status(`BulkTableStorage - Initialize Table ${this.tableName}`);
            this.tableSvc = azureStorage.createTableService(this.creds.connection);
            this.tableSvc.createTableIfNotExists(this.tableName, (err: any, result: any, reponse: any) => {
                // config.status 'BulkTableStorage - createTable #{@tableName} result=', result
                // config.status 'BulkTableStorage - createTable #{@tableName} response=', response

                if (err) {
                    config.status(`BulkTableStorage - createTable ${this.tableName} err=`, err);
                    this.enabled = false;
                    this.log.error('createTableIfNotExists', { talbeName: this.tableName, err: err });
                    resolve(false);
                } else {
                    this.isReady = true;
                    resolve(true);
                }
            });
        });
    }

    private internalSetupTabledata(partKey: any, rowKey: any, newData: any) {
        let tableData: any     = {};
        tableData.PartitionKey = entGen.String(partKey);
        tableData.RowKey       = entGen.String(rowKey);

        let objToEnt = (obj: any, prefix: string) => {
            // See https://azure.github.io/azure-storage-node/TableUtilities.html
            for (let varName in obj) {
                const value = obj[varName];
                let safeName = varName.replace(/[^a-zA-Z0-9]/g, '_');
                safeName = safeName.replace(/^_/, '');
                safeName = `${prefix}${safeName}`;

                if (typeof value === 'string') {
                    tableData[safeName] = entGen.String(value);
                } else if (typeof value === 'object' && typeof value.getTime === 'function') {
                    tableData[safeName] = entGen.DateTime(value);
                } else if (typeof value === 'number') {
                    if (Math.floor(value) === value) {
                        tableData[safeName] = entGen.Int32(value);
                    } else {
                        tableData[safeName] = entGen.Double(value);
                    }
                } else if (typeof value === 'boolean') {
                    tableData[safeName] = entGen.Boolean(value);
                } else if (typeof value === 'object') {
                    objToEnt(value, `${safeName}_`);
                }
            }

            return true;
        };

        objToEnt(newData, '');

        return tableData;
    }

    /**
     * see https://github.com/Azure/azure-storage-node/blob/master/lib/services/table/tableservice.js
     */
    // private doInsertOrReplace(partKey: any, rowKey: any, newData: any) {
    //     return new Promise((resolve, reject) => {
    //         if (!this.enabled) {
    //             resolve(false);
    //             return;
    //         }

    //         let tableData = this.internalSetupTabledata(partKey, rowKey, newData);

    //         this.doInternalOpenTable()
    //         .then((status) => {
    //             if (!status) {
    //                 resolve(false);
    //                 return;
    //             }

    //             config.status(`BulkTableStorage doInsertOrReplace ${this.tableName}:`, newData);
    //             let options: any = {};

    //             this.tableSvc.insertOrReplaceEntity(this.tableName, tableData, options, (err: any, result: any, response: any) => {
    //                 if (err) {
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} data=`, tableData);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} err=`, err);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} result=`, result);
    //                     config.status(`BulkTableStorage - doInsertOrReplace ${this.tableName} response=`, response);

    //                     this.log.error('insertOrReplaceEntity', {tableName: this.tableName, data: tableData, err: err});
    //                     resolve(false);
    //                 } else {
    //                     resolve(true);
    //                 }
    //             });
    //         }).catch(() => {
    //             resolve(false);
    //         });
    //     });
    // }

    private doInsertRecord(partKey: any, rowKey: any, newData: any, ignoreFailed: boolean = true) {
        return new Promise((resolve, reject) => {
            if (!this.enabled) {
                resolve(false);
                return;
            }

            const tableData = this.internalSetupTabledata(partKey, rowKey, newData);
            
            this.doInternalOpenTable()
            .then((status) => {
                if (!status) {
                    resolve(false);
                }

                config.status(`BulkTableStorage doInsertRecord ${this.tableName}:`, newData);

                this.tableSvc.insertEntity(this.tableName, tableData, { echoContent: false }, (err: any, result: any, response: any) => {
                    if (result) {
                        result.rowkey = rowKey;
                        result.partKey = partKey;
                    }

                    if (err) {
                        config.status(`BulkTableStorage - doInsertRecord ${this.tableName} data=`, tableData);
                        config.status(`BulkTableStorage - doInsertRecord ${this.tableName} err=`, err);
                        config.status(`BulkTableStorage - doInsertRecord ${this.tableName} result=`, result);
                        config.status(`BulkTableStorage - doInsertRecord ${this.tableName} response=`, response);

                        this.log.error('intsertEntity', {tableName: this.tableName, data: tableData, err: err});
                        if (ignoreFailed) {
                            resolve(false);
                        } else {
                            reject(err);
                        }
                    } else {
                        resolve(true);
                    }
                });
            }).catch(() => {
                resolve(false);
            });
        });
    }

}