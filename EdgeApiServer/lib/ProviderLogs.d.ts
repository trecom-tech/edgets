import EdgeDataSetManager from 'edgedatasetmanager';
import ApiProvider from './ApiProvider';
export default class ProviderLogs extends ApiProvider {
    dmLogs: EdgeDataSetManager;
    dbLogs: EdgeDataSetManager;
    constructor(prototype: ApiProvider);
    onInit(): Promise<boolean>;
    getBaseName(): string;
    doGetLatestJobs(limit: number): Promise<any[]>;
}
