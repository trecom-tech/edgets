
class ViewTestIcons extends View

    onShowScreen: ()=>
        $.get "/vendor/icons.json", (data)=>
            @setView "DynamicTabs", (tab)->
                regularHTML = solidHTML = lightHTML = "
                        <div class='content'>
                            <div class='row'>
                        "
                for icon in data
                    regularHTML += "
                        <div class='fa-hover col-sm-3 icon_example'>
                            <div class='icon_image'><i class='far #{icon}'></i></div>
                            <div class='icon_name' onClick='doCopyIcon(this, \"far\");'> #{icon} </div>
                        </div>
                    "
                    solidHTML += "
                        <div class='fa-hover col-sm-3 icon_example'>
                            <div class='icon_image'><i class='fas #{icon}'></i></div>
                            <div class='icon_name' onClick='doCopyIcon(this, \"fas\");'> #{icon} </div>
                        </div>
                    "
                    lightHTML += "
                        <div class='fa-hover col-sm-3 icon_example'>
                            <div class='icon_image'><i class='fal #{icon}'></i></div>
                            <div class='icon_name' onClick='doCopyIcon(this, \"fal\");'> #{icon} </div>
                        </div>
                    "

                regularHTML += "</div></div>"
                solidHTML += "</div></div>"
                lightHTML += "</div></div>"

                tab.addTab "Regular", regularHTML
                tab.addTab "Solid", solidHTML
                tab.addTab "Light", lightHTML

                tab.tabs.Regular.body.css 'overflow', 'scroll'
                tab.tabs.Solid.body.css 'overflow', 'scroll'
                tab.tabs.Light.body.css 'overflow', 'scroll'
