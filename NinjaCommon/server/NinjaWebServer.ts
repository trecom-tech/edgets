import WebServerHelper from './WebServerHelper';

const exreport              = require('edgecommonexceptionreport');
const koa                   = require('koa');
const session               = require('koa-session');
const glob                  = require('glob-all');
const stylus                = require('stylus');
const pug                   = require('pug');
const fs                    = require('fs');
const zlib                  = require('zlib');
const coffeeScript          = require('coffeescript');
const { updateSyntaxError } = require('coffeescript/lib/coffee-script/helpers');
const mime                  = require('mime-types');
const co                    = require('co');
const browserify            = require('browserify');
const coffeeify             = require('coffeeify');
const formidable            = require('formidable');
let path                    = require('path');
const koaBody               = require('koa-body');
const bodyParser            = require('koa-bodyparser');
const https                 = require('https');
const http                  = require('http');
const shell                 = require('shelljs');

const { argv } = require('yargs')
    .usage('Usage: $0 --gen')
    .demandOption([]);

const saveNinjaFileAndExit = (strCSS: string, strJS: string) => {

    console.log('Writing ../ninja/ninja.css');
    fs.writeFile('../ninja/ninja.css', strCSS, function(err: Error, done: any) {
        if (err != null) {
            return console.log('Error writing Ninja.css', err);
        }
    });
    console.log('Writing ../ninja/ninja.js');
    return fs.writeFile('../ninja/ninja.js', strJS, function(err: Error, done: any) {
        if (err != null) {
            return console.log('Error writing Ninja.js:', err);
        }
        if (argv.gen) {
            return process.exit(0);
        }
    });
};

const bundle = browserify({
    extensions: ['.coffee'],
});

bundle.transform(coffeeify, {
        bare  : true,
        header: true,
    },
);

const WebPath       = '../ninja/';
let server: any     = null;
let ninjaJavascript = '';
let ninjaCss        = '';

const ninjaPathMiddleware = (app: any) =>

    app.use(async (ctx: any, next: any) => {

        if (/ninja.js/.test(ctx.url)) {
            ctx.set('Content-Encoding', 'gzip');
            ctx.set('Content-Length', ninjaJavascript.length);
            ctx.response.type = 'application/vnd.api+json';
            ctx.body          = ninjaJavascript;
            return true;
        }

        if (/ninja.css/.test(ctx.url)) {
            ctx.set('Content-Encoding', 'gzip');
            ctx.set('Content-Length', ninjaCss.length);
            ctx.response.type = 'text/css';
            ctx.body          = ninjaCss;
            return true;
        }

        await next();
    })
;

const staticPathMiddleware = function(app: any, folder: any, url: string) {

    //
    //  Convert a single path to a list of paths.
    if (typeof folder === 'string') {
        folder = [folder];
    }

    console.log(`Mapped ${folder} to url ${url}`);

    const rePath = new RegExp(`^${url}/([^\?]+)`, 'i');
    return app.use(async (ctx: any, next: any) => {

        let e;
        try {
            const m = ctx.url.match(rePath);
            if (m != null) {

                for (path of Array.from(folder)) {

                    try {
                        const filename = path + m[1];
                        // console.log "staticPathMiddleware checking #{filename}"
                        const stats    = fs.statSync(filename);
                        if (stats != null) {
                            console.log(filename, "size:", stats.size, "bytes");
                            ctx.response.type = mime.lookup(filename);
                            ctx.body          = fs.createReadStream(filename);
                            return true;
                        }

                    } catch (error) {
                        e = error;
                    }
                }

                // console.log "Unable to find #{path}/#{m[1]}"

                return false;
            }

        } catch (error1) {
            e = error1;
            console.log(`Static file error: ${e}`);
        }

        await next();
    });
};


const compileScreen = async (screenName: any, appName: any) => {

    console.log(`compileScreen Screen=${screenName}, appName=${appName}`);

    const pathList = ['../ninja/screens/'];
    // if appName? then pathList = [ "../test/#{appName}/screens/", "../ninja/screens/"]

    const filenameStylus = await WebServerHelper.doFindFileInPath(`screen_${screenName.toLowerCase()}.styl`, pathList);
    const filenamePug    = await WebServerHelper.doFindFileInPath(`screen_${screenName.toLowerCase()}.pug`, pathList);
    const filenameCoffee = await WebServerHelper.doFindFileInPath(`screen_${screenName.toLowerCase()}.coffee`, pathList);

    let html = await WebServerHelper.doCompilePugFile(filenamePug);
    html     = `<div id='Screen${screenName}' class='screen contentNoPadding overflow-hidden'>` + html + '</div>';
    const js = await WebServerHelper.doCompileCoffeeFile(filenameCoffee);

    let str = '';
    str += js;
    str += '\n';
    str += `Screen${screenName}.prototype.screenContent = '`;
    str += escape(html);
    str += '\';';

    //
    //  CSS File
    let css = await WebServerHelper.doCompileStylusFile(filenameStylus);
    if (css) {
        css = escape(css);
        str += `Screen${screenName}.prototype.css = unescape('${css}');`;
    }

    return str;
}

const compileView = async function(viewName: string, appName: string) {

    console.log(`compileView viewName=${viewName}, appName=${appName}`);


    const pathList = ['../ninja/views/', '../test/views/'];
    // if appName? then pathList = [ "../test/#{appName}/views/", "../ninja/views/" ]

    const filenameCss  = await WebServerHelper.doFindFileInPath(`${viewName}.styl`, pathList);
    const filenameHtml = await WebServerHelper.doFindFileInPath(`${viewName}.pug`, pathList);
    const filenameJs   = await WebServerHelper.doFindFileInPath(`${viewName}.coffee`, pathList);

    let css  = await WebServerHelper.doCompileStylusFile(filenameCss);
    let html = await WebServerHelper.doCompilePugFile(filenameHtml);
    let js   = await WebServerHelper.doCompileCoffeeFile(filenameJs);

    html = escape(html);
    css  = escape(css);

    js += '\n';
    js += `${viewName}.prototype.template = unescape('${html}');\n`;
    js += `${viewName}.prototype.css = unescape('${css}');`;
    js += '\n';

    return js;
};

export class NinjaWebServer {
    staticContent: any;
    fileWatch: any;
    fileTimer: any;
    app: any;
    ninjaCoffeeRaw: any;
    ninjaCoffeeNormal: any;
    ninjaCoffeeExtends: any[];
    ninjaCoffeeFiles: any;
    ninjaCoffeeMap: any;
    ninjaStylusFiles: any;
    ninjaPugFiles: any;

    constructor() {
        server             = this;
        this.staticContent = {};
        this.fileWatch     = {};
        this.fileTimer     = {};
        const moduleNames  = [
            'edgecommondatasetconfig',
            'jquery',
            'co',
            'ev-emitter',
            'flatpickr',
            'handlebars',
            'iscroll',
            'draggabilly',
            'numeral',
            'moment',
            'mathjs',
            'jquery-range',
        ];
        bundle.require(moduleNames, { basedir: '../node_modules/' });

        this.rebuildNinja();
        this.app = new koa();
        this.app.use(bodyParser());
        this.app.keys = ['NinjaWebServerKey123'];
        this.app.use(session(this.app));

        this.findAppNameMiddleware();

        this.uploadPathMiddleware();

        this.loadStylusFile('/css/test.css', '../test/css/*styl');
        this.loadPugFile('/', '../test/template/index.pug');
        this.loadPugFile('/index.html', '../test/template/index.pug');

        ninjaPathMiddleware(this.app);

        staticPathMiddleware(this.app, '../node_modules/mathjs/dist/', '/vendor/mathjs');
        // staticPathMiddleware @app, "../node_modules/mathjax/", "/vendor/mathjax"
        // staticPathMiddleware @app, "../node_modules/mathjax/extensions/", "/extensions"

        //
        // Vendor or 3rd getfile
        staticPathMiddleware(this.app, '../test/js/test_data/', '/js/test_data');
        staticPathMiddleware(this.app, '../ninja/vendor/ace/', '/ace');
        staticPathMiddleware(this.app, '../ninja/vendor/closure/', '/closure');
        staticPathMiddleware(this.app, '../ninja/vendor/closure/', '/closure-library/closure');
        staticPathMiddleware(this.app, '../ninja/vendor/blockly/', '/blockly');


        //
        // Local static files
        staticPathMiddleware(this.app, ['../ninja/fonts/', '../test/fonts/'], '/fonts');
        staticPathMiddleware(this.app, ['../ninja/images/', '../test/images/'], '/images');
        staticPathMiddleware(this.app, ['../ninja/vendor/', '../test/vendor/'], '/vendor');
        staticPathMiddleware(this.app, '../../CoffeeNinjaCommon/ninja/', '/ninja');
        staticPathMiddleware(this.app, ['../doc/', '../doc/'], '/doc');

        this.dynamicJavascript();
        this.setupStatic();
        this.screenMiddleware();

        const config = {
            domain: 'localhost',
            http  : {
                port: 9000,
            },
            https : {
                port   : 8443,
                options: {
                    key : fs.readFileSync(path.resolve(process.cwd(), 'certs/privkey.pem'), 'utf8').toString(),
                    cert: fs.readFileSync(path.resolve(process.cwd(), 'certs/fullchain.pem'), 'utf8').toString(),
                },
            },
        };

        const serverCallback = this.app.callback();

        // HTTP Server
        const httpServer = http.createServer(serverCallback);
        httpServer.listen(config.http.port, function(err: Error) {
            if (err != null) {
                return console.error('HTTP server FAIL: ', err, (err && err.stack));
            } else {
                return console.log(`HTTP  server OK: http://${config.domain}:${config.http.port}`);
            }
        });

        // HTTPS Server
        const httpsServer = https.createServer(config.https.options, serverCallback);
        httpsServer.listen(config.https.port, function(err: Error) {
            if (err != null) {
                return console.log('HTTPS server FAIL', err, (err && err.stack));
            } else {
                return console.log(`HTTPS server ok: https://${config.domain}:${config.https.port}`);
            }
        });
    }

    //
    //  Get the component required for starting Socket.io
    getHttpServer() {
        let httpServer;
        return httpServer = require('http').Server(this.app.callback());
    }

    //
    //  Set a timer to watch a file for changes
    setWatchTimer(f: string, callback: any) {
        //
        //  Watch for changes
        if ((server.fileWatch[f] == null)) {
            server.fileWatch[f] = fs.watch(f, { recursive: true }, (event: any, filename: string) => {
                console.log(`WATCH ${filename}:`, event);

                if (server.fileTimer[f]) {
                    clearTimeout(server.fileTimer[f]);
                }
                filename = filename.replace('\\', '/');
                return server.fileTimer[f] = setTimeout(callback, 1000, filename);
            });
        }

        return true;
    }

    //
    //  Process a path or glob to a set of files
    //  Initialize the static content holder and make a callback for each
    processPath(url: string, path: string, contentType: string, callbackEachFile: any) {

        return co(function* () {

            console.log(`Processing ${path} (${contentType}) for ${url}`);
            const files = glob.sync(path);

            this.staticContent[url] = {
                mime   : contentType,
                content: '',
            };

            return yield* (function* () {
                const result = [];
                for (const f of files) {

                    console.log(`processPath ${contentType}: Reading file: ${f} for ${url}`);
                    const content = yield callbackEachFile(f);
                    this.staticContent[url].content += content;

                    //
                    //  Watch for changes
                    result.push(this.setWatchTimer(f, () => {
                        server.fileTimer[f] = null;
                        console.log(`processPath Reloading url=${url} path=${path}`);
                        return this.processPath(url, path, contentType, callbackEachFile);
                    }));
                }
                return result;
            }).call(this);
        }.bind(this));
    }

    loadLessFiles(url: string, path: string) {

        const files = [WebPath + 'css/variables.less', WebPath + 'css/mixins.less'];
        this.processPath(url, path, 'text/css', (filename: string) => {
            if (!/mixins/.test(filename) && !/variables/.test(filename)) {
                files.push(filename);
            }

            return new Promise((resolve, reject) => {
                return resolve('');
            });
        });

        return setTimeout(() => {
                return WebServerHelper.doCompileLessFiles(files)
                    .then((css: any) => {
                        return this.staticContent[url].content = css.css;
                    });
            }
            , 200);
    }


    //
    // Load a stylus template to a URL
    // If that template file changes, recompile and update the url
    //
    loadStylusFile(url: string, path: string) {

        this.processPath(url, path, 'text/css', WebServerHelper.doCompileStylusFile);
        return true;
    }

    loadPugFile(url: string, path: string) {

        this.processPath(url, path, 'text/html', WebServerHelper.doCompilePugFile);
        return true;
    }

    loadCoffeeFiles(url: string, path: string) {

        this.processPath(url, path, 'text/javascript', WebServerHelper.doCompileCoffeeFile);
        return true;
    }

    setupStatic() {

        return this.app.use(async (ctx: any, next: any) => {

            if (server.staticContent[ctx.path] != null) {
                ctx.response.type = server.staticContent[ctx.path].mime;
                ctx.body          = server.staticContent[ctx.path].content;
                return console.log(`setupStatic Static sent ${ctx.path} [`, ctx.response.type, ']');
            } else {
                await next();
            }
        });
    }

    //
    //  If there is an app name, pull it out of the URL
    findAppNameMiddleware() {

        const reApp = new RegExp('^/([a-zA-Z]+)/*$');

        return this.app.use(async (ctx: any, next: any) => {

            const m = ctx.url.match(reApp);
            if ((m != null) && (ctx.url !== '/upload/') && (ctx.url !== '/getfilelist/') && (ctx.url !== '/filedownload/')) {
                console.log('findAppNameMiddleware Found app ', m[1], ' in ', ctx.url);
                path                 = '/';
                ctx.session.appName = m[1];
                ctx.response.type   = server.staticContent[path].mime;
                ctx.body            = server.staticContent[path].content;
                return true;
            } else {
                await next();
            }
        });
    }

    uploadPathMiddleware() {

        const reApp    = new RegExp('^/([a-zA-Z]+)/*$');
        const basePath = '/tmp';

        const getFiles = (dir: string, files_?: any) => {
            files_      = files_ || [];
            const files = fs.readdirSync(dir);
            for (let file_ of Array.from(files)) {
                const name = dir + '/' + file_;
                if (fs.statSync(name).isDirectory()) {
                    getFiles(name, files_);
                } else {
                    files_.push(name);
                }
            }
            return files_;
        };

        const move = (oldPath: string, newPath: string, callback: any) =>
            fs.rename(oldPath, newPath, function(err: any) {
                if (err) {
                    if (err.code === 'EXDEV') {
                        copy(oldPath, newPath, callback);
                    } else {
                        callback(err);
                    }
                    return true;
                }
                return callback();
            })
        ;

        const copy = (oldPath: string, newPath: string, callback: any) => {
            const readStream  = fs.createReadStream(oldPath);
            const writeStream = fs.createWriteStream(newPath);

            readStream.on('error', callback);
            writeStream.on('error', callback);

            readStream.on('close', () => fs.unlink(oldPath, callback));

            return readStream.pipe(writeStream);
        };


        const mkDirByPathSync = (targetDir: string) => {
            const newpath = basePath + '/' + targetDir;
            return shell.mkdir('-p', newpath);
        };

        const writeMetaData = (directory: string, file: string, username: string, description: string, mime: string) => {
            const sourceFile = directory + file;
            const stats      = fs.statSync(sourceFile);
            const bytes      = stats['size'];
            let size         = '';
            if (bytes < 1024) {
                size = bytes + 'B';
            } else if ((bytes / 1024) < 1024) {
                size = Math.floor(bytes / 1024) + 'KB';
            } else if ((bytes / 1024 / 1024) < 1024) {
                size = (bytes / 1024 / 1024).toFixed(2) + 'MB';
            } else {
                size = (bytes / 1024 / 1024 / 1024).toFixed(2) + 'GB';
            }

            const date                 = new Date();
            let hour: string | number  = date.getHours();
            hour                       = (hour < 10 ? '0' : '') + hour;
            let min: string | number   = date.getMinutes();
            min                        = (min < 10 ? '0' : '') + min;
            let sec: string | number   = date.getSeconds();
            sec                        = (sec < 10 ? '0' : '') + sec;
            const year                 = date.getFullYear();
            let month: string | number = date.getMonth() + 1;
            month                      = (month < 10 ? '0' : '') + month;
            let day: string | number   = date.getDate();
            day                        = (day < 10 ? '0' : '') + day;
            const dt                   = year + '-' + month + '-' + day; // + " " + hour + ":" + min + ":" + sec

            const content = `{"original_filename": "${file}` + `", \
"filename": "` + sourceFile + `", \
"username": "` + username + `", \
"file_size": "` + size + `", \
"mime_type": "` + mime + `", \
"timestamp": "` + dt + `", \
"description": "` + description + '"}';
            return fs.writeFileSync(directory + file + '.meta', content);
        };

        return this.app.use(async (ctx: any, next: any) => {
            let form;
            const m = ctx.url.match(reApp);
            if ((m != null) && (ctx.url === '/upload/')) {
                form             = new formidable.IncomingForm();
                form.maxFileSize = 2 * 1024 * 1024 * 1024;
                return form.parse(ctx.req, (err: Error, fields: any, files: any) => {
                    if (err) {
                        console.log(err);
                    }

                    const directory       = fields.dpath;
                    const { username }    = fields;
                    const { description } = fields;
                    const { mimetype }    = fields;
                    mkDirByPathSync(directory);

                    const oldpath = files.myfile.path;
                    const newpath = basePath + '/' + directory + '/' + files.myfile.name; // new Date().getTime()
                    console.log(newpath);
                    move(oldpath, newpath, function(err: Error) {
                        if (err) {
                            throw err;
                        }
                        return writeMetaData(basePath + '/' + directory + '/', files.myfile.name, username, description, mimetype);
                    });

                    path                 = '/';
                    ctx.session.appName = 'upload';
                    ctx.response.type   = server.staticContent[path].mime;
                    return ctx.body = { result: true };
                });

            } else if ((m != null) && (ctx.url === '/getfilelist/')) {
                let fileList       = [];
                const pdata: any[] = [];
                form               = new formidable.IncomingForm();
                return form.parse(ctx.req, (err: Error, fields: any, files: any) => {
                    if (err) {
                        console.log(err);
                    }

                    const dPath        = require('path');
                    const { datapath } = fields;
                    const newpath      = dPath.resolve(basePath, '', `./${datapath}`);
                    if (!fs.existsSync(newpath)) {
                        shell.mkdir('-p', newpath);
                    }
                    fileList = getFiles(newpath);
                    for (const one of fileList) {
                        if (one.substring(one.length - 4, one.length) === 'meta') {
                            const text = fs.readFileSync(one, 'utf8');
                            const obj  = JSON.parse(text);
                            pdata.push(obj);
                        }
                    }

                    path                = '/';
                    ctx.session.appName = 'getfilelist';
                    ctx.response.type   = server.staticContent[path].mime;
                    return ctx.body = { result: true, data: pdata };
                });
            } else if ((m != null) && (ctx.url === '/filedownload/')) {
                form = new formidable.IncomingForm();
                return form.parse(ctx.req, (err: Error, fields: any, files: any) => {
                    if (err) {
                        console.log(err);
                    }

                    const { fpath } = fields;
                    const { fmime } = fields;
                    const filename  = fpath.substr((fpath.lastIndexOf('/')) + 1, fpath.length - 1);
                    console.log(filename);
                    path                 = '/';
                    ctx.session.appName = 'filedownload';
                    ctx.set('Content-disposition', `attachment; filename=${filename}`);
                    ctx.set('Content-type', fmime);
                    return ctx.body = fs.createReadStream(fpath);
                });
            } else {
                await next();
            }
        });
    }

    dynamicJavascript() {

        return this.app.use(async (ctx: any, next: any) => {

            let filename;
            const matchJS         = ctx.url.match(/js\/(.*)\.js/);
            const matchJSOriginal = ctx.url.match(/js\/(.*)\_original.js/);
            if (matchJSOriginal != null) {
                console.log('Dynamic Coffee', matchJSOriginal[1]);
                filename = await WebServerHelper.doFindFileInPath(matchJSOriginal[1] + '.coffee', ['../test/js/', '../test/js/test_data/']);
                console.log('Filename:', filename);

                const coffee = fs.readFileSync(filename).toString();
                ctx.set('Content-Length', coffee.length);
                ctx.response.type = 'application/vnd.api+json';
                ctx.body          = `var originalCode = \`${coffee}\``;
                return true;
            } else if (matchJS != null) {
                console.log('Dynamic JS', matchJS[1]);

                filename = await WebServerHelper.doFindFileInPath(matchJS[1] + '.coffee', ['../test/js/', '../test/js/test_data/']);
                console.log('Filename:', filename);

                const js: any = await WebServerHelper.doCompileCoffeeFile(filename);

                ctx.set('Content-Length', js.length);
                ctx.response.type = 'application/vnd.api+json';
                ctx.body          = js;
                return true;
            } else {
                await next();
            }
        });
    }

    screenMiddleware() {

        const reScreen = new RegExp('^/screens/(.*).js', 'i');
        const reView   = new RegExp('^/views/(.*).js', 'i');

        return this.app.use(async (ctx: any, next: any) => {
            let m = ctx.url.match(reScreen);
            if (m != null) {
                ctx.body          = await compileScreen(m[1], ctx.session.appName);
                ctx.response.type = 'application/javascript';
                return true;
            }

            m = ctx.url.match(reView);
            if (m != null) {
                ctx.body          = await compileView(m[1], ctx.session.appName);
                ctx.response.type = 'application/javascript';
                return true;
            }

            await next();
            return true;
        });
    }

    async rebuildNinjaFile(file: any) {

        const getClassInfo = (name: string) => {

            const m1 = this.ninjaCoffeeRaw[name].match(/class (.*) extends (.*)/);
            if (m1) {
                return { class: m1[1], extends: m1[2], name };
            }
            return null;
        };

        const getRequireInfo = (name: string) => {
            const m1 = this.ninjaCoffeeRaw[name].match(/require (.*)/);
            if (m1) {
                return { class: m1[1], useRequire: true, name };
            }
            return null;
        };


        if (/.coffee/.test(file)) {

            let name;
            const output: any = await WebServerHelper.doCompileCoffeeFile(file, true);

            this.ninjaCoffeeRaw[file]   = fs.readFileSync(file).toString();
            this.ninjaCoffeeFiles[file] = output.js;
            this.ninjaCoffeeMap[file]   = output.v3SourceMap;

            const info       = getClassInfo(file);
            const useRequire = getRequireInfo(file);
            if ((useRequire != null) && (useRequire.useRequire === false)) {
                return bundle.add(file);
            } else if ((info != null) && (info.extends != null)) {
                for (name of Array.from(this.ninjaCoffeeExtends)) {
                    if (name === file) {
                        return true;
                    }
                }
                return this.ninjaCoffeeExtends.push(info);
            } else {
                for (name of Array.from(this.ninjaCoffeeNormal)) {
                    if (name === file) {
                        return true;
                    }
                }
                return this.ninjaCoffeeNormal.push(file);
            }


        } else if (/.styl/.test(file)) {
            // console.log "Adding stylus file:", file
            const css = await WebServerHelper.doCompileStylusFile(file);
            return this.ninjaStylusFiles[file] = css;
        } else if (/.pug/.test(file)) {
            const html = await WebServerHelper.doCompilePugFile(file);
            // console.log "Adding Pug file:", file
            return this.ninjaPugFiles[file] = html;
        } else if (/\.png|\.jpg/.test(file)) {
            // console.log "Adding image file:", file
        } else {
            return false;
        }
    }

    rebuildNinjaSave() {

        let file;
        let str = '';
        for (const name of this.ninjaCoffeeNormal) {
            str += this.ninjaCoffeeFiles[name];
        }

        this.ninjaCoffeeExtends = this.ninjaCoffeeExtends.sort((a: any, b: any) => {
            if (a.name === b.name) {
                return 0;
            } else if (a.name < b.name) {
                return -1;
            } else {
                return 1;
            }
        });

        for (const info of this.ninjaCoffeeExtends) {
            console.log('Extends:', info);
            str += this.ninjaCoffeeFiles[info.name];
        }

        let strCss = '';
        for (file in this.ninjaStylusFiles) {
            const css = this.ninjaStylusFiles[file];
            strCss += css;
        }

        // --xg
        bundle.bundle(function(error: Error, result: any) {
            if (error != null) {
                throw error;
            }
            zlib.gzip(result + str, (_: any, contentJs: any) => {
                return ninjaJavascript = contentJs;
            });
            return saveNinjaFileAndExit(strCss, result.toString() + str);
        });

        zlib.gzip(strCss, (_: any, contentCss: any) => {
            return ninjaCss = contentCss;
        });

        for (file in this.ninjaPugFiles) {
            const html = this.ninjaPugFiles[file];
            console.log(`Error: what do we do with ${file}`);
        }

        return true;
    }

    async rebuildNinja() {
        //
        //  Scan all the source files in the Ninja folder
        //  Compile to ninja.src.coffee and compile that file to javascript
        //


        this.ninjaCoffeeExtends = [];
        this.ninjaCoffeeNormal  = [];
        this.ninjaCoffeeRaw     = {};
        this.ninjaCoffeeFiles   = {};
        this.ninjaCoffeeMap     = {};
        this.ninjaPugFiles      = {};
        this.ninjaStylusFiles   = {};

        const files = glob.sync('../src/**');
        for (let file of Array.from(files)) {
            await this.rebuildNinjaFile(file);
        }
        this.rebuildNinjaSave();

        return this.setWatchTimer('../src/.', (filename: string) => {
            filename = `../src/${filename}`;
            console.log(`rebuildNinja change: ${filename}`);
            return this.rebuildNinjaFile(filename)
                .then(() => {
                    console.log('Resaving');
                    return this.rebuildNinjaSave();
                });
        });
    }
}

export default NinjaWebServer;

//        @app.listen(9000)
//        console.log "Listening on port #{9000}"
