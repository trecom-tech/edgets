//
// class for age data type
//
// @extends [DataFormatterType]
//

import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';
import { Moment }            from 'moment';

const numeral   = require('numeral');
const moment    = require('moment');
const common    = new DataFormatCommon();
const flatpickr = require('flatpickr');

function __guard__(value: any, transform: any) {
    return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}

export class DataFormatDateAge extends DataFormatterType {
    name: string        = 'age';
    width: number       = 135;
    input_width: number = 135;
    align: string       = 'right';
    elEditor: any;
    datePicker: any;
    editorShowing: boolean;
    elDataEditor: any;


    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to open editor with flatpickr
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                    type : "text",
                    class: "dynamic_edit"
                }
            );

            this.appendEditor();
            this.elEditor.on('keydown', (e: Error) => {
                // if editor is closed then close datepicker
                if (!this.editorShowing) {
                    return this.datePicker.close();
                }
            });
        }

        this.attachEditor(this.elEditor, path);

        this.elEditor.css({
            position : "absolute",
            "z-index": 5001,
            top,
            left,
            width,
            height
        });

        this.elEditor.val(currentValue);

        this.elEditor.show();
        return this.elEditor.focus();
    }

    // #|
    // #|  Age editor picks a date only

    attachEditor(elSourceField: any, path: any) {
        super.attachEditor(elSourceField, path);

        // #|
        // #|  Create 3rd party date picker control
        if ((this.datePicker == null)) {
            this.datePicker = flatpickr(elSourceField[0], {
                    maxDate   : new Date(),
                    allowInput: true,
                    dateFormat: "Y-m-d",
                    parseDate(dateStr: string) {
                        const mom = common.getMoment(dateStr, 'MM/DD/YYYY HH:mm:ss');
                        if (mom != null) { return mom.toDate(); }
                        return null;
                    },
                    onChange: (dateObj: any, dateStr: string) => {
                        if ((dateObj != null) && dateObj.length) {
                            this.saveValue(dateObj);
                            elSourceField.val(this.format(dateObj[0], {}, path));
                            return elSourceField.change();
                        }
                    },
                    onOpen: (dateObj: any, dateStr: any, instance: any) => {
                        const time = __guard__(common.getMoment(this.unformat(elSourceField.val(), path)), (x: Moment) => x.format('MM/DD/YYYY HH:mm:ss'));
                        if (time != null) { return instance.setDate(time); }
                    },
                    onClose: (dateObj: any, dateStr: any, instance: any) => {
                        if ((dateObj != null) && dateObj.length) {
                            const date = dateObj[0];
                            const time = __guard__(common.getMoment(date), (x: Moment) => x.format('MM/DD/YYYY HH:mm:ss'));
                            if (time != null) { instance.setDate(date); }
                        }
                        this.editorShowing = false;
                        if (this.elEditor != null) {
                            return this.elEditor.hide();
                        }
                    },
                    enableTime: true,
                    time_24hr : true
                }
            );

            this.elDataEditor = this.datePicker.calendarContainer;
        }

        // #|
        // #|  Attach any keyboard event in the text editor to open the date picker
        elSourceField.on('keydown', (e: Error) => {
            // if editor is closed then close datepicker
            if (!this.editorShowing) {
                return this.datePicker.close();
            }
        });

        return true;
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        const m = common.getMoment(data, "MM/DD/YYYY");
        if ((m == null)) {
            return "";
        }

        let html = `<span class='fdate'>${m.format("MM/DD/YYYY")}</span>`;
        let age  = moment().diff(m);
        age      = age / 86400000;

        if (age < 401) {
            age = numeral(age).format("#") + " d";
        } else if (age < (365 * 2)) {
            age = numeral(age / 30.5).format("#") + " mn";
        } else {
            age = numeral(age / 365).format("#.#") + " yrs";
        }

        html += `<span class='fage'>${age}</span>`;
        return html;
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        const format = "YYYY-MM-DD";
        const m      = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        const dateStr = m.format(format);
        return new Date(dateStr);
    }

    getValueInTooltip(value: any) {
        const format = "YYYY-MM-DD";
        const m      = common.getMoment(value, format);
        if (!m) {
            return "";
        }
        return m.format(format);
    }
}

export default DataFormatDateAge;