
class ViewDisplayObject extends View

    onShowScreen: ()=>

        @TableDescription = {}
        @TableName        = {}

    drawPart : (prefix, obj, ignorePart, extraClassName, level, alreadyFlagged, basePath) =>

        html       = ""
        currentRow = 0

        # console.log "drawPart('#{prefix}', obj, '#{ignorePart}', '#{extraClassName}')"

        if obj._global and !alreadyFlagged
            html += "<tr><td colspan=2 class='global'> <i class='fa fa-flag'></i> Information is global and not store specific </td></tr>"
            alreadyFlagged = true

        for loopCount in [0, 1]

            names = Object.keys(obj)
            for i in names.sort()

                o = obj[i]
                if !o? then continue

                if i == "_global" then continue
                if i == "_lastModified" then continue
                if loopCount == 0 and typeof o == "object" then continue
                if loopCount == 1 and typeof o != "object" then continue

                partialName = i
                indent = ""

                for x in [0...level]
                    indent += "<i class='fa fa-fw'></i> "

                if typeof o == "object" and Array.isArray(o) and o.length == 1

                    html += "<tr class='#{extraClassName} #{classCurrent}'>
                            <td> #{indent} #{i} </td>"

                    html += "<td data-path='#{basePath}/#{prefix}#{i}' data-type='text' class='type_arrayitem'> #{o[0]} </td></tr>"

                else if typeof o == "object"

                    html += "<tr class='topFolder'><td colspan='2'> #{indent}
                       <i class='fa fa-fw fa-arrow-circle-down'></i> #{i} </td></tr>"

                    html += @drawPart(prefix + i + "/", o, ignorePart, extraClassName, level + 1, alreadyFlagged, basePath)

                else

                    reDate1 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T00.00.00.000Z/
                    reDate2 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9]Z/

                    #
                    # Draw Prefix
                    displayKey = i
                    classCurrent = ""
                    if currentRow == 0 then classCurrent = "datasetStart"
                    currentRow++

                    # addItemDataElement(basePath + "/" + prefix + i, o)

                    html += "<tr class='#{extraClassName} #{classCurrent}'>
                            <td> #{indent} #{displayKey} </td>"

                    if typeof o == "number"
                        html += "<td class='type_number' data-path='#{basePath}/#{prefix}#{i}' data-type='number'> #{o} </td></tr>"
                    else if typeof o == "boolean"
                        html += "<td class='type_bool' data-path='#{basePath}/#{prefix}#{i}' data-type='number'> #{o} </td></tr>"
                    else if typeof o == "string" and o.match reDate1
                        if /190[01]-01/.test(o) or /^000[01]/.test(o) or /^9999/.test(o)
                            html += "<td class='type_date invalid_date'>Invalid Date</td></tr>"
                        else
                            stamp = GlobalValueManager.DateFormat GlobalValueManager.GetMoment(o)
                            html += "<td class='type_date' data-path='#{basePath}/#{prefix}#{i}' data-type='date'> #{stamp} </td></tr>"
                            # html += "<td>|#{o}|</td></tr>"
                    else if typeof o == "string" and o.match reDate2
                        if /190[01]-01/.test(o) or /^000[01]/.test(o) or /^9999/.test(o)
                            html += "<td class='type_date invalid_date'>Invalid Date</td></tr>"
                        else
                            stamp = GlobalValueManager.DateTimeFormat GlobalValueManager.GetMoment(o)
                            html += "<td class='type_date' data-path='#{basePath}/#{prefix}#{i}' data-type='datetime'> #{stamp} </td></tr>"
                    else
                        html += "<td data-path='#{basePath}/#{prefix}#{i}' data-type='text'> #{o} </td></tr>"

        return html

    drawDataSection : (basePath, tableName, obj) =>

        @base        = new WidgetBase()
        isGlobal     = ""
        html         = ""
        stamp        = ""
        age_modified = ""

        if obj._lastModified?
            stamp        = GlobalValueManager.GetMoment(obj._lastModified)
            age_modified = GlobalValueManager.DateTimeFormat(stamp)

        if @TableDescription[tableName]
            html += "<div class='itemTableName'> " + @TableDescription[tableName] + "</div>"
        else if @TableName[tableName]?
            html += "<div class='itemTableName'> " + @TableName[tableName] + "</div>"

        html += "<table class='itemDumpInfo'>"
        html += "<tr class='itemDumpImported #{isGlobal}'>
            <th colspan='2'> <a name='Import#{tableName}' /> Path #{basePath}/#{tableName} </a>
            <span class='pull-right' data-path='#{basePath}/#{tableName}/_lastModified' data-type='datetime'>#{age_modified}</span></th>
            </tr>"

        html += @drawPart tableName + "/", obj, tableName, isGlobal, 0, false, basePath

        html += "</table>";

        @elHolder.html html

