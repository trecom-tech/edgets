class ViewCodeEditor extends View

    getDependencyList: ()=>
        return [ "/vendor/ace/ace.js", "/vendor/ace/ext-language_tools.js" ]

    ## -------------------------------------------------------------------------------------------------------------
    ## constructor to create new editor
    ##
    onShowScreen: () =>
        @editorWidget = @addDiv "editor", "editor_" + GlobalValueManager.NextGlobalID()
        @codeEditor = new CodeEditor @editorWidget.getTag()
        @applyCodeEditorSettings()
        @isDirty = false

        ##| brian - setup events for the view
        GlobalClassTools.addEventManager(this)

        @codeEditor.getSession().on 'change', ()=>
            currentClean = @codeEditor.getSession().getUndoManager().isClean();
            if currentClean != @isDirty
                @isDirty = currentClean
                @emitEvent "dirty_change", [@codeEditor, @isDirty]

            @emitEvent "change", [@codeEditor]

    ##
    ## Handle resize event
    onResize: (w, h) =>
        session = @codeEditor?.getInstance()
        if session? and session.resize?
            session.resize()

    ##
    ## Just a fake function to make all the code lines using this function still work
    showEditor: () => 
        true


    ## -------------------------------------------------------------------------------------------------------------
    ## function to get the created editor instance
    ##
    ## @return [CodeEditor] codeEditor
    ##
    getEditorInstance: ->
        return @codeEditor

    getSession: ()=>
        return @codeEditor.getSession()

    ## -------------------------------------------------------------------------------------------------------------
    ## clears the html of the editor that is used to remove the editor
    ##
    clear: ->
        @editorWidget.html ""

    ## -gao
    ## set editor's settings
    ##

    applyCodeEditorSettings: (@codeMode, @content, @theme, @popupMode) =>
        if !@codeEditor? then return false
        if @codeMode then @codeEditor.setMode @codeMode
        if @content then @codeEditor.setContent @content
        if @theme then @codeEditor.setTheme @theme
        if @popupMode? and typeof @popupMode is "boolean" then @codeEditor.popupMode @popupMode
        this

    setTheme: (@theme)=>
        if @codeEditor? and @theme then @codeEditor.setTheme @theme
        this

    setMode: (@codeMode)=>
        if @codeEditor? and @codeMode then @codeEditor.setMode @codeMode
        this

    setPopupMode: (@popupMode)=>
        if @codeEditor? and @popupMode? and typeof @popupMode is "boolean" then @codeEditor.popupMode @popupMode
        this

    setOptions: (@_options)=>
        if @codeEditor? and @_options then @codeEditor.setOptions @_options
        this

    setContent: (val) =>

        if !val
            @content = ''
        else if typeof val isnt 'string'
            @content = val.toString()
        else
            @content = val
        if @codeEditor? and @content then @codeEditor.setContent @content
        this

    getContent: ()=>
        @codeEditor.getContent()

    setHotKey: (name, bindKey, execFunc, readOnly)=>
        if !name? then return false

        if typeof name is "object"
            @hotkeyoptions = name
        else
            @hotkeyoptions = 
                name:     name
                bindKey:  bindKey
                execFunc: execFunc
                readOnly: readOnly

        if @codeEditor?
            @codeEditor.setHotKey @hotkeyoptions.name, @hotkeyoptions.bindKey, @hotkeyoptions.execFunc, @hotkeyoptions.readOnly
        true

    serialize: () =>
        return 
            content:       @content
            popupmode:     @popupMode
            options:       @_options
            mode:          @codeMode
            theme:         @theme
            hotkeyoptions: @hotkeyoptions

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? then reject("Invalid data passed.")

            if options.content?
                @setContent options.content

            if options.popupmode?
                @setPopupMode options.popupmode 

            if options.options?
                @setOptions options.options

            if options.mode?
                @setMode options.mode 

            if options.theme?
                @setTheme options.theme

            if options.hotkeyoptions?
                @setHotKey options.hotkeyoptions

            resolve true
