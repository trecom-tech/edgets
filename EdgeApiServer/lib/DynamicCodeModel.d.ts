import { TableBase } from 'edgecommontablebase';
import { DynamicProvider } from './DynamicProvider';
export default class DynamicCodeModel extends TableBase<any> {
    id: any;
    dirty: boolean;
    provider: DynamicProvider;
    data: any;
    doSave: () => Promise<number | true>;
    constructor(id: any);
    setBranch(newBranch: any): any;
    setCategory(newCategory: any): any;
    setName(newName: any): any;
    setCoffeescriptCode(newCode: any): boolean;
    setNotes(newNotes: any): boolean;
    setStatus(newStatus: any): any;
    addTestCase(options: any): boolean;
    removeTestCase(id: any): boolean;
    updateTestCase(id: any, options: any): boolean;
    getProvider(): DynamicProvider;
    internalCompile(): boolean;
    save(): boolean | Promise<number | true>;
}
