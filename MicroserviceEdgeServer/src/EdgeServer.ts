// General configuration and error handlers
import config from 'edgecommonconfig';
config.setTitle("Edge Server");

// Worker thread
import Worker from './Worker';

// Start the worker
const worker = new Worker(process.pid);

export default worker;