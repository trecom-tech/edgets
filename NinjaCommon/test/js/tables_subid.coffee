$ ->
	dataset1 =
		[
			{
				"code": "00501",
				"city": "Holtsville",
				"state": "NY",
				"county": "SUFFOLK",
				"area_code": "12345",
				"lat": "40.922326",
				"lon": "-72.637078"
			},
			{
				"code": "00544",
				"city": "Holtsville",
				"state": "NY",
				"county": "SUFFOLK",
				"area_code": "",
				"lat": "40.922326",
				"lon": "-72.637078"
			},
			{
				"code": "01001",
				"city": "Agawam",
				"state": "MA",
				"county": "HAMPDEN",
				"area_code": "",
				"lat": "42.140549",
				"lon": "-72.788661"
			},
			{
				"code": "01002",
				"city": "Amherst",
				"state": "MA",
				"county": "HAMPSHIRE",
				"area_code": "",
				"lat": "42.367092",
				"lon": "-72.464571"
			},
			{
				"code": "01003",
				"city": "Amherst",
				"state": "MA",
				"county": "HAMPSHIRE",
				"area_code": "",
				"lat": "42.369562",
				"lon": "-72.63599"
			},
			{
				"code": "01004",
				"city": "Amherst",
				"state": "MA",
				"county": "HAMPSHIRE",
				"area_code": "",
				"lat": "42.384494",
				"lon": "-72.513183"
			},
			{
				"code": "01005",
				"city": "Barre",
				"state": "MA",
				"county": "WORCESTER",
				"area_code": "",
				"lat": "42.32916",
				"lon": "-72.139465"
			},
			{
				"code": "01007",
				"city": "Belchertown",
				"state": "MA",
				"county": "HAMPSHIRE",
				"area_code": "",
				"lat": "42.280267",
				"lon": "-72.402056"
			},
			{
				"code": "01008",
				"city": "Blandford",
				"state": "MA",
				"county": "HAMPDEN",
				"area_code": "",
				"lat": "42.177833",
				"lon": "-72.958359"
			}
		]
	dataset2 = [
		{
			"code": "01028",
			"city": "East Longmeadow",
			"state": "MA",
			"county": "HAMPDEN",
			"area_code": "",
			"lat": "42.062009",
			"lon": "-72.49874"
		},
		{
			"code": "01029",
			"city": "East Otis",
			"state": "MA",
			"county": "BERKSHIRE",
			"area_code": "",
			"lat": "42.190904",
			"lon": "-73.051661"
		},
		{
			"code": "01030",
			"city": "Feeding Hills",
			"state": "MA",
			"county": "HAMPDEN",
			"area_code": "",
			"lat": "42.189335",
			"lon": "-72.79774"
		},
		{
			"code": "01031",
			"city": "Gilbertville",
			"state": "MA",
			"county": "WORCESTER",
			"area_code": "",
			"lat": "42.352554",
			"lon": "-72.205724"
		},
		{
			"code": "01032",
			"city": "Goshen",
			"state": "MA",
			"county": "HAMPSHIRE",
			"area_code": "",
			"lat": "42.443837",
			"lon": "-72.819446"
		},
		{
			"code": "01033",
			"city": "Granby",
			"state": "MA",
			"county": "HAMPSHIRE",
			"area_code": "",
			"lat": "42.262285",
			"lon": "-72.504086"
		},
		{
			"code": "01034",
			"city": "Granville",
			"state": "MA",
			"county": "HAMPDEN",
			"area_code": "",
			"lat": "42.112748",
			"lon": "-72.952003"
		},
		{
			"code": "01035",
			"city": "Hadley",
			"state": "MA",
			"county": "HAMPSHIRE",
			"area_code": "",
			"lat": "42.356804",
			"lon": "-72.576613"
		},
		{
			"code": "01036",
			"city": "Hampden",
			"state": "MA",
			"county": "HAMPDEN",
			"area_code": "",
			"lat": "42.067614",
			"lon": "-72.417507"
		},
		{
			"code": "01037",
			"city": "Hardwick",
			"state": "MA",
			"county": "WORCESTER",
			"area_code": "",
			"lat": "42.347856",
			"lon": "-72.225251"
		}
	]

	addTestButton "Tables with Sub IDs", "Open", () ->
		DataMap.removeTableData "zipcode;1"		
		DataMap.removeTableData "zipcode;2"		
		id = 0
		for rec in dataset1
			DataMap.addDataUpdateTable "zipcode;1", ++id, rec	

		id = 0
		for rec in dataset2
			DataMap.addDataUpdateTable "zipcode;2", ++id, rec	
		
		setTimeout ()=>
			doPopupView "Table", "table: zipcode;1", "table_zipcode_1", 800, 600, (view)=>
				view.addTable "zipcode;1"
		, 1000
		setTimeout ()=>
			doPopupView "Table", "table: zipcode;2", "table_zipcode_2", 800, 600, (view)=>
				view.addTable "zipcode;2"
		, 1000
		true

	addTestButton "Add items to both tables", "Open", () ->
		DataMap.removeTableData "zipcode;1"		
		DataMap.removeTableData "zipcode;2"	
		doPopupView "Table", "table: zipcode;1", "table_zipcode_1", 500, 300, (view)=>
			view.addTable "zipcode;1"

		doPopupView "Table", "table: zipcode;2", "table_zipcode_2", 500, 300, (view)=>
			view.addTable "zipcode;2"

		id1 = 0
		id2 = 0
		DataMap.addDataUpdateTable "zipcode;1", ++id1,
			"code": "01037",
			"city": "Hardwick",
			"state": "MA",
			"county": "WORCESTER",
			"area_code": "",
			"lat": "42.347856",
			"lon": "-72.225251"	
			

		DataMap.addDataUpdateTable "zipcode;2", ++id2,
			"code": "01029",
			"city": "East Otis",
			"state": "MA",
			"county": "BERKSHIRE",
			"area_code": "",
			"lat": "42.190904",
			"lon": "-73.051661"
		true		

	addTestButton "Add items to both Detailed tables", "Open", () ->
		DataMap.removeTableData "zipcode;1"		
		DataMap.removeTableData "zipcode;2"	
		doPopupView "Table", "table: zipcode;1", "table_zipcode_1", 500, 300, (view)=>
			view.setDetailed()
			view.addTable "zipcode;1"

		doPopupView "Table", "table: zipcode;2", "table_zipcode_2", 500, 300, (view)=>
			view.setDetailed()
			view.addTable "zipcode;2"

		id1 = 0
		id2 = 0
		DataMap.addDataUpdateTable "zipcode;1", ++id1,
			"code": "01037",
			"city": "Hardwick",
			"state": "MA",
			"county": "WORCESTER",
			"area_code": "",
			"lat": "42.347856",
			"lon": "-72.225251"	
			

		DataMap.addDataUpdateTable "zipcode;2", ++id2,
			"code": "01029",
			"city": "East Otis",
			"state": "MA",
			"county": "BERKSHIRE",
			"area_code": "",
			"lat": "42.190904",
			"lon": "-73.051661"
		true	

	addTestButton "Tables with Sub IDs in Tabs", "Open", () =>
		DataMap.removeTableData "zipcode;1"		
		DataMap.removeTableData "zipcode;2"	
		addHolder().setView "DynamicTabs", (view) =>
			view.doAddTableTab "zipcode;1", "Zipcode - First"
			view.doAddTableTab "zipcode;2", "Zipcode - Second"
			true

		id1 = 0
		for rec in dataset1
			DataMap.addDataUpdateTable "zipcode;1", ++id1, rec	

		id2 = 0
		for rec in dataset2
			DataMap.addDataUpdateTable "zipcode;2", ++id2, rec	

	addTestButton "Export/Import Data Types", "Open", () ->
		DataMap.removeTableData "zipcode;1"
		DataMap.removeTableData "zipcode;2"
		id = 0
		for rec in dataset1
			DataMap.addDataUpdateTable "zipcode;1", ++id, rec

		id = 0
		for rec in dataset2
			DataMap.addDataUpdateTable "zipcode;2", ++id, rec

		dataTypes = null
		setTimeout ()=>
			doPopupView "Table", "table: zipcode;1", "table_zipcode_1", 800, 600, (view)=>
				view.addTable "zipcode;1"
				dataTypes = DataMap.exportDataTypes("zipcode;1")
				dataTypes.lat.type = "int"
		, 1000
		setTimeout ()=>
			doPopupView "Table", "table: zipcode;2", "table_zipcode_2", 800, 600, (view)=>
				view.addTable "zipcode;2"
				console.log "Zipcode data types = ", dataTypes
				DataMap.importDataTypes "zipcode;2", dataTypes
		, 2000
		true

	go()