class ViewTestLayout5 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        percent: 40
        top: "TestShowSize"
        bottom:
            view: "Docked"
            main: "TestShowSize"
            docked: "TestShowSize"
            location: "left"
            size: 200






