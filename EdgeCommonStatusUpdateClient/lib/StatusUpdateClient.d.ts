declare class StatusUpdateJob {
    client: StatusUpdateClient;
    lastWasZero: any;
    data: any;
    constructor(client: StatusUpdateClient, name: string, options?: any);
    stop(): boolean;
}
export default class StatusUpdateClient {
    name: string;
    defaultType: any;
    gid: number;
    openPromise: any;
    lastWasZero: any;
    timeseriesData: any;
    changemqConnection: any;
    messageExchange: any;
    useTimeSeriesData: boolean;
    static setGlobalStatusToFile(val: boolean): boolean;
    constructor(name?: string, defaultType?: any);
    doOpenChangeMessageQueue(): any;
    startJob(name: string, args: any, slot: any): StatusUpdateJob;
    logTimeseriesUpdate(data: any): boolean;
    sendStatus(data: any): boolean;
}
export {};
