"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonstatusupdateclient_1 = require("edgecommonstatusupdateclient");
const edgecommontimeseriesdata_1 = require("edgecommontimeseriesdata");
const edgecommonlauncher_1 = require("edgecommonlauncher");
const { argv } = require('yargs');
const jsonfile = require('jsonfile');
const os = require('os');
const { spawn } = require('child_process');
let portNumber = 50100;
const serverDetail = {};
let statusUpdateClient;
class AppRunner {
    constructor(runnerConfig) {
        this.config = runnerConfig;
        this.log = edgecommonconfig_1.config.getLogger('AppLauncher');
        this.myPort = ++portNumber;
        const pathParts = __dirname.split('/');
        pathParts.pop();
        this.basePath = pathParts.join('/') + '/';
        this.homePath = process.env.HOME;
    }
    runJob() {
        return new Promise((resolve, reject) => {
            this.log.info('Starting app', {
                config: this.config,
                port: this.myPort
            });
            try {
                const fullOutput = '';
                const startTime = new Date().getTime();
                //#|  Spawn the job
                const envCopy = Object.assign({}, process.env, { TERM: 'HTML', HOME: this.homePath, LANG: 'en_US.UTF-8', LOGNAME: process.env.USER, SHELL: '/bin/bash', SHLVL: '1', TMPDIR: '/tmp/' });
                const commandLine = this.config.cmd;
                const path = this.basePath + this.config.cwd;
                // console.log 'SPAWN PATH:', path
                // console.log 'HOME PATH :', @homePath
                const jobExec = spawn(commandLine, this.config.args, {
                    cwd: path,
                    env: envCopy
                });
                jobExec.on('error', (err) => {
                    console.log(`-> AppLauncher in command=${commandLine} args=`, this.config.args);
                    console.log('-> Path  : ', path);
                    console.log('-> Error : ', err);
                    this.log.error('AppLaunch Error', {
                        job: this.config,
                        commandLine,
                        args: this.config.args,
                        err
                    });
                    return resolve(false);
                });
                const colorRemove = new RegExp('\x1b\[[0-9;]*m', 'g');
                statusUpdateClient.sendStatus({
                    type: 'appstart',
                    name: this.config.title,
                    host: serverDetail.host,
                    load: serverDetail.load_Avg,
                    cmd: this.config.cmd,
                    args: this.config.args,
                    pid: jobExec.pid,
                    port: this.myPort
                });
                jobExec.stdout.on('data', (data) => {
                    let str = jobExec.pid + ' ' + this.config.title + '> ';
                    while (str.length < 30) {
                        str += ' ';
                    }
                    str += data.toString();
                    process.stdout.write(str);
                    return true;
                });
                jobExec.stderr.on('data', (data) => {
                    console.log(this.config.title + ' [err]> ' + data.toString());
                    return true;
                });
                return jobExec.on('close', (code) => {
                    this.log.info('App complete', {
                        config: this.config,
                        port: this.myPort
                    });
                    statusUpdateClient.sendStatus({
                        type: 'append',
                        name: this.config.title,
                        host: serverDetail.host,
                        pid: jobExec.pid,
                        port: this.myPort
                    });
                    return resolve(true);
                });
            }
            catch (e) {
                this.log.info('Exception', {
                    serverDetail,
                    e
                });
                edgecommonconfig_1.config.reportException('Spawn issue:', e);
                return resolve({ error: 'Exception in job', e });
            }
        });
    }
    /**
     * Returns once the module has been fulling installed and updated
     */
    doVerifyLoaded() {
        return __awaiter(this, void 0, void 0, function* () {
            if (argv.fast != null) {
                return true;
            }
            if (this.config.npm != null) {
                console.log('Installing NPM ', this.config.npm);
                edgecommonconfig_1.config.status('AppRunner doVerifyLoaded - Installing NPM ', this.config.npm);
                yield edgecommonlauncher_1.NpmModuleTools.verifyInstalled(this.config.npm, this.config.cwd);
            }
            return true;
        });
    }
    runLoop() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this.runJob();
            edgecommonconfig_1.config.status(`Module ${this.config.title} complete, updating install`);
            yield this.doVerifyLoaded();
            edgecommonconfig_1.config.status(`Module ${this.config.title} complete, waiting 20 seconds`);
            setTimeout(this.runLoop, 20000);
            return true;
        });
    }
}
class AppLauncher {
    constructor() {
        this.log = edgecommonconfig_1.config.getLogger('AppLauncher');
        serverDetail.host = os.hostname();
        serverDetail.total_mem = Math.ceil(os.totalmem());
        setInterval(this.refreshStatus, 5000);
        const timeSeriesData = new edgecommontimeseriesdata_1.EdgeTimeseriesData();
        timeSeriesData.doCreateDatabase('server_status')
            .then(db => {
            return this.timeSeriesDatabase = db;
        });
    }
    refreshStatus() {
        serverDetail.free_mem = Math.ceil(os.freemem());
        serverDetail.load_Avg = os.loadavg();
        if ((this.timeSeriesDatabase != null) && (this.timeSeriesDatabase.doSaveMeasurement != null)) {
            this.timeSeriesDatabase.doSaveMeasurement('status', { host: serverDetail.host }, {
                total_mem: serverDetail.total_mem,
                load: serverDetail.load_Avg[0]
            });
        }
        return true;
    }
    /**
     * Takes in the list of jobs.
     * Makes sure each is loaded / downloaded
     * Returns an array of the AppRunners
     *
     * @param jobs
     */
    doLoadAllApps(jobs) {
        return __awaiter(this, void 0, void 0, function* () {
            const allApps = [];
            for (let job of Array.from(jobs)) {
                const app = new AppRunner(job);
                yield app.doVerifyLoaded();
                allApps.push(app);
            }
            return allApps;
        });
    }
    start() {
        this.log.info('AppLauncher Starting', { serverDetail });
        statusUpdateClient = new edgecommonstatusupdateclient_1.StatusUpdateClient();
        return statusUpdateClient.doOpenChangeMessageQueue()
            .then(() => {
            let filename;
            console.log('Starting AppLauncher on host ', serverDetail.host);
            try {
                let dirs = ['.', '..', os.homedir() + '/EdgeConfig/'];
                if (process.env.NODE_ENV === 'test') {
                    dirs = ['./test/'];
                }
                filename = 'AppLauncherConfig-#{serverDetail.host}.json';
                let file = edgecommonconfig_1.config.FindFileInPath(filename, dirs);
                if (!file) {
                    filename = 'AppLauncherConfig.json';
                    file = edgecommonconfig_1.config.FindFileInPath(filename, dirs);
                }
                const jobs = jsonfile.readFileSync(file, 'utf-8');
                return this.doLoadAllApps(jobs)
                    .then((allApps) => {
                    return Array.from(allApps).map((app) => app.runLoop());
                });
            }
            catch (e) {
                console.error(`There is no config file named ${filename} or ppLauncherConfig-${serverDetail.host}.json in directory ~/EdgeConfig/`);
                this.log.info('AppLauncher start error', { e });
                return process.exit();
            }
        });
    }
}
const runner = new AppLauncher();
runner.start();
//# sourceMappingURL=AppLauncher.js.map