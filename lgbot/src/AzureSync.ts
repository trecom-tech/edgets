import * as fs      from 'fs';
import * as azure   from 'azure-storage';
import { config }   from 'edgecommonconfig';

// Globals
const GlobalShareName  = process.env.AZURESHARE || 'edgedatalogs';
const GlobalShareCreds = (process.env.NODE_ENV !== 'test') ? 
    process.env.AZURECONN || config.getCredentials('azureLogs').connection :
    'DefaultEndpointsProtocol=https;AccountName=obviotest;AccountKey=IRfy3COidoce4u7W3L6dQ1bI+XrSRVPjsGsIa46Fs+JxM8nOTuRt8escpcFIDWGnOCDrqV56FPXPGmA7qyCrWA==;EndpointSuffix=core.windows.net';

/**
 * This class uses Azure file storage to access files
 */
export default class AzureSync {

    fileService:    any;
    share:          any;
    currentToken:   any;
    interval:       any;
    files:          any[];
    directory:      string;

    constructor() {
        this.fileService  = azure.createFileService(GlobalShareCreds);
        this.share        = GlobalShareName;
        this.currentToken = null;
        this.interval     = 1000 * 60;
        this.files        = [];
        this.directory    = '';
    }

    private sync(directory: string) {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory);
        }

        this.fileService.listFilesAndDirectoriesSegmented(this.share, directory, this.currentToken, async (err: any, result: any) => {
            if (err) {
                throw err;
            }

            for (let file of result.entires.files) {
                let fileExists = false;

                this.files.forEach(async (oldFile, i, array) => {
                    if (file.name === oldFile.name && file.contentLength === oldFile.contentLength) {
                        fileExists = true;
                    }

                    if (file.name === oldFile.name && file.contentLength !== oldFile.contentLength) {
                        array[i] = file;
                        await this.downloadFile(directory, file.name, directory, file.name);
                        fileExists = true;
                    }
                });

                if (!fileExists) {
                    this.files.push(file);
                    await this.downloadFile(directory, file.name, directory, file.name);
                }
            }
        });
    }

    listFiles(directory: string) {
        return new Promise((resolve, reject) => {
            this.currentToken = null;
            let output: any[] = [];

            const searchAttempt = (token: any) => {
                this.fileService.listFilesAndDirectoriesSegmented(this.share, directory, token, (err: any, result: any) => {
                    if (err) {
                        console.log('ERR:', err);
                        reject(err);
                    }

                    if (result.entries.files) {
                        for (let item of result.entries.files) {
                            output.push({
                                name: item.name,
                                type: 'f',
                                size: parseInt(item.contentLength, 10)
                            });
                        }
                    }

                    if (result.entries.directories) {
                        for (let item of result.entries.directories) {
                            output.push({
                                name: item.name,
                                type: 'd'
                            });
                        }
                    }
                    
                    if (result.continuationToken && result.continuationToken.nextMarker) {
                        searchAttempt(result.continuationToken);
                    } else {
                        resolve(output);
                    }
                });
            };

            searchAttempt(this.currentToken);
        });
    }

    downloadFile(directory: string, filename: string, targetDirectory: string, targetFilename: string) {
        return new Promise((resolve, reject) => {
            if (!fs.existsSync(targetDirectory)) {
                fs.mkdirSync(targetDirectory);
            }

            this.fileService.getFileToStream(this.share, 
                directory, 
                filename, 
                fs.createWriteStream(`${targetDirectory}/${targetFilename}`), 
                (err: any, result: any) => {
                
                if (err) {
                    reject(err);
                }

                resolve(result);
            });
        });
    }

    syncFiles(directory: string) {
        this.directory = directory;

        if (!fs.existsSync(this.directory)) {
            fs.mkdirSync(this.directory);
        }

        const dir = fs.readdirSync(this.directory);

        for (let name of dir) {
            const contentLength = fs.statSync(`${this.directory}/${name}`).size;
            const oldFile = {
                name: name,
                contentLength: `${contentLength}`
            }
            this.files.push(oldFile);
        }

        this.sync(directory);

        setInterval(() => {
            this.sync(directory);
        }, this.interval);

    }

}