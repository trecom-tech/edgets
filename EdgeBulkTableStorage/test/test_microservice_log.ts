import { config }       from 'edgecommonconfig';
import BulkTableStorage from '../src/BulkTableStorage';

const testRecord1 = {
    id        : 'v2import2770614267962680000_0',
    record_id : '4311169',
    stamp     : new Date(),
    wq        : 'v2-import',
    host      : 'bpollack-d2fd0b',
    proctime  : 519.9916,
    result    : 'No changes'
};

config.setCredentials("azureTest", {connection: "DefaultEndpointsProtocol=https;AccountName=obviotest;AccountKey=IRfy3COidoce4u7W3L6dQ1bI+XrSRVPjsGsIa46Fs+JxM8nOTuRt8escpcFIDWGnOCDrqV56FPXPGmA7qyCrWA==;EndpointSuffix=core.windows.net"});

const ts = new BulkTableStorage("azureLogs", "microservice");
ts.doInsertLogRecord("v2import2770614267962680000_0", testRecord1).
then((result: boolean) => {
    console.log("Insert result:", result);

    ts.doQuery(ts.createQuery("id", "'v2import2770614267962680000_0'", 10))
    .then((result) => {
        console.log("Result 1=", result);
    })
    .catch(() => {/**/});
})
.catch(() => {/**/});