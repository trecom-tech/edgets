##|
##|  Options:
##|  sizes = [ percent1, percent2 ] or sizes = percent1
##|  direction = vertical or horizontal
##|  gutterSize = 6 by default
##|

class ViewSplittable extends View

	constructor   : () ->
		super()

		@optionData = {}
		@optionData.direction  = "vertical"
		@optionData.gutterSize = 6
		@optionData.size       = 50

	getMinWidth: ()=>
		##|
		##|  The minimum width is the total of the minimums
		total = 0
		min1 = @first.getMinWidth() || 0
		min2 = @second.getMinWidth() || 0
		if min1? and typeof min1 == "number" then total += min1
		if min2? and typeof min2 == "number" then total += min2
		total += @gutterSize
		return total

	getMinHeight: ()=>
		##|
		##|  The minimum width is the total of the minimums
		total = 0
		min1 = @first.getMinHeight() || 0
		min2 = @second.getMinHeight() || 0
		if min1? and typeof min1 == "number" then total += min1
		if min2? and typeof min2 == "number" then total += min2
		total += @gutterSize
		return total

	onResize: (w, h)=>
		super(w, h);

		if globalDebugResize
			console.log "ViewSplittable onResize #{w}, #{h}"

		##|
		##|  Parent container has changed size.  The parent is now w x h
		if @wdtSplittable?
			@wdtSplittable.setSize(w, h)

		true

	getPercent: ()=>
		return @optionData.size

	##|
	##|  Set's the split percent which is p1 and 100-p1 assumed
	setPercent: (p1)=>
		if p1 <= 1.0 and p1 >= 0 then p1 *= 100
		@setSizeAndLocalStorage p1
		@resetSize()
		true

	calculateSizes: ()=>

		w = @width()
		h = @height()

		##|
		##|  If not showing, do nothing yet
		if w == 0 or h == 0 then return

		p1 = @optionData.size / 100.0
		if p1 < 0 then p1 = 0
		if p1 > 1 then p1 = 1

		if p1 is 0 or p1 is 1 
			@gutterSize = 0
		else 
			@gutterSize = @optionData.gutterSize

		if @optionData.direction == "vertical"

			##|  vertical split
			space = w - @gutterSize
			@size1 = space * p1
			@size2 = space - @size1

			minWidth = @first.getMinWidth() || 0
			if @size1 < minWidth then @size1 = minWidth
			@size2 = space - @size1

			minWidth = @second.getMinWidth() || 0
			if @size2 < minWidth then @size2 = minWidth
			@size1 = space - @size2

			@gutter.move @size1, 0, @gutterSize, h
			@setSizeAndLocalStorage 100*(@size1/space)

		else

			space = h - @gutterSize
			@size1 = space * p1
			@size2 = space - @size1

			if @first.getMinHeight?
				minHeight = @first.getMinHeight()
				if @size1 < minHeight then @size1 = minHeight
				@size2 = space - @size1

			if @second.getMinHeight?
				minHeight = @second.getMinHeight()
				if @size2 < minHeight then @size2 = minHeight
				@size1 = space - @size2

			@gutter.move 0, @size1, w, @gutterSize
			@setSizeAndLocalStorage 100*(@size1/space)

		# console.log "calculateSizes w=#{w}, h=#{h} size1=#{@size1}, size2=#{@size2}"

		true

	setSize: (w, h)=>

		if globalDebugResize
			console.log "ViewSplittable setSize #{w}, #{h}", @optionData

		super(w, h)

		@setupSplitter()
		@calculateSizes()

		if @optionData.direction == "vertical"

			@gutter.addClass "vertical"
			@gutter.removeClass "horizontal"

			if globalDebugResize
				console.log "ViewSplittable Vertical sizes=#{@size1}, #{@size2}"

			@first.move 0, 0, @size1, h
			@gutter.move @size1, 0, @gutterSize, h
			@second.move @size1+@gutterSize, 0, @size2, h

		else

			@gutter.removeClass "vertical"
			@gutter.addClass "horizontal"

			if globalDebugResize
				console.log "ViewSplittable Horizontal sizes=#{@size1}, #{@size2}"

			@first.move 0, 0, w, @size1
			@gutter.move 0, @size1, w, @gutterSize
			@second.move 0, @size1+@gutterSize, w, @size2

		true

	onDragGutter: (deltaX, deltaY, e)=>
		w = @width()
		h = @height()

		if @optionData.direction == "vertical"
			newSize1 = @startingSize1 + deltaX
			@optionData.size = 100*(newSize1/(@width()-@gutterSize))
		else
			newSize1 = @startingSize1 + deltaY
			@optionData.size = 100*(newSize1/(@height()-@gutterSize))

		@calculateSizes()
		true

	onDragGutterComplete: (deltaX, deltaY, e)=>
		@gutter.removeClass "dragging"

		##|
		##| Figure out the new percentages
		if @optionData.direction == "vertical"
			newSize1 = @startingSize1 + deltaX
			@optionData.size = 100 * (newSize1 / (@width() - @gutterSize))
		else
			newSize1 = @startingSize1 + deltaY
			@optionData.size = 100 * (newSize1 / (@height() - @gutterSize))

		if globalDebugResize
			console.log "ViewSplittable onDragGutterComplete new percent=", @optionData.size, " newSize=", newSize1

		@resetSize()
		true

	##|
	##|  Setup the actual splitter window
	setupSplitter: ()=>

		if @first? and @second? then return true

		@first  = @addDiv "splitterPartLeft"
		@gutter = @addDiv "splitterGutter"
		@second = @addDiv "splitterPartRight"

		@gutter.on "mousedown", (e)=>
			@gutter.addClass "dragging"
			@minWidth1  = @first.getMinWidth() || 0
			@minWidth2  = @second.getMinWidth() || 0
			@maxWidth1  = @first.getMaxWidth()
			@maxWidth2  = @second.getMaxWidth()
			@minHeight1 = @first.getMinHeight() || 0
			@minHeight2 = @second.getMinHeight() || 0
			@maxHeight1 = @first.getMaxHeight()
			@maxHeight2 = @second.getMaxHeight()

			@startingSize1 = @size1
			@startingSize2 = @size2

			GlobalMouseDrag.startDrag(e, @onDragGutter, @onDragGutterComplete)
			@setupMouseScrollListener true
			true

		$(document).on "mouseup", (e) =>
			@setupMouseScrollListener false

		true

	setData: (options) =>

		if options?
			$.extend @optionData, options, true

		@setupSplitter()

		true

	show: (name, size) =>

		@setData()
		if size?
			if Array.isArray(size)
				@optionData.size = size[0]
			else
				@optionData.size = size

		if name?
			@gid = name
		else
			@gid = GlobalValueManager.NextGlobalID()
	
		true

	setHorizontal: ()=>

		@optionData.direction = "horizontal"

		##|
		##| if already showing, force a resize / redraw
		if @gutter?
			@resetSize()

		return true

	getFirst: ()=>
		return @first

	getSecond: ()=>
		return @second

	getWidget: () =>
		return @gutter

	## -gao
	## set ConfigName to save/restore sizes to localStorage
	setConfigName: (strName) =>
		if !strName or strName.length < 1
			return false
		@optionData.configName = "splitter_#{strName}"
		if configPercent = localStorage.getItem @optionData.configName
			@optionData.size = configPercent
			@resetSize configPercent

	## -gao
	## function to set size of view as well as value in localStorage
	setSizeAndLocalStorage: (val) =>
		@optionData.size = val
		if @optionData.configName?
			localStorage.setItem @optionData.configName, val

	## -gao
	## set up listeners for Mouse Scroll events
	setupMouseScrollListener: (bool) =>	
		#if @gutter.classes.indexOf("dragging") < 0 then return false	
		if !@cbMouseWheel
			@cbMouseWheel = (e) =>	
				e.preventDefault()
				e.stopPropagation()
				if e.originalEvent.deltaMode == e.originalEvent.DOM_DELTA_LINE
					deltaX = e.originalEvent.deltaX * -5
					deltaY = e.originalEvent.deltaY * -5
				else
					deltaX = e.originalEvent.deltaX * -1
					deltaY = e.originalEvent.deltaY * -1

				scrollX = Math.ceil(Math.abs(deltaX)/2)
				scrollY = Math.ceil(Math.abs(deltaY)/2)

				if Math.abs(deltaX) < 10 then scrollX = 0
				if Math.abs(deltaY) < 10 then scrollY = 0
				if scrollY is 0 then return false
				if e.originalEvent.wheelDelta > 0
					scrollY *= -1
				w = @width()
				h = @height()
				@startingSize1 += scrollY
				if @optionData.direction == "vertical"
					@optionData.size = 100*(@startingSize1/(@width()-@gutterSize))
				else
					@optionData.size = 100*(@startingSize1/(@height()-@gutterSize))

				@calculateSizes()

				true
		if !@cbDOMMouseScroll
			@cbDOMMouseScroll = (e) =>
				e.preventDefault()
				e.stopPropagation()
				scrollX = 0
				scrollY = Math.ceil(Math.abs(e.detail * 20))
				if e.originalEvent.detail < 0
					scrollY *= -1

				if scrollY is 0 then return false
				w = @width()
				h = @height()
				@startingSize1 += scrollY
				if @optionData.direction == "vertical"
					@optionData.size = 100*(@startingSize1/(@width()-@gutterSize))
				else
					@optionData.size = 100*(@startingSize1/(@height()-@gutterSize))

				@calculateSizes()
				true

		if bool
			@el.on "mousewheel", @cbMouseWheel
			@el.on "DOMMouseScroll", @cbDOMMouseScroll
		else
			@el.off "mousewheel", @cbMouseWheel
			@el.off "DOMMouseScroll", @cbDOMMouseScroll

		true

	##
	## Handle child views when they are trying to show/hide themselves
	onChildVisibility: (child, visibility) =>
		if visibility is "show"
			if child.size?
				@setPercent child.size
				child.el.show()

		else if visibility is "hide"
			percent = @getPercent()
			## only one child can be hidden at once
			if percent is 0 or percent is 100 then return false
			
			child.size = percent
			child.el.hide()
			if child is @getFirst() 
				@setPercent 0
			else
				@setPercent 100
		true

	setMinWidth: (key, w) =>
		if !key? or !w? or !(w > 0) then return false
		if !@optionData[key]?
			@optionData[key] = {}
		@optionData[key].minwidth = w
		
		switch key
			when 'l', 'left', 'v1', 'view1'
				@getFirst().setMinWidth w
				break
			when 'r', 'right', 'v2', 'view2'
				@getSecond().setMinWidth w
				break
			else return false
		true

	setMinHeight: (key, h) =>
		if !key? or !h? or !(h > 0) then return false
		if !@optionData[key]?
			@optionData[key] = {}
		@optionData[key].minheight = h
		
		switch key
			when 't', 'top', 'v1', 'view1'
				@getFirst().setMinHeight h
				break
			when 'b', 'bot', 'bottom', 'v2', 'view2'
				@getSecond().setMinHeight h
				break
			else return false
		true

	setHtml: (key, html) =>
		if !key? or !html? then return false
		if !@optionData[key]?
			@optionData[key] = {}
		@optionData[key].html = html
		
		switch key
			when 'l', 'left', 't', 'top', 'v1', 'view1'
				@getFirst().html html
				break
			when 'r', 'right', 'b', 'bot', 'bottom', 'v2', 'view2'
				@getSecond().html html
				break
			else return false
		true

	setViewOptions: (key, obj) =>
		if !key? or !obj? then return false
		if !@optionData? then @optionData = {}
		@optionData[key] = obj
		true

	serialize: () =>
		options =
			size: @optionData.size
			configname: @optionData.configName?.substr(9)
		return $.extend {}, options, @optionData

	##|
	##|  Autolayout helper class -- called after the view is created, before returning to the main caller.
	##|  Caller can set left/right or top/bottom or view1/view2/direction
	##|  Caller can set percent or size
	##|
	deserialize: (options)=>

		newPromise ()=>

			##|
			##|  The options can specify a percent as "percent" or a size as "size"
			if options.percent? and typeof options.percent == "number"
				@setPercent(options.percent)

			##|
			##|  TODO:  This should be actual size, not percentage
			if options.size? and typeof options.size == "number"
				@setPercent(options.size)

			##|
			##|  Shortcut allows the caller to use options.bot instead of bottom
			if options.bot? and !options.bottom?
				options.bottom = options.bot

			if options.configname?
				@setConfigName(options.configname)

			##|
			##|   The options can specifier two view configurations
			##|   as either left/right or top/down
			##|
			if options.left? and options.right?
				if options.left.minwidth? then @setMinWidth 'left', options.left.minwidth
				if options.right.minwidth? then @setMinWidth 'right', options.right.minwidth

				if options.left.html? then @setHtml 'left', options.left.html
				if options.right.html? then @setHtml 'right', options.right.html

				if options.left.view? then @setViewOptions 'left', options.left
				if options.right.view? then @setViewOptions 'right', options.right

				yield Promise.all [ @getFirst().internalProcessLayout(options.left), @getSecond().internalProcessLayout(options.right)]
			
			else if options.top? and options.bottom?
				@setHorizontal()
				if options.top.minheight? then @setMinHeight 'top', options.top.minheight
				if options.bottom.minheight? then @setMinHeight 'bottom', options.bottom.minheight

				if options.top.html? then @setHtml 'top', options.top.html
				if options.bottom.html? then @setHtml 'bottom', options.bottom.html

				if options.top.view? then @setViewOptions 'top', options.top
				if options.bottom.view? then @setViewOptions 'bottom', options.bottom

				yield Promise.all [ @getFirst().internalProcessLayout(options.top), @getSecond().internalProcessLayout(options.bottom)]
			
			else if @options.view1? and options.view2?
				if options.direction == "h" or options.direction == "horizontal"
					@setHorizontal()
				if options.view1.minwidth? then @getFirst().setMinWidth(options.view1.minwidth)
				if options.view2.minwidth? then @getSecond().setMinWidth(options.view2.minwidth)
				if options.view1.minheight? then @getFirst().setMinWidth(options.view1.minheight)
				if options.view2.minheight? then @getSecond().setMinWidth(options.view2.minheight)

				@setViewOptions 'view1', options.view1
				@setViewOptions 'view2', options.view2

				yield Promise.all [ @getFirst().internalProcessLayout(options.view1), @getSecond().internalProcessLayout(options.view2)]

			@internalCopyVariables(@getFirst())
			@internalCopyVariables(@getSecond())

			return true
