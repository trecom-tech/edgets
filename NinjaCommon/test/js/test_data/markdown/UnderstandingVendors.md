# Understanding Vendors

* Ace (Ajax.org Cloud9 Editor)

  Ace is a standalone code editor written in JavaScript.
  [[Official Website]](https://ace.c9.io/)  [[Github Repo]](https://github.com/ajaxorg/ace/)

* Full Calendar

  A full-sized drag & drop JavaScript event calendar.

  FullCalendar is great for displaying events, but it isn't a complete solution for event content-management.
  [[Official Website]](https://fullcalendar.io/) [[Github Repo]](https://github.com/fullcalendar/fullcalendar)

* Gridstack.js

  `gridstack.js` is a jQuery plugin for widget layout.

  This is drag-and-drop multi-column grid. It allows you to build draggable responsive bootstrap v3 friendly layouts.
  [[Official Website]](http://gridstackjs.com/)  [[Github Repo]](https://github.com/gridstack/gridstack.js)

* JointJS

  JointJS is a JavaScript diagramming library.
  It can be used to create either static diagrams or, and more importantly, fully interactive diagramming tools and application builders.
  [[Official Docs]](https://resources.jointjs.com/docs/jointjs/v2.2/joint.html) [[Github Repo]](https://github.com/clientIO/joint)

* MapboxGL

  Mapbox GL JS is a JavaScript library for interactive, customizable vector maps on the web.

  It takes map styles that conform to the Mapbox Style Specification, applies them to vector tiles that conform to the Mapbox Vector Tile Specification, and renders them using WebGL.
  [[Official Docs]](https://docs.mapbox.com/mapbox-gl-js/overview/) [[Github Repo]](https://github.com/mapbox/mapbox-gl-js)

* PDF.js

  A general-purpose, web standards-based platform for parsing and rendering PDFs.
  [[Official Website]](https://mozilla.github.io/pdf.js/) [[Github Repo]](https://github.com/mozilla/pdf.js)

* Summernote.js

  Summernote is a JavaScript library that helps you create WYSIWYG editors online.
  [[Official Website]](https://summernote.org/) [[Github Repo]](https://github.com/summernote/summernote/)

* FileSaver.js

  FileSaver.js is the solution to saving files on the client-side, and is perfect for web apps that generates files on the client.
  [[Github Repo]](https://github.com/eligrey/FileSaver.js/)

* Xlsx.js

  Parser and writer for various spreadsheet formats.
  [[Github Repo]](https://github.com/SheetJS/js-xlsx)

* Backbone.js

  Backbone.js gives structure to web applications by providing models with key-value binding and custom events, collections with a rich API of enumerable functions, views with declarative event handling, and connects it all to your existing API over a RESTful JSON interface.
  [[Official Docs]](https://backbonejs.org/) [[Github Repo]](http://github.com/jashkenas/backbone/)

* BootStrap Toggle

  Bootstrap Toggle is a highly flexible Bootstrap plugin that converts checkboxes into toggles.
  [[Official Docs]](http://www.bootstraptoggle.com/) [[Github Repo]](https://github.com/minhur/bootstrap-toggle/)

* CanvasJS

  CanvasJS is an easy to use HTML5 and Javascript Charting library.

  It runs across devices including iPhone, iPad, Android, Windows Phone, Microsoft Surface, Desktops, etc. This allows you to create Rich Dashboards that work across devices without compromising on Maintainability or Functionality.
  [[Official Docs]](https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/)

* D3.js

  D3.js is a JavaScript library for manipulating documents based on data.

  D3 helps you bring data to life using HTML, SVG, and CSS. D3’s emphasis on web standards gives you the full capabilities of modern browsers without tying yourself to a proprietary framework, combining powerful visualization components and a data-driven approach to DOM manipulation.
  [[Official Website]](https://d3js.org/) [[Github Repo]](https://github.com/d3/d3)

* Epoch.js

  Epoch is a general purpose charting library for application developers and visualization designers. 

  It focuses on two different aspects of visualization programming: basic charts for creating historical reports, and real-time charts for displaying frequently updating timeseries data.
  [[Official Docs]](https://epochjs.github.io/epoch/getting-started/) [[Github Repo]](https://github.com/epochjs/epoch)

* Flatpickr

  `flatpickr` is a lightweight and powerful datetime picker.

  Lean, UX-driven, and extensible, yet it doesn’t depend on any libraries. There’s minimal UI but many themes. Rich, exposed APIs and event system make it suitable for any environment.
  [[Official Docs]](https://flatpickr.js.org/) [[Github Repo]](https://github.com/flatpickr/flatpickr)

* HTML Sortable

  Lightweight vanillajs micro-library for creating sortable lists and grids using native HTML5 drag and drop API.
  [[Official Examples]](http://lukasoppermann.github.io/html5sortable/index.html) [[Github Repo]](https://github.com/lukasoppermann/html5sortable)

* jQuery Multi Select

  A user-friendlier drop-in replacement for the standard select with multiple attribute activated.
  [[Official Docs]](http://loudev.com/) [[Github Repo]](https://github.com/lou/multi-select/)

* jQuery Range

  A jQuery Plugin to create Range Selector
  [[Official Docs]](https://nitinhayaran.github.io/jRange/demo/) [[Github Repo]](https://github.com/nitinhayaran/jRange)

* Lodash

  A modern JavaScript utility library delivering modularity, performance & extras.
  [[Official Docs]](https://lodash.com/docs/) [[Github Repo]](https://github.com/lodash/lodash/)

* Markdown-it

  Markdown parser: 100% CommonMark support, extensions, syntax plugins & high speed
  [[Official Demos]](https://markdown-it.github.io/) [[Github Repo]](https://github.com/markdown-it/markdown-it)

* Miller.js

  An implementation of Miller columns with jQuery.
  [[Official Demos]](http://mgbx.net/demomiller) [[Github Repo]](https://github.com/mageekguy/jquery.miller.js)

* Mousetrap.js

  Mousetrap is a simple library for handling keyboard shortcuts in Javascript.
  [[Official Docs]](https://craig.is/killing/mice) [[Github Repo]](https://github.com/ccampbell/mousetrap)

* Prism.js

  Prism is a lightweight, extensible syntax highlighter, built with modern web standards in mind. 

  It’s used in thousands of websites, including some of those you visit daily.
  [[Official Docs]](https://prismjs.com/#examples) [[Github Repo]](https://github.com/PrismJS/prism)

* Selectize.js

  Selectize is the hybrid of a textbox and `<select>` box. It's jQuery-based and it's useful for tagging, contact lists, country selectors, and so on.
  [[Official Docs]](https://selectize.github.io/selectize.js/) [[Github Repo]](https://github.com/selectize/selectize.js/)

* Sha256.js

  A small SHA-256 implementation for JavaScript
  [[Github Repo]](https://github.com/geraintluff/sha256)

* Draggabilly

  Draggabilly makes it easy to drag and drop various elements on a web page.
  [[Official Docs]](https://draggabilly.desandro.com/) [[Github Repo]](http://github.com/desandro/draggabilly)

* EvEmitter

  EvEmitter adds publish/subscribe pattern to a browser class. 

  It's a smaller version of Olical/EventEmitter. That EventEmitter is full featured, widely used, and great. This EvEmitter has just the base event functionality to power the event API in libraries like Isotope, Flickity, Masonry, and imagesLoaded.
  [[Github Repo]](https://github.com/metafizzy/ev-emitter#readme)

* Handlerbars

  Handlebars provides the power necessary to let you build semantic templates effectively with no frustration.
  [[Official Docs]](https://handlebarsjs.com/) [[Github Repo]](https://github.com/wycats/handlebars.js/)

* iScroll

  iScroll is a high performance, small footprint, dependency free, multi-platform javascript scroller.
  [[Github Repo]](https://github.com/cubiq/iscroll)

* Moment.js

  A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.
  [[Official Docs]](https://momentjs.com/docs/) [[Github Repo]](https://github.com/moment/moment/)

* Numeral.js

  A javascript library for formatting and manipulating numbers.
  [[Official Docs]](http://numeraljs.com/) [[Github Repo]](https://github.com/adamwdraper/Numeral-js)
