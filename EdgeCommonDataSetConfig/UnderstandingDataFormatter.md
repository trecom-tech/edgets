# Data Formatting
> TODO  Write this document better to explain the DataFormatter

Examples from Gao

    dataSetConfig = require('edgecommondatasetconfig')
    dataFormatter = new  dataSetConfig.DataFormatter()

    ## now we can use data formatters defined in DataFormatTypes.coffee
    dataFormatterText = dataFormatter.getFormatter('text')
    formattedText = dataFormatterText.format([a,b,c])
    formattedText <- 'a, b, c'

    dataFormatterMemo = dataFormatter.getFormatter('memo')
    formattedMemo = dataFormatterMemo.format('this is text for memo')
    formattedMemo <- '<span class=\'memo\'>this is text for memo</span><span class=\'fieldicon\'><i class=\'si si-eyeglasses\'></i></span>'