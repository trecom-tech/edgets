$ ->
    ##|
    ##|  This is just for diagnostics,  you don't need to verify the data map is
    ##|  loaded normally.  The data types should be loaded upon startup.
    addTest "Confirm Zipcodes datatype loaded", () ->
        dm = DataMap.getDataMap()
        if !dm? then return false

        zipcodes = dm.types["zipcode"]
        if !zipcodes? then return false
        if !zipcodes.col["code"]? then return false

        true

    ##|
    ##|  Load the zipcodes JSON file.
    ##|  This will insert the zipcodes into the global data map.
    addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

    addTestButton "Checkboxes", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.setEnableCheckboxes(true, 5)

    addTestButton "Flags", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.addFlag
                name: "Flag 1"
                icon: "fa fa-book"
            view.addFlag
                name: "Flag 2"
                icon: "fa fa-ban"
            view.setEnableFlags(true, 0)
            view.setEnableCheckboxes(true, 5)
        true

    addTestButton "Flags with timer update", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.addFlag
                name: "Flag 1"
                icon: "fa fa-book"
            view.addFlag
                name: "Flag 2"
                icon: "fa fa-ban"
            view.setEnableFlags(true, 0)
            view.setEnableCheckboxes(true, 5)

            setTimeout ()->
                console.log "Updating flagged row for 01001"
                DataMap.updatePathValue "zipcode", "row_flagged", "01001", "Flag 1"
                DataMap.updatePathValue "zipcode", "city", "01001", "Flag 1"
            , 1000

            setTimeout ()->
                console.log "Updating flagged row for 01020"
                DataMap.updatePathValue "zipcode", "row_flagged", "01020", "Flag 1"
                DataMap.updatePathValue "zipcode", "city", "01020", "Flag 1"
            , 2000

            setTimeout ()->
                console.log "Updating flagged row for 01040"
                DataMap.updatePathValue "zipcode", "row_selected", "01040", true
                DataMap.updatePathValue "zipcode", "city", "01040", "Flag 3"
            , 3000

            setTimeout ()->
                console.log "Updating flagged row for 01040"
                DataMap.updatePathValue "zipcode", "row_selected", "01040", false
                DataMap.updatePathValue "zipcode", "city", "01040", "Flag 3b"
            , 5000

            setTimeout ()->
                console.log "Updating stuff that should fail"
                DataMap.updatePathValue "zipcode", "garbage_field", "01040", false
                DataMap.updatePathValue "garbage_table", "city", "01040", "Flag 3b"
            , 6000

        true

    go()