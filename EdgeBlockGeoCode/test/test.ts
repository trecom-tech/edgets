import GeoCode    from '../src/GeoCode';
import { expect } from 'chai';

//
//  Turn off the api so that results are not using the cache
GeoCode.useEdgeApi = false;

const testAddress1 = {
    display: '293 Montibello Dr, Mooresville, NC 28117',
    number : 239,
    street : 'Montibello Dr',
    city   : 'Mooresville',
    state  : 'NC'
};

//  Create a geocoder
const geocode = new GeoCode();

describe('GeoCode block', () => {
    it('test doGetCensus method', async () => {
        const result: any = await geocode.doGetCensus(testAddress1);
        console.log('From Census for normalized address:', result);

        expect(result.source).to.be.equal('CensusData');
        expect(result.confidence).to.be.equal(100);
        expect(result.address).to.be.an('object');
    });

    it('test doGetGeocodio method', async () => {
        const result: any = await geocode.doGetGeocodio(testAddress1);
        console.log('From Census for normalized address:', result);

        expect(result.source).to.be.equal('GeoCodio');
        expect(result.confidence).to.be.equal(10);
        expect(result.address).to.be.an('object');
    });

    it('test doGetMapbox method', async () => {
        const result: any = await geocode.doGetMapbox(testAddress1);
        console.log('From Census for normalized address:', result);

        expect(result.source).to.be.equal('Mapbox');
        expect(result.confidence).to.be.equal(1);
        expect(result.address).to.be.an('object');
    });

    it('test doGetOpenCage method', async () => {
        const result: any = await geocode.doGetOpenCage(testAddress1);
        console.log('From Census for normalized address:', result);

        expect(result.source).to.be.equal('OpenCage');
        expect(result.confidence).to.be.equal(10);
        expect(result.address).to.be.an('object');
    });

    it('test doGetMapboxReverse method', async () => {
        const result: any = await geocode.doGetMapboxReverse(35.58838, -80.8884);
        console.log('From Census for normalized address:', result);

        expect(result.source).to.be.equal('Mapbox');
        expect(result.confidence).to.be.equal(1);
        expect(result.address).to.be.an('object');
    });

    it('test main process method should return processed address', async () => {
        const result: any = await geocode.process(testAddress1);
        console.log('From Census for normalized address:', result);

        expect(result.address).to.be.an('object');
    });

    it('test main process method should return error', async () => {
        const result: any = await geocode.process();
        expect(result.error).to.be.a('string');
    });

    it('test main process method should return error', async () => {
        const result: any = await geocode.process();
        expect(result.error).to.be.a('string');
    });
});
