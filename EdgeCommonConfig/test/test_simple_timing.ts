//
//   This is a test to check the timing on the startup cost for the config library
//

const numeral = require('numeral');
const chalk   = require('chalk');

//
// Placeholder for config
let config: any = null;

const timeFunction = function (name: string, callback: any) {
    const startTime = process.hrtime();
    callback();
    const diff = process.hrtime(startTime);
    const ns   = (diff[0] * 1e9) + diff[1];
    const ms   = ns / 1e6;

    console.log(`[${chalk.yellow(name)}] Total time=`, chalk.cyan(numeral(ms).format("#,###.###")), " ms");
    return true;
};

try {

    timeFunction("Loading Config", () => config = require('../lib/Config'));

    timeFunction("Logging record", () => config.error("This is an error message"));

} catch (e) {

    console.log("Exception:", e);
}
