import * as DataSetConfig from '../src';
const should = require('should');

const { Column } = DataSetConfig;

describe("Testing for Column class", function() {

    it("should be created Column from template", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };

        const c = new Column();

        c.deserialize(testColumnData1);

        c.getName().should.equal(testColumnData1.name);
        c.getType().should.equal(testColumnData1.type);
        c.getWidth().should.equal(testColumnData1.width);
        c.getEditable().should.equal(testColumnData1.editable);
        c.getVisible().should.equal(testColumnData1.visible);
        c.getSource().should.equal(testColumnData1.source);
        c.getAutoSize().should.equal(testColumnData1.autosize);
    });

    it("should be getting template from serialize method", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();
        c.deserialize(testColumnData1);

        const dest = c.serialize();

        dest.name.should.equal(c.getName());
        dest.type.should.equal(c.getType());
        dest.width.should.equal(c.getWidth());
        dest.editable.should.equal(c.getEditable());
        dest.visible.should.equal(c.getVisible());
        dest.source.should.equal(c.getSource());
        dest.autosize.should.equal(c.getAutoSize());
    });

    it("should be colorFunction", function() {        
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();
        c.deserialize(testColumnData1);

        return c.getHasColorFunction().should.equal(true);
    });

    // d
    it("should not be existed for colorFunction", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true
        };

        const c = new Column();
        c.deserialize(testColumnData1);

        c.getHasColorFunction().should.equal(false);
    });

    // e
    it.skip("should be converted exactly text into function for colorFunction or renderFunction", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`,
            render  : () => { 
                return console.log('render function');
            }
        };
                
            
        const c = new Column();
        c.deserialize(testColumnData1);

        const dest = c.serialize();

        c.getColorFunction().should.be.type("function");
        dest.cellColor.should.be.type("string");
    });

    // f
    it("should be worked with several types", function() {
        const testColumnData_text = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const testColumnData_number: any = {
            align: "right",
            autosize: false,
            clickable: false,
            displayFormat: null,
            editable: false,
            hideable: true,
            name: "ID",
            options: null,
            order: 0,
            required: false,
            source: "id",
            system: false,
            tooltip: "",
            type: "int",
            visible: true,
            width: 80
        };

        const testColumnData_money: any = {
            align: "right",
            autosize: false,
            calculate: false,
            clickable: false,
            editable: false,
            hideable: false,
            name: "Rehab Price",
            options: null,
            order: 60,
            render: null,
            required: false,
            source: "REHAB_PRICE",
            system: false,
            type: "money",
            visible: true,
            width: 90
        };

        const testColumnData_duration: any = {
            align: "right",
            autosize: true,
            clickable: false,
            displayFormat: null,
            editable: false,
            hideable: false,
            name: "Duration",
            options: null,
            order: 5,
            required: false,
            source: "duration",
            system: false,
            tooltip: "",
            type: "duration",
            visible: true,
            width: 80
        };


        const c_number = new Column();
        c_number.deserialize(testColumnData_number);

        const c_text = new Column();
        c_text.deserialize(testColumnData_text);

        const c_money = new Column();
        c_money.deserialize(testColumnData_money);

        const c_duration = new Column();
        c_duration.deserialize(testColumnData_duration);

        c_number.getFormatter().format(20, '#.##').should.equal('20.00');
        c_text.getFormatter().format('abcdefg').should.equal('abcdefg');
        c_money.getFormatter().format(321).should.equal('$ 321');
        c_duration.getFormatter().format(360000).should.equal('6 min, 0 sec.');
    });

    // g
    it("should be worked to change column by changecolumn method", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();
        c.deserialize(testColumnData1);

        c.changeColumn("source", "state_name");

        const dest = c.serialize();

        c.getSource().should.equal("state_name");
        return dest.source.should.equal("state_name");
    });

    it("should be worked to change render function by changeColumn method", function() {
        const testColumnData1 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();
        c.deserialize(testColumnData1);

        const testColumnData2 = { 
            name    : "country",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("Blue");\
`
        };
        c.deserialize(testColumnData2);

        c.getColorFunction()().should.equal('Blue');
    });
});



