class ScreenLogin extends Screen

    windowTitle      : "Login"
    windowSubTitle   : "Login to the Edge system"

    cbRemember: true

    ##|
    ##| onSetupButtons is called the first time the screen is created and only called once
    onSetupButtons: ()=>

    doConnect: ()=>

        newPromise ()=>

            yield globalBusyDialog.waitFor "Connecting to server", sock.doConnect()

            @version = yield sock.os_doGetServerInformation()
            console.log "TableDefinitionLoader doConnect Version=", @version
            @version.version_date = moment(@version.version_date).format("dddd, MMMM Do YYYY, h:mm:ss a")
            return true

    doAuthenticate: (txtUsername, txtPassword)=>

        newPromise ()=>

            result = yield globalBusyDialog.waitFor "Authenticating", sock.auth_doAuth(txtUsername, txtPassword)
            if !result? or result.error?
                return result.error

            ##|
            ##|  User is authenticated
            sock.user = result
            return null


    ##|
    ##| Called when the screen is shown to the user.
    onShowScreen : () =>
        @loginInfoWindow.hide()
        @doConnect()
        .then ()=>
            delete @loginBusy
            @btnLogin.on "click", @onDoLogin
            @checkedBox.on "click", () =>
                @cbRemember = !@cbRemember
                if @cbRemember
                    @checkedBox.html "<i class='fa fa-check-circle' />"
                else
                    @checkedBox.html "<i class='far fa-circle' />"

            @txtUsername.focus()
            # @loginRememberMe.attr("checked", "on");

            oldUsername = localStorage.lastUsernameRTC
            oldPassword = localStorage.lastPasswordRTC

            if !oldUsername?
                oldUsername = ""

            if !oldPassword?
                oldPassword = ""

            # oldUsername = "brianpollack"

            @txtUsername.val(oldUsername)
            @txtPassword.val(oldPassword)
            true

    setupUserEvents: ()=>

        #
        # Global events from NinjaCommon based UI controls
        #
        
        DataMap.getDataMap().on "table_change", (tableName, config)->
            console.log "ScreenLogin on DataMap.table_change tableName=#{tableName} config=", config
            true

        globalTableEvents.on "table_change", (tableName, source, field, newValue)=>
            console.log "ScreenLogin on globalTableEvents.table_change tableName=#{tableName} config=", config
            return

    preloadRequiredData: ()=>

        ##|
        ##|  Possible options
        ##|  /AppName/                       = No specific screen, show main menu
        ##|  /AppName#ScreenName             = Show a given screen name
        ##|  /AppName#ScreenName/Param/Param = Show a given screen, pass array of options
        ##|

        screenToShow = "MainMenu"
        params       = []

        if document.location.hash? and document.location.hash.length > 1
            params = document.location.hash.split("/")
            screenToShow = params.shift()
            screenToShow = screenToShow.replace("#", "")

        if !screenToShow? or screenToShow == '' or screenToShow.length == 0 then screenToshow = "MainMenu"
        # console.log "Open screen '#{screenToShow}' #{screenToShow.length} with ", params
        showScreen screenToShow, params
        true

    ##|
    ##| Make a login request to the server.
    onDoLogin: (e) =>

        if @loginBusy? and @loginBusy then return
        @loginBusy = true

        @btnLogin.hide()
        @loginInfoWindow.hide()
        @usernameLabel.removeClass "error"
        @passwordLabel.removeClass "error"

        txtUsername = @txtUsername.val()
        txtPassword = @txtPassword.val()

        ##| Save the login/password to the local storage if remember me checkbox.
        if @cbRemember
            localStorage.lastUsernameRTC = txtUsername
            localStorage.lastPasswordRTC = txtPassword
        else
            localStorage.lastUsernameRTC = ""
            localStorage.lastPasswordRTC = ""

        @loginInfoWindow.show()

        @doAuthenticate(txtUsername, txtPassword)
        .then (result)=>

            delete @loginBusy
            if result != null
                console.log "Login error:", result
                @loginInfoWindow.show()
                @usernameLabel.addClass "error"
                @passwordLabel.addClass "error"
                @btnLogin.show()
                return

            @preloadRequiredData()

