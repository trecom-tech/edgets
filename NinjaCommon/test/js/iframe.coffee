$ ->

	addTestButton "Basic IFrame View", "Open", ()->
		addHolder().setView "IFrame", (view)->
			view.setSource "http://localhost:9000"

		true

	addTestButton "IFrame on Popup", "Open", () ->
		doPopupView "IFrame", "IFrame on Popup", "iframe-popup1", 800, 600, (view) ->
			view.setSource "http://localhost:9000/index.html"
		true

	addTestButton "IFrame in Tab", "Open", () ->
		addHolder().setView "DynamicTabs", (tabs)->
			tabs.doAddViewTab "IFrame", "IFrameViewTab", (view)->
				view.setSource "http://localhost:9000"

			tabs.addTab "EmptyTab", 'Another tab'
		true

	addTestButton "Serialize/deserialize IFrame", "Open", ()->
		addHolder().setView "IFrame", (view)->
			view.deserialize
				src: "http://localhost:9000"
			setTimeout () ->
				doPopupView "IFrame", "Serialized IFrame", "iframe-popup2", 800, 600, (viewPopup) ->
					viewPopup.deserialize view.serialize()
			, 2000
		true

	go()		