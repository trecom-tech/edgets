import EdgeApi            from 'edgeapi';
import { expect }         from 'chai';
import ProviderData       from '../src/ProviderData';
import { addServiceUser } from '../src/utility/AddServiceUser';

const jsonfile       = require('jsonfile');
const path           = require('path');

let dummyUser = {
    streamingListNumber: 999,
    send               : (cmd: string, obj: any) => {
        console.log('send cmd=', cmd, 'obj=', obj);
        return true;
    },
    // Send this back to the client and they know to expect data
    // then use sendPush to send the actual data.
    startStreamingList : () => {
        if (!dummyUser.streamingListNumber) {
            dummyUser.streamingListNumber = 999;
        }

        dummyUser.streamingListNumber++;
        console.log('startStreamingList id=', dummyUser.streamingListNumber);
        return `STREAM:${dummyUser.streamingListNumber}`;
    },

    // Terminate the streaming list by sending a special command
    // to the receiver
    endStreamingList: (stream_id: any) => {
        console.log(`endStreamingList id=${stream_id}`);
        return true;
    },

    // Generic push notification message
    sendPush: (cmd: string, obj: any) => {
        console.log(`sendPush cmd=${cmd} obj=`, obj);
        return true;
    },

    // Push message but delayed to avoid sending too many at once
    sendPushDelayed: (cmd: string, obj: any) => {
        console.log(`sendPushDelayed cmd=${cmd} obj=`, obj);
        return true;
    },
};


describe('ProviderData', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();

    const testUser1 = {
        id: 'tao',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'user1@protovate.com',
        partner_id: 1,
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
        },
        'attrib': {
            'lunch': 'Pizza',
        },
    };


    before(async () => {
        api = new EdgeApi();
        await api.doConnect();

        // Removing all data before tests
        await api.data_doRemove('geo_test', 'zipcodes');
        await api.data_doRemove('lookup', 'table');
        await api.data_doRemove('rets_raw', 'raw');
        await api.data_doRemove('master', 'user');

        // add service user back
        await addServiceUser();

        await api.data_doUpdatePath('master', `/user/${testUser1.id}`, testUser1);

        
    });

    it('data_doUpdatePath', async () => {
        const data = {
            test: 'def',
        };

        const result = await api.data_doUpdatePath('lookup', '/table/test', data);
    });

    it('data_doTestForm', async () => {
        const result = await api.data_doTestForm({test: true});
        result.should.be.eql(true);
    });

    it('data_doTestFormAuth', async () => {
        const result1 = await api.auth_doAuthUser('tao', 'pass', 'test');
        expect(result1.id).to.equal('tao');

        const result = await api.data_doTestFormAuth();
        expect(result).to.equal('Found');
    });

    it('data_doRecordFile', async () => {

        const result = await api.data_doRecordFile('test.json', {write: true, test: true});
        result.should.be.eql(true);
    });

    it('data_doVerifySimpleIndex', async () => {
        const result = await api.data_doVerifySimpleIndex('lookup', 'table', 'newId');
        result.should.be.eql('newId_1');
    });

    it('import samples should return success',  async () => {
        const samples: any[] = jsonfile.readFileSync(path.join(__dirname, './data/samples.json'));

        const promises = [];

        for(let rec of samples) {
            promises.push(api.data_doUpdatePath('geo_test', `/samples/${rec.id}`, rec));
        }

        let result = await Promise.all(promises);
        result.length.should.be.eql(50);

        result = await api.data_doVerifySimpleIndex('geo_test', 'samples', 'loc', 'geo');
        result.should.be.eql('loc_2dsphere');
    })

    it('data_doFindNearby', async () => {
        const result = await api.data_doFindNearby('geo_test', '/samples', { lon: 95, lat: 37 }, 10, 0, 100);
        console.log(result.length);
    });

    it('import zipcodes should return success',  async () => {
        const zipcodeData: { data: any[] } = jsonfile.readFileSync(path.join(__dirname, './data/zipcodes.json'));

        const promises = [];

        for(let rec of zipcodeData.data) {
            rec.lat = parseFloat(rec.lat);
            rec.lon = parseFloat(rec.lon);
            promises.push(api.data_doUpdatePath('geo_test', `/zipcodes/${rec.code}`, rec));
        }

        const result = await Promise.all(promises);
        result.length.should.be.eql(854);
    })

    it('data_doGetItems', async () => {
        
        const allData = await api.data_doGetItems('geo_test', '/zipcodes');
        allData.length.should.be.eql(854);
    });

    it('data_doGetItems', async () => {
        const testDataset    = 'geo_test';
        const testPath       = '/zipcodes';
        const fieldList      = {};
        const sortOrder: any = null;
        const offset         = 0;
        const limit          = 1000;

        const allData = await api.data_doGetItems(testDataset, testPath, fieldList, sortOrder, offset, limit);
        allData.length.should.be.eql(854);
    });

    it('Data data_doGetItemsStream', async () => {
        const testDataset    = 'geo_test';
        const testPath       = '/zipcodes/county:HAMPSHIRE';
        const fieldList      = {};
        const sortOrder: any = null;
        const offset         = 0;
        const limit          = 1000;

        const allData = await api.data_doGetItemsStream(testDataset, testPath, fieldList, sortOrder, offset, limit);

        allData.onData((item: any) => {
            
        });

        allData.onFinished(() => {
            console.log('data_doGetItemsStream finished');
        });
        const result = await allData.doWaitUntilFinished();
    });

    it('data_doGetItem', async () => {
        
        const result = await api.data_doGetItem('geo_test', '/zipcodes');
        result.should.have.property('code');
    });
    
    it('Data data_doGetItemsIfNeeded should resolve when list is empty', async () => {
        const list: any[] = [];
        const stream = await api.data_doGetItemsIfNeeded('geo_test', 'zipcodes', list);

        stream.onData = (record: any) => {
            return console.log('Stream received:', record);
        };

        await stream.doWaitUntilFinished();
    });

    it('Data data_doGetItemsIfNeeded', async () => {
        const testDataset = 'geo_test';
        const testPath    = '/zipcodes/county:HAMPSHIRE';
        const fieldList   = { _id: 1, id: 1, _lastModified: 1 };
        const sortOrder   = {};
        const offset      = 0;
        const limit       = 1000;

        const allData = await api.data_doGetItems(testDataset, testPath, fieldList, sortOrder, offset, limit);

        const list = [];
        for (const rec of allData) {
            list.push({ id: rec.id });
        }

        console.log(`Starting server stream for ${list.length} records.`);

        const stream = await api.data_doGetItemsIfNeeded('geo_test', 'zipcodes', list);

        console.log('Stream starting', stream);
        stream.onData = (record: any) => {
            return console.log('Stream received:', record);
        };

        await stream.doWaitUntilFinished();
    });



    // // If there's cached one it should return that as well.
    // it.only('Data data_doGetItemsIfNeeded with cached id', async () => {
    //     let testDataset = 'geo_test';
    //     let testPath    = '/zipcodes/county:HAMPSHIRE';
    //     let fieldList   = { _id: 1, id: 1, _lastModified: 1 };
    //     let sortOrder   = {};
    //     let offset      = 0;
    //     let limit       = 1;

    //     const allData = await api.data_doGetItems(testDataset, testPath, fieldList, sortOrder, offset, limit);

    //     const list = [];
    //     for (const rec of allData) {
    //         list.push({ id: rec.id });
    //     }

    //     let stream: any = await api.data_doGetItemsIfNeeded(dummyUser, 'rets_raw', 'raw', list);
        
    //     await stream.doWaitUntilFinished();
    // });

    it('data_doGetItemIfNewer', async () => {
        const result = await api.data_doGetItemIfNewer('geo_test', '/zipcodes/county:HAMPSHIRE', null);
        result.should.have.property('county', 'HAMPSHIRE');
    });


    it('doAggregate: Simple projection test for zipcodes Collection', async () => {
        const aggList = [{
            '$match': {
                'state': 'MA'
            }
        }, {
            '$sort': {
                'id': -1
            }
        }, {
            '$project': {
                'id': 1,
                'city': 1,
                'state': 1,
                'code': 1
            }
        }, {
            '$limit': 100
        }, {
            '$group': {
                _id: '$city',
                count: {
                    '$sum': 1
                }
            }
        }];

        const result = await api.data_doAggregate('geo_test', '/zipcodes', aggList);
        result.length.should.be.eql(86);
        result[0].should.have.property('_id');
        return result[0].should.have.property('count');
    });

    it('Complex aggregation test for zipcodes Collection', async () => {
        const aggList = [{
            '$match': {
                '$or': [{ 'state': 'MA' }, { 'state': 'RI' }]
            }

        }, {
            '$match': {
                'id': {
                    '$gt': 1440
                }
            }
        }, {
            '$project': {
                'id': 1,
                'city': 1
            }
        }, {
            '$limit': 100
        }, {
            '$group': {
                _id: '$city',
                count: {
                    '$sum': 1
                }
            }
        }, {
            '$sort': {
                'count': 1
            }
        }];

        const result = await api.data_doAggregate('geo_test', '/zipcodes', aggList);
        result.length.should.be.eql(75);
        result[0].should.have.property('_id');
        result[0].should.have.property('count');
        result[0].count.should.be.greaterThanOrEqual(1);
    });

    it('data_doDeletePath', async () => {
        const result = await api.data_doDeletePath('lookup', '/table/test');
        result.should.be.eql(true);
    });

    it('data_doGetUpdatedStats', async () => {
        const result = await api.data_doGetUpdatedStats('lookup');
        console.log(result);
    });
    
    it('data_doGetUniqueValues', async () => {
        const result = await api.data_doGetUniqueValues('geo_test', '/zipcodes/city', {});
        result.length.should.be.eql(596);
    });
    

    it('data_doAppendPath', async () => {

        const data: any = {
            test: [],
        };

        let result = await api.data_doUpdatePath('lookup', '/table/appendtest', data);

        result = await api.data_doAppendPath('lookup', '/table/appendtest/test', {
            newItem: true
        });

        result = await api.data_doGetItem('lookup', '/table/appendtest');
        result.test.length.should.greaterThanOrEqual(1);
    });
    
    it('Data data_doGetItems', async () => {
        const testDataset    = 'lookup';
        const testPath       = '/table';
        const fieldList      = {};
        const sortOrder: any = null;
        const offset         = 0;
        const limit          = 1000;

        const allData = await api.data_doGetItems(testDataset, testPath, fieldList, sortOrder, offset, limit);
        allData.length.should.be.above(0);
    });

});