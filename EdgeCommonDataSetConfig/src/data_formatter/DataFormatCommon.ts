const moment = require('moment');

export class DataFormatCommon {

    //
    //   Returns a generic list of options
    //   given a string as input which can be:
    //   A text list of items: Apple, Pear, Grape
    //   An array in Json: "['Apple','Pear','Grape']"
    //   A JSON Object with just options: "{ 0: 'Apple', 1: 'Pear' }"

    static getOptions(value: any) {

        const options: any = {};
        options.items      = [];

        if ((value == null)) {
            return options;
        }
        if (typeof value === "string") {

            //
            //  JSON Object
            if ((value.length > 0) && (value.charAt(0) === '{')) {
                const raw = JSON.parse(value);
                for (let varKey in raw) {
                    value    = raw[varKey];
                    value.id = varKey;
                    options.items.push(value);
                }

                //  JSON Array
            } else if ((value.length > 0) && (value.charAt(0) === '[')) {
                options.items = JSON.parse(value);

            } else {
                //
                //  Items are a simple comma seperated list of values
                if (/,/.test(value)) {
                    const rawItems = value.split(",");
                    for (let rawItem of Array.from(rawItems)) {
                        options.items.push(rawItem.trim());
                    }
                } else {
                    //
                    //  Return whatever raw options value was passed in
                    return value;
                }
            }
        } else {
            return value;
        }

        return options;
    }


    //
    // given a string, return number
    //
    // @param [String] data data to be converted to number
    // @return [Float] result equalent version of string in fload
    //
    getNumber(data: any) {
        let m;
        if ((data == null)) {
            return 0;
        }
        if (typeof data === "number") {
            return data;
        }

        m = data.toString().match(/(\d+)\s*%/);
        if (m) {
            return parseFloat(m[1]) / 100.0;
        }

        let result = data.toString().replace(/[^0-9\.\-]/g, "");
        result     = parseFloat(result);
        if (isNaN(result)) {
            return 0;
        }
        return result;
    }

    //
    // Given a date in a human readable form, parse it and return the Moment
    // object (see momentjs) that represents the date/time.
    //
    // @param [string] date The date string to parse
    // @note returns null if the date is invalid
    // @return [Moment] moment object
    //
    getMoment(data: any, format?: any) {
        try {
            let m;
            if ((data == null)) {
                return null;
            }

            if ((data._isAMomentObject != null) && data._isAMomentObject) {
                return data;
            }

            if ((typeof data === "object") && (data.getTime != null) && (typeof data.getTime === "function")) {
                m = moment(data);
                return m;
            }

            m = moment(new Date(data), format);
            if ((m != null) && (m.isValid() === true)) {
                return m;
            }

            if (data.match(/\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d/)) {
                return moment(data, "YYYY-MM-DD HH:mm:ss");
            }

            if (data.match(/\d\d\d\d-\d\d-\d\d/)) {
                return moment(data, "YYYY-MM-DD");
            }

            if (data.match(/\d\d\d\d\.\d\d\.\d\d \d\d:\d\d:\d\d/)) {
                return moment(data, "YYYY-MM-DD HH:mm:ss");
            }

            if (data.match(/\d\d\d\d\.\d\d\.\d\d/)) {
                return moment(data, "YYYY-MM-DD");
            }

            if (data.match(/\d\d-\d\d-\d\d\d\d \d\d:\d\d:\d\d/)) {
                return moment(data, "MM-DD-YYYY HH:mm:ss");
            }

            if (data.match(/\d\d-\d\d-\d\d\d\d/)) {
                return moment(data, "MM-DD-YYYY");
            }

            if (data.match(/\d\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d/)) {
                return moment(data, "MM/DD/YYYY HH:mm:ss");
            }

            if (data.match(/\d\d\/\d\d\/\d\d\d\d/)) {
                return moment(data, "MM/DD/YYYY");
            }

            if (data.match(/..., \d\d\d\d \d\d:\d\d:\d\d/)) {
                return moment(data, "ddd, MMM Do, YYYY h:mm:ss a");
            }

            if (data.match(/..., \d\d\d\d \d:\d\d:\d\d/)) {
                return moment(data, "ddd, MMM Do, YYYY h:mm:ss a");
            }

            if ((typeof data === "object") && (data['$date'] != null)) {
                return moment(new Date(data['$date']));
            }

        } catch (e) {

            console.log("Unable to get date from [", data, "]");
        }

        return null;
    }

    constructor() {
        // write constructor code
    }
}

export default DataFormatCommon;