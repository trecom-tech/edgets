

const slug     = require('slug');
const LRU      = require('lru-cache');

import config                   from 'edgecommonconfig';
import DataSetManager           from 'edgedatasetmanager';
import EdgeApi                  from 'edgeapi';
import ApiProvider              from './ApiProvider';
import ApiSocketUser            from './ApiSocketUser';
import * as DataSetConfig       from 'edgecommondatasetconfig';
import { EdgeError }            from './EdgeError';

const collectionName    = 'table';
const dbName            = 'lookup';
const tableListCacheKey = 'tableList';
const allowEditTablesP  = 'allowEditTables';

export default class ProviderTable extends ApiProvider {
    dsLookup: DataSetManager;
    private cache               : any;

    constructor(prototype: ApiProvider) {
        super(prototype);
    }

    /**
     * Get invoked when provider is added to gateway
     */
    onInit() {

        const dataSet       = slug(dbName);
        this.dsLookup       = new DataSetManager(dataSet);
        // Setup Cache
        const cacheOptions  = {
            max: 100,
            maxAge: 1000 * 60,
        };
        this.cache          = new LRU(cacheOptions);
    }

    /**
     * Returns base name. {BaseName}_{MemberName}
     */
    getBaseName() {
        return 'table';
    }

    /**
     * returns cache key
     * @param tableName - [string] table Name
     */
    internalGetRecordKey(tableName: string) {
        return `TABLE_${tableName}`;
    }

    /**
     * returns cache key for table list
     * @param tableName - [string] table Name
     */
    internalGetListKey(prefix: string) {
        return `${tableListCacheKey}_${prefix}`;
    }

    /**
     * Save `newData` to `path`
     * @param path - [string] Path string to save newData
     * @param newData - [any] JSON object to save
     * @returns difference of new and old data or true when there's no reference.
     */
    async internalDoSaveData(table: DataSetConfig.Table) {

        const tableSettings = table.serialize();
        const differences   = await this.dsLookup.doUpdate(`/${collectionName}/${table.tableName}`, tableSettings, '');

        if (Array.isArray(differences)) {
            config.status(`internalDoSaveData Complete:${ differences.length }`);
        }

        const recordKey = this.internalGetRecordKey(table.tableName);
        this.cache.reset();
        this.cache.set(recordKey, tableSettings);

        return true;
    }

    /**
     * Update / Create table
     * @param tableName - [string] table name
     * @param jsonData - [any] schema data in json for edgedatasetconfig's table 
     */
    async doSetTable(user: ApiSocketUser, tableName: string, jsonData: any) {
        if (!user.hasRole(allowEditTablesP)) {
            throw this.failedRoleCheck(user, allowEditTablesP, 'doSetTable');
        }

        config.status(`doGetTable tableName=${tableName} jsonData=${jsonData}`);
        const strAuctionTableName   = slug(tableName);

        const table                 = new DataSetConfig.Table(strAuctionTableName);
        table.unserialize(jsonData);
        
        return this.internalDoSaveData(table);
    }

    /**
     * Get table
     * @param tableName - [string] table name
     */
    async doGetTable(tableName: string) {

        config.status(`doGetTable tableName=${tableName}`);
        const strAuctionTableName = slug(tableName);

        const recordKey         = this.internalGetRecordKey(strAuctionTableName);
        const cachedTableData   = this.cache.get(recordKey);
        if (cachedTableData) {
            config.status(`doGetTable returns cached ${cachedTableData}`); 
            return cachedTableData;
        }

        const tableData = await this.dsLookup.doGetItem(`/${collectionName}/${strAuctionTableName}`);

        config.status(`doGetTable returns cached ${tableData}`); 
        return tableData.data;
    }

    /**
     * Change column field
     * @param tableName - [string] table name
     * @param columnName - [string] column name
     * @param changeType - [string] column field to change
     * @param newValue - [any] field data
     */
    async doChangeTable(user: ApiSocketUser, tableName: string, columnName: string, changeType: string, newValue: any) {
        
        if (!user.hasRole(allowEditTablesP)) {
            throw this.failedRoleCheck(user, allowEditTablesP, 'doSetTable');
        }

        config.status(`doChangeTable tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}`);
        const strAuctionTableName = slug(tableName);

        const tableData = await this.doGetTable(strAuctionTableName);

        const table = new DataSetConfig.Table(strAuctionTableName);
        table.unserialize(tableData);

        const column = table.getColumn(columnName);
        if (column) {
            column.changeColumn(changeType, newValue);

            config.status(`doChangeTable succeed tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}`);
            return this.internalDoSaveData(table);
        } else {
            config.status(`doChangeTable failed tableName=${tableName} columnName=${columnName} changeType=${changeType} tablenewValueName=${newValue}
                with Column is not found`);
            throw new EdgeError('Column is not found', 'TABLE_COLUMN_NOT_FOUND', 404);
        }
    }

    /**
     * Get tables starts with prefix
     * @param prefix - [string] prefix of table to search
     */
    async doListTables(prefix: string = '') {

        config.status(`doListTables prefix=${ prefix }`);
        
        let filter = '';
        if (prefix) {
            filter = `id:~^${prefix}`;
        }
        const listCacheKey  = this.internalGetListKey(prefix);
        const cacheResult   = this.cache.get(listCacheKey);
        if (cacheResult) {

            config.status(`doListTables returns lru cached results ${ cacheResult }`);
            return cacheResult;
        }

        const tableSettings = await this.dsLookup.doGetItems(`/${collectionName}/${filter}`, null, null, 0, 1000000);
        this.cache.set(listCacheKey, tableSettings);
        config.status(`doListTables returns results ${ tableSettings }`);
        return tableSettings;
    }
};
