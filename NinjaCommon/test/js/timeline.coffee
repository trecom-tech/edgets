$ ->
	Item1 = 
		text     : "This is the text of the event. The text goes here and wraps automatically. The entire text will be showing even if it takes multiple lines. This is 11px San Fransisco text."
		datetime : "2018-02-02 10:20:30"
		sender   : "tester1"
		icon     : "fa-heart"

	Item2 = 
		text     : "Test text for ViewTimelineItem including <p style='color: red; font-size: 24px;'> Styled HTML p Tag </p>"
		datetime : "2018-03-03 9:11:23"
		sender   : "tester2"

	Item3 = 
		text     : "Item text 3"
		datetime : "2017-11-02 20:11:44"

	Item4 = 
		text     : "Item text 4"
		datetime : "2018-03-01 11:45:33"
		sender   : "tester4"	

	Item5 = 
		text     : "This is the text of the event. The text goes here and wraps automatically. The entire text will be showing even if it takes multiple lines. This is 11px San Fransisco text."
		datetime : "2017-08-02 10:20:30"
		sender   : "tester5"
		icon     : "fa-book"

	Item6 = 
		text     : "Item text 6"
		datetime : "2017-08-13 9:11:23"
		sender   : "tester6"

	Item7 = 
		text     : "Item text 7"
		datetime : "2017-08-09 20:11:44"

	Item8 = 
		text     : "Item text 8"
		datetime : "2018-03-04 16:45:33"
		sender   : "tester8"			

	addTestButton "Simple Timeline View", "Open", () ->
		addHolder().setView "Timeline", (view) ->
			view.addItem Item1.datetime, Item1.text, Item1.sender, Item1
			view.addItem Item2.datetime, Item2.text, Item2.sender
			view.addItem Item3.datetime, Item3.text
			view.addItem Item4.datetime, Item4.text, Item4.sender

	addTestButton "Timeline View: Item Update event", "Open", () ->
		addHolder().setView "Timeline", (view) ->
			item1 = view.addItem Item1.datetime, Item1.text, Item1.sender, Item1
			item2 = view.addItem Item2.datetime, Item2.text, Item2.sender, Item2
			item3 = view.addItem Item3.datetime, Item3.text			
			item3.updateContent
				text:     "New content for item 3"
				datetime: "2018-01-22"
				sender:   "New sender of item 3"

	addTestButton "Timeline View: Items in the same Month", "Open", () ->
		addHolder().setView "Timeline", (view) ->
			item1 = view.addItem Item5.datetime, Item5.text, Item5.sender, Item5
			item2 = view.addItem Item6.datetime, Item6.text, Item6.sender, Item6
			item3 = view.addItem Item7.datetime, Item7.text	
			item4 = view.addItem Item8.datetime, Item8.text, Item8.sender
			setTimeout () ->
				view.getItems({ text: Item8.text }).map (item) -> item.updateContent
					text:     "New content for item 4"
					datetime: "2017-08-22"
					sender:   "New sender of item 4"
			, 3000
			setTimeout () ->
				options = view.serialize()				
				console.log "ViewTimeline serialized data = ", options
				doPopupView "Timeline", "Timeline Serialized", null, 1200, 800, (viewPopup) ->
					viewPopup
					.deserialize options
					.then () ->
						console.log "ViewTimeline = ", view.groups
						console.log "ViewTimeline on popup = ", viewPopup.groups
			, 2000

	go()