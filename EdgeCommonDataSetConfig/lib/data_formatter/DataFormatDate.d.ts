import DataFormatterType from './DataFormatterType';
export declare class DataFormatDate extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    datePicker: any;
    elDataEditor: any;
    constructor();
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): Date | "";
    getValueInTooltip(value: any): any;
}
export default DataFormatDate;
