import { config }       from 'edgecommonconfig';
import { EdgeRequest } from 'edgecommonrequest';
import { IContactInfo } from './IContactInfo';
import { IMessage }     from './IMessage';

const apn    = require('apn');
const FCM    = require('fcm-push');
const twilio = require('twilio');

export class EdgeCommonNotification {

    private contacts: IContactInfo[] = [];
    private logger: any;
    private readonly guid: string;

    constructor() {
        this.guid   = EdgeCommonNotification.generateGUID();
        this.logger = config.getLogger("PushNotification");
    }

    addContactDetails(userContactDetails: IContactInfo[] = []): boolean {
        for (const contactDetail of userContactDetails) {
            this.contacts.push(contactDetail);
        }

        return true;
    }

    async doSendMessage(messageDetails: IMessage): Promise<any> {
        for (const contact of this.contacts) {
            switch (contact.type) {
                case 'apple':
                    await this.doSendForApple(contact.account.deviceToken, messageDetails);
                    break;
                case 'google':
                    await this.doSendForGoogle(contact.account.deviceToken, messageDetails);
                    break;
                case 'whatsapp':
                    await this.doSendWhatsApp(contact.account.phoneNumber, messageDetails);
                    break;
                case 'sms':
                    await this.doSendSMS(contact.account.phoneNumber, messageDetails);
                    break;
                case 'email':
                default:
                    break;
            }
        }

        return true;
    }

    /*
    * apple push notification
    * */
    async doSendForApple(deviceToken: string, message: IMessage ) {
        try {
            const appleCreds = config.getCredentials('appleCreds');

            let notification = new apn.Notification(message);
            const provider   = new apn.Provider(appleCreds);

            if (!message.expiry) notification.expiry = Math.floor(Date.now() / 1000) + 3600 * 7; // Expires 7 hour from now.

            if (!message.urlArgs) notification.urlArgs = [];

            if (!message.sound) notification.sound = "chime.caf";

            if (!message.badge) notification.badge = 10;

            if (!message.topic) {
                this.logger.error(this.guid, 'missing topic');
            }

            if (!message.title) {
                this.logger.error(this.guid, 'missing title');
            }

            if (!message.alert) {
                notification.alert = '';
                this.logger.error(this.guid, 'missing body');
            }

            const response = await provider.send(notification, deviceToken);

            if (response.sent.length) {
                this.logger.info(this.guid, {
                    type       : 'apple',
                    deviceToken: deviceToken,
                    message    : message,
                    response   : response.sent,
                    created    : new Date(),
                });
                provider.shutdown();

                return true;
            } else {
                this.logger.error(this.guid, {
                    type       : 'apple',
                    deviceToken: deviceToken,
                    message    : message,
                    response   : response.failed,
                    created    : new Date(),
                });

                return false;
            }
        } catch (err) {
            this.logger.error(this.guid, err);
            return false;
        }
    }

    /*
    * google push notification
    * */
    async doSendForGoogle(deviceToken: string, message: IMessage) {

        try {
            const { serverKey } = config.getCredentials('googleCreds');

            const fcm   = new FCM(serverKey);

            const msg = {
                to          : deviceToken,
                collapse_key: 'green',
                data        : {},
                notification: {
                    title: message.title,
                    body : message.alert,
                },
            };

            const response = await fcm.send(msg);

            this.logger.info(this.guid, {
                type       : 'google',
                deviceToken: deviceToken,
                message    : message,
                response   : response,
                created    : new Date(),
            });

            return true;
        } catch (err) {
            this.logger.error(this.guid, {
                type       : 'google',
                deviceToken: deviceToken,
                message    : message,
                response   : err,
                created    : new Date(),
            });

            return false;
        }

    }

    /*
    * WhatsApp message
    * */
    async doSendWhatsApp(whatsAppNumber: string, message: IMessage) {
        try {
            const { accountSid, authToken, twilioNumber } = config.getCredentials('twilioCreds');
            let tNumber = await EdgeCommonNotification.validatePhoneNumber(twilioNumber);
            whatsAppNumber = await EdgeCommonNotification.validatePhoneNumber(whatsAppNumber);

            const client = await twilio(accountSid, authToken);

            const response = await client.messages.create({
                    body: message.alert,
                    from: `whatsapp:${tNumber}`,
                    to  : `whatsapp:${whatsAppNumber}`,
                    statusCallback: 'https://edgeapiserver.com/whatsapp/callbacks',
                });

            if (response.status === 'queued' || response.status === 'accepted') {
                const request = new EdgeRequest();
                const baseUrl = response._version._domain.baseUrl + response.uri;
                const baseToken = response._version._domain.twilio.httpClient.lastRequest.headers.Authorization;
                request.addHeader('Authorization', baseToken);

                const result: any = await request.doGetLink(`${baseUrl}`);
                console.log('status callback: ', result.status);

                if (result.error_code) {
                    this.logger.error(this.guid, {
                        type    : 'whatsapp',
                        number  : whatsAppNumber,
                        message : message,
                        response: result,
                        status  : result.status,
                        created : new Date(),
                    });

                    return false;
                } else {
                    this.logger.info(this.guid, {
                        type    : 'whatsapp',
                        number  : whatsAppNumber,
                        message : message,
                        response: result,
                        status  : result.status,
                        created : new Date(),
                    });

                    return true;
                }
            }

            return true;

        } catch (err) {
            this.logger.error(this.guid, {
                type          : 'whatsapp',
                whatsAppNumber: whatsAppNumber,
                message       : message,
                response      : err,
                status        : 'failed',
                created       : new Date(),
            });

            return false;

        }
    }

    /*
    * Send SMS with twilio
    * */
    async doSendSMS(phoneNumber: string, message: IMessage) {
        try {
            const { accountSid, authToken, twilioNumber } = config.getCredentials('twilioCreds');

            const client = twilio(accountSid, authToken);

            const response = await client.messages.create({
                body: message.alert,
                from: twilioNumber,
                to  : phoneNumber,
            });

            if (response.status === 'queued' || response.status === 'accepted') {
                const request = new EdgeRequest();
                const baseUrl = response._version._domain.baseUrl + response.uri;
                const baseToken = response._version._domain.twilio.httpClient.lastRequest.headers.Authorization;
                request.addHeader('Authorization', baseToken);

                const result: any = await request.doGetLink(`${baseUrl}`);
                if (result.error_code) {
                    this.logger.error(this.guid, {
                        type    : 'sms',
                        number  : phoneNumber,
                        message : message,
                        response: result,
                        status  : result.status,
                        created : new Date(),
                    });

                    return false;
                } else {
                    this.logger.info(this.guid, {
                        type    : 'sms',
                        number  : phoneNumber,
                        message : message,
                        response: result,
                        status  : result.status,
                        created : new Date(),
                    });

                    return true;
                }
            }

            return

        } catch (err) {
            this.logger.error(this.guid, {
                type   : 'sms',
                number : phoneNumber,
                message: message,
                error  : err,
                status : 'failed',
                created: new Date(),
            });

            return false;
        }
    }

    /*
    * e164 formatted phone number
    * */
    private static validatePhoneNumber(phoneNumber: any) {
        return phoneNumber.toString().substr(0, 1) === '+' ? phoneNumber : `+${phoneNumber}`;
    }

    /*
    * generate GUID to search up logs
    * */
    private static generateGUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16 | 0;
            let v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

}

export default EdgeCommonNotification;
