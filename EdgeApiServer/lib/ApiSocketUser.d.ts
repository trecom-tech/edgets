/// <reference types="node" />
import { EdgeSocketClient, EdgeSubscription } from 'edgeapi';
import ApiWebUser from './ApiWebUser';
import { IUser } from './interfaces/IUser';
import { IPubSubData } from './interfaces/IPubSubData';
export default class ApiSocketUser extends ApiWebUser {
    myConnectionID: number;
    socket: EdgeSocketClient;
    myName: number;
    rec: IUser;
    authAttempts: number;
    private sendDelayedQueue;
    private sendDelayedTimer;
    private streamingListNumber;
    configSendFlat: boolean;
    subscriptionList: EdgeSubscription[];
    private wantStatusUpdate;
    private isAdmin;
    constructor(myConnectionID: number, socket: EdgeSocketClient);
    /**
     * Become an admin user, no specific use case assigned right now
     */
    setAdmin(): boolean;
    /**
     * Unset admin
     */
    setNoAdmin(): boolean;
    setConfigFlat(): boolean;
    /**
     * Subscribe to status updates
     */
    setWantStatusUpdates(): boolean;
    logIncoming(cmd: string, obj: any): boolean;
    send(cmd: string, obj: any): boolean;
    /**
     * Send this back to the client and they know to expect data
     * then use sendPush to send the actual data.
     */
    startStreamingList(): string;
    /**
     * Terminate the streaming list by sending a special command to the receiver
     * @param stream_id
     */
    endStreamingList(stream_id: string): boolean;
    /**
     * Generic push notification message
     * @param cmd string steramer id
     * @param obj message
     */
    sendPush(cmd: string, obj: any): boolean;
    sendPushDelayed(cmd: string, obj: any): NodeJS.Timeout;
    onPushDelayed(): boolean;
    addSubscription(subscription: EdgeSubscription): boolean;
    filterChange(data: IPubSubData): boolean;
    reset(): boolean;
}
