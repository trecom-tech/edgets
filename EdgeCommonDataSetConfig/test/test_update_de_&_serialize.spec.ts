import * as DataSetConfig from '../src';

const should     = require('should');
const { Column } = DataSetConfig;

describe('Test serialize and deserialize for column works fine', function () {
    const testColumnData1 = {
        name     : "country",
        type     : "text",
        editable : true,
        width    : 100,
        visible  : true,
        source   : "country_name",
        autosize : true,
        cellColor: `\
console.log("Dummy Function");
return("red");\
`
    };

    const c = new Column();

    c.deserialize(testColumnData1);

    it('should be renew data with deserialize(new data)', () => {
        const old_width = c.getWidth();
        const newData   = {
            source : "id",
            system : false,
            tooltip: "",
            type   : "int",
            visible: true
        };

        c.deserialize(newData);
        const new_width = c.getWidth();
        old_width.should.not.be.equal(new_width);
        new_width.should.be.equal(90);
    });


    it('should be saved actual data for serialize()', () => {
        const old_column = c.serialize();

        const newData = {
            source : "id",
            system : false,
            type   : "int",
            visible: true,
            width  : 100
        };

        c.deserialize(newData);
        const obj = c.serialize();
        Object.keys(obj).length.should.not.be.equal(Object.keys(old_column).length);
        Object.keys(obj).length.should.be.equal(19);
        obj.source.should.be.equal('id');
        obj.system.should.be.equal(false);
        obj.type.should.be.equal('int');
        obj.visible.should.be.equal(true);
    });
});