import config    from 'edgecommonconfig';
import * as path from "path";

config.setCredentials('appleCreds', {
    pfx: path.join(__dirname, '/../credentials/apple/Cert-Dev.p12'),
    production: false
});

config.setCredentials('googleCreds', {
    serverKey: 'AAAAnUNHlsA:APA91bEMZZX1vrLVlyrGACazD9OxqgXV46Yp4SqKq4BFW9mfxtQSWbi8_D_Et1myZdyJLqfvOOtJ7vqZ__PJ5P_pSuInUT5wbADQS3I9mOXV-989lxfnbsX1Oh_WvV7aduKCvXqc3mAO',
});

config.setCredentials('twilioCreds', {
    accountSid: 'AC0327c79504ab714c19b2cfbc9261e01b',
    authToken: '370c7a269d528a7c86122b91c36e7e97',
    twilioNumber: '14075375201',
    sandboxNumber: '+14155238886'
});
