$ ->
    dataset1 =
        [
            {
                "code": "00501",
                "city": "Holtsville",
                "state": "NY",
                "county": "SUFFOLK",
                "area_code": "12345",
                "lat": "40.922326",
                "lon": "-72.637078"
            },
            {
                "code": "00544",
                "city": "Holtsville",
                "state": "NY",
                "county": "SUFFOLK",
                "area_code": "",
                "lat": "40.922326",
                "lon": "-72.637078"
            },
            {
                "code": "01001",
                "city": "Agawam",
                "state": "MA",
                "county": "HAMPDEN",
                "area_code": "",
                "lat": "42.140549",
                "lon": "-72.788661"
            },
            {
                "code": "01002",
                "city": "Amherst",
                "state": "MA",
                "county": "HAMPSHIRE",
                "area_code": "",
                "lat": "42.367092",
                "lon": "-72.464571"
            },
            {
                "code": "01003",
                "city": "Amherst",
                "state": "MA",
                "county": "HAMPSHIRE",
                "area_code": "",
                "lat": "42.369562",
                "lon": "-72.63599"
            },
            {
                "code": "01004",
                "city": "Amherst",
                "state": "MA",
                "county": "HAMPSHIRE",
                "area_code": "",
                "lat": "42.384494",
                "lon": "-72.513183"
            },
            {
                "code": "01005",
                "city": "Barre",
                "state": "MA",
                "county": "WORCESTER",
                "area_code": "",
                "lat": "42.32916",
                "lon": "-72.139465"
            },
            {
                "code": "01007",
                "city": "Belchertown",
                "state": "MA",
                "county": "HAMPSHIRE",
                "area_code": "",
                "lat": "42.280267",
                "lon": "-72.402056"
            },
            {
                "code": "01008",
                "city": "Blandford",
                "state": "MA",
                "county": "HAMPDEN",
                "area_code": "",
                "lat": "42.177833",
                "lon": "-72.958359"
            }
        ]

    dataMapData = {
       "data":{

       },
       "engine":{
          "zipcode;1":{
             "1":{
                "code":"00501",
                "city":"Holtsville",
                "state":"NY",
                "county":"SUFFOLK",
                "area_code":"12345",
                "lat":"40.922326",
                "lon":"-72.637078",
                "id":"1"
             },
             "2":{
                "code":"00544",
                "city":"Holtsville",
                "state":"NY",
                "county":"SUFFOLK",
                "area_code":"",
                "lat":"40.922326",
                "lon":"-72.637078",
                "id":"2"
             },
             "3":{
                "code":"01001",
                "city":"Agawam",
                "state":"MA",
                "county":"HAMPDEN",
                "area_code":"",
                "lat":"42.140549",
                "lon":"-72.788661",
                "id":"3"
             },
             "4":{
                "code":"01002",
                "city":"Amherst",
                "state":"MA",
                "county":"HAMPSHIRE",
                "area_code":"",
                "lat":"42.367092",
                "lon":"-72.464571",
                "id":"4"
             },
             "5":{
                "code":"01003",
                "city":"Amherst",
                "state":"MA",
                "county":"HAMPSHIRE",
                "area_code":"",
                "lat":"42.369562",
                "lon":"-72.63599",
                "id":"5"
             },
             "6":{
                "code":"01004",
                "city":"Amherst",
                "state":"MA",
                "county":"HAMPSHIRE",
                "area_code":"",
                "lat":"42.384494",
                "lon":"-72.513183",
                "id":"6"
             },
             "7":{
                "code":"01005",
                "city":"Barre",
                "state":"MA",
                "county":"WORCESTER",
                "area_code":"",
                "lat":"42.32916",
                "lon":"-72.139465",
                "id":"7"
             },
             "8":{
                "code":"01007",
                "city":"Belchertown",
                "state":"MA",
                "county":"HAMPSHIRE",
                "area_code":"",
                "lat":"42.280267",
                "lon":"-72.402056",
                "id":"8"
             },
             "9":{
                "code":"01008",
                "city":"Blandford",
                "state":"MA",
                "county":"HAMPDEN",
                "area_code":"",
                "lat":"42.177833",
                "lon":"-72.958359",
                "id":"9"
             }
          }
       },
       "types":{
          "zipcode":{
             "settings":{

             },
             "code":{
                "name":"Code",
                "type":"text",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"code",
                "required":false,
                "hideable":false,
                "order":0,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "city":{
                "name":"City",
                "type":"text",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"city",
                "required":false,
                "hideable":false,
                "order":1,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "state":{
                "name":"State",
                "type":"text",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"state",
                "required":false,
                "hideable":false,
                "order":2,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "county":{
                "name":"County",
                "type":"text",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"county",
                "required":false,
                "hideable":false,
                "order":3,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "area_code":{
                "name":"Area Code",
                "type":"int",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"area_code",
                "required":false,
                "hideable":false,
                "order":4,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "lat":{
                "name":"Lat",
                "type":"decimal",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"lat",
                "required":false,
                "hideable":false,
                "order":5,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "lon":{
                "name":"Lon",
                "type":"decimal",
                "editable":false,
                "visible":true,
                "align":"left",
                "source":"lon",
                "required":false,
                "hideable":false,
                "order":6,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":true,
                "visibleeditform":true,
                "validateoncreate":true,
                "validateonedit":true,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             },
             "id":{
                "name":"Id",
                "type":"int",
                "width":80,
                "editable":false,
                "visible":true,
                "align":"right",
                "source":"id",
                "required":false,
                "hideable":false,
                "order":7,
                "pii":null,
                "blind":null,
                "locked":false,
                "flagsview":[

                ],
                "flagsedit":[

                ],
                "visiblenewform":false,
                "visibleeditform":false,
                "validateoncreate":false,
                "validateonedit":false,
                "validate_err_message":null,
                "validate_warn_message":null,
                "validate_options":null,
                "validation_type":null,
                "validation_details":null
             }
          }
       },
       "savecallbacks":{

       }
    }

    addTestButton "DataMap Serialize", "Open", () ->
        DataMap.removeTableData "zipcode;1"
        id = 0
        for rec in dataset1
            DataMap.addDataUpdateTable "zipcode;1", ++id, rec   

        addHolder()
        .setView "Docked", (view) ->
            view.deserialize
                size: 200
                dock:
                    view: "NavBar"
                    items: [
                        type: "button"
                        icon: "far fa-plus-circle"
                        name: "nav-btn-new-row"
                        text: "New Row"
                        callback: () =>
                            new ModalForm
                                title: "Add a New Row"
                                create_from_table: "zipcode"
                                onsubmit: (form) ->
                                    # console.log "New row form: ", form
                                    DataMap.addDataWithAutoID "zipcode;1",
                                        code: form.code.getValue()
                                        city: form.city.getValue()
                                        state: form.state.getValue()
                                        county: form.county.getValue()
                                        area_code: form.area_code.getValue()
                                        lat: form.lat.getValue()
                                        lon: form.lon.getValue()
                    ,
                        type: "button"
                        icon: "far fa-arrow-alt-down"
                        name: "nav-btn-export-dm"
                        text: "Export DataMap to Console"
                        callback: () =>
                            console.log "DataMap serialized: ", DataMap.serialize()
                    ]
                main:
                    view: "Table"
                    collection: "zipcode;1"
            true

    addTestButton "DataMap Deserialize", "Open", () ->
        DataMap.removeTableData "zipcode;1"

        addHolder()
        .setView "Docked", (view) ->
            view.deserialize
                size: 200
                dock:
                    view: "NavBar"
                    items: [
                        type: "button"
                        icon: "far fa-plus-circle"
                        name: "nav-btn-new-row"
                        text: "Edit DataMap Data"
                        callback: () =>
                            doPopupView "Editor", "DataMap Data Editor", "dm-data-editor", 800, 600, (view) ->
                                view.setSaveFunc (value) ->
                                    try
                                        data = JSON.parse value
                                        DataMap.deserialize data
                                        console.log "Dm data: ", value
                                    catch e
                                        new ModalForm
                                            title: "Error"
                                            content: "Your input is not valid JSON"
                                    true
                    ,
                        type: "button"
                        icon: "far fa-arrow-alt-down"
                        name: "nav-btn-export-dm"
                        text: "Export DataMap to Console"
                        callback: () =>
                            console.log "DataMap serialized: ", DataMap.serialize()
                    ]
                main:
                    view: "Table"
                    collection: "zipcode;1"

    go()