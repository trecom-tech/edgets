$ ->

    addTestButton "ViewNodeDocs Basic", "Open", () ->
        localStorage.clear()
        addHolder()
        .setView "NodeDocs", (view) ->
            view.deserialize
                modules: [
                    key: "react"
                    url: "https://reactjs.org/"
                , 
                    key: "angular"
                    url: "https://angular.io/"
                , 
                    key: "ninja"
                ]

        true

    addTestButton "ViewNodeDocs on Popup", "Open", () ->
        doPopupView "NodeDocs", "Popup with a NodeDocs", null, 800, 600, (view)->
            view.deserialize
                modules: [
                    key: "react"
                    url: "https://reactjs.org/"
                , 
                    key: "angular"
                    url: "https://angular.io/"
                , 
                    key: "ninja"
                ]
            true
        true

    addTestButton "NodeDocs in tab", "Open", ()->
        addHolder()
        .setView "TestLayout1", (view)->
            view.runTest
                view: "DynamicTabs"
                name: "viewTabs"
                tabs: [
                    view: "TestShowSize"
                    keyboard: "1"
                    name: "view1"
                    title: "Top tab 1"
                ,
                    view: "NodeDocs"
                    name: "view2"
                    keyboard: "2"
                    title: "NodeDocs"
                    modules: [
                        key: "koa"
                    ,
                        key: "coffee"
                        url: "http://coffeescript.org/"
                    ,
                        key: "filesystem"
                    ]
                ]
        true

    go()