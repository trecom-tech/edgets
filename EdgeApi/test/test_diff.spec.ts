import EdgeApi   from '../src/EdgeApi';
import { config } from 'edgecommonconfig';

const should = require('should');

config.setCredentials("redisHost", process.env.REDIS_HOST);
config.setCredentials("redisHostWQ", process.env.REDIS_HOST);
config.setCredentials("redisReadHost", process.env.REDIS_HOST);
config.setCredentials("ApiServers", [process.env.API_SERVER]);

let testObject1 = {
    name: "Brian Pollack",
    age: 42,
    birthdate: new Date(1975, 10, 2),
    address: {
        street: "315 E Waterlynn Rd",
        city: "Mooresville",
        state: "NC",
        zipcode: 28115
    },
    income: 123.45678,
    tall: true,
    colors: ["Red", "Green", "Blue"]
};

let testObject2 = {
    name: "Brian Pollack",
    age: 43,
    birthdate: new Date(1975, 10, 2),
    address: {
        street: "315 E Waterlynn Rd",
        city: "Mooresville",
        state: "NC",
        zipcode: 28115
    },
    income: 123.45678,
    tall: true,
    colors: ["Red", "Green", "Blue"]
};

describe("Simple change to age field", () => {

    it("Should handle delete", () => {

        let result = EdgeApi.deepDiff({ "_id": "577abce5ec795af72c029f44", "id": 4, "phone": "111" }, { "phone": null, "id": 4 });
        result[0].should.have.property("kind", "D");
    });

    it("Should return one E result", () => {

        let result = EdgeApi.deepDiff(testObject1, testObject2);
        result[0].should.have.property("kind", "E");
        result[0].should.have.property("lhs", 42);
        result[0].should.have.property("rhs", 43);
        result[0].path.length.should.equal(1);
    });

    it("Clone object should return a copy with no changes", () => {

        let testObject = EdgeApi.cloneObject(testObject1);
        let result = EdgeApi.deepDiff(testObject1, testObject);
        result.length.should.equal(0);
    });

    it("Should handle a change in an array", () => {

        let testObject = EdgeApi.cloneObject(testObject1);
        testObject.colors[1] = "Purple";
        let result = EdgeApi.deepDiff(testObject1, testObject);
        result.length.should.equal(1);
        result[0].kind.should.equal("NE");
        result[0].rhs[0].should.equal('"Purple"');
    });
});

describe("Simple new fields", () => {
    it("Should find a new field", () => {
        let testObject = EdgeApi.cloneObject(testObject1);
        testObject.new_field = 10000;
        let result = EdgeApi.deepDiff(testObject1, testObject);

        return console.log("Result of new:", result);
    });
});


describe('Deepdiff and hash', () => {
    it('getHash', async () => {
        let result = EdgeApi.getHash({
            some   : true,
            complex: 'new',
            object : 'test',
        });

        should.exist(result);
    });

    it('cleanupObject', async () => {
        let result = EdgeApi.cleanupObject({
            some   : true,
            complex: 'new',
            object : 'test',
        });

        result.should.be.eql(true);
    });

    it('deepMergeObject', async () => {
        let result = await EdgeApi.deepMergeObject({
            some   : {
                let: 1,
            },
            complex: 'new',
            object : 'test',
        }, {
            some    : {
                let: 3,
                it : 2,
            },
            complex1: 'new',
        });

        result.some.let.should.be.eql(3);
    });

    it('dumpObject', async () => {
        let result = await EdgeApi.dumpObject({
            some   : {
                let: 1,
            },
            complex: 'new',
            object : 'test',
        }, './test.dump', 'test');
    });


    describe("Fix JSON Object", () => {
        it("Should handle basic number conversion", (done) => {

            let test = { a: "1", b: "10", c: "100.5", d: "123-456", e: -10 };   
            EdgeApi.fixupJson(test);
            test.a.should.equal(1);
            test.b.should.equal(10);
            test.c.should.equal(100.5);
            test.d.should.equal("123-456");
            test.e.should.equal(-10);
            done();
        });
    });

    describe("Deep Diff Objects", () => {

        let a = { name: "Brian", age: 10 };
        let b = { name: "Brian", age: 13 };
        it("Should find the difference", () => {
            let d = EdgeApi.deepDiff(a, b, "/");
            // DIFF= [ { kind: 'E', lhs: 10, rhs: 13, path: [ '/', 'age' ] } ]
            
            d[0].should.have.property("kind", "E");
            d[0].should.have.property("lhs", 10);
            d[0].should.have.property("rhs", 13);
        });
    });

});


describe('GetHash Functionality', () => {

    let large_object_1 = {
        'sys_id': '20170808145250900188000000', 'server_id': 42, 'class_name': 'A', 'class_key': '42_A', 'raw': {
            'Other Flooring'                     : 'Tile',
            'Bedroom 2 Room Level'               : '2',
            'VOWAddressDisplay'                  : true,
            'ListingOfficePhone'                 : '(904) 380-0774',
            'Picture Count'                      : 4,
            'Navgble to Ocean Y/N'               : 'N',
            'Picture Timestamp'                  : '2017-08-08T09:53:44.000Z',
            'High School'                        : 'Terry Parker',
            'Region'                             : '04-ARLINGTON/FT CAROLINE',
            'Days On Market'                     : '7',
            'Waterfront Y/N'                     : 'Y',
            'New Construction Y/N'               : 'Y',
            'Square Foot Source'                 : 'Plans',
            'Approx Parcel Size'                 : 'Less than 1/4 Acre',
            'Property Owner'                     : 'Seller Owned',
            'Timestamp'                          : '2017-08-15T07:31:51.000Z',
            'Elementary School'                  : 'Don Brewer',
            'Misc Exterior'                      : ['Patio - Covered', 'Mandatory Fees', 'Covenants/Restrictns', 'Porch - Front', 'Lanai', 'Lanai - Screened', 'Sprinkler System', 'Builder Warranty'],
            'Middle School'                      : 'Landmark',
            'Bath'                               : 'Ownr Bath Shwr No Tub',
            'Entry Timestamp'                    : '2017-08-08T09:53:39.000Z',
            'Lot'                                : '38',
            'Gated Community'                    : 'N',
            'Legal Name of Subdiv'               : 'CAROLINE HILLS',
            'Directions'                         : 'Take I-295 N to exit 46-Merrill Rd. Turn right onto Merrill Rd and then right onto Jane St. Caroline Hills will be on the left.',
            'Country'                            : 'USA',
            'Real Estate Parcel #'               : '1207200570',
            'Bedroom 3 Flooring'                 : 'Carp',
            'Other Room Level'                   : '1',
            'VOWConsumerComment'                 : true,
            'Kitchen Flooring'                   : 'Tile',
            'Breakfast Rm Room Level'            : '1',
            'Public Remarks'                     : 'Ready NOW! Lennar Homes, Chastain Bonus floor plan, 3 Bed, 2.5 Bath with loft and bonus room on a larger homesite backing to water. Everything\'s Included(r) features:  Quartz kitchen counter tops, 42" cabinets, Frigidaire(r) stainless steel appliances (stove, dishwasher, microwave, and refrigerator) washer & dryer, Ceramic wood tile,  Pre-wired security system, Nexia(tm) Home Automation, pavered & screened lanai, window blinds throughout, sprinkler system, and pavered driveway. 1 year builder warranty, dedicated customer service program and 24-hour emergency service.',
            'Water Heater'                       : 'Electric Water Heatr',
            'Common Name of Sub'                 : 'CAROLINE HILLS',
            'Interior Amenities'                 : ['Ceiling 8+ Ft.', 'Thermal Window(s)', 'Window Treatment(s)', 'Sliding Glass Dr(s)', 'Wall-to-Wall Carpet', 'Tile Floors', 'Wood Floors', 'Walk-in-Closet(s)', 'Structured Wiring', 'Unfurnished', 'Double Paned Windows'],
            'All Bedrooms Conform'               : 'Y',
            'Parking Facilities'                 : ['Attached Garage', 'Garage Door Opener'],
            'Association Fee'                    : 'Y',
            'Listing Type'                       : 'Exclusive Right of Sale',
            'Family Rm Flooring'                 : 'Tile',
            'Half Baths'                         : 1,
            'Lot Description'                    : ['Regular Lot', 'Sidewalks', 'Other Water View', 'Curb & Gutter'],
            'Bedrooms'                           : 3,
            'Bathrooms'                          : 3,
            'Full Baths'                         : 2,
            'Bonus/Den/Office Flooring'          : 'Carp',
            'Bonus/Den/Office Room Remarks'      : 'Bonus Room',
            'Waterfront Descrpt'                 : 'Man Made Lake/Pond',
            'Common/ClubAmenities'               : 'Garbage Pick-up',
            'ListingMemberName'                  : 'MATTHEW FIGLESTHALER',
            'VOWEntireListingDisplay'            : true,
            'Energy Features'                    : ['Heat Pump-Air', 'Ceiling Fan(s)', 'R-19 Wall', 'R-30 Ceiling', 'Forced Air', 'SetBack Thermostat', 'Energy Star Windows'],
            'Title Status'                       : 'N/A',
            'Pool/Hot Tub'                       : 'No Pool',
            'MLS Identifier'                     : '20020916141532502797000000',
            'Category'                           : 'Residential',
            'MLS Approved'                       : 'Y',
            'List Number Main'                   : 895712,
            'Other Room Remarks'                 : 'Laundry',
            'Internal Listing ID'                : '20170808145250900188000000',
            'Documents on File'                  : ['Survey on File', 'Photos on File', 'Floor Plans on File', 'Bldg Permits on File', 'Plat Map on File'],
            'Property Type'                      : 'A',
            'Property Group ID'                  : '19990816212109142258000000',
            'Agent ID'                           : '20140115213442148119000000',
            'Bedroom 2 Flooring'                 : 'Carp',
            'Structure'                          : ['Wood Frame', 'Slab-on-Grade'],
            'ListingMemberShortId'               : '58168',
            'Utilities'                          : ['Water - Public', 'Sewer - Public', 'Cable - Available', 'Underground Util.', 'DSL Available'],
            'New Construction'                   : ['Under Construction', 'Builder Inventory'],
            'Status'                             : 'Active',
            'Status Change Date'                 : '2017-08-08T00:00:00.000Z',
            'Listing Date'                       : '2017-08-08T00:00:00.000Z',
            'Occupancy'                          : 'At Closing',
            'Possible Financing'                 : ['FHA', 'VA', 'Conventional', 'Sellr May Pay Closng', 'Cash'],
            'Kitchen'                            : ['Breakfast Bar', 'Pantry - Walk in', 'Food Prep Island', 'Solid Srfce Cntrtops'],
            'Dining'                             : 'Breakfast Room',
            'ListingOfficeFax'                   : '(904) 380-0898',
            'Bonus/Den/Office Room Level'        : '3',
            'MasterBr Flooring'                  : 'Carp',
            'Stories: # Stories'                 : 2,
            'Roof'                               : 'Shingle Roof',
            'Style'                              : 'Traditional',
            'Type of Cooling'                    : ['Central Cooling', 'Electric Source'],
            'ListingMemberPhone'                 : '(844) 881-9379',
            'VOWAutomatedValuationDisplay'       : true,
            'ListingMemberEmail'                 : 'lennarjacksonville@lennar.com',
            'Breakfast Rm Flooring'              : 'Tile',
            'Approx Heated SqFt'                 : 2299,
            'Geo Lon'                            : -81.548559,
            'MasterBr Room Level'                : '2',
            'Geo Lat'                            : 30.348339,
            'Postal Code'                        : '32225',
            'ListingMemberUrl'                   : 'www.LennarJacksonville.com',
            'ListingOfficeAddress'               : '9440 PHILLIPS HWY, SUITE 7, JACKSONVILLE, FL 32256',
            'Additional Equipment'               : ['Security Sys.-Owned', 'Smoke Detector'],
            'Family Rm Room Level'               : '1',
            'Year Built'                         : 2017,
            'Bedroom 3 Room Level'               : '2',
            'RealtorCOM Type'                    : 'Residential - Single Family',
            'Presently Zoned'                    : ['Residential', 'Single Family', 'PUD'],
            'Exterior Wall'                      : 'Cementitious Siding',
            'Type of Heating'                    : ['Central Heating', 'Electric Source'],
            'Additional Rooms'                   : ['Entry Hall / Foyer', 'Family Room', 'Great Room', 'Bonus Room/Game Room', 'Lndry/Util. (inside)', 'Loft'],
            'Type of Dwelling'                   : 'Sngl. Fam.-Detached',
            'Major Appliances'                   : ['Range Electric', 'Microwave', 'Dishwasher', 'Disposer', 'Refrigerator', 'Washer', 'Dryer', 'Washer/Dryer Connect', 'Self Cleaning Oven', 'Ice Maker-Refrige', 'EnergyStar Appliance'],
            'Parking Facilities: # Garage Spaces': 2,
            'ListingOfficeEmail'                 : 'lennarjacksonville@lennar.com',
            'Price'                              : 287990,
            'Original List Price'                : 287990,
            'Mobile/Mfg Home'                    : 'N',
            'Assoc Fee'                          : 172.5,
            'Area'                               : '042-FT CAROLINE',
            'Road Surface'                       : 'Asphalt Road',
            'Listing ID'                         : '895712',
            'Publish to Internet'                : 'Yes',
            'Structure: Builder'                 : 'Lennar Homes, LLC',
            'Office ID'                          : '20091207190738330576000000',
            'Assoc Fee Freq'                     : 'Quarterly',
            'Street Number'                      : '2600',
            'Road Frontage'                      : 'City Street',
            'Export Options'                     : ['Publish Address', 'Publish Square Feet', 'Publish Directions', 'Publish Public Rmrks'],
            'Street Name'                        : 'CAROLINE HILLS',
            'Street Sfx'                         : 'DR',
            'ListingOfficeShortId'               : 'F18103',
            'City'                               : 'JACKSONVILLE',
            'Homestead'                          : 'N',
            'ListingOfficeName'                  : 'LENNAR REALTY INC',
            'Kitchen Room Level'                 : '1',
            'CDD Fee'                            : 0,
            'CDD Fee Y/N'                        : 'N',
            'State/Province'                     : 'FLORIDA',
            'County'                             : 'DUVAL',
            'Historic Area'                      : 'N',
        },
    };

    let simple1 = {
        name: 'Brian',
        age : 42,
    };

    it('Should get a simple object hash', () => {
        let hash = EdgeApi.getHash(simple1);
        return hash.should.equal('68495873b3a6d9d75b833687dd8c9d1a60ee22b0');
    });

    it('Should get the same on a cloned object', () => {

        let clonedObject = EdgeApi.cloneObject(simple1);
        let hash         = EdgeApi.getHash(clonedObject);
        return hash.should.equal('68495873b3a6d9d75b833687dd8c9d1a60ee22b0');
    });

    it('Should spot a change in a number field', () => {

        let clonedObject = EdgeApi.cloneObject(simple1);
        clonedObject.age = 52;
        let hash         = EdgeApi.getHash(clonedObject);
        return hash.should.not.equal('68495873b3a6d9d75b833687dd8c9d1a60ee22b0');
    });

    it('Should spot a change in a string field', () => {

        let clonedObject  = EdgeApi.cloneObject(simple1);
        clonedObject.name = 'Tim';
        let hash          = EdgeApi.getHash(clonedObject);
        return hash.should.not.equal('68495873b3a6d9d75b833687dd8c9d1a60ee22b0');
    });

    it('Should spot a adding a field', () => {

        let clonedObject  = EdgeApi.cloneObject(simple1);
        clonedObject.race = 'Winner';
        let hash          = EdgeApi.getHash(clonedObject);
        return hash.should.not.equal('68495873b3a6d9d75b833687dd8c9d1a60ee22b0');
    });

    it('Should should work with dates', () => {

        let clonedObject = EdgeApi.cloneObject(simple1);
        clonedObject.now = new Date();

        let clonedObject2 = EdgeApi.cloneObject(clonedObject);

        let hash1 = EdgeApi.getHash(clonedObject);
        let hash2 = EdgeApi.getHash(clonedObject2);
        return hash1.should.equal(hash2);
    });

    it('Should should work with date changes', () => {

        let clonedObject = EdgeApi.cloneObject(simple1);
        clonedObject.now = new Date();

        let clonedObject2 = EdgeApi.cloneObject(clonedObject);
        clonedObject2.now += 1;

        let hash1 = EdgeApi.getHash(clonedObject);
        let hash2 = EdgeApi.getHash(clonedObject2);
        return hash1.should.not.equal(hash2);
    });

    it('Should work on large objects', () => {

        let clonedObject = EdgeApi.cloneObject(large_object_1);
        let hash         = EdgeApi.getHash(clonedObject);
        return hash.should.equal('8e7350c8a5d43c7e3920be9a0e971e76bb278d6b');
    });

    it('Should work on large object changes', () => {

        let clonedObject                  = EdgeApi.cloneObject(large_object_1);
        clonedObject.raw['Picture Count'] = 5;
        let hash                          = EdgeApi.getHash(clonedObject);
        hash.should.equal('53958d188928637c53778d101a9ab0ff19febf06');

        clonedObject.raw['Picture Count'] = 6;
        hash                              = EdgeApi.getHash(clonedObject);
        return hash.should.not.equal('53958d188928637c53778d101a9ab0ff19febf06');
    });
});
