class ViewCodeEditProperties extends View

    updateFromMeta: (codeRecord)=>
        @txtModuleName.html codeRecord.name
        @txtModuleCategory.html codeRecord.category

        if !codeRecord.meta? or !codeRecord.meta.params? or codeRecord.meta.params.length < 1
            @paramBody.html "<tr><td colspan=4> None </td></tr>"
        else
            str = ""
            for item in codeRecord.meta.params
                str += "<tr>"
                str += "<td> #{item.name} </td>"
                str += "<td> #{item.type} </td>"
                str += "<td> #{item.default} </td>"
                str += "<td> #{item.optional} </td>"
                str += "</tr>"

            @paramBody.html str

        true
