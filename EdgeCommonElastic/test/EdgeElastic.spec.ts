
import { config } from 'edgecommonconfig';
import EdgeElastic from '../src/EdgeElastic';


const credentials = config.getCredentials("elasticsearch");
config.setCredentials('elasticsearch', {
    type: 'es',
    host: process.env.ELASTIC_HOST,
});

describe("EdgeElastic", () => {
    let elastic: any;

    // Act before assertions
    before(async () => {
        elastic = EdgeElastic.getElastic();
    });

    after(async() => {
        await elastic.doDeleteIndex({ index: 'image' });
    });

    it('Should be created with id', async() => {
        let params = {
            doc: { "text": "refrigerator", "confidence": "0.55561167" },
            id: "AVpIH3W-qJIlx09f4vtr",
            dataset: "image",
            collection: "google"
        };

        const result = await elastic.doUpdate(params);
        result.should.have.property('_index', params.dataset);
        result.should.have.property('_type', params.collection);
        result.should.have.property('result', 'created');
        result.should.have.property('_id', params.id);
    });


    it('Should be updated with id', async() => {
        let params = {
            doc: { "text": "refrigerator_updated", "confidence": "0.55561167" },
            id: "AVpIH3W-qJIlx09f4vtr",
            dataset: "image",
            collection: "google"
        };

        const result = await elastic.doUpdate(params);
        result.should.have.property('_index', params.dataset);
        result.should.have.property('_type', params.collection);
        result.should.have.property('result', 'updated');
        result.should.have.property('_id', params.id);
    });

    it('Should be able to get a list of indexes', async () => {
        let result = await elastic.doGetIndexes();
        result.should.containEql('image');
    });

    //
    // A simple script to test the connection to Elastic
    //
    it("Should be Normal Elasticsearch if type is es",  async () => {

        if (credentials.type === 'es') {
            elastic.client.transport._config.should.have.property('type', 'es');
            elastic.client.transport._config.should.have.property('host');
        }
    });

    it('Should be able to get a list of mappings', async () => {
        const result = await elastic.doGetMapping({ index: 'image', type: 'google'});
        
        result.should.have.property('image');
        result.image.mappings.should.have.property('google');
        result.image.mappings.google.properties.should.have.property('confidence');
        result.image.mappings.google.properties.should.have.property('text');
        result.image.mappings.google.properties.text.should.have.property('type', 'text');
    });

    it('Should be get document', async() => {
        let body = { "id": "AVpIH3W-qJIlx09f4vtr" };
        let dataset = "image";
        let collection = "google";

        const result = await elastic.doGet({dataset, collection, body});
        result.should.have.property('_index', dataset);
        result.should.have.property('_type', collection);
        result.should.have.property('_source');
        result.should.have.property('_id', body['id']);
    });

    it('Should be searched document', async () => {
        let query = 'query:product';
        let dataset = "image";
        let collection = "google";

        const result = await elastic.doSearch({dataset, collection, query});
        result.should.have.property('_scroll_id');
        result.should.have.property('took');
        result.should.have.property('_shards');
        result.should.have.property('hits');
        result._shards.should.have.property('total');
        result._shards.should.have.property('successful');
        result._shards.should.have.property('failed', 0);
        result.hits.should.have.property('hits');
    });

    it('Should be created document', async () => {
        let doc = { "text": "bedroom", "confident": "0.34233" };
        let dataset = "image";
        let collection = "google";
        let id = null;

        const result = await elastic.doIndex({dataset, collection, id, doc});
        
        result.should.have.property('_index');
        result.should.have.property('_type', collection);
        result.should.have.property('result', 'created');
        result.should.have.property('_id');

        // Should be cached
        const res = await elastic.doIndex({dataset, collection, id, doc});
        res.should.be.eql(true);
    });

    it('Should be created document with hash', async() => {
        let dataset = "image";
        let collection = "google";
        let id = "AVpIH3W-qJIlx09f4vtr";
        let body = { "text": "bedroom", "confident": "0.34233" };
        let subpath = "sub";

        const result = await elastic.setDocumentHash({dataset, collection, id, body, subpath});
        result.should.have.property('_index', "dochash");
        result.should.have.property('_type', "path");
        result.should.have.property('result');
        result.should.have.property('_id', dataset + '_' + collection + '_' + id);
    });

    it('Should be created document with hash', async() => {
        let dataset = "image";
        let collection = "google";
        let id = "AVpIH3W-qJIlx09f4vtr";

        const result = await elastic.doGetDocumentHash({dataset, collection, id});
        result.should.have.property('_index', "dochash");
        result.should.have.property('_type', "path");
        result.should.have.property('_id', dataset + '_' + collection + '_' + id);
        result.should.have.property('found', true);
        result.should.have.property('_source');
        result._source.should.have.property("hashes");
    });

});