"use strict";
//
// Base class for DataFormatterType
//
Object.defineProperty(exports, "__esModule", { value: true });
class DataFormatterType {
    constructor() {
        this.name = '';
        this.width = 80;
        this.editorShowing = false;
        this.editorPath = '';
        // add constructor code here
    }
    //
    // funtion to open the dynamic default text editor
    //
    // @param [JqueryElement] elParent parent element where text editor needs to be displayed
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                type: "text",
                class: "dynamic_edit form-control"
            });
            this.appendEditor();
        }
        this.elEditor.css({
            position: "absolute",
            "z-index": 5001,
            top,
            left,
            width,
            height
        });
        this.elEditor.val(currentValue);
        this.elEditor.show();
        this.elEditor.focus();
        this.elEditor.select();
        // @ts-ignore
        globalKeyboardEvents.once("global_mouse_down", this.onGlobalMouseDown);
        return true;
    }
    //
    //  Attach to an existing <input> tag which is likely used for an inline form.
    //  As the input exists, for most Data Types we can re-use it.  Others
    //  may hide it an append their own but if you do, remember to update the other's value
    //
    attachEditor(elSourceInput, path) {
        //
        //  Nothing to for a normal text editor
        // if @input_width?
        // elSourceInput.css "width", @input_width
        if (this.align != null) {
            elSourceInput.css("text-align", this.align);
        }
    }
    //
    // funtion to get the formatted data fromt the datatype
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additional options to be used in the format for ex. options for select
    // @param [String] path the path where the formatted data needs to be returned
    // @return null
    //
    format(data, options, path) {
        return null;
    }
    //
    // funtion to remove formating applied and get raw data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path the path where the formatted data needs to be returned
    // @return null
    //
    unformat(data, path) {
        return null;
    }
    //
    // funtion to allow specific key stroke on datatype
    //
    // @param [Integer] keycode keycode that is allowed
    // @return [Boolean]
    //
    allowKey(keyCode) {
        return true;
    }
    //
    // funtion to edit the data displayed using data type
    //
    // @param [JqueryElement] parentElement parent Element of the currently rendered data
    // @param [Object] currentValue currentValue of the datatype
    // @param [String] path the path where the data is being displayed
    // @param [Function] onSaveCallback the function that should be called when data is updated
    //
    editData(parentElement, currentValue, path, onSaveCallback) {
        this.onSaveCallback = onSaveCallback;
        let left = 0;
        let top = 0;
        let width = 100;
        let height = 40;
        let elParent = null;
        this.editorPath = path;
        if (parentElement != null) {
            elParent = $(parentElement);
            const pos = elParent.position();
            ({ left } = pos);
            ({ top } = pos);
            width = elParent.outerWidth(false);
            height = elParent.outerHeight(false);
            ({ left } = elParent.offset());
            ({ top } = elParent.offset());
        }
        this.editorShowing = true;
        return this.openEditor(elParent, left, top, width, height, currentValue, path);
    }
    //
    // funtion to save the new value to the datatype holder element
    //
    // @param [Object] newValue new value that needs to be updateds to be returned
    // @return [Boolean]
    //
    saveValue(newValue) {
        newValue = this.unformat(newValue, this.editorPath);
        if (this.onSaveCallback != null) {
            this.onSaveCallback(this.editorPath, newValue);
        }
        return true;
    }
    //
    // funtion to append the editor html to the document
    //
    appendEditor() {
        $("body").append(this.elEditor);
        this.elEditor.on("blur", (e) => {
            if (this.elDataEditor) {
                // ts-ignore
                if ($(this.elDataEditor).css("display") !== "none") {
                    return false;
                }
            }
            if (this.editorShowing) {
                this.editorShowing = false;
                e.preventDefault();
                e.stopPropagation();
                this.elEditor.hide();
                return true;
            }
            return false;
        });
        //  Close the popup with the escape key
        this.elEditor.on("keyup", (e) => {
            if (e.keyCode === 9) {
                //  Tab key handles save
                this.saveValue(this.elEditor.val());
                this.editorShowing = false;
                this.elEditor.hide();
                // Send out a tab key
                // if globalKeyboardEvents?
                // globalKeyboardEvents.emitEvent "tab", [ e ]
                return false;
            }
            if (e.keyCode === 13) {
                this.saveValue(this.elEditor.val());
                this.editorShowing = false;
                this.elEditor.hide();
                return false;
            }
            if (e.keyCode === 27) {
                this.editorShowing = false;
                this.elEditor.hide();
                return false;
            }
            if (this.allowKey(e.keyCode)) {
                return true;
            }
            else {
                return false;
            }
        });
        return $("document").on("click", (e) => {
            return console.log("Click");
        });
    }
    //
    // Trigger when the mouse goes down on any place in the window, this
    // will remove the input field if you click outside the cell.
    //
    onGlobalMouseDown(e) {
        // if e.target.className == "dynamic_edit"
        if (e.target.classList.contains("dynamic_edit")) {
            // This line is disabled because it causes an infinitive loops when once called
            // globalKeyboardEvents.once "global_mouse_down", @onGlobalMouseDown
            return true;
        }
        this.editorShowing = false;
        if (this.elEditor) {
            this.elEditor.hide();
        }
        return true;
    }
    renderTooltip(row, value, tooltipWindow) {
        if (!value) {
            return false;
        }
        value = this.getValueInTooltip(value);
        if (Array.isArray(value)) {
            value = value.join(', ');
        }
        if ((typeof value === "string")) {
            // @ts-ignore
            let w = textWidth(value) + 20;
            let h = (20 * (Math.floor(w / 400) + 1)) + 15;
            if (value.length > 100) {
                w = 440;
            }
            if (value.length > 200) {
                w = 640;
            }
            if (value.length > 300) {
                h = 440;
            }
            tooltipWindow.setSize(w, h);
            tooltipWindow.getBodyWidget().addClass("text");
            tooltipWindow.html(value);
            return true;
        }
        // console.log("renderTooltip row=", row, "value=", value);
        return false;
    }
    getValueInTooltip(value) {
        return value;
    }
}
exports.DataFormatterType = DataFormatterType;
exports.default = DataFormatterType;
//# sourceMappingURL=DataFormatterType.js.map