##|
##|  Load a CoffeeNinjaCommon Module
##|  Returns a promise that will complete once the module is loaded.
##|
##|  The promise is cached so that module is loaded once and efficient to call this
##|  function many times for the same module.
##|
jQuery  = $ = require 'jquery'
co          = require 'co'
Handlebars  = require 'handlebars'
IScroll     = require 'iscroll'
Draggabilly = require 'draggabilly'
EvEmitter   = require 'ev-emitter'
flatpickr   = require 'flatpickr'
numeral     = require 'numeral'
moment      = require 'moment'
math        = require 'mathjs'
require 'jquery-range'

## -------------------------------------------------------------------------------------------------------------
## Load a script, only once.  Then return from the promise after it's loaded
## Safe to call many times for the same script and it quickly returns the already
## resolved promise.
##
## @param url [string] Script to load
##
doLoadScript = (url) ->

    if Scripts[url]?
        return Scripts[url]

    Scripts[url] = new Promise (resolve, reject) ->

        oScript = document.createElement "script"
        oScript.type = "text/javascript"
        oScript.onerror = (oError) ->
            console.log "[#{url}] Script load error:", oError.toString()
            resolve(true)

        oScript.onload = ()->
            # console.log "Script loaded:", url
            resolve(true)

        head = document.head || document.getElementsByTagName("head")[0];
        head.appendChild oScript
        oScript.src = url

## -------------------------------------------------------------------------------------------------------------
## Load a CSS file only once.
##
## @param url [string] CSS file to load
##
doLoadStyle = (url) ->

    if StyleManager[url]?
        return StyleManager[url]

    StyleManager[url] = $ "<link rel='stylesheet' type='text/css' href='#{url}' />"
    $("head").append StyleManager[url]
    return StyleManager[url]
