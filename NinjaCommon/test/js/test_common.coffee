allTests        = []
counter         = 0
wgtMain         = null
viewTestCode    = null
viewTestCases   = null
viewTestExecute = null

##|
##|  Common function to load test data
##|  Load a list of zipcodes
loadZipcodes = ()->

	##|
	##|  Load the zipcode data before the test begins
	new Promise (resolve, reject) ->

		$.get "/js/test_data/zipcodes.json", (allData)->

			counter = 0
			for rec in allData.data
				rec.Weather = "https://www.wunderground.com/cgi-bin/findweather/getForecast?query=pz:#{rec.code}&zip=1"
				DataMap.addDataUpdateTable "zipcode", rec.code, rec

			resolve(true)

loadIcons = (callback)->
	String::startsWith ?= (s) ->
		@slice(0, s.length) == s
	new Promise (resolve, reject)->
		$.get "/vendor/icons.json", (allData)->
			arrayData = []
			for idx, rec of allData
                icon = if (""+rec).startsWith "fa" then '<i class="fa '+rec+'"></i>' else '<i class="si '+rec+'"></i>'
                DataMap.addDataUpdateTable "icon", idx, {icon: icon, value: rec}
                arrayData[idx] = rec

			if callback isnt undefined or callback?
				callback(arrayData)
			resolve(true)

loadStockData = ()->

	new Promise (resolve, reject)->

		$.get "/js/test_data/stocks.json", (allData)->
			for idx, rec of allData
				delete rec._id
				DataMap.addDataUpdateTable "stocks", rec.Ticker, rec

			resolve(true)

loadDatafromJSONFile = (fileName) ->
	new Promise (resolve, reject)->

		$.get "js/test_data/#{fileName}.json", (allData)->
			for rec in allData
				DataMap.addDataUpdateTable "#{fileName}", rec.id, rec

			resolve(true)

loadDatafromMarkDown = (fileName) ->
	new Promise (resolve, reject)->

		$.get "js/test_data/markdown/#{fileName}.md", (allData)->
			resolve(allData)

addHolder = (name) ->
	return viewTestExecute.addHolder(name)

addHolderWidget = (name, w, h)->
	w = viewTestExecute.addHolder name
	w.setSize(w, h)
	return w

allTestData = {}

##|
##|  Very simple call that takes a label (some notes for the user) and
##|  a javascript block to execute as the test.   The code and label are
##|  displayed to the user, the block is executed by a timer in the "go"
##|  function.   Each block is executed in turn.
##|
addTest = (label, fnCall, code, test_id) ->

	if !test_id? then test_id = GlobalValueManager.NextGlobalID()

	if !code? or not code
		code = fnCall.toString()
		code = code.replace /</g, "&lt;"
		code = code.replace />/g, "&gt;"
		code = code.replace "function () {\n", ""
		code = code.replace /[\r\n]*}$/, ""
		code = code.replace "\n", "<br>"

	html = "
		<div class='test_case' id='case_#{test_id}'>
			<div class='row'>
				<div class='test_label col-xs-9' id='test_#{test_id}'> #{label} </div>
				<div class='test_result col-xs-3' id='result_#{test_id}'> </div>
			</div>
			<div class='row'>
				<div class='test_comment col-xs-12' id='comment_#{test_id}'></div>
			</div>
		</div>
	"

	el = $(".testCases").append($(html))

	## get CoffeeScript code of ths test case
	#console.log originalCode
	## replace special character e.g. '('
	## to make it validate in regexp
	replacer = (match) ->
		return '\\' + match
	label =label.replace /[\(\)\&\*\%\$\!]/g, replacer

	# get comment
	reEnd1 = new RegExp 'addTest \\"' + "#{label}" + '\\"', "i"
	reEnd2 = new RegExp 'addTestButton \\"' + "#{label}" + '\\"', "i"
	oLines = originalCode.split "\n"
	comment = ""
	for line in oLines
		ma = line.match(reEnd1) || line.match(reEnd2)
		if ma
			break;
		maCom = line.match(/(^\s*|\t)\#\#\|(.*)/i) || line.match(/(^\s*|\t)\#\#(.*)/i) || line.match(/(^\s*|\t)\#(.*)/i)
		if maCom and (maCom[2] or maCom[2] is '')
			if maCom[2].trim() isnt ''
				if comment is ''
					comment += maCom[2].trim()
				else
					comment += ' ' + maCom[2].trim()
		else if line.trim() isnt ''
			comment = ''
	if comment isnt ''
		el.find("#comment_#{test_id}").text comment

	re1 = new RegExp 'addTest \\"' + "#{label}" + '\\"(.|[\\t\\n])*?(addTest|go\\(\\))', "i"
	re2 = new RegExp 'addTestButton \\"' + "#{label}" + '\\"(.|[\\t\\n])*?(addTest|go\\(\\))', "i"
	arrMatchingString = originalCode.match(re1) || originalCode.match(re2)

	matchingString = arrMatchingString[0]
	lines = matchingString.split "\n"
	lines.splice 0,1
	lines.splice lines.length-1, 1
	trimmedLines = lines.filter (line, index) =>
		## if the line is empty or commented out, then remove it
		if line.trim()[0] == '#'
			return false
		else if line is ""
			return false
		else
			return true
	coffeeCode = trimmedLines.join '\n'
	coffeeCode = coffeeCode.replace /</g, "&lt;"
	coffeeCode = coffeeCode.replace />/g, "&gt;"
	label = label.replace " (", "<br>("
	allTestData[test_id] =
		label   	: label
		callback	: fnCall
		code    	: code
		tag     	: $("#result_" + test_id)
		coffeeCode	: coffeeCode

	elTestCase = el.find("#case_#{test_id}")
	elTestCase.attr "data-id", test_id
	elTestCase.bind "click", (e)=>
		id = $(e.currentTarget).attr("data-id")
		viewTestCode.setCoffeeCode allTestData[id].coffeeCode
		viewTestCode.setJSCode allTestData[id].code
		console.log "CLICK=", id, e
		true

	allTests.push allTestData[test_id]
	true

addTestButton = (label, buttonText, fnCall) ->

	code = fnCall.toString()
	code = code.replace "<", "&lt;"
	code = code.replace ">", "&gt;"
	code = code.replace "function () {\n", ""
	code = code.replace /[\r\n]*}$/, ""
	code = code.replace "\n", "<br>"
	test_id = GlobalValueManager.NextGlobalID()
	addTest label, ()->

		button = $ "<div />",
			class: "ninja-btn btn btn-primary"
			html: buttonText
		.bind 'click', (e) ->
			e.preventDefault()
			e.stopPropagation()
			viewTestCode.setCoffeeCode allTestData[test_id].coffeeCode
			viewTestCode.setJSCode allTestData[test_id].code
			fnCall(e)
	, code, test_id

addMarkdownfile = (filename) ->
	el = $("#mainTestArea")
	el.empty()
	wgtMain = new WidgetTag(el)
	wgtMain.setAbsolute()
	onResizeWindow()
	loadDatafromMarkDown filename
	.then (content) =>
		wgtMain.setView "MarkDown", (view) =>
			view.setHTML content
			true

goNext = () ->

	allTests.splice 0, 1
	setTimeout () ->
		go()
	, 100

go = () ->

	if allTests.length == 0

		return

	else

		try
			result = allTests[0].callback()

			if typeof result == "function"
				result = result()

			# console.log "Test common: result = ", result

			if typeof result != "object"
				$(allTests[0].tag).css "padding-right", "20px"
				$(allTests[0].tag).html result
				goNext()
			else

				if result.constructor.toString().match /Promise/

					result.then (trueResult) ->
						$(allTests[0].tag).css "padding-right", "20px"
						$(allTests[0].tag).append trueResult
						goNext()
					.catch (e)  ->
						strText = e.toString()
						strText += "<br>" + e.stack

						$(allTests[0].tag).append "Exception:" + strText

				else

					$(allTests[0].tag).append result
					goNext()

		catch e

			console.log "Exception:", e
			console.log "Stack:", e.stack

			$(allTests[0].tag).html "<div class='exception'>Exception: " + e + "</div>"

##|
##|  Setup the main widget that holds everything
onResizeWindow = ()->

	winw = $(window).width()
	winh = $(window).height()
	offset = wgtMain.offset()

	##|
	##|  Remove space on the left for that left side menu
	##|  todo: use css to measure
	offset_left = 250
	wgtMain.move offset_left, 0, winw-offset_left, winh
	true

$ ->

	console.log "TestCommon 1.0"

	globalTableEvents.on "set_custom", (tableName, source, field, newValue)=>
		console.log "globalTableEvents: table=#{tableName} source=#{source} field=#{field} new=", newValue
		true

	globalTableEvents.on "row_selected", (tableName, id, status)=>
		console.log "globalTableEvents: rowSelected tableName=#{tableName}, id=#{id}, status=#{status}"
		true

	el = $("#mainTestArea")
	wgtMain = new WidgetTag(el)
	wgtMain.setAbsolute()
	onResizeWindow()

	mainWidth = wgtMain.width()
	console.log "Main width: ", mainWidth

	##|
	##|  Add a splitter
	wgtMain.setView "Splittable", (splitter1)->

		splitter1.getFirst().setView "TestExecute", (view)->
			viewTestExecute = view

		splitter1.setPercent 80
		splitter1.getSecond().setMinWidth(if mainWidth < 800 then 200 else 400)
		splitter1.getSecond().setView "Splittable", (splitter2)->
			splitter2.setHorizontal()
			splitter2.setPercent 50
			splitter2.getFirst().setView "TestCases", (view)->
				viewTestCases = view
				view.setScrollable()
				onResizeWindow()

			splitter2.getSecond().setView "TestCode", (view)->
				viewTestCode = view

		wgtMain.resetSize()
		splitter1.resetSize()

	, 80
	page = ""
	if document.location.search?
		m = document.location.search.match /page=(.*)/
		if m? and m[1] then page = m[1]
			#

	if page? and page.length
		$("a##{page}").closest("li[data-toggle=collapse]").click()
		$("a##{page}").addClass "hovered"
	window.elTestCase = $("#testCases")

	##|
	##|  Watch for browser changes
	$(window).on "resize", onResizeWindow



