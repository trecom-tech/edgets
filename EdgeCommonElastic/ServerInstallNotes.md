## Notes for Azure

Article on how to add swap space to an Azure server
https://blogs.msdn.microsoft.com/linuxonazure/2017/04/10/how-to-add-swap-to-linux-vms-on-azure/

Add to /etc/sysctl.conf
(reference: https://www.percona.com/blog/2016/08/12/tuning-linux-for-mongodb/)

	vm.dirty_ratio = 15
	vm.dirty_background_ratio = 5
	vm.swappiness = 1
	net.core.somaxconn = 4096
	net.ipv4.tcp_fin_timeout = 30
	net.ipv4.tcp_keepalive_intvl = 30
	net.ipv4.tcp_keepalive_time = 120
	net.ipv4.tcp_max_syn_backlog = 4096


Install a clock source
	apt-get install ntp

## ElasticSearch Instructions:

### 1. Install Oracle Java 8

$ add-apt-repository ppa:webupd8team/java

$ apt update; apt install oracle-java8-installer

### 2. Download and install ElasticSearch

$ wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.3.0.deb

$ sha1sum elasticsearch-5.3.0.deb

$ dpkg -i elasticsearch-5.3.0.deb

$ /bin/systemctl daemon-reload

$ /bin/systemctl enable elasticsearch.service

$ systemctl start elasticsearch.service

$ rm elasticsearch-5.3.0.deb

Check if everything is up and running:
$ curl -XGET 'localhost:9200/?pretty'

### 3. Configure ElasticSearch path

#### 3.1 Edit elasticsearch config

$ vim /etc/elasticsearch/elasticsearch.yml

Uncomment path.data property and set its value: 

path.data: /disk2/elastic

Write file and exit

#### 3.2 Create directory and change permissions

$ mdkir /disk2/elastic

$ chown -R elasticsearch:elasticsearch /disk2/elastic

$ systemctl restart elasticsearch.service

Check if everything is up and running again
$ curl -XGET 'localhost:9200/?pretty'

### 4. Setup security on ElasticSearch

$ apt install -y nginx

We need apache2-utils to get htapasswd tool

$ apt install apache2-utils

$ htpasswd -c /etc/nginx/.es_admin sa

$ vim /etc/nginx/sites-available/default

Enter configuration below default configuration:
	
	# elasticsearch configuration
	server {
		listen *:9201;
		server_name db.rrportal.net;
		access_log /var/log/nginx/elastic.log;
		error_log /var/log/nginx/elastic.error.log;
		location / {
			proxy_pass http://127.0.0.1:9200;
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			auth_basic "Restricted";
			auth_basic_user_file "/etc/nginx/.es_admin";
		}
	}

$ service nginx restart

### 5. Download and install Kibana

$ wget https://artifacts.elastic.co/downloads/kibana/kibana-5.3.0-amd64.deb

$ sha1sum kibana-5.3.0-amd64.deb

$ dpkg -i kibana-5.3.0-amd64.deb

$ /bin/systemctl daemon-reload

$ /bin/systemctl enable kibana.service

$ rm kibana-5.3.0-amd64.deb

$ systemctl start kibana.service

### 6. Setup security on Kibana

$ htpasswd -c /etc/nginx/.kibana_admin sa

$ vim /etc/nginx/sites-available/default

Append configuration at the end of file

	# kibana configuration
	server {
		listen *:5602;
		server_name db.rrportal.net;
		access_log /var/log/nginx/kibana.log;
		error_log /var/log/nginx/kibana.error.log;
		location / {
			proxy_pass http://127.0.0.1:5601;
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			auth_basic "Restricted";
			auth_basic_user_file "/etc/nginx/.kibana_admin";
		}
	}

$ service nginx restart