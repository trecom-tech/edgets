import EdgeApi            from 'edgeapi';
import { expect }         from 'chai';
import { addServiceUser } from '../src/utility/AddServiceUser';

describe('ProviderAuth', () => {

    let api1: EdgeApi;
    let api2: EdgeApi;
    let api3: EdgeApi;
    let api4: EdgeApi;

    const testUser1 = {
        id: 'tao',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'user1@protovate.com',
        partner_id: 1,
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
            'global': [
                'serviceUser',
            ]
        },
        'attrib': {
            'lunch': 'Pizza',
        },
    };

    const testUser2 = {
        id: 'wang',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'user2@protovate.com',
        partner_id: 1,
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
        },
        'attrib': {
            'lunch': 'Pizza',
        },
    };

    const normalUserGroup = {
        id: 'NormalUserGroup',
        name: 'NormalUserGroup',
        description: 'Test Group for normal users',
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
            'global': [
                'serviceUser',
            ]
        },
        members: [
            'tao',
            'wang',
            'roy',
        ],
    };

    const testUser3 = {
        id: 'taotest',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'user2@protovate.com',
        partner_id: 1,
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
        },
        'attrib': {
            'lunch': 'Pizza',
        },
    };

    const normalUserGroupOnlyWithTestSystem = {
        id: 'NormalUserGroupWithTest',
        name: 'NormalUserGroupWithTest',
        description: 'Test Group for normal users',
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ]
        },
        members: [
            'taotest'
        ],
    };

    const testAdmin = {
        id: 'admin',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'admin@protovate.com',
        partner_id: 1,
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
                'adminGroupManager',
            ],
            global: [
                'serviceUser',
                'adminManageUsers',
                'adminGroupManager',
            ]
        },
        'attrib': {
            'lunch': 'Pizza',
        },
    };

    const adminGroup = {
        id: 'AdminUserGroup',
        name: 'AdminUserGroup',
        description: 'Test Group for admin users',
        roles: {
            'test': [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
                'adminGroupManager',
            ],
            global: [
                'serviceUser',
                'adminManageUsers',
                'adminGroupManager',
            ]
        },
        members: [
            'admin',
        ],
    };

    // Act before assertions
    before(async () => {
        api1 = new EdgeApi();
        await api1.doConnect();

        api2 = new EdgeApi();
        await api2.doConnect();

        api3 = new EdgeApi();
        await api3.doConnect();

        api4 = new EdgeApi();
        await api4.doConnect();

        // clean up user table
        await api1.data_doRemove('master', 'user');
        // clean up groups table
        await api1.data_doRemove('lookup', 'groups');
        // add service user back
        await addServiceUser();

        // Setup test groups;
        await api1.data_doUpdatePath('lookup', `/groups/${normalUserGroup.id}`, normalUserGroup);
        await api1.data_doUpdatePath('lookup', `/groups/${normalUserGroupOnlyWithTestSystem.id}`, normalUserGroupOnlyWithTestSystem);
        await api2.data_doUpdatePath('lookup', `/groups/${adminGroup.id}`, adminGroup);

        // Setup test user;
        await api1.data_doUpdatePath('master', `/user/${testUser1.id}`, testUser1);
        await api2.data_doUpdatePath('master', `/user/${testUser2.id}`, testUser2);
        await api3.data_doUpdatePath('master', `/user/${testAdmin.id}`, testAdmin);
        await api4.data_doUpdatePath('master', `/user/${testUser3.id}`, testUser3);
    });

    describe('doAuthUser', () => {
        it('should return user for correct credentials', async () => {
            const result1 = await api1.auth_doAuthUser('tao', 'pass', 'test');
            expect(result1.id).to.equal('tao');
            const result2 = await api2.auth_doAuthUser('wang', 'pass', 'test');
            expect(result2.id).to.equal('wang');
            const result3 = await api3.auth_doAuthUser('admin', 'pass', 'test');
            expect(result3.id).to.equal('admin');
        });

        it('should return success for incorrect role but if user has global role', async () => {
            const result = await api1.auth_doAuthUser('tao', 'pass', 'incorrectsystem');
            expect(result.id).to.equal('tao');
        });

        it.skip('should return success for incorrect role and user does not have global role', async () => {
            const result = await api1.auth_doAuthUser('taotest', 'pass', 'incorrectsystem');
            expect(result.error).to.equal('Invalid role');
        });

        it('should return error for incorrect credentials', async () => {
            const result = await api1.auth_doAuthUser('tao', 'incorrectpass', 'test');
            expect(result.error).to.equal('Incorrect user password');
        });

        it('should return error for incorrect user', async () => {
            const result = await api1.auth_doAuthUser('tao1', 'incorrectpass', 'test');
            expect(result.error).to.equal('Can not find user with provided username');
        });
    });

    describe('doAuthFromToken', () => {
        it('should return user for correct token', async () => {
            const result1 = await api1.auth_doAuthUser('tao', 'pass', 'test');
            expect(result1.id).to.equal('tao');

            const token = result1.authToken;
            const tokenUser = new EdgeApi();
            await tokenUser.doConnect();

            const result = await tokenUser.auth_doAuthFromToken(token);
            expect(result.id).to.equal('tao');
            expect(result.authToken).to.equal(token);
        });
    });

    describe('doChangePassword', () => {

        // TODO should enable this test case when we enable the role verification function
        it.skip('should return error for updating password of other user', async () => {
            const result = await api1.auth_doChangePassword('wang', 'newpass');
            expect(result.error).to.include('not authorized for');
        });

        it('admin can change password of other user', async () => {
            const result = await api3.auth_doChangePassword('wang', 'newpass');
            expect(result).to.equal(true);
        });

        it('should update password correctly', async () => {
            const result = await api1.auth_doChangePassword('tao', 'newpass');
            expect(result).to.equal(true);

            // verify if the password is updated correctly
            const updatedUser = await api1.data_doGetItem('master', `/user/${testUser1.id}`);
            expect(updatedUser.password).not.to.equal(testUser1.password);
        });

        it('should be able to login with updated password', async () => {
            const result = await api1.auth_doAuthUser('tao', 'newpass', 'test');
            expect(result.id).to.equal('tao');
        });
    });

    describe('doCreateUser', () => {

        it('should create new user with admin', async () => {
            const result = await api3.auth_doCreateUser('rodrigo');
            expect(result.password).to.be.a('string');
        });

        it('should return error with non admin user', async () => {
            const result = await api1.auth_doCreateUser('tao1');
            expect(result.error).to.equal('Not allowed to make this api call');
        });

        it('should return error with invalid username', async () => {
            const result = await api3.auth_doCreateUser('#invalid');
            expect(result.error).to.equal('Username is not valid');
        });

        it('should return error for already existing user', async () => {
            const result = await api3.auth_doCreateUser('tao');
            expect(result.error).to.equal('Username already exists');
        });
    });

    describe('doUpdateUser', () => {

        it('should update name of user', async () => {
            const result = await api1.auth_doUpdateUser('tao', { name: 'Tao Wang'});
            expect(result).to.equal(true);
        });

    });

    describe('doUpdateUserRoles', () => {

        it('should update role of user', async () => {
            const result = await api3.auth_doUpdateUserRoles('tao', 'global', ['user']);
            expect(result).to.equal(true);
        });

    });

    describe('doGroupCreate', () => {
        it('should create a group', async () => {
            const result = await api3.auth_doGroupCreate('protovate', 'Protovate Test Group');
            expect(result.name).to.equal('protovate');
        });
    });

    describe('doGroupAddRole', () => {
        it('should add a role to a group', async () => {
            const result = await api3.auth_doGroupAddRole('protovate', 'test', 'testRole');
            expect(result).to.equal(true);
        });
    });

    describe('doGroupRemoveRole', () => {
        it('should add a role to a group', async () => {
            const result = await api3.auth_doGroupRemoveRole('protovate', 'test', 'testRole');
            expect(result).to.equal(true);
        });
    });

    describe('doGroupAddUser', () => {
        it('should add a user to a group', async () => {
            const result = await api3.auth_doGroupAddUser('protovate', 'test', 'tao');
            expect(result).to.equal(true);
        });
    });

    describe('doGroupRemoveUser', () => {
        it('should remove a user from a group', async () => {
            const result = await api3.auth_doGroupRemoveUser('protovate', 'test', 'tao');
            expect(result).to.equal(true);
        });
    });

    describe('Test User', () => {
        it('should return user', async () => {
            const result = await api1.auth_doTestUser();
            expect(result).to.equal('tao');
        });
    });
});