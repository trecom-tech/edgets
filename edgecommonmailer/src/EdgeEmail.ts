/**
 * EdgeEmail Class
 * Includes mail validation and parsing markdown template
 */

import { Mail }                 from '@sendgrid/helpers/classes';
import EdgeMailData             from './EdgeMailData';
import EdgeCommonMailerError    from './EdgeCommonMailerError';
import config                   from 'edgecommonconfig';

const MarkdownIt = require('markdown-it');
const md = new MarkdownIt({
    html: true
});
const path = require('path');
const fs = require('fs');
const Datauri = require('datauri');
const datauri = new Datauri();


/**
 * Email wrapper of sendgrid email v3
 * https://sendgrid.com/docs/API_Reference/api_v3.html
 */

const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export default class EdgeEmail extends Mail {
    constructor(data: EdgeMailData) {
        super(data);

        if (data.mdTemplate) {
            this.parseTemplate(data.mdTemplate);
        }
    }

    parseTemplate(template: string) {
        const tokens        = md.parse(template);
        const imageParser   = md.renderer.rules['image'];
        const basePath      = config.getDataPath("images");

        md.renderer.rules['image'] = (tokens: any, idx: any, options: any, env: any, self: any) => {
            tokens.map((token: any) => {
                const srcIndex = token.attrs.map((attr: any) => attr[0]).indexOf('src');
                if (srcIndex !== -1) {
                    const externalLinkRegEx = /^http[s]*:\/\//i;
        
                    if (token.attrs[srcIndex][1] || !externalLinkRegEx.test(token.attrs[srcIndex][1])) {
                        
                        const filePath = path.join(basePath, token.attrs[srcIndex][1]);
        
                        if (fs.existsSync(filePath)) {
                            token.attrs[srcIndex][1] = datauri.encodeSync(filePath);
                        }
                    }
                }
            })
            return imageParser(tokens, idx, options, env, self);
        };

        const result = md.renderer.render(tokens, {});
        
        this.setContent([{
            type: 'text/plain',
            value: template
        }, {
            type: 'text/html',
            value: result
        }]);
    }
    
    validate() {
        const jsonBody = this.toJSON();
        
        if (!jsonBody.personalizations || jsonBody.personalizations.length === 0) {
            throw new EdgeCommonMailerError('Email Validation: should have at least one recipient group.');
        }

        jsonBody.personalizations.forEach(group => {
            const recipients = group.to as Array<any>;
            if (!recipients || recipients.length === 0) {
                throw new EdgeCommonMailerError('Email Validation: should have at least one recipient.');
            }

            recipients.forEach((to: any) => {
                if (!emailRegex.test(to.email)) {
                    throw new EdgeCommonMailerError(`Email Validation: ${to.email} is not valid.`);
                }
            })
        });

        if (!jsonBody.content || jsonBody.content.length === 0) {
            throw new EdgeCommonMailerError('Email Validation: should have at least one content body.');
        }

        if (!emailRegex.test(jsonBody.from.email)) {
            throw new EdgeCommonMailerError(`Email Validation: ${jsonBody.from.email} is not valid.`);
        }

        if (!jsonBody.subject) {
            throw new EdgeCommonMailerError(`Email Validation: subject is required`);
        }
    }
}