import { config }             from 'edgecommonconfig';
import { EdgeRateCapture }    from 'edgecommonratecapture';
import { StatusUpdateClient } from 'edgecommonstatusupdateclient';
import { EdgeTimeseriesData } from 'edgecommontimeseriesdata';
import { TimeSeriesDatabase } from 'edgecommontimeseriesdata/lib/EdgeTimeseriesData';
import {
    EdgeLauncher,
    NpmModuleTools
}                             from 'edgecommonlauncher';

const { argv }  = require('yargs');
const jsonfile  = require('jsonfile');
const os        = require('os');
const { spawn } = require('child_process');

let portNumber          = 50100;
const serverDetail: any = {};
let statusUpdateClient: StatusUpdateClient;

class AppRunner {
    config: any;
    log: any;
    myPort: number;
    basePath: string;
    homePath: string;

    constructor(runnerConfig: any) {
        this.config = runnerConfig;
        this.log    = config.getLogger('AppLauncher');
        this.myPort = ++portNumber;

        const pathParts = __dirname.split('/');
        pathParts.pop();

        this.basePath = pathParts.join('/') + '/';
        this.homePath = process.env.HOME;
    }

    runJob() {

        return new Promise((resolve, reject) => {

            this.log.info('Starting app', {
                    config: this.config,
                    port  : this.myPort
                }
            );

            try {

                const fullOutput = '';

                const startTime = new Date().getTime();

                //#|  Spawn the job
                const envCopy = {
                    ...process.env,
                    TERM   : 'HTML',
                    HOME   : this.homePath,
                    LANG   : 'en_US.UTF-8',
                    LOGNAME: process.env.USER,
                    SHELL  : '/bin/bash',
                    SHLVL  : '1',
                    TMPDIR : '/tmp/',
                };

                const commandLine = this.config.cmd;
                const path        = this.basePath + this.config.cwd;

                // console.log 'SPAWN PATH:', path
                // console.log 'HOME PATH :', @homePath

                const jobExec = spawn(commandLine, this.config.args, {
                        cwd: path,
                        env: envCopy
                    }
                );

                jobExec.on('error', (err: Error) => {
                    console.log(`-> AppLauncher in command=${commandLine} args=`, this.config.args);
                    console.log('-> Path  : ', path);
                    console.log('-> Error : ', err);

                    this.log.error('AppLaunch Error', {
                            job : this.config,
                            commandLine,
                            args: this.config.args,
                            err
                        }
                    );

                    return resolve(false);
                });

                const colorRemove = new RegExp('\x1b\[[0-9;]*m', 'g');
                statusUpdateClient.sendStatus({
                    type: 'appstart',
                    name: this.config.title,
                    host: serverDetail.host,
                    load: serverDetail.load_Avg,
                    cmd : this.config.cmd,
                    args: this.config.args,
                    pid : jobExec.pid,
                    port: this.myPort
                });

                jobExec.stdout.on('data', (data: any) => {
                    let str = jobExec.pid + ' ' + this.config.title + '> ';
                    while (str.length < 30) {
                        str += ' ';
                    }
                    str += data.toString();
                    process.stdout.write(str);

                    return true;
                });

                jobExec.stderr.on('data', (data: any) => {
                    console.log(this.config.title + ' [err]> ' + data.toString());
                    return true;
                });

                return jobExec.on('close', (code: any) => {
                    this.log.info('App complete', {
                            config: this.config,
                            port  : this.myPort
                        }
                    );

                    statusUpdateClient.sendStatus({
                        type: 'append',
                        name: this.config.title,
                        host: serverDetail.host,
                        pid : jobExec.pid,
                        port: this.myPort
                    });

                    return resolve(true);
                });

            } catch (e) {
                this.log.info('Exception', {
                        serverDetail,
                        e
                    }
                );

                config.reportException('Spawn issue:', e);
                return resolve({ error: 'Exception in job', e });
            }
        });
    }

    /**
     * Returns once the module has been fulling installed and updated
     */
    async doVerifyLoaded() {
        if (argv.fast != null) {
            return true;
        }

        if (this.config.npm != null) {
            console.log('Installing NPM ', this.config.npm);
            config.status('AppRunner doVerifyLoaded - Installing NPM ', this.config.npm);
            await NpmModuleTools.verifyInstalled(this.config.npm, this.config.cwd);
        }

        return true;
    }


    async runLoop() {


        const result = await this.runJob();

        config.status(`Module ${this.config.title} complete, updating install`);
        await this.doVerifyLoaded();

        config.status(`Module ${this.config.title} complete, waiting 20 seconds`);
        setTimeout(this.runLoop, 20000);

        return true;
    }
}


class AppLauncher {
    log: any;
    timeSeriesDatabase: TimeSeriesDatabase;

    constructor() {
        this.log               = config.getLogger('AppLauncher');
        serverDetail.host      = os.hostname();
        serverDetail.total_mem = Math.ceil(os.totalmem());
        setInterval(this.refreshStatus, 5000);

        const timeSeriesData = new EdgeTimeseriesData();
        timeSeriesData.doCreateDatabase('server_status')
            .then(db => {
                return this.timeSeriesDatabase = db;
            });
    }

    refreshStatus() {

        serverDetail.free_mem = Math.ceil(os.freemem());
        serverDetail.load_Avg = os.loadavg();

        if ((this.timeSeriesDatabase != null) && (this.timeSeriesDatabase.doSaveMeasurement != null)) {
            this.timeSeriesDatabase.doSaveMeasurement('status', { host: serverDetail.host }, {
                total_mem: serverDetail.total_mem,
                load     : serverDetail.load_Avg[0]
            });
        }

        return true;
    }

    /**
     * Takes in the list of jobs.
     * Makes sure each is loaded / downloaded
     * Returns an array of the AppRunners
     *
     * @param jobs
     */
    async doLoadAllApps(jobs: any[]) {

        const allApps: any[] = [];
        for (let job of Array.from(jobs)) {
            const app = new AppRunner(job);
            await app.doVerifyLoaded();
            allApps.push(app);
        }

        return allApps;
    }

    start() {

        this.log.info('AppLauncher Starting', { serverDetail });

        statusUpdateClient = new StatusUpdateClient();
        return statusUpdateClient.doOpenChangeMessageQueue()
            .then(() => {
                let filename;
                console.log('Starting AppLauncher on host ', serverDetail.host);

                try {
                    let dirs = ['.', '..', os.homedir() + '/EdgeConfig/'];
                    if (process.env.NODE_ENV === 'test') {
                        dirs = ['./test/'];
                    }

                    filename = 'AppLauncherConfig-#{serverDetail.host}.json';
                    let file = config.FindFileInPath(filename, dirs);

                    if (!file) {
                        filename = 'AppLauncherConfig.json';
                        file     = config.FindFileInPath(filename, dirs);
                    }

                    const jobs = jsonfile.readFileSync(file, 'utf-8');

                    return this.doLoadAllApps(jobs)
                        .then((allApps: any[]) => {

                            return Array.from(allApps).map((app) =>
                                app.runLoop());
                        });

                } catch (e) {
                    console.error(`There is no config file named ${filename} or ppLauncherConfig-${serverDetail.host}.json in directory ~/EdgeConfig/`);
                    this.log.info('AppLauncher start error', { e });
                    return process.exit();
                }
            });
    }
}

const runner = new AppLauncher();
runner.start();
