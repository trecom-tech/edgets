class ViewPasswordChange extends View
    @HASH_KEY_TEXT: "EDGEKEY1"

    getDependencyList: ()=>
        return ["/vendor/sha256.min.js"]
    
    getLayout: () =>
        resultObj = 
            view     : "Form"
            inputs   : [
                name   : "new_password"
                label  : "New Password"
                fieldType: "password"
            ,
                name   : "confirm_password"
                label  : "Confirm Password"
                fieldType: "password"
            ]
            onSubmit : (view) =>
                view.getValues()

                ## if new password doesn't  match with confirmed one, then it's invalid
                if view.new_password.getValue() isnt view.confirm_password.getValue()

                    new ModalForm
                        title  : "Password Error"
                        content: "Passwords do not match."

                    return false

                @password = sha256(view.new_password.getValue() + ViewPasswordChange.HASH_KEY_TEXT)

                # console.log "ViewPasswordChange is submitted...", view, @password
                if @password? and @path? and @path isnt ""
                    DataMap.getDataMap().updatePathValueEvent @path, view.new_password.getValue()
                    @emit "updated_password", [ @password, view.new_password.getValue() ]

                true

            callback : "initializeView" 

        if @optionData.hasOriginPassword is true

            resultObj.inputs.unshift 
                name     : "origin_password"
                label    : "Current Password"
                fieldType: "password"


        resultObj

    ## 
    ## Initialize current View
    initializeView: (@viewForm, @layoutOptions) =>
        console.log @viewForm.getWidth(), @viewForm.getHeight()
        @setSize @viewForm.getWidth(), @viewForm.getHeight()
        @onSubmitCallback = @viewForm.onSubmitCallback
        true

    ##
    ## Add a field for the origin password
    setOriginPassword: (passwordValueHash) =>
        @optionData.hasOriginPassword = true
        @password = passwordValueHash

        if !@viewForm? then return false

        ## 
        ## origin password input is the first one in viewForm.fields
        ## so it's easy to get it
        if @viewForm.fields.length is 0 then return false

        @inputPasswordValue = @viewForm.fields[0]
        @inputPasswordValue.validateFunction = (value, input) =>
            value_hashed = sha256(value + ViewPasswordChange.HASH_KEY_TEXT)

            if value_hashed is @password
                return input.setValid()
            else
                return input.setErrorMsg("Password doesn't match")

        true

    setPath: (@path) =>

    ##
    ## function to get ViewForm inside this view
    getViewForm: () =>
        if @viewForm?
            return @viewForm

        null

    ##
    ## function to get the current Password 
    getPassword: () =>
        @password

    ## 
    ## set initial options
    setData: (@optionData = {}) =>
        true

    onShowScreen: () =>
        GlobalClassTools.addEventManager(this)

    serialize: () =>
        return
            hasoriginpassword: @optionData.hasOriginPassword
            originpassword   : @password
            path             : @path

    deserialize: (options) =>
        new Promise (resolve, reject) =>

            if !options? or typeof options isnt "object"
                reject "ViewPasswordChange: invalid data passed"

            if options.hasoriginpassword? and options.originpassword?
                @setOriginPassword options.originpassword

            if options.path?
                @setPath options.path

            resolve true
