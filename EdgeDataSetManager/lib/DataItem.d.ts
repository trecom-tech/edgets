import DataSetPath from './DataSetPath';
export declare class DataItem {
    path: DataSetPath;
    db: any;
    data: any;
    basePath: string;
    base: any;
    id: string;
    /**
     * Initialize the Item requires an item id and store id
     * @param path
     * @param db
     * @param data
     */
    constructor(path: DataSetPath, db: any, data: any);
    /**
     * Internal virtual function that saves items or updates
     */
    doSaveUpdates(): Promise<boolean>;
}
export default DataItem;
