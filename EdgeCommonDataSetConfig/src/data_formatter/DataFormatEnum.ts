//
// class for enum data type
//
// @extends [DataFormatterType]
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

export class DataFormatEnum extends DataFormatterType {
    name: string  = 'enum';
    align: string = 'left';
    options: any;

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        //
        //  Show a popup menu
        // @ts-ignore
        const p = new PopupMenu("Options", left, top);
        if (typeof this.options === "string") {
            this.options = this.options.split(",");
        }

        if ((typeof this.options === "object") && (typeof this.options.length === "number")) {
            for (let i in this.options) {
                // console.log "Adding[", i, "][", o, "]"
                const o = this.options[i];
                p.addItem(this.format(o, this.options), (coords: any, data: any) => {
                        // console.log "Saving[", data, "]"
                        return this.saveValue(data);
                    }
                    , o);
            }
        } else {
            console.log("Invalid options: ", this.options);
        }

        return true;
    }

    //
    //  Attach to an existing text input.
    //  Hide the text input and add the select statement
    //  and then use selectize https://github.com/selectize/selectize.js/blob/master/docs/api.md
    attachEditor(elSourceField: any, path: any) {
        let name;
        let value;
        super.attachEditor(elSourceField, path);

        const options = DataFormatCommon.getOptions(elSourceField.data("options"));

        const config: any  = {};
        config.valueField  = "id";
        config.labelField  = "title";
        config.searchField = "title";
        config.options     = [];
        config.items       = [];

        const currentValue = elSourceField.val();
        if ((currentValue != null) && (currentValue !== "")) {
            config.items.push(currentValue);
        }

        if (Array.isArray(options.items)) {
            for (name of Array.from(options.items)) {
                config.options.push({id: name, title: this.format(name)});
            }
        } else {
            for (name in options.items) {
                value = options.items[name];
                config.options.push({id: value, title: this.format(name)});
            }
        }

        //
        //  Copy forward any config
        for (name in options) {
            value = options[name];
            if (name === "items") {
                continue;
            }
            config[name] = value;
        }

        config.onChange = (newValue: any) => {
            elSourceField.val(newValue);
            return true;
        };

        //
        elSourceField.hide();
        elSourceField.elSelect = $("<select />");
        elSourceField.parent().prepend(elSourceField.elSelect);
        const selectList = elSourceField.elSelect.selectize(config);
        // console.log "DataFormatEnum Create with options:", config
        elSourceField.elSelect.on("change", function (e: any) {
            elSourceField.change();
            return true;
        });

        elSourceField.val = (newValue: any) => {
            if ((newValue == null)) {
                return selectList[0].selectize.getValue();
            }
            // console.log "enum val(): ", newValue
            selectList[0].selectize.setValue(newValue, true);
            return newValue;
        };

        return true;
    }

    //
    // In this case, the options is an array or a comma seperated list of values
    // data must be one of those values of a numeric index to those values.
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options?: any, path?: any) {

        let i;
        let o;
        this.options = options;
        if (typeof this.options === "string") {
            this.options = this.options.split(/\s*,\s*/);
        }

        let result = data;

        if ((data == null)) {
            return "";
        }

        for (const i in this.options) {
            o = this.options[i];
            if (data === o) {
                result = o;
            }
        }

        for (const i in this.options) {
            o = this.options[i];
            if (`${data}` === `${i}`) {
                result = o;
            }
        }

        return result;
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        return data;
    }

    getValueInTooltip(value: any) {
        return this.format(value);
    }
}

export default DataFormatEnum;
