import config from 'edgecommonconfig';

export default class DummyJob {
    constructor() {
        console.log('This is a dummy module');
        console.log("Current path:", process.cwd());
    }

    run() {

    }
}
