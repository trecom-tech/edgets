class ViewConsoleOutput extends View

    restart: ()=>
        ##|  restart running timers
        @startTime = new Date()
        @viewConsoleOutputTable.html("")
        @viewConsoleOutputTable.parent().css "overflow", "auto"

    ##|
    ##| called when execute is done
    ##| @param data.total_ms {number} total execution time
    endRun: (data)=>
        if !data? then data = {}
        if !data.total_ms? then data.total_ms = new Date()-@startTime

        str = "<tr><td class='final'> " + numeral(data.total_ms/1000).format("#,###.###") + " sec </td><td> Execution complete, result = "
        str += @debugValue(data.result)
        str += "</td>"
        @viewConsoleOutputTable.append($(str))
        rowpos = $('#viewConsoleOutputTable tr:last').position();
        viewHolder = @viewConsoleOutputTable.parent()
        viewHolder.scrollTop(@viewConsoleOutputTable.height())
        @onResize viewHolder.width(), viewHolder.height()

    debugValue: (item)=>
        str = ""
        if typeof item == "string"
            str += "<span class='string'>" + item + "</span>"
        else if typeof item == "number"
            str += "<span class='num'>#{item}</span>"
        else if typeof item == "boolean"
            str += "<span class='num'>#{item}</span>"
        else
            str += "<span class='json'>" + JSON.stringify(item) + "</span>"
        return str

    ##|
    ##|  Start a line of output but make it spin
    startOutput: (dataString)=>

        now = new Date()
        diff = now - @startTime
        diff_time = numeral(diff/1000).format("#,###.###")

        output = {}
        output.row = $ "<tr class='working'></tr>"
        output.icon = $("<td> #{diff_time} </td>")
        output.text = $("<span>#{dataString}</span>")
        output.final = $("<span><i class='fa fa-spin fa-asterisk'></i></span>")
        output.start = new Date()
        output.finish = (newText)->
            diff = new Date()-output.start
            diff_time = numeral(diff/1000).format("#,###.##")
            output.final.html(diff_time + " ms")
            if newText? then output.text.html(newText)
            output.row.removeClass "working"
            true

        output.row.append output.icon
        output.row.append $("<td/>").append(output.text)
        output.row.append $("<td/>").append(output.final)

        @viewConsoleOutputTable.append(output.row)
        rowpos = $('#viewConsoleOutputTable tr:last').position();
        viewHolder = @viewConsoleOutputTable.parent()
        viewHolder.scrollTop(@viewConsoleOutputTable.height())
        @onResize viewHolder.width(), viewHolder.height()
        return output


    ##|
    ##|  Output data from the server
    ##|
    ##|  @param data.type == c  - Console output captured in values
    ##|  @param data.values {array} Array of values when type="c" for console
    ##|
    ##|  @param data.type == error - Display an error message
    ##|  @param data.values - Text error message to show
    ##|

    ##|
    addOutput: (data)=>

        if data? and typeof data == "string"
            ##|
            ##|  Simple form, string output
            tmp = data
            data = {}
            data.type = "c"
            data.values = [ tmp ]

        ##|
        ##|  If restart wasn't called then automatically restart
        if !@startTime?
            @restart()

        now = new Date()
        diff = now - @startTime
        diff_time = numeral(diff/1000).format("#,###.###")

        if data.time_ms
            if data.time_ms > 1000
                diff = numeral(data.time_ms/1000).format("#,###.###") + " sec"
            else
                diff = numeral(data.time_ms).format("#,###.###") + " ms"
        else
            data.time_ms = 0
            diff = diff_time

        str = "<tr><td>" + diff + "</td><td>"

        if data.type == "c"

            for item in data.values
                str += @debugValue(item)

            str += "</td><tr>"

        else if data.type == "error"

            str += "<span class='error'>" + data.values + "</span>"

        @viewConsoleOutputTable.append($(str))
        rowpos = $('#viewConsoleOutputTable tr:last').position();
        viewHolder = @viewConsoleOutputTable.parent()
        viewHolder.scrollTop(@viewConsoleOutputTable.height())
        @onResize viewHolder.width(), viewHolder.height()

    ##
    ## handle resize event for this view
    onResize: (w, h) =>
        if !@viewConsoleOutputTable? then return false
        ## check if there is vertical scrollbar on the view holder
        console.log "ViewConsoleOutput resized: ", w, h
        if @viewConsoleOutputTable.height() > h
            @viewConsoleOutputTable.width w-20
        else 
            @viewConsoleOutputTable.width w
        true

