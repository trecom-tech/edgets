import EdgeBlock from 'edgeblock';
export declare class AddressNormalizer extends EdgeBlock {
    constructor(...args: any[]);
    getInputs(): string[];
    getZipcodeData(zipcode: number): any;
    getZipWithCityState(city: string, state: string): string;
    getZipcodeFromCityState(city: string, state: string): Promise<any>;
    process(addressText: string, city?: string, state?: string, zip?: string): any;
    fixTitleCase(strTitleText: string): string;
}
export default AddressNormalizer;
