import StatusUpdateClient from '../src/StatusUpdateClient';

StatusUpdateClient.setGlobalStatusToFile(true);

let count                = 0;
const statusUpdateClient = new StatusUpdateClient("testing", "detaultType");
statusUpdateClient.doOpenChangeMessageQueue()
    .then(() => {
        const job      = statusUpdateClient.startJob("testing", { arg1: 1, arg2: 10 }, 1);
        const interval = setInterval(() => {
                console.log("Sending update ", count);
                statusUpdateClient.sendStatus({ counter: count });
                count++;
                if (count > 100) {
                    clearInterval(interval);
                    job.stop();
                    console.log("Done.");
                }
            }
            , 50
        );
    });

setTimeout(function () {
        console.log("Time to die");
        return process.exit();
    }
    , 25000);
