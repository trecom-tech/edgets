import { createConnection } from 'mysql';
import { config }           from 'edgecommonconfig';
import EdgeCommonMySQL      from '../src/EdgeCommonMySQL';

const should   = require('should');
const host     = process.env.HOST;
const user     = 'root';
const password = '12345678';

config.setCredentials('trialserver', {
    host: host,
    port: 3306,
    user: user,
    password: password,
    database: 'test_trialserver',
    options: {
        connectionLimit: 3,
        connectTimeout: 2000,
        queueLimit: 0,
        debug: false,
        timezone: 'UTC',
        waitForConnections: true
    }
});

const mysql = new EdgeCommonMySQL('trialserver');

describe('EdgeCommonMySQL', () => {

    before(() => {
        let connectionConfig: any = {
            host        : host,
            user        : user,
            password    : password
        };
        let connection = createConnection(connectionConfig);

        return new Promise((resolve, reject) => {

            connection.connect((err) => {
                if (err) reject(err);

                let sql = 'CREATE DATABASE test_trialserver;';
                connection.query(sql, (err) => {
                    if (err) reject(err);

                    sql = "CREATE TABLE `test` (\
                        `id` int(11) NOT NULL AUTO_INCREMENT,\
                        `name` varchar(100) DEFAULT 'testname',\
                        `birthday` date DEFAULT '2004-01-01',\
                        `is_verified` tinyint(4) DEFAULT '1',\
                        PRIMARY KEY (`id`)\
                        ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;";
                    connectionConfig.database = 'test_trialserver';
                    connection = createConnection(connectionConfig);
                    connection.query(sql, (err, result) => {
                        if (err) reject(err);
                        resolve(result);
                    });
                });
            });
        });
    });

    it('Run a query', () => {
        const sql = 'SELECT * FROM `test`';

        let result: any = mysql.doRunQuery(sql, null);
        result.should.equal(false);

        result = mysql.doRunQuery(sql, 'test');
        result.should.equal(false);
    });

    it('Insert records', () => {
        const sql = 'INSERT INTO `test` (name, birthday, is_verified)\
                       VALUES ("test1","2003-01-12",1),("test2","2003-01-13",0)'
        mysql.doRunQuery(sql, (err: any, result: any, fields: any) => {
            if (err) {
                should.not.exist(result);
            } else if (result.affectedRows) {
                result.affectedRows.should.equal(2);
            }
        });
    });

    it('Update a record', () => {
        const sql = 'UPDATE `test` \
                     SET birthday = "2003-01-20" \
                     WHERE name = "test1"';
        mysql.doRunQuery(sql, (err: any, result: any, fields: any) => {
            if (err) {
                should.not.exist(result);
            } else if (result.affectedRows) {
                result.affectedRows.should.equal(1);
            }
        });
    });

    it('Get records', () => {
        const sql = 'SELECT * FROM `test`';
        let countRows = 0;
        mysql.doRunQuery(sql, (err: any, result: any, fields: any) => {
            countRows ++;
            if (err) {
                should.not.exist(result);
            } else if (countRows === 2) {
                result.name.should.equal('test2');
                result.birthday.getTime().should.equal(new Date('2003-01-13').getTime());
                result.is_verified.should.equal(false);
            }
        });
    });

    it('Delete records', () => {
        const sql = 'DELETE FROM `test`';
        mysql.doRunQuery(sql, (err: any, result: any, fields: any) => {
            if (err) {
                should.not.exist(result);
            } else if (result.affectedRows) {
                result.affectedRows.should.equal(2);
            }
        });
    });

    after(() =>{
        const connectionConfig: any = {
            host        : host,
            user        : user,
            password    : password
        };
        const connection = createConnection(connectionConfig);

        return new Promise((resolve, reject) => {

            connection.connect((err) => {
                if (err) reject(err);

                let sql = 'DROP DATABASE `test_trialserver`';
                connection.query(sql, (err, result) => {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        });
    });

});