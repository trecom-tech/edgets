import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatInt extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    constructor(...args: any[]);
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): number | "";
    attachEditor(elSourceInput: any, path: any): any;
}
export default DataFormatInt;
