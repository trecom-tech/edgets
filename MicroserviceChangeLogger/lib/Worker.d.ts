export default class Worker {
    log: any;
    callback: any;
    changeStream: any;
    changeLogsTable: any;
    loggingTableName: string;
    csvLogEnabled: boolean;
    azureLogEnabled: boolean;
    pathSkip: any[];
    dmLookup: any;
    rules: any[];
    constructor(azureConfigName: string, azureTableName: string);
    doRun(callback?: any): void;
    setupSkipList(): void;
    private _doSaveChangesDatabase;
    private _doReloadRules;
    private _processChange;
    private _doSetupMongoChangeStream;
}
