# Understanding API Providers

## The basename and api names

All API Provider classes have a function called *getBaseName* that returns
a string.   This string becomes the first part of the API call.  For example:

    getBaseName() {
        return "customer"
    }
        
In this case, all API calls will start with "customer_" when exposed through the EdgeApi socket or /customer/ when exposed on the REST server.

##  Class Members

The *onInit* function is called by the consturctor of the base class and you should
use it for one time initialization.   Note that it is called async.

Any function with *internal* in the name is private and not exposed through the API.
By standards of programming, we should include internal functions at the bottom of 
the class.

Any function with do*SomeName* are exposed to the API automatically as well as the
parameters.   For example:

    getBaseName() {
        return "test"
    }
        
    #
    # Simple command that returns a string from the inputs
    doTestCommand(a, b) {
        return "X" + a + "Y" + b      
    }

In the above code we have a provider named "test" and a function named "doTestCommand" that requires 2 variables, "a" and "b".   

Your code can return a promise *optionally*.   If you do, the system will wait until
it is resolved before returning to the caller as in this example:

    #
    # Dummy command that returns a promise
    doTestCommand3() {
        return new Promise((resolve, reject) => {
            setTimeout(resolve, 3000, "Test3 Result Here");
        })
    }

These calls can be accessed two ways.

### Using the EdgeApi
> Note, in the browser the global "sock." is a connected EdgeApi connection.  

The EdgeApi connection automatically negotiates all possible API calls and makes them
available as functions.    Calling the above test command is very simple then:

    api.test_doTestCommand(5,10)
    .then((result) => {
        console.log("Result of doTestCommand=", result);
    }
        
As you can see, the command is simple baseName + "_" + method name and the parameters are handled automatically.

The EdgeApi connection is always connected, or attempts to be always connected using
socket.io so there is a way to know which person is calling.   We do that by adding the
variable "user" as the first parameter.   When set, the [user object](src/ApiSocketUser.ts) of the connected user is passed in.

    #
    # Simple command that also gets passed the user object (ApiSocketUser)
    doTestCommand2(user, a, b) {
        console.log("doTestCommand2, User=", user);
        return 123;
    }
        
The user object can be updated like a session management system.  

	user->page_number = 2
	
The API caller doesn't need to worry about the user variable and the call works
exactly the same way:

    api.test_doTestCommand2(10, 20)
    .then (result) {
        console.log("Result of doTestCommand2=", result);
    }
        
### Using the REST interface

All API calls can be called through the REST interface as well.   This is done with a general format that includes the base name, the function name without "do" and the 
parameters.  The end of the request must contain the return type expected.   For example,
ending a request in *.json*, .xml, or .csv.

	http://<server>/<base name>/<function name>/[<parameters list>].<extension>
	
For example,  here is the same call made directly to the server:
       
	bash$ curl "http://localhost:8001/test/TestCommand/123/456.json"
	"X123Y456"
	
	time curl "http://localhost:8001/test/TestCommand3.json"
	"Test3 Result Here"
	real	0m3.019s
	user	0m0.004s
	sys	0m0.004s
	
Notice in the above example the total call took 3.019s seconds which was 3
seconds of "setTimeout" and a very tiny bit of overhead.

### Returning multiple results

> These functions are for socket connected API calls only.

Your API call can return many results using "streaming".  First,  in your
function quickly return a stream id to the user allowing the caller to
get setup.

	stream_id = user.startStreamingList()
	resolve(stream_id)

Then in your code you can push new results like this:

	user.sendPush stream_id, someObjectOrData
	
Then when you are done with the results you do this:

	user.endStreamingList(stream_id)
	
You can also use this to send updates at any time to the connected
user.   This requires that you share some name or common ID with the 
client, for example,  you can have a doJoinRoom function that 
saves the user to a local array.  Then you can push messages any time

	for(let u in @users)
		u.sendPush("NEW_MESSAGE", someObject)
		
The EdgeApi would simply put in their code:

	api.on("NEW_MESSAGE", (someObject) => {
		##  callback for a server push event 
	})
		
		






