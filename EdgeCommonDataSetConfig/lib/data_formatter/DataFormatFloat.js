"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for decimal data type
//
// @extends [DataFormatterType] data formatted data
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const common = new DataFormatCommon_1.DataFormatCommon();
class DataFormatFloat extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'decimal';
        this.width = 100;
        this.input_width = 100;
        this.align = 'right';
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        if ((data == null)) {
            return "";
        }
        if ((options != null) && /#/.test(options)) {
            return numeral(common.getNumber(data)).format(options);
        }
        else {
            return numeral(common.getNumber(data)).format("#,###.######");
        }
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return common.getNumber(data);
    }
    attachEditor(elSourceInput, path) {
        // #
        // # set type of this input as "number"
        return elSourceInput.css("text-align", "right");
    }
    //
    // funtion to allow specific key code
    //
    // @param [Integer] keyCode keyCode to allow
    // @return [Boolean]
    //
    allowKey(keyCode) {
        return true;
        if ((keyCode >= 48) && (keyCode <= 57)) {
            return true;
        }
        if ((keyCode >= 96) && (keyCode <= 105)) {
            return true;
        }
        if (keyCode === 190) {
            return true;
        }
        if (keyCode === 189) {
            return true;
        }
        if (keyCode === 119) {
            return true;
        }
        if (keyCode === 109) {
            return true;
        }
        // console.log "Rejecting key:", keyCode
        return false;
    }
}
exports.DataFormatFloat = DataFormatFloat;
exports.default = DataFormatFloat;
//# sourceMappingURL=DataFormatFloat.js.map