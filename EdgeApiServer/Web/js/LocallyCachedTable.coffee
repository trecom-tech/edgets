
class LocallyCachedTable

    constructor: (@dataset, @tableName)->
        @name = @dataset + "_" + @tableName
        @data = {}

    doGetMany: (title, list, useCacheSeconds) =>

        new Promise (resolve, reject)=>

            #
            # Resolve quickly on an empty list
            if !list? or list.length == 0
                resolve(true)
                return true

            globalBusyDialog.showBusy title + " (cache)"
            globalBusyDialog.setMinMax 0, list.length-1

            #
            # Ignore anything already loaded
            newlist = []
            for item in list
                globalBusyDialog.step()
                if @data[item]? and @checkDate(@data[item]) then continue
                newlist.push item

            globalBusyDialog.finished()

            if newlist.length < 1
                resolve(true)
                return

            #
            # There are items to load

            newPromise ()=>

                if !@db?
                    @db = yield @doOpenLocalDatabase()

                globalBusyDialog.showBusy title + " (locally)"
                globalBusyDialog.setMinMax 0, newlist.length-1

                all = []
                @serverNeeded = []
                for item in newlist
                    if !item? or !item
                        # console.log "Skipping Local Item:", item
                        continue
                    # console.log "Checking #{@dataset} item=#{item} useCacheSeconds=#{useCacheSeconds}"
                    all.push @doGetRecordNew(item, useCacheSeconds)

                yield Promise.all(all)

                globalBusyDialog.finished()

                #
                # What remains in @serverNeeded is a list of records
                # that are either expired or new, we request those form the server
                #
                # console.log "serverNeeded #{@dataset}/#{@tableName}:", @serverNeeded
                if @serverNeeded.length > 0

                    globalBusyDialog.showBusy title + " (Server)"
                    globalBusyDialog.setMinMax 0, @serverNeeded.length-1

                    stream = yield sock.data_doGetItemsIfNeeded @dataset, @tableName, @serverNeeded
                    stream.onData = (record)=>

                        if record.useCache? and record.useCache
                            #
                            # Do nothing, the value in our cache is solid
                            # but doSetRecord will update the poll time so we
                            # can use this copy for a while longer.
                            #

                            @doSetRecord @data[record.id], false

                        else

                            @doSetRecord record

                        globalBusyDialog.step()

                    all = yield stream.doWaitUntilFinished()

                    globalBusyDialog.finished()

                    globalBusyDialog.showBusy title + " (Saving)"
                    yield @doWaitUntilSavesComplete()
                    globalBusyDialog.finished()

                    # @doWaitUntilSavesComplete()

                return true

            .then ()=>

                resolve()

    #
    # Return true if we have had this record in cache less than
    # the allowed amount of time.
    #
    checkDate: (record, useCacheSeconds)=>

        if !record? or !record._cacheTime?
            return false

        diff = new Date().getTime() - record._cacheTime
        diff /= 1000

        # console.log "Cache is only #{diff} < #{useCacheSeconds} on #{record.id}"
        if diff < useCacheSeconds
            return true

        return false

    doGetRecordNew: (id, useCacheSeconds)=>

        new Promise (resolve, reject)=>

            if !id?
                console.log "Invalid call to doGetRecordNew, id=<#{id}> on #{@tableName}"
                resolve(null)
                return

            @db.get(id)
            .then (result)=>
                # console.log "Result of #{id} [#{typeof id}]:", result

                if @checkDate(result, useCacheSeconds)
                    #
                    # Found a valid record in the local database
                    # that is within the time limit specified
                    #
                    @doSetRecord result, false
                    globalBusyDialog.step()
                    resolve(true)
                    return

                hash = null
                if result? and result._hash?
                    hash = result._hash
                    @data[id] = result

                @serverNeeded.push
                    id   : id
                    hash : hash

                resolve(true)

            .catch (e) =>

                console.log "Internal error on #{@tableName} id=#{id} :", e
                @serverNeeded.push { id: id }
                resolve(null)

            true

    #
    # Loop through JSON Object, fix fields
    fixupJson: (obj)=>

        for varName, value of obj
            if !value? then continue

            t = typeof value
            if t == "object"
                @fixupJson(value)
            else if t == "string"
                # console.log "Checking ", value, value.length
                if value.length == 24 and value.charAt(10) == 'T' and value.charAt(23) == 'Z'
                    obj[varName] = new Date(value)

        true

    doInvalidateRecord: (id)=>
        if @data[id]?
            delete @data[id]._hash
            delete @data[id]._cacheTime
        @db.delete id
        return true

    doSetRecord: (record, shouldSave = true)=>
        if !record? then return null

        # console.log "doSetRecord #{@dataset}/#{@tableName} id=#{record.id} [#{typeof record.id}]: #{record._hash}"
        record._cacheTime = new Date().getTime()

        #
        # Fixup JSON to handle dates and other possibly non JSON things
        @fixupJson(record)

        #
        # Save record locally / memory
        @data[record.id] = record

        if !record? or !record.id?
            console.log "INVALID RECORD DOPUT:", record
        else
            if !@savesPending?
                @savesPending = []

            if shouldSave? and shouldSave == true
                @savesPending.push @db.put record

        return null

    doWaitUntilSavesComplete: ()=>

        new Promise (resolve, reject)=>

            if !@savesPending?
                resolve(true)
                return

            Promise.all(@savesPending)
            .then ()=>
                @savesPending = []
                resolve(true)

    #
    # Open a local database to store samples and fields
    #
    doOpenLocalDatabase: ()=>

        if @localDatabasePromise? then return @localDatabasePromise
        @localDatabasePromise = new Promise (resolve, reject)=>

            @doDeleteLocalDatabase()
            .then ()=>

                serverName = "rrv3tc-#{@dataset}-#{@tableName}"

                tableScheme = {}
                tableScheme["cache"] =
                    key: { keyPath: 'id' }
                    # key: {keyPath: '_local_id', autoIncrement:  true}
                    # indexes: { 'id' : { } }

                db.open
                    server: serverName
                    version: 1
                    schema: tableScheme

                .then (server)=>
                    resolve(server["cache"])

    #
    # Append data to a record

    doAppendToPath: (id, path, newRecord)=>

        newPromise ()=>

            if !@data[id]?
                yield @doGetMany "Downloading record", [ id ], 60

            if !@data[id]?
                console.log "warning: invalid record [#{id}] unable to append path"
                return false

            pathList = path.split("/")
            basePath = @data[id]
            finalPath = pathList.pop()
            while pathList.length > 0
                subPath = pathList.shift()
                if !subPath? or subPath.length == 0 then continue
                if !basePath[subPath]? or typeof basePath[subPath] != "object"
                    basePath[subPath] = {}
                basePath = basePath[subPath]

            if !basePath[finalPath]? or !Array.isArray(basePath[finalPath])
                basePath[finalPath] = []

            basePath[finalPath].push newRecord
            yield sock.data_doAppendPath @dataset, "/#{@tableName}/#{id}#{path}", newRecord
            console.log "Append ds=#{@dataset} path=/#{@tableName}/#{id}#{path}", newRecord
            @doSetRecord @data[id], true
            return basePath
            true

    #
    # Delete the local storage database for this table
    #
    doDeleteLocalDatabase: ()->

        new Promise (resolve, reject) ->

            resolve(true)
            return

            serverName = "rrv3tc-#{@dataset}-#{@tableName}"

            db.delete serverName
            .then (ev)=>
                console.log "DELETE RESULT [#{@serverName}]:", ev
                resolve(ev)

            .catch (err)=>
                console.log "CATCH DELETE  [#{@serverName}]:", err
                reject(err)
