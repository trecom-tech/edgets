$ ->
##|
##|  Simple canvasjs chart using this
##|  example: http://canvasjs.com/editor/?id=http://canvasjs.com/example/gallery/overview/label-on-axis/
##|

	timeline =
		[
			{
				"index": 0,
				"guid": "46346147-e994-4247-9e40-0d781582e729",
				"balance": "1994.51",
				"age": 35,
				"name": "Simpson Bernard",
				"registered": "2014-05-01 08:28:12"
			},
			{
				"index": 1,
				"guid": "d3431009-d21c-4f15-acea-be74c7d8c3a1",
				"balance": "1840.43",
				"age": 20,
				"name": "Marsha Carpenter",
				"registered": "2014-06-16 10:16:26"
			},
			{
				"index": 2,
				"guid": "ec2cd5f9-2702-4af2-8cab-42e11afd680f",
				"balance": "3631.83",
				"age": 27,
				"name": "Mccall Vincent",
				"registered": "2017-12-26 07:15:14"
			},
			{
				"index": 3,
				"guid": "10534bf4-55ed-49bb-9fdc-711676055745",
				"balance": "2675.11",
				"age": 38,
				"name": "Deana Mcdaniel",
				"registered": "2016-12-18 11:01:33"
			},
			{
				"index": 4,
				"guid": "476022da-bea7-41e8-86e6-2b261c3456cd",
				"balance": "3148.33",
				"age": 28,
				"name": "Jimenez Nguyen",
				"registered": "2017-11-14 02:54:33"
			},
			{
				"index": 5,
				"guid": "203e327e-9ca6-4733-a940-ae28718e9522",
				"balance": "2909.19",
				"age": 22,
				"name": "Brianna Rodriquez",
				"registered": "2017-01-25 05:42:23"
			},
			{
				"index": 6,
				"guid": "c866018e-47ad-4fd1-9b3e-30aa463967fa",
				"balance": "3094.32",
				"age": 20,
				"name": "Haney Cohen",
				"registered": "2014-07-23 12:50:12"
			},
			{
				"index": 7,
				"guid": "2bad64cd-42f9-4de0-bc88-474226a5a987",
				"balance": "2374.64",
				"age": 36,
				"name": "Oneal Aguirre",
				"registered": "2017-10-03 06:09:55"
			},
			{
				"index": 8,
				"guid": "caeae518-82b6-42c1-9527-079c1de21639",
				"balance": "3820.13",
				"age": 20,
				"name": "Marta Petersen",
				"registered": "2016-11-03 02:59:06"
			},
			{
				"index": 9,
				"guid": "b9ebf4bc-d3ae-4171-9de9-d8a94da6a192",
				"balance": "2872.26",
				"age": 21,
				"name": "Mayra Stephenson",
				"registered": "2015-12-06 08:11:17"
			},
			{
				"index": 10,
				"guid": "b890e266-1b21-46ef-ba36-3cd4eedf0ee6",
				"balance": "3383.35",
				"age": 21,
				"name": "Lillie Downs",
				"registered": "2016-08-04 04:56:10"
			},
			{
				"index": 11,
				"guid": "9e3631c6-4d7c-46d1-b313-e29426c2b75b",
				"balance": "1404.44",
				"age": 24,
				"name": "Bauer Pate",
				"registered": "2014-12-13 01:01:44"
			},
			{
				"index": 12,
				"guid": "07421f05-c7f5-467a-a6c2-58f4b548a8a5",
				"balance": "1634.43",
				"age": 31,
				"name": "Gaines Martinez",
				"registered": "2015-11-22 06:50:13"
			},
			{
				"index": 13,
				"guid": "999ae080-76c7-450f-9e55-e86827e9b377",
				"balance": "3565.08",
				"age": 23,
				"name": "Gilmore Swanson",
				"registered": "2014-11-11 01:33:29"
			},
			{
				"index": 14,
				"guid": "7fd944f0-87f7-44b2-ac56-71c7fbbe0e2a",
				"balance": "1130.63",
				"age": 21,
				"name": "Chris Mcintyre",
				"registered": "2015-08-15 02:05:19"
			},
			{
				"index": 15,
				"guid": "06690ac3-f310-410f-adf9-e7fd11beba8a",
				"balance": "2609.97",
				"age": 37,
				"name": "Mildred Morton",
				"registered": "2017-03-09 03:28:09"
			},
			{
				"index": 16,
				"guid": "37195896-908a-43e7-bf06-d8adffbbb0c9",
				"balance": "1115.76",
				"age": 27,
				"name": "Cecelia Mullins",
				"registered": "2017-04-17 04:17:11"
			},
			{
				"index": 17,
				"guid": "e405c29f-c4e0-4dd1-aeb2-bf5439c937dc",
				"balance": "3989.65",
				"age": 37,
				"name": "Brenda Fowler",
				"registered": "2015-06-11 03:42:20"
			},
			{
				"index": 18,
				"guid": "7b268c92-2638-4da0-b6ae-be7ed5f88776",
				"balance": "1316.07",
				"age": 21,
				"name": "Elva Whitehead",
				"registered": "2017-12-03 09:53:58"
			},
			{
				"index": 19,
				"guid": "35c51a29-aa94-4fe0-a83d-d98d0764de41",
				"balance": "2065.24",
				"age": 28,
				"name": "Katelyn Oneil",
				"registered": "2015-05-07 11:43:37"
			},
			{
				"index": 20,
				"guid": "d8c81dda-c2c3-4bc2-82f8-0ffe1b0ec990",
				"balance": "3298.02",
				"age": 26,
				"name": "Alyssa Wade",
				"registered": "2014-07-06 03:23:57"
			},
			{
				"index": 21,
				"guid": "130e794e-ba8a-4919-bef2-3aa8a1542522",
				"balance": "1566.39",
				"age": 27,
				"name": "Marion Mills",
				"registered": "2016-06-16 11:38:56"
			},
			{
				"index": 22,
				"guid": "c20168f1-d587-4cec-a189-732df55ba521",
				"balance": "3936.58",
				"age": 28,
				"name": "Gomez Huber",
				"registered": "2014-06-13 01:49:49"
			},
			{
				"index": 23,
				"guid": "878d7502-09ba-48cf-8916-ef28c49a6f8d",
				"balance": "2633.83",
				"age": 36,
				"name": "Susan Ferguson",
				"registered": "2015-10-01 01:58:13"
			}
		]

	for idx, rec of timeline
		DataMap.addDataUpdateTable "timeline", idx, rec

	addTestButton "Simple GraphTime", "Open", ()->
		addHolder().setView "GraphTime", (view)=>
			view.addSeries "Simple GraphTime", "timeline", "registered", "balance"
			view.showGraphTime()

	addTestButton "Null value GraphTime", "Open", ()->
		addHolder().setView "GraphTime", (view)=>
			view.addSeries "Simple GraphTime", "timeline", "registered", null
			view.showGraphTime()

	##|
	##|  popup window chart test
	##|
	addTestButton "Simple GraphTime in popup", "Open", ()->
		doPopupView 'GraphTime','Simple GraphTime in popup', 'chart_popup', 1000, 450, (view) ->
			view.addSeries "Simple GraphTime", "timeline", "registered", "age"
			view.addSweepLine "Limited Age", 30
			view.showGraphTime()
		true

	addTestButton "GraphTime in tab", "Open", ()->
		addHolder()
			.setView "DynamicTabs", (viewTabs)->
				viewTabs.doAddViewTab "GraphTime", "GraphTime-Balance", (view)->
					view.addSeries "Simple GraphTime", "timeline", "registered", "balance"
					view.addSweepLine "Limited Balance", 3200
					view.showGraphTime()
				viewTabs.doAddViewTab "GraphTime", "GraphTime-Age", (view)->
					view.addSeries "Simple GraphTime", "timeline", "registered", "age"
					view.showGraphTime()
		true

	go()