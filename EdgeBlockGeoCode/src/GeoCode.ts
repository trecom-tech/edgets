import EdgeBlock   from 'edgeblock';
import config      from 'edgecommonconfig';
import * as crypto from "crypto";
import EdgeApi     from 'edgeapi';

const Geocodio     = require('geocodio');
const MapboxClient = require('mapbox');

interface Address {
    number?: number,
    display?: string,
    city?: string,
    state?: string,
    street?: string,
    county?: string,
    zip?: number,
    unit?: number;
    hash?: string;

    [key: string]: any;
}

interface AddressResult {
    _lastModified: Date,
    source: string,
    confidence: number,
    lat: number,
    lon: number,
    address?: Address,
    display?: string,
    other: any
}

export class GeoCode extends EdgeBlock {
    static useEdgeApi: boolean = true;

    constructor(...args: any[]) {
        super(...args);
    }

    getInputs() {
        return ["Normalized Address"];
    }

    //
    //  Initialize the results that we are going to return
    //  this is mostly a reminder of the template
    initResult() {

        let result: AddressResult = {
            _lastModified: new Date(),
            source       : "unknown",
            confidence   : 0,
            lat          : null,
            lon          : null,
            address      : {},
            other        : {}
        };

        return result;
    }

    //
    //  Convert a string address to an address object which can be used for lookup functions
    internalConvertAddressToObject(strAddress: any) {

        const address: Address = {
            display: "",
            number : 0,
            street : "",
            city   : "",
            state  : "",
        };

        if (strAddress == null) {
            return null;
        }

        if (typeof strAddress === "string") {
            address.display = strAddress;
        } else if (typeof strAddress === "object") {
            for (const varName in strAddress) {
                const value      = strAddress[varName];
                address[varName] = value;
            }
        }

        return address;
    }

    //
    //
    async doGetCensus(address: any): Promise<AddressResult | null> {

        if (GeoCode.useEdgeApi) {
            this.updateStatsToday("GeoCode", { "CensusData": 1 });
        }
        address = this.internalConvertAddressToObject(address);

        const host = "https://geocoding.geo.census.gov/geocoder/locations/onelineaddress";

        const result: any = await this.doGetLink(host, {
            address  : address.display,
            format   : "json",
            benchmark: 8
        });

        if ((result != null) && (result.result != null) && (result.result.addressMatches != null)) {
            const matches = result.result.addressMatches;
            if ((matches[0] != null) && (matches[0].coordinates != null)) {

                config.status("Found Census");
                const output           = this.initResult();
                output.source          = "CensusData";
                output.lon             = matches[0].coordinates.x;
                output.lat             = matches[0].coordinates.y;
                output.address.display = matches[0].matchedAddress;
                output.confidence      = 100;

                if (matches[0].addressComponents != null) {
                    output.address.city  = matches[0].addressComponents.city;
                    output.address.state = matches[0].addressComponents.state;
                    output.address.zip   = matches[0].addressComponents.zip;
                }

                return output;
            }
        }

        return null;
    }

    //
    //  Location lookup with OpenCage
    async doGetOpenCage(address: any) {
        address = this.internalConvertAddressToObject(address);

        if (GeoCode.useEdgeApi) {
            this.updateStatsToday("GeoCode", { "OpenCage": 1 });
        }

        const result: any = await this.doGetLink("http://api.opencagedata.com/geocode/v1/json", {
            q  : address.display,
            key: "d2ed58adefe9edb46a80739394f5a82f"
        });

        if ((result != null) && (result.results != null) && (result.results.length > 0)) {

            const bestResult = result.results.sort((a: any, b: any) => b.confidence - a.confidence).shift();
            // config.dump "OpenCage:", bestResult
            if ((bestResult.formatted != null) && (bestResult.components._type === "building")) {
                const output             = this.initResult();
                output.address.display   = bestResult.formatted.replace(", United States of America", "");
                output.address.number    = bestResult.components.house_number;
                output.address.zip       = bestResult.components.postcode;
                output.address.street    = bestResult.components.road;
                output.address.state     = bestResult.components.state;
                output.address.city      = bestResult.components.town;
                output.address.county    = bestResult.components.county;
                output.lat               = bestResult.geometry.lat;
                output.lon               = bestResult.geometry.lng;
                output.source            = "OpenCage";
                output.confidence        = bestResult.confidence;
                output.other.annotations = bestResult.annotations;

                EdgeApi.cleanupObject(output);
                return output;
            }
        }

        return null;
    }

    //
    //  Location lookup with GeoCodio
    doGetGeocodio(address: any) {

        return new Promise((resolve, reject) => {

            address = this.internalConvertAddressToObject(address);
            if (GeoCode.useEdgeApi) {
                this.updateStatsToday("GeoCode", { "GeoCodio": 1 });
            }
            const geocodio = new Geocodio({ api_key: "5878d6de6d26e7e8f2de2c37e36f2ed368556d1" });

            // geocodio.get 'parse', {q: address.display }, (err, result)=>
            //     console.log "PARSE ERR:", err
            //     console.log "PARSE RESULT:", result

            // console.log "ADDR:", address.display
            return geocodio.get("geocode", { q: address.display }, (err: Error, location: any) => {

                if (err) {
                    this.log.error("Geocodio Error", err);
                    resolve(null);
                }

                if (typeof location === "string") {
                    location = JSON.parse(location);
                }

                // config.dump "Geocodia Census Result:", err, location

                // config.inspect location
                const output: AddressResult = this.initResult();
                if ((location != null) && (location.results != null)) {
                    for (let result of location.results) {
                        if (result.accuracy >= 0.9) {
                            output.address.number = result.address_components.number;
                            output.address.street = result.address_components.formatted_street;
                            output.address.city   = result.address_components.city;
                            output.address.state  = result.address_components.state;
                            output.address.zip    = result.address_components.zip;
                            output.address.county = result.address_components.county;

                            if (result.formatted_address != null) {
                                output.display = result.formatted_address;
                            }

                            output.lat        = result.location.lat;
                            output.lon        = result.location.lng;
                            output.confidence = result.accuracy * 10;

                            output.source = "GeoCodio";
                            resolve(output);
                        }
                    }
                }

                resolve(null);
            });
        });
    }


    async doGetMapbox(address: any) {

        address = this.internalConvertAddressToObject(address);
        if (GeoCode.useEdgeApi) {
            this.updateStatsToday("GeoCode", { "Mapbox": 1 });
        }

        const client = new MapboxClient('pk.eyJ1IjoiYnJpYW5wb2xsYWNrIiwiYSI6IjUwZDQ0OWFkMDNhNTgyNjdlZmMyOWM1YTRhMzVmYjRjIn0.K0Pbg-Jo9bHGyxNvhtfVOQ');
        const result = await client.geocodeForward(address.display);
        if ((result == null) || (result.entity == null)) {
            return null;
        }

        const output  = this.initResult();
        output.source = "Mapbox";

        let seenAddress = false;
        for (let item of result.entity.features) {
            if ((item.place_type[0] === "address") && (seenAddress === false)) {
                output.address.display = item.place_name;
                output.lat             = item.center[1];
                output.lon             = item.center[0];
                output.confidence      = item.relevance;
                for (let c of item.context) {
                    if (/region/.test(c.id)) {
                        output.address.state = c.text;
                    }
                    if (/neighborhood/.test(c.id)) {
                        output.address.subdivision = c.text;
                    }
                    if (/place/.test(c.id)) {
                        output.address.city = c.text;
                    }
                    if (/postcode/.test(c.id)) {
                        output.address.zip = c.text;
                    }
                }
                seenAddress = true;
            }
            if (item.place_type[0] === "postcode") {
                output.address.zip = item.text;
            }
        }

        return output;
    }

    async doGetMapboxReverse(lat: number, lon: number) {

        const client = new MapboxClient('pk.eyJ1IjoiYnJpYW5wb2xsYWNrIiwiYSI6IjUwZDQ0OWFkMDNhNTgyNjdlZmMyOWM1YTRhMzVmYjRjIn0.K0Pbg-Jo9bHGyxNvhtfVOQ');
        const result = await client.geocodeReverse({ latitude: lat, longitude: lon });

        //
        //  Same exact processing as above Mapbox Formward request
        if ((result == null) || (result.entity == null)) {
            return null;
        }

        const output  = this.initResult();
        output.source = "Mapbox";

        let seenAddress = false;
        for (let item of result.entity.features) {
            if ((item.place_type[0] === "address") && (seenAddress === false)) {
                output.address.display = item.place_name;
                output.lat             = item.center[1];
                output.lon             = item.center[0];
                output.confidence      = item.relevance;
                for (let c of item.context) {
                    if (/region/.test(c.id)) {
                        output.address.state = c.text;
                    }
                    if (/neighborhood/.test(c.id)) {
                        output.address.subdivision = c.text;
                    }
                    if (/place/.test(c.id)) {
                        output.address.city = c.text;
                    }
                    if (/postcode/.test(c.id)) {
                        output.address.zip = c.text;
                    }
                }
                seenAddress = true;
            }
            if (item.place_type[0] === "postcode") {
                output.address.zip = item.text;
            }
        }

        return output;
    }

    //
    //  Convert an address text into address parts
    //
    async process(address?: any) {

        if (module.exports.devMode) {
            this.log.info("GeoCoding address", address);
        }


        let api, hash;
        let location: any;
        address = this.internalConvertAddressToObject(address);

        //
        //  Lookup the Geocode information
        // if !address or !address.number or isNaN(address.number)
        if (!address || !address.number) {
            return { error: "Invalid address, no house number" };
        }

        address.display = address.display.replace("  ", " ");
        if (!/,/.test(address.display)) {
            address.display = [address.display, address.city, address.state, address.zip].join(", ");
        }

        //
        //  Step 1- Attempt to locate the address's hash in the existing
        //  database.   If so, don't need to lookup
        if (address.hash == null) {
            hash = crypto.createHash('md5');
            hash.update(address.display);
            address.hash = hash.digest('hex');
        }

        if (GeoCode.useEdgeApi) {
            api         = await this.doGetApi();
            const items = await api.data_doGetItems("lookup", `/GeoCode/${address.hash}`, {}, {}, 0, 1);

            if ((items != null) && (items.length > 0)) {
                location = items[0];
                if (location.source != null) {
                    return location;
                }

                //
                // Old hash from RR Portal 2.0
                if ((location.street != null) && (location.number != null)) {
                    const output = this.initResult();
                    output.lat   = parseFloat(location.lat);
                    output.lon   = parseFloat(location.lon);
                    return output;
                }
            }
        }

        //
        //  Step 2- Loop through different options
        //
        location = null;

        if (location == null) {
            location = await this.doGetCensus(address);
        }

        if (location == null) {
            //
            //  Lookup with GeoCodio
            location = await this.doGetGeocodio(address);
        }

        if (location == null) {
            //
            //  Attempt to find the address using OpenCage API
            location = await this.doGetOpenCage(address);
        }

        if (location == null) {
            //
            //  Attempt to use Mapbox
            location = await this.doGetMapbox(address);
        }

        //
        //  If we found a location, update the cache and return it
        //
        if ((location != null) && location.lon && location.lat) {

            // console.log "Saving to /GeoCode/#{address.hash}"
            if (GeoCode.useEdgeApi && (address.hash != null)) {
                await api.data_doUpdatePath("lookup", `/GeoCode/${address.hash}`, location);
            }

            return location;

        } else {

            // console.log "Error Saving to /GeoCode/#{address.hash}", location
            if (GeoCode.useEdgeApi) {
                await api.data_doUpdatePath("lookup", `/GeoCode/${address.hash}`, { error: "Unable to find location" });
            }
            return { error: "Unable to find location" };
        }
    }
}

export default GeoCode;