# EdgeServer
> Reads documents from the item-change queue and then save them to the database

## Understand data flow

In order to save data to the database you must have

1. A Dataset name which is like a database
2. A Path which is like the name of the table and sub fields in the record combined

An application can submit a change to the message queue and that change will be received here.   This code will load the
existing record, look for changes, and save the record if there are changes.  For each field changes a change record is
saved.   See the Change Logger service to see what is done with those changes.

##  Testing manually

You can run a job file through manually like this:

    ts-node src/EdgeServer.ts --test ./SampleData59/SampleData1.json