$ ->

    test_lon = -80.890674
    test_lat = 35.445173

    addTestButton "Simple MapBox", "Show Map", ()->

        addHolder().setView "MapBox", (view)->
            view.setViewLocation test_lat, test_lon, 17
            view.setNavigationControl()
            view.addBuildingsLayer()
            view.on "moved", (bounds)=>
                console.log "Moved to:", bounds

    go()