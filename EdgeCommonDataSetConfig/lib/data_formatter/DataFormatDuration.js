"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for timeago data type
//
// @extends [DataFormatterType]
//
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
class DataFormatDuration extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'duration';
        this.width = 90;
        this.input_width = 90;
        this.align = 'right';
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        let min;
        let txt;
        if ((data == null)) {
            return "";
        }
        if (typeof data === "string") {
            data = parseFloat(options);
        }
        let sec = data / 1000;
        if (sec < 60) {
            txt = numeral(sec).format("#.###") + " sec";
        }
        else if (sec < (60 * 60 * 2)) {
            min = Math.floor(sec / 60);
            sec = sec - (min * 60);
            txt = min + " min, " + Math.floor(sec) + " sec.";
        }
        else {
            const hrs = Math.floor(sec / (60 * 60));
            min = Math.floor((sec - (hrs * 60 * 60)) / 60);
            txt = hrs + " hrs, " + min + " min";
        }
        return txt;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    unformat(data, path) {
        return data;
    }
}
exports.DataFormatDuration = DataFormatDuration;
exports.default = DataFormatDuration;
//# sourceMappingURL=DataFormatDuration.js.map