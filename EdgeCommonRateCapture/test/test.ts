import EdgeRateCapture from '../src/EdgeRateCapture';
import 'mocha';

const should = require('should');

describe('Add sample, Reset, Stop', () => {
    const rateCapture = new EdgeRateCapture('Test');

    it('Add, last value should be the same with total samples', () => {
        rateCapture.addSample(10);
        rateCapture.should.have.property('lastValue', 10);
        rateCapture.should.have.property('totalSamples', 10);

        rateCapture.addSample(20);
        rateCapture.should.have.property('loopTimer', 1000);
    });

    it('Reset, last value should be reset', () => {
        rateCapture.reset();
        rateCapture.should.have.property('sampleValues', new Array(500).fill(-1));
        rateCapture.should.have.property('sampleTimes', new Array(500).fill(-1));
        rateCapture.should.have.property('lastTimer', null);
        rateCapture.should.have.property('lastValue', 0);
    });

    it('Stop, looptimer should be deleted', () => {
        rateCapture.stop();
        should(rateCapture.loopTimer).not.be.ok();
    });
});

describe('Increase, set, add var', () => {
    const rateCapture = new EdgeRateCapture('Test');

    it('Increase', () => {
        const testName = 'testName';

        rateCapture.varList[testName] = 20;
        rateCapture.incVar(testName, 10);
        rateCapture.varList[testName].should.equal(30);
    });

    it('Set', () => {
        const testName = 'testName';
        rateCapture.setVar(testName, 10);
        rateCapture.varList[testName].should.equal(10);
    });

    it('Add', () => {
        const testName = 'testName';
        const testTitle = 'testTitle';
        rateCapture.addVar(testName, testTitle, 10);
        rateCapture.varList[testName].should.equal(10);
        rateCapture.varNames[testName].should.equal(testTitle);
        rateCapture.stop();
    });
});

describe('StatusUpdateTimer', () => {
    const rateCapture = new EdgeRateCapture('Test');

    it('Check if statusupdateclient created', () => {
        setTimeout(() => {
            should.exist(rateCapture.statusUpdateClient);
        }, 6000);
    });

    it('Show detailed status', () => {
        setTimeout(() => {
            should.exist(rateCapture.status.remain);
            should.exist(rateCapture.status.total);
            should.exist(rateCapture.status.avg);
            should.exist(rateCapture.status.numproc);
            should.exist(rateCapture.status.proctime);
            process.exit(0);
        }, 7000);
    });

});





