import ApiProvider    from './ApiProvider';
import * as uuidv4    from 'uuid/v4';
import ApiSocketUser  from './ApiSocketUser';
import DataSetManager from 'edgedatasetmanager';
import EdgeApi        from 'edgeapi';
import EdgeError      from './EdgeError';

const RedisLock = require('redislock');
const slug      = require('slug');

export class ProviderUtility extends ApiProvider {
    redisLock: any;
    dmSequential: DataSetManager;
    sequentialDatasetName: string    = 'lookup';
    sequentialCollectionName: string = 'sequential';

    constructor(prototype: ApiProvider) {
        super(prototype);
        this.sequentialDatasetName    = 'lookup';
        this.sequentialCollectionName = 'sequential';
    }

    onInit() {
    }

    getBaseName() {
        return 'utility';
    }

    doGetGuid() {
        return uuidv4();
    }

    doGetServerStamp(user?: ApiSocketUser) {
        return {
            timestamp: new Date().getTime(),
            user     : user.myName,
        };
    }

    async doGetNextSequential(user: ApiSocketUser, slug: string) {
        if (!user.hasRole('serviceUser')) {
            throw new EdgeError('Authentication required', 'ProviderUtility');
        }
        const dm = this.doInternalGetSequentialDataSet();

        if (!this.redisLock) {
            await EdgeApi.cacheGet(`SEQ_${slug}`);
            this.redisLock = RedisLock.createLock(EdgeApi.globalCacheReadonly, {
                timeout: 2000,
                retries: 200,
                delay  : 100,
            });
        }

        await this.redisLock.acquire(`app:sequential:lock`);

        const existingInRedis: any = await EdgeApi.cacheGet(`SEQ_${slug}`);
        if (existingInRedis) {
            const nextSequential = parseInt(existingInRedis, 10) + 1;
            await this.doInternalReserveSequential(dm, slug, nextSequential);

            await this.redisLock.release();
            return nextSequential;
        }

        const nextSequential: any = await dm.doGetItem(`/${this.sequentialCollectionName}/${slug}`);
        nextSequential.count      = nextSequential.count ? nextSequential.count + 1 : 1;
        await this.doInternalReserveSequential(dm, slug, nextSequential.count);

        await this.redisLock.release();
        return nextSequential.count;
    }

    async doGetNextNewObject(user: ApiSocketUser, datasetName: string, collectionName: string) {
        if (!user.hasRole('serviceUser')) {
            throw new EdgeError('Authentication required', 'ProviderUtility');
        }

        const nextSequentialId = await this.doGetNextSequential(user, slug(`db ${datasetName} ${collectionName}`));

        const dm: DataSetManager = new DataSetManager(datasetName);
        const item               = await dm.doGetItem(`/${collectionName}/${nextSequentialId}`);

        return item.data;
    }

    doInternalGetSequentialDataSet() {
        if (!this.dmSequential) {
            this.dmSequential = new DataSetManager(this.sequentialDatasetName);
        }
        return this.dmSequential;
    }

    async doInternalReserveSequential(dm: DataSetManager, slug: string, nextSequential: number) {
        await dm.doUpdate(`/${this.sequentialCollectionName}/${slug}`, { count: nextSequential });
        await EdgeApi.cacheSet(`SEQ_${slug}`, nextSequential.toString());
    }

}

export default ProviderUtility;
