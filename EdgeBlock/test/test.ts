import EdgeBlock  from '../src/EdgeBlock';
import DummyBlock from './DummyBlock';

describe("Edge Block Manager Test", () =>

    describe("Loading a dummy block", function () {

        it("Should load using a local require statement", function () {
            EdgeBlock.getBlockManager().load("DummyBlock", DummyBlock);
        });

        it("Should be able to load a local block file", function () {
            const result = EdgeBlock.getBlockManager().run("DummyBlock", null);
            result.should.equal(true);
        });
    })
);

