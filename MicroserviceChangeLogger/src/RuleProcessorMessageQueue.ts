import { EdgeAMQPController }   from 'edgecommonmessagequeue';
import { config }               from 'edgecommonconfig';
import RuleProcessor            from './RuleProcessor';

export default class RuleProcessorMessageQueue extends RuleProcessor {
    
    mq: any;

    onInit() {
        if (!this.rule.mq_name) {
            return this.setError('Invalid Message Queue or missing "mq_name"');
        }

        EdgeAMQPController.doOpenMessageQueue(this.rule.mq_name)
        .then((mq) => {
            this.mq = mq;
        })
        .catch((err) => {
            console.log(err);
        });
    }

    onExecute(job: any, count: number = 0) {
        if (!this.mq) {
            config.status('RuleProcessorMessageQueue onEexecute, mq not defined, postpone job');

            if (count === this.limit) {
                throw new Error('RuleProcessorMessageQueue onEexecute, mq not defined');
            } 

            setTimeout(() => {
                this.onExecute(job, count++);
            }, 1000);
            
            return;
        }

        this.mq.doSubmitBuffer(job);
    }

}