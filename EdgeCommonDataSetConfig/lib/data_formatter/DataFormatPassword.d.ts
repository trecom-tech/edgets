import { DataFormatText } from './DataFormatText';
export declare class DataFormatPassword extends DataFormatText {
    name: string;
    viewPasswordChange: any;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    attachEditor(elSourceInput: any, path: any): this;
}
export default DataFormatPassword;
