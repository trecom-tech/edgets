
const slug = require('slug');
const chalk = require('chalk');

import { ObjectID } from 'mongodb';

let reMatchIn = /^in:(.*)/;
let reMatchInName = /^([^: ,\-]+):in:(.*)/;
// let reDate1 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T00.00.00.000Z/;
// let reDate2 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9]Z/;

export class DataSetPath {
    [key                     : string]: any;

    keyName                  : string;
    // A value that is set if the condition is a simple "id" search
    // but only if it's a single records search
    value                    : any    = null;
    // Search / save conditions
    condition                : any    = {};
    // Elements that point deeper into an object for sub keys
    subPath                  : any    = null;
    collection               : string = "";
    // The raw input path
    rawPath                  : any    = null;
    // True if the path condition can be used to save a record
    canSave                  : boolean= null;
    // Internal helpers
    static checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
    static checkForNumber    = /^\-{0,1}[\d\.]{0,12}$/;

    toString() {

        let str = this.rawPath;
        str += " | ";
        str += `collection=${ chalk.red(this.collection) }, subPath=${ chalk.cyan(JSON.stringify(this.subPath)) }, `;
        str += ` condition=${ chalk.yellow(JSON.stringify(this.condition)) }`;

        if (this.canSave != null && this.canSave) {
            str += ", canSave=Yes";
        } else {
            str += ", canSave=No";
        }

        if (this.value != null) {
            str += `, value=${ chalk.blue(this.value) }`;
        }

        return str;
    }

    constructor(rawPath: string | any) {
        if (typeof rawPath === "object" && rawPath.parsePath != null) {
            // Object is already a path, duplicate it

            for (let keyName in rawPath) {
                const keyValue = rawPath[keyName];
                this[keyName] = keyValue;
            }
        } else if (typeof rawPath === "object" && rawPath.toString != null) {

            this.parsePath(rawPath.toString());
        } else if (typeof rawPath === "object") {

            throw new Error(`Unable to parse path from object: ${ JSON.stringify(rawPath) }`);
        } else if (typeof rawPath === "string") {

            this.parsePath(rawPath);
        } else {

            throw new Error(`Unable to parse path from unknown: ${ JSON.stringify(rawPath) }`);
        }
    }

    /**
     * Set the test file to understand what path options are possible
     * @param path
     */
    parsePath(path: string) {

        this.condition = {};
        this.value = null;
        this.subPath = null;
        this.collection = null;
        this.canSave = true;
        this.rawPath = path;

        if (path == null || path.length < 4) {
            throw new Error(`Invalid path, length is too short (4 or more): ${ path }`);
        }

        if (path.charAt(0) !== '/') {
            throw new Error(`Invalid path, char[0] should be / and it's ${ path.charAt(0) } for ${ path }`);
        }

        // Path must be /collection/[keyName:]value/subpath/subpath

        path = path.replace("//", "/");

        const parts: string[] = path.split('/');
        if (path.charAt(0) === '/') {
            parts.shift();
        }
        if (parts == null || !parts.length || parts.length < 1) {
            throw new Error(`Error parsing path '${ path }'`);
        }

        this.collection = slug(parts.shift());

        if (this.collection.length > 32) {
            throw new Error(`Invalid collection name length, ${ this.collection.length } > 32`);
        }

        if (parts.length === 0) {
            this.condition = {};
            this.subPath = [];
            this.canSave = false;
            return true;
        }

        // Split the value which may contain a key
        this.parsePathSubkey("", parts.shift());

        // If any elements of the path remain, they are references into the object
        if (parts.length > 0) {

            // console.log "parsePath, parts.length is still available"
            // A trailing slash may leave an empty path at the end that needs to be removed.
            if (parts[parts.length - 1].length === 0) {
                parts.pop();
            }

            this.subPath = [];

            let strSubpath = "";
            for (let name of Array.from(parts)) {
                if (strSubpath.length > 0) {
                    strSubpath += ".";
                }
                if (/:/.test(name)) {
                    //
                    // Contains a condition
                    let [fieldName, conditionText] = Array.from(name.split(':', 2));
                    this.parsePathSubkey(strSubpath, name);
                    strSubpath += fieldName;
                    this.subPath.push(fieldName);
                } else {
                    strSubpath += name;
                    this.subPath.push(name);
                }
            }
        } else {
            this.subPath = [];
        }

        return true;
    }

    /**
     * Special condition when a key is in a group of values
     * /item/in:1,2,3
     * @param fieldName
     * @param subPathText
     * @param pathText
     */
    parseInGroupCondition(fieldName: string, subPathText: string, pathText: string) {
        // console.log "parseInGroupCondition"

        pathText = pathText.replace(/:*in:/, "");

        // console.log "parseInGroupCondition, fieldName=", fieldName, "subPathText=", subPathText, "pathText=", pathText

        let validValues = [];
        for (let val of Array.from(pathText.split(","))) {
            validValues.push(this.getConditionValue(val));
        }
        this.condition[subPathText + fieldName] = { '$in': validValues };

        // console.log "parseInGroupCondition, condition:", @condition
        this.canSave = false;
        return true;
    }

    /**
     *
     * @param subPathText
     * @param pathText
     */
    parsePathSubkey(subPathText: string, pathText: string) {

        // console.log "parse path , subpath:", subPathText
        // console.log "parse path , pathText:", pathText
        //
        // Special search for "in" or selecting multiple records with the same key

        let condition: any;
        let m = pathText.match(reMatchInName);
        if (m) {
            // console.log "parse path, first case"
            return this.parseInGroupCondition(m[1], subPathText, pathText.replace(m[1], ""));
        }
        m = pathText.match(reMatchIn)
        if (m) {
            // console.log "parse path, second case"
            return this.parseInGroupCondition("id", subPathText, pathText);
        }

        // console.log "parse path, third case", pathText
        //
        // Split the value which may contain a key
        let allConditions = pathText.split(',');
        // console.log "allConditions:  ", allConditions

        if (allConditions.length === 1) {

            //
            // Single condition (no comma)
            condition = this.parsePathCondition('', allConditions[0]);

            // console.log "allConditions length == 1, condition ", condition
            if (Object.keys(condition).length === 1 && condition["id"] != null && typeof condition["id"] !== "object") {
                this.value = condition["id"];
            }
        } else {

            // console.log "allConditions length != 1"
            //
            // Default logical search is $and for items with a comma, but you can specifiy another such as or
            // /item/or:1,2,5
            let logicCommand = '$and';
            m = pathText.match(/^([a-z]+):/)
            if (m) {
                pathText = pathText.replace(m[0], "");
                if (m[1] === "and" || m[1] === "or" || m[1] === "not" || m[1] === "in") {
                    logicCommand = `$${ m[1] }`;
                    allConditions = pathText.split(',');
                }
            }

            // console.log "allConditions length != 1, allConditions: ", allConditions 
            //
            // Multiple conditions in an "or" situation
            let conditionList = [];
            for (let oneCondition of Array.from(allConditions)) {
                conditionList.push(this.parsePathCondition('', oneCondition));
            }

            // console.log "allConditions length != 1. conditionList:" , conditionList

            condition = {};
            condition[logicCommand] = conditionList;

            this.canSave = false;
        }

        //
        // Merge the condition
        for (let keyName in condition) {
            let keyValue = condition[keyName];
            this.condition[subPathText + keyName] = keyValue;
        }

        // console.log "parsePathSubkey, result, condition:", @condition

        return true;
    }

    /**
     * Given a condition in "DataSet" terms, return a viable
     * Mongo search condition.
     * @param basePath
     * @param oneCondition
     */
    parsePathCondition(basePath: string, oneCondition: string) {

        let condition: any = {};
        let keyParts = oneCondition.split(':');


        if (keyParts[0] != null && keyParts[1] != null) {
            condition[keyParts[0]] = this.getConditionValue(keyParts[1]);
            if (condition['id'] == null) {
                this.canSave = false;
            }
        } else {
            // Check for mongo key
            if (DataSetPath.checkForHexRegExp.test(keyParts[0])) {
                condition["_id"] = ObjectID.createFromHexString(keyParts[0]);
            } else {
                if (keyParts[0] != null && keyParts[0].length > 0) {
                    condition["id"] = this.getConditionValue(keyParts[0]);
                    if (condition["id"] == null) {
                        this.canSave = false;
                        condition = {};
                    }
                } else {
                    this.canSave = false;
                    condition = {};
                }
            }
        }

        return condition;
    }

    /**
     * Get the value in the correct data type
     * @param value
     */
    getConditionValue(value: string) {
        let result;
        if (value.charAt(0) === '+') {
            // Logical field check with a positive value
            this.canSave = false;

            if (value === "+exists") {
                return { '$exists': true };
            }
        } else if (value.charAt(0) === '-') {
            // Logical field check with a negative value
            this.canSave = false;
            if (value === "-exists") {
                return { '$exists': false };
            }
        }

        // item/3-10  -- A range of numerical values
        if (typeof value === "string" && /^([0-9]{1,12})-([0-9]{1,12})$/.test(value)) {
            // console.log "getConditionValue, third case"
            this.canSave = false;
            let m = value.match(/^([0-9]+)-([0-9]+)$/);
            result = {
                '$gt': parseInt(m[1], 10) - 1,
                '$lt': parseInt(m[2], 10) + 1
            };
            return result;
        }

        // item/123+ -- A range greater than
        if (typeof value === "string" && /^([0-9]{1,12})\+$/.test(value)) {
            // console.log "getConditionValue, fourth case"
            this.canSave = false;
            result = { '$gt': parseInt(value, 10) - 1 };
            return result;
        }

        // item/123 -- A string as a number is a number, go fast
        if (typeof value === "string" && DataSetPath.checkForNumber.test(value)) {
            return parseFloat(value);
        }

        // item/5745d5bd7be83a7fa64f0d07 -- Hex based mongodb object id
        if (DataSetPath.checkForHexRegExp.test(value)) {
            return ObjectID.createFromHexString(value);
        }

        // item/~start  -- A regex search
        if (typeof value === "string" && value.charAt(0) === '~') {
            value = value.replace("~", "");
            return new RegExp(value, "i");
        }

        // Single wildcard, all values
        if (value === '*') {
            this.canSave = false;
            return null;
        }
        return value;
    }
};

export default DataSetPath;