import { DataFormatter } from '../src';
import { expect }        from 'chai';
const should             = require('should');
const moment             = require('moment');

describe("DataFormatter", () =>

    describe("Test formatData Method ()", function() {

        it("should be string if type is text and given string is string", function() { 

            const dataFormatter = new DataFormatter();

            const stringData = "Text value";
            const result = dataFormatter.formatData("text", stringData);

            result.should.be.type("string");
            return result.should.equal("Text value");
        });

        it("should be string if type is text and given string is number", function() { 

            const dataFormatter = new DataFormatter();

            const numberData = 1234;

            const result = dataFormatter.formatData("text", numberData);

            result.should.be.type("string");
            return result.should.equal("1234");
        });

        it("should be integer if type is int and given string is number", function() {
            const dataFormatter = new DataFormatter();

            const numberData = 1234;

            const result = dataFormatter.formatData("int", numberData);

            result.should.be.type("string");
            return result.should.equal("1,234");
        });

        it("should be decimal number if type is number", function() { 
            const dataFormatter = new DataFormatter();

            const numberData = 1234;

            const result = dataFormatter.formatData("number", numberData);

            result.should.be.type("string");
            return result.should.equal("1,234");
        });

        it("should be decimal number if type if number", function() {
            const dataFormatter = new DataFormatter();

            const numberData = 1234.123;

            const result = dataFormatter.formatData("number", numberData);

            result.should.be.type("string");
            return result.should.equal("1,234.12");
        });

        it("should be money if type is money", function() { 
            const dataFormatter = new DataFormatter();

            const numberData = 1234;

            const result = dataFormatter.formatData("money", numberData);

            result.should.be.type("string");
            return result.should.equal("$ 1,234");
        });

        it("should be percent if type is percent", function() {
            const dataFormatter = new DataFormatter();

            const numberData = 0.25;

            const result = dataFormatter.formatData("percent", numberData);

            result.should.be.type("string");
            return result.should.equal("25 %");
        });

        it("should be date type if type is date and given string is MySQL Date type", function() {
            const dataFormatter = new DataFormatter();

            const date   = "2015-10-31 14:15:32";

            const result = dataFormatter.formatData("date", date);

            result.should.be.type("string");
            return result.should.equal("10/31/2015");
        });

        it("should be date type if type is datetime and given string is MySQL Date type", function() {
            const dataFormatter = new DataFormatter();

            const date   = "2015-10-31 14:15:32";

            const result = dataFormatter.formatData("datetime", date);

            result.should.be.type("string");
            return result.should.equal("Sat, Oct 31st, 2015 2:15:32 PM");
        });

        it("should be age date if type is age", function() {
            const dataFormatter = new DataFormatter();

            const date   = "2015-08-31 14:15:32";

            const result = dataFormatter.formatData("age", date);

            result.should.be.type("string");
            expect(result).to.include("<span class='fdate'>08/31/2015</span><span class='fage'>");
            expect(result).to.include('yrs</span>');
        });

        it("test for enum type", function() {
            const dataFormatter = new DataFormatter();

            let data: any = ["Red", "Green", "Blue", "Pink"];

            let result = dataFormatter.formatData("enum", "Blue", data);
            result.should.equal("Blue");

            result = dataFormatter.formatData("enum", "Yellow", data);
            result.should.be.type("string");
            result.should.equal("Yellow");

            result = dataFormatter.formatData("enum", 1, data);
            result.should.equal("Green");

            data   = "Red, Blue, Green, Pink";
            result = dataFormatter.formatData("enum", 0, data);
            return result.should.equal("Red");
        });

        it("test for distance", function() {
            const dataFormatter = new DataFormatter();

            let result = dataFormatter.formatData("distance", 0.25);
            result.should.equal("< 50 ft");

            result = dataFormatter.formatData("distance", 500);
            return result.should.equal("0.31 mi");
        });

        it("test for boolean", function() {
            const dataFormatter = new DataFormatter();

            let result = dataFormatter.formatData("boolean", 1);
            result.should.equal("<i class='fas fa-circle'></i> Yes");

            result = dataFormatter.formatData("boolean", "Yes");
            result.should.equal("<i class='fas fa-circle'></i> Yes");

            result = dataFormatter.formatData("boolean", 0);
            result.should.equal("<i class='far fa-circle'></i> No");

            result = dataFormatter.formatData("boolean", false);
            return result.should.equal("<i class='far fa-circle'></i> No");
        });

        return it("test for tags", function() {
            const dataFormatter = new DataFormatter();

            let data: any   = "Apple,Grape,Watermellon";
            let result = dataFormatter.formatData("tags", data);
            result.should.equal("Apple, Grape, Watermellon");

            data   = ["Apple", "Grape", "Watermellon"];
            result = dataFormatter.formatData("tags", data);
            return result.should.equal("Apple, Grape, Watermellon");
        });
    })
);