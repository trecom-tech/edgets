import { AddressNormalizer } from '../src/AddressNormalizer';

process.env.redisHostWQ = process.env.REDIS_HOST || "redis://127.0.0.1:6379";
process.env.redisReadHost = process.env.REDIS_HOST || "redis://127.0.0.1:6379";
process.env.redisHost = process.env.REDIS_HOST || "redis://127.0.0.1:6379";

describe("Calling the module directly", () =>

    describe("Simple processing of an address", function () {

        const block = new AddressNormalizer();

        it("Should handle this one", function () {
            let result;
            result = block.process('2798 Bridle Path SE', 'Conyers', 'GA', '30094');
        });

        it("Should process a simple address", function () {
            const result = block.process("293 montibello dr", "mooresville", "nc", '28115');
            result.should.have.property("number", 293);
            result.should.have.property("city", "Mooresville");
            result.should.have.property("state", "NC");
            result.should.have.property("zip", "28115");
        });

        it("Should process a simple address without a zipcode", function () {
            const result = block.process("293 montibello dr", "mooresville", "nc");
            result.should.have.property("number", 293);
            result.should.have.property("city", "Mooresville");
            result.should.have.property("state", "NC");
            result.should.have.property("zip", "28115");
        });

        it("Should process a simple address without city and state", function () {
            const result = block.process("293 montibello dr", "", "", '28115');
            result.should.have.property("number", 293);
            result.should.have.property("city", "Mooresville");
            result.should.have.property("state", "NC");
            result.should.have.property("zip", "28115");
        });

        it("Should handle unit numbers at the end [Type 1]", function () {
            const test_address = '250 N Banana River Dr Apt C6';
            const test_city    = 'Merritt Island';
            const test_state   = 'Florida';
            const test_zip     = '32952';
            const result       = block.process(test_address, test_city, test_state, test_zip);
            result.should.have.property("number", 250);
            result.should.have.property("city", "Merritt Island");
            result.should.have.property("state", "FL");
            result.should.have.property("zip", "32952");
            result.should.have.property("display", "250 N Banana River Dr Apt C6, Merritt Island, FL, 32952");
        });

        it("Should handle unit numbers [Type 2]", function () {
            const test_address = '6607 RAPID WATER WAY #104, GLEN BURNIE, MD, 21060';
            const result       = block.process(test_address);
            result.should.have.property("unit", 104);
        });

        it("getZipcodeFromCityState", async function () {
            const result = await block.getZipcodeFromCityState('belmont', 'ma');
            result.should.equal('02178');
        });
    })
);
