//  A gateway can take a command and options
//  and pass it to a provider

import config             from 'edgecommonconfig';
import ApiProvider        from './ApiProvider';
import ProviderOS         from './ProviderOS';
import ProviderData       from './ProviderData';
import ProviderLogs       from './ProviderLogs';
import ProviderStats      from './ProviderStats';
import ProviderWorkQueue  from './ProviderWorkQueue';
import ProviderCodeEditor from './ProviderCodeEditor';
import ProviderUtility    from './ProviderUtility';
import ProviderAuth       from './ProviderAuth';
import ProviderTimeline   from './ProviderTimeline';
import ProviderTable      from './ProviderTable';
import ProviderTest       from './ProviderTest';
import ApiSocketUser      from './ApiSocketUser';

import { EdgeSocketRequestData, EdgeSocketResponseData } from 'edgeapi';
import IApiUser                                          from './interfaces/IApiUser';

export default class ApiGateway {
    apiCallList: { [s: string]: Array<string> }    = {};
    providers: ApiProvider[]                       = [];
    apiWantsUser: any                              = {};
    apiCallFunc: any                               = {};
    apiOwner: { [methodName: string]: ApiProvider} = {};

    /**
     * Creates an instance
     */
    constructor() {
        this.providers.push(new ProviderOS(ProviderOS.prototype));
        this.providers.push(new ProviderCodeEditor(ProviderCodeEditor.prototype));
        this.providers.push(new ProviderData(ProviderData.prototype));
        this.providers.push(new ProviderLogs(ProviderLogs.prototype));
        this.providers.push(new ProviderStats(ProviderStats.prototype));
        this.providers.push(new ProviderWorkQueue(ProviderWorkQueue.prototype));
        this.providers.push(new ProviderUtility(ProviderUtility.prototype));
        this.providers.push(new ProviderAuth(ProviderAuth.prototype));
        this.providers.push(new ProviderTimeline(ProviderTimeline.prototype));
        this.providers.push(new ProviderTable(ProviderTable.prototype));
        this.providers.push(new ProviderTest(ProviderTest.prototype));

        this.determineApiCalls();
    }

    /**
     * An API Provider is a class the implements "ApiProvider" to expose functions
     * @param apiProvider instance of class extends ApiProvider
     */
    addProvider(apiProvider: ApiProvider) {

        try {
            // Give api privder the reference to gateway.
            apiProvider._apiGatewayRef = this;

            let name = apiProvider.getBaseName();
            config.status(`Adding calls for ${ name }`);

            for (let objName in apiProvider.apiCallList) {

                let objVal = apiProvider.apiCallList[objName];
                config.status(`Adding ${ name }_${ objName }(${ JSON.stringify(objVal) })`);
                if (objVal != null && objVal.length > 0 && objVal[0] === 'user') {
                    this.apiWantsUser[name + '_' + objName] = true;
                    objVal.shift();
                    this.apiCallList[name + '_' + objName] = objVal;
                } else {
                    this.apiWantsUser[name + '_' + objName] = false;
                    this.apiCallList[name + '_' + objName]  = objVal;
                }

                this.apiOwner[name + '_' + objName] = apiProvider;
                this.apiCallFunc[name + '_' + objName] = apiProvider[objName];
            }

            return true;
        } catch (e) {

            config.status('Error loading API Provider:', e);
            return false;
        }
    }

    /**
     * Look at the functions exposed by the API and create an
     * array to reference those functions so that the socket can
     * RPC call them.
     */
    determineApiCalls() {
        try {
            this.apiCallList = {};
            this.apiCallFunc = {};
            return Array.from(this.providers).map(api => this.addProvider(api));
        } catch (e) {
            console.log(`Exception in determineApiCalls: ${ e }`);
            return config.reportError('Exception in determineApiCalls', e);
        }
    }

    MakeDynamicCall(callName: string, callData: any, user: IApiUser) {
        let args = [];

        if (this.apiWantsUser[callName]) {
            args.push(user);
        }

        for (let varName of Array.from(this.apiCallList[callName])) {
            args.push(callData[varName]);
        }

        let result = this.apiCallFunc[callName].apply(this.apiOwner[callName], args);
        return result;
    }

    hasApiFunc(callName: string) {
        return this.apiCallFunc.hasOwnProperty(callName);
    }

    /**
     * Handle a call to the API
     * @param callName - [string] function name provider_method
     * @param callData - [EdgeSocketRequestData]
     * @param callerSession - [ApiSocketUser]
     */ 
    doProcessApiCall(callName: string, callData: EdgeSocketRequestData, callerSession: ApiSocketUser): Promise<EdgeSocketResponseData> {
        if (this.apiCallList[callName] != null) {

            let result = this.MakeDynamicCall(callName, callData, callerSession);
            return result;
        }
        return new Promise((resolve) => {
            resolve({
                error: true,
                message: `Invalid api call: ${ callName }`,
            });
        });
    }
};
