export default class WebServerHelper {
    /**
     * Given a filename and path list, resolve with the full path that exists.
     * @param filename
     * @param pathList
     * @param fake
     */
    static doFindFileInPath(filename: string, pathList: string[] | string): Promise<string>;
    static doCompileLessFiles(filenameList: string[] | string): Promise<{
        css: any;
    }>;
    static doCompileStylusFile(filename: string): Promise<string>;
    static doCompilePugFile(filename: string): Promise<string>;
    static doCompileCoffeeFile(filename: string): Promise<string>;
}
