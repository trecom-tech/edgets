import config                   from 'edgecommonconfig';
import * as Client              from '@sendgrid/client';
import EdgeEmail                from './EdgeEmail';
import EdgeMailData             from './EdgeMailData';
import EdgeCommonMailerError    from './EdgeCommonMailerError';

export { default as EdgeEmail }     from './EdgeEmail';
export { default as EdgeMailData }  from './EdgeMailData';

export class EdgeCommonMailer {
    static haveCreds: boolean;
    static singleton: EdgeCommonMailer  = null;
    private logger  : any               = null;

    constructor() {
        let credentials = config.getCredentials('sendgrid');
        if ((credentials == null) || (credentials.type == null) || credentials.type !== 'sendgrid') {
            EdgeCommonMailer.warnMissingCreds();
        } else if (credentials.type === 'sendgrid') {
            EdgeCommonMailer.haveCreds = true;
            Client.setApiKey(credentials.apikey);
        }
        
        this.logger = config.getLogger('EdgeCommonMailer');
    }

    /**
     * @returns singleton for the elastic search
     */
    static getMailer(forceRefresh?: boolean) {
        if (EdgeCommonMailer.singleton === null || forceRefresh) {
            EdgeCommonMailer.singleton = new EdgeCommonMailer();
        }

        return EdgeCommonMailer.singleton;
    }

    /**
     * warn if cred is not found.
     */
    static warnMissingCreds() {
        config.status('EdgeCommonMailer: Warning, sendgrid credentials not found!');
        return true;
    }

    /**
     * Log error internally
     */
    internalReportError(error: EdgeCommonMailerError, message: any) {
        this.logger.error('Email was failed to sent', {
            success: 0,
            message: message,
            errorMessage: error.message,
            code: error.code,
            timestamp: new Date()
        }, error);

        return false;
    }

    /**
     * Log success internally
     */
    internalReportMail(message: any, result: any) {
        this.logger.info('Email sent successfully', {
            success: 1,
            message: message,
            result: result,
            timestamp: new Date()
        });

        return true;
    }

    /**
     * Send email
     * @param message [EdgeMailData | any | EdgeEmail] - message body
     */
    async doSendMessage(message: EdgeMailData | any) {
        try {
            // Check if it has credential
            if (!EdgeCommonMailer.haveCreds) {
                EdgeCommonMailer.warnMissingCreds();
                
                throw new EdgeCommonMailerError('No SendGrid Api Configured.');
            }

            // Email initiailization
            let mail: EdgeEmail;
            if (message instanceof EdgeEmail) {
                mail = message;
            } else if (typeof message === 'object') {
                mail = new EdgeEmail(message);
            } else {
                throw new EdgeCommonMailerError('message should be EdgeMailData or object');
            }

            // Email validation
            mail.validate();

            // Send email
            const request = {
                method: 'POST',
                url: '/v3/mail/send',
                body: mail.toJSON()
            };

            try {
                const result = await Client.request(request);
                this.internalReportMail(message, result);
                return result;
            } catch (err) {
                throw new EdgeCommonMailerError('Sendgrid failed to send email', 'SENDGRID_ERROR', err.response.body);
            }
            
        } catch(err) {
            this.internalReportError(err, message);
            throw err;
        }
    }
}

export default EdgeCommonMailer;
