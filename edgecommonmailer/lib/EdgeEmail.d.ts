/**
 * EdgeEmail Class
 * Includes mail validation and parsing markdown template
 */
import { Mail } from '@sendgrid/helpers/classes';
import EdgeMailData from './EdgeMailData';
export default class EdgeEmail extends Mail {
    constructor(data: EdgeMailData);
    parseTemplate(template: string): void;
    validate(): void;
}
