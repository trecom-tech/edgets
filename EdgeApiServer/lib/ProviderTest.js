"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ApiProvider_1 = require("./ApiProvider");
class ProviderTest extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
    }
    getBaseName() {
        return 'test';
    }
    /**
     * The most basic form of API call, /test/SimpleTest outputs the current system date.
     * http://localhost:8001/api/test/doSimpleTest.json
     */
    doSimpleTest() {
        return new Date().toString();
    }
    ;
    doShowUser(user) {
        let result = {
            name: user.myName,
            ip_address: user.ipAddress,
            authAttempts: user.authAttempts,
            timezoneOffset: user.timezoneOffset,
            browser: user.userAgent,
            location: user.location
        };
        return result;
    }
}
exports.ProviderTest = ProviderTest;
;
exports.default = ProviderTest;
//# sourceMappingURL=ProviderTest.js.map