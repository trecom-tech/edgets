import Server    from '../src/EdgeServer';
import config    from 'edgecommonconfig';
const jsonfile = require('jsonfile');
import * as path from "path";

process.env.redisHostWQ   = process.env.REDIS_HOST;
process.env.redisReadHost = process.env.REDIS_HOST;
process.env.redisHost     = process.env.REDIS_HOST;
process.env.mqHost        = process.env.AMQP_HOST;

config.setCredentials("ApiServers", [process.env.API_SERVER]);
config.setCredentials('MongoDB', {
    url    : `${process.env.DB_HOST}`,
    options: {
        connectTimeoutMS: 600000,
        poolSize        : 16,
        socketTimeoutMS : 600000,
    },
});

describe('EdgeServer', () => {
    const job = jsonfile.readFileSync(path.join(__dirname, `/ExampleTestFile.json`));

    before(async () => {
        await Server.setup();
    });

    // it('appendData', async () => {
    //     job.type = 'append';
    //     await Server.processJob(job);
    // });

    it('insertdata', async () => {
        job.type = 'insertdata';
        await Server.processJob(job);
    });

    it('deletepath', async () => {
        job.type = 'deletepath';
        await Server.processJob(job);
    });

    it('message', async () => {
        job.type = 'message';
        job.txt = 'testMessage';
        await Server.processJob(job);
    });
});