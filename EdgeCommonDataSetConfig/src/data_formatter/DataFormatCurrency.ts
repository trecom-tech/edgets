//
// class for money data type
//
// @extends [DataFormatterType]
//
import DataFormatCommon  from './DataFormatCommon';
import DataFormatterType from './DataFormatterType';

const common  = new DataFormatCommon();
const numeral = require('numeral');

export class DataFormatCurrency extends DataFormatterType {
    name: string = 'money';
    align: string = 'right';
    input_width: number =120;

    constructor() {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if ((data == null) || (data === null) || (data === "")) {
            return "";
        }

        return numeral(common.getNumber(data)).format('$ #,###.[##]');
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        return common.getNumber(data);
    }

    attachEditor(elSourceField: any, path: any) {
        super.attachEditor(elSourceField, path);
        return elSourceField.attr("onclick", "this.select();");
    }
}

export default DataFormatCurrency;