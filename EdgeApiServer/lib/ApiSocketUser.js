"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ApiWebUser_1 = require("./ApiWebUser");
const edgecommonconfig_1 = require("edgecommonconfig");
// A socket is something that connects on top of the web so 
// the Socket user has all the functions of a web user as well.
//
class ApiSocketUser extends ApiWebUser_1.default {
    constructor(myConnectionID, socket) {
        super(myConnectionID, socket.conn && socket.conn.remoteAddress, socket.conn && socket.conn.request);
        this.myConnectionID = myConnectionID;
        this.socket = socket;
        this.myName = null;
        this.rec = null;
        this.authAttempts = 0;
        this.sendDelayedQueue = [];
        this.sendDelayedTimer = null;
        this.streamingListNumber = null;
        this.configSendFlat = false;
        this.subscriptionList = [];
        this.wantStatusUpdate = false;
        this.isAdmin = false;
        this.myName = this.myConnectionID;
        this.onSetupSocket(this.socket);
    }
    /**
     * Become an admin user, no specific use case assigned right now
     */
    setAdmin() {
        this.isAdmin = true;
        return true;
    }
    /**
     * Unset admin
     */
    setNoAdmin() {
        this.isAdmin = false;
        this.wantStatusUpdate = false;
        return true;
    }
    setConfigFlat() {
        console.log('Set flat on ', this.myName);
        return this.configSendFlat = true;
    }
    /**
     * Subscribe to status updates
     */
    setWantStatusUpdates() {
        this.wantStatusUpdate = true;
        return true;
    }
    logIncoming(cmd, obj) {
        // Uncomment the next line for detailed coms tracing
        // console.log chalk.white("<< ") + @myName + " " + chalk.cyan("Reply") + " [" + chalk.white(cmd) + "] => ", JSON.stringify(obj)
        return true;
    }
    send(cmd, obj) {
        // Uncomment the next line for detailed coms tracing
        // console.log chalk.blue(">> ") + @myName + " " + chalk.blue("Reply") + " [" + chalk.cyan(cmd) + "] => ", JSON.stringify(obj)
        this.socket.emit(cmd, obj);
        return true;
    }
    /**
     * Send this back to the client and they know to expect data
     * then use sendPush to send the actual data.
     */
    startStreamingList() {
        if (!this.streamingListNumber) {
            this.streamingListNumber = 999;
        }
        this.streamingListNumber++;
        return `STREAM:${this.streamingListNumber}`;
    }
    /**
     * Terminate the streaming list by sending a special command to the receiver
     * @param stream_id
     */
    endStreamingList(stream_id) {
        this.sendPush(stream_id, 'END');
        return true;
    }
    /**
     * Generic push notification message
     * @param cmd string steramer id
     * @param obj message
     */
    sendPush(cmd, obj) {
        // console.log ">> " + @myName + " Push [" + chalk.cyan(cmd) + "] => ", JSON.stringify(obj)
        this.socket.emit('cmd-push', {
            command: cmd,
            data: obj,
        });
        return true;
    }
    //
    //  Push message but delayed to avoid sending too many at once
    sendPushDelayed(cmd, obj) {
        if (this.sendDelayedTimer != null) {
            clearTimeout(this.sendDelayedTimer);
        }
        if (this.sendDelayedQueue == null) {
            this.sendDelayedQueue = [];
        }
        this.sendDelayedQueue.push([cmd, obj]);
        return this.sendDelayedTimer = global.setTimeout(this.onPushDelayed, 200);
    }
    onPushDelayed() {
        if (this.sendDelayedTimer != null) {
            clearTimeout(this.sendDelayedTimer);
            delete this.sendDelayedTimer;
        }
        if (!this.sendDelayedQueue) {
            console.log('Missing queue?');
            return false;
        }
        while (this.sendDelayedQueue.length > 20) {
            this.sendDelayedQueue.shift();
        }
        for (let msg of Array.from(this.sendDelayedQueue)) {
            this.sendPush(msg[0], msg[1]);
        }
        delete this.sendDelayedQueue;
        return true;
    }
    addSubscription(subscription) {
        //
        // Subscription request should have a channel
        if (subscription.channel == null) {
            console.log('Invalid Subscription request:', subscription);
            return false;
        }
        this.subscriptionList.push(subscription);
        edgecommonconfig_1.config.status(`User ${this.myConnectionID} subscribed to `, subscription.channel);
        return true;
    }
    filterChange(data) {
        //
        //  Change example:
        //  {"type":"change","path":"/item/1221/test_data/apple",
        //    "stamp":"2016-03-07T13:54:06.756Z","kind":"E",
        //    "lhs":"2016-03-07T13:53:15.448Z","rhs":"2016-03-07T13:54:05.986Z"}
        //
        for (const sub of this.subscriptionList) {
            if (sub.channel === data.channel) {
                this.send('change', data);
                return true;
            }
        }
        return false;
    }
    reset() {
        return true;
    }
}
exports.default = ApiSocketUser;
;
//# sourceMappingURL=ApiSocketUser.js.map