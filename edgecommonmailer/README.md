# EdgeCommonMailer

> Provides ways to send various type of emails

    npm install --save "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonMailer"
    import EdgeCommonMailer from 'edgecommonmailer'
    mailer = EdgeCommonMailer.getMailer();

> To send normal text-version emails
```
const message: any = {
    to          : 'to@gmail.com',
    subject     : 'EdgeCommonMailer Test',
    from        : 'noreply@edgecommonmailer.com',
    text        : 'Test',
};
await mailer.doSendMessage(message);
```
> To send markdown version template. This will be parsed and compiled into text/html version.
```
const sampleMarkdown = `
# h1 Heading 8-)
![Minion](./minion.png)
`;
const message: any = {
    to          : 'to@gmail.com',
    subject     : 'EdgeCommonMailer Test',
    from        : 'noreply@edgecommonmailer.com',
    mdTemplate  : sampleMarkdown,
};
await mailer.doSendMessage(message);
```

## API
### `doSendMessage(message: EdgeMail | any): Promise<EdgeCommonMailerError)`
> message is normal json extending EdgeMailData or instance of EdgeMail.

## Markdown
If you include `mdTemplate` in the message data, content will be parse from that markdown into text/html version.

For images, if the markdown image token is looking for local image, then it just loads image from `config.getDataPath('image')` and embed as base64 data url.

## Testing    
### run the test
```
npm run test
```


