"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ApiProvider_1 = require("./ApiProvider");
const uuidv4 = require("uuid/v4");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const edgeapi_1 = require("edgeapi");
const EdgeError_1 = require("./EdgeError");
const RedisLock = require('redislock');
const slug = require('slug');
class ProviderUtility extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
        this.sequentialDatasetName = 'lookup';
        this.sequentialCollectionName = 'sequential';
        this.sequentialDatasetName = 'lookup';
        this.sequentialCollectionName = 'sequential';
    }
    onInit() {
    }
    getBaseName() {
        return 'utility';
    }
    doGetGuid() {
        return uuidv4();
    }
    doGetServerStamp(user) {
        return {
            timestamp: new Date().getTime(),
            user: user.myName,
        };
    }
    doGetNextSequential(user, slug) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.hasRole('serviceUser')) {
                throw new EdgeError_1.default('Authentication required', 'ProviderUtility');
            }
            const dm = this.doInternalGetSequentialDataSet();
            if (!this.redisLock) {
                yield edgeapi_1.default.cacheGet(`SEQ_${slug}`);
                this.redisLock = RedisLock.createLock(edgeapi_1.default.globalCacheReadonly, {
                    timeout: 2000,
                    retries: 200,
                    delay: 100,
                });
            }
            yield this.redisLock.acquire(`app:sequential:lock`);
            const existingInRedis = yield edgeapi_1.default.cacheGet(`SEQ_${slug}`);
            if (existingInRedis) {
                const nextSequential = parseInt(existingInRedis, 10) + 1;
                yield this.doInternalReserveSequential(dm, slug, nextSequential);
                yield this.redisLock.release();
                return nextSequential;
            }
            const nextSequential = yield dm.doGetItem(`/${this.sequentialCollectionName}/${slug}`);
            nextSequential.count = nextSequential.count ? nextSequential.count + 1 : 1;
            yield this.doInternalReserveSequential(dm, slug, nextSequential.count);
            yield this.redisLock.release();
            return nextSequential.count;
        });
    }
    doGetNextNewObject(user, datasetName, collectionName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!user.hasRole('serviceUser')) {
                throw new EdgeError_1.default('Authentication required', 'ProviderUtility');
            }
            const nextSequentialId = yield this.doGetNextSequential(user, slug(`db ${datasetName} ${collectionName}`));
            const dm = new edgedatasetmanager_1.default(datasetName);
            const item = yield dm.doGetItem(`/${collectionName}/${nextSequentialId}`);
            return item.data;
        });
    }
    doInternalGetSequentialDataSet() {
        if (!this.dmSequential) {
            this.dmSequential = new edgedatasetmanager_1.default(this.sequentialDatasetName);
        }
        return this.dmSequential;
    }
    doInternalReserveSequential(dm, slug, nextSequential) {
        return __awaiter(this, void 0, void 0, function* () {
            yield dm.doUpdate(`/${this.sequentialCollectionName}/${slug}`, { count: nextSequential });
            yield edgeapi_1.default.cacheSet(`SEQ_${slug}`, nextSequential.toString());
        });
    }
}
exports.ProviderUtility = ProviderUtility;
exports.default = ProviderUtility;
//# sourceMappingURL=ProviderUtility.js.map