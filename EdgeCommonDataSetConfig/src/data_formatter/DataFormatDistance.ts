//
// class for distance data type
//
// @extends [DataFormatterType]
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

const numeral = require('numeral');
const common  = new DataFormatCommon();

export class DataFormatDistance extends DataFormatterType {
    name: string        = 'distance';
    width: number       = 100;
    input_width: number = 100;
    align: string       = 'left';

    constructor(...args: any[]) {
        super();
    }

    //
    // Takes meters in, returns a formatted string
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if (data === 0) {
            return 0;
        }

        //  DATA is in METERS
        const feet = 3.28084 * data;
        // feet = 5280 * data
        if (feet < 50) {
            return "< 50 ft";
        }
        if (feet < 1000) {
            return Math.ceil(feet) + " ft";
        }
        data = feet / 5280;
        return numeral(data).format('#,###.##') + " mi";
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        // console.log "Unformat distance doesn't work:", data
        const val = common.getNumber(data);
        return val;
    }
}

export default DataFormatDistance;