###

 Class:  TableView
 =====================================================================================

 This is a multi-purpose table view that handles many aspects of a fast display and
 edit table/grid.

 @example:
 new TableView $(".tableHolder")

 Events:
 =====================================================================================

 "click_col" : will trigger when a row is clicked with the name "col", for example
 @example: table.on "click_zipcode", (row, e) =>

###

globalKeyboardEvents    = new EvEmitter()
globalTableEvents       = new EvEmitter()
globalTableAdmin        = true
minHeightOfTable        = 400
DataSetConfig 			= require 'edgecommondatasetconfig'
dataFormatter   		= new DataSetConfig.DataFormatter()

$(window).on "resize", (e)=>
	w = $(window).width()
	h = $(window).height()
	globalTableEvents.emitEvent "resize", [w, h]

$(document).on "keyup", (e)=>

	if e.target == document.body
		if e.keyCode == 38
			# console.log "DOC KEY [up]"
			globalKeyboardEvents.emitEvent "up", [e]
		else if e.keyCode == 40
			# console.log "DOC KEY [down]"
			globalKeyboardEvents.emitEvent "down", [e]
		else if e.keyCode == 37
			# console.log "DOC KEY [left]"
			globalKeyboardEvents.emitEvent "left", [e]
		else if e.keyCode == 39
			# console.log "DOC KEY [right]"
			globalKeyboardEvents.emitEvent "right", [e]
		else if e.keyCode == 9
			# console.log "DOC KEY [tab]"
			globalKeyboardEvents.emitEvent "tab", [e]
		else if e.keyCode == 13
			# console.log "DOC KEY [enter]"
			globalKeyboardEvents.emitEvent "enter", [e]
		else if e.keyCode == 27
			# console.log "DOC KEY [esc]"
			globalKeyboardEvents.emitEvent "esc", [e]

	return true

$(document).on "mousedown", (e)=>
	globalKeyboardEvents.emitEvent "global_mouse_down", [ e ]
	return true

class TableView

	@SORT_ASC  : 1
	@SORT_DESC : -1
	@SORT_NONE : 0

	# @property [String] imgChecked html to be used when checkbox is checked
	imgChecked     : "<img src='/images/checkbox.png' width='16' height='16' alt='Selected' />"

	# @property [String] imgNotChecked html to be used when checkbox is not checked
	imgNotChecked  : "<img src='/images/checkbox_no.png' width='16' height='16' alt='Selected' />"

	## -------------------------------------------------------------------------------------------------------------
	## returns the number of rows checked
	##
	## @return [Integer] no of rows checked in current table
	##
	numberChecked: () =>
		return Object.keys(@rowDataSelected).length

	## -------------------------------------------------------------------------------------------------------------
	## Initialize the class by sending in the ID of the tag you want to beccome a managed table.
	## This should be a simple <table id='something'> tag.
	##
	## @param [JQueryElement] elTableHolder the $() referenced element that will hold the table
	## @param [Boolean] showCheckbox if check boxes are visible or not
	##
	constructor: (@elTableHolder, @showCheckboxes) ->

		GlobalClassTools.addEventManager(this)

		# @property [Array] list of columns as array
		@colList           = []
		@actionColList     = []
		@titleColList      = []

		# @property [Array] list of rows as array
		@rowDataRaw      = []
		@rowDataSelected = {}
		@rowDataFlagged  = {}

		# @property [Boolean] to show headers of table
		@showHeaders    = true

		# @property [Boolean] to show textbox to filter data
		@showFilters     = true
		@allowSelectCell = true
		@showResize      = true
		@showConfigTable = true
		@enableMouseOver = false
		@showMathRow     = false

		# @property [Object] currentFilters current applied filters to the table
		@currentFilters    = {}
		@currentGroups     = []
		@sortRules         = []
		@lockList          = {}
		@groupToHide       = {}
		@showGroupPadding  = false
		@groupPaddingWidth = 10

		@currentFilters[location.tableName] = {}

		# @property [Boolean|Function] callback to call on context menu click
		@contextMenuCallbackFunction = 0

		# @property [Boolean|Function] add menu to context menu
		@contextMenuCallSetup        = 0

		# @property [int] the max number of rows that can be selected
		@checkboxLimit               = 1

		# @property [boolean] flag indicating whether this table needs to creating its DOM elements
		@renderRequired              = true

		# @property [Boolean] showCheckboxes if checkbox to be shown or not default false
		@showCheckboxes              = !!@showCheckboxes?

		# @property [Boolean] showFlags if flag to be shown or not default false		
		@showFlags                   = false

		# @property [Number] flagLimit: limit on count of flagged rows
		@flagLimit                   = 0

		# @property [Array] flagsColumn: possible flag values, element format is { name: "xxx", icon: "yyy" }
		@flagsColumn     = [{ name: "None", icon: "none" }]

		# @property [Array] rowClassSetters: functions to set class by row content
		# element format : { "className": "xxx", "callback": function(){..} }
		@rowClassSetters = {}

		if (!@elTableHolder[0])
			console.log "Error: Table id #{@elTableHolder} doesn't exist for #{@primaryTableName}"
			return

		#@property [int] the offset from the top to start showing
		@offsetShowingTop  = 0
		@offsetShowingLeft = 0

		@title 			   = null

		##|
		##|  The height of each cell to draw
		##|
		@dataCellHeight   = 24
		@headerCellHeight = 24
		@filterCellHeight = 20
		@mathCellHeight   = 24

		@titleHeight  	  = 32
		##
		## constants for StatusBar and ScrollBar
		@statusBarHeight  = 26
		@scrollBarSize    = 20

		##
		## Content to be shown when No Data Cell
		@noDataMsg        = "No data available"
		@loadingMsgTxt    = "Loading data..."

		##|
		##|  Event manager for Event Emitter style events
		@on "added_event", @onAddedEvent
		@on "focus_cell", @onFocusCell
		@on "click_row_selected", @toggleRowSelected
		@on "click_row_flagged", @toggleFlag

		@attachGlobalEvent "Keyboard", "up", @moveCellUp
		@attachGlobalEvent "Keyboard", "down", @moveCellDown
		@attachGlobalEvent "Keyboard", "left", @moveCellLeft
		@attachGlobalEvent "Keyboard", "right", @moveCellRight
		@attachGlobalEvent "Keyboard", "tab", @moveCellRight
		@attachGlobalEvent "Keyboard", "enter", @pressEnter
		@attachGlobalEvent "Keyboard", "global_mouse_down", @onGlobalMouseDown
		@attachGlobalEvent "Keyboard", "change", @onGlobalDataChange		
		@attachGlobalEvent "Table", "table_change", @onGlobalTableChange
		@on "resize", @onResize
		window.addEventListener "new_data", @onGlobalNewData, false

		##|
		##|  Create a unique ID for the table, that doesn't change
		##|  even if the table is re-drawn
		if !@gid?
			@gid = GlobalValueManager.NextGlobalID()

	## -------------------------------------------------------------------------------------------------------------
	## to add the table in the view from datamap
	##
	## @example tableView.addTable "zipcode"
	## @param [String] tableName name of the table to consider from datamap
	## @param [Function] columnReduceFunction will be applied to each row and each column and if returns true then only column will be included
	## @param [Function] reduceFunction will be applied to each row and if returns true then only row will be included
	## @return [Boolean]
	##
	addTable: (tableName, @columnReduceFunction, @overallReduceFunction) =>
		@primaryTableName = tableName
		true

	##|
	##| Return a column from source name
	findColumn: (source)=>

		for c in @colList
			if c.getSource() == source
				return c

	##|-------------------------------------------------------------------------------------------------------------
	##| Convert one of the columns into an action column
	##|
	moveActionColumn: (sourceName, isAdd = true)=>

		##|  check if the column is already action
		for col, index in @actionColList
			if col.getSource() == sourceName
				if isAdd
					return true
				else
					@actionColList.splice index, 1
					@updateColumnList()
					@resetCachedFromSize()
					@updateVisibleText()
					return true

		columns = DataMap.getColumnsFromTable(@primaryTableName, null)
		for col, index in columns
			if col.getSource() is sourceName
				if isAdd
					@actionColList.push col
					@updateColumnList()
					@resetCachedFromSize()
					@updateVisibleText()
					return true

		if !@pendingActionColumn?
			@pendingActionColumn = []

		for pActionCol, index in @pendingActionColumn
			if pActionCol.sourceName is sourceName
				pActionCol.isAdd   = isAdd
				return true

		@pendingActionColumn.push
			sourceName: sourceName
			isAdd     : isAdd
		false

	##|-------------------------------------------------------------------------------------------------------------
	##| Convert one of the columns into an title column
	##|
	moveTitleColumn: (sourceName, isAdd = true, isUpdatingTable = true)=>

		##|  check if the column is already title
		for col, index in @titleColList
			if col.getSource() == sourceName
				if isAdd
					return true
				else
					@titleColList.splice index, 1
					DataMap.changeColumnAttribute @primaryTableName, col.getSource(), "locked", false
					if isUpdatingTable
						@updateColumnList()
						@updateVisibleText()
					return true

		columns = DataMap.getColumnsFromTable(@primaryTableName, null)

		for col in columns
			if col.getSource() == sourceName
				if isAdd
					@titleColList.push col
					DataMap.changeColumnAttribute @primaryTableName, col.getSource(), "locked", true
					if isUpdatingTable
						@updateColumnList()
						@updateVisibleText()
					return true
				else
					return true

		if !@pendingTitleColumn?
			@pendingTitleColumn = []

		for pTitleCol, index in @pendingTitleColumn
			if pTitleCol.sourceName is sourceName
				pTitleCol.isAdd   = isAdd
				return true

		@pendingTitleColumn.push
			sourceName: sourceName
			isAdd     : isAdd
		false

	##|-------------------------------------------------------------------------------------------------------------
	##| Add a column to the end of the table
	##|
	addActionColumn: (options)=>

		config    =
			name      : ""
			render    : null
			width     : 100
			callback  : null
			source    : null
			tableName : @primaryTableName

		$.extend config, options
		
		##  check if the column is already action
		for col in @actionColList
			if col.getName() == config.name then return true
		
		button = new TableViewColButton(@primaryTableName, config.name)
		button.width       = config.width
		button.actualWidth = config.width

		if config.render?
			button.render = config.render

		button.source = config.source
		@actionColList.push button
		if config.callback?
			@on "click_#{button.getSource()}", config.callback

		@resetCachedFromSize()
		@updateVisibleText()

		true

	##|-------------------------------------------------------------------------------------------------------------
	##| Add a column to the end of the table
	##|
	addTitleColumn: (options)=>

		config    =
			name      : ""
			render    : null
			width     : 100
			callback  : null
			source    : null
			tableName : @primaryTableName

		$.extend config, options

		button = new TableViewColButton(@primaryTableName, config.name)
		button.width       = config.width
		button.actualWidth = config.width

		if config.render?
			button.render = config.render

		button.source = config.source
		@titleColList.push button

		if config.callback?
			@on "click_#{button.getSource()}", config.callback

		@resetCachedFromSize()
		@updateVisibleText()

		true

	##
	## set whether showing filters in the table or not
	setShowFilter: (@showFilters) =>
		true

	## -------------------------------------------------------------------------------------------------------------
	## Table cache name is set, this allows saving/loading table configuration
	##
	## @param [String] tableCacheName the cache name to attach with table
	##
	setTableCacheName: (@tableCacheName) =>

	## -------------------------------------------------------------------------------------------------------------
	## make the table with fixed header and scrollable
	##
	## @param [Boolean] fixedHeader if header is fixed or not
	##
	setSimpleAndFixed: () =>

		@showFilters     = false
		@showHeaders     = false
		@fixedHeader     = true

	## -------------------------------------------------------------------------------------------------------------
	## make the table with fixed header and scrollable
	##
	## @param [Boolean] fixedHeader if header is fixed or not
	##
	setFixedHeaderAndScrollable: (@fixedHeader = true) =>

		$(window).on 'resize', () =>
			@cachedVisibleWidth  = null
			@cachedTotalWidth    = null
			@cachedVisibleHeight = null

	## -------------------------------------------------------------------------------------------------------------
	## remove the checkbox for all items except those included in the bookmark array that comes from the server
	##
	## @param [Array] bookmarkArray the array of key to consider as bookmark
	## @return [Boolean]
	##
	resetChecked : () =>
		return false

	getRowSelected: (id)=>
		val = DataMap.getDataField @primaryTableName, id, "row_selected"
		if val? and val == true then return true
		false

	getRowFlag: (id)=>
		val = DataMap.getDataField @primaryTableName, id, "row_flagged"
		if !val then return null
		val

	getRowLocked: (id)=>
		if @lockList[id]? then return true
		return false

	##|
	##| Toggle a row as selected/not selected
	toggleRowSelected: (row) =>

		if @getRowLocked(row.id) then return false

		val = @getRowSelected row.id
		newVal = val == false

		if val
			DataMap.getDataMap().updatePathValueEvent "/#{@primaryTableName}/#{row.id}/row_selected", false
			globalTableEvents.emitEvent "row_selected", [ @primaryTableName, row.id, false ]
			delete @rowDataSelected[row.id]

		else

			if @checkboxLimit == 1
				for id in Object.keys(@rowDataSelected)
					globalTableEvents.emitEvent "row_selected", [ @primaryTableName, id, false ]
					DataMap.getDataMap().updatePathValueEvent "/#{@primaryTableName}/#{id}/row_selected", false

				@rowDataSelected = {}

			if @numberChecked() >= @checkboxLimit
				return false

			DataMap.getDataMap().updatePathValueEvent "/#{@primaryTableName}/#{row.id}/row_selected", true
			globalTableEvents.emitEvent "row_selected", [ @primaryTableName, row.id, true ]
			@rowDataSelected[row.id] = true

		@resetChecked()

		true

	##
	## Toggle flag from one to next one
	toggleFlag: (row, e) =>
		elem = $(e.target)
		pos  = elem.offset()		
		menu = new PopupMenu "Flags", pos.left + 70, pos.top + 24

		for flag in @flagsColumn
			menu.addItem "<i class='#{flag.icon}'></i> #{flag.name}", (event, data) =>
				@setRowFlag row, data 
			, flag
		true

	setRowFlag: (row, flag) =>
		if !row or !row.id? then return false
		if !flag? or !flag.name then return false

		newValue = flag.name

		## check if this value is just the same as before
		exValue  = @getRowFlag row.id 
		if newValue is exValue then return false 

		## check the limit on count of Flagged rows
		if !@rowDataFlagged[row.id]? or @rowDataFlagged[row.id] is "None"
			if @flagLimit > 0 and Object.keys(@rowDataFlagged).length >= @flagLimit
				return false

		DataMap.getDataMap().updatePathValueEvent "/#{@primaryTableName}/#{row.id}/row_flagged", newValue
		@rowDataFlagged[row.id] = newValue
		globalTableEvents.emitEvent "row_flagged", [ @primaryTableName, row.id, newValue ]
		true

	##
	## get index of this flag in flagsColumn
	getFlagIndex: (name) =>
		for flag, index in @flagsColumn
			if name is flag['name'] then return index 
		return 0

	##
	## Scroll-handlers
	scrollUp: (amount) =>
		@offsetShowingTop += amount
		@updateScrollbarSettings()
		@resetCachedFromScroll()
		true

	scrollRight: (amount) =>
		@offsetShowingLeft += amount
		@updateScrollbarSettings()
		@resetCachedFromSize()
		true

	##|
	##|  New data from the server
	onGlobalNewData: (e) =>

		if !e? or e.detail.tablename == @primaryTableName

			if @resetTimer?
				clearTimeout(@resetTimer)

			@resetTimer = setTimeout ()=>
				delete @resetTimer
				@updateRowData()
			, 50

			true

	##|
	##|  Event triggered when any tableview has a column change
	##|  so that we can see if we need to update the view
	onGlobalTableChange: (tableName, sourceName, field, newValue) =>

		if tableName == @primaryTableName.replace(/;.*/, "")
			@onGlobalNewData(null)
		true

	onGlobalDataChange: (path, newData) =>
		##|
		##|  Something globally change the value of a path, see if we care
		cell = @findPathVisible(path)
		if cell? then @updateVisibleText()
		true

	onGlobalMouseDown: (e) =>
		##|
		##|  remove focus on a mouse down someplace, it will
		##|  get reset if the mouse was on this table
		@setFocusCell null, null

	##|
	##|  Set a column's filter to show a popup instead of clear typing
	setColumnFilterAsPopup: (sourceName) =>

	pressEnter: (e) =>

		# console.log @primaryTableName, "pressEnter cell=", @currentFocusCell, "path=", @currentFocusPath, e.path

		if !@currentFocusCell? and !@currentFocusPath?
			return false

		if @currentFocusCell? and !@currentFocusPath?
			##|
			##|  Likely due to an open editor so now reset focus
			@setFocusCell(@currentFocusRow, @currentFocusCol, e)

		if !@currentFocusCell? or !@currentFocusPath?
			return false

		if e.path? then @currentFocusPath = e.path

		parts     = @currentFocusPath.split "/"
		tableName = parts[1]
		record_id = parts[2]
		fieldName = parts.splice(3).join('/')
		path      = @currentFocusPath

		for row in @rowDataRaw
			if not row.id? then continue
			if row.id.toString() == record_id

				for c in @colList
					row[c.getSource()] = DataMap.getDataField @primaryTableName, row.id, c.getSource()

					if c.getSource() == fieldName

						if c.getEditable()
							@currentFocusPath = null
							DataMap.getDataMap().editValue path, @currentFocusCell.el

				##|  Use the new event manager
				@emitEvent "click_#{fieldName}", [ row, e ]
				@emitEvent "click_row", [ row, e ]

		true

	onColumnResizeDrag: (diffX, diffY, e) =>

		##|
		##|  Locally and temporary change width
		##|
		source   = @colByNum[@resizingColumn].getSource()
		col      = @findColumn(source)
		newWidth = @resizingBefore + diffX

		if newWidth < 10 then newWidth = 10
		if newWidth > 800 then newWidth = 800

		col.changeColumn("width", newWidth)
		col.currentWidth = newWidth
		@resetCachedFromSize()
		@updateVisibleText()
		true

	onColumnResizeFinished: (diffX, diffY, e) =>

		source   = @colByNum[@resizingColumn].getSource()
		col      = @findColumn(source)
		newWidth = @resizingBefore + diffX

		if newWidth < 10 then newWidth = 10
		if newWidth > 800 then newWidth = 800

		DataMap.changeColumnAttribute col.tableName, col.getSource(), "width", newWidth
		col.currentWidth = newWidth
		delete @resizingColumn
		true

	##
	## Enable custom tooltips on MouseHover event
	## [Param] tooltipRenderFunc: function with params [ row, col ]
	setCustomTooltip: (bool, callback) =>
		if !bool? then return false
		if bool is false
			@hasCustomTooltip = false
		else if bool is true
			if !callback? or typeof callback isnt "function" then return false
			@hasCustomTooltip  = true
			@tooltipRenderFunc = callback
		true

	##
	## Hander for mouse hover event
	onMouseHover: (e) =>
		coords = GlobalValueManager.GetCoordsFromEvent(e)
		if !e.path?
			@tooltipWindow.hide()
			return false

		@tooltipShowing = false
		row  = @findRowFromPath e.path
		col  = @findColFromPath e.path

		if @hasCustomTooltip is true
			w = @tooltipWindow.getBodyWidget()
			w.resetClasses "ninjaTooltipBody"
			@tooltipWindow.floatingWin.addClass "ninjaTooltip"
			##
			## No tooltip on Header and Filter rows
			if row is "Header" or row is "Filter" then return false

			result = @tooltipRenderFunc row, col, @tooltipWindow

			if result
				@tooltipWindow.html result
				@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y - 10 - @tooltipWindow.getHeight())
				##
				## if current mouse pointer is on TooltipWindow, try to avoid this
				if coords.y <= (@tooltipWindow.getTop() + 10 + @tooltipWindow.getHeight())
					@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y + 10)	
				@tooltipWindow.show()
				@tooltipShowing = true
				return true

		for c in @colList
			if c.getSource() is col
				w = @tooltipWindow.getBodyWidget()
				w.resetClasses "ninjaTooltipBody"
				@tooltipWindow.floatingWin.addClass "ninjaTooltip"

				if row? and row[col]? and row[col] isnt ""
					result = c.renderTooltip(row, row[col], @tooltipWindow)
					if result == true
						@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y - 10 - @tooltipWindow.getHeight())
						##
						## if current mouse pointer is on TooltipWindow, try to avoid this
						if coords.y <= (@tooltipWindow.getTop() + 10 + @tooltipWindow.getHeight())
							@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y + 10)	

						@tooltipWindow.show()
						@tooltipShowing = true
		true

	##
	## Enable or disable mouse hover events
	setEnableMouseover: (isEnabled)=>
		@enableMouseOver = isEnabled

	##
	## Hander for mouse out events
	onMouseOut: (e) =>

		if @lastMouseMovePath? and @enableMouseOver
			@lastMouseMovePath = null
			globalTableEvents.emitEvent "mouseover", [ @primaryTableName, null, null, null ]

		if @mouseHoverTimer?
			clearTimeout @mouseHoverTimer
			delete @mouseHoverTimer

		if @tooltipShowing? and @tooltipShowing == true
			@tooltipShowing = false
			@tooltipWindow.hide()

		true

	##
	## Handler for mouse move events
	onMouseMove: (e)=>

		if e? and e.path? and @enableMouseOver
			##|
			##|  If mouseover events are enabled
			##|  Emit the event as Table Name, Path, Row Data, col
			##|
			if e.path != @lastMouseMovePath
				row = @findRowFromPath e.path
				col = @findColFromPath e.path
				globalTableEvents.emitEvent "mouseover", [ @primaryTableName, e.path, row, col ]
				@lastMouseMovePath = e.path

		if @mouseHoverTimer? then clearTimeout @mouseHoverTimer

		@mouseHoverTimer = setTimeout @onMouseHover, 1000, e

		if @tooltipShowing? and @tooltipShowing == true
			coords = GlobalValueManager.GetCoordsFromEvent(e)
			@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y - 10 - @tooltipWindow.getHeight())

			##
			## if current mouse pointer is on TooltipWindow, try to avoid this
			if coords.y <= (@tooltipWindow.getTop() + 10 + @tooltipWindow.getHeight())
				@tooltipWindow.moveTo(coords.x - (@tooltipWindow.getWidth() / 2), coords.y + 10)			
		true

	## -------------------------------------------------------------------------------------------------------------
	## to setup event internally for the table
	##
	internalSetupMouseEvents: () =>

		@virtualScrollV.on "scroll_to", (amount)=>
			totalRows      = @getTableTotalRows()
			maxVisibleRows = @getTableMaxVisibleRows()
			@offsetShowingTop = amount

			if @offsetShowingTop < 0
				@offsetShowingTop = 0

			else if @offsetShowingTop > totalRows - maxVisibleRows
				@offsetShowingTop = totalRows - maxVisibleRows			

			@resetCachedFromScroll()			
			@updateScrollbarSettings()
			true

		@virtualScrollH.on "scroll_to", (amount)=>
			totalCols          = @getTableTotalCols()
			lastVisibleCols    = @getTableLastVisibleCols()
			@offsetShowingLeft = amount

			if @offsetShowingLeft < 0
				@offsetShowingLeft = 0

			else if @offsetShowingLeft > totalCols - lastVisibleCols
				@offsetShowingLeft = totalCols - lastVisibleCols			

			@resetCachedFromScroll()
			@updateScrollbarSettings()			
			true

		@elTheTable.on "mouseout", @onMouseOut
		@elTheTable.on "mousemove", @onMouseMove
		@elTheTable.on "mousedown", (e) =>
			##|
			##|  Let other tables know because this gives up focus on cells
			globalKeyboardEvents.emitEvent "global_mouse_down", [ e ]

			##|
			##|  Check for a resize start
			data = WidgetTag.getDataFromEvent e

			if data.path? and data.path == "grab"
				##|
				##|  Start resizing, save the location that was selected at the start
				@resizingColumn = data.cn
				@resizingRow    = data.rn
				@resizingBefore = @colByNum[data.cn].currentWidth
				return GlobalMouseDrag.startDrag(e, @onColumnResizeDrag, @onColumnResizeFinished)

			return false

		@elTheTable.on "click touchbegin", (e) =>

			if e.target.className == "dataFilter"
				$(e.target).focus()
				return false

			data = WidgetTag.getDataFromEvent e

			if !data? or !data.path?
				return false

			row  = @findRowFromPath data.path
			col  = @findColFromPath data.path

			if !row?
				@setFocusCell(null)
				return false

			if row == "Filter"
				##|
				##|  Don't do anything here for filter columns
				@setFocusCell(null)
				return false

			if row == "Header"
				##|
				##|  Sort columns
				@sortByColumn col
				@setFocusCell(null)
				return false

			if data.action?
				@emitEvent "click_#{data.action}", [ row, e ]
			else if data.titlecol?
				@emitEvent "click_#{data.titlecol}", [ row, e ]
			else if row? and row.id?
				@setFocusCell(data.vr, data.vc, e)
			else
				@setFocusCell(null)

			if col isnt "row_selected" and col isnt "row_flagged"
				##|
				##|  Use the new event manager
				@pressEnter(e)

				##|
				##|  See if the cell has a focus function, which we
				##|  only call on mouse focus not keyboard
				realCol = @findColumn(col)
				if realCol? and realCol.onFocus? then realCol.onFocus(e, col, row)

			false

	onCopyToClipboard: (e, value)=>
		copyToClipboard(value)
		true

	onContextMenuData: (e)=>

		data = WidgetTag.getDataFromEvent(e)
		row  = @findRowFromPath data.path
		col  = @findColFromPath data.path

		if !row? or !col? then return false

		popupMenu = new PopupMenu "Options", e

		aValue = DataMap.getDataField @primaryTableName, row.id, col

		if aValue?
			aValue = aValue.toString().trim()

			if aValue.length > 300
				aValueToShow = aValue.substr(0, 300) + "..."

			else
				aValueToShow = aValue

			popupMenu.addItem "Copy '#{aValueToShow}'", @onCopyToClipboard, aValue

		bValue = DataMap.getDataFieldFormatted @primaryTableName, row.id, col

		if bValue? and bValue != aValue
			popupMenu.addItem "Copy '#{bValue}'", @onCopyToClipboard, bValue

		if @showCheckboxes and row.id?

			if !aValue? or row.id != aValue
				popupMenu.addItem "Copy '#{row.id}'", @onCopyToClipboard, row.id

		##
		## copy entire row
		rowValue = ""

		for key, value of row
			if typeof value is "object" then value = JSON.stringify(value)
			if rowValue isnt "" then rowValue += ", "
			rowValue += value

		popupMenu.addItem "Copy Row", @onCopyToClipboard, rowValue
		true

	##
	## Export table content
	exportTable: (tableName = @primaryTableName, fileName = "Untitled", reducer = @reduceFunction) =>

		try
			ws = XLSX.utils.json_to_sheet(DataMap.getValuesFromTable(tableName, reducer))
			wb = XLSX.utils.book_new()
			wb.SheetNames.push(tableName)
			wb.Sheets[tableName] = ws

			wopts = { bookType:'xlsx', bookSST:false, type:'binary' }
			s2ab = (s) ->  
				buf = new ArrayBuffer(s.length)
				view = new Uint8Array(buf)
				for char, i in s
					view[i] = s.charCodeAt(i) & 0xFF
				return buf

			wbout = XLSX.write(wb, wopts)
			blob = new Blob([s2ab(wbout)], {type:"application/octet-stream"})
			saveAs blob, fileName

		catch e
			console.log "Error exporting table data: ", e, tableName, fileName

	##
	## Export table menu
	onContextMenuExportTable: (e) =>
		subMenu = new PopupMenu "Export Table", e

		ws = XLSX.utils.json_to_sheet(DataMap.getValuesFromTable(@primaryTableName, @reduceFunction))

		subMenu.addItem "Copy to Clipboard", (e, table) =>
			table.onCopyToClipboard e, XLSX.utils.sheet_to_csv(ws)
		, this

		subMenu.addItem "Save as Excel", (e, table) =>
			##
			## Open a Modal Dialog where to type file name
			options =
				title: "Save as..."
				content: "Please enter name of the file you are going to save(.xlsx)."
				inputs: [
					type: "text"
					label: "File Name"
					name: "input1"
				]
				buttons: [
					type: "submit"
					text: "Save"
				]
				onSubmit: (form)=>
					fileName = form.input1.value
					regexp   = /\.xlsx/g 
					if regexp.test(fileName) is false
						fileName += ".xlsx"
					@exportTable @primaryTableName, fileName
					return true

			m = new ModalForm(options)			
		, this
		true

	##|
	##|  Called when context menu on a group row
	onContextMenuGroup: (rowNum, coords) =>
		popupMenu = new PopupMenu "Data Grouping", coords.x, coords.y

		##| add sorting menu item
		for source in @currentGroups
			popupMenu.addItem "Removing #{source}", (e, source) =>
				@ungroupColumn source
				newList = []

				for name in @currentGroups
					if name == source then continue
					newList.push name

				@currentGroups = newList
				@groupToHide = {}
				@updateRowData()
			, source

		true

	onResize: (w, h) =>

		@cachedVisibleWidth        = null
		@cachedTotalWidth          = null
		@cachedVisibleHeight       = null
		@cachedTotalVisibleCols    = null
		@cachedTotalVisibleRows    = null
		@cachedMaxTotalVisibleCol  = null
		@cachedLastTotalVisibleCol = null
		@cachedMaxTotalVisibleRows = null

		if !@isVisible() then return

		if globalDebugResize
			console.log "ViewTable tableView onResize(#{w}, #{h}) fixed=#{@fixedWidth}, #{@fixedHeight}"

		@elTableHolder.width(w)
		@elTableHolder.height(h)
		@refreshScrollbar()
		@resetCachedFromSize()
		@updateRowData()
		true

	##
	## Refresh Scrollbars' sizes
	refreshScrollbar: () =>
		@virtualScrollV?.onResize @virtualScrollV.width(), (if @hasTitle() then @titleHeight + @getTableVisibleHeight() else @getTableVisibleHeight())
		@virtualScrollH?.onResize @elTableHolder.width(), @virtualScrollH.height()

	##|
	##|  Dialog to rename a column
	onRenameField: (source) =>

		for index, col of @colByNum

			if col.getSource() == source
				m = new ModalForm
					title:        "Name:"
					content:      "Enter a new name for this column"
					position:     "top"
					inputs: [
						name:  "input1"
						label: "Name"
						value: col.getName()
					]
					buttons: [
						type:  "submit"
						label: "Save"
					]
					onsubmit: (form) =>
						DataMap.changeColumnAttribute @primaryTableName, source, "name", form.input1.value
						@updateVisibleText()
						m.hide()

	contextMenuChangeType: (source, coords) =>
		popupMenu = new PopupMenu "New Type: #{source}", coords.x-150, coords.y-200

		for name in Object.keys(dataFormatter.formats)
			popupMenu.addItem name, (e, opt)=>

				for index, col of @colByNum
					if col.getSource() == source
						DataMap.changeColumnAttribute col.tableName, source, "type", opt
						return
			, name
		true

	onContextMenuHeader: (source, coords) =>
		if globalOpenWindowList? and globalOpenWindowList.length > 0

			for w in globalOpenWindowList

				if w.isVisible and w.title == "Editing table: #{@primaryTableName}" and w.configurations.tableName == "#{@primaryTableName}"
					return false

		popupMenu = null

		for index, col of @colByNum

			if col.getSource() == source
				popupMenu = new PopupMenu "#{col.getName()}", coords.x, coords.y
				popupMenu.addItem "Hide column", (e, source)=>
					DataMap.changeColumnAttribute @primaryTableName, source, "visible", false
					@updateRowData()
				, source
				popupMenu.addItem "Group similar values", (e, source)=>
					@groupBy(source)
				, source

				##|
				##|  Allow table to reconfigure
				if @showConfigTable
					popupMenu.addItem "Rename Column", (e, source)=>
						@onRenameField source
						@updateVisibleText()
					, source
					popupMenu.addItem "Change Column Type", (e, source)=>
						setTimeout ()=>
							@contextMenuChangeType source, coords
						, 500
					, source

					if globalTableAdmin
						popupMenu.addItem "Open table editor", (e, source)=>
							for index, col of @colByNum
								if col.getSource() == source
									doPopupView "ShowTableEditor", "Editing table: #{@primaryTableName}", "tableEditor_#{@primaryTableName}", 1300, 800, (view)=>
										view.deserialize
											table: @primaryTableName
						, source

					##
					## Export table data 
					popupMenu.addItem "Export Table", (ev) =>
						setTimeout @onContextMenuExportTable, 250, ev
					, true					

		if !popupMenu?
			popupMenu = new PopupMenu "Unknown #{source}", coords.x, coords.y

		##|
		##|  Make a list of hidden columns that we can offer to unhide
		for col in @colList

			if col.visible? and col.visible == false
				popupMenu.addItem "Show #{col.getName()}", (e, list)=>
					showName = list.pop()
					source   = list.pop()
					num      = @findColumn(source).getOrder()
					DataMap.changeColumnAttribute col.tableName, showName, "visible", true
					DataMap.changeColumnAttribute col.tableName, showName, "order", num
					setTimeout () =>
						@resetCachedFromSize()
						@updateVisibleText()
					, 200
				, [source, col.getSource()]

		false

	## -------------------------------------------------------------------------------------------------------------
	## Internal function that enables context menus
	##
	setupContextMenu: (@contextMenuCallbackFunction) =>

		if @contextMenuCallSetup == 1 then return true
		@contextMenuCallSetup = 1

		@elTableHolder.bind "contextmenu", (e) =>
			data   = WidgetTag.getDataFromEvent(e)

			if !data.path? then return false

			if m = data.path.match(/^.group[^a-zA-Z](.*)/)
				@onContextMenuGroup(parseInt(m[1]), data.coords)
				return false

			if m = data.path.match(/^.*Header[^a-zA-Z](.*)/)
				@onContextMenuHeader(m[1], data.coords)
				return false

			@onContextMenuData(e)
			return false
		true

	##|
	##|  Add a new sort rule in a given order
	##|  sortMode = 0 / toggle
	##|  sortMode = -1 / descending
	##|  sortMode = 1 / ascending
	##|  sortMode = other value / error
	##|
	addSortRule: (sourceName, sortMode) =>
		found = null

		for rule in @sortRules
			if rule.source == sourceName
				found = rule
				break

		if found == null
			found = { source: sourceName, tableName: @primaryTableName, state: 0 }
			@sortRules = [ found ]

		if sortMode? and sortMode == 0
			found.state = found.state * -1
			if found.state == 0 then found.state = 1

		else if sortMode? and sortMode == 1
			found.state = 1

		else if sortMode? and sortMode == -1
			found.state = -1

		else
			@addSortRule sourceName, 0
		
		@updateRowData()
		true

	##|
	##|  Lock a given value at the top, does not matter what the sort is
	addLock: (id) =>
		@lockList[id] = true
		@updateRowData()
		true

	removeLock: (id) =>
		delete @lockList[id]
		@updateRowData()
		true

	getLocked: () =>
		@lockList

	## -------------------------------------------------------------------------------------------------------------
	## internal function to apply sorting on table with column and sorting type
	##
	## @param [Object] column column on which sorting should be applied
	## @param [String] type it can be ASC|DESC
	##
	applySorting: (rowData) =>

		if !@sortRules?
			@sortRules = []

		@numLockedRows = Object.keys(@lockList).length

		if @sortRules.length == 0 and @numLockedRows == 0 then return rowData

		sorted = rowData.sort (a, b) =>

			for rule in @sortRules
				if rule.state == 0 then continue

				aValue = DataMap.getDataField @primaryTableName, a.id, rule.source
				bValue = DataMap.getDataField @primaryTableName, b.id, rule.source

				if aValue? and !bValue then return rule.state
				if bValue? and !aValue then return rule.state * -1
				if !aValue? and !bValue? then return 0

				if rule.state == -1 and aValue < bValue then return 1
				if rule.state == -1 and aValue > bValue then return -1
				if rule.state == 1 and aValue < bValue then return -1
				if rule.state == 1 and aValue > bValue then return 1

			return 0

		if @numLockedRows > 0
			finalList = []

			for rec in sorted
				if @lockList[rec.id]?
					finalList.push rec
					rec.locked = true

			for rec in sorted

				if !@lockList[rec.id]? then finalList.push rec

			return finalList

		sorted

	## -------------------------------------------------------------------------------------------------------------
	## Apply filters stored in "currentFilters" to each column and show/hide the rows
	##
	applyFilters: () =>
		##|
		##|  Generate a function that takes a row and returns true or false
		##|  to keep or filter the row.
		##|
		strJavascript = ""

		if @overallReduceFunction? and typeof @overallReduceFunction == "string"
			strJavascript += "try {\n" + @overallReduceFunction + ";\n} catch (e) { console.log(\"eee=\",e); }\n";

		else if @overallReduceFunction? and typeof @overallReduceFunction == "function"
			window["overallReduceFunction#{@gid}"] = @overallReduceFunction
			strJavascript += "if (window['overallReduceFunction#{@gid}'](row) == false) return false;\n"

		filters = []

		for tableName, fieldList of @currentFilters

			for fieldName, filterValue of fieldList

				if !filterValue.comparatorFunc or !filterValue.value or filterValue.value is '' then continue

				field = "row['#{fieldName}']"

				## special workflow when the column to filter is "row_selected"
				if fieldName is "row_selected" or fieldName is "row_flagged"
					strJavascript += "var col = this.getColumnByName('#{fieldName}');\nif (!col) return false;\n"
					strJavascript += "var val = false;\n"
					strJavascript += "if (typeof(#{field}) !== 'undefined' && #{field} != null) {\n val = #{field};\n}\n"
					strJavascript += "if ((#{filterValue.comparatorFunc})(val) == false) return false;\n"

				else
					strJavascript += "if (typeof(#{field}) == 'undefined') return false;\n"
					strJavascript += "if (#{field} == null) return false;\n"
					strJavascript += "var col = this.getColumnByName('#{fieldName}');\nif (!col) return false;\n"
					strJavascript += "var val = col.getFormatter().unformat(#{field});\n"
					strJavascript += "var comp = col.getFormatter().unformat(this.currentFilters['#{tableName}']['#{fieldName}']['value']);\n"
					strJavascript += "if ((#{filterValue.comparatorFunc})(val, comp) == false) return false;\n"

		strJavascript += "return true;\n"
		reduceFunction = new Function("row", strJavascript)
		@reduceFunction = reduceFunction.bind(this)
		true

	##|
	##|  If this table is not a "fixed size" table then we have to adjust the
	##|  widget height to allow all rows to show.
	##|
	updateFullHeight: () =>
		if @fixedHeader then return

		h = 0

		if @hasTitle() then h = h + @titleHeight
		if @showHeaders then h = h + @headerCellHeight
		if @showFilters then h = h + @filterCellHeight

		h = h + (@totalAvailableRows * @dataCellHeight)

		if not @fixedHeight
			@elTableHolder.height(h)

		h

	##|
	##|  Refresh the list of available columns
	updateColumnList: () =>
		@colList  = []
		@colByNum = {}

		##
		## Add a flag column if existing
		if @showFlags
			foundFlags = false

			for tCol in @titleColList

				if tCol.constructor.name is "TableViewColFlag"
					foundFlags = true

			if !foundFlags
				c = new TableViewColFlag(@primaryTableName)
				pos = if @showCheckboxes then 1 else 0
				@titleColList.splice(pos, 0, c)

		##|
		##|  Add a checkbox column if needed
		if @showCheckboxes
			foundCheckbox = false

			for tCol in @titleColList

				if tCol.constructor.name is "TableViewColCheckbox"
					foundCheckbox = true

			if !foundCheckbox
				c = new TableViewColCheckbox(@primaryTableName)
				@titleColList.unshift c

		##|
		##|  Find the columns for the specific table name
		columns = DataMap.getColumnsFromTable(@primaryTableName, @columnReduceFunction)
		columns = columns.sort (a, b)->
			return a.getOrder() - b.getOrder()

		for col in columns

			if not col.getVisible() then continue

			foundInActionCol = false

			if col.getLocked()
				@moveTitleColumn col.getSource(), true, false

			else
				@moveTitleColumn col.getSource(), false, false

			for acol in @actionColList

				if acol.getSource() == col.getSource()
					foundInActionCol = true
					break

			if !foundInActionCol

				for tcol in @titleColList

					if tcol.getSource() == col.getSource() 
						foundInActionCol = true
						break

			if foundInActionCol then continue

			if @isColumnEmpty(col)
				# console.log "Column is empty:", col.getSource()
				continue

			@colList.push(col)

		##|
		##|  reset available columns and sort them
		total = 0
		sortedColList = @colList.sort (a, b) ->
			return a.getOrder() - b.getOrder()

		for col in sortedColList

			foundInGroup = false

			for source in @currentGroups

				if source == col.getSource()
					col.isGrouped = true
					foundInGroup = true
					break

			if foundInGroup
				continue

			else
				col.isGrouped = false

			@colByNum[total] = col
			total++

		##|
		##|  Try to make action and title columns from those pending
		if @pendingActionColumn?
			tempList = []

			for pActionCol in @pendingActionColumn
				tempList.push pActionCol

			delete @pendingActionColumn

			for pActionCol in tempList
				@moveActionColumn pActionCol.sourceName, pActionCol.isAdd

		if @pendingTitleColumn?
			tempList = []

			for pTitleCol in @pendingTitleColumn
				tempList.push pTitleCol
			delete @pendingTitleColumn

			for pTitleCol in tempList
				@moveTitleColumn pTitleCol.sourceName, pTitleCol.isAdd


		##
		## to show sorting arrow on ActionColumn header
		for acol in @actionColList
			acol.sort = 0

			if acol.constructor.name is "Column"

				for sortrule in @sortRules

					if sortrule.tableName is @primaryTableName and sortrule.source is acol.getSource()
						acol.sort = sortrule.state

				@colByNum[total] = acol
				total++

		@resetCachedFromSize()
		true

	## -------------------------------------------------------------------------------------------------------------
	## function to update row data on the screen if new data has been added in datamapper they can be considered
	##
	updateRowData: () =>
		@applyFilters()
		@rowDataRaw = []
		allData     = DataMap.getValuesFromTable @primaryTableName, @reduceFunction

		##
		## Clear cached table data
		@clearCachedTableData()

		if !@isVisible()

			if allData.length > 0
				@totalAvailableRows = allData.length
				globalTableEvents.emitEvent "row_count", [ @primaryTableName, allData.length ]
			return

		##|
		##|  Path 1 - There are no groups defined so just quickly sort and filter
		##|
		if @currentGroups.length == 0
			@showGroupPadding = false
			##|
			##|  Simple data, no grouping
			filteredData = @applySorting(allData)

			for obj in filteredData
				@rowDataRaw.push { id: obj.id, group: null }

			@totalAvailableRows = @rowDataRaw.length
			@updateColumnList()

			@updateFullHeight()

			if @renderRequired then @real_render()

			@layoutShadow()
			@updateScrollbarSettings()
			@resetCachedFromSize()

			globalTableEvents.emitEvent "row_count", [ @primaryTableName, @totalAvailableRows ]
			return

		##|
		##|  Apply grouping filters
		groupedData        = {}
		@rowDataRaw        = []
		@showGroupPadding  = true
		currentGroupNumber = 0

		for item in allData
			key = ""

			for name in @currentGroups

				if key != "" then key += ", "

				displayName = name

				for col in @colList

					if col.getSource() == name then displayName = col.getName()

				value = DataMap.getDataField @primaryTableName, item.id, name
				key += displayName + ": " + value

			if !groupedData[key]?
				groupedData[key] = []

			groupedData[key].push item

		for value in Object.keys(groupedData).sort()
			currentGroupNumber++

			if currentGroupNumber > 7 then currentGroupNumber = 1

			filteredData = groupedData[value]

			if filteredData.length > 0
				##|
				##|  There are rows of data in this group so add the group to the row list.
				@rowDataRaw.push
					id       : null
					cellType : "group"
					name     : value
					group    : currentGroupNumber
					count    : filteredData.length

				if @groupToHide[value]? and @groupToHide[value].visible is false then continue

				filteredData = @applySorting(filteredData)

				for obj in filteredData
					@rowDataRaw.push { id: obj.id, group: currentGroupNumber, visible: true}

		@totalAvailableRows = @rowDataRaw.length
		@updateColumnList()
		@updateFullHeight()

		if @renderRequired then @real_render()

		@layoutShadow()
		@updateScrollbarSettings()

		globalTableEvents.emitEvent "row_count", [ @primaryTableName, @totalAvailableRows ]

		true

	##| -------------------------------------------------------------------------------------------------------------
	##| Enable a status bar along the bottom.
	setStatusBarEnabled: (isEnabled = true) =>
		@showStatusBar = isEnabled

	isVisible: () =>
		if !@elTableHolder? then return false

		pos = @elTableHolder.position()

		if !pos? then return false

		tableWidth = @elTableHolder.outerWidth()

		if pos.top == 0 and pos.left == 0 and tableWidth == 0
			return false

		true

	## -------------------------------------------------------------------------------------------------------------
	## function to make column customizable in the popup
	##
	## @example
	##		table.allowCustomize()
	## @param [Boolean] customizableColumns
	##
	allowCustomize: (@customizableColumns = true) ->

	getTotalActionWidth: () =>
		##|
		##|  Width of all fixed right action columns
		total = 0
		for col in @actionColList
			total += col.getWidth()

		total

	##
	## Get width of all title columns
	getTotalTitleWidth: () =>
		##|
		##|  Width of all fixed left action columns
		total = 0
		for col in @titleColList
			total += col.getWidth()

		total

	##|
	##|  Total available rows to display excluding headers
	getTableTotalRows: () =>
		@totalAvailableRows

	getTableTotalCols: () =>
		total = 0

		for col in @colList

			if col.isGrouped? and col.isGrouped == true then continue

			total++

		total

	getTableVisibleWidth: () =>
		if @cachedVisibleWidth? && @cachedVisibleWidth > 0 then return @cachedVisibleWidth

		maxWidth = @elTableHolder.width()

		if @virtualScrollV? and @virtualScrollV.visible then maxWidth -= @scrollBarSize

		@cachedVisibleWidth = maxWidth - @getTotalActionWidth() - @getTotalTitleWidth()
		@cachedVisibleWidth

	getTableTotalWidth: () =>
		if @cachedTotalWidth? && @cachedTotalWidth > 0 then return @cachedTotalWidth

		maxWidth = @elTableHolder.width()

		if @virtualScrollV? and @virtualScrollV.visible then maxWidth -= @scrollBarSize

		@cachedTotalWidth = maxWidth
		@cachedTotalWidth

	getTableVisibleHeight: () =>
		if @cachedVisibleHeight? && @cachedVisibleHeight > 0 then return @cachedVisibleHeight

		maxHeight = @elTableHolder.height()

		if @hasTitle()
			maxHeight -= @titleHeight

		if @showStatusBar
			maxHeight -= @elStatusBar.height()

		if @virtualScrollH? and @virtualScrollH.visible
			maxHeight -= @scrollBarSize

		if @showMathRow
			maxHeight -= @mathCellHeight * @mathTypes.length

		@cachedVisibleHeight = maxHeight
		@cachedVisibleHeight

	##|
	##|  Returns the maximum rows visible starting at the end of the rows
	getTableMaxVisibleRows: () =>
		if @cachedMaxTotalVisibleRows? then return @cachedMaxTotalVisibleRows

		y           = 0
		visRowCount = 0
		maxHeight   = @getTableVisibleHeight()
		rowNum      = @getTableTotalRows() - 1

		while y <= maxHeight

			if rowNum < 0 then break

			y = y + @getRowHeight({rowNum:rowNum, visibleRow:visRowCount})
			rowNum--
			visRowCount++

		##
		## if there are still rows not showing, should consider header and filter
		if y > maxHeight
			visRowCount -= @getTableLockedRows()

			##
			## if visible part of last row is too short(shorter than 10px)
			## then remove it from visible row count
			if y - maxHeight > @getRowHeight({rowNum: rowNum+1, visibleRow: visRowCount-1}) * 0.2
				visRowCount--

		@cachedMaxTotalVisibleRows = if visRowCount > 0 then visRowCount else 0
		@cachedMaxTotalVisibleRows

	##|
	##|  Number of visible rows
	getTableVisibleRows: () =>
		if @cachedTotalVisibleRows? then return @cachedTotalVisibleRows

		y           = 0
		visRowCount = 0
		maxHeight   = @getTableVisibleHeight()
		rowNum      = @getTableTotalRows() - 1

		while y <= maxHeight

			if rowNum < 0 then break

			y = y + @getRowHeight({rowNum:rowNum, visibleRow:visRowCount})
			rowNum--
			visRowCount++

		##
		## if there are still rows not showing, should consider header and filter
		if y > maxHeight
			visRowCount -= @getTableLockedRows()

			## -gao
			## if visible part of last row is too short(shorter than 10px)
			## then remove it from visible row count
			if y - maxHeight > @getRowHeight({rowNum: rowNum+1, visibleRow: visRowCount-1}) * 0.2
				visRowCount--

		@cachedTotalVisibleRows = if visRowCount > 0 then visRowCount else 0
		@cachedTotalVisibleRows

	getTableMaxVisibleCols: () =>

		if @cachedMaxTotalVisibleCol? then return @cachedMaxTotalVisibleCol

		visColCount = 0
		x           = 0
		colNum      = @getTableTotalCols() - 1
		maxWidth    = @getTableVisibleWidth()

		cn = 0

		while x <= maxWidth and cn <= colNum
			col = @colByNum[cn]
			location =
				colNum 	   : cn
				tableName  : col.tableName
				sourceName : col.getSource()
				visibleCol : cn

			if (cn <= colNum) and @shouldSkipCol(location)
				cn++
				continue

			col.currentWidth = @getColWidth(location)
			x = x + col.currentWidth
			visColCount++
			cn++

		if visColCount > 0 then @cachedMaxTotalVisibleCol = visColCount
		visColCount

	##
	## calculate visible cols at the very right end
	## this value is used to calculate size of horizontal scrollbar
	getTableLastVisibleCols: () =>

		if @cachedLastTotalVisibleCol? then return @cachedLastTotalVisibleCol

		visColCount = 0
		x           = 0
		totalCols   = @getTableTotalCols() - 1
		maxWidth    = @getTableVisibleWidth()

		colNum = totalCols

		while x <= maxWidth and colNum >= 0
			col = @colByNum[colNum]
			location =
				colNum 	   : colNum
				tableName  : col.tableName
				sourceName : col.getSource()
				visibleCol : colNum

			if (colNum >= 0) and @shouldSkipCol(location)
				colNum--
				continue

			col.currentWidth = @getColWidth(location)
			x = x + col.currentWidth
			visColCount++
			colNum--

		##
		## Exclude last column if its width is not wide enough
		if x > maxWidth and (x - maxWidth) > 15
			visColCount--

		if visColCount > 0 then @cachedLastTotalVisibleCol = visColCount
		visColCount

	##|
	##|  Number of visible columns
	getTableVisibleCols: () =>

		if @cachedTotalVisibleCols? then return @cachedTotalVisibleCols

		visColCount = 0
		x           = 0
		colNum      = @offsetShowingLeft
		maxWidth    = @getTableVisibleWidth()
		totalCols   = @getTableTotalCols()

		while x <= maxWidth and colNum >= 0 and colNum < totalCols

			col = @colByNum[colNum]
			location =
				colNum     : colNum
				tableName  : col.tableName
				sourceName : col.getSource()
				visibleCol : colNum

			if (colNum < totalCols) and @shouldSkipCol(location)
				colNum++
				continue

			if colNum >= totalCols
				break

			col.currentWidth = @getColWidth(location)
			x = x + col.currentWidth
			visColCount++
			colNum++

		##
		## Exclude last column if its width is not wide enough
		# if x > maxWidth and (x - maxWidth) > 15
		# 	visColCount--

		if visColCount > 0 then @cachedTotalVisibleCols = visColCount
		visColCount

	##|
	##|  Compute the width of a given column
	getColWidth: (location) =>

		if location.cellType == "group"
			colNum = location.visibleCol

			if colNum == 1 then return 200
			if colNum == 2 then return 90
			if colNum == 3 then return @getTableTotalWidth() - 290 - @groupPaddingWidth
			return 0

		if !@colByNum[location.colNum]?
			return 10

		if @colByNum[location.colNum].actualWidth? and !isNaN(@colByNum[location.colNum].actualWidth)
			return Math.floor(@colByNum[location.colNum].actualWidth)

		if @colByNum[location.colNum].currentWidth and !isNaN(@colByNum[location.colNum].currentWidth)
			return Math.floor(@colByNum[location.colNum].currentWidth)

		@colByNum[location.colNum].getWidth()

	##|
	##|  Compute the height of a given row
	getRowHeight: (location) =>
		@dataCellHeight

	##|
	##|  Return true if a cell is editable
	##|
	getCellEditable: (location) =>
		if !@colByNum[location.colNum]? then return false
		@colByNum[location.colNum].getEditable()

	getCellClickable: (location) =>
		if @colByNum[location.colNum]?
			return @colByNum[location.colNum].getClickable()

		false

	##|
	##|  Returns true if the row/column should have the darker color
	##|  background (striped rows on a table)
	##|
	getCellStriped: (location) =>
		if (@showHeaders or @showFilters) and location.visibleRow == 0 then return false

		if (@showHeaders and @showFilters) and location.visibleRow == 1 then return false

		if location.cellType == "group" then return false

		location.visibleRow % 2 == 0

	getCellGroupNumber: (location) =>
		if location.cellType == "group" then return @rowDataRaw[location.rowNum].group

		if location.cellType == "invalid" or location.visibleCol > 0 then return null

		if !@rowDataRaw[location.rowNum]? then return null

		@rowDataRaw[location.rowNum].group

	##|
	##|  Return right/left/center - left is assumed by default
	getCellAlign: (location) =>
		if location.cellType is "group"

			if location.visibleCol == 1 then return "left"

			return "right"

		@getColumn(location).getAlign()

	getCellTablename: (location) =>
		@primaryTableName

	getCellCalculation: (location) =>
		if location.cellType == "invalid" then return false

		if @actionColList? and @actionColList.length

			for acol in @actionColList

				if acol.getSource() is location.sourceName
					return acol.getIsCalculation()

		if @titleColList? and @titleColList.length

			for tcol in @titleColList

				if tcol.getSource() is location.sourceName
					return tcol.getIsCalculation()

		if @colByNum[location.colNum]?
			return @colByNum[location.colNum].getIsCalculation()

		false

	getCellMath: (location) =>
		return location.cellType == "math"

	getCellSource: (location) =>
		if location.cellType == "invalid" then return null
		if !@colByNum[location.colNum]? then return null
		@colByNum[location.colNum].getSource()

	getCellRecordID: (location) =>
		if !@rowDataRaw[location.rowNum]? then return 0
		@rowDataRaw[location.rowNum].id

	getCellFormatterName: (location) =>
		@colByNum[location.colNum].getFormatterName()

	shouldAdvanceCol: (location) =>
		true

	getCellDataPath: (location) =>
		if location.cellType == "data"
			return "/#{location.tableName}/#{location.recordId}/#{location.sourceName}"

		if location.cellType == "group"
			return "/group/#{location.rowNum}"

		"/unknown/" + location.celltype

	getCellLastData: (location) =>
		visibleCols = @getTableVisibleCols()

		if location.colNum >= visibleCols + @offsetShowingLeft - 1
			return true

		false

	##
	## Set filter value to the next one of current value
	## Only works for the columns that have popup menu as a filter selector
	setNextFilter: (colName) =>
		if !colName? then return false

		col    = @getColumnByName colName
		valObj = @getValuesInColumn(@primaryTableName, colName)
		values = Object.keys(valObj or {})

		if !col? or !values? or values.length < 1 then return false

		if values.length > 10 then return false

		currentValue = @currentFilters[@primaryTableName][colName]?['value']
		index = 0

		if currentValue? and currentValue isnt ""
			index = values.indexOf(currentValue) + 1

			if index >= values.length
				index %= values.length

		@updateFilter @primaryTableName, colName, 'comparator', '='
		@updateFilter @primaryTableName, colName, 'value', values[index]
		true

	## 
	## function to show PopupMenu with comparators
	showComparators: (e) =>
		elem = $(e.target)
		pos = elem.offset()

		parts      = e.path.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnName = parts.splice(3).join('/')

		##
		## disable on action columns
		if @getColumnType(columnName) > 2 then return false

		menu = new PopupMenu "Comparison", pos.left + 70, pos.top + 24
		menu.resize 140

		if columnName is "row_selected" 
			menu.addItem "All", (event, data) =>
				@updateFilter(tableName, columnName, 'comparator', "checkbox_all")

			menu.addItem "Checked", (event, data) =>
				@updateFilter(tableName, columnName, 'comparator', "checkbox_checked")

			menu.addItem "Unchecked", (event, data) =>
				@updateFilter(tableName, columnName, 'comparator', "checkbox_unchecked")

			menu.addItem "Select All", (event, data) =>
				@forAllChecked false, (row) =>
					row.row_selected = true
					@rowDataSelected[row.id] = true
					row

			menu.addItem "Unselect All", (event, data) =>
				@forAllChecked true, (row) =>
					if row.row_selected?
						row.row_selected = false
						delete @rowDataSelected[row.id]
					row

			return true

		else if columnName is "row_flagged"
			menu.addItem "All", (event, data) =>
				@updateFilter(tableName, columnName, 'comparator', "checkbox_all")

			for flag in @flagsColumn
				menu.addItem "<i class='#{flag.icon}'></i> #{flag.name}", (event, data) =>
					@updateFilter(tableName, columnName, 'comparator', data)
				, "row_flagged_#{flag.name}"
			return true

		menu.addItem "=  Equals", (event, data) => 
			if @updateFilter(tableName, columnName, 'comparator', '=') is true
				elem.text '='

		menu.addItem "!= Not Equals", (event, data) => 
			if @updateFilter(tableName, columnName, 'comparator', '!=') is true
				elem.text '!='

		menu.addItem "> Greater Than", (event, data) => 
			if @updateFilter(tableName, columnName, 'comparator', '>') is true
				elem.text '>'

		menu.addItem "<  Less Than", (event, data) => 
			if @updateFilter(tableName, columnName, 'comparator', '<') is true
				elem.text '<'
		, '<'

		menu.addItem "=~ Close to", (event, data) => 
			if @updateFilter(tableName, columnName, 'comparator', '=~') is true
				elem.text '=~'

		true

	##
	## function to show column editor when filter is clicked
	showColumnEditor: (e) =>
		data = WidgetTag.getDataFromEvent(e)
		path = data.path
		el   = $ e.target

		parts      = path.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnName = parts.splice(3).join('/')

		if @getColumnType(columnName) > 2
			return false

		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}

		if !@currentFilters[tableName][columnName]?
			@currentFilters[tableName][columnName] = {}	

		if columnName is "row_selected" or columnName is "row_flagged"
			@showComparators e
			return true

		##
		## Show Select List by PopupMenu if there are less than 11 unique values in the column
		col    = @getColumnByName columnName
		values = @getValuesInColumn(tableName, columnName) 	

		if Object.keys(values)?.length < 11 
			menu = new PopupMenu col.getName(), data.coords.x, data.coords.y
			menu.addItem "All", (event, data) =>
				@updateFilter tableName, columnName, 'comparator', '=~'
				@updateFilter tableName, columnName, 'value', data
			, ""

			for key, value of values 
				menu.addItem "#{col.getFormatter().format key, col.getOptions(), tableName, keyValue}: #{value}", (event, data) =>
					@updateFilter tableName, columnName, 'comparator', '='
					@updateFilter tableName, columnName, 'value', data
				, key

		else
			existingValue = @currentFilters[tableName][columnName]['value']
			formatter     = col.getFormatter()

			formatter.editData el, existingValue, path, (path, newValue) =>
				if @updateFilter(tableName, columnName, 'value', newValue) is true
					el.html col.getFormatter().format newValue, col.getOptions(), tableName, keyValue

		true

	##
	## Update a filter by key-value pair
	## possible keys: 'comparator', 'dataFormatter', 'value', 'comparatorFunc'
	updateFilter: (tableName, columnName, key, value) =>
		if !key then return false
		if !value? then value = ""

		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}

		if !@currentFilters[tableName][columnName]?
			@currentFilters[tableName][columnName] = {}

		filter      = @currentFilters[tableName][columnName]
		exValue     = filter[key]
		filter[key] = value

		if key is 'comparator'
			filter['comparatorFunc'] = @createComparator filter['comparator']

		if filter['comparator']? and filter['value']?
			globalTableEvents.emitEvent "table_update_filter", [ tableName, columnName, key, value ]

			if @updateFilterTimer?
				clearTimeout @updateFilterTimer

			@updateFilterTimer = setTimeout () =>
				@updateRowData()
			, 100

		@updateSaveSearchIconStatus()
		true

	##
	## initialize/setup a filter for a location
	setFilterFunction: (location) =>
		dataPath   = location.cell.children[0].getDataValue("path")

		if !dataPath then return false

		parts      = dataPath.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnName = parts.splice(3).join('/')

		el0 = location.cell.children[0].getTag()
		el1 = location.cell.children[1].getTag()
		el0.removeClass("default")

		if @getColumnType(columnName) > 2
			el0.html ""
			el1.html ""
			return false

		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}

		if !@currentFilters[tableName][columnName]?
			@currentFilters[tableName][columnName] = {}

		if !@currentFilters[tableName][columnName]['dataFormatter']?
			@currentFilters[tableName][columnName]['dataFormatter'] = @getColumnByName(columnName).getFormatter()

		if !@currentFilters[tableName][columnName]['comparator']? 
			if columnName is "row_selected" or columnName is "row_flagged"
				@currentFilters[tableName][columnName]['comparator'] = "none"	
			else 
				@currentFilters[tableName][columnName]['comparator'] = "=~"

		filter = @currentFilters[tableName][columnName]		
		filter['comparatorFunc'] = @createComparator filter['comparator']

		if columnName is "row_flagged"

			if /row_flagged/.test(filter['comparator'])
				flagName        = filter['comparator'].replace "row_flagged_", ""
				filter['value'] = flagName
				icon            = @flagsColumn[@getFlagIndex(flagName)].icon
				el0.html "<i class='#{icon}'></i>"
			else
				el0.html ""
				filter['value'] = "none"

			el1.html ""

		else if columnName is "row_selected"
			## empty content of cell of the filter's comparator
			el0.text ""
			el1.html ""
			filter['value'] = "none"

		else 
			el0.text filter['comparator']
			col = @getColumnByName columnName

			if filter['value']
				el1.html col.getFormatter().format filter['value'], col.getOptions(), tableName, keyValue
				el0.removeClass "empty"
			else
				el1.html ""
				el0.addClass "empty"

		true

	createComparator: (comparator) =>
		switch comparator
			when "=" then return `function(val, comp) {
					if (isNaN(Number(val))) {
						return String(val) === String(comp);
					} else if (val.getTime){
						return val.getTime() === comp.getTime();
					} else {
						return Number(val) === Number(comp);
					}
				}`

			when "!=" then return `function(val, comp) {
					if (isNaN(Number(val))) {
						return String(val) !== String(comp);
					} else if (val.getTime) {
						return val.getTime() !== comp.getTime();
					} else {
						return Number(val) !== Number(comp);
					}
				}`

			when "<" then return `function(val, comp) {
					if (isNaN(Number(val))) {
						return String(val) < String(comp);
					} else if (val.getTime) {
						return val.getTime() < comp.getTime();
					} else {
						return Number(val) < Number(comp);
					}
				}`

			when ">" then return `function(val, comp) {
					if (isNaN(Number(val))) {
						return String(val) > String(comp);
					} else if (val.getTime) {
						return val.getTime() > comp.getTime();
					} else {
						return Number(val) > Number(comp);
					}
				}`

			when "=~" then return `function(val, comp) {
					if (isNaN(Number(val))) {
						return String(val).toLowerCase().indexOf(String(comp).toLowerCase()) >= 0;
					} else if (val.getTime) {
						return Math.abs(val.getTime() - comp.getTime()) <= 86400000;
					} else {
						return Math.abs(Number(val) - Number(comp)) <= 1;
					}
				}`

			when "checkbox_all" then return `function(val, comp) {
					return true;
				}`

			when "checkbox_checked" then return `function(val, comp) {
					return val === true;
				}`			

			when "checkbox_unchecked" then return `function(val, comp) {
					return val === false;
				}`

			else
				if /row_flagged/.test(comparator) 
					flagName = comparator.replace "row_flagged_", ""
					return "function(val, comp) { if('#{flagName}' === 'None') { return (val === '#{flagName}') || (!val); }\nreturn val === '#{flagName}'; }"
				return "function(val, comp) { return null;}"

	##
	## calculate width of DataFilter Input
	getFilterInputWidth: (location) =>
		result   = 0
		maxWidth = location.maxWidth
		colType  = @getColumnType location.sourceName
		if colType is 2
			maxWidth += @getTotalActionWidth()

		result = if maxWidth - location.x < location.colWidth then maxWidth - location.x else location.colWidth
		result = if result > 24 then result - 24 else 0
		result

	setHeaderFilterField: (location) =>
		path = "/#{location.tableName}/Filter/#{location.sourceName}"

		if location.cell.children.length == 0
			location.cell.addClass "dataFilterWrapper"
			location.cell.add "div", "dataFilterIcon"
			location.cell.add "div", "dataFilter"
			location.cell.children[0].bind "click", @showComparators
			location.cell.children[1].bind "click", @showColumnEditor
			location.cell.children[0].addClass "default"

		if location.sourceName is "row_selected" or location.sourceName is "row_flagged"
			location.cell.children[0].setDataPath path
			location.cell.children[0].setClass "dataFilterNoBorder", true
			location.cell.children[1].setDataPath path

		else if location.cell.children[0].getDataValue("path") isnt path
			location.cell.children[0].setDataPath path
			location.cell.children[1].setDataPath path

		filterWidth = @getFilterInputWidth location
		location.cell.children[1].move 24, 0, filterWidth, location.rowHeight
		location.cell.children[1].setClass "text-right", location.align == "right"
		location.cell.children[1].setClass "text-center", location.align == "center"				

		@setFilterFunction location
		true

	setHeaderField: (location) =>
		location.cell.currentCol = location.colNum
		location.cell.html ""
		location.cell.removeClass "spacer"

		if location.visibleRow == 0
			@colByNum[location.colNum].sort = 0

			for sort in @sortRules
				if sort.source == @colByNum[location.colNum].getSource()
					@colByNum[location.colNum].sort = sort.state

			@colByNum[location.colNum].RenderHeader location.cell, location
			location.cell.setDataPath "/#{location.tableName}/Header/#{location.sourceName}"

		else
			@setHeaderFilterField(location)

		true

	setHeaderGroupField: (location) =>
		##|
		##|  Setup the div that is in a header row along left where the group indicator color
		##|  normally goes.  Headers don't have groups
		if location.visibleRow == 0
			location.cell.addClass "tableHeaderField"

		else if location.visibleRow == 1
			location.cell.addClass "dataFilterWrapper"

		true

	setDataField: (location) =>
		if location.cell.classes.indexOf("dataFilterWrapper") >= 0 and location.cell.children.length >= 2
			location.cell.removeClass "dataFilterWrapper"
			location.cell.children[0].hide()
			location.cell.children[1].hide()

		if location.cellType == "invalid"
			location.cell.hide()
			return

		if location.cellType == "group"
			if location.visibleCol == 1
				## 
				## render "collapse" icon for this group row
				if @groupToHide[@rowDataRaw[location.rowNum].name]? and @groupToHide[@rowDataRaw[location.rowNum].name].visible is false
					location.cell.html "<i class='fa fa-fw fa-caret-right'></i>&nbsp#{@rowDataRaw[location.rowNum].name}"
				else
					location.cell.html "<i class='fa fa-fw fa-caret-down'></i>&nbsp#{@rowDataRaw[location.rowNum].name}"

			else if location.visibleCol == 3
				location.cell.html ""

			else if location.visibleCol == 2
				if @rowDataRaw[location.rowNum].count == 1
					location.cell.html "1 Record"
				else
					location.cell.html "#{@rowDataRaw[location.rowNum].count} Records"

			location.cell.setDataPath "/group/#{@rowDataRaw[location.rowNum].name}/#{location.visibleCol}"
			location.cell.on "click", (e) =>

				if !e.path then return false

				paths = e.path.split '/'
				groupName = paths[2]
				@toggleGroupVisible groupName
				true

			return

		col = @getColumn location

		if location.cellType == "math"
			displayValue = @calculateMathField location
			location.cell.html displayValue or ""
			return

		displayValue = DataMap.getDataFieldFormatted @primaryTableName, location.recordId, location.sourceName
		location.cell.html displayValue
		colorValue = DataMap.getCellColor col.tableName, location.recordId, location.sourceName

		if !colorValue? or colorValue is null
			location.cell.css 'color', ''
		else
			location.cell.css 'color', colorValue

		bgColorValue = DataMap.getCellBGColor col.tableName, location.recordId, location.sourceName

		if bgColorValue
			location.cell.css 'background-color', bgColorValue
		else
			location.cell.css 'background-color', ''

		if @lockList? and @lockList.length > 0
			aValue = DataMap.getDataField @primaryTableName, location.recordId, location.sourceName

			if typeof aValue == "number"
				bValue = DataMap.getDataField @primaryTableName, @lockList[0], location.sourceName

				if aValue > bValue
					location.cell.addClass "valueHigher"
					location.cell.removeClass "valueLower"
				else if aValue < bValue
					location.cell.addClass "valueLower"
					location.cell.removeClass "valueHigher"
				else
					location.cell.removeClass "valueLower"
					location.cell.removeClass "valueHigher"

			else
				location.cell.removeClass "valueLower"
				location.cell.removeClass "valueHigher"

		true

	shouldSkipCol: (location) =>
		if !@colByNum[location.colNum]? then return true
		if location.cellType is "group" and location.visibleCol > 3 then return true
		false

	##|
	##|  Returns a state record for the current row
	##|  data - Cells of data
	##|  locked - Cells of header or locked content
	##|  group - Starting a new group
	##|  skip - Skip this row
	##|  invalid - Invalid row
	##|
	getRowType: (location) =>
		if @isHeaderCell(location) then return "locked"
		if !@rowDataRaw[location.rowNum]? then return "invalid"
		if @rowDataRaw[location.rowNum]? and @rowDataRaw[location.rowNum].cellType? and @rowDataRaw[location.rowNum].cellType is "group" then return "group"
		"data"

	getCellType: (location) =>
		if @isHeaderCell(location) then return "locked"
		if @getCellMath(location) then return "math"
		if !location.rowNum? or !@rowDataRaw[location.rowNum]? then return "invalid"
		if !@rowDataRaw[location.rowNum]? then return "invalid"

		# brian- changed from .type to cellType which I think is actually correct but needs testing
		# changed because the show table editor has a "type" column and it causes an issue with rowDataRaw
		# if @rowDataRaw[location.rowNum].type? then return @rowDataRaw[location.rowNum].type
		if @rowDataRaw[location.rowNum].cellType? then return @rowDataRaw[location.rowNum].cellType
		"data"

	isHeaderCell: (location) =>
		if location.visibleRow == 1 and (@showHeaders and @showFilters) then return true
		if location.visibleRow == 0 and (@showHeaders or @showFilters) then return true
		return false

	isResizable: (location) =>
		if not @showResize then return false
		if location.visibleRow > 0 then return false
		if not location.isHeader then return false
		if @showGroupPadding and location.visibleCol == 0 then return false
		if location.sourceName == "row_selected" then return false
		if location.sourceName == "row_flagged" then return false
		if location.colNum >= location.totalColCount then return false
		true

	checkSpecialColumn: (sourceName) =>
		if sourceName is "row_selected"
			return true
		if sourceName is "row_flagged"
			return true
		false

	##|
	##|  Setup the spacer cell for a location
	initializeSpacerCell: (location, spacer) =>
		if location.groupNum?
			spacer.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/
		else
			spacer.setClassOne null, /^groupRowChart/

		spacer.setDataPath "grab"
		spacer.setDataValue "rn", location.rowNum
		spacer.setDataValue "cn", location.colNum
		spacer.setDataValue "vr", 0
		spacer.setDataValue "vc", 0
		spacer.setClass "last_data", false
		spacer.setClass "first-action", false
		spacer.setClass "last-title", false
		spacer.setClass "tableActionHeaderField", false
		true

	updateCellClasses: (location, div) =>
		div.show()

		##|
		##|  Set the column / row data on the element
		div.setDataValue "rn", location.rowNum
		div.setDataValue "cn", location.colNum
		div.setDataValue "vr", location.visibleRow
		div.setDataValue "vc", location.visibleCol
		div.setDataValue "action", null

		if location.groupNum?
			div.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/
		else
			div.setClassOne null, /^groupRowChart/

		##|
		##|  Align right/center as left is the default
		location.cell.setClass "even", @getCellStriped(location)
		location.cell.setClass "first-action", false
		location.cell.setClass "last-title", false
		location.cell.setClass "tableTitleHeaderField", false
		location.cell.setClass "tableActionHeaderField", false

		##|
		##|  Apply alignment to the cell
		location.align = @getCellAlign(location)
		location.cell.setClass "text-right", (location.align == "right")
		location.cell.setClass "text-center", (location.align == "center")

		##|
		##|  Fixed calculation
		location.cell.setClass "calculation", @getCellCalculation(location) and !@getCellMath(location)

		##
		## Math row
		location.cell.setClass "math-cell", @getCellMath(location)
		@updateCellRowClasses location.cell
		true

	##
	## add classes to cells by rowClassSetters
	updateCellRowClasses: (cell, row) =>
		if !row or !row.id
			dataPath = cell.getDataValue('path')

			if !dataPath then return false
			row = @findRowFromPath dataPath

			if !row or !row.id then return false 

		for className, callback of @rowClassSetters
			cell.setClass className, callback(row)

	incrementColumn: (location, showSpacer) =>
		if location.x + location.colWidth > location.maxWidth
			location.colWidth = location.maxWidth - location.x

		if location.spacer?
			location.cell.move location.x, 0, location.colWidth-3, location.rowHeight
			location.spacer.move location.colWidth+location.x-3, 0, 3, location.rowHeight
			location.spacer.show()
			location.spacer.addClass "spacer"
			location.spacer.html ""
			location.shadowVisibleCol++

		else
			location.cell.move location.x, 0, location.colWidth, location.rowHeight

		location.x += location.colWidth
		location.shadowVisibleCol++
		location.visibleCol++
		true

	##
	##  Draw a math row.
	##
	updateVisibleMathRow: (location) =>
		location.x                = 0    ## Pixel location of current column
		location.visibleCol       = 0
		location.colNum           = @offsetShowingLeft
		location.shadowVisibleCol = 0
		location.isHeader         = false
		location.cellType         = "math"
		location.visibleRow++

		if !@shadowCells[location.visibleRow]?
			@shadowCells[location.visibleRow] = @elTheTable.addDiv "tableRow"
			@shadowCells[location.visibleRow].setAbsolute()
			@shadowCells[location.visibleRow].show()

		@shadowCells[location.visibleRow].move 0, location.y, location.maxWidth+location.actionWidth, location.rowHeight
		@shadowCells[location.visibleRow].show()

		## Render title columns on the left of the row
		mathTitleRendered = false

		for tcol, index in @titleColList
			location.cell = @addShadowCell location
			location.cell.show()
			location.sourceName = tcol.getSource()

			##
			## refresh column width 
			location.colWidth = if location.cellType is "group" then @getColWidth(location) else tcol.getWidth()
			location.cell.move location.x, 0, location.colWidth, location.rowHeight

			if location.sourceName isnt "row_selected" and location.sourceName isnt "row_flagged"

				if !mathTitleRendered
					location.cell.setClass "math-title", true
					location.cell.html @mathTypes[location.mathRowNum or 0]?.toUpperCase()
					mathTitleRendered = true

				else
					displayValue = @calculateMathField location
					location.cell.html displayValue or ""

			@updateCellClasses location, location.cell
			location.cell.setClass "last-title", index is @titleColList.length - 1
			@incrementColumn(location)

		while location.x < location.maxWidth
			if location.colNum >= location.totalColCount then break

			if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol
				@shadowCells[location.visibleRow].addDiv "cell"
				@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setAbsolute()

			location.cell       = @shadowCells[location.visibleRow].children[location.shadowVisibleCol]
			location.spacer     = null
			location.tableName  = @getCellTablename location
			location.sourceName = @getCellSource location

			if location.colNum < location.totalColCount - 1
				location.colWidth = @getColWidth(location) 
			else 
				location.colWidth = location.maxWidth - location.x

			if @shouldSkipCol(location)
				location.colNum++
				continue

			if @isResizable(location)
				##|
				##|  Headers get an extra spacer column
				if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol+1
					@shadowCells[location.visibleRow].addDiv "spacer"
					@shadowCells[location.visibleRow].children[location.shadowVisibleCol+1].setAbsolute()

				@initializeSpacerCell(location, @shadowCells[location.visibleRow].children[location.shadowVisibleCol+1])
				location.spacer = @shadowCells[location.visibleRow].children[location.shadowVisibleCol+1]

			if @showGroupPadding and location.visibleCol == 0
				##|
				##|  This is a group indicator column
				location.colWidth = @groupPaddingWidth

				if !location.isHeader
					location.cell.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/
					location.cell.removeClass "even"
					location.cell.html ""
				else
					@setHeaderGroupField location

				@incrementColumn(location)
				continue

			if location.isHeader
				@setHeaderField(location)
			else
				location.recordId = @getCellRecordID(location)
				location.cell.setClass "clickable",   false
				location.cell.setClass "editable",    false
				location.cell.setClass "row_checked", false
				location.cell.setClass "row_locked",  false
				location.cell.setClass "calculation", false

				for flag, index in @flagsColumn
					flagName = flag['name']
					location.cell.setClass "flag_#{index}", false

				location.cell.setDataPath @getCellDataPath(location)
				@setDataField(location)
			
			@updateCellClasses(location, location.cell)
			@incrementColumn(location)

			if @shouldAdvanceCol(location)
				location.colNum++

		##|
		##|  Add in the action rows
		@updateVisibleActionRow(location)

		##|
		##|  Hide any remaining cached cells on the right
		while @shadowCells[location.visibleRow].children[location.shadowVisibleCol]?
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].resetDataValues()
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].hide()
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setDataPath(null);
			location.shadowVisibleCol++

		true

	updateVisibleActionRowText: (location, acol) =>
		location.tableName  = acol.tableName
		location.sourceName = acol.getSource()
		location.cell.setClass "clickable", acol.getClickable()

		if acol.render?
			try
				currentValue = DataMap.getDataField @primaryTableName, location.recordId, location.sourceName
				displayValue = acol.render(currentValue, @primaryTableName, location.sourceName, location.recordId)
			catch e
				console.log "updateVisibleActionRow locaiton=", location
				console.log "Custom render error:", e
				displayValue = ""
			location.cell.html displayValue

		else
			@setDataField location
		true

	updateVisibleActionRow: (location) =>
		count = 0

		for acol, index in @actionColList

			if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol
				@shadowCells[location.visibleRow].addDiv "cell"
				@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setAbsolute()

			location.cell = @shadowCells[location.visibleRow].children[location.shadowVisibleCol]
			location.cell.show()

			##
			## refresh column width 
			location.colWidth   = acol.getWidth()
			location.colNum     = @colList.length + @titleColList.length + index
			location.sourceName = acol.getSource()

			location.cell.move location.x, 0, location.colWidth, location.rowHeight
			location.cell.setClass "text-right", acol.getAlign() == "right"
			location.cell.setClass "text-center", acol.getAlign() == "center"
			location.cell.setClass "first-action", count++ == 0
			location.cell.setClass "editable",    @getCellEditable(location)
			location.cell.setClass "row_checked", @getRowSelected(location.recordId) and @showCheckboxes
			location.cell.setClass "calculation", @getCellCalculation(location) and !@getCellMath(location)
			location.cell.setClass "math-cell", @getCellMath(location)
			location.cell.setClass "last_data", false

			for flag, index in @flagsColumn
				flagName = flag['name']
				location.cell.setClass "flag_#{index}", (flagName == @getRowFlag(location.recordId)) and @showFlags			

			location.cell.removeClass "spacer"
			location.cell.setClass "even", @getCellStriped(location)
			location.cell.setDataValue "rn", location.rowNum
			location.cell.setDataValue "cn", location.colNum
			location.cell.setDataValue "vr", location.visibleRow
			location.cell.setDataValue "vc", location.visibleCol
			location.cell.setDataValue "action", acol.getSource()

			if location.isHeader
				location.cell.setDataPath "/#{@primaryTableName}/Header/#{acol.getSource()}"
				location.cell.addClass "tableActionHeaderField"
			else
				location.cell.setDataPath "/#{@primaryTableName}/#{location.recordId}/#{acol.getSource()}"
				location.cell.setClass "row_locked",  @getRowLocked(location.recordId)

			if location.groupNum?
				location.cell.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/
			else
				location.cell.setClassOne null, /^groupRowChart/

			if location.isHeader

				if location.visibleRow == 0
					acol.RenderHeader location.cell, location
					location.cell.show()
				else
					@setHeaderFilterField location

			else

				if location.state == "group"
					location.cell.removeClass "clickable"
					location.cell.html ""
				else
					@updateVisibleActionRowText(location, acol)

			@updateCellRowClasses location.cell
			location.x += acol.getWidth()
			location.shadowVisibleCol++
			true

	updateVisibleTitleRowText: (location, tcol) =>
		location.tableName  = tcol.tableName
		location.sourceName = tcol.getSource()
		location.cell.setClass "clickable", tcol.getClickable()

		if tcol.render?
			try
				currentValue = DataMap.getDataField @primaryTableName, location.recordId, location.sourceName
				displayValue = tcol.render(currentValue, @primaryTableName, location.sourceName, location.recordId)
			catch e
				console.log "updateVisibleActionRow locaiton=", location
				console.log "Custom render error:", e
				displayValue = ""

			location.cell.html displayValue

		else if @checkSpecialColumn(location.sourceName)
			@renderSpecialColumn(location, tcol)

		else
			@setDataField location

		true

	updateVisibleTitleRow: (location) =>
		count               = 0
		location.isHeader   = @isHeaderCell(location)
		location.groupNum   = @getCellGroupNumber(location)
		location.cellType   = @getCellType(location)
		location.recordId   = @getCellRecordID(location)
		titleCnt            = @titleColList.length or 0

		for tcol, index in @titleColList

			if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol
				@shadowCells[location.visibleRow].addDiv "cell"
				@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setAbsolute()

			cell          = @shadowCells[location.visibleRow].children[location.shadowVisibleCol]
			location.cell = cell
			location.cell.show()
			location.sourceName = tcol.getSource()

			##
			## refresh column width 
			location.colWidth = if location.cellType is "group" then @getColWidth(location) else tcol.getWidth()
			location.cell.move location.x, 0, location.colWidth, location.rowHeight
			location.cell.setClass "text-right", tcol.getAlign() == "right"
			location.cell.setClass "text-center", tcol.getAlign() == "center"
			location.cell.setClass "editable",    false
			location.cell.setClass "row_checked", @getRowSelected(location.recordId) and @showCheckboxes
			location.cell.setClass "last-title", ++count == titleCnt
			location.cell.setClass "calculation", @getCellCalculation(location) and !@getCellMath(location)

			for flag, index in @flagsColumn
				flagName = flag['name']
				location.cell.setClass "flag_#{index}", (flagName == @getRowFlag(location.recordId)) and @showFlags			

			location.cell.removeClass "spacer"
			location.cell.setClass "even", @getCellStriped(location)
			location.cell.setDataValue "rn", location.rowNum
			location.cell.setDataValue "cn", location.totalColCount + index
			location.cell.setDataValue "vr", location.visibleRow
			location.cell.setDataValue "vc", location.visibleCol
			location.cell.setDataValue "titlecol", tcol.getSource()

			if location.isHeader

				for sort in @sortRules
					if sort.source == tcol.getSource()
						tcol.sort = sort.state

				location.cell.setDataPath "/#{@primaryTableName}/Header/#{tcol.getSource()}"
				location.cell.setClass "tableTitleHeaderField", true

			else
				location.cell.setDataPath "/#{@primaryTableName}/#{location.recordId}/#{tcol.getSource()}"
				location.cell.setClass "row_locked",  @getRowLocked(location.recordId)

			if location.groupNum?
				location.cell.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/

			else
				location.cell.setClassOne null, /^groupRowChart/

			if location.isHeader

				if location.visibleRow == 0
					tcol.RenderHeader location.cell, location
					location.cell.show()

				else
					@setHeaderFilterField location

			else

				if location.state == "group"
					location.cell.removeClass "clickable"
					@setDataField(location)

				else
					@updateVisibleTitleRowText(location, tcol)

			@updateCellRowClasses location.cell
			location.x += location.colWidth
			location.shadowVisibleCol++
			location.visibleCol++
			true

	##
	## Draw Group padding in a row
	drawGroupPadding: (location) =>
		location.cell = @shadowCells[location.visibleRow]?.children?[location.shadowVisibleCol]

		if !location.cell? then return false

		location.spacer     = null
		location.isHeader   = @isHeaderCell(location)
		location.groupNum   = @getCellGroupNumber(location)
		location.cellType   = @getCellType(location)
		location.tableName  = @getCellTablename location
		location.sourceName = @getCellSource location

		##|
		##|  This is a group indicator column
		location.colWidth = @groupPaddingWidth

		if !location.isHeader
			location.cell.setClassOne "groupRowChart#{location.groupNum}", /^groupRowChart/
			location.cell.removeClass "even"
			location.cell.html ""
		else
			@setHeaderGroupField location

		true

	##|
	##|  Draw a single row, returns the next rowNum.
	##|
	updateVisibleTextRow: (location) =>
		location.x                = 0    ## Pixel location of current column
		location.visibleCol       = 0
		location.colNum           = @offsetShowingLeft
		location.shadowVisibleCol = 0

		## Draw Group padding if needed
		if @showGroupPadding
			@addShadowCell(location)
			@drawGroupPadding location
			@incrementColumn(location)

		## Render title columns on the left of the row
		@updateVisibleTitleRow(location)

		while location.x < location.maxWidth

			if location.colNum >= location.totalColCount then break

			@addShadowCell(location)

			location.cell       = @shadowCells[location.visibleRow].children[location.shadowVisibleCol]
			location.spacer     = null
			location.isHeader   = @isHeaderCell(location)
			location.groupNum   = @getCellGroupNumber(location)
			location.cellType   = @getCellType(location)
			location.tableName  = @getCellTablename location
			location.sourceName = @getCellSource location

			if location.colNum < location.totalColCount - 1
				location.colWidth = @getColWidth(location) 
			else 
				location.colWidth = location.maxWidth - location.x

			if location.cellType == "invalid"
				console.log "Invalid cell at colNum=#{location.colNum}"
				break

			if @shouldSkipCol(location)
				location.colNum++
				continue

			if @isResizable(location)
				##|
				##|  Headers get an extra spacer column
				if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol+1
					@shadowCells[location.visibleRow].addDiv "spacer"
					@shadowCells[location.visibleRow].children[location.shadowVisibleCol+1].setAbsolute()

				@initializeSpacerCell(location, @shadowCells[location.visibleRow].children[location.shadowVisibleCol+1])
				location.spacer = @shadowCells[location.visibleRow].children[location.shadowVisibleCol+1]

			if location.isHeader
				@setHeaderField(location)

			else
				location.recordId = @getCellRecordID(location)
				location.cell.setClass "clickable",   @getCellClickable(location)
				location.cell.setClass "editable",    @getCellEditable(location)
				location.cell.setClass "row_checked", @getRowSelected(location.recordId) and @showCheckboxes
				location.cell.setClass "row_locked",  @getRowLocked(location.recordId)

				for flag, index in @flagsColumn
					flagName = flag['name']
					location.cell.setClass "flag_#{index}", (flagName == @getRowFlag(location.recordId)) and @showFlags			

				location.cell.setDataPath @getCellDataPath(location)
				@setDataField(location)

			location.cell.setClass "last_data", @getCellLastData(location)			
			@updateCellClasses(location, location.cell)
			@incrementColumn(location)

			if @shouldAdvanceCol(location)
				location.colNum++

		##|
		##|  Add in the action rows
		@updateVisibleActionRow(location)

		##|
		##|  Hide any remaining cached cells on the right
		while @shadowCells[location.visibleRow].children[location.shadowVisibleCol]?
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].resetDataValues()
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].hide()
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setDataPath(null);
			location.shadowVisibleCol++
		true

	updateVisibleText: () =>
		if !@elTheTable? then return

		if !@offsetShowingTop? or @offsetShowingTop < 0
			@offsetShowingTop = 0

		if !@offsetShowingLeft? or @offsetShowingLeft < 0
			@offsetShowingLeft = 0

		if @rowDataRaw.length == 0

			if !@noDataCell?
				@noDataCell = @elTheTable.addDiv "tableRow"
				@noDataCell.setAbsolute()
				@noDataCell.setZ(1)
				@updateNoDataArea()
				@noDataCell.show()
				r1 = @virtualScrollV.setRange 0, 0, 0, 0
				r2 = @virtualScrollH.setRange 0, 0, 0, 0			

			else
				@noDataCell.show()
				@updateNoDataArea()

			marginRight = @getContentRightMargin()
			marginTop   = @getContentTopMargin()
			@noDataCell.move(0, marginTop, @elTableHolder.width() - marginRight, @elTableHolder.height() - marginTop)

		else if @noDataCell?
			@noDataCell.hide()

		## change size of title element
		if @hasTitle() and @elTitle?
			@elTitle.move 0, 0, @getTableTotalWidth, @titleHeight

		y               = if @hasTitle() then @titleHeight else 0
		groupState      = null
		maxHeight       = @getTableVisibleHeight()
		totalRowCount   = @getTableTotalRows()
		refreshRequired = false

		if @hasTitle() then maxHeight += @titleHeight

		##|
		##|  For rabbit mouse scrolling, this resets the offset back to a reasonable amount
		##|  so that the refreshNeeded doesn't have to loop through a bunch of times
		##|
		if @offsetShowingTop >= totalRowCount
			if totalRowCount > 1 then @offsetShowingTop = totalRowCount - 1 else @offsetShowingTop = 0

		location =
			visibleRow    : 0
			rowNum        : 0
			totalColCount : @getTableTotalCols()
			maxWidth      : @getTableVisibleWidth() + @getTotalTitleWidth()
			actionWidth   : @getTotalActionWidth()
			titleWidth    : @getTotalTitleWidth()

		lockRowsRemain = @numLockedRows
		hasFinishedLockedRows = false

		# console.log "updateVisibleText offsetShowingTop=", @offsetShowingTop, " offsetShowingLeft=", @offsetShowingLeft, " maxRow=", totalRowCount, "maxCol=", totalColCount
		while y < maxHeight and (location.rowNum < totalRowCount or @isHeaderCell(location))

			if lockRowsRemain ==0 and !hasFinishedLockedRows
				hasFinishedLockedRows = true
				location.rowNum += @offsetShowingTop

			location.rowHeight = @getRowHeight(location)
			location.state     = @getRowType(location)

			if !@shadowCells[location.visibleRow]?
				@shadowCells[location.visibleRow] = @elTheTable.addDiv "tableRow"
				@shadowCells[location.visibleRow].setAbsolute()
				@shadowCells[location.visibleRow].show()

			##|
			##|  Row div goes the entire width
			@shadowCells[location.visibleRow].move 0, y, location.maxWidth+location.actionWidth, location.rowHeight

			if location.state == "invalid"
				##|
				##|  Scroll down too far
				if @offsetShowingTop > 0
					@offsetShowingTop--
					refreshRequired = true
				break

			else if location.state == "skip"
				location.rowNum++
				continue

			else if location.state == "group"
				@shadowCells[location.visibleRow].show()
				@shadowCells[location.visibleRow].removeClass "tableRow"
				@updateVisibleTextRow location
				location.rowNum++

			else if location.state == "locked"
				@shadowCells[location.visibleRow].show()
				@shadowCells[location.visibleRow].removeClass "tableRow"
				@updateVisibleTextRow location

			else if location.state == "data"
				@shadowCells[location.visibleRow].show()
				@shadowCells[location.visibleRow].addClass "tableRow"
				@updateVisibleTextRow location
				location.rowNum++
				if lockRowsRemain > 0 then lockRowsRemain--

			else
				location.rowNum++

			y += location.rowHeight
			location.visibleRow++

		if refreshRequired
			return true

		if @shadowCells[location.visibleRow]?
			@shadowCells[location.visibleRow].hide()
			@shadowCells[location.visibleRow].resetDataValues()

		if @showMathRow
			location.y = if y < maxHeight then y else maxHeight

			for mathType, index in @mathTypes
				location.mathRowNum = index
				@updateVisibleMathRow(location)
				location.y += @mathCellHeight

			location.visibleRow++

		while @shadowCells[location.visibleRow]?
			@shadowCells[location.visibleRow].hide()
			@shadowCells[location.visibleRow].resetDataValues()
			location.visibleRow++
		true

	##
	## Add shadow cells if needed
	addShadowCell: (location) =>
		if @shadowCells[location.visibleRow].children.length <= location.shadowVisibleCol
			@shadowCells[location.visibleRow].addDiv "cell"
			@shadowCells[location.visibleRow].children[location.shadowVisibleCol].setAbsolute()

		@shadowCells[location.visibleRow].children[location.shadowVisibleCol]

	resetCachedFromScroll: () =>
		@cachedTotalVisibleCols    = null
		@cachedTotalVisibleRows    = null
		@cachedMaxTotalVisibleCol  = null
		@cachedLastTotalVisibleCol = null
		@cachedMaxTotalVisibleRows = null
		@onMouseOut()
		true

	resetCachedFromSize: () =>
		@cachedTotalVisibleCols  	= null
		@cachedTotalVisibleRows  	= null
		@cachedMaxTotalVisibleRows  = null
		@cachedMaxTotalVisibleCol  	= null
		@cachedLastTotalVisibleCol  = null		
		@cachedVisibleWidth      	= null
		@cachedTotalWidth           = null
		@cachedVisibleHeight    	= null
		@cachedLayoutShadowWidth	= null

		for col in @colList
			col.currentCol = null

		@layoutShadow()
		@onMouseOut()
		true

	##|
	##|  Up the visibility and settings of the scrollbars
	updateScrollbarSettings: () =>
		currentVisibleCols = @getTableVisibleCols()
		currentVisibleRows = @getTableVisibleRows()
		lastVisibleCols    = @getTableLastVisibleCols()
		maxAvailableRows   = @getTableTotalRows()
		maxAvailableCols   = @getTableTotalCols()
		
		##|
		##|  Don't show more rows than fit on the screen
		if @offsetShowingTop >= maxAvailableRows - currentVisibleRows
			@offsetShowingTop = maxAvailableRows - currentVisibleRows

		maxCurrentVisibleCols = if lastVisibleCols > currentVisibleCols then lastVisibleCols else currentVisibleCols

		if @offsetShowingLeft >= maxAvailableCols - maxCurrentVisibleCols
			@offsetShowingLeft = maxAvailableCols - maxCurrentVisibleCols

		##|
		##| Don't set offset as less than 0
		if @offsetShowingTop < 0 then @offsetShowingTop = 0

		if @offsetShowingLeft < 0 then @offsetShowingLeft = 0

		if @elStatusScrollTextRows?
			@elStatusScrollTextRows.html "Rows #{@offsetShowingTop+1} - #{@offsetShowingTop+currentVisibleRows} of #{maxAvailableRows}"
			@elStatusScrollTextCols.html "Cols #{@offsetShowingLeft+1}-#{@offsetShowingLeft+currentVisibleCols} of #{maxAvailableCols}"

		##|
		##|  Scrollbar settings show/hide
		r1 = @virtualScrollV.setRange 0, maxAvailableRows, currentVisibleRows, @offsetShowingTop
		r2 = @virtualScrollH.setRange 0, maxAvailableCols, lastVisibleCols, @offsetShowingLeft

		if r1.added or r2.added
			@refreshScrollbar()
			@resetCachedFromSize()
			@updateScrollbarSettings()
			return

		if r1.updated or r2.updated
			@refreshScrollbar()
			@updateScrollbarSettings()
			return

		@updateVisibleText()

	##|
	##|  Return true if a column is empty
	##|
	isColumnEmpty: (col) =>
		if @rowDataRaw.length == 0 then return false

		if col.getEditable() == true then return false

		if col.getIsCalculation() == true then return false

		if !@cachedColumnEmpty? then @cachedColumnEmpty = {}

		if @cachedColumnEmpty[col.getSource()]? then return @cachedColumnEmpty[col.getSource()]

		@cachedColumnEmpty[col.getSource()] = false
		source = col.getSource()

		for obj in @rowDataRaw
			##
			## If there are grouped columns, then force to keep all ungrouped columns
			if obj.cellType is "group"
				return false

			if !obj.id? then continue

			value = DataMap.getDataField @primaryTableName, obj.id, source

			if !value? then continue

			if typeof value == "string" and value.length > 0 then return false

			if typeof value == "number" and value != 0 then return false

			if typeof value == "boolean" then return false

			if typeof value == "object" then return false

		@cachedColumnEmpty[col.getSource()] = true
		true

	##|
	##|  Find the best fit for the data in a given column
	##|
	findBestFit: (col) =>
		if !@cachedBestFit? then @cachedBestFit = {}

		if @cachedBestFit[col.getSource()] then return @cachedBestFit[col.getSource()]

		max    = 10
		source = col.getSource()

		for obj in @rowDataRaw
			if !obj.id? then continue

			value = DataMap.getDataFieldFormatted @primaryTableName, obj.id, source

			if !value? then continue

			len   = value.toString().length

			if len > max then max = len

		if max < 10 then max = 10
		if max > 40 then max = 40

		@cachedBestFit[col.getSource()] = (max * 8)
		return (max * 8)

	setAutoFillWidth: (@autoFitWidth = true) =>
		delete @cachedLayoutShadowWidth
		true

	##|
	##|  For a standard table, adjust the width of the columns to fit the space available
	##|  and if it's not a close fit, then scroll the table instead
	##|
	layoutShadow: () =>
		maxWidth = @getTableVisibleWidth()

		unless @cachedLayoutShadowWidth? and @cachedLayoutShadowWidth == maxWidth
			@cachedLayoutShadowWidth = maxWidth

		autoAdjustableColumns = []

		for i in @colList
			if i.getAutoSize()
				i.actualWidth = @findBestFit(i)
				autoAdjustableColumns.push(i)

		if !@autoFitWidth? or @autoFitWidth == false then return false

		totalWidth = 0
		colNum = 0

		for i in @colList
			if i.isGrouped then continue

			location =
				colNum     : colNum
				visibleCol : colNum
				tableName  : @primaryTableName
				sourceName : i.getSource()

			w = @getColWidth(location)
			totalWidth += w
			colNum++

		diffAmount = (maxWidth - totalWidth) / autoAdjustableColumns.length
		diffAmount = Math.floor(diffAmount)

		for col in autoAdjustableColumns
			col.actualWidth += diffAmount

		true

	updateStatusText: (message...) =>
		if !@elStatusText? then return

		str = message.join ", "
		@elStatusText.html str
		true

	## -------------------------------------------------------------------------------------------------------------
	## function to render the added table inside the table holder element
	##
	## @example tableview.render()
	## @return [Boolean]
	##
	render: () =>
		if !@widgetBase?
			@renderRequired = true
		true

	real_render: () =>
		@renderRequired = false

		if !@shadowCells?
			@shadowCells = {}

		if !@fixedHeader
			@elTableHolder.width("100%")

		@elTableHolder.html("")
		@widgetBase    = new WidgetBase()
		tableWrapper   = @widgetBase.addDiv "table-wrapper", "tableWrapper#{@gid}"
		outerContainer = tableWrapper.addDiv "outer-container"
		@elTheTable    = outerContainer.addDiv "inner-container tableview"

		##
		## Scroll bars
		@virtualScrollV = new VirtualScrollArea outerContainer, true
		@virtualScrollH = new VirtualScrollArea outerContainer, false

		##
		## make room for title
		if @hasTitle()
			@elTitle = @elTheTable.addDiv "table-title"
			@elTitle.setAbsolute()
			@elTitle.height @titleHeight
			@elTitle.text @title

		##|
		##|  Make room for the status bar
		if @showStatusBar? and @showStatusBar == true
			@virtualScrollH.bottomPadding = @statusBarHeight
			@virtualScrollH.resize()
			@virtualScrollV.bottomPadding = @statusBarHeight
			@virtualScrollV.resize()
			@elStatusBar = tableWrapper.addDiv "statusbar"
			@elStatusText = @elStatusBar.addDiv "scrollStatusText"
			@elStatusText.html "Ready."
			@elStatusScrollTextRows = @elStatusBar.addDiv "scrollTextRows"
			@elStatusScrollTextRows.html ""
			@elStatusScrollTextCols = @elStatusBar.addDiv "scrollTextCols"
			@elStatusScrollTextCols.html ""
			@elStatusActionCopy = @elStatusBar.addDiv "statusActionsCopy"
			@elStatusActionCopy.html "<i class='fa fa-copy'></i> Copy"
			@elStatusActionCopy.on "click", @onActionCopyCell

			##
			## Set height of the status bar
			@elStatusBar.height(@statusBarHeight)

		##|
		##|  Get the data from that table
		if !@rowDataRaw? or @rowDataRaw.length == 0
			@updateRowData()

		@layoutShadow()
		@updateVisibleText()
		@elTableHolder.append @widgetBase.el
		@internalSetupMouseEvents()

		##|
		##|  This is a new render which means we need to re-establish any context menu
		@contextMenuCallSetup = 0

		##|
		##|  Setup context menu on the header
		@setupContextMenu @contextMenuCallbackFunction

		@tooltipWindow = new FloatingWindow(0, 0, 100, 100)

		##|  set title if elTheTable didn't exist at the time
		if @title? then @setTitle(@title)
		if @savedSearchesName? then @setSavedSearch(@savedSearchesName)
		true

	## -------------------------------------------------------------------------------------------------------------
	## function to sort the table base on column and type
	##
	## @param [String] name name of the column to apply sorting on
	## @param [String] type it can be ASC|DSC
	##
	sortByColumn: (name, type) =>
		if type is "ASC"
			sortType = 1
		else if type is "DSC"
			sortType = -1
		else
			sortType = 0

		##
		## Enable sorting on checkbox column
		if name is "row_selected"
			@addSortRule name, sortType
			return true

		for key, col of @colByNum

			if col.getSource() is name
				@addSortRule name, sortType
				return true

		false

	##|
	##|  Add a group by condition
	groupBy: (columnSource) =>

		if Array.isArray(columnSource) is true

			for source in columnSource
				@groupBy source

			return true

		for name in @currentGroups

			if name == columnSource then return

		@currentGroups.push columnSource
		@groupToHide = {}
		@updateRowData()
		@updateVisibleText()

	setEnableCheckboxes: (enabled, newLimit) =>
		if !enabled? then enabled = true

		if !newLimit? or typeof newLimit is not "number"
			newLimit = 1

		if @showCheckboxes is enabled and @checkboxLimit is newLimit then return false

		@showCheckboxes = enabled
		@checkboxLimit = newLimit
		@updateRowData()

	##
	## set if this table can have flags column
	setEnableFlags: (enabled, newLimit) =>
		if !enabled? then enabled = true

		if !newLimit? or typeof newLimit is not "number"
			newLimit = @flagLimit

		if @showFlags is enabled and @flagLimit is newLimit then return false

		@showFlags = enabled
		@flagLimit = newLimit
		@updateRowData()

	##
	## add a flag value object
	addFlag: (obj) =>

		if !obj? or typeof obj isnt "object" then return false

		if !obj.name or !obj.icon then return false

		@flagsColumn.push 
			name: obj.name
			icon: obj.icon
		true

	setEnableMathRow: (enabled) =>

		if !enabled? then enabled = true

		if @showMathRow is enabled then return false

		@showMathRow = enabled
		true

	addComputedRow: (mathTypes, @mathCols) =>
		if !mathTypes? or mathTypes is "" or mathTypes.length is 0 then return false

		@showMathRow = true

		if !@mathTypes? then @mathTypes = []

		if typeof mathTypes is "string"
			mathTypes = mathTypes.split(",").map (type) ->
				type?.trim()

		if Array.isArray(mathTypes)

			for type in mathTypes

				if @mathTypes.indexOf(type) < 0
					@mathTypes.push type

		@resetCachedFromSize()
		@updateScrollbarSettings()

	## -----------------------------------------------------------------------------------------------ƒmove--------------
	## intenal event key press in a filter field, that executes during the filter text box keypress event
	##
	## @event onFilterKeypress
	## @param [Event] e jquery event keypress object
	## @return [Boolean]
	##
	onFilterKeypress: (e) =>
		parts      = e.path.split '/'
		tableName  = parts[1]
		keyValue   = parts[2]
		columnName = parts.splice(3).join('/')

		if @getColumnType(columnName) > 2
			return false
		
		if !@currentFilters[tableName]?
			@currentFilters[tableName] = {}
		
		if !@currentFilters[tableName][columnName]?
			@currentFilters[tableName][columnName] = {}			

		@currentFilters[tableName][columnName]['value'] = $(e.target).val()

		if !@currentFilters[tableName][columnName]['comparatorFunc'] then return false

		@updateRowData()
		true

	## -------------------------------------------------------------------------------------------------------------
	## add a row that takes the full width using colspan
	##
	## @param [String] message the message that should be displayed in column
	##
	addMessageRow: (message) =>
		@rowDataRaw.push message
		return 0

	## -------------------------------------------------------------------------------------------------------------
	## clear the table using jquery .html ""
	##
	clear: () =>
		@elTableHolder.html ""

	## -------------------------------------------------------------------------------------------------------------
	## clear the html and also remove the associated column and rows reference
	##
	reset: () =>
		@elTableHolder.html ""
		@colList = []
		true

	## -------------------------------------------------------------------------------------------------------------
	##|
	##|  Called when an even has been added using the event manager
	onAddedEvent: (eventName, callback)=>
		m = eventName.match /click_(.*)/
		if m? and m[1]?
			##|
			##|  Added a click event to a column named m[1]
			##|
			col = @findColumn(m[1])

			if col?
				col.clickable = true
				@updateVisibleText()

		true

	moveCellRight: () =>
		if !@currentFocusRow? or !@currentFocusCol? then return false

		tempFocusRow = @currentFocusRow
		tempFocusCol = @currentFocusCol

		## if reached at the very right row, then do nothing
		## here, grouped columns have to be considered as getTableTotalCols() doesn't contain them
		if @offsetShowingLeft + @currentFocusCol >= @getTableTotalCols() + @getTableGroupedCols() - 1
			return false

		## if reached at the very right row of the current screen, scroll 1 col right
		visColCount = @getTableVisibleCols()

		if @currentFocusCol >= visColCount + @getTableGroupedCols() - 1 
			@scrollRight(1)

			unless @setFocusCell(@currentFocusRow, @currentFocusCol)
				@setFocusCell tempFocusRow, tempFocusCol - 1
			return true

		unless @setFocusCell(@currentFocusRow, @currentFocusCol + 1)
			@setFocusCell tempFocusRow, tempFocusCol

		true

	moveCellLeft: () =>
		if !@currentFocusRow? or !@currentFocusCol? then return false

		tempFocusRow = @currentFocusRow
		tempFocusCol = @currentFocusCol

		## if reached at the very left col, then do nothing
		if @offsetShowingLeft + @currentFocusCol <= 0
			return false

		## if reached at the very left col on the current screen, scroll 1 col left
		if @currentFocusCol <= 0
			@scrollRight(-1)
			@setFocusCell(@currentFocusRow, @currentFocusCol)
			return true

		unless @setFocusCell(@currentFocusRow, @currentFocusCol - 1)
			@setFocusCell tempFocusRow, tempFocusCol

		true

	moveCellUp: () =>
		if !@currentFocusRow? or !@currentFocusCol? then return false

		tempFocusRow = @currentFocusRow
		tempFocusCol = @currentFocusCol

		## calculate row count in data area(excluding header and filter rows)
		rowNum = @currentFocusRow - @getTableLockedRows()
		if rowNum < 0 then rowNum = 0

		## if reached at the very first of possible rows, then do nothing
		if rowNum + @offsetShowingTop <= 0
			return false

		## if the very first row on the current screen, then scroll 1 row up
		if rowNum <= 0
			@scrollUp(-1)
			@setFocusCell(@currentFocusRow, @currentFocusCol)
			return true			

		if not @setFocusCell(@currentFocusRow - 1, @currentFocusCol)
			@setFocusCell(tempFocusRow, tempFocusCol)

		true

	moveCellDown: () =>
		if !@currentFocusRow? or !@currentFocusCol? then return false

		tempFocusRow = @currentFocusRow
		tempFocusCol = @currentFocusCol
		visRow       = @getTableVisibleRows()

		## calculate row number in data area(excluding header and filter rows)
		rowNum = @currentFocusRow - @getTableLockedRows()

		if rowNum < 0 then rowNum = 0

		## if reached at the very last row, then do nothing
		if rowNum + @offsetShowingTop >= @getTableTotalRows() - 1
			return false

		## if the very last row on the current screen, then scroll 1 row down
		if rowNum >= @getTableVisibleRows() - 1
			@scrollUp(1)
			@setFocusCell(@currentFocusRow, @currentFocusCol)
			return true

		unless @setFocusCell(@currentFocusRow + 1, @currentFocusCol)
			@setFocusCell(tempFocusRow, tempFocusCol)

		true

	##|
	##|  Auto select the first visible cell
	setFocusFirstCell: () =>
		@setFocusCell(0,0)
		true

	##|
	##|  Copy to clipboard the current cell
	onActionCopyCell: () =>
		if !@currentFocusCell? then return

		path = @currentFocusCell.getDataValue("path")

		if path?
			parts     = path.split("/")
			tableName = parts[1]
			record_id = parts[2]
			source    = parts.splice(3).join('/')
			item      = @findRowFromPath(path)

		true

	##|
	##|  Focus on a path cell
	setFocusCell: (visibleRow, visColNum, e) =>
		if !@allowSelectCell
			return false

		##|
		##|  Remove old focus
		if @currentFocusCell? and @currentFocusCell.removeClass?
			@currentFocusCell.removeClass "cellfocus"

		@currentFocusCell = null
		@currentFocusCol  = null
		@currentFocusRow  = null
		@currentFocusPath = null

		if visibleRow == null or visColNum == null
			@updateStatusText "Nothing selected"
			return false

		element = null

		for tag_id, tag_data of globalTagData
			parts = tag_data.path?.split? '/'

			if parts? and parts.length > 3
				tempTableName = parts[1]
				tempKeyValue  = parts[2]
				tempSource    = parts.splice(3).join('/')

			if tempTableName == @primaryTableName and tag_data.vr == visibleRow and tag_data.vc == visColNum
				element = @elTableHolder.find("[data-id='#{tag_id}']")
				rowNum  = tag_data.rn
				colNum  = tag_data.cn
				path    = tag_data.path
				break

		if !element?
			console.log "Unable to find element for #{visibleRow}/#{visColNum}"
			return false

		if path?
			@currentFocusCell = path
			parts = path.split? "/"

			if parts?
				tableName = parts[1]
				record_id = parts[2]
				source    = parts.splice(3).join('/')

		cellType = @getCellType { visibleRow: visibleRow, visibleCol: visColNum, rowNum: rowNum, colNum: colNum }

		if !source?
			source = @getCellSource { visibleRow: visibleRow, visibleCol: visColNum, rowNum: rowNum, colNum: colNum }

		if !visibleRow? or !visColNum? or cellType != "data"
			@updateStatusText "Nothing selected"
			return false

		if visibleRow == null or visColNum == null
			@currentFocusRow = null
			@currentFocusCol = null
			@updateStatusText "Nothing selected"
			return false

		visibleRow       = parseInt visibleRow
		visColNum        = parseInt visColNum
		@currentFocusRow = visibleRow
		@currentFocusCol = visColNum

		# if @showCheckboxes? and @showCheckboxes is true
		# 	visColNum++

		# if @showFlags? and @showFlags is true
		# 	visColNum++

		@currentFocusCell = @shadowCells[visibleRow].children[visColNum]

		if @currentFocusCell?
			path = @currentFocusCell.getDataValue("path")
			if path?
				@currentFocusPath = path
				@currentFocusCell.addClass "cellfocus"
				item = @findRowFromPath(path)
				@emitEvent 'focus_cell', [ path, item ]
				@updateStatusText item[source]

		true

	##
	## set focus to cell by field value
	setFocusByValue: (sourceName, value) =>
		## get id of the row including the value
		rowID    = null
		rowIndex = null

		for row, index in @rowDataRaw
			fieldValue = DataMap.getDataFieldFormatted @primaryTableName, row.id, sourceName

			if fieldValue is value 
				rowID = row.id 
				rowIndex = index
				break

		## if there's no matching field
		if rowID is null then return false

		## try to scroll, so that targeted row gets visible
		visibleRows = @getTableVisibleRows()

		if @offsetShowingTop > rowIndex 
			@scrollUp(rowIndex - @offsetShowingTop) 
		else if rowIndex > @offsetShowingTop + visibleRows
			@scrollUp(rowIndex - (@offsetShowingTop + visibleRows) + 1)

		## if the targeted row is visible now, focus on it
		for tag_id, tag_data of globalTagData

			if !tag_data.path? then continue 

			paths      = tag_data.path.split '/'
			tableName  = paths[1]
			keyValue   = paths[2]
			columnName = paths.splice(3).join('/')

			if keyValue is rowID and columnName is sourceName 
				@setFocusCell tag_data.vr, tag_data.vc 
				return true

		false

	##|
	##|  Returns true if a path is visible
	findPathVisible: (path) =>

		for idx, shadow of @shadowCells

			for cell in shadow.children

				if cell.getDataValue("path") == path
					return cell

		return null

	show: () =>
		return

	hide: () =>
		return

	destroy: () =>
		try
			## 
			## Datach all global events from globalEventObjects
			@detachAllGlobalEvents "Table"
			@detachAllGlobalEvents "Keyboard"	
			##
			## Remove eventManager to unhook all self-attached events
			delete @eventManager	
		catch e
			console.log "Exception in destroying TableView: ", e, this
			return false

		true

	##
	## Hook global events 
	## @param objId[String]: id of global events object, e.g. "Table" for globalTableEvents
	## @param eventName[String]: name of an event to get hooked
	## @param handler[function]: event handler
	attachGlobalEvent: (objId, eventName, handler) =>
		if !@globalEvents? then @globalEvents = {}

		try

			switch objId

				when "Table"
					globalTableEvents.on eventName, handler
					if !@globalEvents["Table"]?
						@globalEvents["Table"] = []
					@globalEvents["Table"].push 
						eventname: eventName
						handler: handler
					break

				when "Keyboard"
					globalKeyboardEvents.on eventName, handler
					if !@globalEvents["Keyboard"]?
						@globalEvents["Keyboard"] = []
					@globalEvents["Keyboard"].push 
						eventname: eventName
						handler: handler            
					break

				else
					break

		catch e

			console.log "Exception in attachGlobalEvent: ", e, objId, eventName, handler
			return false
		true

	##
	## Unhook an event from global object
	detachGlobalEvent: (objId, eventName, handler) =>

		try

			switch objId

				when "Table"
					globalTableEvents.off eventName, handler
					position = -1
					for ev, index in @globalEvents["Table"]
						if ev.eventname is eventName then position = index 
					if position >= 0
						@globalEvents["Table"].splice position, 1
					break

				when "Keyboard"
					globalKeyboardEvents.off eventName, handler
					position = -1

					for ev, index in @globalEvents["Keyboard"]
						if ev.eventname is eventName then position = index 

					if position >= 0
						@globalEvents["Keyboard"].splice position, 1
					break                    

		catch e
			console.log "Exception in detachGlobalEvent: ", e, objId, eventName, handler
			return false
		true
		
	##
	## Unhook all global events that are already attached to this 
	detachAllGlobalEvents: (objId) =>
		if !@globalEvents then return true

		try

			switch objId

				when "Table"

					if @globalEvents["Table"]?

						for ev, index in @globalEvents["Table"]
							globalTableEvents.off ev.eventname, ev.handler
						@globalEvents["Table"] = []

				when "Keyboard"

					if @globalEvents["Keyboard"]?

						for ev, index in @globalEvents["Keyboard"]
							globalTableEvents.off ev.eventname, ev.handler

						@globalEvents["Keyboard"] = []

		catch e
			console.log "Exception in detachAllGlobalEvents: "
			console.log "Error: ", e 
			console.log "GlobalEventObjectName: ", objId
			return false

		true

	## -------------------------------------------------------------------------------------------------------------
	## internal function to find the col name from the event object
	##
	## @param [Event] e the jquery Event object
	## @param [Integer] stackCount number of checking round
	##
	findColFromPath: (path) =>
		if !path? then return null

		parts     = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		colName   = parts.splice(3).join('/') 

		for part, i in parts when i >= 4 
			colName = colName + '/' + part

		colName

	## -------------------------------------------------------------------------------------------------------------
	## internal function to find the row element from the event object
	##
	## @param [Event] e the jquery Event object
	## @param [Integer] stackCount number of checking round
	##
	findRowFromPath: (path) =>
		if !path? then return null

		parts     = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		colName   = parts.splice(3).join('/')

		if keyValue == "Filter"
			return "Filter"

		if keyValue == "Header"
			return "Header"

		data   = {}
		colNum = 0
		data   = DataMap.getDataForKey @primaryTableName, keyValue

		if !data? then return null

		data["id"] = keyValue
		data

	## -------------------------------------------------------------------------------------------------------------
	## check if a column is data column or action column
	##
	## @param [String] colName name of column to be checked
	## @return [Integer] 1 : Data Column, 2 : Action Column, 0 : not both
	##
	getColumnType: (colName) =>

		for actionCol in @actionColList

			if actionCol.getSource() is colName

				## differentiate columns that were moved from data columns
				if actionCol.constructor.name is "Column"
					return 2
				else 
					return 3

		for titleCol in @titleColList

			if titleCol.getSource() is colName

				if titleCol.constructor.name is "Column"
					return 1

				else 
					return 2

		for index, dataCol of @colByNum

			if dataCol.getSource() is colName
				return 1

		return 0

	##
	## set column's isGrouped property as false
	##
	## @param [string] colName name of column
	## @return [Boolean] true if action is succeeded, else, false
	##
	ungroupColumn: (colName) =>
		columns = DataMap.getColumnsFromTable(@primaryTableName, @columnReduceFunction)

		for col in columns

			if col.getSource() is colName

				col.isGrouped = false
				return true

		false

	##|
	##| set the "save" response for this table to use the EdgeApi's doUpdatePathHigh call
	##| in order to automatically save any values that are editable.
	##|
	setDefaultSaveCallback: (dataSet, tableName, callbackEditValue) =>
		DataMap.setSaveCallback @primaryTableName, (id, field, oldValue, newValue) =>

			if callbackEditValue?
				val = callbackEditValue(id, field, oldValue, newValue)

				if !val? then return null

				newValue = val

			newData = {}
			newData[field] = newValue
			sock.data_doUpdatePathHigh dataSet, "/#{tableName}/#{id}", newData
			true

	##
	## function to set title of the table
	##
	setTitle: (@title) =>
		##| brian- no table yet, maybe not visible.   Ignore this request, we'll set it later.
		if !@elTheTable?
			return

		if !@elTitle?
			@elTitle = @elTheTable.addAtPosition "div", "table-title", 0
			@elTitle.height @titleHeight
			@elTitle.move 0, 0, @getTableTotalWidth, @titleHeight

		@elTitle.text @title

		##
		## NoDataCell must be relocated
		if @noDataCell?
			marginRight = if @virtualScrollV.visible then @virtualScrollV.displaySize else 0
			marginTop = @headerCellHeight + @getRowHeight()

			if @hasTitle() then marginTop += @titleHeight

			@noDataCell.move(0, marginTop, @elTableHolder.width() - marginRight, @elTableHolder.height() - marginTop)

		@updateRowData()

	##
	## function to check if this table has title
	##
	hasTitle: () =>
		if @title then return true
		false

	## -------------------------------------------------------------------------------------------------------------
	## get column by its source name
	##
	## @param [String] colName name of column to be checked
	## @return [Column] column which has the input name
	##
	getColumnByName: (colName) =>

		for index, dataCol of @colByNum

			if dataCol.getSource() is colName
				return dataCol

		for actionCol in @actionColList

			if actionCol.getSource() is colName
				return actionCol

		for titleCol in @titleColList

			if titleCol.getSource() is colName
				return titleCol

		null

	##
	## function to handle 'focus_cell' event
	onFocusCell: (path, row) =>
		if !path? then return null

		parts     = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		colName   = parts.splice(3).join('/')		

		col = @getColumnByName colName

		## if this column is linked to other table
		## display data of the linked table's row
		if col.getLinked()

			if !col.getEditable() 
				linkedKey = row[colName]
				linkedRow = DataMap.getDataForKey col.getLinked(), linkedKey
				doPopupTableView linkedRow, "#{col.getLinked()}(#{linkedKey})", "settings_#{col.getLinked()}(#{linkedKey})", 600, 400
				.then (view) ->
					true

	##
	## function to get count of Locked(header, filter) rows
	getTableLockedRows: () =>
		cnt = 0
		if @showHeaders then cnt++
		if @showFilters then cnt++
		cnt
	
	##
	## function to get count of Locked(action) columns
	getTableLockedColsRight: () =>
		cnt = 0
		if @actionColList? then cnt = @actionColList.length
		cnt

	getTableLockedColsLeft: () =>
		cnt = 0
		if @titleColList? then cnt = @titleColList.length
		return cnt

	##
	## function to get count of grouped columns
	getTableGroupedCols: () =>
		cnt = 0
		if @currentGroups? then cnt = @currentGroups.length
		cnt

	getTitleColCount: () =>
		cnt = @titleColList.length
		if @showCheckboxes then cnt--
		if @showFlags then cnt--
		cnt

	setLoadingMessage: (msgText) =>
		@loadingMsgTxt      = msgText
		@showLoadingMessage = true
		true

	unsetLoadingMessage: () =>
		@showLoadingMessage = false
		@updateVisibleText()
		true

	setNoDataMessage: (content) =>
		if !content? or @noDataMsg is content then return false

		@noDataMsg = content

		if @rowDataRaw.length is 0
			@updateVisibleText()
		true

	updateNoDataArea: () =>
		if !@noDataCell? then return false

		if @showLoadingMessage
			@noDataCell.html "<i class='fa fa-spinner fa-spin'></i>&nbsp #{@loadingMsgTxt} "
		else
			@noDataCell.html @noDataMsg		
		true

	##
	## function to process all rows
	## including those that don't show by filter
	## callback(function) should return an updated row object
	forAll: (callback) =>
		promises = []
		rowData  = DataMap.getValuesFromTable @primaryTableName

		for row in rowData

			result = callback(row)

			if result? and result.then? and typeof result.then is "function"
				promises.push result

			if !result or !result.id? then continue
			DataMap.addData @primaryTableName, result.id, result

		if promises.length > 0

			Promise.all(promises)
			.then (results) =>

				for result in results

					if !result? or !result.id? then continue

					DataMap.addData @primaryTableName, result.id, result
		true

	##
	## function to process fitlered/filtered out rows
	## callback(function) should return an updated row object
	forAllFiltered: (isFiltered = true, callback) =>
		promises = []
		@applyFilters()
		rowData = DataMap.getValuesFromTable @primaryTableName

		for row in rowData

			if isFiltered and @reduceFunction(row)
				result = callback(row)

			if !isFiltered and !@reduceFunction(row)
				result = callback(row)

			else
				continue

			if result? and result.then? and typeof result.then is "function"
				promises.push result

			if !result or !result.id? then continue

			DataMap.addData @primaryTableName, result.id, result

		if promises.length > 0

			Promise.all(promises)
			.then (results) =>

				for result in results

					if !result? or !result.id? then continue

					DataMap.addData @primaryTableName, result.id, result

		true

	##
	## function to process all checked/unchecked rows
	forAllChecked: (isChecked = true, callback) =>
		promises = []
		rowData = DataMap.getValuesFromTable @primaryTableName

		for row in rowData

			if isChecked and row.row_selected is isChecked
				result = callback(row)

			else if !isChecked and (row.row_selected is false or row.row_selected is undefined)
				result = callback(row)

			else
				continue

			if result? and result.then? and typeof result.then is "function"
				promises.push result

			if !result? or !result.id? then continue

			DataMap.addData @primaryTableName, result.id, result

		if promises.length > 0

			Promise.all(promises)
			.then (results) =>

				for result in results

					if !result or !result.id? then continue
					DataMap.addData @primaryTableName, result.id, result

		true

	##
	## function to process all visible rows
	forAllVisible: (isVisible = true, callback) =>
		promises = []
		allRows  = DataMap.getValuesFromTable @primaryTableName

		for row in allRows

			if isVisible is true and @isRowVisible(row)
				result = callback(row)
			else if !isVisible and !@isRowVisible(row)
				result = callback(row)
			else
				continue

			if result? and result.then? and typeof result.then is "function"
				promises.push result

			if !result? or !result.id? then continue

			DataMap.addData @primaryTableName, result.id, result

		if promises.length > 0
			Promise.all(promises)
			.then (results) =>

				for result in results

					if !result or !result.id? then continue

					DataMap.addData @primaryTableName, result.id, result

		true

	##
	## Toggle a group visible or invisible 
	## `groupName` can be presented by either of 2 ways:
	## - Object: { columnName1: value1, columnName2: value2, ...}
	## - String: `columnName1: value1, columnName2: value2...`
	##
	toggleGroupVisible: (groupName, mode = null) =>
		if !groupName? then return false

		if typeof groupName is "object"
			str = ""

			for key, val of groupName
				displayName = key

				for col in @colList
					if col.getSource() == key then displayName = col.getName()
					if col.getName() == key then key = col.getSource()
				##
				## if this column isn't in currentGroups
				## just ignore the one
				if @currentGroups.indexOf(key) < 0 then continue

				if str != "" then str += ", "

				str += displayName + ": " + val

			groupName = str

		if mode? and typeof mode is "boolean"
			@groupToHide[groupName] = 
				visible: mode

		else
			if @groupToHide[groupName]?
				@groupToHide[groupName].visible = !@groupToHide[groupName].visible
			else
				@groupToHide[groupName] = 
					visible: false

		@updateRowData()
		true

	##
	## Check if a row is visible by filters
	isRowVisible: (row) =>
		rowId = null

		if typeof row isnt "object"
			rowId = row
		else
			rowId = row.id

		if !rowId then return false

		for obj in @rowDataRaw
			if obj.id is rowId then return true

		false	

	##
	## calculate actual width of table
	getTableActualWidth: () =>
		result = 0

		for index, col of @colByNum
			## check if this column is grouped
			if @currentGroups.indexOf(col.getSource()) > 0 then continue

			if (!col.currentWidth or isNaN(col.currentWidth)) and col.getWidth() then col.currentWidth = col.getWidth()

			result += (col.actualWidth || col.currentWidth || 0)

		## 
		## add group padding if table has grouped column(s)
		if @showGroupPadding is true
			result += @groupPaddingWidth

		result

	##
	## Calculate top margin of table content(aka. table data cells)
	getContentTopMargin: () =>
		marginTop = @headerCellHeight + @getRowHeight()

		if @hasTitle() then marginTop += @titleHeight

		marginTop

	##
	## Calculate right margin of table content(aka. table data cells)
	getContentRightMargin: () =>
		marginRight = if @virtualScrollV?.visible then @virtualScrollV.displaySize else 0
		marginRight

	##
	## set class to rows by their content
	## @param: callback: function that returns true or false based on passed row data
	##         if it returns true, then add the class to the row, else remove the class
	setRowClass: (className, callback) =>
		if !className? or typeof className isnt "string" then return false
		if !callback or typeof callback isnt "function" then return false 

		@rowClassSetters[className] = callback 
		@updateVisibleText()
		true

	##
	## Enable saving filters on this table to localStorage for future use
	setSavedSearch: (name) =>
		if !@hasTitle() then return false 

		@savedSearchesName = name 
		
		if @elTitle?
			@elSavedSearch         = @elTitle.add "span", "ninja-table-saved-search"
			@elSavedSearchText     = @elSavedSearch.add "span", "ninja-table-saved-search-text"
			@elSavedSearchText.html  "Saved Searches "
			
			@elSavedSearchDropDown = @elSavedSearchText.add "span", "ninja-table-saved-search-dropdown"
			
			@elSavedSearchDropDown.html "<i class='fa fa-fw fa-caret-down'></i>  "
			@updateDropDownSavedSearchIconStatus()
			
			@elSavedSearchSave     = @elSavedSearch.add "span", "ninja-table-saved-search-save"
			
			@elSavedSearchSave.html "<i class='far fa-fw fa-save'></i>"
			
			@elSavedSearchSave.el.on "click", @onClickSaveSearches
			@elSavedSearchSave.el.on "click", @onClickRemoveSearch
			
			@enabledSaveSearch     = false
			@enabledRemoveSearch   = false
			@updateSaveSearchIconStatus()
		true

	##
	## Update SaveSearches icon's status
	updateSaveSearchIconStatus: (searchName) =>
		if !@elSavedSearchSave? then return false

		if !searchName 
			@elSavedSearchSave.html "<i class='far fa-fw fa-save'></i>"
			@enabledRemoveSearch = false

			if @getValidFiltersCount() > 0
				@elSavedSearchSave.setClass "disabled", false
				@enabledSaveSearch = true				
				return true

			else
				@elSavedSearchSave.setClass "disabled", true
				@enabledSaveSearch = false
				return false

		else
			@elSavedSearchSave.html "<i class='far fa-fw fa-trash-alt'></i>"
			@elSavedSearchSave.setClass "disabled", false
			@enabledSaveSearch = false
			@enabledRemoveSearch = true

			return true

	##
	## Update Dropdown-SavedSearches icon's status
	updateDropDownSavedSearchIconStatus: () =>
		if !@elSavedSearchDropDown? then return false

		if @getNamesofSavedSearches().length > 0
			@elSavedSearchText.setClass "disabled", false
			@elSavedSearchText.el.on "click", @onClickDropDownSavedSearches
			return true

		else
			@elSavedSearchText.setClass "disabled", true
			@elSavedSearchText.el.off "click", @onClickDropDownSavedSearches
			return false			

	##
	## "click" event handler for SaveSearches icon
	onClickSaveSearches: (e) =>
		if !@enabledSaveSearch then return false

		options =
			title: "Save Searches..."
			content: "Please enter name of the Searches."
			inputs: [
				type: "text"
				label: "Searches Name"
				name: "input1"
				table: this
			]
			buttons: [
				type: "submit"
				text: "Save"
			]
			onSubmit: (form)=>
				searchName = form.input1.value
				table = form.input1.table
				table.saveSearches searchName
				return true

		m = new ModalForm(options)			
		true

	##
	## "click" event listener for RemoveSearch icon
	onClickRemoveSearch: (e) =>
		if !@enabledRemoveSearch then return false
		if !@currentAppliedSearch then return false

		options =
			title: "Delete Search..."
			content: "Are you sure to remove this search - #{@currentAppliedSearch} ?"
			buttons: [
				type: "submit"
				text: "Confirm"
				table: this
			]
			onSubmit: (form)=>
				table = form.submitButton.table
				table.removeSearch @currentAppliedSearch, @savedSearchesName
				return true

		m = new ModalForm(options)
		true

	##
	## "click" event listener for Dropdown to show saved searches
	onClickDropDownSavedSearches: (e) =>
		popupMenu = new PopupMenu "Searches", e

		for name in @getNamesofSavedSearches()
			popupMenu.addItem name, (e, data) =>
				data.table.applySavedSearches data.searchname, @savedSearchesName
			, { searchname: name, table: this }
		true

	##
	## calculate count of valid filters
	getValidFiltersCount: () =>
		result = 0

		for key, filter of @currentFilters[@primaryTableName]
			if !filter['comparator']? or filter['comparator'] is "" or filter['comparator'] is "none"
				continue

			if !filter['value']? or filter['value'] is "" or filter['value'] is "none"
				continue

			result++
		result

	##
	## Save current filters to localStorage
	saveCurrentFilters: (saveName = @primaryTableName, objName = "table_filters_latest") =>
		currentFilters = []

		for key, filter of @currentFilters[@primaryTableName]
			##
			## collect current filters
			currentFilters.push 
				column    : key
				comparator: filter['comparator']
				value     : filter['value']

		savedSearches = localStorage.getItem objName

		if !savedSearches 
			savedSearches = []
		else 
			savedSearches = JSON.parse savedSearches

		##
		## remove if the same value is existing
		position = -1

		for search, index in savedSearches
			if search['name'] is saveName 
				position = index
				break
		
		if position >= 0	
			savedSearches.splice position, 1

		savedSearches.push
			name   : saveName
			filters: currentFilters

		##
		## store these searches to localStorage
		localStorage.setItem objName, JSON.stringify(savedSearches)
		savedSearches

	##
	## save current filters to localStorage and update UI dropdown
	saveSearches: (saveName) =>
		if !saveName? or saveName is "" then return false 

		if !@savedSearchesName then return false

		searches = @saveCurrentFilters saveName, @savedSearchesName
		@savedSearches = []

		searches.forEach (search) => 
			@savedSearches.push search

		@currentAppliedSearch = saveName
		@updateDropDownSavedSearchIconStatus()
		@updateSaveSearchIconStatus(saveName)
		true

	##
	## remove saved filters from localStorage
	removeSearch: (name = @primaryTableName, objName = "table_filters_latest") =>
		if !name then return false

		savedSearches = localStorage.getItem objName 

		if !savedSearches 
			savedSearches = []
		else 
			savedSearches = JSON.parse savedSearches

		##
		## remove if the same value is existing
		position = -1

		for search, index in savedSearches

			if search['name'] is name
				position = index 
				break

		##
		## store these searches to localStorage
		if position >= 0
			savedSearches.splice position, 1
			localStorage.setItem @savedSearchesName, JSON.stringify(savedSearches)

		@updateSaveSearchIconStatus()
		@updateDropDownSavedSearchIconStatus()
		@resetCurrentFilters()
		true

	##
	## Apply filters stored in localStorage
	applySavedSearches: (name = @primaryTableName, objName = "table_filters_latest") =>
		if !name then return false

		savedSearches = localStorage.getItem objName

		if !savedSearches 
			savedSearches = []
		else 
			savedSearches = JSON.parse savedSearches

		##
		## find and apply filter group by name
		for search in savedSearches

			if search['name'] is name 
				filters = search['filters']

				for filter in filters 
					@updateFilter @primaryTableName, filter['column'], "comparator", filter['comparator']
					@updateFilter @primaryTableName, filter['column'], "value", filter['value']
				##
				## change SaveSearch icon to RemoveSearch icon
				@updateSaveSearchIconStatus(name)
				@currentAppliedSearch = name
				return true		
		false

	##
	## get names of searches that are stored in localStorage
	getNamesofSavedSearches: () =>
		result = []

		if !@savedSearchesName then return result

		savedSearches = localStorage.getItem @savedSearchesName 

		if !savedSearches 
			savedSearches = []
		else 
			savedSearches = JSON.parse savedSearches

		for search in savedSearches
			result.push search['name']
		
		result

	##
	## reset current filters on this table
	resetCurrentFilters: () =>
		for key, filter of @currentFilters[@primaryTableName]

			if key is "row_selected" or key is "row_flagged"
				@updateFilter @primaryTableName, key, "comparator", "checkbox_all"
			else
				@updateFilter @primaryTableName, key, "comparator", "=~"

			@updateFilter @primaryTableName, key, "value", null
		@currentAppliedSearch = null
		true

	##
	## reset table data
	resetTable: () =>
		@colList           = []
		@actionColList     = []
		@titleColList      = []

		if @colByNum?
			for key of @colByNum
				delete @colByNum[key]

		# @property [Array] list of rows as array
		@rowDataRaw      = []

		if @rowDataSelected?
			for key of @rowDataSelected
				delete @rowDataSelected[key]
		
		if @rowDataFlagged?
			for key of @rowDataFlagged
				delete @rowDataFlagged[key]

		@currentGroups     = []
		@sortRules         = []

		##
		## reset object: cachedColumnEmpty so that empty cols won't be visible
		if @cachedColumnEmpty?
			for key of @cachedColumnEmpty
				delete @cachedColumnEmpty[key]

		@resetCurrentFilters()
		true

	##
	## Get all existing values in the column
	getValuesInColumn: (tableName, colSource) =>
		if @cachedColumnValues? and @cachedColumnValues[colSource]? then return @cachedColumnValues[colSource]
		allRows = DataMap.getValuesFromTable tableName

		values = allRows.reduce (acc, row) ->
			value = row[colSource]
			if value?
				if !acc[value]?
					acc[value] = 1
				else
					acc[value]++
			return acc 
		, {}

		##
		## Cache this result to variable
		if !@cachedColumnValues? then @cachedColumnValues = {}

		@cachedColumnValues[colSource] = values
		values

	getColumn: (location) =>
		colSource = location.sourceName
		col = null
		found = false

		for tcol in @titleColList
			if tcol.getSource() is colSource
				col = tcol
				found = true
				break

		for acol in @actionColList
			if acol.getSource() is colSource
				col = acol
				found = true
				break

		if !found
			col = @colByNum[location.colNum]
		col

	calculateMathField: (location) =>
		col = @getColumn location

		if !col? or !col.getSource? then return false

		tableName = location.tableName
		colSource = col.getSource()

		if !@showMathRow or @mathCols.indexOf(colSource) < 0 then return null
		if @cachedMathValues? and @cachedMathValues[colSource]? then return @cachedMathValues[colSource]

		allRows = DataMap.getValuesFromTable tableName, @reduceFunction

		getValue = (rawValue, dataFormatter) -> 
			if !dataFormatter? then return rawValue
			unformattedVal = dataFormatter.unformat rawValue
			if unformattedVal.getTime? and typeof unformattedVal.getTime is "function"
				return unformattedVal.getTime()
			unformattedVal

		getFormattedValue = (value, dataFormatter) ->
			if !dataFormatter? or !dataFormatter.format? then return value
			dataFormatter.format value

		doSum = (rows, colSource) =>
			if !rows? or rows.length is 0 then return 0
			result = 0
			result = rows.reduce (acc, row) ->
				value = getValue row[colSource], col?.getFormatter()
				acc += value
				return acc
			, 0
			result

		doAverage = (rows, colSource) => 
			if !rows? or rows.length is 0 then return 0
			result = doSum(rows,colSource) / rows.length
			result

		getMin = (rows, colSource) =>
			if !rows? or rows.length is 0 then return [0, 0]
			values = rows.map (row) ->
				value = getValue row[colSource], col?.getFormatter()
				if value.getTime? and typeof value.getTime is "function"
					return value.getTime()
				value
			Math.min(values...)

		getMax = (rows, colSource) =>
			if !rows? or rows.length is 0 then return [0, 0]
			values = rows.map (row) ->
				value = getValue row[colSource], col?.getFormatter()
				if value.getTime? and typeof value.getTime is "function"
					return value.getTime()
				value
			Math.max(values...)

		doRange = (rows, colSource) =>
			[ getMin(rows, colSource), getMax(rows, colSource) ]

		mathType = @mathTypes[location.mathRowNum or 0]

		if mathType is "average"
			return getFormattedValue(doAverage(allRows, colSource), col?.getFormatter())
		else if mathType is "sum"
			return getFormattedValue(doSum(allRows, colSource), col?.getFormatter())
		else if mathType is "min"
			return getFormattedValue(getMin(allRows, colSource), col?.getFormatter())
		else if mathType is "max"
			return getFormattedValue(getMax(allRows, colSource), col?.getFormatter())

		null

	##
	## Clear cached table data from DataMap
	clearCachedTableData: () =>
		##
		## reset object: cachedColumnEmpty, so that empty cols won't be visible
		if @cachedColumnEmpty?
			for key of @cachedColumnEmpty
				delete @cachedColumnEmpty[key]

		##
		## reset object: cachedColumnValues, so that count of unique values in columns can be reset
		if @cachedColumnValues?
			for key of @cachedColumnValues
				delete @cachedColumnValues[key]

		##
		## Clear cached auto width values for columns
		if @cachedBestFit?
			for colName, val of @cachedBestFit
				delete @cachedBestFit[colName] 
		true

	## 
	## Render contents of special columns: Checkbox, Flag
	renderSpecialColumn: (location, col) =>
		if col.getSource() == "row_selected"

			if @getRowLocked(location.recordId)
				location.cell.html "<i class='fa fa-lock'></i>"

			else if @getRowSelected(location.recordId)
				location.cell.html @imgChecked

			else
				location.cell.html @imgNotChecked

		else if col.getSource() == "row_flagged"

			flagName = @getRowFlag(location.recordId)
			icon     = @flagsColumn[@getFlagIndex(flagName)].icon
			location.cell.html "<i class='#{icon}'></i>"		

		true

	##
	## Highlight selected rows
	highlightRows: (className = "cell_highlighted", selector) =>
		@setRowClass className, selector
		true

	editRow: (rowId) =>
		columns = DataMap.getColumnsFromTable @primaryTableName
		formInputs = columns
			.filter((col) -> col.getVisibleOnEdit())
			.map((item) ->
				type:  item.getType()
				label: item.getName()
				name:  item.getSource()
			)

		options =
			title:   "Edit #{@primaryTableName}/#{rowId}"
			content: ""
			table:   @primaryTableName
			idvalue: rowId
			inputs:  formInputs
			buttons: [
				type: "submit"
				text: "Save"
			]
			onSubmit: (form) ->
				console.log "Table row edit form submitted: ", form
				true

		m = new ModalForm(options)

	serialize: () =>
		## filters
		filters = {}

		for colName, obj of @currentFilters?[@primaryTableName]
			if obj? and obj.value?
				newObj = Object.assign {}, obj
				delete newObj.comparatorFunc
				filters[colName] = newObj

		## css classes that applies to rows
		rowcsses = []

		for name, callback of @rowClassSetters
			rowcsses.push
				classname: name
				callback : callback

		## Sorting rules
		sortRule = @sortRules?[0]
		sortDir  = null

		if sortRule?
			if sortRule.state is 1
				sortDir = "asc"
			else if sortRule.state is -1
				sortDir = "desc"
			sort = if sortDir? then "#{sortRule.source} #{sortDir}" else sortRule.source

		## action columns
		actioncolumns    = []
		newactioncolumns = []

		for acol in @actionColList
			if acol.constructor.name is "TableViewColButton"
				newactioncolumns.push
					name:      acol.getName()
					source:    acol.getSource()
					width:     acol.getWidth()
					tableName: @primaryTableName
					render:    acol.render
			else
				actioncolumns.push acol.getSource()

		## title columns
		titlelocks    = []
		newtitlelocks = []

		for tcol in @titleColList
			if tcol.constructor.name is "TableViewColButton"
				newtitlelocks.push
					name:      tcol.getName()
					source:    tcol.getSource()
					width:     tcol.getWidth()
					tableName: @primaryTableName
					render:    tcol.render
			else
				source = tcol.getSource()
				if source isnt "row_selected" and source isnt "row_flagged"
					titlelocks.push tcol.getSource()

		return
			showfilter:       @showFilters
			colfilter:        @columnReduceFunction
			rowfilter:        @overallReduceFunction
			tabletitle:       @title
			savedsearch:      @savedSearchesName
			autofill:         @autoFitWidth
			enablecheckboxes: @showCheckboxes
			checkboxlimit:    @checkboxLimit
			rowcsses:         rowcsses
			hascustomtooltip: @hasCustomTooltip
			tooltiprenderer:  @tooltipRenderFunc
			sort:             sort
			groupby:          @currentGroups
			filters:          filters
			actioncolumns:    actioncolumns
			newactioncolumns: newactioncolumns
			titlelocks:       titlelocks
			newtitlelocks:    newtitlelocks
			flaglimit:        @flagLimit
			flags: if @showFlags then @flagsColumn else null
