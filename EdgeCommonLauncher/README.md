# EdgeCommonLauncher
> Launch an application, capture the output and handle all module installs and 
> other factors needed to execute the script.

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonLauncher.git
    import {Launcher, NpmModuleTools} from 'edgecommonlauncher';;

# Testing 

 - start Redis, MongoDB using docker
 
    docker-compose up
    
 - run the test locally
 
    npm run test-local

    
# Main Modules

## Launcher
> Launch an app that from an NPM

    const launcher = new Launcher();


#### General helper functions

- insertJobLog
> Any job can output one or more "Results=<Number>" lines and this
> will total up those numbers and add them to the record for reporting.

      launcher.insertJobLog( job: object, command: string, fullOutput: string, slot_id: number );

- runJob
> Job is an object as defined in "UnderstandingJobs.md" slot_id is any number that is used for reporting and also
> used to help the real-time status boards keep track of jobs.

      launcher.runJob( job: object, slot_id: number );
      
## NpmModuleTools
> NpmModuleTools - Help to make sure a module is verifyInstalled or updated

    NpmModuleTools.verifyInstalled(moduleSource: any, path: any);



