$ ->

    addTestButton "Basic CalendarWide", "Open", ()->
        addHolder().setView "CalendarWide", (view)->
            true
        true

    addTestButton "CalendarWide on Popup", "Open", () ->
        doPopupView "CalendarWide", "CalendarWide on Popup", null, 600, 800, (view)->
            true
        true

    go()