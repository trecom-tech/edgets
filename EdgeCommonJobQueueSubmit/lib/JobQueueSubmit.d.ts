export declare class JobQueueSubmit {
    priorityType: string;
    messageQueueItemUpdates: any;
    internalConnectionPromise: any;
    constructor(priorityType: string);
    doSubmitAppend(dataSet: any, path: string, newJson: any): any;
    doSubmitInsert(dataSet: any, path: string, data: any): any;
    doDeletePath(dataSet: any, path: string): any;
    doSubmitData(dataSet: any, path: string, data: any): Promise<any>;
    doInternalConnectItemUpdateMessageQueue(): any;
}
export default JobQueueSubmit;
