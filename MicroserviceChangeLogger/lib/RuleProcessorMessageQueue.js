"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var edgecommonmessagequeue_1 = require("edgecommonmessagequeue");
var edgecommonconfig_1 = require("edgecommonconfig");
var RuleProcessor_1 = require("./RuleProcessor");
var RuleProcessorMessageQueue = /** @class */ (function (_super) {
    __extends(RuleProcessorMessageQueue, _super);
    function RuleProcessorMessageQueue() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RuleProcessorMessageQueue.prototype.onInit = function () {
        var _this = this;
        if (!this.rule.mq_name) {
            return this.setError('Invalid Message Queue or missing "mq_name"');
        }
        edgecommonmessagequeue_1.EdgeAMQPController.doOpenMessageQueue(this.rule.mq_name)
            .then(function (mq) {
            _this.mq = mq;
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    RuleProcessorMessageQueue.prototype.onExecute = function (job, count) {
        var _this = this;
        if (count === void 0) { count = 0; }
        if (!this.mq) {
            edgecommonconfig_1.config.status('RuleProcessorMessageQueue onEexecute, mq not defined, postpone job');
            if (count === this.limit) {
                throw new Error('RuleProcessorMessageQueue onEexecute, mq not defined');
            }
            setTimeout(function () {
                _this.onExecute(job, count++);
            }, 1000);
            return;
        }
        this.mq.doSubmitBuffer(job);
    };
    return RuleProcessorMessageQueue;
}(RuleProcessor_1.default));
exports.default = RuleProcessorMessageQueue;
//# sourceMappingURL=RuleProcessorMessageQueue.js.map