"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonfile = require("jsonfile");
var mongodb_1 = require("mongodb");
var edgebulktablestorage_1 = require("edgebulktablestorage");
var edgedatasetmanager_1 = require("edgedatasetmanager");
var edgecommonconfig_1 = require("edgecommonconfig");
var RuleProcessorMessageQueue_1 = require("./RuleProcessorMessageQueue");
var RuleProcessorWorkqueue_1 = require("./RuleProcessorWorkqueue");
var reDate1 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T00.00.00.000Z/;
var reDate2 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9]Z/;
var reDate3 = / [0-9][0-9] [12][0-9][0-9][0-9] /;
var pipeline = [
    {
        $project: { documentKey: false }
    }
];
var Worker = /** @class */ (function () {
    function Worker(azureConfigName, azureTableName) {
        this.log = edgecommonconfig_1.config.getLogger('ChangeWorker');
        this.changeLogsTable = new edgebulktablestorage_1.default(azureConfigName, azureTableName);
        this.loggingTableName = azureTableName;
        this.dmLookup = new edgedatasetmanager_1.default('lookup');
        this.csvLogEnabled = false;
        this.azureLogEnabled = false;
        this.pathSkip = [];
        this.rules = [];
    }
    Worker.prototype.doRun = function (callback) {
        var _this = this;
        if (callback && typeof callback === 'function') {
            this.callback = callback;
        }
        this._doSetupMongoChangeStream()
            .then(function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._doReloadRules()];
                    case 1:
                        _a.sent();
                        this.changeStream.on('change', function (change) { return __awaiter(_this, void 0, void 0, function () {
                            var changeRec;
                            var _this = this;
                            return __generator(this, function (_a) {
                                changeRec = this._processChange(change);
                                this._doSaveChangesDatabase(changeRec)
                                    .then(function (success) {
                                    if (success && _this.callback) {
                                        _this.callback(null, changeRec);
                                    }
                                })
                                    .catch(function (err) {
                                    if (_this.callback) {
                                        _this.callback(err, changeRec);
                                    }
                                });
                                return [2 /*return*/];
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        }); })
            .catch(function (err) {
            console.log(err);
            _this.log.error('doRun', { err: err });
            if (_this.callback) {
                _this.callback(err, null);
            }
        });
    };
    Worker.prototype.setupSkipList = function () {
        var pathSkipList = jsonfile.readFileSync(edgecommonconfig_1.config.FindFileInPath('skip_list.json', ['~/EdgeConfig/']));
        for (var str in pathSkipList) {
            try {
                var re = new RegExp(str, 'i');
                this.pathSkip.push(re);
            }
            catch (e) {
                console.log('Invalid Regexp:', str);
                this.log.error('Invalid Regexp:', { re: str });
            }
        }
    };
    Worker.prototype._doSaveChangesDatabase = function (changeRec) {
        return this.changeLogsTable.doInsertLogRecord(this.loggingTableName, changeRec);
    };
    Worker.prototype._doReloadRules = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, _i, result_1, rule;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        edgecommonconfig_1.config.status('doReloadRules Reading lookup/change_rules');
                        return [4 /*yield*/, this.dmLookup.doGetItems('/change_rules', {})];
                    case 1:
                        result = _a.sent();
                        for (_i = 0, result_1 = result; _i < result_1.length; _i++) {
                            rule = result_1[_i];
                            edgecommonconfig_1.config.status('doReloadRules Adding:', rule);
                            if (rule.action === 'workqueue') {
                                this.rules.push(new RuleProcessorWorkqueue_1.default(rule));
                            }
                            else if (rule.action === 'messagequeue') {
                                this.rules.push(new RuleProcessorMessageQueue_1.default(rule));
                            }
                            else {
                                console.log('Invalid Rule:', rule);
                                this.log.error('Invalid rule', { rule: rule });
                            }
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    Worker.prototype._processChange = function (change) {
        var changeRec = {};
        changeRec.dataset = change.ns.db;
        changeRec.collection = change.ns.coll;
        changeRec.change_date = new Date();
        changeRec.kind = change.operationType;
        changeRec.record_id = String(change.fullDocument.id) || String(change.fullDocument._id);
        changeRec.path = change.ns.coll + "/" + changeRec.record_id;
        if (change.fullDocument.name) {
            changeRec.path += "/" + change.ns.coll + "/" + change.fullDocument.name;
        }
        if (changeRec.record_id.length >= 64) {
            console.log("Length error in record_id, '" + changeRec.record_id + "' > 64");
            changeRec.record_id = changeRec.record_id.substring(0, 64);
        }
        if (changeRec.dataset.length > 32) {
            console.log("Length error in dataset, '" + changeRec.dataset + "' > 32");
            changeRec.dataset = changeRec.dataset.substring(0, 32);
        }
        if (changeRec.collection.length > 64) {
            console.log("Length error in collection, '" + changeRec.collection + "' > 64");
            changeRec.collection = changeRec.collection.substring(0, 64);
        }
        changeRec.stamp = new Date();
        for (var _i = 0, _a = this.rules; _i < _a.length; _i++) {
            var rule = _a[_i];
            if (!rule.active) {
                continue;
            }
            if (rule.ds_name && rule.ds_name !== changeRec.dataset) {
                continue;
            }
            rule.check(changeRec);
        }
        return changeRec;
    };
    Worker.prototype._doSetupMongoChangeStream = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mongoServer, client;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        mongoServer = edgecommonconfig_1.config.getCredentials('MongoDB');
                        if (!mongoServer || typeof mongoServer !== 'object') {
                            throw new Error('Error: failed to load credentials');
                        }
                        return [4 /*yield*/, mongodb_1.MongoClient.connect(mongoServer.url_replicaSet, { useNewUrlParser: true })];
                    case 1:
                        client = _a.sent();
                        this.changeStream = client.watch(pipeline);
                        return [2 /*return*/];
                }
            });
        });
    };
    return Worker;
}());
exports.default = Worker;
//# sourceMappingURL=Worker.js.map