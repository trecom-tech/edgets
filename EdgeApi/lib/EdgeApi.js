"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const fs = require("fs");
const redis = require("redis");
const sockio = require("socket.io-client");
const edgecommonconfig_1 = require("edgecommonconfig");
const EdgeApiBase_1 = require("./EdgeApiBase");
const DeepDiff_1 = require("./DeepDiff");
const edgedatasetmanager_1 = require("edgedatasetmanager");
var EdgeApiProvider_1 = require("./EdgeApiProvider");
exports.EdgeApiProvider = EdgeApiProvider_1.default;
var DeepDiff_2 = require("./DeepDiff");
exports.DeepDiff = DeepDiff_2.default;
class EdgeApi extends EdgeApiBase_1.default {
    // New API Connection
    constructor() {
        super();
        this.socketConnectCallbackList = [];
        this.currentServer = null;
        // server url list
        this.serverList = null;
        this.socket = null;
    }
    /**
     * Get EdgeApi instance for specific server
     * @param serverName string[] | string server url
     */
    static doGetApiForServer(serverName) {
        return __awaiter(this, void 0, void 0, function* () {
            if (EdgeApi.globalCachedInstanceRemote == null) {
                EdgeApi.globalCachedInstanceRemote = {};
            }
            if (EdgeApi.globalCachedInstanceRemote[serverName] != null) {
                return EdgeApi.globalCachedInstanceRemote[serverName];
            }
            EdgeApi.globalCachedInstanceRemote[serverName] = new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                const api = new EdgeApi();
                api.serverList = [serverName];
                yield api.doConnect();
                resolve(api);
            }));
            return EdgeApi.globalCachedInstanceRemote[serverName];
        });
    }
    // Static function to return an instance of the api
    static doGetApi() {
        return __awaiter(this, void 0, void 0, function* () {
            if (EdgeApi.globalCachedInstance != null) {
                return EdgeApi.globalCachedInstance;
            }
            EdgeApi.globalCachedInstance = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                let api = new EdgeApi();
                try {
                    yield api.doConnect();
                    resolve(api);
                }
                catch (err) {
                    const log = edgecommonconfig_1.config.getLogger('EdgeApi');
                    log.error('Api doConnect Error', err);
                    reject(err);
                }
            }));
            return EdgeApi.globalCachedInstance;
        });
    }
    /**
     * Returns Redis Readonly connection
     */
    static getRedisReadonlyConnection() {
        let redisHost = edgecommonconfig_1.config.getCredentials('redisReadHost');
        edgecommonconfig_1.config.status(`getRedisReadonlyConnection to ${redisHost}`);
        const conn = redis.createClient(redisHost, {
            no_ready_check: true,
            retry_strategy: (options) => {
                if (options != null && options.attempt < 5) {
                    return 1000;
                }
                let log = edgecommonconfig_1.config.getLogger('Redis');
                log.error('RedisReconnect', { options });
                return 10 * 1000;
            },
        });
        conn.on('error', (err) => {
            let log = edgecommonconfig_1.config.getLogger('Redis');
            log.error('RedisError', { err });
            console.log('Redis Error: ', err);
            return true;
        });
        return conn;
    }
    static getRedisConnectionWQ() {
        let redisHost = edgecommonconfig_1.config.getCredentials('redisHostWQ');
        if (process.env.NODE_ENV === 'test')
            redisHost = process.env.REDIS_HOST;
        let conn = redis.createClient(redisHost, {
            no_ready_check: true,
            retry_strategy: options => {
                if (options != null && options.attempt < 5) {
                    return 1000;
                }
                let log = edgecommonconfig_1.config.getLogger('Redis');
                log.error('RedisReconnect', { options });
                return 10 * 1000;
            },
        });
        conn.on('error', err => {
            let log = edgecommonconfig_1.config.getLogger('Redis');
            log.error('RedisError', { err });
            console.log('Redis Error: ', err);
            return true;
        });
        return conn;
    }
    static getRedisConnection() {
        let redisHost = edgecommonconfig_1.config.getCredentials('redisHost');
        if (process.env.NODE_ENV === 'test')
            redisHost = process.env.REDIS_HOST;
        let conn = redis.createClient(redisHost, {
            no_ready_check: true,
            retry_strategy: options => {
                if (options != null && options.attempt < 5) {
                    return 1000;
                }
                let log = edgecommonconfig_1.config.getLogger('Redis');
                log.error('RedisReconnect', { options });
                return 10 * 1000;
            },
        });
        conn.on('error', err => {
            let log = edgecommonconfig_1.config.getLogger('Redis');
            log.error('RedisError', { err });
            console.log('Redis Error: ', err);
            return true;
        });
        return conn;
    }
    doClose() {
        return new Promise((resolve, reject) => {
            edgecommonconfig_1.config.status('EdgeApi::doClose');
            return this.doCompletePending().then(() => {
                edgecommonconfig_1.config.status('EdgeApi::doClose Provider is complete');
                this.socket.disconnect();
                delete this.socket;
                return resolve(true);
            });
        });
    }
    connectToHost() {
        edgecommonconfig_1.config.status('EdgeApi connectToHost called with server list:', this.serverList);
        this.currentServer = this.serverList.shift();
        this.serverList.push(this.currentServer);
        edgecommonconfig_1.config.status(`EdgeApi doGetSocket connecting to ${this.currentServer}`);
        let opt = {
            reconnection: false,
        };
        // brian - Without reconnect, something is causing the
        // client to exit when a connection drops instead of
        // moving to the next call
        opt = {
            reconnection: true,
            reconnectionDelay: 2000,
            reconnectionDelayMax: 5000,
            reconnectionAttempts: 99999,
        };
        this.socket = sockio(this.currentServer, opt);
        this.socket.on('connect', (_err, result) => {
            edgecommonconfig_1.config.status(`EdgeApi connectToHost Connect [${this.currentServer}]`);
            return true;
        });
        this.socket.on('error', (err, result) => {
            console.log('EdgeApi connectToHost Error', {
                err,
                result,
            });
            return true;
        });
        this.socket.on('connect_error', (_err, result) => {
            edgecommonconfig_1.config.status('EdgeApi connectToHost Connect Error');
            console.log('EdgeApi connectToHost Connect Error');
            return true;
        });
        this.socket.on('connect_timeout', (_err, result) => {
            edgecommonconfig_1.config.status('EdgeApi connectToHost Connect Timeout');
            console.log('EdgeApi connectToHost Connect Timeout');
            return true;
        });
        this.socket.on('reconnecting', (_err, result) => {
            edgecommonconfig_1.config.status('EdgeApi connectToHost Reconnecting');
            console.log('EdgeApi connectToHost Reconnecting');
            return true;
        });
        this.socket.on('reconnect_error', (_err, result) => {
            edgecommonconfig_1.config.status('EdgeApi connectToHost reconnect_error');
            console.log('EdgeApi connectToHost reconnect_error');
            return true;
        });
        this.socket.on('reconnect_failed', (_err, result) => {
            edgecommonconfig_1.config.status('EdgeApi connectToHost reconnect_failed');
            console.log('EdgeApi connectToHost reconnect_failed');
            return true;
        });
        if (this.socketConnectCallbackList != null) {
            for (let cb of Array.from(this.socketConnectCallbackList)) {
                cb.bind(this)(this.socket);
            }
        }
        return true;
    }
    /**
     * Returns a valid socket-io based class
     * @param socketConnectCallbackList
     */
    doGetSocket(socketConnectCallbackList) {
        return __awaiter(this, void 0, void 0, function* () {
            this.socketConnectCallbackList = socketConnectCallbackList;
            if (!this.serverList) {
                this.serverList = edgecommonconfig_1.config.getCredentials('ApiServers');
            }
            edgecommonconfig_1.config.status('EdgeApi::doGetSocket connecting');
            this.connectToHost();
            return true;
        });
    }
    /**
     * Internal helper function that recursively hashes a variable
     * @param recordHash
     * @param value
     */
    static getDataHash(recordHash, value) {
        if (value == null) {
            return true;
        }
        if (typeof value === 'string' && value.charAt(0) === '_') {
            return false;
        }
        if (typeof value !== 'object') {
            // console.log "recordHash update [#{value.toString()}]"
            recordHash.update(value.toString());
            return false;
        }
        for (const varName of Object.keys(value).sort()) {
            if (varName.charAt(0) === '_') {
                continue;
            }
            if (varName === 'processed') {
                continue;
            }
            const val = value[varName];
            if (val == null) {
                continue;
            }
            recordHash.update(varName);
            this.getDataHash(recordHash, val);
        }
        return true;
    }
    static getHash(value) {
        const recordHash = crypto.createHash('sha1');
        EdgeApi.getDataHash(recordHash, value);
        return recordHash.digest('hex');
    }
    /**
     * Removed undefined and null values in an object
     * @param obj
     */
    static cleanupObject(obj) {
        for (let keyName in obj) {
            let keyVal = obj[keyName];
            if (typeof keyVal === 'object') {
                this.cleanupObject(keyVal);
            }
            else if (keyVal === null || keyVal == null || keyVal === '') {
                delete obj[keyName];
            }
        }
        return true;
    }
    static deepDiff(object1, object2, basePath = null) {
        return DeepDiff_1.default(object1, object2, (basePath = null));
    }
    /**
     * General helper function to deep copy / clone an object
     * @param objTarget
     * @param objSrc
     * @param addAttributes
     * @param deleteAttributes
     */
    static deepMergeObject(objTarget, objSrc, addAttributes, deleteAttributes) {
        try {
            if (objTarget == null) {
                return null;
            }
            if (objSrc == null) {
                return null;
            }
            // console.log "objTarget:", objTarget
            // console.log "objSource:", objSrc
            let flagFound = false;
            for (const i in objSrc) {
                let x;
                let o = objSrc[i];
                if (typeof o === 'function') {
                    continue;
                }
                if (o === null) {
                    objTarget[i] = null;
                }
                else if (Array.isArray(o)) {
                    // Merge the arrange
                    objTarget[i] = [];
                    for (let subItem of Array.from(o)) {
                        objTarget[i].push(EdgeApi.cloneObject(subItem));
                    }
                }
                else if (o instanceof Date) {
                    objTarget[i] = new Date(o.getTime());
                    flagFound = true;
                }
                else if (typeof o !== 'object') {
                    objTarget[i] = o;
                    flagFound = true;
                }
                else {
                    if (!objTarget[i]) {
                        objTarget[i] = {};
                    }
                    EdgeApi.deepMergeObject(objTarget[i], o, addAttributes, deleteAttributes);
                }
                if (flagFound && addAttributes != null) {
                    for (x in addAttributes) {
                        let y = addAttributes[x];
                        objTarget[x] = y;
                    }
                }
                if (flagFound && deleteAttributes != null) {
                    for (x of deleteAttributes) {
                        delete objTarget[x];
                    }
                }
            }
            // console.log "Returning:", objTarget
            return objTarget;
        }
        catch (e) {
            console.log('Target=', objTarget);
            console.log('Source=', objSrc);
            edgecommonconfig_1.config.reportError('deepMergeObject:', e);
            return {};
        }
    }
    /**
     * General helper function to deep copy / clone an object
     * @param obj
     */
    static cloneObject(obj) {
        if (obj == null || typeof obj !== 'object') {
            return obj;
        }
        if (obj instanceof Date) {
            return new Date(obj.getTime());
        }
        if (Array.isArray(obj)) {
            let result = [];
            for (let subItem of Array.from(obj)) {
                result.push(EdgeApi.cloneObject(subItem));
            }
            return result;
        }
        if (obj instanceof RegExp) {
            let flags = '';
            if (obj.global != null) {
                flags += 'g';
            }
            if (obj.ignoreCase != null) {
                flags += 'i';
            }
            if (obj.multiline != null) {
                flags += 'm';
            }
            if (obj.sticky != null) {
                flags += 'y';
            }
            return new RegExp(obj.source, flags);
        }
        let newInstance = new obj.constructor();
        for (let key in obj) {
            newInstance[key] = EdgeApi.cloneObject(obj[key]);
        }
        return newInstance;
    }
    /**
     * Helper function to record the contents of an item object
     * to a text file in a way to can be used to debug or view the data easily.
     * @param obj
     * @param saveFile
     * @param theName
     */
    static dumpObject(obj, saveFile, theName) {
        return new Promise(function (resolve, reject) {
            let output = [];
            let maxlen = 0;
            let wstream = fs.createWriteStream(saveFile);
            wstream.on('close', () => resolve());
            let loopObject = function (obj, prefix, betweenText = '') {
                for (let i in obj) {
                    let o = obj[i];
                    if (typeof o === 'object') {
                        loopObject(o, prefix + '.' + i);
                    }
                    else {
                        output.push({
                            key: prefix + '.' + i,
                            val: o,
                        });
                        if (i.length + prefix.length + 2 > maxlen) {
                            maxlen = i.length + prefix.length + 2;
                        }
                    }
                }
                if (betweenText != null) {
                    return output.push({
                        key: betweenText,
                    });
                }
                return null;
            };
            loopObject(obj, theName, '-------------------------------------------------------------------------------------------------------------------------------------------');
            for (let i of Array.from(output)) {
                while (i.key.length < maxlen) {
                    i.key += ' ';
                }
                if (i.val != null) {
                    wstream.write(`${i.key} = ${i.val}\n`);
                }
                else {
                    wstream.write(`${i.key}\n`);
                }
            }
            return wstream.end();
        });
    }
}
EdgeApi.cacheGet = (keyname) => __awaiter(this, void 0, void 0, function* () {
    edgecommonconfig_1.config.status(`EdgeApi::cacheGet ${keyname}`);
    return new Promise(function (resolve, reject) {
        try {
            if (EdgeApi.globalCacheReadonly == null) {
                edgecommonconfig_1.config.status('EdgeApi::cacheGet Connecting to redis');
                EdgeApi.globalCacheReadonly = EdgeApi.getRedisReadonlyConnection();
            }
            return EdgeApi.globalCacheReadonly.get(keyname, (_err, doc) => {
                edgecommonconfig_1.config.status(`EdgeApi::cacheGet ${keyname} found value `, doc);
                resolve(doc);
            });
        }
        catch (e) {
            edgecommonconfig_1.config.status('EdgeApi::cacheGet Exception:', e);
            edgecommonconfig_1.config.reportError('EdgeApi::cacheGet Exception:', e);
            resolve(null);
        }
    });
});
EdgeApi.cacheSetExpireSeconds = (keyname, value, expireTime) => {
    if (expireTime == null) {
        expireTime = 120;
    }
    if (!EdgeApi.globalCache) {
        EdgeApi.globalCache = EdgeApi.getRedisConnection();
    }
    if (keyname == null) {
        return false;
    }
    if (value == null) {
        edgecommonconfig_1.config.status(`EdgeApi::cacheSet delete ${keyname}`);
        EdgeApi.globalCache.del(keyname);
    }
    else {
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }
        edgecommonconfig_1.config.status(`EdgeApi::cacheSet ${keyname} = ${value}`);
        EdgeApi.globalCache.setex(keyname, expireTime, value);
    }
    return true;
};
/**
 *
 * @param keyname
 * @param value
 */
EdgeApi.cacheSet = (keyname, value) => __awaiter(this, void 0, void 0, function* () {
    if (!EdgeApi.globalCache) {
        EdgeApi.globalCache = EdgeApi.getRedisConnection();
    }
    if (keyname == null) {
        return false;
    }
    if (value == null) {
        edgecommonconfig_1.config.status(`EdgeApi::cacheSet delete ${keyname}`);
        EdgeApi.globalCache.del(keyname);
    }
    else {
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }
        edgecommonconfig_1.config.status(`EdgeApi::cacheSet ${keyname} = ${value}`);
        return new Promise((resolve, reject) => {
            EdgeApi.globalCache.set(keyname, value, (err, res) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(res);
                }
            });
        });
    }
    return true;
});
EdgeApi.cacheHGetAll = (keyname) => {
    return new Promise(function (resolve) {
        try {
            if (EdgeApi.globalCacheReadonly == null) {
                EdgeApi.globalCacheReadonly = EdgeApi.getRedisReadonlyConnection();
            }
            return EdgeApi.globalCacheReadonly.hgetall(keyname, function (_err, doc) {
                edgecommonconfig_1.config.status(`EdgeApi::cacheHGet ${keyname} found value `, doc);
                return resolve(doc);
            });
        }
        catch (e) {
            edgecommonconfig_1.config.status('EdgeApi::cacheHGet Exception:', e);
            edgecommonconfig_1.config.reportError('EdgeApi::cacheHGet Exception:', e);
            return resolve(null);
        }
    });
};
/**
 * Send command to redis https://redis.io/commands
 * @param keyname
 * @param data
 */
EdgeApi.cacheHMSet = (keyname, data) => {
    if (!EdgeApi.globalCache) {
        EdgeApi.globalCache = EdgeApi.getRedisConnection();
    }
    if (keyname == null) {
        return false;
    }
    if (data == null) {
        let log = edgecommonconfig_1.config.getLogger('EdgeApi');
        log.error('invalid HMSET', { keyname, data });
        data = '';
    }
    EdgeApi.globalCache.hmset(keyname, data);
    return true;
};
/**
 *
 */
EdgeApi.cacheSetPop = (keyname) => {
    return new Promise((resolve, reject) => {
        if (EdgeApi.globalCacheSets == null) {
            EdgeApi.globalCacheSets = EdgeApi.getRedisConnectionWQ();
        }
        edgecommonconfig_1.config.status(`cacheSetPop ${keyname}`);
        if (keyname == null) {
            reject();
        }
        return EdgeApi.globalCacheSets.spop(keyname, (err, data) => {
            if (err != null) {
                edgecommonconfig_1.config.status('pop error=', err);
            }
            return resolve(data);
        });
    });
};
// REDIS SADD
EdgeApi.cacheSetAdd = (keyname, data) => {
    if (EdgeApi.globalCacheSets == null) {
        EdgeApi.globalCacheSets = EdgeApi.getRedisConnectionWQ();
    }
    if (keyname == null) {
        return false;
    }
    edgecommonconfig_1.config.status(`cacheSetAdd ${keyname}, ${data}`);
    EdgeApi.globalCacheSets.sadd(keyname, data);
    return true;
};
// REDIS SCARD
EdgeApi.cacheGetSetLength = (keyname) => {
    return new Promise((resolve, reject) => {
        if (EdgeApi.globalCacheSets == null) {
            EdgeApi.globalCacheSets = EdgeApi.getRedisConnectionWQ();
        }
        edgecommonconfig_1.config.status(`Checking set length on ${keyname}`);
        if (keyname == null) {
            return false;
        }
        return EdgeApi.globalCacheSets.scard(keyname, (err, data) => {
            if (err != null) {
                edgecommonconfig_1.config.status('scard error=', err);
            }
            return resolve(data);
        });
    });
};
// REDIS SMEMBERS
EdgeApi.cacheSetMembers = (keyname) => {
    return new Promise((resolve, reject) => {
        if (EdgeApi.globalCacheSets == null) {
            EdgeApi.globalCacheSets = EdgeApi.getRedisConnectionWQ();
        }
        if (keyname == null) {
            reject();
        }
        edgecommonconfig_1.config.status(`cacheSetMembers ${keyname}`);
        return EdgeApi.globalCacheSets.smembers(keyname, (err, data) => {
            if (err != null) {
                edgecommonconfig_1.config.status('cacheSetMembers error=', err);
            }
            return resolve(data);
        });
    });
};
/**
 *
 * Quick utility function to query a table
 *
 * @param dataSet
 * @param tableName
 * @param condition
 * @param sort
 * @param callback (item, index, total) => Promise | booilean :  process each item callback
 */
EdgeApi.doQuickFind = (dataSet, tableName, condition, sort, callback) => __awaiter(this, void 0, void 0, function* () {
    let ds = new edgedatasetmanager_1.default(dataSet);
    let collection = yield ds.doGetCollection(tableName);
    let all = yield collection.doFind(condition, null, sort, 0, 100000);
    let counter = 0;
    let total = all.length;
    for (let item of Array.from(all)) {
        let result = callback(item, counter++, total);
        if (result != null && result === false) {
            break;
        }
        if (result != null) {
            yield result;
        }
    }
    return true;
});
// Run an aggregate query.
// TODO:  Determine if this should be run throught the API or Locally
// TODO:  Possible cache option
EdgeApi.doAggregate = (dataSet, tableName, aggregateQuery, options = {}) => __awaiter(this, void 0, void 0, function* () {
    if (edgecommonconfig_1.config.debugAggregateCalls) {
        edgecommonconfig_1.config.status(`Starting doAggregate ${dataSet}/${tableName}`);
    }
    const ds = new edgedatasetmanager_1.default(dataSet);
    const collection = yield ds.doGetCollection(tableName);
    return collection.doAggregate(aggregateQuery);
});
/**
 * A simple way to run a query such as SELECT field, count(*) FROM table GROUP BY field
 * This runs a mongo aggregate against the "fieldList" array
 * @param dataSet [string] Data set name
 * @param tableName [string] Name of the table
 * @param fieldList [Object or Array or Comma String] A list of fields to group
 * @param matchCondition [object] Can be null, only count if a certain condition exists
 * @param sortCondition [Object] Optional sort condition
 * @param limit [number] Optional maximum records to return
 *
 */
EdgeApi.doAggregateCount = (dataSet, tableName, fieldList, matchCondition, sortCondition, limit) => __awaiter(this, void 0, void 0, function* () {
    edgecommonconfig_1.config.dump(`doAggregateCount Start ${dataSet}/${tableName}`, {
        fieldList,
        matchCondition,
        sortCondition,
        limit,
    });
    const query = {};
    const aggList = [];
    // Add the match condition if one was specified.   Not required by Mongo
    if (matchCondition != null && typeof matchCondition === 'object') {
        aggList.push({ $match: matchCondition });
    }
    // If fieldList is text/string (may be a single field) then convert to array
    if (fieldList != null && typeof fieldList === 'string') {
        fieldList = fieldList.split(',');
    }
    // Build the "group by" part of the query
    query['$group'] = {};
    query['$group']['_id'] = {};
    query['$group']['count'] = { $sum: 1 };
    for (let idx in fieldList) {
        let fieldName = fieldList[idx];
        if (typeof idx === 'number' || !/[a-zA-Z]/.test(idx)) {
            idx = fieldName.split('.').pop();
            idx = idx.replace('$', '');
            idx = idx.replace(/\s/g, '');
        }
        query['$group']['_id'][idx] = fieldName;
    }
    console.log('aggList query=', query['$group']);
    aggList.push(query);
    if (sortCondition != null) {
        aggList.push({ $sort: sortCondition });
    }
    // Run the aggregate query
    edgecommonconfig_1.config.dump('calling doAggregate with list:', aggList);
    let results = yield EdgeApi.doAggregate(dataSet, tableName, aggList);
    edgecommonconfig_1.config.status('Query complete');
    return results;
});
// Cleanup floats and ints and dates
EdgeApi.fixupJson = (obj) => {
    let reNumber = /^[\-1-9][0-9]{0,10}$/;
    let reDecimal = /^[\-1-9\.][0-9\.]{0,11}\.[0-9]+$/;
    for (const i in obj) {
        let o = obj[i];
        if (typeof o === 'object') {
            if (o != null && o.getTime == null) {
                EdgeApi.fixupJson(o);
            }
        }
        else if (typeof o === 'number') {
            if (isNaN(o)) {
                o = null;
            }
            if (!isFinite(o)) {
                o = null;
            }
        }
        else if (typeof o === 'string') {
            if (o.length === 24 &&
                o.charAt(10) === 'T' &&
                o.charAt(23) === 'Z') {
                obj[i] = new Date(o);
            }
            else if (o.length === 23 && o.charAt(10) === 'T') {
                obj[i] = new Date(o);
            }
            else if (o.length === 19 &&
                o.charAt(10) === 'T' &&
                o.charAt(4) === '-') {
                obj[i] = new Date(o);
            }
            else if (o.length === 10 &&
                o.charAt(4) === '-' &&
                o.charAt(7) === '-') {
                obj[i] = new Date(o);
            }
            else if (o.length === 8 &&
                o.charAt(2) === '/' &&
                o.charAt(5) === '/') {
                obj[i] = new Date(o);
            }
            else if (o.length === 10 &&
                o.charAt(2) === '/' &&
                o.charAt(5) === '/') {
                obj[i] = new Date(o);
            }
            else if (reNumber.test(o)) {
                // tslint:disable-next-line:radix
                obj[i] = parseInt(o);
                // console.log "Convert int [#{o}] to #{obj[i]}"
            }
            else if (reDecimal.test(o)) {
                obj[i] = parseFloat(o);
            }
        }
        // console.log "Convert [#{o}] to #{obj[i]}"
        if (o == null) {
            delete obj[i];
        }
    }
    return obj;
};
// Flatten an object to a single array
EdgeApi.flattenObject = (obj, prefix, result) => {
    if (prefix == null) {
        prefix = '';
    }
    if (result == null) {
        result = {};
    }
    for (let varName in obj) {
        let value = obj[varName];
        varName = varName.toLowerCase().replace(/[^a-zA-Z0-9\_]/g, '_');
        varName = varName.replace('__', '_');
        if (value == null) {
            continue;
        }
        if (typeof value === 'object' &&
            typeof value.getTime === 'function') {
            result[prefix + varName] = new Date(value);
        }
        else if (typeof value === 'object' && Array.isArray(value)) {
            if (value[0] != null &&
                value[1] == null &&
                typeof value[0] === 'object') {
                EdgeApi.flattenObject(value[0], prefix + varName + '.', result);
            }
            else if (typeof value[0] === 'object') {
                for (let keyName of Array.from(value)) {
                    EdgeApi.flattenObject(value[keyName], prefix + varName + '.' + keyName + '.', result);
                }
            }
            else {
                result[prefix + varName] = value.join(',');
            }
        }
        else if (typeof value === 'object') {
            EdgeApi.flattenObject(value, prefix + varName + '.', result);
        }
        else {
            result[prefix + varName] = value;
        }
    }
    return result;
};
exports.EdgeApi = EdgeApi;
exports.default = EdgeApi;
//# sourceMappingURL=EdgeApi.js.map