"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * General configuration and error handlers
 */
var edgecommonconfig_1 = require("edgecommonconfig");
var Worker_1 = require("./Worker");
edgecommonconfig_1.config.setTitle('Edge Server');
// Start the worker
var worker = new Worker_1.default('azureTest', 'changelogs');
worker.doRun();
//# sourceMappingURL=ChangeLogServer.js.map