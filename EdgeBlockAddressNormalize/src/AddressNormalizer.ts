// -------------------------------------------------------------------------------------------------------------
// Edge Block that will take an address and try to return all the parts and pieces.   If possible it will lookup
// missing details such as zipcodes and fill in the blanks.
//
// process: (addressText, city, state, zip)
import config      from 'edgecommonconfig';
import EdgeBlock   from 'edgeblock';
import EdgeRequest from 'edgecommonrequest';
import * as crypto from 'crypto';
import EdgeApi     from 'edgeapi';

const jsonfile = require('jsonfile');
const parser   = require('parse-address');

//
//  Stores a list of zipcodes and city, state data.  Loaded dynamically
//  at runtime as needed from the module's directory
//
let globalZipcodes: any = null;

const globalStatesList: any = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AS": "American Samoa",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "DC": "District Of Columbia",
    "FM": "Federated States Of Micronesia",
    "FL": "Florida",
    "GA": "Georgia",
    "GU": "Guam",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MH": "Marshall Islands",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "MP": "Northern Mariana Islands",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PW": "Palau",
    "PA": "Pennsylvania",
    "PR": "Puerto Rico",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VI": "Virgin Islands",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
};

const globalAddressParts = {
    "allee"     : "Aly",
    "alley"     : "Aly",
    "ally"      : "Aly",
    "anex"      : "Anx",
    "annex"     : "Anx",
    "annx"      : "Anx",
    "arcade"    : "Arc",
    "av"        : "Ave",
    "aven"      : "Ave",
    "avenu"     : "Ave",
    "avenue"    : "Ave",
    "avn"       : "Ave",
    "avnue"     : "Ave",
    "bayoo"     : "Byu",
    "bayou"     : "Byu",
    "beach"     : "Bch",
    "bend"      : "Bnd",
    "bluf"      : "Blf",
    "bluff"     : "Blf",
    "bluffs"    : "Blfs",
    "bot"       : "Btm",
    "bottm"     : "Btm",
    "bottom"    : "Btm",
    "boul"      : "Blvd",
    "boulevard" : "Blvd",
    "boulv"     : "Blvd",
    "branch"    : "Br",
    "brdge"     : "Brg",
    "bridge"    : "Brg",
    "brnch"     : "Br",
    "brook"     : "Brk",
    "brooks"    : "Brks",
    "burg"      : "Bg",
    "burgs"     : "Bgs",
    "bypa"      : "Byp",
    "bypas"     : "Byp",
    "bypass"    : "Byp",
    "byps"      : "Byp",
    "camp"      : "Cp",
    "canyn"     : "Cyn",
    "canyon"    : "Cyn",
    "cape"      : "Cpe",
    "causeway"  : "Cswy",
    "causway"   : "Cswy",
    "cen"       : "Ctr",
    "cent"      : "Ctr",
    "center"    : "Ctr",
    "centers"   : "Ctrs",
    "centr"     : "Ctr",
    "centre"    : "Ctr",
    "circ"      : "Cir",
    "circl"     : "Cir",
    "circle"    : "Cir",
    "circles"   : "Cirs",
    "ck"        : "Crk",
    "cliff"     : "Clf",
    "cliffs"    : "Clfs",
    "club"      : "Clb",
    "cmp"       : "Cp",
    "cnter"     : "Ctr",
    "cntr"      : "Ctr",
    "cnyn"      : "Cyn",
    "common"    : "Cmn",
    "corner"    : "Cor",
    "corners"   : "Cors",
    "course"    : "Crse",
    "court"     : "Ct",
    "courts"    : "Cts",
    "cove"      : "Cv",
    "coves"     : "Cvs",
    "cr"        : "Crk",
    "crcl"      : "Cir",
    "crcle"     : "Cir",
    "crecent"   : "Cres",
    "creek"     : "Crk",
    "crescent"  : "Cres",
    "cresent"   : "Cres",
    "crest"     : "Crst",
    "crossing"  : "Xing",
    "crossroad" : "Xrd",
    "crscnt"    : "Cres",
    "crsent"    : "Cres",
    "crsnt"     : "Cres",
    "crssing"   : "Xing",
    "crssng"    : "Xing",
    "crt"       : "Ct",
    "curve"     : "Curv",
    "dale"      : "Dl",
    "dam"       : "Dm",
    "div"       : "Dv",
    "divide"    : "Dv",
    "driv"      : "Dr",
    "drive"     : "Dr",
    "drives"    : "Drs",
    "drv"       : "Dr",
    "dvd"       : "Dv",
    "estate"    : "Est",
    "estates"   : "Ests",
    "exp"       : "Expy",
    "expr"      : "Expy",
    "express"   : "Expy",
    "expressway": "Expy",
    "expw"      : "Expy",
    "extension" : "Ext",
    "extensions": "Exts",
    "extn"      : "Ext",
    "extnsn"    : "Ext",
    "falls"     : "Fls",
    "ferry"     : "Fry",
    "field"     : "Fld",
    "fields"    : "Flds",
    "flat"      : "Flt",
    "flats"     : "Flts",
    "ford"      : "Frd",
    "fords"     : "Frds",
    "forest"    : "Frst",
    "forests"   : "Frst",
    "forg"      : "Frg",
    "forge"     : "Frg",
    "forges"    : "Frgs",
    "fork"      : "Frk",
    "forks"     : "Frks",
    "fort"      : "Ft",
    "freeway"   : "Fwy",
    "freewy"    : "Fwy",
    "frry"      : "Fry",
    "frt"       : "Ft",
    "frway"     : "Fwy",
    "frwy"      : "Fwy",
    "garden"    : "Gdn",
    "gardens"   : "Gdns",
    "gardn"     : "Gdn",
    "gateway"   : "Gtwy",
    "gatewy"    : "Gtwy",
    "gatway"    : "Gtwy",
    "glen"      : "Gln",
    "glens"     : "Glns",
    "grden"     : "Gdn",
    "grdn"      : "Gdn",
    "grdns"     : "Gdns",
    "green"     : "Grn",
    "greens"    : "Grns",
    "grov"      : "Grv",
    "grove"     : "Grv",
    "groves"    : "Grvs",
    "gtway"     : "Gtwy",
    "harb"      : "Hbr",
    "harbor"    : "Hbr",
    "harbors"   : "Hbrs",
    "harbr"     : "Hbr",
    "haven"     : "Hvn",
    "havn"      : "Hvn",
    "height"    : "Hts",
    "heights"   : "Hts",
    "hgts"      : "Hts",
    "highway"   : "Hwy",
    "highwy"    : "Hwy",
    "hill"      : "Hl",
    "hills"     : "Hls",
    "hiway"     : "Hwy",
    "hiwy"      : "Hwy",
    "hllw"      : "Holw",
    "hollow"    : "Holw",
    "hollows"   : "Holw",
    "holws"     : "Holw",
    "hrbor"     : "Hbr",
    "ht"        : "Hts",
    "hway"      : "Hwy",
    "inlet"     : "Inlt",
    "island"    : "Is",
    "islands"   : "Iss",
    "isles"     : "Isle",
    "islnd"     : "Is",
    "islnds"    : "Iss",
    "jction"    : "Jct",
    "jctn"      : "Jct",
    "jctns"     : "Jcts",
    "junction"  : "Jct",
    "junctions" : "Jcts",
    "junctn"    : "Jct",
    "juncton"   : "Jct",
    "key"       : "Ky",
    "keys"      : "Kys",
    "knol"      : "Knl",
    "knoll"     : "Knl",
    "knolls"    : "Knls",
    "la"        : "Ln",
    "lake"      : "Lk",
    "lakes"     : "Lks",
    "landing"   : "Lndg",
    "lane"      : "Ln",
    "lanes"     : "Ln",
    "ldge"      : "Ldg",
    "light"     : "Lgt",
    "lights"    : "Lgts",
    "lndng"     : "Lndg",
    "loaf"      : "Lf",
    "lock"      : "Lck",
    "locks"     : "Lcks",
    "lodg"      : "Ldg",
    "lodge"     : "Ldg",
    "loops"     : "Loop",
    "manor"     : "Mnr",
    "manors"    : "Mnrs",
    "meadow"    : "Mdw",
    "meadows"   : "Mdws",
    "medows"    : "Mdws",
    "mill"      : "Ml",
    "mills"     : "Mls",
    "mission"   : "Msn",
    "missn"     : "Msn",
    "mnt"       : "Mt",
    "mntain"    : "Mtn",
    "mntn"      : "Mtn",
    "mntns"     : "Mtns",
    "motorway"  : "Mtwy",
    "mount"     : "Mt",
    "mountain"  : "Mtn",
    "mountains" : "Mtns",
    "mountin"   : "Mtn",
    "mssn"      : "Msn",
    "mtin"      : "Mtn",
    "neck"      : "Nck",
    "orchard"   : "Orch",
    "orchrd"    : "Orch",
    "overpass"  : "Opas",
    "ovl"       : "Oval",
    "parks"     : "Park",
    "parkway"   : "Pkwy",
    "parkways"  : "Pkwy",
    "parkwy"    : "Pkwy",
    "passage"   : "Psge",
    "paths"     : "Path",
    "pikes"     : "Pike",
    "pine"      : "Pne",
    "pines"     : "Pnes",
    "pk"        : "Park",
    "pkway"     : "Pkwy",
    "pkwys"     : "Pkwy",
    "pky"       : "Pkwy",
    "place"     : "Pl",
    "plain"     : "Pln",
    "plaines"   : "Plns",
    "plains"    : "Plns",
    "plaza"     : "Plz",
    "plza"      : "Plz",
    "point"     : "Pt",
    "points"    : "Pts",
    "port"      : "Prt",
    "ports"     : "Prts",
    "prairie"   : "Pr",
    "prarie"    : "Pr",
    "prk"       : "Park",
    "prr"       : "Pr",
    "rad"       : "Radl",
    "radial"    : "Radl",
    "radiel"    : "Radl",
    "ranch"     : "Rnch",
    "ranches"   : "Rnch",
    "rapid"     : "Rpd",
    "rapids"    : "Rpds",
    "rdge"      : "Rdg",
    "rest"      : "Rst",
    "ridge"     : "Rdg",
    "ridges"    : "Rdgs",
    "river"     : "Riv",
    "rivr"      : "Riv",
    "rnchs"     : "Rnch",
    "road"      : "Rd",
    "roads"     : "Rds",
    "route"     : "Rte",
    "rvr"       : "Riv",
    "shoal"     : "Shl",
    "shoals"    : "Shls",
    "shoar"     : "Shr",
    "shoars"    : "Shrs",
    "shore"     : "Shr",
    "shores"    : "Shrs",
    "skyway"    : "Skwy",
    "spng"      : "Spg",
    "spngs"     : "Spgs",
    "spring"    : "Spg",
    "springs"   : "Spgs",
    "sprng"     : "Spg",
    "sprngs"    : "Spgs",
    "spurs"     : "Spur",
    "sqr"       : "Sq",
    "sqre"      : "Sq",
    "sqrs"      : "Sqs",
    "squ"       : "Sq",
    "square"    : "Sq",
    "squares"   : "Sqs",
    "station"   : "Sta",
    "statn"     : "Sta",
    "stn"       : "Sta",
    "str"       : "St",
    "strav"     : "Stra",
    "strave"    : "Stra",
    "straven"   : "Stra",
    "stravenue" : "Stra",
    "stravn"    : "Stra",
    "stream"    : "Strm",
    "street"    : "St",
    "streets"   : "Sts",
    "streme"    : "Strm",
    "strt"      : "St",
    "strvn"     : "Stra",
    "strvnue"   : "Stra",
    "sumit"     : "Smt",
    "sumitt"    : "Smt",
    "summit"    : "Smt",
    "terr"      : "Ter",
    "terrace"   : "Ter",
    "throughway": "Trwy",
    "tpk"       : "Tpke",
    "tr"        : "Trl",
    "trace"     : "Trce",
    "traces"    : "Trce",
    "track"     : "Trak",
    "tracks"    : "Trak",
    "trafficway": "Trfy",
    "trail"     : "Trl",
    "trails"    : "Trl",
    "trk"       : "Trak",
    "trks"      : "Trak",
    "trls"      : "Trl",
    "trnpk"     : "Tpke",
    "trpk"      : "Tpke",
    "tunel"     : "Tunl",
    "tunls"     : "Tunl",
    "tunnel"    : "Tunl",
    "tunnels"   : "Tunl",
    "tunnl"     : "Tunl",
    "turnpike"  : "Tpke",
    "turnpk"    : "Tpke",
    "underpass" : "Upas",
    "union"     : "Un",
    "unions"    : "Uns",
    "valley"    : "Vly",
    "valleys"   : "Vlys",
    "vally"     : "Vly",
    "vdct"      : "Via",
    "viadct"    : "Via",
    "viaduct"   : "Via",
    "view"      : "Vw",
    "views"     : "Vws",
    "vill"      : "Vlg",
    "villag"    : "Vlg",
    "village"   : "Vlg",
    "villages"  : "Vlgs",
    "ville"     : "Vl",
    "villg"     : "Vlg",
    "villiage"  : "Vlg",
    "vist"      : "Vis",
    "vista"     : "Vis",
    "vlly"      : "Vly",
    "vst"       : "Vis",
    "vsta"      : "Vis",
    "walks"     : "Walk",
    "well"      : "Wl",
    "wells"     : "Wls",
    "wy"        : "Way"
};

export class AddressNormalizer extends EdgeBlock {

    constructor(...args: any[]) {
        super(...args);
    }

    getInputs() {
        return ["Address Text"];
    }

    //
    //  Lookup a zipcode and return the information about it
    getZipcodeData(zipcode: number) {

        if (globalZipcodes === null) {
            const filename = config.FindFileInPath("zipcodes.json", ["./", "../", "./src/", "../src/"]);

            if (filename != null) {
                globalZipcodes = jsonfile.readFileSync(filename);
            } else {
                config.status("AddressNormalizer getZipcodeData unable to find zipcodes.json file");
                return null;
            }
        }

        return globalZipcodes[zipcode.toString()];
    }

    //
    // Get zip code with city and state from zipcodes.json
    //
    getZipWithCityState(city: string, state: string) {

        let zipCode = '';

        if (globalZipcodes === null) {
            const filename = config.FindFileInPath("zipcodes.json", ["./", "../", "./src/", "../src/"]);
            if (filename) {
                globalZipcodes = jsonfile.readFileSync(filename);
            } else {
                config.status("AddressNormalizer getZipcodeData unable to find zipcodes.json file");
                return null;
            }
        }

        for (let zip in globalZipcodes) {
            const value = globalZipcodes[zip];
            if ((value.city.toLowerCase() === city.toLowerCase()) && (value.state.toLowerCase() === state.toLowerCase())) {
                zipCode = zip;
                return zip;
            }
        }

        return zipCode;
    }

    //
    //  Given a city and state, try to find the matching zipcode
    //  Uses the Zippopotam API and caches the result
    //
    async getZipcodeFromCityState(city: string, state: string) {
        const key       = `ZIPFROM${city},${state}`;
        let result: any = await EdgeApi.cacheGet(key);

        if (result == null) {
            result = await new EdgeRequest().doGetLink(`http://api.zippopotam.us/us/${state}/${city}`);
            await EdgeApi.cacheSet(key, result);
        }

        if ((result != null) && (typeof result === "string")) {
            try {
                result = JSON.parse(result);
            } catch (e) {
                result = null;
            }
        }

        if ((result != null) && (result.places != null) && (result.places[0] != null) && result.places[0]['post code']) {
            return result.places[0]['post code'];
        } else {
            console.log(`RESULT OF C ${city}, ${state}=`, result);
            return null;
        }
    }

    //
    //  Convert an address text into address parts
    //
    process(addressText: string, city?: string, state?: string, zip?: string): any {

        let output: any;
        let result: any;
        config.status("AddressNormalizer process addressText=", addressText, "city=", city, "state=", state, "zip=", zip);

        try {
            result = {};
            if ((addressText.indexOf(city) === -1) && (addressText.indexOf(",") === -1)) {
                addressText += `, ${city}, ${state} ${zip}`;
            }

            config.status(`libpostal: ${addressText}`);
            output = parser.parseLocation(addressText);

        } catch (e) {
            config.status("Exception during address parse");
            this.log.error("Processing Error", {
                addressText,
                city,
                state,
                zip,
                e
            });
            return null;
        }

        config.status(`Looking up ${city}, ${state} - ${zip}`);
        if ((city != null) && (state != null) && ((zip == null) || (zip.length === 0))) {
            //
            //  Try to find the zipcode for a given city/state
            // return new Promise (resolve, reject)=>
            // zip = @getZipcodeFromCityState(city, state)
            const zipCode = this.getZipWithCityState(city, state);
            const address = addressText.split(',')[0];
            return this.process(address, city, state, zipCode);
            // return null
            // return null

        } else {
            result.street              = "";
            const postcode_list: any[] = [];
            for (let key in output) {
                if (key === "number") {
                    if (result.number != null) {
                        result.lot = output[key];
                    } else {
                        result.number = output[key];
                    }
                } else if (key === "prefix") {
                    result.street += output[key] + ' ';
                } else if (key === "street") {
                    result.street += output[key] + ' ';
                } else if (key === "type") {
                    result.street += output[key] + ' ';
                } else if (key === "sec_unit_type") {
                    result.street += output[key] + ' ';
                } else if (key === "sec_unit_num") {
                    result.street += output[key];
                } else if (key === "city") {
                    result.city = this.fixTitleCase(output[key]);
                } else if (key === "state") {
                    result.state = output[key];
                } else if (key === "zip") {
                    result.zip = output[key];
                } else if (key === "suffix") {
                    result.street += output[key] + ' ';
                } else {
                    console.log(`Unknown [${key}] = [${output[key]}]: ${addressText}`);
                    this.log.error("Unknown AddressNormalize parse", { key, output: output[key] });
                }
            }

            if ((result.number == null) && (postcode_list.length > 1)) {
                result.number = postcode_list.shift();
            }

            if ((result.city == null) && (city != null)) {
                result.city = city.trim();
            }

            if ((result.state == null) && (state != null)) {
                result.state = state.trim();
            }

            if ((result.zip == null) && (zip != null)) {
                result.zip = zip.toString().trim();
            }

            if ((!result.city || !result.state) && result.zip) {
                const zipdata = this.getZipcodeData(result.zip);
                if (zipdata != null) {
                    result.city  = zipdata.city;
                    result.state = zipdata.state;
                }
            }

            if (result.state == null) {
                result.display = addressText;
                return result;
            }

            if (result.city == null) {
                result.display = addressText;
                return result;
            }

            //  Convert the state to the state abbreviation
            for (let ab in globalStatesList) {
                const fullname = globalStatesList[ab];
                if (ab.toLowerCase() === result.state.toLowerCase()) {
                    result.state = ab;
                    break;
                } else if (fullname.toLowerCase() === result.state.toLowerCase()) {
                    result.state = ab;
                    break;
                }
            }

            result.city   = this.fixTitleCase(result.city);
            result.street = this.fixTitleCase(result.street);

            const addressParts = [];
            if (result.number != null) {
                addressParts.push(result.number);
            }

            //  Convert the house number to a number if possible
            if (typeof result.number !== "number") {
                const tmpHouseNumber = parseInt(result.number, 10);
                if (!isNaN(tmpHouseNumber)) {
                    result.number = tmpHouseNumber;
                }
            }

            if (result.number != null) {
                result.display = result.number + " ";
            } else {
                result.display = "";
            }

            //  Search for unit number type 2
            //  Make sure the unit number is part of the hash.
            const m = result.street.match(/#\s{0,1}([0-9]+)/);
            if (m != null) {
                result.street = result.street.replace(m[0], "");
                result.street = result.street.trim();
                result.unit   = parseInt(m[1], 10);
            }


            // if result.prefix
            //     result.display += " " + result.prefix
            result.display = result.display + result.street;
            if (result.sec_unit_type != null) {
                result.display += ` ${result.sec_unit_type} ${result.sec_unit_num}`;
            }
            if (result.unit != null) {
                result.display += ` #${result.unit}`;
            }

            result.display += `, ${result.city}`;
            result.display += `, ${result.state}`;
            result.display += `, ${result.zip}`;

            //
            //  Create a hash for lookup purposes
            const hash = crypto.createHash('md5');
            hash.update(result.display);
            result.hash = hash.digest('hex');

            this.log.info("Normalizing address", {
                addressText,
                city,
                state,
                zip,
                parserOutput: output,
                result
            });

            return result;
        }
    }

    // -------------------------------------------------------------------------------------------------------------
    // convert text from unknown case to title case, as in
    //
    // @example PHOENIX becomes Phoenix, NEW york becomes New York
    // @param [String] strTitleText text to be converted
    // @return [String] converted text
    //
    fixTitleCase(strTitleText: string) {
        if (strTitleText == null) {
            return "";
        }

        return strTitleText.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    }
}

export default AddressNormalizer;
