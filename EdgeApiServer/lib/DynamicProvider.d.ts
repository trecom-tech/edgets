export declare class DynamicProvider {
    params: any[];
    fn: Function;
    resources: any[];
    constructor();
    onInit(): void;
    serialize(): {
        params: any[];
    };
    initRunOptions(): any;
    verifyParamValue(paramNum: string, value: any): any;
    logFunction(executeOptions: any, ...values: any): boolean;
    doCallFunction(executeOptions: any): Promise<unknown>;
    parseCodeComments(code: any): boolean;
    addParam(name: any, comment: any): boolean;
    createFunctionFromCode(code: any): any;
    doCompileCoffeeToCode(coffeeText: any): any;
    doCompileCoffeeFile(filename: any): Promise<unknown>;
}
export default DynamicProvider;
