class ViewTestLayout7 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        name: "viewSplitter"
        percent: 30
        top: "TestShowSize"
        bottom:
            view: "DynamicTabs"
            name: "viewTabs"
            tabs: [
                view: "TestShowSize"
                keyboard: "1"
                name: "view1"
                title: "Top tab 1"
            ,
                view: "TestShowSize"
                name: "view2"
                keyboard: "2"
                title: "Top tab 2"
                disabled: true
            ,
                view: "DynamicTabs"
                name: "view3"
                title: "Top tab 3"
            ]

    onShowScreen: ()=>
        console.log "ViewTestLayout7 onShowScreen viewSplitter=", @viewSplitter
        console.log "ViewTestLayout7 onShowScreen view1=", @view1
        console.log "ViewTestLayout7 onShowScreen view2=", @view2
        console.log "ViewTestLayout7 onShowScreen viewTabs=", @viewTabs







