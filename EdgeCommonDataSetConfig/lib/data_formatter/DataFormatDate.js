"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for date data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const common = new DataFormatCommon_1.default();
class DataFormatDate extends DataFormatterType_1.default {
    constructor() {
        super();
        this.name = 'date';
        this.width = 65;
        this.input_width = 65;
        this.align = 'left';
    }
    //
    // funtion to open editor as åpickr
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                type: "text",
                class: "dynamic_edit"
            });
            this.appendEditor();
            this.elEditor.on('keydown', (e) => {
                // if editor is closed then close datepicker
                if (!this.editorShowing) {
                    return this.datePicker.close();
                }
            });
        }
        // @ts-ignore
        this.datePicker = new flatpickr(this.elEditor[0], {
            allowInput: true,
            parseDate(dateString) {
                const mom = common.getMoment(dateString, 'MM/DD/YYYY HH:mm:ss');
                if (mom != null) {
                    return mom.toDate();
                }
                return null;
            },
            onChange: (dateObject, dateString) => {
                this.saveValue(dateObject);
                this.editorShowing = false;
                return this.elEditor.hide();
            },
            onOpen: (dateObj, dateStr, instance) => {
                return instance.setDate(new Date(currentValue));
            },
            onClose: () => {
                this.editorShowing = false;
                this.elEditor.hide();
            },
            enableTime: true,
            time_24hr: true
        });
        this.elDataEditor = this.datePicker.calendarContainer;
        this.elEditor.css({
            position: "absolute",
            "z-index": 235001,
            // "z-index" : "auto"
            top,
            left,
            width,
            height
        });
        if (currentValue != null) {
            this.elEditor.val(currentValue);
        }
        else {
            this.elEditor.val("");
        }
        this.elEditor.show();
        return this.elEditor.focus();
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        const format = "MM/DD/YYYY";
        const m = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        return m.format(format);
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        const format = "YYYY-MM-DD";
        const m = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        const dateStr = m.format(format);
        return new Date(dateStr);
    }
    getValueInTooltip(value) {
        const format = "YYYY-MM-DD";
        const m = common.getMoment(value, format);
        if (!m) {
            return "";
        }
        return m.format(format);
    }
}
exports.DataFormatDate = DataFormatDate;
exports.default = DataFormatDate;
//# sourceMappingURL=DataFormatDate.js.map