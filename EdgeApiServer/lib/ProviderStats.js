"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const url = require("url");
const edgecommontimeseriesdata_1 = require("edgecommontimeseriesdata");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonrequest_1 = require("edgecommonrequest");
const ApiProvider_1 = require("./ApiProvider");
class ProviderStats extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
        // init timeseries data, create database if it doesn't exist
        let timeseriesData = edgecommontimeseriesdata_1.default.getInstance();
        timeseriesData.doCreateDatabase('general').then((db) => {
            if (db != null && db !== false) {
                return this.timeseriesData = db;
            }
            else {
                return edgecommonconfig_1.default.status('ProviderStats onInit: No time series database.   Unable to initialize.', db);
            }
        });
        return true;
    }
    doWriteStatisticalData(databaseName, categoryName, dataTags, dataFields) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            if (this.statDatabases == null) {
                this.statDatabases = {};
            }
            if (this.statDatabases[databaseName] == null) {
                let timeseriesData = edgecommontimeseriesdata_1.default.getInstance();
                this.statDatabases[databaseName] = yield timeseriesData.doCreateDatabase(databaseName);
            }
            if (this.statDatabases[databaseName] != null && this.statDatabases[databaseName]) {
                let result = yield this.statDatabases[databaseName].doSaveMeasurement(categoryName, dataTags, dataFields);
                resolve(result);
            }
            resolve(null);
        }));
    }
    // Connect to RabbitMQ Using the management API and return Queue status
    doGetMessageQueueStats() {
        let mqHost = edgecommonconfig_1.default.getCredentials('mqAdmin');
        let urlParts = url.parse(mqHost);
        let { auth } = urlParts;
        let host = urlParts.hostname;
        let port = urlParts.port || 15672;
        let dataUrl = `http://${auth}@${host}:${port}/api/queues`;
        let qs = {};
        let r = new edgecommonrequest_1.default();
        return r.doGetLink(dataUrl, qs);
    }
    getBaseName() {
        return 'stats';
    }
    doGetStats(intervalName, categoryName) {
        return null;
    }
    // Add many factors to the stats
    doAddStatsMany(intervalName, categoryName, keys) {
        for (let keyName in keys) {
            let keyValue = keys[keyName];
            this.doAddStats(categoryName, keyName, keyValue);
        }
        return true;
    }
    // Add a single record to stats
    doAddStats(intervalName, categoryName, counterName, count = 1) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status(`doAddStats intervalName=${intervalName}, category=${categoryName}, counter=${counterName}, count=${count}`);
            // intervalName is usually "today" or something similar to make sure we're
            // making it easier for mongo but EdgeCommonTimeSeries with Influx is better
            // and doesn't care about that so we can ignore the interval.
            // tags are indexed
            let tags = { key: counterName };
            // no fields of data
            let fields = { num_proc: count || 1 };
            if (this.timeseriesData == null) {
                edgecommonconfig_1.default.status('ProviderStats doAddStats not connected');
                console.log('ProviderStats doAddStats not connected');
                return true;
            }
            // TODO ROY: doWriteMeasurement is not defined on timeseries
            return this.timeseriesData.doSaveMeasurement(categoryName, tags, fields);
            // return this.timeseriesData.doWriteMeasurement(categoryName, counterName, counterName, fields);
        });
    }
}
exports.default = ProviderStats;
;
//# sourceMappingURL=ProviderStats.js.map