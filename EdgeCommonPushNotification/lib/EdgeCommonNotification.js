"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgecommonrequest_1 = require("edgecommonrequest");
const apn = require('apn');
const FCM = require('fcm-push');
const twilio = require('twilio');
class EdgeCommonNotification {
    constructor() {
        this.contacts = [];
        this.guid = EdgeCommonNotification.generateGUID();
        this.logger = edgecommonconfig_1.config.getLogger("PushNotification");
    }
    addContactDetails(userContactDetails = []) {
        for (const contactDetail of userContactDetails) {
            this.contacts.push(contactDetail);
        }
        return true;
    }
    doSendMessage(messageDetails) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const contact of this.contacts) {
                switch (contact.type) {
                    case 'apple':
                        yield this.doSendForApple(contact.account.deviceToken, messageDetails);
                        break;
                    case 'google':
                        yield this.doSendForGoogle(contact.account.deviceToken, messageDetails);
                        break;
                    case 'whatsapp':
                        yield this.doSendWhatsApp(contact.account.phoneNumber, messageDetails);
                        break;
                    case 'sms':
                        yield this.doSendSMS(contact.account.phoneNumber, messageDetails);
                        break;
                    case 'email':
                    default:
                        break;
                }
            }
            return true;
        });
    }
    /*
    * apple push notification
    * */
    doSendForApple(deviceToken, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const appleCreds = edgecommonconfig_1.config.getCredentials('appleCreds');
                let notification = new apn.Notification(message);
                const provider = new apn.Provider(appleCreds);
                if (!message.expiry)
                    notification.expiry = Math.floor(Date.now() / 1000) + 3600 * 7; // Expires 7 hour from now.
                if (!message.urlArgs)
                    notification.urlArgs = [];
                if (!message.sound)
                    notification.sound = "chime.caf";
                if (!message.badge)
                    notification.badge = 10;
                if (!message.topic) {
                    this.logger.error(this.guid, 'missing topic');
                }
                if (!message.title) {
                    this.logger.error(this.guid, 'missing title');
                }
                if (!message.alert) {
                    notification.alert = '';
                    this.logger.error(this.guid, 'missing body');
                }
                const response = yield provider.send(notification, deviceToken);
                if (response.sent.length) {
                    this.logger.info(this.guid, {
                        type: 'apple',
                        deviceToken: deviceToken,
                        message: message,
                        response: response.sent,
                        created: new Date(),
                    });
                    provider.shutdown();
                    return true;
                }
                else {
                    this.logger.error(this.guid, {
                        type: 'apple',
                        deviceToken: deviceToken,
                        message: message,
                        response: response.failed,
                        created: new Date(),
                    });
                    return false;
                }
            }
            catch (err) {
                this.logger.error(this.guid, err);
                return false;
            }
        });
    }
    /*
    * google push notification
    * */
    doSendForGoogle(deviceToken, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { serverKey } = edgecommonconfig_1.config.getCredentials('googleCreds');
                const fcm = new FCM(serverKey);
                const msg = {
                    to: deviceToken,
                    collapse_key: 'green',
                    data: {},
                    notification: {
                        title: message.title,
                        body: message.alert,
                    },
                };
                const response = yield fcm.send(msg);
                this.logger.info(this.guid, {
                    type: 'google',
                    deviceToken: deviceToken,
                    message: message,
                    response: response,
                    created: new Date(),
                });
                return true;
            }
            catch (err) {
                this.logger.error(this.guid, {
                    type: 'google',
                    deviceToken: deviceToken,
                    message: message,
                    response: err,
                    created: new Date(),
                });
                return false;
            }
        });
    }
    /*
    * WhatsApp message
    * */
    doSendWhatsApp(whatsAppNumber, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { accountSid, authToken, twilioNumber } = edgecommonconfig_1.config.getCredentials('twilioCreds');
                let tNumber = yield EdgeCommonNotification.validatePhoneNumber(twilioNumber);
                whatsAppNumber = yield EdgeCommonNotification.validatePhoneNumber(whatsAppNumber);
                const client = yield twilio(accountSid, authToken);
                const response = yield client.messages.create({
                    body: message.alert,
                    from: `whatsapp:${tNumber}`,
                    to: `whatsapp:${whatsAppNumber}`
                });
                const request = new edgecommonrequest_1.EdgeRequest();
                const baseUrl = response._version._domain.baseUrl + response.uri;
                const baseToken = response._version._domain.twilio.httpClient.lastRequest.headers.Authorization;
                request.addHeader('Authorization', baseToken);
                const result = yield request.doGetLink(`${baseUrl}`);
                if (result.error_code) {
                    this.logger.error(this.guid, {
                        type: 'whatsapp',
                        number: whatsAppNumber,
                        message: message,
                        response: result,
                        status: result.status,
                        created: new Date(),
                    });
                    return false;
                }
                else {
                    this.logger.info(this.guid, {
                        type: 'whatsapp',
                        number: whatsAppNumber,
                        message: message,
                        response: result,
                        status: result.status,
                        created: new Date(),
                    });
                    return true;
                }
            }
            catch (err) {
                this.logger.error(this.guid, {
                    type: 'whatsapp',
                    whatsAppNumber: whatsAppNumber,
                    message: message,
                    response: err,
                    status: 'failed',
                    created: new Date(),
                });
                return false;
            }
        });
    }
    /*
    * Send SMS with twilio
    * */
    doSendSMS(phoneNumber, message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { accountSid, authToken, twilioNumber } = edgecommonconfig_1.config.getCredentials('twilioCreds');
                const client = twilio(accountSid, authToken);
                const response = yield client.messages.create({
                    body: message.alert,
                    from: twilioNumber,
                    to: phoneNumber,
                });
                const request = new edgecommonrequest_1.EdgeRequest();
                const baseUrl = response._version._domain.baseUrl + response.uri;
                const baseToken = response._version._domain.twilio.httpClient.lastRequest.headers.Authorization;
                request.addHeader('Authorization', baseToken);
                const result = yield request.doGetLink(`${baseUrl}`);
                if (result.error_code) {
                    this.logger.error(this.guid, {
                        type: 'sms',
                        number: phoneNumber,
                        message: message,
                        response: result,
                        status: result.status,
                        created: new Date(),
                    });
                    return false;
                }
                else {
                    this.logger.info(this.guid, {
                        type: 'sms',
                        number: phoneNumber,
                        message: message,
                        response: result,
                        status: result.status,
                        created: new Date(),
                    });
                    return true;
                }
            }
            catch (err) {
                this.logger.error(this.guid, {
                    type: 'sms',
                    number: phoneNumber,
                    message: message,
                    error: err,
                    status: 'failed',
                    created: new Date(),
                });
                return false;
            }
        });
    }
    /*
    * e164 formatted phone number
    * */
    static validatePhoneNumber(phoneNumber) {
        return phoneNumber.toString().substr(0, 1) === '+' ? phoneNumber : `+${phoneNumber}`;
    }
    /*
    * generate GUID to search up logs
    * */
    static generateGUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16 | 0;
            let v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
exports.EdgeCommonNotification = EdgeCommonNotification;
exports.default = EdgeCommonNotification;
//# sourceMappingURL=EdgeCommonNotification.js.map