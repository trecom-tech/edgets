import { createPool, Types }    from 'mysql';
import { config }               from 'edgecommonconfig';

const DEFAULT_HOST                 = 'localhost';
const DEFAULT_PORT                 = 3306;
const DEFAULT_CONNECTION_LIMIT     = 3;
const DEFAULT_CONNECT_TIMEOUT      = 2000;
const DEFAULT_QUEUE_LIMIT          = 0;
const DEFAULT_TIMEZONE             = 'UTC';
const DEFAULT_DEBUG                = false;
const DEFAULT_WAIT_FOR_CONNECTIONS = true;

export class EdgeCommonMySQL {
    
    private configName      : string;

    private creds           : any;

    private pool            : any;

    private logger          : any;

    /**
     * Creates an instance
     * @param configName name of config as defined in the credential
     * @param options options for mysql connection
     */
    constructor(configName: string) {
        this.configName    = configName;
        this.creds         = config.getCredentials(configName);
        this.logger        = config.getLogger('MySQL');
        this.pool          = null;
    }

    /**
     * Execute a SQL query
     * @param sql Query string
     * @param callbackOnRow callback function
     */
    public doRunQuery(sql: string, callbackOnRow: any) {
        if (!callbackOnRow) { 
            return false; 
        }

        if (typeof callbackOnRow !== 'function') {
            return false;
        }

        if (!this.pool) {
            this.pool = this.createPool();
        }

        this.pool.query(sql, (err: any, results: any, fields: any) => {
            if (err) {
                this.logger.error('doRunQuery', {sql: sql, err: err});
                callbackOnRow(err, results, fields);
            }

            if (results instanceof Array) {
                if (results.length === 0) {
                    callbackOnRow(err, results, fields);
                } else {
                    for (let result of results) {
                        for (let field of fields) {
                            if (field.type === Types.TINY) {
                                result[field.name] = result[field.name] === 1;
                            }
                        }

                        callbackOnRow(err, result, fields);
                    }
                }
            } else if (results.affectedRows){
                callbackOnRow(err, results, fields);
            }
        });
    }

    /**
     * Create a connection pool
     */
    private createPool() {
        if (!this.creds) {
            config.status(`EdgeCommonMySQL no credentials for ${this.configName}`);
            throw new Error(`EdgeCommonMySQL no credentials for ${this.configName}`);
        }

        if (!this.creds.options) {
            this.creds.options = {};
        }

        let options: any           = {};
        options.host               = this.creds.host || DEFAULT_HOST;
        options.port               = this.creds.port || DEFAULT_PORT;
        options.user               = this.creds.user;
        options.password           = this.creds.password;
        options.database           = this.creds.database;
        options.connectionLimit    = this.creds.options.connectionLimit || DEFAULT_CONNECTION_LIMIT;
        options.connectTimeout     = this.creds.options.connectTimeout || DEFAULT_CONNECT_TIMEOUT;
        options.queueLimit         = this.creds.options.queueLimit || DEFAULT_QUEUE_LIMIT;
        options.debug              = this.creds.options.debug || DEFAULT_DEBUG;
        options.timezone           = this.creds.options.timezone || DEFAULT_TIMEZONE; // acceptable values: local, Z, +HH:MM, -HH:MM
        options.waitForConnections = this.creds.options.waitForConnections || DEFAULT_WAIT_FOR_CONNECTIONS;

        if (!options.user) {
            throw new Error('EdgeCommonMySQL username must be specified');
        } else if (!options.password) {
            throw new Error('EdgeCommonMySQL password must be specified');
        } else if (!options.database) {
            throw new Error('EdgeCommonMySQL database must be specified');
        }

        options.timezone = options.timezone === 'UTC' ? '+00:00' : options.timezone;

        return createPool(options);
    }

}

export default EdgeCommonMySQL;