import { config } from 'edgecommonconfig';
import * as zlib  from 'zlib';
import * as mqtt  from 'async-mqtt';

// MessageQueue controller for MQTT
class EdgeMQTTPubSub {
    // MQTT client, we declared it to static because we dont want multiple connections
    static mqttClient: mqtt.AsyncMqttClient;

    // Optional client id, we pass it when we initialize mqtt
    clientId: string;

    constructor(clientId: string = 'edgemqttclient') {
        this.clientId = clientId;
    }

    // method for subscribing to a topic
    async consume(topicName: string, callback: any) {
        config.status(`EdgeMQTTMessageQueue consume Starting`);
        await this.doConnect();
        await EdgeMQTTPubSub.mqttClient.subscribe(topicName, { qos: 1 });
        EdgeMQTTPubSub.mqttClient.on('message', (topic: string, message: any) => {
            if (topicName === topic) {
                callback(message);
            }
        });
    }

    // method for connecting
    doConnect() {
        return new Promise((resolve, reject) => {
            if (!EdgeMQTTPubSub.mqttClient || !EdgeMQTTPubSub.mqttClient.connected) {
                // config.getCredentials('mqHost');
                config.status(`EdgeMQTTMessageQueue connecting to client`);
                const server              = config.getCredentials('mqttHost');
                EdgeMQTTPubSub.mqttClient = mqtt.connect(server, {
                    clientId       : this.clientId,
                    protocolId     : 'MQIsdp',
                    protocolVersion: 3,
                    connectTimeout : 1000
                });
                EdgeMQTTPubSub.mqttClient.on('connect', () => {
                    config.status(`EdgeMQTTMessageQueue client connected`);
                    resolve();
                });
                EdgeMQTTPubSub.mqttClient.on('error', (err: Error) => {
                    config.status(`EdgeMQTTMessageQueue client connection faileed`);
                    reject(err);
                });
            } else {
                resolve();
            }
        });
    }

    //
    //  Disconnect / disable the message queue
    async doClose() {
        config.status(`EdgeMQTTMessageQueue client disconnecting`);
        if (EdgeMQTTPubSub.mqttClient) {
            await EdgeMQTTPubSub.mqttClient.end(true);
            config.status(`EdgeMQTTMessageQueue client disconnected`);
        }
    }


    // Publish string to a topic
    async doPublish(topicName: string, str: string) {
        await this.doConnect();
        return new Promise((resolve, reject) => {

            if (typeof str === 'object') {
                str = JSON.stringify(str);
            }


            zlib.deflate(str, async (err: Error, buffer: Buffer) => {
                if (err != null) {
                    config.error('zlib.deflate error:', err);
                    reject(err);
                } else {
                    config.status(`EdgeMQTTMessageQueue Publish ${buffer.length} bytes (from ${str.length})`);
                    try {
                        await EdgeMQTTPubSub.mqttClient.publish(topicName, buffer);
                        resolve(true);
                    } catch (err) {
                        reject(err);
                    }

                }
            });
        });
    }

    //
    //  Write either JSON text or a binary buffer to the message queue
    //  returns a promise write the write is complete.
    //
    async doSubmitBuffer(topicName: string, str: Buffer) {
        await this.doConnect();
        return new Promise((resolve, reject) => {
            // Compress data and submit it
            zlib.deflate(str, async (err: Error, buffer: Buffer) => {
                if (err != null) {
                    config.error('zlib.deflate error:', err);
                    reject(err);
                } else {
                    config.status(`EdgeMQTTMessageQueue doSubmitBuffer Sending Compressed ${str.length} => ${buffer.length}`);

                    try {
                        await EdgeMQTTPubSub.mqttClient.publish(topicName, buffer);
                        resolve(true);
                    } catch (err) {
                        reject(err);
                    }
                }
            });
        });
    }
}

export const EdgeMQTTController = new EdgeMQTTPubSub();
export default EdgeMQTTController;
