import EdgeDataSetManager from 'edgedatasetmanager';
import ApiProvider        from './ApiProvider';

export default class ProviderLogs extends ApiProvider {
    dmLogs: EdgeDataSetManager;
    dbLogs: EdgeDataSetManager;

    constructor(prototype: ApiProvider) {
        super(prototype);
    }

    async onInit() {
        // Initialize the provider
        this.dmLogs          = new EdgeDataSetManager('logs');

        // TODO roy: dbLogs is not defined.
        // let c = this.dbLogs.collection("jobrun");
        // await c.doInternalGetCollection();
        // return c;
        return true;
    }

    getBaseName() {
        return 'logs';
    }

    // The /jobrun collection stores the output from jobs, this will return
    // the last number of records from that log.
    doGetLatestJobs(limit: number) {

        if (limit == null || typeof limit !== 'number') {
            limit = 100;
        }
        return this.dmLogs.doGetItems('/jobrun', {}, { _id: -1 }, 0, limit);
    }
};
