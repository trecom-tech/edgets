#!/bin/bash

npm version minor --force
npm run build
git add lib/*js lib/*map
git commit -a -m "$*"
git push
