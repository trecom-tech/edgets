$ ->

	samples = [
		{"name":"Αρχίδαμος","surname":"Ζάρκος","gender":"male#0000ff","region":"Greece","age":34,"title":"mr","phone":"(883) 582 2169","birthday":{"dmy":"23\/07\/1983","mdy":"07\/23\/1983","raw":427796402},"email":"Αρχίδαμος-83@example.com","password":"Ζάρκος83)&","credit_card":{"expiration":"11\/24","number":"7202-9110-3508-9458","pin":3662,"security":760},"photo":"https:\/\/uinames.com\/api\/photos\/male\/6.jpg"},
		{"name":"Eva","surname":"Lucan","gender":"female","region":"Romania","age":26,"title":"ms","phone":"(802) 691 3416","birthday":{"dmy":"20\/05\/1991","mdy":"05\/20\/1991","raw":674727611},"email":"eva.lucan@example.com","password":"Lucan91$$","credit_card":{"expiration":"10\/21","number":"5550-3037-6060-8527","pin":6409,"security":483},"photo":"https:\/\/uinames.com\/api\/photos\/female\/24.jpg"},
		{"name":"Nicodim","surname":"Băsescu","gender":"male#0000ff","region":"Romania","age":33,"title":"mr","phone":"(983) 669 2793","birthday":{"dmy":"21\/06\/1984","mdy":"06\/21\/1984","raw":456660546},"email":"nicodim_84@example.com","password":"Băsescu84{&","credit_card":{"expiration":"10\/24","number":"2987-3930-7060-3734","pin":1140,"security":784},"photo":"https:\/\/uinames.com\/api\/photos\/male\/8.jpg"},
		{"name":"Ηγήσιππος","surname":"Νικολάκος","gender":"male#0000ff","region":"Greece","age":21,"title":"mr","phone":"(177) 839 8072","birthday":{"dmy":"08\/08\/1996","mdy":"08\/08\/1996","raw":839545452},"email":"Ηγήσιππος96@example.com","password":"Νικολάκος96_$","credit_card":{"expiration":"2\/18","number":"2521-2974-7527-2979","pin":5198,"security":614},"photo":"https:\/\/uinames.com\/api\/photos\/male\/19.jpg"},
		{"name":"侯","surname":"哲","gender":"male#0000ff","region":"China","age":21,"title":"mr","phone":"(365) 358 8317","birthday":{"dmy":"07\/08\/1996","mdy":"08\/07\/1996","raw":839468487},"email":"侯哲@example.com","password":"哲96)$","credit_card":{"expiration":"8\/22","number":"1489-6416-2796-5451","pin":5387,"security":993},"photo":"https:\/\/uinames.com\/api\/photos\/male\/15.jpg"},
		{"name":"Elkin","surname":"Castillo","gender":"male#0000ff","region":"Colombia","age":27,"title":"mr","phone":"(690) 177 1297","birthday":{"dmy":"10\/02\/1990","mdy":"02\/10\/1990","raw":634634710},"email":"elkin90@example.com","password":"Castillo90#%","credit_card":{"expiration":"3\/20","number":"5119-9265-3296-7847","pin":3151,"security":524},"photo":"https:\/\/uinames.com\/api\/photos\/male\/7.jpg"},
		{"name":"Bimal","surname":"Luitel","gender":"male#0000ff","region":"Nepal","age":26,"title":"mr","phone":"(620) 164 9180","birthday":{"dmy":"03\/01\/1991","mdy":"01\/03\/1991","raw":662937919},"email":"bimal_luitel@example.com","password":"Luitel91^~","credit_card":{"expiration":"3\/21","number":"5661-2435-6031-9505","pin":9274,"security":260},"photo":"https:\/\/uinames.com\/api\/photos\/male\/19.jpg"},
		{"name":"Viorela","surname":"Davidovici","gender":"female","region":"Romania","age":34,"title":"ms","phone":"(393) 509 8983","birthday":{"dmy":"27\/06\/1983","mdy":"06\/27\/1983","raw":425579641},"email":"viorela83@example.com","password":"Davidovici83}","credit_card":{"expiration":"8\/20","number":"7663-7034-8773-9867","pin":1891,"security":203},"photo":"https:\/\/uinames.com\/api\/photos\/female\/19.jpg"},
		{"name":"Lana","surname":"Louis","gender":"female","region":"France","age":28,"title":"mrs","phone":"(234) 242 7664","birthday":{"dmy":"09\/08\/1989","mdy":"08\/09\/1989","raw":618640830},"email":"lana-louis@example.com","password":"Louis89}%","credit_card":{"expiration":"9\/22","number":"2153-5022-2074-3706","pin":9783,"security":411},"photo":"https:\/\/uinames.com\/api\/photos\/female\/6.jpg"},
		{"name":"Alexandrina","surname":"Burada","gender":"female","region":"Romania","age":32,"title":"ms","phone":"(210) 869 4993","birthday":{"dmy":"05\/11\/1985","mdy":"11\/05\/1985","raw":500030815},"email":"alexandrina-85@example.com","password":"Burada85)+","credit_card":{"expiration":"4\/22","number":"1447-5976-8081-9275","pin":1150,"security":398},"photo":"https:\/\/uinames.com\/api\/photos\/female\/5.jpg"},
		{"name":"Selim","surname":"Sağlam","gender":"male#0000ff","region":"Turkey","age":30,"title":"mr","phone":"(971) 420 4965","birthday":{"dmy":"07\/04\/1987","mdy":"04\/07\/1987","raw":544772710},"email":"selim87@example.com","password":"Sağlam87}%","credit_card":{"expiration":"9\/25","number":"4248-6757-1098-1670","pin":2992,"security":918},"photo":"https:\/\/uinames.com\/api\/photos\/male\/11.jpg"},
		{"name":"Bohumila","surname":"Machata","gender":"female","region":"Slovakia","age":29,"title":"mrs","phone":"(465) 346 4361","birthday":{"dmy":"25\/09\/1988","mdy":"09\/25\/1988","raw":591233887},"email":"bohumila-88@example.com","password":"Machata88+=","credit_card":{"expiration":"6\/18","number":"4051-4210-9202-2661","pin":6480,"security":728},"photo":"https:\/\/uinames.com\/api\/photos\/female\/1.jpg"},
		{"name":"Beniamin","surname":"Mocioni","gender":"male#0000ff","region":"Romania","age":25,"title":"mr","phone":"(980) 546 9941","birthday":{"dmy":"29\/02\/1992","mdy":"02\/29\/1992","raw":699347550},"email":"beniamin-92@example.com","password":"Mocioni92}}","credit_card":{"expiration":"12\/24","number":"9934-5768-4414-1000","pin":8925,"security":610},"photo":"https:\/\/uinames.com\/api\/photos\/male\/11.jpg"},
		{"name":"Matilda","surname":"Almașanu","gender":"female","region":"Romania","age":30,"title":"ms","phone":"(469) 620 1220","birthday":{"dmy":"20\/01\/1987","mdy":"01\/20\/1987","raw":538201700},"email":"matilda87@example.com","password":"Almașanu87$$","credit_card":{"expiration":"1\/20","number":"1797-4475-2415-3344","pin":3425,"security":197},"photo":"https:\/\/uinames.com\/api\/photos\/female\/10.jpg"},
		{"name":"Marin","surname":"Stolnici","gender":"male#0000ff","region":"Romania","age":25,"title":"mr","phone":"(372) 509 8075","birthday":{"dmy":"06\/07\/1992","mdy":"07\/06\/1992","raw":710408248},"email":"marin-92@example.com","password":"Stolnici92__","credit_card":{"expiration":"8\/20","number":"6447-8854-9619-7310","pin":3510,"security":278},"photo":"https:\/\/uinames.com\/api\/photos\/male\/8.jpg"},
		{"name":"Ľuboš","surname":"Dusík","gender":"male#0000ff","region":"Slovakia","age":30,"title":"mr","phone":"(639) 795 6328","birthday":{"dmy":"23\/01\/1987","mdy":"01\/23\/1987","raw":538411611},"email":"Ľuboš_87@example.com","password":"Dusík87*&","credit_card":{"expiration":"5\/20","number":"9885-9201-4080-6951","pin":2025,"security":451},"photo":"https:\/\/uinames.com\/api\/photos\/male\/16.jpg"},
		{"name":"Phillip","surname":"Knight","gender":"male#0000ff","region":"United States","age":33,"title":"mr","phone":"(631) 312 5747","birthday":{"dmy":"16\/12\/1984","mdy":"12\/16\/1984","raw":472046118},"email":"phillip_84@example.com","password":"Knight84$#","credit_card":{"expiration":"10\/23","number":"9900-7399-8225-3223","pin":9931,"security":797},"photo":"https:\/\/uinames.com\/api\/photos\/male\/6.jpg"},
		{"name":"Grace","surname":"Beck","gender":"female","region":"United States","age":27,"title":"ms","phone":"(773) 633 8731","birthday":{"dmy":"21\/06\/1990","mdy":"06\/21\/1990","raw":646020433},"email":"grace-beck@example.com","password":"Beck90(","credit_card":{"expiration":"11\/22","number":"7708-1347-2838-9115","pin":1798,"security":500},"photo":"https:\/\/uinames.com\/api\/photos\/female\/13.jpg"},
		{"name":"Διόνυσος","surname":"Ταμτάκος","gender":"male#0000ff","region":"Greece","age":29,"title":"mr","phone":"(772) 302 6013","birthday":{"dmy":"08\/04\/1988","mdy":"04\/08\/1988","raw":576512658},"email":"Διόνυσος88@example.com","password":"Ταμτάκος88(=","credit_card":{"expiration":"3\/22","number":"8509-3550-1058-4334","pin":4692,"security":884},"photo":"https:\/\/uinames.com\/api\/photos\/male\/9.jpg"},
		{"name":"Suzana","surname":"Caranica","gender":"female","region":"Romania","age":29,"title":"ms","phone":"(243) 320 1619","birthday":{"dmy":"26\/05\/1988","mdy":"05\/26\/1988","raw":580698685},"email":"suzana_88@example.com","password":"Caranica88~{","credit_card":{"expiration":"7\/20","number":"5748-3794-1508-5870","pin":7872,"security":501},"photo":"https:\/\/uinames.com\/api\/photos\/female\/20.jpg"},
		{"name":"Nicuță","surname":"Besoiu","gender":"male#0000ff","region":"Romania","age":23,"title":"mr","phone":"(959) 886 8006","birthday":{"dmy":"31\/01\/1994","mdy":"01\/31\/1994","raw":760022764},"email":"nicuță_94@example.com","password":"Besoiu94$#","credit_card":{"expiration":"11\/25","number":"9682-5609-8936-4009","pin":9231,"security":515},"photo":"https:\/\/uinames.com\/api\/photos\/male\/19.jpg"},
		{"name":"Eftimia","surname":"Sabău","gender":"female","region":"Romania","age":24,"title":"ms","phone":"(677) 564 5091","birthday":{"dmy":"28\/03\/1993","mdy":"03\/28\/1993","raw":733343228},"email":"eftimia93@example.com","password":"Sabău93@}","credit_card":{"expiration":"12\/24","number":"8097-9567-8950-6873","pin":1656,"security":356},"photo":"https:\/\/uinames.com\/api\/photos\/female\/1.jpg"},
		{"name":"Bipana","surname":"Kayastha","gender":"female","region":"Nepal","age":25,"title":"ms","phone":"(552) 598 7080","birthday":{"dmy":"23\/02\/1992","mdy":"02\/23\/1992","raw":698890177},"email":"bipana92@example.com","password":"Kayastha92+&","credit_card":{"expiration":"7\/23","number":"5591-5644-4762-2645","pin":8422,"security":242},"photo":"https:\/\/uinames.com\/api\/photos\/female\/21.jpg"},
		{"name":"Marijo","surname":"Gudelj","gender":"male#0000ff","region":"Bosnia and Herzegovina","age":30,"title":"mr","phone":"(617) 916 8939","birthday":{"dmy":"11\/06\/1987","mdy":"06\/11\/1987","raw":550466110},"email":"marijo-87@example.com","password":"Gudelj87@%","credit_card":{"expiration":"4\/22","number":"8533-5733-9151-4839","pin":3386,"security":766},"photo":"https:\/\/uinames.com\/api\/photos\/male\/1.jpg"}]

	##|
	##| Add the sample data to the datamap
	id = 0
	for rec in samples
		rec.birthday = new Date(rec.birthday.raw * 1000)
		DataMap.addDataUpdateTable "test_data", ++id, rec

	##|
	##| Change the order
	DataMap.changeColumnAttribute "test_data", "birthday", "order", 0
	DataMap.changeColumnAttribute "test_data", "age", "order", 1

	##|
	##| Make birthday an edit field
	DataMap.changeColumnAttribute "test_data", "birthday", "editable", true

	##|
	##| Make gender have only 3 options
	DataMap.changeColumnAttribute "test_data", "gender", "type", "enum"
	DataMap.changeColumnAttribute "test_data", "gender", "options", ["male#0000ff", "female", "unspecified"]
	DataMap.changeColumnAttribute "test_data", "gender", "editable", true

	##|
	##| Create a render function for "age" that calculates the age in realtime
	##| based on the birthday.
	DataMap.changeColumnAttribute "test_data", "age", "render", (val, tableName, fieldName, id, row)=>
		##|  ID is the record id, get the birthday
		birthday = DataMap.getDataField "test_data", id, "birthday"
		age = new Date().getYear() - birthday.getYear()
		return age
	##
	## Create an edit function for "password"
	DataMap.changeColumnAttribute "test_data", "password", "editable", true
	DataMap.changeColumnAttribute "test_data", "password", "editFunction", (el, val, path, saveCallback) =>

		console.log "Edit function on Password!!!", val, path
		m = new ModalViewDialog
			showOnCreate : false
			content      :  "Write the new Password in the 2 boxes."
			title        : "Change Password"
			ok           : "Save"

		m.setView "PasswordChange", (viewPasswordChange) ->
			viewPasswordChange.setOriginPassword sha256(val + ViewPasswordChange.HASH_KEY_TEXT)
			m.viewForm = viewPasswordChange.getViewForm()
			viewPasswordChange.setPath path
			true
		, { hasOriginPassword: true }

		m.show() 

	##|
	##|  For testing purposes
	DataMap.getDataMap().on "table_change", (tableName, config)->
		console.log "NEW CONFIG for #{tableName}:", config

	addHolder()
	.setView "Table", (view)->
		table = view.addTable "test_data"


	# setTimeout ()=>
	# 	doPopupView "ShowTableEditor", "Editing table: test_data", "tableEditor", 1300, 800, (view)=>
	# 		view.showTableEditor "test_data"
	# , 1000