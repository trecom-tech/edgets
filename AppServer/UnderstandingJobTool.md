# JobTool

The [Job Tool](src/JobTool.coffee) command line tool will help deal with jobs
by updating or interacting with the os "jobs" collection in the database.

### --list

List the available jobs

### --add

Add a job using the command line "--add

* --title "Title of the job"
* --add "hourly|daily|always"
* --hours "minimum frequency between jobs in either hours or days"
* --cwd "Path to change into based on install path"
* Everything else is the command line of the job to run

Examples

Add a new job that runs every 30 minutes if possible.   Cd into "Tools" before running and execute the command line "coffee ImportMySQLProperty.coffee".  Record the job as "Import v2 Properties"

    c JobTool --title "Import v2 Properties" --add "hourly" --hours 0.5 --cwd "Tools" coffee ImportMySQLProperty.coffee


### --update

You can update a job field using

* --update <job id>
* --field <field name to change>
* --value <new value>

Example:

    c JobTool.coffee --update 724b25a67d4bddd681a6bbf12113b55926466189 --field enabled --value false
    Saving os:/job/724b25a67d4bddd681a6bbf12113b55926466189 : { enabled: false }
    Save result: [ { kind: 'E', lhs: true, rhs: false, path: [ 'enabled' ] } ]

Notice when this runs I see the result has a diff record from the data set manager because the path enabled was changed.  If we run the
command again with the same exact values we should get no diff record because there was no change.

    c JobTool.coffee --update 724b25a67d4bddd681a6bbf12113b55926466189 --field enabled --value false
    Saving os:/job/724b25a67d4bddd681a6bbf12113b55926466189 : { enabled: false }
    Save result: []

Changing it back and we get a diff record showing that the old value (false) was replaced

    c JobTool.coffee --update 724b25a67d4bddd681a6bbf12113b55926466189 --field enabled --value t
    Saving os:/job/724b25a67d4bddd681a6bbf12113b55926466189 : { enabled: true }
    Save result: [ { kind: 'E', lhs: false, rhs: true, path: [ 'enabled' ] } ]
