##|
##| Global object that authenticates the user and manages in memory data

class TableDefinitionLoader

	constructor: ()->

	checkUserPermission: (strName) =>   
        ##|
        ##|  TODO:  Check roles
		false

	doConnect: ()=>

		newPromise ()=>

			yield globalBusyDialog.waitFor "Connecting to server", sock.doConnect()

			@version = yield sock.os_doGetServerInformation()
			console.log "TableDefinitionLoader doConnect Version=", @version
			@version.version_date = moment(@version.version_date).format("dddd, MMMM Do YYYY, h:mm:ss a")

			strWelcome = "<table>"
			strWelcome += "<tr><td>Connected to server: </td><td> <b>#{@version.host}</b> </td></tr>"
			strWelcome += "<tr><td>Portal Version: </td><td> <b>#{@version.version}</b> </td></tr>"
			strWelcome += "<tr><td>Portal Build Date: </td><td> <b>#{@version.version_date}</b></td></tr>"
			strWelcome += "</table><br>"
			strWelcome += "<ul>"
			if @version? and @version.version_text?
				for line in @version.version_text.reverse()
					strWelcome += "<li> <i class='fa fa-star'></i> #{line} </li>"
			strWelcome += "</ul>"

			return true

	doAuthenticate: (txtUsername, txtPassword)=>

		newPromise ()=>

			@localStoredAuth.result = yield globalBusyDialog.waitFor "Authenticating", sock.auth_doAuth(txtUsername, txtPassword)
			if !@localStoredAuth.result? or @localStoredAuth.result.error?
				return @localStoredAuth.result.error

			##|
			##|  User is authenticated
			sock.user = @localStoredAuth.result
			return null
