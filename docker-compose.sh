#!/usr/bin/env bash

export ROOT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
export ELASTIC_HOST=http://elasticsearch:9200
export NODE_ENV=test
export DB_HOST=dockerized_mongo
export MQTT_HOST=mqtt://localhost:1883
export AMQP_HOST=amqp://127.0.0.1:5672
export INFLUX_HOST=influxdb
export INFLUXDB_DATA_ENGINE=tsm1

docker-compose -f $ROOT_PATH/EdgeCommonElastic/docker-compose.yml \
-f $ROOT_PATH/EdgeCommonMessageQueue/docker-compose.yml \
-f $ROOT_PATH/EdgeCommonTimeseriesData/docker-compose.yml \
-f $ROOT_PATH/EdgeDataSetManager/docker-compose.yml \
"$@"
