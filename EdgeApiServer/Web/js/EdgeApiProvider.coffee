#
# This class is a general interface that can hold a list of dynamic
# API calls and then execute those calls as needed
#

class EdgeApiProviderStream

	constructor: (provider, @stream_id)->
		@allData = []
		@isFinished = false
		provider.on @stream_id, @onDataCallback

	onData: (record)=>
		@allData.push record
		return false

	onFinished: ()=>
		@isFinished = true
		return false

	#
	# Create a promise that waits until all results are available
	doWaitUntilFinished: ()=>
		new Promise (resolve, reject)=>
			if @isFinished
				resolve(@allData)
				return true

			@onFinished = ()=>
				resolve(@allData)

	onDataCallback: (data)=>

		try

			if data? and typeof data == "string" and data == "END"
				@onFinished()
				return true

			@onData(data)

		catch e

			console.log "Exception in EdgeApiProviderStream::onDataCallback, data=", data, "Exception=", e

		true

class EdgeApiProvider

	constructor: ()->

		#
		# Contains one record for every UUID pending a response
		@dynamicResolveList  = {}
		@dynamicRejectList   = {}

		# Contains one record with the name of every call,
		# future use for call timeouts/retry
		@dynamicCallPending  = {}

		#
		# Contains one record for all available calls
		# with the call name index and the param list
		@dynamicApiCallsList = {}

		#
		# One record with a callback function to execute
		# when a command is received
		@commandCallback     = {}

		#
		# Data for the "register" function for all commands
		# so that we can reconnect to the server and send them
		@registeredList = []

		@uuidCounter = 0

		isVerbose   = true

	fastUUID: () ->
		++@uuidCounter
		uuid = (new Date()).getTime() + "-" + Math.ceil(Math.random()*100000) + @uuidCounter
		return uuid

	onReconnect: () =>
		#
		#Resend registrations
		for cmd in @registeredList
			@socket.emit "register", cmd

		for i,o of @dynamicCallPending
			console.log "RECONNECT PENDING=", i, "=", o

		true

	onCmdReplyComplete: (data)=>
		#
		# Default event when a command is done executing

	#
	# Setup the socket to listen for an incoming reply
	onSetupSocket: (@socket) =>

		#
		# Receive a command
		@socket.on "cmd", @onIncomingCommandRequest

		@socket.on 'api-list', @onReceiveApiList

		#
		# Server sends "cmd-reply" with the results of an API call
		@socket.on "cmd-reply", @onIncomingCommandReply


	#
	# This should be a reply to our command
	# which must contain the UUID of the request
	#
	onIncomingCommandReply: (data) =>

		if !data.uuid
			console.log "Invalid cmd-reply, no uuid:", data
			return false

		#
		# Receive a response to a command
		# console.log "RECEIVED cmd-reply", data
		if @dynamicResolveList[data.uuid]?

			if data.result? and typeof data.result == "string" and /^STREAM:([0-9]+)/.test data.result
				@dynamicResolveList[data.uuid](new EdgeApiProviderStream(this, data.result))
			else
				@dynamicResolveList[data.uuid](data.result)

			delete @dynamicResolveList[data.uuid]
			delete @dynamicRejectList[data.uuid]
			delete @dynamicCallPending[data.uuid]
			@onCmdReplyComplete(data);
		else
			console.log "Received response to nothing:", data

		true


	#
	# Receive one of more API calls from the server
	#
	onReceiveApiList: (data) =>

		for name, params of data
			@addDynamicApiCall name, params

		true

	#
	# Execute a dynamic call, returns a promise
	# the promise is resolve with the result of the API call
	# Param is an array of call values or an object with call values
	#

	MakeDynamicCall: (callName, params) =>

		new Promise (resolve, reject) =>

			# console.log "MakeDynamicCall [#{callName}] >", params

			#
			# Param could be [1004, 564332]
			# or { store_id: 1004, item_id: 564332 }
			#

			if !@dynamicApiCallsList[callName]?
				reject new Error "Invalid API call: #{callName}"

			pos  = 0
			data = {}

			# console.log "DY=", @dynamicApiCallsList[callName]

			for varName in @dynamicApiCallsList[callName]

				# console.log "VAR=", varName
				# if varName == "user"
				#     console.log "Skipping user:", varName
				#     continue

				if typeof params[pos] != "undefined"
					data[varName] = params[pos++]
				else if params[varName]?
					data[varName] = params[varName]
				else
					console.log "Invalid call to ", callName, "(", params, ")"
					console.log "Parameter for ", varName, " not passed in."
					reject new Error "Invalid call to #{callName}, missing #{varName}"

			data.callName = callName
			data.uuid     = @fastUUID()

			@dynamicResolveList[data.uuid] = resolve
			@dynamicRejectList[data.uuid]  = reject
			@dynamicCallPending[data.uuid] = new Date()

			# console.log "Sending Emit ", data
			@socket.emit "cmd", data

	#
	# Register a single API command, create a dynamic function that calls it
	# This is an API call that the server knows how to answer, not a call
	# that we are capable of answering on this client
	#

	addDynamicApiCall: (callName, callParams) =>

		#
		# Don't re-register the call
		if this[callName]? then return true

		if !callParams?
			callParams = []

		# console.log "dynamicApiCallsList[#{callName}] = ", callParams
		@dynamicApiCallsList[callName] = callParams
		tmp = new Function( "return function " + callName + "() { return this.MakeDynamicCall(arguments.callee.name, arguments); }" )
		this[callName] = tmp()
		return true


	#
	# ----------------------------------------------------
	# Related to API calls that this client can execute
	# ----------------------------------------------------
	#

	#
	# Execute a local command that is a registered API call.
	# The callData comes from the caller and may or may not match
	onExecuteLocalCommand: (callName, callData) =>

		if !callName? or typeof callName != "string"
			throw new error "Invalid call, missing callName"

		if !callData?
			throw new error "Invalid call, missing callData"

		if !@commandCallback[callName]?
			console.log "Invalid onExecuteLocalCommand(#{callName});", @commandCallback

		args = []
		for varName, description of @commandCallback[callName].params
			if !callData[varName]?
				console.log "Warning:  Missing '#{varName}' in ", callData

			args.push callData[varName]

		console.log "onExecuteLocalCommand args=", args
		result = @commandCallback[callName].target.apply this, args
		console.log "onExecuteLocalCommand result=", result

		return result

	#
	# Socketed receievd a "cmd".  This could be something
	# we support, but it may not be.
	onIncomingCommandRequest: (data) =>

		if !@commandCallback[data.callName]?
			return false

		result = @onExecuteLocalCommand(data.callName, data)
		if result? and result.then? and typeof result.then == "function"

			#
			# Handle the promise by waiting for the answer and then
			# sending that answer back to the client
			result.then (newResult) =>
				# console.log "Secondary response=", newResult
				@socket.emit "cmd-reply",
					uuid   : data.uuid
					result : newResult

		else

			@socket.emit "cmd-reply",
				uuid   : data.uuid
				result : result

		return true

	#
	# Tell the server we are able to support a given command
	# When the command arrives from the socket, we'll execute it
	# and return the result.  The promise here returns after
	# the registration is sent to the server
	#
	doRegisterApiCommand: (apiDefinition) =>

		new Promise (resolve, reject) =>

			path = apiDefinition.base + "_" + apiDefinition.command
			console.log "doRegisterApiCommand: #{path}"

			@commandCallback[path] = apiDefinition
			@registeredList.push apiDefinition
			@socket.emit "register", apiDefinition

			#
			# We send the register command to the central server
			# The central server converts this to an API Call
			# The central server sends the API call back out
			# which is received as "api-list" and added
			#

			#
			# When a client wants to trigger this call, a "cmd"
			# is received,

			resolve(true)

#
# This is the base class for calling the Edge API Server
# The api can expose RPC functions and it can consume functions
#


class EdgeApiBase extends EdgeApiProvider

	constructor: () ->

		@subscriptionList = []
		@registeredList   = []
		@commandCallback  = {}
		@isVerbose        = false

		@emitter          = new EvEmitter();

		super()

	#
	# Establish a socket connection to the server
	#

	#
	# Allow registering for push message actions
	on: (pushMessage, callback) =>
		@emitter.on pushMessage, callback

	#
	# Stop a push message action
	off: (pushMessage, callback) =>
		@emitter.off pushMessage, callback

	#
	# Single callback for an event
	once: (pushMessage, callback) =>
		@emitter.on pushMessage, callback

	onConnect: (socket) =>
		#
		# Nothing

	onDisconnect: () =>
		#
		# Nothing

	onReady: () =>
		#
		# Nothing

	doGetSocket: () =>
		#
		throw new Error "Parent must implement"

	onCmdReplyComplete: () =>
		#
		# Called when a command finishes

	onInitializeSocket: () =>

		new Promise (resolve, reject) =>

			@socket.on 'connect', () =>
				#
				# Send/Resend current subscriptions
				for sub in @subscriptionList
					@socket.emit "subscribe", sub

				@onConnect(@socket)
				@onReconnect(@socket)
				resolve(true)
				true

			@socket.on 'disconnect', () =>
				@onDisconnect()
				@waitForPromises = {}
				true

			@socket.on "change", (data) =>
				# console.log "CHANGE MATCH=", data
				@updateValue data
				true

			@socket.on "cmd-push", (data) =>

				#
				# incoming push message
				command = data.command.toString()
				# console.log "Push received [#{command}] = ", data
				@emitter.emitEvent command, [ data.data ]
				true

	doWaitForApi: (callName) =>

		if !@waitForPromises
			@waitForPromises = {}

		if @waitForPromises[callName]
			return @waitForPromises[callName]

		@waitForPromises[callName] = new Promise (resolve, reject) =>

			do checkAvailable = ()=>

				if this[callName]?
					resolve(true)
				else
					setTimeout checkAvailable, 100

		return @waitForPromises[callName]

	doConnect: () =>

		if @connectPromise?
			return @connectPromise

		@connectPromise = new Promise (resolve, reject)=>

			@doGetSocket()
			.then () =>
				@onSetupSocket(@socket)
				@onInitializeSocket()
			.then () =>
				@doWaitForApi "os_doGetServerInformation"
			.then () =>
				resolve(true)
			.catch (e) =>
				console.log "EdgeApiProvider doConnect Exception:", e

	#
	# Wrapper function to subscribe to a single item
	subscribeItem: (store_id, item_id) =>
		#
		# Subscribe to changes for a given item
		store_id = store_id.toString()
		store_id = "0" + store_id while store_id.length < 4

		@subscribe "store_#{store_id}", "/item/#{item_id}/"
		@subscribe "store_0000", "/item/#{item_id}/"
		true

	#
	#Subscribe to a given path
	subscribe: (dataset, path) =>

		#
		# Prevent re-subsubscribing
		for sub in @subscriptionList
			if sub.path == path and sub.dataset == dataset then return true

		sub =
			path: path
			dataset: dataset

		@socket.emit "subscribe", sub
		@subscriptionList.push sub
		true

	#
	# Send a command to the server
	# command [String] - The command to execute
	# options [any] - Some type of parameter as known by the command's API
	#
	sendCommand: (command, options) =>

		if typeof options != "object"
			options = {}

		@socket.emit command, options




