# General documentation for the EdgeServer system

## Background - What is Edge

Edge is a common collection of Javascript classes written in TypeScript that make a system for high performance micro-server based processing.

## Key Concepts

* [Understanding Paths](http://gitlab.protovate.com/EdgeTS/EdgeApiServer/blob/master/UnderstandingPaths.md) - A guide to understand file paths within the API.   This explains how to data is stored in "folders" and "paths" within the database.

* [Understanding API Providers](http://gitlab.protovate.com/EdgeTS/EdgeApiServer/blob/master/UnderstandingApiProviders.md) - A guide to API providers, REST Data, and how Edge delivers data by creating dynamic API providers.

* [Understanding Message Queues](http://gitlab.protovate.com/EdgeTS/EdgeCommonMessageQueue) - A guide to how Edge uses AQMP Message Queues to save data along with MQTT for data collection and REDIS.

* [Understanding Work Queues](http://gitlab.protovate.com/EdgeTS/EdgeApiServer/blob/master/UnderstandingWorkQueues.md) - A guide to the Work Queue API set

* [UnstandingInstall.md](UnderstandingInstall.md) - A guide to how servers are configured to run Edge






## Common Config
> The Edge system has several core requirements that are provided by EdgeCommonConfig.  This library provides common logging, common status updates, benchmarking, and access to server credentials.   No Edge application should store credentials within the code.   All Edge applications must include EdgeCommonConfig

* [Config](http://gitlab.protovate.com/EdgeTS/EdgeCommonConfig) - The core "config" library that all modules should include, read about it here.






##  Utility Classes

* [Rate Capture](http://gitlab.protovate.com/EdgeTS/EdgeCommonRateCapture) - a library that will produce a console status message and tracks progress of events over time.   Updates are trackable on the server status dashboard.
* [ninjadebug](http://gitlab.protovate.com/EdgeTS/ninjadebug) - a debug tool to return value with a variety of data formats
* [EdgeCommonLauncher](http://gitlab.protovate.com/EdgeTS/EdgeCommonLauncher) - a library to launch an application, capture the output and handle all module installs and other factors needed to execute the script.
##  Messaging between differnt parts of Edge






## The Common API
>  Edge uses Socket.io to communicate and make API calls.  The EdgeApi library wraps the socket.io client and handles connected to the API server(s) and making API calls.   
>  EdgeApi provides common functions for caching data as well.

* [EdgeApi](http://gitlab.protovate.com/EdgeTS/EdgeApi) - Make server API calls,  Cache data in REDIS,  Add to Work Queues





## Messages
>  Edge uses messages to communicate between services and other things.

* [Load Level Design Pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/queue-based-load-leveling) - A guide from Microsoft that explains about Load Level design pattern.

* [EdgeCommon Message Queue](http://gitlab.protovate.com/EdgeTS/EdgeCommonMessageQueue) - Interface to listen for messages from a message queue

* [EdgeCommon Cluster Worker](TODO) - Wrapper around common code to subscribe to a work queue and process messages.

* [StatusUpdateClient](http://gitlab.protovate.com/EdgeTS/EdgeCommonStatusUpdateClient) - All currently running apps or microservices can send status updates using
the message queue "status-updates".




## Accessing Data

* [EdgeCommon Table Base](http://gitlab.protovate.com/EdgeTS/EdgeCommonTableBase) - A wrapper class around a collection that handles api connection and other functions to get data.  This can also be used to save data and is similar to a Windows DAO object or a Ruby ActiveRecord concept.

* [EdgeCommon Tabbed File Import](http://gitlab.protovate.com/EdgeTS/EdgeCommonTabbedFileImportAsync) - A very fast tabbed or CSV file import class.   Provides both sync and async versions that can handle any size file with any delimiter.   Handles conversion to JSON including dates and times and other values.

* [EdgeCommon Request](http://gitlab.protovate.com/EdgeTS/EdgeCommonRequest) - An extensive wrapper around http.request that supports GET and POST,  Windows IIS Digest Auth, multi-part attachments, GZIP and Zlib compression, and timeouts.  Supports FTP, HTTP, and HTTPS.  Supports Basic-Auth, OAuth, MD5 HMAC Auth.   Setting to easily use [Charles Internet Proxy](https://www.charlesproxy.com/) for debugging. Always use this instead of node's default http and https modules.

## Running a node
> The main code that needs to be run on each "node" is called "Jobs" and it contains the appserver and jobserver programs.   
> These two are the only things that need to be installed on a new server.   
> They will install all required components using the configuration file for that machine.

* [Understanding os.jobs](TODO) - A guide to understand the os.jobs table and how scripts or services are run based on time.
* [Understanding AppLauncherConfig](TODO) - A guide to understand how microservices are setup on a node.
* [Jobs](git clone git@gitlab.protovate.com:Edge/Jobs.git Jobs)





##  Web Services / API Services
> The main API accesses the server in the form of the EdgeApiServer or an app or Microservice derived from it.

* [EdgeApiServer](http://gitlab.protovate.com/EdgeTS/EdgeApiServer) - The EdgeApiServer Project

* [How to test](HowToTest.md) - A guide that explains how to test a running server




## Logging and Debugging

* [EdgeCommon Time Series Data](http://gitlab.protovate.com/EdgeTS/EdgeCommonTimeseriesData) - Log and report on time series data such as events and performance.




## Other links worth tracking

- [Azure Command Tricks](AzureCommands.md) - Some tricks using the azure command line tools our developers and system admins maintain.

- [GitHub/GitLab Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) - Markdown information for MD files.

- [Clean Code with TypeScript](https://github.com/labs42io/clean-code-typescript) - How to format your Typescript for readability.

- [Lint File](TODO) - The shared Typescript Lint configuration file we use

- [Formatting File](TODO) - The shared Typescript Pretty code formatting configuration


