$ ->

	addTestButton "Test Tabs", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabs", (tabs)->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two"

		return 1

	addTestButton "Test Tabs with order", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabs", (tabs)->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two", 1
			tabs.addTab "Test 3", "Default Three", 0
			tabs.addTab "Test 4", "Default Four"
			tabs.addTab "Test 5", "Default Five", 2

		return 1

	addTestButton "Test Tabs order/setTimeout", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabs", (tabs)->
			setTimeout ->
				console.log "Added 0"
				tabs.addTab "Test 0", "Default One", 0
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 1"
				tabs.addTab "Test 1", "Default Two", 1
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 2"
				tabs.addTab "Test 2", "Default Three", 2
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 3"
				tabs.addTab "Test 3", "Default Four", 4
			, (2000 * Math.random())
			setTimeout ->
				console.log "Added 4"
				tabs.addTab "Test 4", "Default Five", 5
			, (2000 * Math.random())

		return 1

	addTestButton "Test Tabs with badge", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "Test 1", "Default One"
			tab2 = tabs.addTab "Test 2", "Default Two"
			tab1.setBadge(5)

		return 1

	addTestButton "Test Tabs with Icons", "Open", (e)->

		addHolder()
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"			

		return 1

	addTestButton "Tabs Disabled", "Open", (e)->

		addHolder()
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"
			tab2 = tabs.addTab "tab_3", "Default Three"
			setTimeout () ->
				tabs.setTabEnabled "tab_1", false
			, 2000
			setTimeout () ->
				tabs.setTabEnabled "tab_2", false
			, 4000
		return 1		

	addTestButton "Tabs Background color", "Open", (e)->

		addHolder()
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"
			tab2 = tabs.addTab "tab_2", "Default Two"
			tabs.addIconToTab "tab_2", "fa fa-bell"
			tab3 = tabs.addTab "tab_3", "Default Three"
			tab3 = tabs.addTab "tab_4", "Default Four"
			tab3 = tabs.addTab "tab_5", "Default Five"			
			tabs.setTabBGColor "tab_1", 1
			tabs.setTabBGColor "tab_2", 2
			tabs.setTabBGColor "tab_3", 3
			tabs.setTabBGColor "tab_4", "tabcolor4"
			tabs.setTabBGColor "tab_5", "tabcolor5"	
			setTimeout () ->
				tabs.setTabEnabled "tab_1", false
			, 3000

			setTimeout () ->
				tabs.setTabEnabled "tab_1", true
			, 6000

		return 1

	addTestButton "Tabs with classes", "Open", (e)->

		addHolder()
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "tab_1", "Apple"
			tabs.setTabClass("tab_1", "ninja-tab-fixed-140")
			tabs.addIconToTab "tab_1", "far fa-book"

			tab1 = tabs.addTab "tab_2", "Ball"
			tabs.setTabClass /_2/, "ninja-tab-fixed-140"
			tabs.addIconToTab "tab_2", "fas fa-book"

			tab3 = tabs.addTab "This is a very long tab title that seems really dumb", "Nothing here"
			tabs.setTabClass tab3.id, "ninja-tab-fixed-140"
			tabs.addIconToTab tab3.id, "fal fa-book"

		true

	addTestButton "Tabs Close Buttons", "Open", (e)->

		addHolder()
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "tab_1", "Default One"
			tabs.addIconToTab "tab_1", "fa fa-book"	
			tabs.addCloseButton "tab_1"
			tab2 = tabs.addTab "tab_2", "Default Two"	
			tab2.setBadge(5,'warning')
			tabs.addIconToTab "tab_2", "fa fa-ban"	
			tabs.addCloseButton "tab_2"
			tab3 = tabs.addTab "tab_3", "Default Three"

		return 1	

	addTestButton "Tabs Close Buttons - Deserialize", "Open", (e)->		
		addHolder()
		.setView "TestLayout1", (view) ->
			view.runTest
				view: "DynamicTabs"
				tabs: [
					title: "tab1"
					icon: "fa fa-book"
					close: true 
					ontabhide: (tab) ->
						console.log "Tab1 is being hidden...", tab.id
				, 
					title: "tab2"
					icon: "fa fa-ban"
					close: true
					ontabhide: (tab) ->
						console.log "Tab2 is being hidden...", tab.id					
				,
					title: "tab3"
					close: false
					ontabhide: (tab) ->
						console.log "Tab3 is being hidden...", tab.id					
				]
		
		return 1

	addTestButton "Test Tabs with badge with context classes", "Open", (e)->

		addHolder("renderTest1")
		.setView "DynamicTabs", (tabs)->
			tab1 = tabs.addTab "Test 1", "Default One"
			tab2 = tabs.addTab "Test 2", "Default Two"
			tab1.setBadge(5,'warning')
			tab2.setBadge(2,'danger','back') ## direction of the badge it can be front (default) or back

		return 1

	addTestButton "Tab Settings", "Open", (e) ->
		addHolder()
		.setView "DynamicTabs", (tabs) ->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two", 1
			tabs.addTab "Test 3", "Default Three", 0
			tabs.addTab "Test 4", "Default Four"
			tabs.addTab "Test 5", "Default Five", 2
			tabs.setSettingsName "tab_test_active_tab"
		return 1	

	addTestButton "Tab Set Overflow HTML", "Open", (e) ->
		addHolder()
		.setView "DynamicTabs", (tabs) ->
			tabs.addTab "Test 1", "Default One"
			tabs.addTab "Test 2", "Default Two", 1
			tabs.addTab "Test 3", "Default Three", 0
			tabs.addTab "Test 4", "Default Four"
			tabs.addTab "Test 5", "Default Five", 2
			tabs.setOverflowHTML "Default style text"
			setTimeout () ->
				tabs.setOverflowHTML "<span style='color:red;'>Red Part</span> Normal Part"
			, 2000
		true

	addTestButton "Tabs with tables", "Open", (e)->

		newPromise ()->

			yield loadZipcodes()
			yield loadStockData()
			tabs = yield addHolder().setView "DynamicTabs"
			tabs.doAddTableTab "zipcode", "Zipcodes", 1
			tabs.doAddTableTab "stocks", "Stock Data", 0

		.then ()->
			console.log "Done."

	addTestButton "Tabs Deserialize", "Open", () ->
		addHolder()
		.setView "DynamicTabs", (view) ->
			view.deserialize
				tabs: [
					title: "tab1"
					html : "First Tab"
					order: 3
					ontabshow: (tab) ->
						console.log "Tab1 is showing", tab
				,
					title: "tab2"
					order: 1
					view : "Form"
					name : "viewform"
					inputs: [
						type: "text"
						name: "input1"
						label: "Input 1"
					,
						type: "text"
						name: "input2"
						label: "Input 2"					
					]
					ontabshow: (tab) ->
						console.log "Tab2 is showing", tab					
				,
					title: "tab3"
					html: "Third Tab"
					order: 2
					ontabshow: (tab) ->
						console.log "Tab3 is showing", tab					
				]
				settingsname: "test_tab1_settings_name"

			setTimeout () ->
				options = view.serialize()
				console.log "DynamicTabs serialized data = ", options
				doPopupView "DynamicTabs", "Tabs Deserialized", null, 1200, 800, (viewPopup) ->
					viewPopup.deserialize options
			, 3000

	addTestButton "Tabs Delay in Loading views", "Open", () ->

		newPromise ()->

			yield loadZipcodes()
			yield loadStockData()
			addHolder()
			.setView "DynamicTabs", (view) ->
				view.deserialize
					tabs: [
						title  : "tab1"
						html   : "First tab"
						order  : 3
						ontabshow: (tab) ->
							console.log "Tab is showing ", tab						
					,
						title: "tab2"
						view   : "TestShowSize"
						delayed: false
						name: "viewTestShowSize"
						order: 1
						ontabshow: (tab) ->
							console.log "Tab2 is showing ", tab
						ontabhide: (tab) ->
							console.log "Tab2 is being hidden ", tab
					,
						title: "tab3"
						view: "Table"
						collection: "zipcode"
						delayed: true
						name: "viewTable"
						order: 2
						ontabshow: (tab) ->
							console.log "Tab3 is showing ", tab
						ontabhide: (tab) ->
							console.log "Tab3 is being hidden ", tab						
					]

				setTimeout () ->
					console.log "ViewDynamicTabs = ", view
				, 8000
		.then () =>
			console.log "Done."

		true

	go()
