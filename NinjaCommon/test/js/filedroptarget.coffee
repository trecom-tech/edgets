$ ->

    addTestButton "Simple FileDropTarget", "Open", ()->
        addHolder().setView "DragFileTarget", (view)->
            true

    addTestButton "Custom FileDropTarget", "Open", ()->
        options =
            title: "Drag & Drop File Here"
            width: 300
            height: 300
            uploadURL: '/filemanager/upload/'
        addHolder().setView "DragFileTarget", (view)->
            true
        , options

    addTestButton "FileDropTarget on Popup", "Open", () ->
        doPopupView "DragFileTarget", "Popup with a DragFileTarget", null, 800, 600, (view)->
            true
        true

    addTestButton "FileDropTarget in tab", "Open", ()->
        addHolder()
            .setView "DynamicTabs", (viewTabs)->
                viewTabs.doAddViewTab "DragFileTarget", "FileDropTargetTab", (view)->
                    true
                viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
        true

    go()