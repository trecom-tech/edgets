class ViewDragFileTarget extends View

    constructor: () ->
        super()

        @options =
            title: 'Press to select a file or drop a file here'
            width: -1
            height: -1
            uploadURL: '/filemanager/savefile/'
            # uploadURL: '/upload'
            dataPath: ''
            username: '@guest'

        GlobalClassTools.addEventManager(this)

    ## ----------------------------------------------------------------
    ## Set data function
    ## Create elements to display on screen and add drag and drop events
    ## @param option object
    ##
    setData: (option)=>
        if option? and option.title? then @options.title = option.title
        if option? and option.width? then @options.width = option.width
        if option? and option.height? then @options.height = option.height
        if option? and option.uploadURL? then @options.uploadURL = option.uploadURL

    onShowScreen: () =>
        @fileTitle.html @options.title
        dragEnter = dragLeave = dragOver = false
        @dragFileForm.on 'dragover', (event) =>
            stop_event_propogation event
            if !dragOver
                dragOver = true
                @dragFileForm.addClass 'dragover'

        @dragFileForm.on 'dragenter', (event) =>
            stop_event_propogation event
            if dragEnter
                dragLeave = true
            else
                dragEnter = true
                @dragFileForm.addClass 'dragover'

        @dragFileForm.on 'dragleave', (event) =>
            stop_event_propogation event
            if dragLeave      then dragLeave = false
            else if dragEnter then dragEnter = false
            if not dragEnter and not dragLeave
                @dragFileForm.removeClass 'dragover'

        uploadURL = @options.uploadURL
        @dragFileForm.on 'drop', (event) =>
            stop_event_propogation event
            dragEnter = false
            @dragFileForm.removeClass 'dragover'
            desc = ''

            iterateFilesAndDirs = (filesAndDirs, path)->
                for fd in filesAndDirs
                    if typeof fd.getFilesAndDirectories is 'function'
                        path = fd.path

                        fd.getFilesAndDirectories().then (subFilesAndDirs)->
                            iterateFilesAndDirs subFilesAndDirs, path
                    else
                        uploadFile fd, uploadURL, path, desc
                true

            dataTransfer = event.dataTransfer or event.originalEvent.dataTransfer
            if 'getFilesAndDirectories' of (dataTransfer or {})
                dataTransfer.getFilesAndDirectories().then (filesAndDirs)->
                    m = new ModalForm
                        title        : "Description for files"
                        inputs: [
                            name:  "desc"
                            label: "Description:"
                        ]
                        buttons: [
                            type: "submit"
                            text: "Upload"
                        ]
                        onsubmit: (form) =>
                            desc = form.desc.value
                            iterateFilesAndDirs filesAndDirs, '/'
                            m.hide()

            true

        @dragFileForm.on 'click', (event) =>
            event.preventDefault()
            event.stopPropagation()
            @dragFileInput.click()

        @dragFileInput.on 'click', (event) =>
            event.stopPropagation()

        @dragFileInput.on 'change', (event) =>
            files = event.target.files
            if files.length isnt 0
                file = files[0]

                m = new ModalForm
                    title        : "Description For: " + file.name
                    ok           : "Upload"
                    inputs: [
                        name: "desc"
                        label: "Description:"
                    ]
                    onsubmit: (form) =>
                        desc = form.desc.value
                        uploadFile file, uploadURL, '', desc
                        m.hide()

            true

        window.addEventListener "busydialog_finished", @onBusyDialogFinished

        uploadFile = (file, url, path='', description='') =>
            reader = new FileReader()
            reader.onload = (e) ->
                match = /^data:(.*);base64,(.*)$/.exec e.target.result
                if match is null then throw "Could not parse result"

            if not file?
                return false

            reader.readAsDataURL file

            if !@multiBusyDialog?
                @multiBusyDialog = new MultiBusyDialog "Uploading..."
                @fid = 0
            @fid += 1

            ffid = @fid
            @multiBusyDialog.addBusy ffid, file.name
            @emitEvent "file_start", [ file.name, file.size ]
            formData = new FormData
            formData.append 'myfile', file
            formData.append 'dpath', @options.dataPath + path
            formData.append 'mimetype', file.type
            formData.append 'username', @options.username
            formData.append 'description', description
            @fileTitle.html file.name
            @fileTitle.addClass 'dragfile-title-active'

            $.ajax
                xhr: ()=>
                    xhr = new window.XMLHttpRequest()
                    numberWithCommas = (x)->
                        return x.toString().replace /\B(?=(\d{3})+(?!\d))/g, ","

                    xhr.upload.addEventListener "progress", (evt)=>
                        if  evt.lengthComputable
                            percentComplete = evt.loaded / evt.total
                            percentComplete = parseInt percentComplete * 100
                            sizeComplete = null
                            if evt.total > 1024 * 1024
                                loaded = Math.floor evt.loaded / (1024 * 1024)
                                total = Math.floor evt.total / (1024 * 1024)
                                sizeComplete = "#{loaded}MB / #{total}MB"
                            else if evt.total > 1024
                                loaded = Math.floor evt.loaded / 1024
                                total = Math.floor evt.total / 1024
                                sizeComplete = "#{loaded}KB / #{total}KB"
                            else
                                sizeComplete = "#{loaded}B / #{total}B"

                            @multiBusyDialog.updatePercent ffid, percentComplete
                            @emitEvent "file_progress", [ file.name, evt.loaded, evt.total ]

                            if percentComplete is 100
                                @multiBusyDialog.finished ffid
                                @emitEvent "file_complete", [ file.name ]
                    , false

                    return xhr
                url: url
                method: 'post'
                data: formData
                processData: false
                contentType: false
                success: (response) =>
                    console.log "Success: "
                error: (xhr, status, err) =>
                    @multiBusyDialog?.hide()
                    m = new ModalForm
                        title        : "Error"
                        content      : "Uploading failed due to an error."
                        buttons: [
                            type: "submit"
                            label: "OK"
                        ]
                        onsubmit: (form) ->
                            m.hide()
            true

        stop_event_propogation = (event) ->
            event.preventDefault()
            event.stopPropagation()

    onBusyDialogFinished: (e) =>
        # console.log "busy dialog finished: ", e, @fid
        if e.detail.fid isnt @fid then return false
        @fileTitle.html @options.title
        @fileTitle.removeClass 'dragfile-title-active'
        @multiBusyDialog = null
        true

    openFileUpload: ()=>
        @dragFileInput.click()

    ## -----------------------------------------------------------------
    ## set path of upload files
    ## @param string url
    ##
    setPath: (dataPath) =>
        @options.dataPath = dataPath

    ## -----------------------------------------------------------------
    ## set username
    ## @param string url
    ##
    setUsername: (username) =>
        @options.username = username

    ## -----------------------------------------------------------------
    ## Function to set upload URL
    ## @param string url
    ##
    setServerPath: (url) =>
        @options.uploadURL = url

    ## -----------------------------------------------------------------
    ## Function to set DragFileTarget size
    ## @param width, height as number
    ##
    setBoxSize: (width, height) =>
        @options.width  = width
        @options.height = height

    ## -----------------------------------------------------------------
    ## Function to set DragFileTarget Title
    ## @param string title
    ##
    setTitle: (title) =>
        @options.title = title

    ## -----------------------------------------------------------------
    ## Function to set option object
    ## @param object option
    ##
    setOptions: (option) =>
        @setData option

    onResize: (w, h) =>
        if !@dragFileForm then return false
        width = if @options.width < 0 or @options.width > w then w else @options.width
        height = if @options.height < 0 or @options.height > h then h else @options.height
        @dragFileForm.outerWidth width
        @dragFileForm.outerHeight height
        true

    serialize: () =>
        @options

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewDragFileTarget: invalid options passed"

            if options.title?
                @setTitle options.title

            if options.dataPath?
                @setPath options.dataPath

            if options.username?
                @setUsername options.username

            if options.uploadURL?
                @setServerPath options.uploadURL

            if options.width? and options.height?
                @setBoxSize options.width, options.height

            if options.options?
                @setOptions options.options

            resolve true
