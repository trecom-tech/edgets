
class ViewImageAnnotation extends View
	constructor: () ->
		super()
		@notes 			= {}
		@icons 			= {}
		@isSetDataPath 	= false
		@tooltipWindow 	= new FloatingWindow(0, 0, 100, 100)
		@iconSize 		= 20
		@noteId 		= 0

	setData: (data) =>
		## -gao
		## Create Image Holder
		##
		@wgtImgHolder 			= @addDiv "img-annotation-holder"
		@wgtImgHolder.onResize 	= @resizeImage
		@wgtImage 				= @wgtImgHolder.add "img"

	## function to set image source for this view
	setImage: (image) =>
		unless image? then return false
		if typeof image == "object" and image.src?
			image = image.src		
		img = new Image()
		img.onload = (o) =>
			imageData = 
				src : img.src
				w 	: img.naturalWidth
				h 	: img.naturalHeight

			@wgtImage.el.attr "src", img.src
			@wgtImage.el.bind "click", (e) =>
				#console.log "Click event on ImageAnnotation"
				true
			@wgtImage.el.bind "contextmenu", (e) =>
				@showAddNoteMenu e
				true
			@imageData = imageData
			@resizeImage()

		img.onerror = (o) =>
			console.log "ViewImageAnnotation setImage error: ", image, o

		img.src = image
		return @imageData

	## set data path to where notes' data is saved
	setDataPath: (@tableName, @path) =>
		if !@tableName or !@path then return false
		@isSetDataPath = true
		setTimeout @loadNotesFromDataMap, 200
		true

	## function to show modal dialog to add a note to the view
	showAddNoteModal: (x, y) =>
		m = new ModalForm
			title 	 	: "Add a note"
			content 	: "Write down a note in this box:"
			position 	: "top"
			ok 			: "Save"
			inputs: [
				name:  "input-text"
				label: "Note:"
			]
			onsubmit: (form) =>
				@addNote x, y, form["input-text"].value, new Date()
				m.hide()

	## function to show modal dialog to edit currently added note
	showEditNoteModal: (x, y, note, noteNumber) =>
		m = new ModalForm
			content 	: "Edit this note:"
			position 	: "top"
			title 		: "Edit note"
			ok 			: "Update"
			inputs: [
				name:  "input-text"
				label: "Note:"
				value: note
			]
			onsubmit: (form) =>
				@editNote noteNumber, form["input-text"].value, new Date()
				m.hide()

	## function to add a note's data to DataMap
	addNote: (x, y, note, noteStamp) =>
		imagePos = @wgtImage.el.offset()
		##
		## calcuate position of note inside image by percent
		xPercent = (x - imagePos.left) / @wgtImage.width()
		yPercent = (y - imagePos.top) / @wgtImage.height()
		# console.log "Adding a note: ", x, y, note, noteStamp		
		@notes[@noteId] =
			x: xPercent
			y: yPercent
			note: note
			noteStamp: noteStamp
		## update dataMap with current List of Note
		@updateNotesInDataMap @path, @notes, @noteId, xPercent, yPercent, noteStamp

		## add icon for this note
		holderPos = @wgtImgHolder.el.offset()
		@addNoteIcon x - holderPos.left, y - holderPos.top, note
		@noteId++
		true

	## function to update a note's data in DataMap
	editNote: (noteNumber, note, noteStamp) =>
		if !@notes[noteNumber] then return false
		@notes[noteNumber]['note'] = note
		@notes[noteNumber]['noteStamp'] = noteStamp
		@updateNotesInDataMap @path, @notes, noteNumber
		true

	## function to remove a note's data from DataMap
	removeNote: (noteNumber) =>
		if !@notes[noteNumber] then return false
		delete @notes[noteNumber]
		@updateNotesInDataMap @path, @notes, noteNumber
		@removeNoteIcon noteNumber
		true

	## 
	## Update datamap field 
	updateNotesInDataMap: (path, notes, noteNumber, xPercent, yPercent, noteStamp) =>
		parts     = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		fieldName = parts[3]		
		DataMap.addData tableName, keyValue, 
			"#{fieldName}": 
				notes: notes
		##
		## emit event for this change in DataMap
		globalTableEvents.emitEvent "note_added", [ tableName, keyValue, fieldName, notes, noteNumber, xPercent, yPercent, noteStamp ]
		true

	##
	## Load notes data from DataMap
	loadNotesFromDataMap: () =>
		parts     = @path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		fieldName = parts[3]

		data = DataMap.getDataField tableName, keyValue, fieldName		
		if !data then return false

		@notes = data.notes
		for key, note of @notes
			@addNoteOnScreen note
		true

	## show a menu with option to add a note to the view
	showAddNoteMenu: (e) =>
		unless @isSetDataPath then return false
		menu = new PopupMenu "Menu", e
		menu.addItem "Add Note", (data) =>
			@showAddNoteModal menu.x, menu.y
			true
		, @notes.length

	## show a menu with option to edit a note of the view
	showEditNoteMenu: (e) =>
		unless @isSetDataPath then return false
		menu = new PopupMenu "Menu", e
		menu.addItem "Edit Note", (data) =>
			@showEditNoteModal menu.x, menu.y, @notes[e.data.number]['note'], e.data.number
			true
		, e.data.number
		menu.addItem "Delete Note", () =>
			@removeNote e.data.number
			true
		, e.data.number

	## function to add a icon for each note
	addNoteIcon: (x, y, note) =>
		wgtIcon = @wgtImgHolder.add "i", "si si-note img-annotation-icon", "img-annotation-icon-#{@icons.length}", 
			"tooltip" 		: "simple"
			"data-title" 	: note

		wgtIcon.move x, y, @iconSize, @iconSize
		iconNum = @noteId
		wgtIcon.el.on "mouseover", { number: iconNum }, (e) =>
			# console.log "Mouse Over event!!!", this, e
			coords = GlobalValueManager.GetCoordsFromEvent e
			setTimeout @showTooltip, 200, coords.x, coords.y, @notes[e.data.number]['note']
		wgtIcon.el.on "mouseout", (e) =>
			setTimeout @hideTooltip, 200
		wgtIcon.el.on "contextmenu", { number: iconNum }, (e) =>
			@hideTooltip()
			@showEditNoteMenu e
		@icons[@noteId] = wgtIcon

	## function to remove a icon for a certain note
	removeNoteIcon: (iconNumber) =>
		if !@icons[iconNumber] then return false
		@icons[iconNumber].destroy()
		delete @icons[iconNumber]

	## function to redraw icons when the view is resized
	refreshNoteIcons: (xImg, yImg, wImg, hImg) =>
		holderPos = @wgtImgHolder.el.offset()
		for index, note of @notes 
			xIconPos = note.x * wImg + xImg 
			yIconPos = note.y * hImg + yImg 
			@icons[index].move xIconPos, yIconPos, @iconSize, @iconSize
		true

	## show a tooltip with the corresponding note's content on itself
	showTooltip: (x, y, note) =>
		@tooltipWindow.html "<div class='img-annotation-tooltip'>#{note}</div>"
		@tooltipWindow.show()
		noteWidth = textWidth note
		noteHeight = 20
		if noteWidth > 300
			noteWidth = 310
			noteHeight = 20 * Math.ceil(noteWidth / 300)
		@tooltipWindow.setSize noteWidth, noteHeight
		@tooltipWindow.moveTo x, y

	## hide a tooltip for a note
	hideTooltip: () =>
		@tooltipWindow.hide()

	## resize function for the view
	resizeImage: (w, h) =>
		ratio1 = w / @imageData.w
		ratio2 = h / @imageData.h
		if ratio1 < ratio2
			newWidth = ratio1 * @imageData.w
			newHeight = ratio1 * @imageData.h
		else
			newWidth = ratio2 * @imageData.w
			newHeight = ratio2 * @imageData.h

		newWidth = Math.floor(newWidth)
		newHeight = Math.floor(newHeight)

		offsetX = (w - newWidth) / 2
		offsetY = (h - newHeight) / 2

		@wgtImage.move offsetX, offsetY, newWidth, newHeight
		@refreshNoteIcons offsetX, offsetY, newWidth, newHeight

	## event handler for "Resize"
	onResize: (w, h) =>
		@wgtImgHolder.onResize w, h
		true

	addNoteOnScreen: (note) =>
		if note.x and note.y and note.note and note.noteStamp
			## calculate x, y coords from percent values
			imagePos = @wgtImage.el.offset()
			xCoord = note.x * @wgtImage.width() + imagePos.left
			yCoord = note.y * @wgtImage.height() + imagePos.top
			@addNote xCoord, yCoord, note.note, note.noteStamp
			return true 
		false

	## function to get this view's data
	serialize: () =>
		obj 			= {}
		obj.imageData 	= @imageData
		obj.tableName 	= @tableName
		obj.path 		= @path
		obj.notes 		= @notes
		obj.icons 		= @icons 
		obj

	## function to set properties of this view from object
	deserialize: (obj) =>
		new Promise (resolve, reject) =>
			if !obj? or typeof obj isnt "object"
				reject "ViewImageAnnotation: invalid data passed"

			@isSetDataPath 	= false
			@setImage obj.imageData
			
			setTimeout () =>	
				if !@setDataPath(obj.tableName, obj.path) then return false
				
				for note in obj.notes
					@addNoteOnScreen note
			, 100
			resolve true
