import EdgeBlock from '../src/EdgeBlock';

export default class DummyBlock extends EdgeBlock {

    getInputs() {
        return {};
    }

    getOutputs() {
        return {};
    }

    process(data: any) {
        return true;
    }
};