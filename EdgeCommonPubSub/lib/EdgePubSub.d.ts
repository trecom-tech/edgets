/// <reference types="node" />
import * as mqtt from 'async-mqtt';
declare class EdgeMQTTPubSub {
    static mqttClient: mqtt.AsyncMqttClient;
    clientId: string;
    constructor(clientId?: string);
    consume(topicName: string, callback: any): Promise<void>;
    doConnect(): Promise<{}>;
    doClose(): Promise<void>;
    doPublish(topicName: string, str: string): Promise<{}>;
    doSubmitBuffer(topicName: string, str: Buffer): Promise<{}>;
}
export declare const EdgeMQTTController: EdgeMQTTPubSub;
export default EdgeMQTTController;
