export declare class Table {
    tableName: string;
    col: any;
    settings: any;
    constructor(tableName: string);
    setOption(varName: string, value: any): any;
    getOption(varName: string, defaultValue?: any): any;
    getColumn(source: string): any;
    verifyOrderIsUnique(): boolean;
    serialize(): any;
    renderFunctionToString(functionObject: any): any;
    unserialize(settingsObject: any): boolean;
    configureColumn(col: any, skipDeduce: boolean): any;
}
export default Table;
