import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
import { EdgeSocketRequestData, EdgeSocketResponseData } from 'edgeapi';
import IApiUser from './interfaces/IApiUser';
export default class ApiGateway {
    apiCallList: {
        [s: string]: Array<string>;
    };
    providers: ApiProvider[];
    apiWantsUser: any;
    apiCallFunc: any;
    apiOwner: {
        [methodName: string]: ApiProvider;
    };
    /**
     * Creates an instance
     */
    constructor();
    /**
     * An API Provider is a class the implements "ApiProvider" to expose functions
     * @param apiProvider instance of class extends ApiProvider
     */
    addProvider(apiProvider: ApiProvider): boolean;
    /**
     * Look at the functions exposed by the API and create an
     * array to reference those functions so that the socket can
     * RPC call them.
     */
    determineApiCalls(): boolean | boolean[];
    MakeDynamicCall(callName: string, callData: any, user: IApiUser): any;
    hasApiFunc(callName: string): any;
    /**
     * Handle a call to the API
     * @param callName - [string] function name provider_method
     * @param callData - [EdgeSocketRequestData]
     * @param callerSession - [ApiSocketUser]
     */
    doProcessApiCall(callName: string, callData: EdgeSocketRequestData, callerSession: ApiSocketUser): Promise<EdgeSocketResponseData>;
}
