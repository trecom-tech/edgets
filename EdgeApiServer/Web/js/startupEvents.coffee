## Prevents user from using "Enter" button on keypad, instead requires
## a UI button click to submit a form.
##

removeMatchingFromObject = (obj, list)->
    for name in list
        delete obj[name]

preventFormEnter = ->
    $("input").keypress (evt) ->
        charCode = evt.charCode || evt.keyCode;
        if (charCode  == 13)
            return false
        return true

## This function keeps the app from bouncing like a normal
## website would and makes it act more like a normal native app.
##
preventTouchMove = ->
    document.addEventListener 'touchmove', (e) ->
        e.preventDefault();
    return true

$ ->
    # preventTouchMove()
    preventFormEnter()
    showScreen "Login"
