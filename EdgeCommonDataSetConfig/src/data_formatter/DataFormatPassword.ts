//
// class for Password datatype
//
// @extends DataFormatText
//
import { DataFormatText } from './DataFormatText';

export class DataFormatPassword extends DataFormatText {
    name: string = 'password';
    viewPasswordChange: any;
    options: any;

    constructor(...args: any[]) {
        super(...args);
    }

    //
    // funtion to open editor for password
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        if ((currentValue == null)) {
            currentValue = "";
        }

        // @ts-ignore
        const m = new ModalViewDialog({
            showOnCreate: false,
            content     : "You can change password by filling out this form.",
            title       : "Change Password",
            ok          : "Save"
        });

        m.setView("PasswordChange", (viewPasswordChange: any) => {
                this.viewPasswordChange = viewPasswordChange;
                return true;
            }
            , { hasOriginPassword: (this.options != null ? this.options.hasOriginPassword : undefined) });

        m.onButton2 = (e: any, fields: any) => {
            this.viewPasswordChange.onSubmitCallback(this.viewPasswordChange.getViewForm());
            m.hide();
            return true;
        };

        return m.show();
    }

    attachEditor(elSourceInput: any, path: any) {
        // #
        // # set type of this input as "password"
        elSourceInput.prop("type", "password");

        if (this.align != null) {
            elSourceInput.css("text-align", this.align);
        }

        return this;
    }
}

export default DataFormatPassword;
