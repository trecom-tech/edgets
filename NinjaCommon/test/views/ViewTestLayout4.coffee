class ViewTestLayout4 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        name: "viewSplitter"
        percent: 40
        top:
            name: "viewTop"
            view: "Splittable"
            left: "TestShowSize"
            right: "TestShowSize"
            percent: 40
        bottom:
            name: "viewBottom"
            view: "Splittable"
            left: "TestShowSize"
            right: "TestShowSize"
            percent: 70

    onShowScreen: ()=>
        ##|
        ##|  Here viewSplitter is defined because of name
        ##|  layoutSplit2 is defined as the 2nd splitter
        ##|  va is defined as the left test show size view
        ##|  vb is defined as the right test show size
        ##|  all these needs where copied up this this class automatically

        console.log "ViewTestLayout4 onShowScreen viewSplitter=", @viewSplitter
        console.log "ViewTestLayout4 onShowScreen viewTop=", @viewTop
        console.log "ViewTestLayout4 onShowScreen viewBottom=", @viewBottom





