/// <reference types="koa-bodyparser" />
/// <reference types="node" />
import * as koa from 'koa';
import ApiGateway from './ApiGateway';
import ApiWebUser from './ApiWebUser';
interface CustomRoutes {
    pattern: string;
    callback: Function;
}
/**
 * Escapse and parse host name to get app name
 * Convert to lowercase
 * Remove extension such as .com and .net
 * Remove www. from the front
 * Replace any remaining dots with _, for example, www.admin.website.com becomes "admin_website"
 * Filter the name to remove ^a-zA-Z0-9_ such that we have a safe and ascii printable name.
 * @param host - [string] host of browser
 */
export declare const safeEscapeAppName: (host: string) => string;
export default class ApiWebServer {
    app: koa;
    fileTimer: any;
    fileWatch: any;
    staticContent: any;
    addedRoutes: CustomRoutes[];
    gateway: ApiGateway;
    globalUnique: number;
    isBasicAuthEnabled: boolean;
    strRealm: string;
    constructor();
    enableBasicAuth(strRealm: string): void;
    setupMiddlewares(): void;
    /**
     * Get the component required for starting Socket.io
     */
    getHttpServer(): any;
    /**
     * Get HTTPS server required for starting Socket.io with SSL support
     */
    getHttpsServer(): any;
    /**
     * Set a timer to watch a file for changes
     * @param f file to watch
     * @param callback callback after changed
     */
    setWatchTimer(f: string, callback: Function): boolean;
    /**
     * Process a path or glob to a set of files
     * Initialize the static content holder and make a callback for each
     * @param url
     * @param pathList
     * @param contentType
     * @param callbackEachFile
     */
    processPath(url: string, pathList: string[] | string, contentType: string, callbackEachFile: Function): Promise<boolean>;
    loadLessFiles(url: string, path: string | string[]): NodeJS.Timeout;
    loadStylusFile(url: string, path: string | string[]): boolean;
    loadJadeFile(url: string, path: string | string[]): boolean;
    loadCoffeeFiles(url: string, path: string | string[]): boolean;
    setupStatic(): koa<koa.DefaultState, koa.DefaultContext>;
    /**
     * Validates that an app name is valid.  For example app "test" must have Web/test/ as a folder.
     * @param appName The name of app folder
     *
     * TODO: Remove invalid file path chars from appName just in case.
     */
    doValidateAppname(appName: string): Promise<boolean>;
    doLoadAppTemplate(requestPath: string, appName: string): Promise<unknown>;
    swaggerMiddleware(): void;
    loggingMiddleware(): koa<koa.DefaultState, koa.DefaultContext>;
    /**
     * Bans hacker middleware
     * Return 403 permission error when request url is matched with a pattern
     */
    banHackerMiddleWare(): koa<koa.DefaultState, koa.DefaultContext>;
    findAppNameMiddleware(): koa<koa.DefaultState, koa.DefaultContext>;
    screenMiddleware(): koa<koa.DefaultState, koa.DefaultContext>;
    findStaticFilePath(folderName: string, filename: string): string;
    addRouteMiddleware(): koa<koa.DefaultState, koa.DefaultContext>;
    /**
     * Add a custom URL or Path
     * @param pathPattern {string|regex} The pattern to match
     * @param callback {function} (url, fields, files, matches)
     */
    addRoute(pathPattern: string, callback: Function): void;
    /**
     * Create a ApiWebUser which will be saved in session.
     */
    initializeWebUserSession(ctx: koa.Context): Promise<ApiWebUser>;
    /**
     * handles data/datset/collection.(json|xml)
     */
    dataPathMiddleware(): Promise<koa<koa.DefaultState, koa.DefaultContext & koa.Context>>;
    /**
     * handles dynamic api call
     */
    apiPathMiddleware(): Promise<koa<koa.DefaultState, koa.DefaultContext & koa.Context>>;
    /**
     * If nothing matched so far and no url exists then, redirect url to
     * ${protocal}://${hostname}/${escaped appName from hostname}?
     * So that in next redirection, findappNameMiddleware will match that appName to load
     * Web/${appName}/index.pug
     */
    hostRedirectMiddleware(): koa<koa.DefaultState, koa.DefaultContext>;
    createConfigFilesIfNotExist(): boolean;
    /**
     * Final Middleware
     */
    finalMiddleware(): koa<koa.DefaultState, koa.DefaultContext & koa.Context>;
    /**
     * Simple response for /status
     */
    statusMiddleware(): boolean;
}
export {};
