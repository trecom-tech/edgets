class ViewTestLayout12 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        name: "viewSplitter"
        percent: 30
        top: "TestShowSize"
        bottom:
            view         : "Timeline"
            name         : "viewTimeline"
            groupby      : "MMM-YYYY"
            collapseolder: false
            items: [
                text     : "This is the text of the event. The text goes here and wraps automatically. The entire text will be showing even if it takes multiple lines. This is 11px San Fransisco text."
                datetime : "2018-01-02 10:20:30"
                sender   : "tester1"
                icon     : "fa-heart"
            ,
                text     : "Item text 2"
                datetime : "2018-03-03 9:11:23"
                sender   : "tester2"
            ,
                text     : "Item text 3"
                datetime : "2018-01-02 20:11:44"
            ]

    onShowScreen: ()=>
        console.log "ViewTestLayout7 onShowScreen viewSplitter=", @viewSplitter
        console.log "ViewTestLayout7 onShowScreen view1=", @view1
        console.log "ViewTestLayout7 onShowScreen view2=", @view2
        console.log "ViewTestLayout7 onShowScreen viewTGrids=", @viewGrids
