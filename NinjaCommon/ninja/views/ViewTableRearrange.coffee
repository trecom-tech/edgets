class ViewTableRearrange extends View

    imgChecked     : "<img src='/images/checkbox.png' width='16' height='16' alt='Selected' />"

    # @property [String] imgNotChecked html to be used when checkbox is not checked
    imgNotChecked  : "<img src='/images/checkbox_no.png' width='16' height='16' alt='Selected' />"

    # @property [Array] Column fields that can make changes in this view
    validFields    : [ "name", "source", "visible", "hideable", "order" ]

    getDependencyList: ()=>
        return [ "/vendor/html.sortable.min.js" ]    

    updateColumnText: ()=>

        for col in @columns
            if col.getAlwaysHidden() then continue

            col.tagName.html col.getName()
            col.tagOrderText.html col.getOrder()+1
            if col.getVisible()
                col.tagCheck.html @imgChecked
                col.tag.removeClass "notVisible"
            else
                col.tagCheck.html @imgNotChecked
                col.tag.addClass "notVisible"

            col.tagLock.html "<i class='far fa-lock-alt'></i>"
            if col.getLocked()
                col.tagLock.removeClass "unlocked"
            else
                col.tagLock.addClass "unlocked"

            col.tag.setClass "calculation", col.getIsCalculation()

        true

    onClickVisible: (e)=>
        for col in @columns
            if col.getAlwaysHidden() then continue
            if col.getSource() != e.path then continue
            DataMap.changeColumnAttribute @tableName, e.path, "visible", (col.getVisible() == false)
            @updateColumnText()

    onClickLock: (e) =>
        for col in @columns
            if col.getSource() != e.path then continue
            console.log "lock: ", col
            DataMap.changeColumnAttribute @tableName, e.path, "locked", (col.getLocked() == false)
            @updateColumnText()
        true

    ##
    ## refresh all columns in this View
    refreshColumns: () =>
        if !@tableName? then return false 
        @sortItemsList.getTag()?.empty()
        @setTable @tableName

    ## @param [String] message the message to show in the modal as message
    ##
    setTable: (@tableName) ->

        ##|
        ##|  List is an array
        ##|  with each element has name, order, active
        ##|

        @sortItemsList = new WidgetTag(@sortedItemsListHolder)

        ##|
        ##|  Get the columns
        @columns = DataMap.getColumnsFromTable(@tableName)
        @columns = @columns.sort (a, b)->
            return a.getOrder() - b.getOrder()

        for col in @columns
            if col.getAlwaysHidden() then continue

            col.tag          = @sortItemsList.add "li", "columnItem"
            col.gid          = col.tag.gid

            col.tagCheck     = col.tag.add "div", "colVisible"
            col.tagName      = col.tag.add "div", "colName"
            col.tagLock      = col.tag.add "div", "colLock"
            col.tagOrderText = col.tag.add "div", "orderText"

            col.tagCheck.setDataPath col.getSource()
            col.tagCheck.on "click", @onClickVisible
            col.tagLock.setDataPath col.getSource()
            col.tagLock.on "click", @onClickLock

        @updateColumnText()

        sortable this.sortedItemsListHolder,
            forcePlaceholderSize: true
            placeholderClass:     'placeholder'

        sortable(this.sortedItemsListHolder)[0].addEventListener 'sortupdate', (e)=>

            order = 0
            for el in @sortItemsList.el.children()
                id = $(el).data("id")

                for col in @columns
                    if col.getAlwaysHidden() then continue
                    if col.gid != id then continue

                    oldOrder = col.getOrder()
                    if oldOrder != order
                        DataMap.changeColumnAttribute @tableName, col.getSource(), "order", order
                        # console.log "Change #{col.getSource()} order from #{oldOrder} to #{order}"

                order++ 

            @updateColumnText()
            true

        @onButton1 = (e)=>
            @hide()
            true

        @onButton2 = ()=>
            @hide()
            true

    onShowScreen: () =>
        GlobalClassTools.addEventManager(this)
        DataMap.getDataMap().on "table_change", (tableName, data) =>
            if tableName is @tableName 
                @refreshColumns()
        globalTableEvents.on "table_change", (tableName, sourceName, field, newValue) => 
            if tableName is @tableName and @validFields.indexOf(field) >= 0
                @refreshColumns()
        true

    serialize: () =>
        return
            table: @tableName

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewTableRearrange: invalid data passed"

            if options? and options.table?
                @setTable(options.table)

            resolve true
