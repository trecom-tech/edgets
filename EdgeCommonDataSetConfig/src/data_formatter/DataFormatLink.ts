// ###
// ### class for simpleobject data type
// ###
// ### @extends [DataFormatterType]
// ###
import { DataFormatterType } from './DataFormatterType';

export class DataFormatLink extends DataFormatterType {
    name: string        = 'link';
    width: number       = 70;
    input_width: number = 70;
    clickable: boolean  = true;
    align: string       = 'left';

    constructor(...args: any[]) {
        super();
    }

    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): null {

        // ### TODO:  Open a dialog to edit the link and validate it
        console.log("TODO: openEditor not implemented for link");
        return null;
    }

    // ###
    // ### funtion to format the currently passed data
    // ###
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additonal options defined for the datatype
    // ### @param [String] path path where the value is being edited
    // ### @return [Object] data formatted data
    // ###
    format(data: any, options: any, path: any) {
        if ((data == null)) {
            return "";
        }
        if (/www/.test(data)) {
            return "Open Link";
        }
        if (/^http/.test(data)) {
            return "Open Link";
        }
        if (/^ftp/.test(data)) {
            return "Open FTP";
        }
        if (data.length > 0) {
            return data;
        }
        return "";
    }


    // ###
    // ### funtion to unformat the currently formatted data
    // ###
    // ### @param [Object] data data to be unformatted
    // ### @param [String] path path where the value is being edited
    // ### @return [Object] data unformatted data
    // ###
    unformat(data: any, path: any) {
        // console.log "TODO: DataFormatLink.unformat not implemented:", data
        return data;
    }

    onFocus(e: any, col: any, data: any) {
        // console.log "click, col=", col, "data=", data
        const url = data[col];
        if ((url != null) && (url.length > 0)) {
            const win = window.open(url, "_blank");
            win.focus();
        }

        return true;
    }
}

export default DataFormatLink;