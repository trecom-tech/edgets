"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// ###
// ### class for simpleobject data type
// ###
// ### @extends [DataFormatterType]
// ###
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatLink extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'link';
        this.width = 70;
        this.input_width = 70;
        this.clickable = true;
        this.align = 'left';
    }
    openEditor(elParent, left, top, width, height, currentValue, path) {
        // ### TODO:  Open a dialog to edit the link and validate it
        console.log("TODO: openEditor not implemented for link");
        return null;
    }
    // ###
    // ### funtion to format the currently passed data
    // ###
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additonal options defined for the datatype
    // ### @param [String] path path where the value is being edited
    // ### @return [Object] data formatted data
    // ###
    format(data, options, path) {
        if ((data == null)) {
            return "";
        }
        if (/www/.test(data)) {
            return "Open Link";
        }
        if (/^http/.test(data)) {
            return "Open Link";
        }
        if (/^ftp/.test(data)) {
            return "Open FTP";
        }
        if (data.length > 0) {
            return data;
        }
        return "";
    }
    // ###
    // ### funtion to unformat the currently formatted data
    // ###
    // ### @param [Object] data data to be unformatted
    // ### @param [String] path path where the value is being edited
    // ### @return [Object] data unformatted data
    // ###
    unformat(data, path) {
        // console.log "TODO: DataFormatLink.unformat not implemented:", data
        return data;
    }
    onFocus(e, col, data) {
        // console.log "click, col=", col, "data=", data
        const url = data[col];
        if ((url != null) && (url.length > 0)) {
            const win = window.open(url, "_blank");
            win.focus();
        }
        return true;
    }
}
exports.DataFormatLink = DataFormatLink;
exports.default = DataFormatLink;
//# sourceMappingURL=DataFormatLink.js.map