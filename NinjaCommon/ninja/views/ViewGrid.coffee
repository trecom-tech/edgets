
class ViewGrid extends View
	
	@handlerHeight: 28
	handlerIcon: "<i class='far fa-list-alt'></i>"
	padding: 20
	
	constructor: (optionData = {}) ->
		super()

		defaultOptionData = 
			#animate: true
			auto: true
			width: 12
			float: true
			verticalMargin: 10
			alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
			resizable:
				handles: 'e, se, s, sw, w'
			draggable:
				handle: '.gs-handler'

		@optionData = jQuery.extend true, defaultOptionData, optionData

	getDependencyList: ()=>
		return [ "/vendor/lodash.min.js", "/vendor/gridstack/gridstack.all.js", "/vendor/gridstack/gridstack.min.css" ]

	setData: () =>
		# console.log "ViewGrid setData"

	onShowScreen: ()=>

		@panels       = {}
		@panelCount   = 0
		@activePanel  = null

		@gid = GlobalValueManager.NextGlobalID()

		@gsWrapper = @addDiv "grid-stack", "grid-stack-#{@gid}"

		$("#grid-stack-#{@gid}").gridstack(@optionData)
		@grid = $("#grid-stack-#{@gid}").data('gridstack')
		$("#grid-stack-#{@gid}").on('change', (event, items) =>
			@updatePanels()
		)
		@panelData 	= {}
		## - gao
		## Allow this view to have events
		GlobalClassTools.addEventManager(this)

	getInsideWidth: (panel)=>
		return panel.content.el.width()

	getInsideHeight: (panel)=>
		return panel.content.el.height() - ViewGrid.handlerHeight

	updatePanels: ()=>

		for id, panel of @panelData

			if @getInsideHeight(panel) == 0
				panel.content.hide()
			else if panel.body.move?
				# console.log "Content size", @getInsideWidth(panel), panel.content.el.outerWidth()
				panel.body.move 0, panel.content.el.height() - @getInsideHeight(panel), @getInsideWidth(panel), @getInsideHeight(panel)
			if panel.body.childView?
				panel.body.childView.onResize @getInsideWidth(panel), @getInsideHeight(panel)

		true

	enableResize: (name, bool) =>
		panel = @panels[name]
		if !panel? then return false
		panel.disable_resize = !bool
		@grid.resizable panel.wrapper.getTag(), bool 
		true

	getCellHeight: () =>
		@grid.cellHeight()

	setCellHeight: (h) =>
		if !h? or h <= 0 then return false
		@grid.cellHeight h
		@updatePanels()
		true

	getCellWidth: () =>
		@grid.cellWidth()

	setGridWidth: (gridWidth, doNotPropagate) =>
		if !gridWidth? or gridWidth < 1 or gridWidth > 12 then return false
		@grid.setGridWidth gridWidth, doNotPropagate
		@updatePanels()
		true

	addWidget: (panelName, options = {}) =>

		if @panels[panelName]? then return @panels[panelName]
		defaultOptions = 
			x: 0
			y: 0
			width: 3
			height: 3
			handlericon: @handlerIcon

		options = jQuery.extend true, defaultOptions, options
		id = "panel#{@panelCount++}"

		if !@grid then return console.log("Grid isn't initialized correctly..")

		elWidget = @gsWrapper.addDiv "grid-stack-item", "grid-stack-item-#{@gsWrapper.getChildren().length}"
		elContent = elWidget.addDiv "grid-stack-item-content grid-radius-border"
		@grid.addWidget elWidget.el, options.x, options.y, options.width, options.height

		handlerWidget = elContent.addDiv "gs-handler border-titlebar"
		handlerWidget.height ViewGrid.handlerHeight
		titleWidget = handlerWidget.addDiv "gs-title"
		titleWidget.html "#{@handlerIcon}#{panelName or ''}"
		closeBtnWidget = handlerWidget.addDiv "gs-close-button"
		closeBtnWidget.html "<i class='far fa-times'></i>"
		closeBtnWidget.on "click", (e) =>
			@panelData[id].close()
			true

		bodyWidget = elContent.addDiv "gs-view-body"
		bodyWidget.height elContent.height() - ViewGrid.handlerHeight
		bodyWidget.onResize = (w, h) ->
			this.setSize w, h

		@panelData[id] =
			name 	:	panelName || id
			id 		:	id
			options : 	options
			parent	: 	this
			wrapper :   elWidget
			content : 	elContent
			body 	: 	bodyWidget
			show : ()=>
				@activePanel = id
				@updatePanels()
			close: () =>
				if elWidget.childView? and elWidget.childView.removeView?
					elWidget.childView.removeView()
				elWidget.destroy()
				delete @panelData[id]
				delete @panels[panelName]
				@updatePanels()

		if panelName?
			@panels[panelName] = @panelData[id]
		@updatePanels()

		@panelData[id]

	addView: (viewName, others...) =>
		if typeof others[0] is "object"
			options = others[0]
			callbackWithView = others[1]
		else if typeof others[0] is "function"
			callbackWithView = others[0]
			options = null

		new Promise (resolve, reject) =>
			name = options?.name
			gid = GlobalValueManager.NextGlobalID()
			panel = @addWidget name, options

			##
			## Save this view options for future use
			if !@addedViews? then @addedViews = {}
			if @addedViews[name] then return resolve(@addedViews[name])
			@addedViews[panel.name] = $.extend options, { name: panel.name }
			panel.body.setView viewName, callbackWithView
			.then (view)=>
				# console.log "GridStack added: #{viewName}"
				view.elHolder = panel.body.el
				resolve view

			true


	setName: (strName) =>
		@serializedData = _.map $('.grid-stack > .grid-stack-item:visible'), (el) =>
			el = $(el)
			node = el.data('_gridstack_node')
			res =
				x: node.x
				y: node.y
				width: node.width
				height: node.height

		# console.log "ViewGrid serializedData: ", @serializedData
		localStorage.setItem "gridSetting#{strName}", @serializedData
		@emitEvent "settingsChange", [ "#{strName}", @serializedData ]

	setHandlerIcon: (@handlerIcon) =>
		true

	setPadding: (@padding) =>
		@el.css "padding", "#{@padding}px"
		true

	onResize: (w, h) =>
		super(w,h)

		# console.log "Resizing ViewGrid!!!: ", w, h
		## set maximum height of grid
		cellHeight = @optionData.cellHeight || 60
		verticalMargin = @optionData.verticalMargin
		maxHeight = parseInt(h / (cellHeight + verticalMargin))
		@grid.batchUpdate()
		@gsWrapper.setAttribute 'data-gs-height', maxHeight
		@grid.commit()
		## resize view-body
		for id, panel of @panelData
			panel.content?.resetCached()
			panel.body?.onResize panel.content.width(), panel.content.height() - ViewGrid.handlerHeight

		true

	serialize: () =>
		panels = []
		for id, p of @panelData
			if p.options?.view?
				panels.push p.options
			else
				panels.push $.extend(p.options, { title: p.name, disable_resize: p.disable_resize })
		return
			handlericon: @handlericon
			padding: @padding
			grids: panels

	##
	## with this function, getLayout() can use this view
	deserialize: (options)=>

		newPromise ()=>
			##
			## Set default handler icon for general items
			if options.handlericon?
				@setHandlerIcon options.handlericon

			if options.padding?
				@setPadding options.padding

			if options.grids?

				for item in options.grids

					title  = "Unknown"
					x      = 0
					y      = 0
					width  = 3
					height = 3
					if item.title? then title = item.title
					if item.x? then x = item.x
					if item.y? then y = item.y
					if item.width? then width = item.width
					if item.height? then height = item.height					

					if item.view?
						@addView item.view, item
					else					
						widget = @addWidget title, 
							x:      x
							y:      y
							width:  width
							height: height
							handlericon: item.handlericon or @handlerIcon

					if item.disable_resize?
						@enableResize title, !item.disable_resize

			return true
