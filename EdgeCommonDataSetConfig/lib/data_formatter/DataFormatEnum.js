"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for enum data type
//
// @extends [DataFormatterType]
//
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
class DataFormatEnum extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'enum';
        this.align = 'left';
    }
    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        //
        //  Show a popup menu
        // @ts-ignore
        const p = new PopupMenu("Options", left, top);
        if (typeof this.options === "string") {
            this.options = this.options.split(",");
        }
        if ((typeof this.options === "object") && (typeof this.options.length === "number")) {
            for (let i in this.options) {
                // console.log "Adding[", i, "][", o, "]"
                const o = this.options[i];
                p.addItem(this.format(o, this.options), (coords, data) => {
                    // console.log "Saving[", data, "]"
                    return this.saveValue(data);
                }, o);
            }
        }
        else {
            console.log("Invalid options: ", this.options);
        }
        return true;
    }
    //
    //  Attach to an existing text input.
    //  Hide the text input and add the select statement
    //  and then use selectize https://github.com/selectize/selectize.js/blob/master/docs/api.md
    attachEditor(elSourceField, path) {
        let name;
        let value;
        super.attachEditor(elSourceField, path);
        const options = DataFormatCommon_1.DataFormatCommon.getOptions(elSourceField.data("options"));
        const config = {};
        config.valueField = "id";
        config.labelField = "title";
        config.searchField = "title";
        config.options = [];
        config.items = [];
        const currentValue = elSourceField.val();
        if ((currentValue != null) && (currentValue !== "")) {
            config.items.push(currentValue);
        }
        if (Array.isArray(options.items)) {
            for (name of Array.from(options.items)) {
                config.options.push({ id: name, title: this.format(name) });
            }
        }
        else {
            for (name in options.items) {
                value = options.items[name];
                config.options.push({ id: value, title: this.format(name) });
            }
        }
        //
        //  Copy forward any config
        for (name in options) {
            value = options[name];
            if (name === "items") {
                continue;
            }
            config[name] = value;
        }
        config.onChange = (newValue) => {
            elSourceField.val(newValue);
            return true;
        };
        //
        elSourceField.hide();
        elSourceField.elSelect = $("<select />");
        elSourceField.parent().prepend(elSourceField.elSelect);
        const selectList = elSourceField.elSelect.selectize(config);
        // console.log "DataFormatEnum Create with options:", config
        elSourceField.elSelect.on("change", function (e) {
            elSourceField.change();
            return true;
        });
        elSourceField.val = (newValue) => {
            if ((newValue == null)) {
                return selectList[0].selectize.getValue();
            }
            // console.log "enum val(): ", newValue
            selectList[0].selectize.setValue(newValue, true);
            return newValue;
        };
        return true;
    }
    //
    // In this case, the options is an array or a comma seperated list of values
    // data must be one of those values of a numeric index to those values.
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        let i;
        let o;
        this.options = options;
        if (typeof this.options === "string") {
            this.options = this.options.split(/\s*,\s*/);
        }
        let result = data;
        if ((data == null)) {
            return "";
        }
        for (const i in this.options) {
            o = this.options[i];
            if (data === o) {
                result = o;
            }
        }
        for (const i in this.options) {
            o = this.options[i];
            if (`${data}` === `${i}`) {
                result = o;
            }
        }
        return result;
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        return data;
    }
    getValueInTooltip(value) {
        return this.format(value);
    }
}
exports.DataFormatEnum = DataFormatEnum;
exports.default = DataFormatEnum;
//# sourceMappingURL=DataFormatEnum.js.map