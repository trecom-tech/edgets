import config           from 'edgecommonconfig';
import EdgeRequest      from 'edgecommonrequest'
import EdgeApi          from 'edgeapi';
import EdgeMessageQueue from 'edgecommonmessagequeue';

//
//  Reference to the global cache for the block manager
let globalBlockManager: any = null;

//
//  The Block Manager is an interface that can dynamic load a "Block" at runtime
//  by loading the module and then keeping it loaded in a cache for future calls
//
class BlockManager {
    blocks: any                 = {};
    static blockPaths: string[] = [
        "./",
        "../",
        "./node-modules/",
        "../node-modules/",
        process.env.HOME + "/EdgeData/Blocks/"
    ];

    load(name: string, obj: any) {

        this.blocks[name] = new obj();
        return true;
    }

    run(name: string, ...param: any[]) {

        try {

            if ((this.blocks[name] == null)) {

                //
                //  Try to find the block in coffee or js file in one of these available
                //  path names and if found, load it.
                //
                let filename = config.FindFileInPath(name + ".coffee", BlockManager.blockPaths);

                config.status(`BlockManager run name=${name}, searching in ${BlockManager.blockPaths.join('m')}: ${filename}`);

                if (filename === null) {
                    filename = config.FindFileInPath(name + ".js", BlockManager.blockPaths);
                    config.status(`BlockManager run name=${name}, searching for .js in ${BlockManager.blockPaths.join('m')}: ${filename}`);
                }

                if (filename === null) {
                    config.status(`BlockManager run name=${name} block file not found`);
                    throw new Error(`Unable to find block ${name}`);
                }

                filename          = filename.replace(".coffee", "").replace(".js", "");
                const lib         = require(filename);
                this.blocks[name] = new lib(name);
            }

            return this.blocks[name].process.apply(this.blocks[name], param);

        } catch (e) {

            return console.log("BlockManager exception:", e);
        }
    }
}

class EdgeBlock {
    blockName: string;
    log: any;
    api: any;
    cacheMessageQueue: any;

    static getBlockManager() {

        if (globalBlockManager === null) {
            globalBlockManager = new BlockManager();
        }

        return globalBlockManager;
    };

    constructor(blockName?: string) {
        this.blockName = blockName;
        if ((this.blockName == null)) {
            this.blockName = this.constructor.name;
        }
        this.log = config.getLogger(`block-${this.blockName}`);
        this.api = null;
    }

    doGetApi() {
        return EdgeApi.doGetApi();
    }

    //
    //  Given a URL it will use the existing options to download it.
    //
    doGetLink(dataUrl: string, qs?: any) {

        const r = new EdgeRequest();
        return r.doGetLink(dataUrl, qs);
    }

    //
    //  Given a mongo object ID, return a date based on when it was created
    //
    dateFromObjectId(objectId: any) {
        return new Date(parseInt(objectId.toString().substring(0, 8), 16) * 1000);
    }

    //
    //  General error function to report one or more errors in the
    //  attempt to process the block data.
    //
    setError(message: string, optionalData: any) {

        config.status(`EdgeBlock ${this.blockName} error: ${message}`);
        this.log.error(message, optionalData);
        return false;
    }

    //
    //  Send a record to a queue for additional processing
    //
    sendToQueue(queueName: string, data: any) {

        return new Promise((resolve, reject) => {

            const strBuffer = JSON.stringify(data);
            config.status(`EdgeBlock ${this.blockName} sendToQueue ${queueName}:`, strBuffer);

            if ((this.cacheMessageQueue == null)) {
                this.cacheMessageQueue = {};
            }

            if ((this.cacheMessageQueue[queueName] == null)) {
                this.cacheMessageQueue[queueName] = 1;

                EdgeMessageQueue.doOpenMessageQueue(queueName)
                    .then((mq: any) => {
                        this.cacheMessageQueue[queueName] = mq;
                        this.cacheMessageQueue[queueName].doSubmitBuffer(strBuffer);
                        resolve(true);
                    })
                    .catch((err: Error) => {
                        console.log(err);
                        reject(err);
                    });

            } else {

                if (typeof this.cacheMessageQueue[queueName] === "number") {
                    setTimeout(this.sendToQueue, 100, queueName, data);
                    resolve(true);

                } else {

                    this.cacheMessageQueue[queueName].doSubmitBuffer(strBuffer);
                    resolve(true);
                }
            }

            return true;
        });
    }

    //
    //  Add stats for the day key a key as an object or scalar
    async updateStatsToday(name: string, keys: any) {

        if ((this.api == null)) {

            const api = await EdgeApi.doGetApi();
            this.api  = api;
            await this.updateStatsToday(name, keys);

        } else {

            if (typeof keys !== "object") {
                const tmp: any = {};
                tmp[keys]      = 1;
                keys           = tmp;
            }

            for (let keyName in keys) {
                const keyValue = keys[keyName];
                this.api.stats_doAddStats("today", name, keyName, keyValue);
            }
        }

        return true;
    }
}

export default EdgeBlock;
