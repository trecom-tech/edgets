# EdgeCommonMessageQueue
> A general interface to the Edge message pub/sub system.  Allows pub/sub and consuming.

    npm install --save ssh+git://git@gitlab.protovate.com:Edge/EdgeCommonPubSub
    import { EdgeMQTTController } from 'edgecommonpubsub'; // For MQTT Controller

This wrapper provides the general functions needed to quickly read or write with a mqtt.

## How to setup credentials
Please add `mqttHost` variable to credentials for MQTT.

## MQTT Tutorial
### Opening a message queue by name

Send message to a topic

    await EdgeMQTTController.doSubmitBuffer('topic', someObject);

Subscribe to a topic

    await EdgeMQTTController.consume('topic', (event) => {
        // This callback function is triggered when new message is received
    });


### Setup Mosquito for testing

Install Mosquitto Broker

    sudo apt-get update
    sudo apt-get install mosquitto

Install the Clients and Test

    sudo apt-get install mosquitto-clients

    mosquitto_sub -h localhost -t test

Open another terminal and run following command

    mosquitto_pub -h localhost -t test -m "hello world"

You should see hello world pop up in the other terminal.


### Run tests locally with local rabbitmq and mosquitto server

    npm run test

### Testing using docker

    docker-compose build
    docker-compose up
