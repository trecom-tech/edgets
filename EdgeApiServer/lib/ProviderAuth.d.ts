import IApiUser from './interfaces/IApiUser';
import DataSetManager from 'edgedatasetmanager';
import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
import MongoModel from 'edgedatasetmanager/lib/MongoModel';
import IApiUserOffline from './interfaces/IApiUserOffline';
import * as LRU from 'lru-cache';
export default class ProviderAuth extends ApiProvider {
    globalAuthTokensCache: LRU<unknown, unknown>;
    dataStore: {
        [dataSetName: string]: DataSetManager;
    };
    internalCollections: any;
    constructor(prototype: ApiProvider);
    onInit(): void;
    getBaseName(): string;
    /**
     *
     * Authenticate the user and set values in the persistant user object for future use.
     * @see [Data Model:Master / User](https://brians.atlassian.net/wiki/spaces/OBVIO/pages/258342984/Data+Model+Master+User)
     *
     * @param user - The IApiUser object of a connected user
     * @param username - A username to validate
     * @param passwordHash - A hashed version of the user's password for comparison
     * @param system - The name of the system the user is trying to login to.  Used to validate the user is a member of that system and load Roles.
     * @return object - authentication information
     */
    doAuthUser(user: ApiSocketUser, username: string, passwordHash: string, system: string): Promise<any>;
    /**
     *
     * Authenticate the user using token.
     * @see [Data Model:Master / User](https://brians.atlassian.net/wiki/spaces/OBVIO/pages/258342984/Data+Model+Master+User)
     *
     * @param user - The IApiUser object of a connected user
     * @param strToken - auth token to login
     * @return object - authentication information
     */
    doAuthFromToken(user: ApiSocketUser, strToken: string): Promise<any>;
    doTestUser(user: IApiUser): string;
    /**
     * Change the password for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param newPasswordHash - True to indicate that the password has been changed or an error message
     */
    doChangePassword(user: ApiSocketUser, username: string, newPasswordHash: string): Promise<any>;
    /**
     * Change the password for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     */
    doCreateUser(user: ApiSocketUser, username: string): Promise<any>;
    /**
     * Update the information for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param newData - The user data to update
     */
    doUpdateUser(user: ApiSocketUser, username: string, newData: any): Promise<any>;
    /**
     * Update the information for a given user
     * @param user - currently logged in user
     * @param username - The username to locate and update
     * @param system - The system to locate and update
     * @param rolesAdd - The username to locate and update
     * @param rolesRemove - The user data to update
     */
    doUpdateUserRoles(user: ApiSocketUser, username: string, system: string, rolesAdd: string[], rolesRemove: string[]): Promise<any>;
    /**
     * return list of groups based on prefix filter
     * @param user - currently logged in user
     * @param prefix - prefix to filter group
     */
    doGroupList(user: ApiSocketUser, prefix: string): Promise<any[]>;
    /**
     * create a new group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param description - description of the group to create
     */
    doGroupCreate(user: ApiSocketUser, name: string, description: string): Promise<{
        id: string;
        name: string;
        description: string;
        created_by: string;
        first_modified: Date;
    }>;
    /**
     * add new role(s) to a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param strRole - role string or array of string
     */
    doGroupAddRole(user: ApiSocketUser, name: string, system: string, strRole: string | string[]): Promise<boolean>;
    /**
     * remove roles for a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param strRole - role string or array of string
     */
    doGroupRemoveRole(user: ApiSocketUser, name: string, system: string, strRole: string | string[]): Promise<boolean>;
    /**
     * add user to a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param username- username to add to a group
     */
    doGroupAddUser(user: ApiSocketUser, name: string, system: string, username: string): Promise<boolean>;
    /**
     * remove user from a group
     * @param user - currently logged in user
     * @param name - name of the group to create
     * @param system - system
     * @param username- username to add to a group
     */
    doGroupRemoveUser(user: ApiSocketUser, name: string, system: string, username: string): Promise<boolean>;
    /**
     *
     * @param user - logged in user
     * @param targetUsername - username to update
     * @param role - role name to verify
     */
    verifyUserRole(user: ApiSocketUser, targetUsername: string, role: string, system?: string): void;
    internalGetCollection(dataset?: string, collection?: string): Promise<MongoModel>;
    /**
     * Get the reference to the data set
     * @param datasetName
     */
    internalGetDataset(datasetName: string): DataSetManager;
    /**
     * Get the random password
     * @param length
     */
    internalGeneratePassword(length?: number): string;
    doInternalLoadUser(username: string): Promise<IApiUserOffline>;
    doInternalLoadGroup(name: string): Promise<any>;
    doInternalGetAllGroups(prefix?: string): Promise<any[]>;
    doInternalMergeRoles(original: any, target: any): Promise<void>;
}
