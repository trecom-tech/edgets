## -------------------------------------------------------------------------------------------------------------
## function to open globalEditor which is simple textbox
##
## @param [Element] e the element in which the editor to create
## @return [Boolean]
##
DataSetConfig = require 'edgecommondatasetconfig'

globalOpenEditor = (e) ->
	##|
	##|  Clicked on an editable field
	#console.log "GlobalOpenEditor:", e
	data = WidgetTag.getDataFromEvent(e)
	#console.log "GlobalOpenEditor Data:", data
	path = data.path
	DataMap.getDataMap().editValue path, e.target
	false

root = exports ? this

## -------------------------------------------------------------------------------------------------------------
## class DataMap
## this is the class to handle and map the data into application including custom data types and data for that datatypes
##
class DataMap

	## -------------------------------------------------------------------------------------------------------------
	## constructor
	##
	constructor: (@dataSetName)->

		# @property [Object] data to save the data objects
		@data     = {}

		# @property [Object] types to save datatypes for the column in table
		@types    = {}

		# @property [Function] onSave function to be called when edited data is saved
		@onSave   = {}

		# @property [Object] objStore object store
		@objStore = {}

		##|  Cached values for display
		@cachedFormat = {}

		## Cached time formats
		@cachedTimeFormats = []

		@engine = new DataMapEngine()
		@engine.on "change", (diff, a, b)=>

			if diff.kind == "E"
				@updateScreenPathValue diff.path, diff.lhs, true

		GlobalClassTools.addEventManager(this)

		## reset time format data every minute
		setInterval @expireCache, 60000

	## -------------------------------------------------------------------------------------------------------------
	## function to get singleton global instance of data map
	##
	## @return [DataMap] globalDataMap global instance of dataMap
	##
	@getDataMap: () =>
		##
		if !root.globalDataMap
			root.globalDataMap = new DataMap()

		return root.globalDataMap

	## ===[ Utilities / helper functions on the data ]===

	## -------------------------------------------------------------------------------------------------------------
	## initiate the edit of the data using editor defined in the data type
	##
	## @param [String] path indicating the location of the data beign edited
	## @param [JqueryElement] el element in which to open editor
	## @return [Boolean]
	##
	editValue: (path, el) =>

		parts     = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		fieldName = parts[3]

		existingValue = @engine.getFast tableName, keyValue, fieldName
		col 		  = DataMap.getTypes(tableName).col[fieldName]
		formatter     = col.getFormatter()

		## if this column is linked to other table
		## it has values only valid in the table
		if col.getLinked()
			t = new TypeaheadInput el, col.getLinked(), ["id"]
			t.on "change", (val) ->
				# console.log "Linked Table's id: ", val
				DataMap.getDataMap().updatePathValueEvent path, val
			t.onFocus()
			return true

		##|
		##|  Fix the options in the global formatter object
		formatter.options = DataMap.getTypes(tableName).col[fieldName].getOptions()

		#console.log "editValue path=#{path} table=#{tableName}, keyValue=#{keyValue}, field=#{fieldName}"
		#console.log "Formatter:", formatter
		f = col.getEditFunction() 
		if f?
			f(el, existingValue, path, @updatePathValueEvent)
		else 
			formatter.editData el, existingValue, path, @updatePathValueEvent
		return true

	## -------------------------------------------------------------------------------------------------------------
	## update the occurence of the path value with the updated value on currently rendered screen
	##
	## @param [String] path indicating the location of the data to be updated
	## @param [Object] newValue new value to be set instead of old data
	## @param [Boolean] didDataChange if data is different from the previous value
	## @return [Boolean]
	##
	updateScreenPathValue: (path, newValue, didDataChange) =>

		##|
		##|  Broadcast the data change event
		if globalKeyboardEvents? and didDataChange
			dm = DataMap.getDataMap()
			globalKeyboardEvents.emitEvent "change", [path, newValue]

		true

	##|
	##|  Called from server push events
	@newDataEvent: (tablename, fieldname, newData, e)=>

		if globalKeyboardEvents?
			globalKeyboardEvents.emitEvent "new_data", [tablename, fieldname, newData, e]

		true

	##|
	##|  Update a value stored in the engine.
	##|  This can be called when some external event wants to update a path
	##|  without causing save events to trigger
	##|
	@updatePathValue: (tablename, fieldname, keyvalue, newValue) =>

		dm = DataMap.getDataMap()

		tableTypes = DataMap.getTypes(tablename)
		if !tableTypes? then return false

		if fieldname != "row_selected" and fieldname != "row_flagged"
			col = tableTypes.col[fieldname]
			if !col? then return false

		dm.engine.setFast tablename, keyvalue, fieldname, newValue

		##|  Already cached so it's on display or has been, thus update it
		##|
		delete dm.cachedFormat["/#{tablename}/#{keyvalue}/#{fieldname}"]
		globalKeyboardEvents.emitEvent "change", ["/#{tablename}/#{keyvalue}/#{fieldname}", newValue]

		true

	## -------------------------------------------------------------------------------------------------------------
	## works as the event triggered at the update of the value
	##
	## @param [String] path indicating the location of the data to be updated
	## @param [Object] newValue new value to be set instead of old data
	## @event updatePathValueEvent
	## @return [Boolean]
	##
	updatePathValueEvent: (path, newValue) =>

		##|
		##|  Works just like updatePathValue except that an event is triggered (onSave) if available
		parts = path.split '/'
		tableName = parts[1]
		keyValue  = parts[2]
		fieldName = parts[3]

		##|
		##|  Remove any cached values that happen to exist
		dm = DataMap.getDataMap()
		for col in DataMap.getColumnsFromTable(tableName)
			delete dm.cachedFormat["/#{tableName}/#{keyValue}/#{col.getSource()}"]

		existingValue = @engine.getFast tableName, keyValue, fieldName

		##| check if the existing type is boolean
		if typeof existingValue == 'boolean' and existingValue == Boolean(newValue) then return true
		if existingValue == newValue then return true

		dm.engine.setFast tableName, keyValue, fieldName, newValue
		@updateScreenPathValue path, newValue, true

		if @onSave[tableName]?
			@onSave[tableName](keyValue, fieldName, existingValue, newValue)

		true

	## ===[ Events ]===

	## -------------------------------------------------------------------------------------------------------------
	## Call this function to set a callback when an editable field changes
	## due to an inline editor.   The callback receives
	## Table Name, Key, Old Value, New Value
	##
	## @param [String] tableName tableName to associate callback with
	## @param [Function] callbackFunction funtion to execute on save
	## @return [Boolean]
	##
	@setSaveCallback: (tableName, callbackFunction) =>

		dm = DataMap.getDataMap()
		dm.onSave[tableName] = callbackFunction

		true

	## ===[ Dealing with Schema (column definitions) ]===

	## -------------------------------------------------------------------------------------------------------------
	## Import the given data types from a saved selection
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Object] columns containing data types
	##
	@importDataTypes: (tableName, savedConfig) =>
		dm = DataMap.getDataMap()
		DataMap.setTypes tableName

		for sourceName, obj of savedConfig
			if sourceName == "_lastModified" then continue
			if typeof obj != "object" then continue
			DataMap.getTypes(tableName).unserialize([ obj ], true)

		dm.resetCache()
		globalTableEvents.emitEvent "table_change", [ tableName.replace(/;.*/, "") ]
		# console.log  "importDataTypes table=#{tableName}:", DataMap.getTypes(tableName)
		true

	## -------------------------------------------------------------------------------------------------------------
	## Export data types from a table
	##
	## @param [String] tableName table name from which data types are exported
	##
	@exportDataTypes: (tableName) =>
		try
			return DataMap.getTypes(tableName).serialize()
		catch e
			console.log "Error exporting data types from #{tableName}"
			return false
		{}

	##
	## Save table column settings to local storage
	##
	@saveDataTypesToLocal: (tableName, saveName) =>
		try
			dataTypes = DataMap.exportDataTypes tableName
			localStorage.setItem(saveName or "table_col_settngs_#{tableName}", JSON.stringify(dataTypes))
		catch e
			console.log "Error saving column settings of #{tableName} to local storage", e
			return false
		true

	##
	## Restore table column settings from local storage
	##
	@restoreDataTypesFromLocal: (tableName, saveName) =>
		try
			dataTypes = JSON.parse(localStorage.getItem(saveName or "table_col_settngs_#{tableName}"))
			DataMap.importDataTypes tableName, dataTypes
		catch e
			console.log "Error restoring columns settings from local storage: ", e
			return false
		true

	## -------------------------------------------------------------------------------------------------------------
	## Set the data type for a given type of data Called statically to
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Object] columns array of the columns to set as data type
	## @return [Boolean]
	##
	@setDataTypes: (tableName, columns) =>

		dm = DataMap.getDataMap()

		if !DataMap.getTypes(tableName)?
			DataMap.setTypes tableName

		DataMap.getTypes(tableName).unserialize columns

		# console.log "ADDING:", tableName, columns
		# console.log DataMap.getTypes(tableName)
		true

	##|
	##|  Remove the table's data but leave all column types
	##|
	@removeTableData: (tableName) =>
		dm = DataMap.getDataMap()
		dm.engine.eraseCollection(tableName)
		dm.cachedFormat = {}
		dm.cachedTimeFormats = []

		##
		## emit event "new_data" so that everyone can know
		## that new data is added to table
		DataMap.sendNewDataNotification tableName, null			

		return true

	##|
	##|  Entirely remove a table including all data and all
	##|  data types.
	##|
	@removeTable: (tableName) =>

		dm = DataMap.getDataMap()
		dm.engine.eraseCollection(tableName)

		if dm.types? and dm.types[tableName]?
			delete dm.types[tableName]

		dm.cachedFormat = {}
		dm.cachedTimeFormats = []
		return true

	##|
	##|  Add a column data type
	@addColumn: (tableName, options) =>

		config =
			name        : "New Column"
			source      : "newcol"
			visible     : true
			hideable    : false
			editable    : false
			sortable    : true
			required    : false
			align       : "left"
			type        : "text"
			width       : null
			tooltip     : ""
			render      : null
			calculation : false

		$.extend config, options

		DataMap.setDataTypes tableName, [ config ]
		dm = DataMap.getDataMap()

		##|
		##|  Save this new column entirely
		##|
		saveText = DataMap.getTypes(tableName).serialize()
		dm.emitEvent "table_change", [tableName, saveText]
		return DataMap.getTypes(tableName).col[config.source]

	## -------------------------------------------------------------------------------------------------------------
	## Reset the columns for a table based on some object
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Object] columns array of the columns to set as data type
	##
	@setDataTypesFromObject: (tableName, objects) =>

		dm = DataMap.getDataMap()
		DataMap.setTypes tableName

		updated = false
		for i, o of objects
			if dm.setDataTypesFromSingleObject(tableName, o)
				updated = true

		if updated
			dm.cachedFormat = {}
			dm.cachedTimeFormats = []
		return updated

	setDataTypesFromSingleObject: (tableName, newData)=>

		if !DataMap.getTypes(tableName)?
			#@types[tableName] = new DataSetConfig.Table(tableName)
			DataMap.setTypes tableName

		##|
		##|  Returns updated = true if the data type(s) found in this table
		##|  are new or different than before.
		##|  TODO:  Check to see if something we assumed before, such as a blank value
		##|  or a number may contain a less restricitve type now and convert the
		##|  column to something like text.
		##|

		updated = false
		for keyName, value of newData
			if keyName == "_id" then continue
			if keyName == "loc" then continue
			# if keyName == "id" then continue
			if keyName.charAt(0) == '_' then continue
			if keyName == "hash" then continue

			found = DataMap.getTypes(tableName).getColumn(keyName)
			if !found?

				colName = keyName.replace(/([a-z])([A-Z])/g, "$1 $2")
				colName = colName.replace /_/g, " "
				colName = colName.ucwords()

				config =
					name   : colName
					source : keyName

				updated = true
				DataMap.addColumn tableName, config

			else

				found.deduceColumnType(value)

		return updated


	## -------------------------------------------------------------------------------------------------------------
	## Return the columns associated with a given table
	## reduceFunction is called with every table name and return the columns that return true
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Function] reduceFunction function to validate the column if returns true column will be included
	## @return [Array] columns array of included columns
	##
	@getColumnsFromTable: (tableName, reduceFunction) =>

		dm = DataMap.getDataMap()

		columns = []
		if !DataMap.getTypes(tableName)
			return columns

		for source, col of DataMap.getTypes(tableName).col

			keepColumn = true
			if reduceFunction?
				keepColumn = reduceFunction(col)

			if source.charAt(0) == "_"
				keepColumn = false

			if col.getDeleted()
				keepColumn = false

			if keepColumn
				columns.push col

		return columns

	## ===[ Importing Data ]===

	## -------------------------------------------------------------------------------------------------------------
	## Quickly import an entire array of objects into table clears the table first
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Array] objects array of objects to set in the table in form of 2d array
	## @return [Boolean]
	##
	@importDataFromObjects: (tableName, objects) =>

		# console.log "importDataFromObjects table=#{tableName} Obj=", objects
		# console.log JSON.stringify(objects)

		dm = DataMap.getDataMap()
		for i, o of objects
			@addDataUpdateTable tableName, i, o

		true

	## ===[ Exporting / Retreiving Data ]===

	@setDataCallback: (tableName, methodName, callback)=>
		return DataMap.getDataMap().engine.setDataCallback tableName, methodName, callback

	##|
	##|  Export a table / return all data as a single object
	##|
	@exportTable: (tableName)=>
		return DataMap.getDataMap().engine.export(tableName)

	## -------------------------------------------------------------------------------------------------------------
	## get the values of the columns which will be retured true by reduceFunction
	##
	## @param [String] tableName table name for which the new data type is being set
	## @param [Function] reduceFunction function that will called on each column and if returns true then it will considered
	## @return [Array] results the array of the data included using reduceFunction
	##
	@getValuesFromTable: (tableName, reduceFunction) =>
		return DataMap.getDataMap().engine.find tableName, reduceFunction

	## -------------------------------------------------------------------------------------------------------------
	## add data to the dataMap and update the occurence on the currently rendered screen
	##
	## @param [String] tableName name of the table in which the data is being added
	## @param [String] keyValue unique key to track the data row inside the DataMap
	## @param [Object] values values of the row in form of object
	## @return [Boolean]
	##
	@addData: (tableName, keyValue, newData) =>

		path = "/#{tableName}/#{keyValue}"
		dm = DataMap.getDataMap()
		if !DataMap.getTypes(tableName)?
			DataMap.setTypes tableName

		##
		## Apply default values if any
		defaultValues = {}
		columns = DataMap.getColumnsFromTable(tableName)
		if columns?
			for col in columns
				if col.getDefault()
					defaultValues[col.getSource()] = col.getDefault()
			for key, value of defaultValues
				if !newData[key]
					newData[key] = value

		# doc = dm.engine.set path, newData
		doc = dm.engine.set path, Object.assign(defaultValues, newData)

		##|
		##|  Remove any cached values that happen to exist
		for varName, value of newData
			delete dm.cachedFormat["/#{tableName}/#{keyValue}/#{varName}"]

		##
		## emit event "new_data" so that everyone can know
		## that new data is added to table
		DataMap.sendNewDataNotification tableName, keyValue

		return doc

	##
	## add data which doesn't have a its own id to the dataMap
	@addDataWithAutoID: (tableName, newData) =>
		if !tableName or !newData then return false
		values = DataMap.getValuesFromTable(tableName)
		if values? and values.length 
			values = values.map( (value) ->
				parseInt(value.id)
			).filter( (id) -> 
				!isNaN(id)
			).sort((a, b) ->
				a > b
			)
		if !values? or values.length is 0
			values = [1]
		lastId = values[values.length - 1] + 1
		DataMap.addData tableName, lastId, newData
		true


	## -------------------------------------------------------------------------------------------------------------
	## Change an attribute from an active table
	##
	@changeColumnAttribute: (tableName, sourceName, field, newValue, ignoreEvents)=>

		dm = DataMap.getDataMap()
		if !DataMap.getTypes(tableName)?
			# console.log "Warning: can't changeColumnAttribute for missing table #{tableName}"
			return false

		col = DataMap.getTypes(tableName).getColumn(sourceName)
		if !col?
			# console.log "Warning: can't changeColumnAttribute for missing table #{tableName} column #{sourceName} (#{field} = #{newValue})"
			return false

		dm.cachedFormat = {}
		dm.cachedTimeFormats = []

		# if field == "render"
			# new ErrorMessageBox("Field 'render' is no longer used, see renderCode, change #{tableName}, source=#{sourceName}, field=#{field} new=#{newValue}")
			# return

		##
		## When trying to set column's width, it is required to clear column's currentWidth to apply the setting correctly
		if field is "width"
			delete col.currentWidth
			# delete col.actualWidth

		##|
		##|  Make the change
		if !col.changeColumn(field, newValue) then return false
		# console.log "Datamap.changeColumnAttribute: ", tableName, sourceName, field, newValue

		##
		## Make sure each column has a unique value of "order" 
		if field is "order"
			newValue = parseInt newValue
			columns = DataMap.getColumnsFromTable tableName
			for column in columns 
				source = column.getSource()
				if sourceName isnt source and newValue is parseInt(column.getOrder())
					DataMap.changeColumnAttribute(tableName, source, "order", newValue+1)
		##|
		##|  Send out events to those that need it unless we aren't sending events.
		##|
		if ignoreEvents? and ignoreEvents == true or !globalTableEvents?
			return true

		##| Send chagne event
		# console.log "changeColumnAttribute #{tableName}, #{sourceName}, #{field}, #{newValue}"
		globalTableEvents.emitEvent "table_change", [ tableName.replace(/;.*/, ""), sourceName, field, newValue ]
		return true

	##
	## Change bunch of attributes of a column from an active table
	@changeColumnAttributes: (tableName, sourceName, obj, ignoreEvents) =>
		if !obj? or typeof obj isnt "object" 
			return false 
		for field, newVale of obj 
			DataMap.changeColumnAttribute tableName, sourceName, field, newVale, ignoreEvents
		true


	@addDataUpdateTable: (tableName, keyValue, newData) =>

		path = "/#{tableName}/#{keyValue}"
		# doc = DataMap.getDataMap().engine.set path, newData
		dm = DataMap.getDataMap()
		doc = dm.engine.setFastDocument tableName, keyValue, newData


		if !DataMap.getTypes(tableName)?
			DataMap.setTypes tableName

		if typeof newData is "object"
			updated = dm.setDataTypesFromSingleObject tableName, newData 

		##|
		##|  Remove any cached values that happen to exist
		for varName, value of newData
			delete dm.cachedFormat["/#{tableName}/#{keyValue}/#{varName}"]

		##
		## emit event "new_data" so that everyone can know 
		## that new data is added to table
		DataMap.sendNewDataNotification tableName, keyValue

		return doc

	## -------------------------------------------------------------------------------------------------------------
	## delete row form the screen and dataMap using the keyvale
	##
	## @param [String] tableName name of the table in which the data is being added
	## @param [String] keyValue unique key to track the data row inside the DataMap
	## @return [Boolean]
	##
	## >>> RETURNS A PROMISE
	@deleteDataByKey: (tableName, keyValue) =>
		dm = DataMap.getDataMap()
		return dm.engine.delete "/#{tableName}/#{keyValue}"

	## -------------------------------------------------------------------------------------------------------------
	## get the data for a given key
	##
	## @param [String] tableName name of the table in which the data is being added
	## @param [String] keyValue unique key to track the data row inside the DataMap
	## @return [String]
	##
	@getDataForKey: (tableName, keyValue) =>
		dm = DataMap.getDataMap()
		return dm.engine.getFastRow tableName, keyValue

	##|
	##|  Delete a key
	@deleteDataForkey: (tableName, keyValue)=>
		# console.log "TODO: Not Implemented"

	##
	## Remove an item from collection
	@removeItem: (tableName, keyValue) =>
		dm = DataMap.getDataMap()
		dm.engine.delete "/#{tableName}/#{keyValue}"

		##
		## emit event "new_data" so that everyone can know 
		## that new data is deleted from table
		DataMap.sendNewDataNotification tableName, keyValue
		true		

	## -------------------------------------------------------------------------------------------------------------
	## get the single column|field value using the key and column name
	##
	## @param [String] tableName name of the table in which the data is being added
	## @param [String] keyValue unique key to track the data row inside the DataMap
	## @param [String] fieldName name of the column to return the value of
	## @return [String]
	##
	@getDataField: (tableName, keyValue, fieldName) =>

		dm = DataMap.getDataMap()
		return dm.engine.getFast tableName, keyValue, fieldName

	@getDataFieldFormatted: (tableName, keyValue, fieldName) =>

		path = "/" + tableName + "/" + keyValue + "/" + fieldName
		dm = DataMap.getDataMap()

		if dm.cachedFormat[path]?
			return dm.cachedFormat[path]

		currentValue = DataMap.getDataField tableName, keyValue, fieldName
		##|
		##|  TODO Fix Row Data
		rowData = @getDataForKey tableName, keyValue
		if DataMap.getTypes(tableName)?.col[fieldName]?
			col = DataMap.getTypes(tableName).col[fieldName]
			currentValue = col.renderValue(currentValue, keyValue, rowData)
			if col.getLinked()?
				if col.getAlign() is "right"
					currentValue = "<i class='fa fa-external-link-square'></i>&nbsp" + currentValue
				else
					currentValue += "&nbsp<i class='fa fa-external-link-square'></i>"

		if !currentValue? or currentValue == null
			currentValue = ""

		dm.cachedFormat[path] = currentValue
		##
		## time format data should be reset, so store those types to another object
		type = col?.getType()
		if type is "age" or type is "datetime" or type is "duration" or type is "timeago"
			dm.cachedTimeFormats.push path
		return currentValue

	@getCellColor: (tableName, keyValue, fieldName) =>

		column = null
		dm = DataMap.getDataMap()
		if DataMap.getTypes(tableName)?.col[fieldName]?
			column = DataMap.getTypes(tableName)?.col[fieldName]
		else
			return null

		if !column.getHasColorFunction? or !column.getHasColorFunction()
			return null

		value = dm.engine.getFast tableName, keyValue, fieldName
		rowData = {}

		colorFunction = column.getColorFunction()
		colorValue = colorFunction(value, keyValue, rowData)

		return colorValue

	@getCellBGColor: (tableName, keyValue, fieldName) =>
		column = null
		dm = DataMap.getDataMap()

		if DataMap.getTypes(tableName)?.col[fieldName]?
			column = DataMap.getTypes(tableName)?.col[fieldName]
		else
			return null

		if column.getType() is "enum"
			value = dm.engine.getFast tableName, keyValue, fieldName
			parts = (value or "").split("#")
			if parts.length > 1 then return "##{parts[1]}" else return null
		null

	@getSelectedList: (tableName) =>
		selectedRows = DataMap.getValuesFromTable tableName, (row) =>
			return row.row_selected

		selectedIds = selectedRows.map (row) => row.id
		selectedIds

	@refreshTempTable: (tableName, data, isArray) =>
		tableUpdated = false
		i = 0
		if !isArray
			@removeTableData tableName
			for id, rec of data
				unless typeof rec is "object"
					@addDataUpdateTable tableName, data.id || i++, data
					tableUpdated = true
					break

			if tableUpdated == false
				@importDataFromObjects tableName, data
		else
			@removeTableData tableName
			for rec, id in data
				unless typeof rec is "object"
					@addDataUpdateTable tableName, i++, data
					tableUpdated = true
					break

			if tableUpdated == false
				@importDataFromObjects tableName, data

	## -gao
	## function to get table by name
	@getTypes: (tableName) =>
		tempName = tableName.replace(/;.*/, "")
		DataMap.getDataMap().types[tempName]

	## -gao
	## function to set table in DataMap
	@setTypes: (tableName, table) =>
		tempName = tableName.replace(/;.*/, "")
		if !table?
			table = new DataSetConfig.Table(tempName)		
		DataMap.getDataMap().types[tempName] = table

	##
	## function to store serialized forms in a table in datamap
	@setFormInTable: (tableName, formName, formConfig) =>
		if !tableName or !formName? or !formConfig? then return false
		tempName = tableName.replace(/;.*/, "")
		table = DataMap.getDataMap().types[tempName]
		if !table? then return false
		if !table.forms then table.forms = {}
		table.forms[formName] = JSON.stringify(formConfig)
		DataMap.setTypes tableName, table 
		return true

	##
	## function to remove a form in a table in datamap
	@removeFormInTable: (tableName, formName) =>
		if !tableName or !formName? then return false
		tempName = tableName.replace(/;.*/, "")
		table = DataMap.getDataMap().types[tempName]
		if !table? or !table.forms? or typeof table.forms isnt "object" then return false
		delete table.forms[formName]
		DataMap.setTypes tableName, table 
		true

	##
	## function to get form config from a table in DataMap
	@getFormInTable: (tableName, formName) =>
		if !tableName then return null
		tempName = tableName.replace(/;.*/, "")
		table = DataMap.getDataMap().types[tempName]
		if !table? or !table.forms then return null
		if !formName? then return table.forms
		JSON.parse(table.forms[formName])

	##
	## function to emit "new_data" event for table
	@sendNewDataNotification: (tableName, keyValue) =>
		dm = DataMap.getDataMap()
		
		if !dm.engine.internalGetCollection(tableName)? then return false
		
		if dm.engine.internalGetCollection(tableName).evWaiting?
			clearTimeout dm.engine.internalGetCollection(tableName).evWaiting

		##|
		##|  Set a timer to let everyone know there is new data
		dm.engine.internalGetCollection(tableName).evWaiting = setTimeout ()=>
			ev = new CustomEvent("new_data", { detail: { tablename: tableName, id: keyValue }})
			window.dispatchEvent ev
			delete dm.engine.internalGetCollection(tableName).evWaiting
		, 10
		true

	##
	## function that makes some or all of the columns in a table editable
	@makeColumnsEditable: (tableName, columnNames) =>
		columns = DataMap.getColumnsFromTable tableName
		if !columns? or !columns.length then return false

		for col in columns
			if !columnNames or (columnNames.includes? and (columnNames.includes(col.getName()) or columnNames.includes(col.getSource())))
				DataMap.changeColumnAttribute tableName, col.getSource(), "editable", true

		true

	##
	## Clean cached value of date/time type every 1 minute
	expireCache: () =>

		if !@cachedTimeFormats? then return false
		try
			keysToUpdate = {}
			for path in @cachedTimeFormats
				if @cachedFormat[path]?
					#console.log "Time format cache cleared: ", path
					delete @cachedFormat[path]

					parts     = path.split '/'
					tableName = parts[1]
					keyValue  = parts[2]
					fieldName = parts[3]
					if !keysToUpdate[tableName]?
						keysToUpdate[tableName] = []
					if keysToUpdate[tableName].indexOf(keyValue) < 0
						keysToUpdate[tableName].push keyValue

			## emit "new_data" event to update table fields
			for table, keys of keysToUpdate
				for key in keys
					DataMap.sendNewDataNotification table, key
		catch e
			console.log "Exception in expiring cache in DataMap: ", e	

		true

	##
	## reset cached data
	resetCache: () =>
		@cachedFormat      = {}
		@cachedTimeFormats = []
		true

	##
	## Serialize data map into JSON
	@serialize: () =>
		dm = DataMap.getDataMap()

		##
		## serialize engine data
		engineData = dm.engine.serialize()

		##
		## serialize types
		typesData = {}

		for typeName, type of dm.types
			typesData[typeName] = type.serialize()

		return
			data         : dm.data
			engine       : engineData
			types        : typesData
			savecallbacks: dm.onSave

	##
	## Deserialize JSON data into DataMap
	@deserialize: (data) =>

		##
		## Deserialize types
		if data.types? and typeof data.types is "object"

			for typeName, type of data.types
				DataMap.importDataTypes typeName, type

		##
		## import engine data
		if data.engine? and typeof data.engine is "object"

			for tableName, tableData of data.engine

				for id, data of tableData
					DataMap.addData tableName, id, data

		##
		## import onSave callbacks
		if data.savecallbacks? and typeof data.savecallbacks is "object"

			for tableName, callback of data.savecallbacks

				DataMap.setSaveCallback tableName, callback

		true
