class ViewTestCode extends View

	constructor : () ->
		super()
		@setView "DynamicTabs", (tabs) =>
			@coffeeCodeTab = tabs.addTab "CoffeeScript"
			@jsCodeTab = tabs.addTab "JavaScript"
			@tabs = tabs

	getDependencyList: ()=>
		return [ "/vendor/prism.js", "/vendor/prism.css" ]

	setCode: (newCode)=>

		newCode = "<pre><code class='language-javascript'>" + newCode + "</code></pre>"
		@txtCode.html newCode
		Prism.highlightAll()

	setCoffeeCode : (newCode) =>
		if !@tabs then return console.log "ViewTestCode is not initialized yet..."
		@coffeeCodeTab.body.html "<pre><code class='language-javascript'>" + newCode + "</code></pre>"
		Prism.highlightAll()

	setJSCode : (newCode) =>
		if !@tabs then return console.log "ViewTestCode is not formatted yet..."
		@jsCodeTab.body.html "<pre><code class='language-javascript'>" + newCode + "</code></pre>"		
		Prism.highlightAll()