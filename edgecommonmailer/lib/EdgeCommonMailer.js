"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const Client = require("@sendgrid/client");
const EdgeEmail_1 = require("./EdgeEmail");
const EdgeCommonMailerError_1 = require("./EdgeCommonMailerError");
var EdgeEmail_2 = require("./EdgeEmail");
exports.EdgeEmail = EdgeEmail_2.default;
class EdgeCommonMailer {
    constructor() {
        this.logger = null;
        let credentials = edgecommonconfig_1.default.getCredentials('sendgrid');
        if ((credentials == null) || (credentials.type == null) || credentials.type !== 'sendgrid') {
            EdgeCommonMailer.warnMissingCreds();
        }
        else if (credentials.type === 'sendgrid') {
            EdgeCommonMailer.haveCreds = true;
            Client.setApiKey(credentials.apikey);
        }
        this.logger = edgecommonconfig_1.default.getLogger('EdgeCommonMailer');
    }
    /**
     * @returns singleton for the elastic search
     */
    static getMailer(forceRefresh) {
        if (EdgeCommonMailer.singleton === null || forceRefresh) {
            EdgeCommonMailer.singleton = new EdgeCommonMailer();
        }
        return EdgeCommonMailer.singleton;
    }
    /**
     * warn if cred is not found.
     */
    static warnMissingCreds() {
        edgecommonconfig_1.default.status('EdgeCommonMailer: Warning, sendgrid credentials not found!');
        return true;
    }
    /**
     * Log error internally
     */
    internalReportError(error, message) {
        this.logger.error(error.message, message);
        return false;
    }
    /**
     * Log success internally
     */
    internalReportMail(message, result) {
        this.logger.log(message, result);
        return true;
    }
    /**
     * Send email
     * @param message [EdgeMailData | any | EdgeEmail] - message body
     */
    doSendMessage(message) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // Check if it has credential
                if (!EdgeCommonMailer.haveCreds) {
                    EdgeCommonMailer.warnMissingCreds();
                    throw new EdgeCommonMailerError_1.default('No SendGrid Api Configured.');
                }
                // Email initiailization
                let mail;
                if (message instanceof EdgeEmail_1.default) {
                    mail = message;
                }
                else if (typeof message === 'object') {
                    mail = new EdgeEmail_1.default(message);
                }
                else {
                    throw new EdgeCommonMailerError_1.default('message should be EdgeMailData or object');
                }
                // Email validation
                mail.validate();
                // Send email
                const request = {
                    method: 'POST',
                    url: '/v3/mail/send',
                    body: mail.toJSON()
                };
                try {
                    const result = yield Client.request(request);
                    this.internalReportMail(message, result);
                    return result;
                }
                catch (err) {
                    throw new EdgeCommonMailerError_1.default('Sendgrid failed to send email', 'SENDGRID_ERROR', err.response.body);
                }
            }
            catch (err) {
                this.internalReportError(err, message);
                throw err;
            }
        });
    }
}
EdgeCommonMailer.singleton = null;
exports.EdgeCommonMailer = EdgeCommonMailer;
exports.default = EdgeCommonMailer;
//# sourceMappingURL=EdgeCommonMailer.js.map