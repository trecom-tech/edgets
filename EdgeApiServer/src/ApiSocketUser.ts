import {
    EdgeSocketClient,
    EdgeSubscription,
    EdgeApi    
}                        from 'edgeapi';
import IApiUser          from './interfaces/IApiUser';
import ApiWebUser        from './ApiWebUser'
import { IUser }         from './interfaces/IUser';
import { config }        from 'edgecommonconfig';
import { IPubSubData }   from './interfaces/IPubSubData';


// A socket is something that connects on top of the web so 
// the Socket user has all the functions of a web user as well.
//
export default class ApiSocketUser extends ApiWebUser 
{
    public myName: number                       = null;
    public rec: IUser                           = null;
    public authAttempts: number                 = 0;
    private sendDelayedQueue: Array<Array<any>> = [];
    private sendDelayedTimer: NodeJS.Timeout    = null;
    private streamingListNumber: number         = null;
    public configSendFlat: boolean              = false;
    public subscriptionList: EdgeSubscription[]    = [];
    private wantStatusUpdate: boolean           = false;
    private isAdmin: boolean                    = false;

    constructor(public myConnectionID: number, public socket: EdgeSocketClient) 
    {
        super(myConnectionID, socket.conn && socket.conn.remoteAddress, socket.conn && socket.conn.request);
        this.myName    = this.myConnectionID;
        this.onSetupSocket(this.socket);
    }

    /**
     * Become an admin user, no specific use case assigned right now
     */
    setAdmin() {
        this.isAdmin = true;
        return true;
    }

    /**
     * Unset admin
     */
    setNoAdmin() {
        this.isAdmin          = false;
        this.wantStatusUpdate = false;
        return true;
    }

    setConfigFlat() {
        console.log('Set flat on ', this.myName);
        return this.configSendFlat = true;
    }

    /**
     * Subscribe to status updates
     */
    setWantStatusUpdates() {
        this.wantStatusUpdate = true;
        return true;
    }

    logIncoming(cmd: string, obj: any) {
        // Uncomment the next line for detailed coms tracing
        // console.log chalk.white("<< ") + @myName + " " + chalk.cyan("Reply") + " [" + chalk.white(cmd) + "] => ", JSON.stringify(obj)

        return true;
    }

    send(cmd: string, obj: any) {
        // Uncomment the next line for detailed coms tracing
        // console.log chalk.blue(">> ") + @myName + " " + chalk.blue("Reply") + " [" + chalk.cyan(cmd) + "] => ", JSON.stringify(obj)

        this.socket.emit(cmd, obj);
        return true;
    }

    /**
     * Send this back to the client and they know to expect data
     * then use sendPush to send the actual data.
     */
    startStreamingList() {
        if (!this.streamingListNumber) {
            this.streamingListNumber = 999;
        }

        this.streamingListNumber++;
        return `STREAM:${ this.streamingListNumber }`;
    }

    /**
     * Terminate the streaming list by sending a special command to the receiver
     * @param stream_id
     */
    endStreamingList(stream_id: string) {
        this.sendPush(stream_id, 'END');
        return true;
    }

    /**
     * Generic push notification message
     * @param cmd string steramer id
     * @param obj message
     */
    sendPush(cmd: string, obj: any) {
        // console.log ">> " + @myName + " Push [" + chalk.cyan(cmd) + "] => ", JSON.stringify(obj)
        this.socket.emit('cmd-push', {
            command: cmd,
            data: obj,
        });
        return true;
    }

    //
    //  Push message but delayed to avoid sending too many at once
    sendPushDelayed(cmd: string, obj: any) {

        if (this.sendDelayedTimer != null) {
            clearTimeout(this.sendDelayedTimer);
        }

        if (this.sendDelayedQueue == null) {
            this.sendDelayedQueue = [];
        }

        this.sendDelayedQueue.push([cmd, obj]);
        return this.sendDelayedTimer = global.setTimeout(this.onPushDelayed, 200);
    }

    onPushDelayed() {

        if (this.sendDelayedTimer != null) {
            clearTimeout(this.sendDelayedTimer);
            delete this.sendDelayedTimer;
        }

        if (!this.sendDelayedQueue) {
            console.log('Missing queue?');
            return false;
        }

        while (this.sendDelayedQueue.length > 20) {
            this.sendDelayedQueue.shift();
        }

        for (let msg of Array.from(this.sendDelayedQueue)) {
            this.sendPush(msg[0], msg[1]);
        }

        delete this.sendDelayedQueue;
        return true;
    }

    addSubscription(subscription: EdgeSubscription) {
        //
        // Subscription request should have a channel
        if (subscription.channel == null) {
            console.log('Invalid Subscription request:', subscription);
            return false;
        }

        this.subscriptionList.push(subscription);
        config.status(`User ${ this.myConnectionID } subscribed to `, subscription.channel);
        return true;
    }

    filterChange(data: IPubSubData) {
        //
        //  Change example:
        //  {"type":"change","path":"/item/1221/test_data/apple",
        //    "stamp":"2016-03-07T13:54:06.756Z","kind":"E",
        //    "lhs":"2016-03-07T13:53:15.448Z","rhs":"2016-03-07T13:54:05.986Z"}
        //
        for (const sub of this.subscriptionList) {
            if (sub.channel === data.channel) {
                this.send('change', data);
                return true;
            }
        }

        return false;
    }

    reset() {
        return true;
    }
};
