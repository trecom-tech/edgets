$ ->
	event1 = 
		title: "testevent1"
		start: "2018-06-06"
		end  : "2018-06-07"
		description: "Test description"

	event2 =
		title: "testevent2"
		start: new Date()
		allDay: true
		description: 
			RR: '4263703'
			MLS: '0002537'
			Date: "Jun 5th, 2018"
			Description: "Closing Date"
			Address: "5373 Roberita Crossing Dr.SW"
			Priority: "High"
			Region: "Charlotte"
			Workflow: "Offfer-Accepted"

	event3 = 
		title: "testevent3"
		start: "2018-06-21"
		end  : "2018-06-31"        
				
	addTestButton "Simple Calendar View", "Open", () ->
		addHolder().
		setView "Calendar", (view) =>
			true
	addTestButton "Calendar Add Events", "Open", () -> 
		console.log event1
		addHolder().
		setView "Calendar", (view) =>
			view.addEvent event1
			view.addEvent event2
			view.addEvent event3
			true

	addTestButton "Calendar Reset Events", "Open", () ->
		addHolder().
		setView "Calendar", (view) =>
			view.addEvent event1
			view.addEvent event2
			view.addEvent event3
			setTimeout () ->
				view.resetEvents()
				true
			, 3000
		true

	addTestButton "Calendar on Popup", "Open", () ->
		doPopupView "Calendar", "PopupCalendar", null, 800, 600, (view)->
			view.addEvent event1
			view.addEvent event2
			view.addEvent event3   
		true
	
	addTestButton "Calendar in tab", "Open", ()->
		addHolder()
		.setView "DynamicTabs", (viewTabs)->
			viewTabs.doAddViewTab "Calendar", "TabCalendar", (view) ->
				view.addEvent event1
				view.addEvent event2
				view.addEvent event3   						
			viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
		true

	addTestButton "Calendar Weekly View with Event Object", "Open", () ->
		addHolder()
		.setView "Calendar", (view) ->
			view.deserialize 
				defaultview: 'agendaWeek'
				editable: false
				events: [ event1, event2, event3 ]

	go()