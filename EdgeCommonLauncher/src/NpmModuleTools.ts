import * as fs    from 'fs';
import { spawn }  from 'child_process';
import { config } from 'edgecommonconfig';

/*
* NpmModuleTools - Help to make sure a module is verifyInstalled or updated
*
* A simple class that exports verifyInstalled
* */

export class NpmModuleTools {

    /**
     * verify packages are installed
     * @param moduleSource
     * @param path
     */
    static verifyInstalled(moduleSource: any, path: any, forceInstall: boolean = false) {

        return new Promise((resolve, reject) => {

            if (!forceInstall) {
                try {
                    const stat = fs.lstatSync(path);
                    config.status(`Skipping install of ${moduleSource} due to ${path} exists.`);
                    return resolve(true);
                } catch (error) {
                    //  Does not already exist
                }
            }

            //  Spawn the job
            const envCopy = {
                ...process.env,
                TERM   : "HTML",
                LANG   : "en_US.UTF-8",
                LOGNAME: "www",
                SHELL  : "/bin/bash",
                SHLVL  : "1",
                TMPDIR : "/tmp/",
                USER   : "www"
            };

            config.status('env vars', envCopy);

            const commandLine = "npm";
            let args          = ["install", moduleSource];

            //  update is not catching changes correctly, using the slower "install" instead.
            config.status(`NpmModuleTools command=[${commandLine}] args=`, args);

            //  Check to see if the path exists
            let stat = null;
            args     = ['install', moduleSource];

            try {
                stat = fs.lstatSync(path);
            } catch (error) {
                config.status(`NpmModuleTools path does not exist for module [${moduleSource}] path=${path} ** Using Install **`);
                stat = null;
            }

            try {
                let jobExec = spawn(commandLine, args, {
                    // cwd: path
                    env: envCopy,
                    shell: true
                });

                jobExec.stdout.on("data", (data) => {
                    let str;
                    str = `NPM ${args[0]} ${moduleSource} ==> `;
                    str += data.toString();
                    str = str.replace("\n", "\\n");
                    str += "\n";
                    process.stdout.write(str);
                    return true;
                });

                jobExec.stderr.on("data", (data) => {
                    let str;
                    str = `[ERROR] NPM ${args[0]} ${moduleSource} ==> `;
                    str += data.toString();
                    str = str.replace("\n", "\\n");
                    str += "\n";
                    process.stdout.write(str);
                    return true;
                });

                return jobExec.on("close", (code) => {
                    console.log(`NPM Tools, done with ${moduleSource}`);
                    resolve(true);
                });
            } catch (error) {

                return console.log(`Exception during command for ${moduleSource} in ${path}`, error);

            }
        });
    }
}

export default NpmModuleTools;
