import ApiProvider   from '../../src/ApiProvider';
import ApiSocketUser from '../../src/ApiSocketUser';

export default class TestApiProvider extends ApiProvider{

    constructor(prototype: ApiProvider) {
        super(prototype);
    }

    onInit() {
        return console.log("TestApi Class Initialized");
    }

    getBaseName() {
        return "test";
    }

    /**
     * Simple api to test doCall
     * @param user - [ApiSocketUser]
     * @param data - [any]
     */
    async doTestdoCall(user: ApiSocketUser, params: any) {
        const data = {
            params
        }
        const result = await this.doCall('test_doTestApiWithUser', data, user);
        return result;
    }
    
    /**
     * Simple api returns user.myName and the data it got.
     * @param user - [ApiSocketUser]
     * @param data - [any]
     */
    doTestApiWithUser(user: ApiSocketUser, params: any) {
        console.log(params);
        return `User ${user.myName} has sent request with ${JSON.stringify(params)}`;
    }
    
    // Simple command that returns a string from the inputs
    doTestCommand(a: any, b: any) {
        return `X${ a }Y${ b }`;
    }

    // Simple command that also gets passed the user object (ApiSocketUser)
    doTestCommand2(user: any, a: any, b: any) {
        return 123;
    }

    doTestCommandCrash(x: any) {

        if (Math.random() > 0.8) {
            return new Promise((resolve, reject) => {
                console.log(`${ x } Forced Crash`);
                return setTimeout(process.exit, 300, 0);
            });
        }

        console.log(`API Return ${ x }`);
        return x * 10;
    }

    // Dummy command that returns a promise
    doTestCommand3() {
        return new Promise((resolve, reject) => {
            return setTimeout(resolve, 3000, "Test3 Result Here");
        });
    }
}
