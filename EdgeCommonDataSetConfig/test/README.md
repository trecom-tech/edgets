# Test Data

## Test data file (lookup.table.json)

This is an export from "lookup" on a project.   It contains several tables and the configuration they have.  You can use this for loading or testing as needed.

## Computation Fields

A field that is a "Computation Field" can be

1. Null
2. An excel style math formula
3. A Javascript Function in text format

For the "excel style" formula,  the field must have "=" at the first character.   In this case
the field is processed like excel and all other fields in the object are automatically added as 
variables.

For javascript functions, the code is converted to a function upon load for fast executation and can use the main object to produce a value.  For example:

	if ((val != null) && val > 0) {
		return val + " Rules";
	} else {
		return "Create";
	}

Which is stored like this in the database:

	"render" : "if ((val != null) && val > 0) {\n          return val + \" Rules\";\n        } else {\n          return \"Create\";\n        }",


More info

- See table_view_col internalMathRender and getRenderFunction

## Column options

Currently the following fields can be set for a column

| Field         | Description |
|---------------|-------------|
| name          | The name of the field as it is display to the user |
| source        | The field in the database |
| visible       | see main README |
| hideable      | see main README |
| type          | The text name of the data type |
| width         | The suggest width for displaying in html |
| tooltip       | *Computation Field* - The text used as a short version or html version for tooltips |
| render        | *Computation Field* - A formula or function that computes the value |
| order         | Numeric order for display in lists |
| editable      | see main README |
| required      | see main README |
| displayFormat | |
| options       | see main README |
| clickable     | see main README |
| align         | see main README |
| system        | True if this is a system specific column |
| autosize      | True if the column can be "autosized" by table displays |
