export declare class ColumnBase {
    reDate1: RegExp;
    reDate2: RegExp;
    reNumber: RegExp;
    reDecimal: RegExp;
    formatter: any;
    data: any;
    constructor();
    getName(): string;
    getSource(): string;
    getOrder(): number;
    getOptions(): null;
    getClickable(): boolean;
    getEditable(): boolean;
    getAlign(): null;
    getWidth(): number;
    RenderHeader(parent: any, location: any): any;
    RenderHeaderHorizontal(parent: any, location: any): any;
    UpdateSortIcon(newSort: any): null | boolean;
    getVisible(): boolean;
    getType(): string;
    getFormatter(): any;
    getFormatterName(): any;
    onFocus(e: any, col: any, data: any): boolean;
    getRequired(): boolean;
    getAlwaysHidden(): boolean;
    getSystemColumn(): boolean;
    getAutoSize(): boolean;
    getIsCalculation(): boolean;
    getLinked(): null;
    renderValue(value: any, keyValue: any, row: any): any;
    changeColumn(varName: any, value: any): boolean;
    getRenderFunction(): null;
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
    deduceColumnType(newData: any): null | boolean;
    deduceInitialColumnType(): null | boolean;
    getFlagsRequiredView(): any[];
    getFlagsRequriedEdit(): any[];
    getVisibleOnNew(): boolean;
    getVisibleOnEdit(): boolean;
    /**
     *     Should a value be validated prior to allowing a new record to be created?
     */
    getValidateOnCreate(): boolean;
    /**
     * Should a valid be validated prior to allowing a change to the record?
     */
    getValidateOnEdit(): boolean;
    /**
     * An optional message to show if validation fails.   Only Error or Warning should be returned.
     * an error message should stop edit/create but a wanring should not.
     */
    getValidateErrorMessage(): null;
    getValidateWarningMessage(): null;
    /**
     * Could be either javascript code or number or a range of numbers of whatever is required for validation.
     * format and value to be determined.
     */
    getValidateOptions(): null;
    /**
     *     function used to edit the column
     */
    getEditFunction(): null;
    serialize(): any;
    deserialize(obj: any): boolean;
}
export default ColumnBase;
