import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatTags extends DataFormatterType {
    name: string;
    align: string;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    attachEditor(elSourceInput: any, path: any): boolean;
    format(currentValue: any, options: any, path: any): string;
    unformat(currentValue: any, path: any): any;
}
export default DataFormatTags;
