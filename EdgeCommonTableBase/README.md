# EdgeCommonTableBase
# EdgeCommonTableBase
> Wrapper around an object and a record in a database

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonTableBase.git
    import TableBase from 'edgecommontablebase';

## Creating an instance

    const tb = new TableBase(dataSet, collection, id);

* dataSet: database name in mongodb
* collection: collection name in mongodb
* id: value of `id` field of document

## Testing

You need to run EdgeApiServer locally to test EdgeCommonTableBase.  
And then run command `npm run test-local`.


## Helper functions

### isset(path: string)

Check if value for specific path has been set

    const result = tb.isset('id');

### safeString(path: string)

Returns a string value for the specific path such as `/test/data/name` or `test.name`

    const result = tb.safeString('name');

### safeNumber(path: string)

Returns an int value for the specific path such as `/test/data/age` or `test.age`

    const result = tb.safeString('age');

### safeBool(path: string)

Returns an int value for the specific path such as `/test/data/is_verified` or `test.is_verified`

    const result = tb.safeBool('is_verified');

### doSave()

Save values when there are subject changes to the record

    tb.data.name = 'test';
    const result = await tb.doSave();

### doAggregate(aggList: any)

Execute an aggregate

    const result = await tb.doAggregate({ $match: { age: 43 } })

### doFind(condition: any)

Load a single record

    const result = await tb.doFind({ age: 43 });

### doFindAll(condition?: any, sortCondition?: any, offset?: number, limit?: number)

Get all record that matched with condition by limitation  
* condition: if `null`, return all records
* sortCondition: default value is `{ _id: 1 }`
* offset: default value is 0
* limit: default value is 1000

    const result = await tb.doFindAll({ age: 43 }, null, 0, 1);

### doRemove()

Remove a record that matched with condition

    const result = await tb.doRemove({id: id});