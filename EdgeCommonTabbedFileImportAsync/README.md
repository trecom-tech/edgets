# EdgeCommonTabbedFileImportAsync
> Reads a file very quickly line by line, can handle any size file.  Handles CSV quotes.  Handles window CRLF or Mac/Linux LF.  Converts dates to Javascript date objects, numbers to numbers, etc.

    npm install --save git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonTabbedFileImportAsync.git
    import TabbedFileImportAsync from 'edgecommontabbedfileimportasync';

## Creating an instance to import a file

    let tfi = new TabbedFileImportAsync("filename");

There are several possible options you can set as well

* hasHeader = false // Set to true if the CSV / Tabbed file has a header line.  If true, the headers will be used to create an array for each line.
* seperator = 9  // Set to the ascii code, 9 is a tab and 32 is space
* skipCount = 0 // The number of lines to skip upon startup.
* handleEscaped = true // Handle escaped strings where \Something means whatever is after \ is always exactly used.

Reading a file is done with the readLine function which **returns a Promise**

    const result = tfi.readLine((data: any) => {
        // Do something with the data
        // return true or a Promise yourself
        console.log(data);
    });

During the import if you return a Promise from the callback then the import will pause and wait for the Promise to finish.  If you are, for example, sending
a large amount of data over the network you will quickly fill the node buffers so you would return a Promise if the stream needs a "drain" and then resolve
once complete.

# Helper functions

Set all the required settings for a normal CSV file with a header row

    tfi.setFormatCsvWithHeaders();