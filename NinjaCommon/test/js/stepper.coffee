$ ->

	##|
	##|  This is just for diagnostics,  you don't need to verify the data map is
	##|  loaded normally.  The data types should be loaded upon startup.
	addTest "Confirm Zipcodes datatype loaded", () ->
		dm = DataMap.getDataMap()
		if !dm? then return false

		zipcodes = dm.types["zipcode"]
		if !zipcodes? then return false
		if !zipcodes.col["code"]? then return false

		true

	##|
	##|  Load the zipcodes JSON file.
	##|  This will insert the zipcodes into the global data map.
	addTest "Loading Zipcodes", () ->

		new Promise (resolve, reject) ->
			ds  = new DataSet "zipcode"
			ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
			ds.doLoadData()
			.then (dsObject)->
				resolve(true)
			.catch (e) ->
				console.log "Error loading zipcode data: ", e
				resolve(false)

	addTestButton "ViewStepper Simplest", "Open", (e)->

		addHolder("renderTest1")
		.setView "Stepper", (stepper)->
			stepper.addStep "Test 1", "fa fa-android", "Default One"
			stepper.addStep "Test 2", "fa fa-university", "Default Two"
			stepper.setTitle "Test Title"
			stepper.setSubTitle "Test Sub Title"
			stepper.addStep "Test 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"

		return 1

	addTestButton "ViewStepper Paddings", "Open", (e)->

		addHolder("renderTest1")
		.setView "Stepper", (stepper)->
			stepper.addStep "Test 1", "fa fa-android", "Default One"
			stepper.addStep "Test 2", "fa fa-university", "Default Two"
			stepper.setTitle "Test Title"
			stepper.setSubTitle "Test Sub Title"
			stepper.addStep "Test 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"
			stepper.setInnerPadding 40
			stepper.setOuterPadding 10

		return 1		

	addTestButton "ViewStepper Move To First", "Open", (e)->

		addHolder("renderTest1")
		.setView "Stepper", (stepper)->
			stepper.addStep "Test 1", "fa fa-android", "Default One"
			stepper.addStep "Test 2", "fa fa-university", "Default Two"
			stepper.setTitle "Test Title"
			stepper.setSubTitle "Test Sub Title"
			stepper.addStep "Test 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"

			setTimeout () ->
				stepper.goToLastStep()
			, 2000

			setTimeout () ->
				stepper.goToFirstStep()
			, 5000

		return 1

	addTestButton "ViewStepper Setting Heights", "Open", (e)->

		addHolder("renderTest1")
		.setView "Stepper", (stepper)->
			stepper.addStep "Test 1", "fa fa-android", "Default One"
			stepper.addStep "Test 2", "fa fa-university", "Default Two"
			stepper.setTitle "Test Title"
			stepper.setSubTitle "Test Sub Title"
			stepper.addStep "Test 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"
			stepper.setTitleHeight 100
			stepper.setSubTitleHeight 40
			stepper.setStepHeight 100
			stepper.setButtonHeight 60
			stepper.setButtonWidth 120

		return 1

	addTestButton "ViewStepper with custom Flags", "Open", (e)->

		addHolder("renderTest1")
		.setView "Stepper", (stepper)->
			stepper.addStep "Test 1", "fa fa-android", "Default One"
			stepper.addStep "Test 2", "fa fa-university", "Default Two"
			stepper.setTitle "Test Title"
			stepper.setSubTitle "Test Sub Title"
			stepper.addStep "Test 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"
			stepper.setEnablePrevious false
			stepper.setEnableSkip true
			stepper.setEnableSubmit false

		return 1		

	addTestButton "ViewStepper Next with function returning false", "Open", (e)->

		addHolder()
		.setView "Stepper", (stepper)->
			stepper.addStep "Step 1", "fa fa-android", "Default One"
			stepper.doAddViewStep "TestLayout2", "Step 2", "fa fa-university", (view) ->
				view.wizardAllowContinue = () -> 
					console.log "ViewTestLayout2.wizardAllowContinue returning false..."
					return false
			stepper.setTitle "ViewStepper Test"
			stepper.setSubTitle "ViewStepper Sub Title"
			stepper.addStep "Step 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"

		return 1

	addTestButton "ViewStepper Next with function returning a promise", "Open", (e)->

		addHolder()
		.setView "Stepper", (stepper)->
			stepper.addStep "Step 1", "fa fa-android", "Default One"
			stepper.doAddViewStep "TestLayout2", "Step 2", "fa fa-university", (view) ->
				view.wizardAllowContinue = () -> 
					console.log "ViewTestLayout2.wizardAllowContinue returning a promise..."
					return new Promise (resolve, reject) -> 
						setTimeout () ->
							console.log "Promise is resolved in 2 seconds..."
							resolve true
						, 2000
			stepper.setTitle "ViewStepper Test"
			stepper.setSubTitle "ViewStepper Sub Title"
			stepper.addStep "Step 3", null, "Default Three"
			stepper.setTextPrevious "Go back"
			stepper.setTextNext "Go forward"

		return 1	

	addTestButton "ViewStepper with Views", "Open", (e)->

		addHolder()
		.setView "Stepper", (stepper)->
			stepper.doAddViewStep "TestLayout2", "Step 1", "fa fa-android", (view) ->
				console.log "TestLauout2 in Step 1"
			stepper.doAddViewStep "TestLayout3", "Step 2", "fa fa-university", (view) ->
				console.log "TestLauout3 in Step 2"
			stepper.doAddViewStep "TestLayout4", "Step 3", "fa fa-book", (view) ->
				console.log "TestLauout4 in Step 3"

			stepper.setTitle "ViewStepper"
			stepper.setSubTitle "This is a little longer ViewStepper Sub Title"

		return 1

	addTestButton "ViewStepper onStepShow/Hide", "Open", (e)->

		addHolder()
		.setView "Stepper", (stepper)->
			stepper.doAddViewStep "TestLayout2", "Step 1", "fa fa-check", (view) ->
				view.onStepShow = (view) =>
					console.log "First step is showing: ", view
				view.onStepHide = (view) =>
					console.log "Fist step is hiding: ", view

			stepper.doAddViewStep "TestLayout3", "Step 2", "fa fa-university", (view) ->
				view.onStepShow = (view) =>
					alert("Second step is showing")
				view.onStepHide = (view) =>
					console.log "Second step is hiding: ", view


			stepper.doAddViewStep "TestLayout4", "Step 3", "fa fa-book", (view) ->
				view.onStepShow = (view) =>
					console.log "Third step is showing: ", view
				view.onStepHide = (Third) =>
					console.log "Fist step is hiding: ", view


			stepper.setTitle "ViewStepper"
			stepper.setSubTitle "This is a little longer ViewStepper Sub Title"

		return 1		

	addTestButton "ViewStepper Deserialize", "Open", () ->
		$("body").append '''
			<style type="text/css">
			.custom-button1 {
				border : 1px solid #bbbbdd;
				color  : red;
			}
			.custom-button2 {
				background-color: green;
			}
			.custom-title1 {
				border: 1px solid red;
				border-radius: 50%;
				line-height: 30px;
				min-width: 30px;				
			}
			</style>
		'''		
		addHolder()
		.setView "Stepper", (stepper) =>
			stepper.deserialize 
				title         : "Stepper <span style='color: red;'>Deserialize</span> Example"
				subtitle      : "This is a demo showing ViewStepper deserialized"
				enableskip    : false 
				enableprevious: true 
				textprevious  : "Go Back"
				textnext      : "Go Next"
				textsubmit    : "Let's go"
				custombutton  : [
					"custom-button1"
					"custom-button2"
				]
				onsubmitfunction: (view) ->
					console.log "Stepper is submitting: ", view 
					true
				steps: [
					title: "Step 1"
					titlehtml: "<div class='custom-title1'> 1 </div>"
					order: 2
					view : "Form"
					name : "viewform"
					inputs: [
						type: "text"
						name: "input1"
						label: "Input 1"
					,
						type: "text"
						name: "input2"
						label: "Input 2"					
					]
					wizardAllowContinue: (view, id) =>
						console.log "wizardAllowContinue : ", this, view, id 
						return true
				,
					title: "Step 2"
					order: 4
					view : "TestLayout3"
					name : "viewtestlayout3"
				,
					title: "Step 3"
					icon : "fa fa-book"
					order: 1
					view : "TestForms1"
					name : "viewtestforms"
				]

			setTimeout () ->
				options = stepper.serialize()
				console.log "Stepper serialized data = ", options
				doPopupView "Stepper", "Stepper Deserialized", null, 1200, 800, (stepperPopup) ->
					stepperPopup.deserialize options
			, 3000


	addTestButton "ViewStepper Wizard Example", "Open", () ->
		doPopupView "Stepper", "Offline Market", "offline-market1", 1200, 700, (stepper) ->
			stepper.deserialize 
				# title         : "<span style='color: red;'>Form Wizard</span>"
				# subtitle      : "This is a Form Wizard based on ViewStepper and ViewForm"
				enableskip    : false
				enableprevious: true
				textprevious  : "BACK"
				textnext      : "NEXT"
				textsubmit    : "FINISH"
				custombutton  : [

				]
				submitfunction: (view) ->
					console.log "Stepper is submitting: ", view.viewform1, view.viewform2, view.viewform3
					new Promise (resolve, reject) ->
						setTimeout () ->
							console.log "Wizard is just Submitted"
							resolve true
						, 2000

				steps: [
					title    : "step1"
					titlehtml: "LOCATION"
					order    : 0
					view     : "Form"
					name     : "viewform1"
					onsubmit : (form) ->
						console.log "Form1 is submitted : ", form, this
						return true
					rows: [
						inputs: [
							type : "text"
							name : "address"
							label: "Address"
						]
					,
						inputs: [
							name : "or"
							label: "Or"
							noInput: true
						]
					, 
						inputs: [
							type : "decimal"
							name : "lat"
							label: "Latitude"
							validateFunction: (val) ->
								if isNaN(val) or val < -180 or val > 180
									@setErrorMsg "You must enter a number between -180 and 180"
									return ViewForm.ERROR
								ViewForm.SUCCESS
						,
							type : "decimal"
							name : "lon"
							label: "Longitude"
							validateFunction: (val) ->
								if isNaN(val) or val < -180 or val > 180
									@setErrorMsg "You must enter a number between -180 and 180"
									return ViewForm.ERROR
								ViewForm.SUCCESS
						]
					]
					inputcustomstyles: [
						# "custom-input1"
					]
					wizardAllowContinue: (view, id) =>
						form = view.viewform1
						form.getValues()
						console.log "Form1 values: "
						console.log "Address = ", form.address.getValue(), ", Latitude = ", form.lat.getValue(), ", Longitude = ", form.lon.getValue()
						if form.address.getValue().length == 0 and (form.lat.startValidation() is ViewForm.ERROR or form.lon.startValidation() is ViewForm.ERROR)
							m = new ModalForm
								title:        "Error"
								content:      "Please input either Address or Latitude & Longitude correctly."
								position:     "top"
								ok:           "OK"
							return false 
						true
				,
					title: "step2"
					titlehtml: "DETAILS"
					order: 1
					view : "Form"
					name : "viewform2"
					rows : [
						inputs: [
							type : "decimal"
							name : "marketvalue"
							label: "Market Value"
						,
							type : "decimal"
							name : "sqft"
							label: "SQFT"
							validateFunction: (val) ->
								if !val? or val is 0
									@setErrorMsg "This field is required."
									return ViewForm.ERROR 
								return ViewForm.SUCCESS
						]
					,
						inputs: [
							type : "int"
							name : "bedrooms"
							label: "Bedrooms"
							validateFunction: (val) ->
								if !val? or val is 0
									@setErrorMsg "This field is required."
									return ViewForm.ERROR
								return ViewForm.SUCCESS
						,
							type : "int"
							name : "bathrooms"
							label: "Bathrooms"
							validateFunction: (val) ->
								if !val? or val is 0
									@setErrorMsg "This field is required."
									return ViewForm.ERROR 
								return ViewForm.SUCCESS 							
						]
					,
						inputs: [
							type : "int"
							name : "yearbuilt"
							label: "Year Built"
							validateFunction: (val) ->
								if !val? or val is 0
									@setErrorMsg "This field is required."
									return ViewForm.ERROR 
								return ViewForm.SUCCESS
						,
							name   : ""
							label  : ""
							noInput: true
						]
					]
					inputcustomstyles: [
					]
					wizardAllowContinue: (view, id) =>
						form = view.viewform2
						result = form.getValues()
						console.log "ViewForm2 values: "
						console.log "marketvalue = ", form.marketvalue.getValue(), ", sqft = ", form.sqft.getValue(), ", bedrooms = ", form.bedrooms.getValue(), ", bathrooms = ", form.bathrooms.getValue(), ", yearbuilt = ", form.yearbuilt.getValue()
						if result is false
							m = new ModalForm
								title:        "Error"
								content:      "Please input all the required fields."
								position:     "top"
								ok:           "OK"
							return false 						
						return true
				,
					title: "step3"
					titlehtml: "CONFIRM"
					order: 2
					view : "Form"
					name : "viewform3"
					inputs: [
						type : "memo"
						name : "comments"
						label: "Comments"
					]
					wizardAllowContinue: (view, id) =>
						return true
				]
		, { resizable: false }

	go()
