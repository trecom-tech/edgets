class ViewTestLayout6 extends View

    ##|  using an array of layout view helpers, with options
    getLayout: ()=>
        view: "Splittable"
        name: "viewSplitter"
        percent: 30
        top: "TestShowSize"
        bottom:
            view: "Docked"
            size: 100
            location: "left"
            docked:
                view: "TestShowSize"
                name: "viewTest2"
            main:
                view: "DynamicTabs"
                name: "viewTabs"

    onShowScreen: ()=>
        console.log "ViewTestLayout6 onShowScreen viewSplitter=", @viewSplitter
        console.log "VIewTestLayout6 viewTest2=", @viewTest2
        console.log "VIewTestLayout6 viewTabs=", @viewTabs





