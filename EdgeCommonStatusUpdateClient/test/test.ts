
import StatusUpdateClient from '../src/StatusUpdateClient';

let count                = 0;
describe("StatusUpdateClient", () => {
    describe("Send Status Test", () =>
        it("Should send status", async () => {
            return new Promise(async (resolve, reject) => {
                const statusUpdateClient = new StatusUpdateClient("testing", "detaultType");
                await statusUpdateClient.doOpenChangeMessageQueue();
                const job = statusUpdateClient.startJob("testing", { arg1: 1, arg2: 10 }, 1);
                const interval = setInterval(() => {
                        console.log("Sending update ", count);
                        statusUpdateClient.sendStatus({ counter: count });
                        count++;
                        if (count > 100) {
                            clearInterval(interval);
                            job.stop();
                            console.log("Done.");
                            resolve();
                        }
                    }
                    , 50
                );
            });
        })
    );
});