export declare class EdgeLauncher {
    private api;
    private basePath;
    private emitter;
    private homePath;
    private log;
    private statusInterval;
    private statusUpdateClient;
    private saveResult;
    private timer;
    private static saveResult;
    constructor();
    /**
     *
     * Any job can output one or more "Results=<Number>" lines and this
     * will total up those numbers and add them to the record for reporting.
     *
     * @param job
     * @param command
     * @param fullOutput
     * @param slot_id
     */
    insertJobLog(job: any, command: any, fullOutput: any, slot_id: any): Promise<boolean>;
    /**
     *
     * job is an object as defined in "UnderstandingJobs.md"
     * slot_id is any number that is used for reporting and also
     * used to help the real-time status boards keep track of jobs
     *
     * @param job
     * @param slot_id
     */
    runJob(job: any, slot_id: any): Promise<unknown>;
}
export default EdgeLauncher;
