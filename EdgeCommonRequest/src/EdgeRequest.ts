import { parseString }     from 'xml2js';
import * as crypto         from 'crypto';
import * as http           from 'http';
import * as https          from 'https';
import * as querystring    from 'querystring';
import * as url            from 'url';
import * as zlib           from 'zlib';
import * as fs             from 'fs';
import { config }          from 'edgecommonconfig';
import { MultipartParser } from './MultipartParser';
import EdgeApi             from 'edgeapi';
import RateCapture         from 'edgecommonratecapture';

let globalUseCharlesProxy = false;
let globalDebugRequest    = false;

export class EdgeRequest {
    username: string          = null;
    password: string          = null;
    headers: any              = {};
    cookies: any              = {};
    nc: number                = 0;
    requestOptions: any       = {};
    tokenDate: Date           = null;
    challenge: any            = null;
    method: string            = 'GET';
    baseUrl: string           = null;
    userAgent: string         = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36';
    userAgentPassword: string = null;
    strDebugFilename: string  = null;
    followRedirect: boolean   = true;
    timeoutMS: number         = 60 * 5 * 1000;
    useStatusUpdate: boolean  = false;
    desiredResultKey: string  = null;
    cacheFile: any            = null;
    postData: any;
    authParams: any;
    watchdogTimer: NodeJS.Timeout;

    req: any;

    setUseGlobalProxy() {
        globalUseCharlesProxy = true;
        globalDebugRequest    = true;
        return true;
    }

    //  Set the default method to use for requests
    setMethod(method: string) {
        this.method = method;
    }

    /**
     * setUseragentPassword
     * @param userAgentPassword
     */
    setUseragentPassword(userAgentPassword: string) {
        this.userAgentPassword = userAgentPassword;
    }

    /**
     * setUseragent
     * @param userAgent
     */
    setUseragent(userAgent: string) {
        this.userAgent = userAgent;
    }

    setUsername(username: string, password: string) {
        this.username = username;
        this.password = password;
    }

    /**
     * Add a custom header value
     *
     * @param varName
     * @param value
     */
    addHeader(varName: string, value: string) {
        this.headers[varName] = value;
        return true;
    }

    /**
     * Given a URL it will use the existing options to download it.
     * @param dataUrl
     * @param qs
     */
    doGetLink(dataUrl: string, qs: any = {}) {

        config.status('doGetLink url=', dataUrl, 'qs=', qs);

        delete this.postData;
        if (this.baseUrl != null) {
            // console.log 'Here baseUrl=', @baseUrl, ' dataulr=', dataUrl
            dataUrl = url.resolve(this.baseUrl, dataUrl);
        }

        //
        //  Parse the URL
        const urlParts = url.parse(dataUrl);

        if ((urlParts.protocol === 'https:') && (urlParts.port == null)) {
            urlParts.port = '443';
        }

        if (urlParts.port == null) {
            urlParts.port = '80';
        }

        this.requestOptions.host = urlParts.hostname;
        this.requestOptions.port = urlParts.port || 80;
        this.requestOptions.path = urlParts.pathname;

        //  Pull out any initial query
        if (urlParts.search != null) {
            urlParts.search = urlParts.search.replace('?', '');
            if (qs == null) {
                qs = {};
            }
            for (let part of urlParts.search.split('&')) {
                const elements  = part.split('=');
                qs[elements[0]] = elements[1];
            }
        }

        config.status('EdgeRequest requestOptions:', this.requestOptions);

        if ((urlParts.auth != null) && (urlParts.auth.length > 0)) {
            this.headers['Authorization'] = `Basic ${Buffer.from(urlParts.auth).toString('base64')}`;
        }

        this.baseUrl = urlParts.protocol + '//' + urlParts.hostname + ':' + urlParts.port + '/' + urlParts.pathname;

        if (qs != null) {

            const postData = querystring.stringify(qs);
            if (this.method === 'GET') {
                this.requestOptions.path += `?${postData}`;
            } else {
                this.headers['Content-Length'] = Buffer.byteLength(postData);
                this.headers['Content-Type']   = 'application/x-www-form-urlencoded';
                this.postData                  = postData;
            }
        }

        return this.doRequest(this.requestOptions);
    }

    /**
     * Given an object of options in node's http options format it will download.
     * @param options
     */
    async doRequest(options: any) {

        if (globalUseCharlesProxy || (process.env.CHARLES != null)) {

            options.orig = {
                path: options.path,
                host: options.host,
                port: options.port
            };

            //
            //  Rewrite request to use Charles Internet Proxy on port 8888 on the local machine
            this.headers['Host'] = options.host;
            options.path         = `http://${options.host}:${options.port}${options.path}`;
            options.host         = '127.0.0.1';
            options.port         = 8888;
            options.headers      = this.headers;
        }


        let result = null;
        if (this.cacheFile != null) {
            try {
                if (fs.existsSync(this.cacheFile)) {
                    result = JSON.parse(fs.readFileSync(this.cacheFile).toString());
                }

            } catch (e) {
                // fail silently
            }
        }
        //
        // File doesn't exist

        if (result == null) {
            result = await this.doInternalSendRequest(options);
        }


        if (globalDebugRequest) {
            console.log('Checking result for realm response');
        }

        if (this.internalCheckRedirect(result.res, options)) {
            //
            //  Resent the new request
            result = await this.doInternalSendRequest(options);
        }

        if (this.internalCheckResponseForRealm(result.res, options)) {

            //
            // Resend with the header
            this.nc        = 0;
            result         = await this.doInternalSendRequest(options);
            this.tokenDate = new Date();
        }

        if (this.cacheFile != null) {
            fs.writeFileSync(this.cacheFile, JSON.stringify(result));
        }

        return this.doInternalProcessResult(result.res, result.body, result.bodyBuffer);
    }

    /**
     * Given a res object from Node's http response, check for cookie headers
     * update the internal cookie jar.
     *
     * @param res
     */
    internalParseCookie(res: any) {

        if (res.headers['set-cookie'] == null) {
            return;
        }
        for (let cookie of Array.from(res.headers['set-cookie'] as string)) {
            const parts               = cookie.split(';');
            const subParts            = parts[0].split('=');
            this.cookies[subParts[0]] = subParts[1];
        }

        return true;
    }

    /**
     * Internal function that loks for a redirect header
     * @param res
     * @param options
     */
    internalCheckRedirect(res: any, options: any) {

        // TODO should confirm logic, weired return here;
        return false;

        if (!this.followRedirect) {
            return false;
        }

        if ((res.headers['location'] != null) && (res.headers['location'].length > 1)) {

            config.status('internalCheckRedirect found location:', res.headers['location']);
            console.log('Old options:', options);
            return process.exit(0);
        }
    }

    /**
     *  Internal function that looks for a Microsoft Digest challenge request and will
     *  update the headers for a correct response.
     * @param res
     * @param options
     */
    internalCheckResponseForRealm(res: any, options: any) {

        if (res.headers['www-authenticate'] == null) {
            return false;
        }

        this.challenge = this.parseChallenge(res.headers['www-authenticate']);
        this.nc        = 0;
        return true;
    }

    /**
     *
     * @param options
     */
    internalSetNextCommand(options: any) {

        const ha1 = crypto.createHash('md5');
        const ha2 = crypto.createHash('md5');

        ha1.update([this.username, this.challenge.realm, this.password].join(':'));
        ha2.update([options.method, options.path].join(':'));

        let cnonce               = false;
        let nc: string | boolean = false;
        if (typeof this.challenge.qop === 'string') {

            if (this.challenge.cnonce == null) {
                const cnonceHash = crypto.createHash('md5');
                cnonceHash.update(Math.random().toString(36));
                this.challenge.cnonce = cnonceHash.digest('hex').substr(0, 8);
            }

            ({ cnonce } = this.challenge);
            nc = this.updateNC();
        }

        //
        //  Generate the response hash
        const response       = crypto.createHash('md5');
        const responseParams = [];
        responseParams.push(ha1.digest('hex'));
        responseParams.push(this.challenge.nonce);

        if (cnonce) {
            if (globalDebugRequest) {
                console.log('Pushing param nc=', nc, cnonce);
            }

            responseParams.push(nc);
            responseParams.push(cnonce);
        }

        responseParams.push(this.challenge.qop);
        responseParams.push(ha2.digest('hex'));
        response.update(responseParams.join(':'));

        this.authParams = {
            username: this.username,
            realm   : this.challenge.realm,
            nonce   : this.challenge.nonce,
            uri     : options.path,
            qop     : this.challenge.qop || '',
            response: response.digest('hex'),
            opaque  : this.challenge.opaque || ''
        };

        // if @nc > 1
        // delete @authParams['qop']

        if (cnonce) {
            this.authParams.nc     = nc;
            this.authParams.cnonce = cnonce;
        }

        this.headers['Authorization'] = this.compileParams(this.authParams);

        return true;
    }

    /**
     *
     * @param digest
     */
    parseChallenge(digest: string) {

        const prefix      = 'Digest ';
        const challenge   = digest.substr(digest.indexOf(prefix) + prefix.length);
        const parts       = challenge.split(',');
        const { length }  = parts;
        const params: any = {};
        let i             = 0;
        while (i < length) {

            if (typeof parts[i] === 'string') {
                let part = parts[i].match(/^\s*?([a-zA-Z0-0]+)='(.*)'\s*?$/);
                if ((part != null) && (part.length > 2)) {
                    params[part[1]] = part[2];
                } else {
                    part = parts[i].split('=');
                    if ((part != null) && (part.length === 2)) {
                        params[part[0].trim()] = part[1].trim();
                    }
                }
            }
            i++;
        }

        config.status('EdgeRequest parseChallenge', { digest, parseChallenge: params });

        return params;
    }

    /**
     * Return the actual header
     * @param params
     */
    compileParams(params: any) {
        const parts = [];
        for (let param in params) {
            if ((param === 'nc') || (param === 'qop') || (param === 'algorith')) {
                parts.push(`${param}=${params[param]}`);
            } else {
                parts.push(`${param}="${params[param]}"`);
            }
        }

        return `Digest ${parts.join(', ')}`;
    }

    /**
     * Update and zero pad nc
     */
    updateNC() {
        this.nc++;
        if (this.nc > 99999999) {
            this.nc = 1;
        }
        const padding = new Array(8).join('0') + '';
        const nc      = this.nc + '';
        return padding.substr(0, 8 - (nc.length)) + nc;
    }

    /**
     * Helper function that finds elements in an object set
     * @param obj
     * @param name
     * @param findOne
     */
    findElements(obj: any, name: string, findOne: any) {

        const searchList = [];
        searchList.push(obj);
        let pos       = 0;
        const results = [];

        while (pos < searchList.length) {

            for (let keyName in searchList[pos]) {
                const keyVal: any = searchList[pos][keyName];
                if (keyName === name) {
                    this.fixupJsonResults(keyVal);
                    if ((findOne != null) && (findOne !== false)) {
                        return keyVal;
                    }
                    results.push(keyVal);
                } else if (typeof keyVal === 'object') {
                    searchList.push(keyVal);
                }
            }

            pos++;
        }

        return results;
    }

    /**
     * The XML to JSON gives us weird arrays with 1 element because of how XML works
     * we are going to clean that up here.
     * @param obj
     */
    fixupJsonResults(obj: any) {

        let keyName;
        let keyVal;
        const reDate2 = /^[0-9][0-9][0-9][0-9].[0-9][0-9].[0-9][0-9]T[0-9][0-9].[0-9][0-9].[0-9][0-9]/;

        for (keyName in obj) {
            keyVal = obj[keyName];
            if ((typeof keyVal === 'object') && Array.isArray(keyVal)) {
                if ((keyVal.length === 1) && (keyVal[0] != null)) {
                    obj[keyName] = keyVal[0];

                    if (typeof obj[keyName] === 'object') {
                        this.fixupJsonResults(obj[keyName]);
                    }

                } else {
                    for (let n of Array.from(keyVal)) {
                        this.fixupJsonResults(n);
                    }
                }
            } else if (typeof keyVal === 'object') {
                this.fixupJsonResults(keyVal);
            }
        }

        for (const keyName in obj) {

            keyVal = obj[keyName];
            if ((typeof keyVal === 'string') && (keyVal === '')) {
                delete obj[keyName];

            } else if (reDate2.test(keyVal)) {
                if (!/Z/.test(keyVal)) {
                    keyVal += 'Z';
                }
                obj[keyName] = new Date(keyVal);

            } else if ((typeof keyVal === 'string') && /^-{0,1}[0-9\.]+$/.test(keyVal)) {
                if (keyVal.length < 12) {
                    obj[keyName] = parseFloat(keyVal);
                }
            }
        }

        for (const keyName in obj) {

            keyVal         = obj[keyName];
            let newKeyName = keyName.replace(/[^a-zA-Z0-9\ \_]+/g, '_');
            if (keyName === '$') {
                newKeyName = 'attr';
            }
            if (keyName === '_') {
                newKeyName = 'value';
            }
            if (newKeyName !== keyName) {
                obj[newKeyName] = keyVal;
                delete obj[keyName];
            }
        }

        return true;
    }

    /**
     * Given the final http response, convert the XML and check error codes.
     * Returns either a json object converted from XML or whatever page is returned in raw
     * html format if no XML was received.
     * @param res
     * @param body
     * @param bodyBuffer
     */
    doInternalProcessResult(res: any, body: any, bodyBuffer: any) {

        return new Promise((resolve, reject) => {

            if (config.traceEnabled) {
                config.status('EdgeRequest doInternalProcessResult Start');
                config.dump('EdgeRequest Result Headers', res.headers);
            }

            if (/image/.test(res.headers['content-type'])) {
                config.status('doInternalProcessResult found image');
                resolve(bodyBuffer);
            }

            if (/multipart/.test(res.headers['content-type'])) {
                //
                //  Parse out a multi-part response
                // config.dump 'headers', res.headers
                let m = res.headers['content-type'].match(/multipart.*boundary='([^']+)'/);

                if ((m == null) || (m[1] == null)) {
                    m = res.headers['content-type'].match(/multipart.*boundary=(.+)/);
                }

                if ((m != null) && (m[1] != null)) {
                    if (config.traceEnabled) {
                        config.status('MultipartOne writing to /tmp/multipart1.dat', bodyBuffer.toString());
                        fs.writeFileSync('/tmp/multipart1.dat', bodyBuffer);
                    }
                    m = new MultipartParser(m[1], bodyBuffer);
                    m.doParse();
                    resolve({ body: 'multipart', items: m.items, raw: bodyBuffer });
                    return;
                }
            }

            //
            //  Check for XML
            if (/json/.test(res.headers['content-type'])) {

                try {
                    let result = JSON.parse(body);

                    if (result == null) {
                        config.status('doInternalProcessResult null result from content');
                        resolve({ error: 'invalid json, no result object', body });
                        return true;
                    }

                    if (this.desiredResultKey != null) {
                        if ((result[this.desiredResultKey] != null) && (typeof result[this.desiredResultKey] === 'object')) {
                            result = result[this.desiredResultKey];
                        }
                    }

                    return resolve(result);

                } catch (error) {
                    return reject(error);
                }

            } else if (/xml/.test(res.headers['content-type'])) {

                try {

                    return parseString(body, async (err: any, result: any) => {
                        if (err != null) {
                            console.log('Invalid response, not XML');
                            console.log('body=', body);
                            return reject(err);
                        } else {

                            if (result == null) {
                                config.status('doInternalProcessResult null result from content xml');
                            }

                            if (this.desiredResultKey != null) {
                                if ((result[this.desiredResultKey] != null) && (typeof result[this.desiredResultKey] === 'object')) {
                                    result = result[this.desiredResultKey];
                                }
                            }

                            if (this.strDebugFilename) {
                                await EdgeApi.dumpObject(result, this.strDebugFilename, 'result');
                            }

                            if (globalDebugRequest) {
                                console.log('Sending JSON to be processed');
                            }

                            return resolve(result);
                        }
                    });

                } catch (error) {
                    config.getLogger('EdgeRequest').error('XML Parse Error', { body });
                    return reject(error);
                }

            } else {

                return resolve(body);
            }
        });
    }

    //
    //  A placeholder for a callback that can be used to modify the request
    //  prior to sending.  return false to stop the request from going out.
    onBeforeSendRequest() {
        return true;
    }

    resetTimeout(shouldRestart?: boolean) {

        if (this.watchdogTimer != null) {
            clearTimeout(this.watchdogTimer);
        }

        if ((shouldRestart != null) && shouldRestart && (this.timeoutMS != null) && (this.timeoutMS > 0)) {

            if ((!this.req)) {
                console.log('Warning, missing @req');
                return;
            }
            this.watchdogTimer = setTimeout(this.onRequestTimeout, this.timeoutMS);
        }

        return true;
    }

    //
    //  Internal function takes the final options for Node's http request
    //  downloads the page.   It combines the global @headers and @cookies
    //  before downloading, parses the cookies and updates the jar.
    //
    //  Retruns and object with res: and body: defined.
    //
    doInternalSendRequest(options: any) {

        let keyName;
        let keyVal;

        if (options.headers == null) {
            options.headers = {};
        }

        if (this.userAgent) {
            options.headers['User-Agent'] = this.userAgent;
        }

        // Sane defaults
        options.headers['Accept']          = '*/*';
        options.headers['Accept-Encoding'] = 'gzip,deflate';
        options.method                     = this.method;

        if (!globalUseCharlesProxy) {
            options.headers['Host'] = options.host;
        }

        //
        //  An optional function that can modify the request prior to sending,
        //  can be used in a subclass or dynamic modification
        const result = this.onBeforeSendRequest();
        if ((result != null) && (result === false)) {
            return null;
        }

        if (this.challenge != null) {
            this.internalSetNextCommand(options);
        }

        for (keyName in this.headers) {
            keyVal                   = this.headers[keyName];
            options.headers[keyName] = keyVal;
        }

        let strCookie = '';
        for (keyName in this.cookies) {
            keyVal = this.cookies[keyName];
            strCookie += `${keyName}=${keyVal};`;
        }

        if (strCookie.length) {
            this.headers['Cookie']    = strCookie;
            options.headers['Cookie'] = strCookie;
        }

        //
        //  Don't keep the connection open
        options.headers['Connection'] = 'Close';
        this.headers['Connection']    = 'Close';

        return new Promise((resolve, reject) => {

            try {

                let bar: any;
                if (globalDebugRequest) {
                    console.log('Sending request:', options);
                    if (this.postData) {
                        console.log('Post data:', this.postData);
                    }
                }

                let bodyText            = '';
                let bodyBuffer          = null;
                const bodyChunks: any[] = [];
                let totalLen            = 0;
                let totalRecv           = 0;

                let runner = http.request;
                if (typeof options.port === 'string') {
                    options.port = parseInt(options.port, 10);
                }

                if (/^https:\/\//.test(this.baseUrl)) {
                    runner = https.request;
                }

                //
                //  Close the connection after each request
                //
                // TODO I dont think this is valid operation, commenting out for now
                // runner.shouldKeepAlive = false;

                if (config.traceEnabled) {
                    config.status('EdgeRequest doInternalSendRequest Start');
                    config.dump('EdgeRequest Options', options);
                }

                if (this.useStatusUpdate) {
                    bar = new RateCapture(`Downloading ${this.baseUrl}`);
                }

                this.req = runner(options, (res: any) => {

                    if (options.orig != null) {
                        options.path = options.orig.path;
                        options.port = options.orig.port;
                        options.host = options.orig.host;
                    }

                    if (res.headers['content-length'] != null) {
                        totalLen = parseInt(res.headers['content-length'], 10);
                    }

                    if (globalDebugRequest) {
                        console.log('Got status ', res.statusCode, res.statusMessage);
                    }

                    this.internalParseCookie(res);

                    res.on('error', (err: Error) => {
                        console.log('Request error:', err);
                        return process.exit(0);
                    });

                    res.on('data', (data: any) => {
                        //
                        // a block of bytes has been received, add it to the buffer
                        // and add some amount to the rate capture bar, only display the
                        // bar once every 10 times data is received.
                        //
                        bodyChunks.push(data);
                        totalRecv += data.length;

                        if (this.useStatusUpdate) {
                            bar.addSample(totalRecv, totalLen);
                        }
                        this.resetTimeout(true);
                        return true;
                    });

                    res.on('abort', () => {
                        this.resetTimeout(false);
                        bodyBuffer = Buffer.concat(bodyChunks);
                        if (this.useStatusUpdate) {
                            bar.stop();
                        }
                        reject('Connection timeout');
                        return false;
                    });

                    return res.on('end', () => {

                        //
                        //  Done receiving, decompress the buffer if needed
                        //  return the result as text.
                        //
                        this.resetTimeout(false);
                        bodyBuffer = Buffer.concat(bodyChunks);
                        if (this.useStatusUpdate) {
                            bar.stop();
                        }
                        if ((res.headers['content-encoding'] != null) && (res.headers['content-encoding'] === 'gzip')) {
                            if (globalDebugRequest) {
                                console.log('Gzip encoding found');
                            }
                            return zlib.gunzip(bodyBuffer, (err: any, data: any) => {
                                if (err) {
                                    return reject(err);
                                }
                                if (data == null) {
                                    data = '';
                                }
                                return resolve({
                                    res,
                                    bodyBuffer: data,
                                    body      : data.toString()
                                });
                            });

                        } else if ((res.headers['content-encoding'] != null) && (res.headers['content-encoding'] === 'deflate')) {
                            if (globalDebugRequest) {
                                console.log('Deflate encoding found');
                            }
                            return zlib.inflate(bodyBuffer, (err: any, data: any) => {
                                if (err) {
                                    return reject(err);
                                }
                                if (data == null) {
                                    data = '';
                                }
                                return resolve({
                                    res,
                                    bodyBuffer: data,
                                    body      : data.toString()
                                });
                            });

                        } else {
                            bodyText = bodyBuffer.toString();
                            return resolve({
                                bodyBuffer,
                                res,
                                body: bodyText
                            });
                        }
                    });
                });


                if (this.postData) {
                    if (globalDebugRequest) {
                        console.log('Sending POST:', this.postData);
                    }

                    this.req.write(this.postData);
                }

                this.req.end();

                return this.resetTimeout(true);


            } catch (e) {

                console.log('EdgeRequest Caught:', e);
                return reject(e);
            }
        });
    }

    onRequestTimeout = () => {
        config.status('EdgeRequest Timeout');
        this.req.abort();
        return true;
    };
}

export default EdgeRequest;