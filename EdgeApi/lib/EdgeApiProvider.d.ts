/// <reference types="socket.io-client" />
import { EdgeApiDefinition, EdgeSocketClient } from './EdgeApi';
declare class EdgeApiProviderStream {
    stream_id: any;
    private allData;
    private isFinished;
    constructor(provider: EdgeApiProvider, stream_id: any);
    onData(record: any): boolean;
    onFinished(): any;
    /**
     * Create a promise that waits until all results are available
     */
    doWaitUntilFinished(): Promise<any>;
    onDataCallback(data: any): boolean;
}
export declare class EdgeApiProvider {
    [DynamicFunctionName: string]: any;
    isVerbose: boolean;
    dynamicResolveList: {
        [uui: string]: EdgeApiProviderStream | any;
    };
    socket: EdgeSocketClient;
    registeredList: any[];
    commandCallback: {
        [callName: string]: EdgeApiDefinition;
    };
    dynamicApiCallsList: {
        [index: string]: any;
    };
    dynamicCallPending: any[];
    dynamicRejectList: any[];
    constructor();
    /**
     * @returns uuid for api call
     */
    fastUUID(): string;
    /**
     * Callback when socket is connected again
     * @param socket socket to reconnect
     */
    onReconnect(socket?: SocketIOClient.Socket): boolean;
    /**
     * Default event when a command is done executing
     *
     */
    onCmdReplyComplete(data: any): void;
    /**
     * Setup the socket to listen for an incoming reply
     * @param socket socket to setup
     */
    onSetupSocket(socket: any): void;
    /**
     * This should be a reply to our command which must contain the UUID of the request
     * @param data result from the cmd reply
     */
    onIncomingCommandReply(data: any): boolean;
    /**
     * @returns get pending call length
     */
    getPending(): number;
    /**
     * Return when all pending calls are complete
     * allows a timeout
     * @param waitTimeout wait time span
     */
    doCompletePending(waitTimeout?: number): Promise<unknown>;
    /**
     * Callback when it get api list from serer
     * @param data Api call list
     */
    onReceiveApiList(data: any): boolean;
    /**
     * Execute a dynamic call, returns a promise
     * the promise is resolve with the result of the API call
     * Param is an array of call values or an object with call values
     * @param callName api name
     * @param params array of call values or an object with call values
     * @returns success or failure
     */
    MakeDynamicCall(callName: string, params: any[]): Promise<boolean>;
    /** Register a single API command, create a dynamic function that calls it
     * This is an API call that the server knows how to answer, not a call
     * that we are capable of answering on this client
     * @param callName call name to add
     * @param callParams call params to add
     */
    addDynamicApiCall(callName: string, callParams: object): boolean;
    /**
     * Execute a local command that is a registered API call.
     * The callData comes from the caller and may or may not match
     * the requirements of the call.
     * @param callName
     * @param callData
     */
    onExecuteLocalCommand(callName: string, callData: any): any;
    /**
     * Callback when it got result from server
     * @param data data from server
     */
    onIncomingCommandRequest(data: any): boolean;
    /**
     * Tell the server we are able to support a given command
     * When the command arrives from the socket, we'll execute it
     * and return the result.  The promise here returns after
     * the registration is sent to the server
     * @param apiDefinition
     */
    doRegisterApiCommand(apiDefinition: EdgeApiDefinition): Promise<boolean>;
}
export default EdgeApiProvider;
