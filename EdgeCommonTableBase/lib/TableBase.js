"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var storage = require("node-persist");
var crypto_1 = require("crypto");
var edgecommonconfig_1 = require("edgecommonconfig");
var edgeapi_1 = require("edgeapi");
/**
 * This is the base class for a table that is stored on a remote
 * storage technology. Right now this is coded against the Edge API for loading / saving records.
 */
var TableBase = /** @class */ (function () {
    function TableBase(dataSet, tableName, id) {
        this.dataSet = dataSet;
        this.tableName = tableName;
        this.id = id;
        this.api = null;
        this.isConnected = false;
        this.marks = null;
    }
    TableBase.prototype.isset = function (path) {
        var base = this.data;
        var parts = path.split(/[\/\.]/);
        while (parts.length > 0) {
            var name_1 = String(parts.shift());
            if (!base) {
                return false;
            }
            if (!base[name_1]) {
                return false;
            }
            base = base[name_1];
        }
        if (!base) {
            return false;
        }
        return true;
    };
    /**
     * Returns a value give a path such as /test/data/name or test.name
     */
    TableBase.prototype.safeString = function (path) {
        var base = this.data;
        var parts = path.split(/[\/\.]/);
        while (parts.length > 0) {
            var name_2 = String(parts.shift());
            if (!base) {
                return '';
            }
            if (!base[name_2]) {
                return '';
            }
            base = base[name_2];
        }
        if (!base) {
            return '';
        }
        return base.toString();
    };
    TableBase.prototype.safeNumber = function (path) {
        var base = this.data;
        var parts = path.split(/[\/\.]/);
        while (parts.length > 0) {
            var name_3 = String(parts.shift());
            if (!base) {
                return 0;
            }
            if (!base[name_3]) {
                return 0;
            }
            base = base[name_3];
        }
        if (!base) {
            return 0;
        }
        if (typeof base === 'number') {
            return base;
        }
        return parseFloat(base);
    };
    TableBase.prototype.safeBool = function (path) {
        var base = this.data;
        var parts = path.split(/[\/\.]/);
        while (parts.length > 0) {
            var name_4 = String(parts.shift());
            if (!base) {
                return false;
            }
            if (!base[name_4]) {
                return false;
            }
            base = base[name_4];
        }
        if (!base) {
            return false;
        }
        if (base === true ||
            base === 'Yes' ||
            base === 'Y' ||
            base === 'yes' ||
            base === 'true' ||
            base === '1' ||
            base === 1) {
            return true;
        }
        return false;
    };
    /**
     * Called after a connection is established for the first time.
     * Should be used to check indexes and such.
     */
    TableBase.prototype.onVerifyModel = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, true];
            });
        });
    };
    /**
     * You can overwrite this function to see what changed.
     */
    TableBase.prototype.onSaved = function (totalChanges, newData) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, true];
            });
        });
    };
    /**
     * Potentially subject changes to this record.
     */
    TableBase.prototype.doSave = function () {
        return __awaiter(this, void 0, void 0, function () {
            var newData, totalChanges, varName, recordHash, hashValue, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.data) {
                            throw new Error("Invalid call to doSave on " + this.tableName + ", id=" + this.id + ", not loaded");
                        }
                        edgecommonconfig_1.config.status('doSave::doConnect()');
                        return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _a.sent();
                        if (!this.marks) return [3 /*break*/, 7];
                        newData = {};
                        totalChanges = 0;
                        for (varName in this.data) {
                            if (varName === '_id' || varName === '_lastModified') {
                                continue;
                            }
                            recordHash = crypto_1.createHash('sha1');
                            edgeapi_1.default.getDataHash(recordHash, this.data[varName]);
                            hashValue = recordHash.digest('hex');
                            if (!this.marks[varName] || hashValue !== this.marks[varName]) {
                                newData[varName] = this.data[varName];
                                totalChanges++;
                                edgecommonconfig_1.config.status("Change found " + this.dataSet + "://" + this.tableName + "/" + this.id + " for " + varName);
                                this.marks[varName] = hashValue;
                            }
                            else {
                                edgecommonconfig_1.config.status("No change in " + this.dataSet + "://" + this.tableName + "/" + this.id + " for " + varName + " [" + hashValue + "]");
                            }
                        }
                        if (!(totalChanges === 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.onSaved(0, null)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, 0];
                    case 3:
                        newData._lastModified = new Date();
                        edgecommonconfig_1.config.status(this.dataSet + "://" + this.tableName + "/" + this.id + " -- Found " + totalChanges + " total changes", newData);
                        return [4 /*yield*/, this.api.data_doUpdatePath(this.dataSet, "/" + this.tableName + "/" + this.id, newData)];
                    case 4:
                        _a.sent();
                        edgecommonconfig_1.config.status('Calling on Saved');
                        result = this.onSaved(totalChanges, newData);
                        if (!result) return [3 /*break*/, 6];
                        return [4 /*yield*/, result];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/, totalChanges];
                    case 7:
                        edgecommonconfig_1.config.status("doSave::data_doUpdatePath " + this.dataSet + ", /" + this.tableName + "/" + this.id);
                        return [4 /*yield*/, this.api.data_doUpdatePath(this.dataSet, "/" + this.tableName + "/" + this.id, this.data)];
                    case 8:
                        _a.sent();
                        return [4 /*yield*/, this.onSaved(10000, this.data)];
                    case 9:
                        _a.sent();
                        _a.label = 10;
                    case 10:
                        edgecommonconfig_1.config.status('Saved');
                        return [2 /*return*/, true];
                }
            });
        });
    };
    TableBase.prototype.doVerifyLoaded = function () {
        return __awaiter(this, void 0, void 0, function () {
            var cacheKey, existingRecord, tmpData, _a, e_1, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        edgecommonconfig_1.config.status('TableBase doVerifyLoaded Starting');
                        return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _c.sent();
                        _c.label = 2;
                    case 2:
                        _c.trys.push([2, 8, , 10]);
                        cacheKey = this.dataSet + ":" + this.tableName + "/" + this.id;
                        existingRecord = storage.getItemSync(cacheKey);
                        if (!existingRecord) return [3 /*break*/, 4];
                        // Check the hash
                        edgecommonconfig_1.config.status("TableBase doVerifyLoaded Gettting If Newer /" + this.tableName + "/" + this.id);
                        return [4 /*yield*/, this.api.data_doGetItemIfNewer(this.dataSet, "/" + this.tableName + "/" + this.id, existingRecord._hash)];
                    case 3:
                        tmpData = _c.sent();
                        if (typeof tmpData === 'object') {
                            this.data = tmpData;
                        }
                        else {
                            this.data = existingRecord;
                        }
                        return [3 /*break*/, 7];
                    case 4:
                        edgecommonconfig_1.config.status("TableBase doVerifyLoaded Getting Item /" + this.tableName + "/" + this.id);
                        _a = this;
                        return [4 /*yield*/, this.api.data_doGetItem(this.dataSet, "/" + this.tableName + "/" + this.id)];
                    case 5:
                        _a.data = _c.sent();
                        return [4 /*yield*/, storage.setItem(cacheKey, this.data)];
                    case 6:
                        _c.sent();
                        _c.label = 7;
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        e_1 = _c.sent();
                        edgecommonconfig_1.config.status('Exception in doVerifyLoaded:', e_1);
                        _b = this;
                        return [4 /*yield*/, this.api.data_doGetItem(this.dataSet, "/" + this.tableName + "/" + this.id)];
                    case 9:
                        _b.data = _c.sent();
                        return [3 /*break*/, 10];
                    case 10:
                        edgecommonconfig_1.config.status('doVerifyLoaded Marking values');
                        this.markDataValues();
                        return [2 /*return*/, this.data];
                }
            });
        });
    };
    /**
     * Execute an aggregate
     */
    TableBase.prototype.doAggregate = function (aggList) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.api.data_doAggregate(this.dataSet, "/" + this.tableName, aggList)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Load a single record
     */
    TableBase.prototype.doFind = function (condition) {
        return __awaiter(this, void 0, void 0, function () {
            var all;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.doFindAll(condition, null, 0, 1)];
                    case 1:
                        all = _a.sent();
                        if (!all) {
                            return [2 /*return*/, null];
                        }
                        if (all.length === 0) {
                            return [2 /*return*/, null];
                        }
                        return [2 /*return*/, all[0]];
                }
            });
        });
    };
    TableBase.prototype.doRemove = function (condition) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.isConnected) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.api.data_doRemove(this.dataSet, this.tableName, condition)];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    TableBase.prototype.doFindAll = function (condition, sortCondition, offset, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var className, strCondition, varName, all, results, _i, all_1, obj, oneItem;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        className = this.constructor.name;
                        if (!condition) {
                            condition = '';
                        }
                        if (typeof condition === 'object') {
                            strCondition = '';
                            for (varName in condition) {
                                if (strCondition.length > 0) {
                                    strCondition += ',';
                                }
                                strCondition += varName + ":" + condition[varName];
                            }
                            condition = strCondition;
                        }
                        sortCondition = sortCondition || { _id: 1 };
                        offset = offset || 0;
                        limit = limit || 1000;
                        return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.api.data_doGetItems(this.dataSet, "/" + this.tableName + "/" + condition, null, sortCondition, offset, limit)];
                    case 2:
                        all = _a.sent();
                        results = [];
                        for (_i = 0, all_1 = all; _i < all_1.length; _i++) {
                            obj = all_1[_i];
                            oneItem = new TableBase(this.dataSet, this.tableName, obj.id);
                            oneItem.data = obj;
                            results.push(oneItem);
                        }
                        return [2 /*return*/, results];
                }
            });
        });
    };
    TableBase.prototype.doVerifyIndex = function (keyName, indexType, isUnique) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.isConnected) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.doConnect()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.api.data_doVerifySimpleIndex(this.dataSet, this.tableName, keyName, indexType, isUnique)];
                    case 3:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * Called when an object is loaded to create hash values from the top level attributes
     * this way we can avoid sending them to save if they don't change
     */
    TableBase.prototype.markDataValues = function () {
        edgecommonconfig_1.config.status('TableBase::markDataValues Start');
        this.marks = {};
        for (var varName in this.data) {
            if (varName === '_id' || varName === '_lastModified') {
                continue;
            }
            var recordHash = crypto_1.createHash('sha1');
            edgeapi_1.default.getDataHash(recordHash, this.data[varName]);
            this.marks[varName] = recordHash.digest('hex');
        }
        edgecommonconfig_1.config.status('TableBase::markDataValues Complete');
        return true;
    };
    TableBase.prototype.doConnect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, result;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        edgecommonconfig_1.config.status('TableBase doConnect Starting');
                        _a = this;
                        return [4 /*yield*/, edgeapi_1.default.doGetApi()];
                    case 1:
                        _a.api = _b.sent();
                        this.isConnected = true;
                        edgecommonconfig_1.config.status('TableBase Verify Model Starting');
                        result = this.onVerifyModel();
                        if (!(result && result.then)) return [3 /*break*/, 3];
                        return [4 /*yield*/, result];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        edgecommonconfig_1.config.status('TableBase doConnect Connected');
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return TableBase;
}());
exports.default = TableBase;
//# sourceMappingURL=TableBase.js.map