$ ->
	toh = null
	addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

	addTestButton "Simple Message Box", "Open", ()->

		m = new ModalMessageBox "Test message box"

	addTestButton "Change Title", "Open", ()->

		m = new ModalForm
			title:   "Change title test"
			content: "Test message box"

	addTestButton "Change Buttons", "Open", ()->

		m = new ModalForm
			title:   "Change buttons test"
			content: "Test message box"
			ok:      "I am 1"
			close:   "I am 2"
			buttons: [
				type: "button"
				# label: "Click to close"
				text: "Close"
				onClick: (form) ->
					alert "Close button clicked."
			,
				type: "submit"
				label: ""
				text: "Submit"
			]
			onSubmit: (form) ->
				alert "Form submitted."

	addTestButton "Custom Events", "Open", ()->

		m = new ModalDialog
			title:   "Change buttons test"
			content: "Test message box"
			close:   "I am 1"
			ok:      "I am 2"
			onButton1: ()->
				console.log "On Button 1 Custom"
				true
			onButton2: (e)->
				console.log "On Button 2 Custom", e
				true

	addTestButton "Error Message", "Open", ()->

		m = new ErrorMessageBox "Error message text"

	addTestButton "Busy Dialog 1", "Open", ()->
		clearTimeout(toh)
		window.globalBusyDialog.showBusy "Busy doing something for 3 seconds"
		toh = setTimeout ()->
			window.globalBusyDialog.finished()
		, 3000

	addTestButton "Busy Dialog 2", "Open", ()->
		clearTimeout(toh)
		window.globalBusyDialog.showBusy "Busy doing something first"
		toh = setTimeout ()->
			window.globalBusyDialog.showBusy "Busy doing something else"
			toh = setTimeout ()->
				window.globalBusyDialog.finished()
				toh = setTimeout ()->
					##|  Close out the first task
					window.globalBusyDialog.finished()
				, 3000
			, 3000
		, 3000

	addTestButton "Busy Dialog - Percents 1", "Open", ()->
		window.clearTimeout(toh)
		window.globalBusyDialog.showBusy "Doing something that takes time"
		window.globalBusyDialog.setMinMax(0, 100)

		counter = 0
		do loopRunning = ()->
			window.globalBusyDialog.updatePercent(counter++)
			if counter < 100
				toh = setTimeout loopRunning, 100
			else
				window.globalBusyDialog.finished()

	addTestButton "Simple Form 1", "Open", () ->

		m = new ModalForm
			title:        "Form Title"
			content:      "Fill out this example form"
			showOnCreate: false
			inputs: [
				name: "input1"
				label: "Example Input 1"
			]
			buttons: [
				type: "submit"
				label: ""
				text: "OK"
			]
			onSubmit: (form) ->
				console.log "Submitted form, test value=", form.input1.getValue()
				m.hide()

	addTestButton "Simple Form 2", "Open", () ->
		m = new ModalForm
			title:        "Form Title"
			content:      "Fill out this example form"
			inputs: [
				name: "input1"
				label: "Example Input 1"
			,
				name: "input2"
				label: "Example Input 2"
			,
				name: "input3"
				label: "Example Input 3"
			]
			buttons: [
				type: "button"
				text: "Cancel"
				onClick: () ->
					alert "Sorry, you cancelled it..."
			,
				type: "submit"
				text: "Go"
			]
			onSubmit: (form) ->
				console.log "Submitted form, test value1=", form.input1.getValue()
				console.log "Submitted form, test value2=", form.input2.getValue()
				console.log "Submitted form, test value3=", form.input3.getValue()
				m.hide()

	addTestButton "Tags Input (Selectize)", "Open", () ->

		m = new ModalDialog
			showOnCreate: false
			content:      "Fill out this example form"
			title:        "Form Title"
			ok:           "Go"

		m.getForm (formView) ->
			formView.addInput "input1", "Example Input 1"
			.setOptions "Apple, Banana, Peach"
			.setValue "Apple"
			.setDataFormatter "tags"

			formView.setSubmitFunction (form) =>
				console.log "Submitted form, test value 1=", form.input1
				m.hide()
		m.show()

	states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia',
	'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts',
	'Michigan', 'Minnesota','Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
	'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina',
	'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming' ]

	addTestButton "Form Typeahead", "Open", () ->

		m = new ModalDialog
			showOnCreate: false
			content:      "Type part of a state name"
			position:     "top"
			title:        "Typeahead Test"
			ok:           "Go"

		m.getForm (formView) ->
			formView.addInput "input1", "State"
			.setOptions states
			.setDataFormatter "enum"
			formView.setSubmitFunction (form) =>
				console.log "Submitted form, test value=", form.input1
				m.hide()

		m.show()

	addTestButton "ModalViewDialog", "Open", () ->
		m = new ModalViewDialog
			showOnCreate: false
			content:      "Fill out this example form"
			title:        "Form Title"
			ok:           "Go"

		m.setFormView (view)->
			view.addInput "input1", "Example Input 1"
			view.addInput "input2", "Example Input 2"
			view.addInput "input3", "Example Input 3"

			view.setSubmitFunction (form) =>
				console.log "Submitted form, test value 1=", form.input1
				console.log "Submitted form, test value 2=", form.input2
				console.log "Submitted form, test value 3=", form.input3
				m.hide()
			console.log "FormView created"
			view.show()

		m.onButton2 = (e, fields) ->
			console.log "FIELDS=", fields
			m.hide()
			return true

		m.show()

	go()
