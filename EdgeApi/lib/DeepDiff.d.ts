declare const deepDiff: (source: any, dest: any, basePath: any[]) => any[];
export default deepDiff;
