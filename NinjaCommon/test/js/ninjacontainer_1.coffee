$ ->

    addTestButton "Simple Widget with HTML", "Open", (e)->
        addHolder().addDiv().html "HTML Set"

    addTestButton "Simple Widget Resize Test", "Open", (e)->
        w = addHolder()

        info = w.addDiv()
        info.css "padding", 10
        info.css "fontSize", "18px"

        ##|  save old event
        superResize = w.onResize

        ##|  intercept onResize which happens after the resize is complete
        w.onResize = (w, h)->
            info.html "My size is now #{w}, #{h}"
            superResize(w, h)

        info.html "Resize the browser or move the splitter to change the size of this test window."

    addHolder().addDiv().html "This page shows examples of the base NinjaContainer which is the base class behind views, widgets, and screens."

    addTestButton "Simple View showing size for comparison", "Open", (e)->

        w = addHolder()
        w.setView "TestShowSize"

    addTestButton "Multiple Views in the same Holder", "Open", (e)->

        w = addHolder()
        w.setView "TestShowSize"
        w.setView "ErrorMessage", (view) ->
            view.setMessage "This is an error message"

    addTestButton "Show/Hide Child of ViewDynamicTabs", "Open", (e)->
        addHolder("renderTest1")
        .setView "DynamicTabs", (tabs)->
            tab1 = tabs.addTab "Test 1", "Default One"
            tab2 = tabs.addTab "Test 2", "Default Two", 1
            tab3 = tabs.addTab "Test 3", "Default Three", 0
            tab4 = tabs.addTab "Test 4", "Default Four"
            tab5 = tabs.addTab "Test 5", "Default Five", 2
            tabs.show "tab1"
            setTimeout () =>
                tab2.tab.hide()
            , 2000
            setTimeout () =>
                tab5.tab.hide()
            , 4000
            setTimeout () =>
                tab4.tab.hide()
            , 6000                        
            setTimeout () =>
                tab5.tab.show()
            , 8000
            setTimeout () =>
                tab4.tab.show()
            , 10000
            setTimeout () =>
                tab2.tab.show()
            , 12000

    go()
