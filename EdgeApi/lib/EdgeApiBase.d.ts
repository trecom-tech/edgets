/// <reference types="node" />
/// <reference types="socket.io-client" />
/**
 * This is the base class for calling the Edge API Server
 * The api can expose RPC functions and it can consume functions
 */
import * as EvEmitter from 'events';
import EdgeApiProvider from './EdgeApiProvider';
import { EdgeSubscription } from './EdgeApi';
export declare abstract class EdgeApiBase extends EdgeApiProvider {
    subscriptionList: EdgeSubscription[];
    registeredList: any[];
    connectPromise: Promise<boolean>;
    waitForPromises: {
        [index: string]: Promise<boolean>;
    };
    emitter: EvEmitter;
    isVerbose: boolean;
    commandCallback: {};
    listeners: any;
    constructor();
    /**
     * Establish a socket connection to the server
     * Allow registering for push message actions
     * @param pushMessage
     * @param callback
     */
    on(pushMessage: string, callback: any): EvEmitter;
    listen(channel: string, callback: any): void;
    /**
     * Stop a push message action
     * @param pushMessage
     * @param callback
     */
    off(pushMessage: string, callback: any): EvEmitter;
    /**
     * Single callback for an event
     * @param pushMessage
     * @param callback
     */
    once(pushMessage: string, callback: any): EvEmitter;
    /**
     *
     * @param socket
     */
    onConnect(socket: SocketIOClient.Socket): boolean;
    /**
     *
     */
    onDisconnect(): boolean;
    onReady(): boolean;
    /**
     *
     * @param socketConnectCallbackList
     */
    doGetSocket(socketConnectCallbackList: Function[]): Promise<any>;
    onCmdReplyComplete(): void;
    processChangeEvent(data: any): boolean;
    onInitializeSocket(): Promise<unknown>;
    /**
     * Wait until callName gets available
     * @param callName string
     */
    doWaitForApi(callName: string): Promise<boolean>;
    /**
     * Create socket and wait to be connected.
     */
    doConnect(): Promise<boolean>;
    doConnectPromise(): Promise<boolean>;
    /**
     * Subscribe to a given path
     * @param channel
     */
    subscribe(channel: string): boolean;
    /**
     * Send a command to the server
     * @param command
     * @param options
     */
    sendCommand(command: string, options: any): boolean;
}
export default EdgeApiBase;
