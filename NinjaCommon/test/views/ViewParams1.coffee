class ViewParams1 extends View

    ##|  using an array of layout view helpers, with options
    ##|
    getLayout: ()=>
        view:    "Splittable"
        name:    "viewSplitter"
        percent: 30
        top:
            view:             "Table"
            name:             "viewTable"
            title:            "Zipcode data for New England"
            collection:       "zipcode"
            enablecheckboxes: true
        bottom:
            view: "Splittable"
            percent: 50
            right:
                view: "TestShowZipcode"
                name: "viewZipview2"
            left:
                view: "DynamicTabs"
                name: "viewTabs"
                tabs : [
                    title: "Zipcode View 1"
                    view:  "TestShowZipcode"
                    name:  "viewZipview1"
                ]

    selectZipcode: (@id)=>
        ##|
        ##|  Select a zipcode which should update the children views
        for id, isLocked of @viewTable.getLocked()
            @viewTable.removeLock(id)

        @viewTable.addLock(@id)
        @cascade("setZipcode", @id)
        true

    onShowScreen: ()=>
        console.log "ViewParams1 onShowScreen"
        @selectZipcode("01002")

        @viewTable.moveActionColumn "Weather"

        globalTableEvents.on "row_selected", (tableName, id, status)=>
            if tableName is "zipcode"
                if status then @selectZipcode id

