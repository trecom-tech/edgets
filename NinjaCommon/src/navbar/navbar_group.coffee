##
## class to create groups in ViewNavBar
##

class NavGroup 
	@DEFAULT_MAX_ITEMS : 5
	@DEFAULT_ICON : ""
	constructor: (@widget, optionData = {}) ->
		@options            = {}
		@options.id         = optionData.id
		@options.parentView = optionData.parentView
		@options.icon       = optionData.icon || NavGroup.DEFAULT_ICON
		@options.title      = optionData.title
		@options.isEnabled  = if optionData.isEnabled is undefined then true else optionData.isEnabled
		@options.isOpen     = if optionData.isOpen is undefined then false else optionData.isOpen
		@options.maxItems   = optionData.maxItems || NavGroup.DEFAULT_MAX_ITEMS
		@options.hasFilter  = optionData.hasFilter

		@wgtTitleWrapper    = @widget.add "div", "ninja-nav-group-title-wrapper"
		@wgtTitleWrapper.on "click", @toggleGroup
		# @wgtTitleWrapper.add "span", "arrow"

		if @options.icon? and @options.icon != ""
			@setIcon @options.icon

		@wgtTitle = @wgtTitleWrapper.addDiv "ninja-nav-group-title"
		@wgtTitle.html "<span>#{@options.title}</span>"
		
		@wgtItems = @widget.add "ul", "ninja-nav-group-items collapse", "ninja-nav-group-items-#{@options.id}"

		if @options.hasFilter
			@addFilter()

			@wgtNotFound = @wgtItems.add "div", "ninja-nav-group-not-found"
			@wgtNotFound.el.html "No matches found."
			@wgtNotFound.hide()

		@setOpen @options.isOpen
		@setEnabled @options.isEnabled		
		@items = []

	setTitle: (title) =>
		if !title then return false
		@options.title = title
		@wgtTitle.html "<span>#{@options.title}</span>"		
		true

	setIcon: (icon) =>
		if !icon or icon is "" then return false
		@options.icon = icon
		if !@wgtIconWrapper?
			@wgtIconWrapper = @wgtTitleWrapper.addAtPosition "div", "ninja-nav-group-icon-wrapper fa-hover", 0

		if @options.icon.includes("class")
			@wgtIconWrapper.html @options.icon
		else
			@wgtIconWrapper.html "<i class='#{@options.icon}' />"
		true

	setEnabled: (bool) =>
		if typeof bool isnt "boolean" 
			return false
			
		@options.isEnabled = bool
		@wgtTitleWrapper.setClass "disabled", !@options.isEnabled
		if !bool then @setOpen false
		clickHandler = (e) =>
			e.preventDefault()
			e.stopPropagation()
			true
		if !bool
			@widget.el.on "click", clickHandler
		else
			@widget.el.off "click", clickHandler
		true

	setOpen: (bool) =>
		if typeof bool isnt "boolean"
			return false
		@options.isOpen = bool
		@widget.setClass "collapsed", !@options.isOpen
		@wgtItems.setClass "in", @options.isOpen
		true

	toggleGroup: () =>
		@setOpen !@options.isOpen
		true

	setMaxItems: (maxItems) =>
		if maxItems < 1 then return false
		@options.maxItems = maxItems
		true

	addFilter: () =>
		if !@wgtItems? then return false
		@wgtFilterWrapper = @wgtItems.add "div", "ninja-nav-group-filter-wrapper"
		@wgtFilterInput = @wgtFilterWrapper.add "input", "ninja-nav-group-filter-input"
		@wgtFilterClose = @wgtFilterWrapper.add "div", "ninja-nav-group-filter-close"
		@wgtFilterClose.el.html "<i class='far fa-times-circle ' />"
		@setUpFilterEventHandler()

		if !@items or !@items.length then @wgtFilterWrapper.hide()
		true

	##|
	##| Remove selected status on all child items
	deselectAll: ()=>
		for item in @items
			item.setSelected false
		true

	addItem: (options) =>
	
		itemId              = 'nav-item-' + GlobalValueManager.NextGlobalID()
		
		if @items.length    >= @options.maxItems 
			@items.splice @options.maxItems - 1
			@wgtItems.children[@options.maxItems - 1].destroy()
		
		if options.location is 'top'
			itemWidget      = @wgtItems.addAtPosition 'li', 'ninja-nav-item', if @options.hasFilter then 2 else 0	
			itemWidget.id   = itemId
		else 
			itemWidget      = @wgtItems.add 'li', 'ninja-nav-item', itemId
		
		options.parentGroup = this
		options.id          = itemId
		itemWidget.el.on "click", (e) =>
			##|
			##|  De-select all previously selected options
			@options.parentView.deselectAll()
		
		item = new NavItem itemWidget, options
		if options.location is 'top'
			@items.unshift item
		else 
			@items.push item

		if @wgtFilterWrapper? then @wgtFilterWrapper.show()
		item

	##
	## remove all items in a group
	removeAllItems: () =>
		@deselectAll()
		for item in @items
			item.widget.destroy()
		@items = []
		true

	onResize: (w, h, @narrowed) =>
		@widget.width w
		if @narrowed 
			@wgtTitle.hide()
			@wgtTitleWrapper.setSize(w / 2, w / 2)

			if @wgtIcon?
				@wgtIconWrapper.addClass "full-width"
				@wgtIcon.el.css "font-size", w / 2 - 10

			@widget.el.bind 'click', @showPopupMenu
		else
			@wgtTitle.show()
			@wgtTitleWrapper.setSize w, 30

			if @wgtIcon?
				@wgtIconWrapper.removeClass "full-width"
				@wgtIcon.el.css "font-size", 18
				
			@widget.el.unbind 'click', @showPopupMenu			

	showPopupMenu: (e) =>
		menu = new PopupMenu "#{@options.title}", e
		for item in @items
			menu.addItem item.options.title, item.options.callback, item.options.id

	setUpFilterEventHandler: () =>
		@wgtFilterWrapper.on "click", (e) =>
			# e.preventDefault()
			e.stopPropagation()
			true

		@wgtFilterInput.on "keyup", @filterItems


		@wgtFilterClose.on "click", (e) =>
			@wgtFilterInput.val("")
			@filterItems()
		true

	filterItems: () =>
		if !@wgtFilterInput? then return false
		filterKey = @wgtFilterInput.val().toLowerCase()
		shownItemCnt = 0
		for item in @items
			itemTitle = item.options.title.toLowerCase()
			if filterKey and !itemTitle.includes(filterKey)
				item.setFilteredOut true
			else
				item.setFilteredOut false
				shownItemCnt++

		if shownItemCnt is 0
			@wgtNotFound.show()
		else
			@wgtNotFound.hide()
		true

	serialize: () =>
		defaultObj = 
			icon 		: null
			title 		: null
			isEnabled 	: false
			isOpen		: false
			maxItems 	: @DEFAULT_MAX_ITEMS
		obj       = Object.assign defaultObj, @options
		obj.items = @items.map (item) => item.serialize()
		obj

	deserialize: (obj) =>
		@setTitle obj.title
		@setIcon  obj.icon
		@setEnabled obj.isEnabled
		@setOpen obj.isOpen
		@setMaxItems obj.maxItems

		@items = obj.items.map (item) =>
			navItem = @addItem {}
			navItem.deserialize item
			navItem
		true