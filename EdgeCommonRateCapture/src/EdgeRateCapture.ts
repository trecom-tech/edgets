import * as os            from 'os';
import StatusUpdateClient from 'edgecommonstatusupdateclient';
import EdgeTimeseriesData from 'edgecommontimeseriesdata';
import Ninja              from 'ninjadebug';

// we can not use import syntax for these libraries because they are not not typescript ready
const chalk   = require('chalk');
const numeral = require('numeral');

export default class EdgeRateCapture {
    readonly name: string;
    readonly uuid: string;
    readonly max: number        = 500;
    sampleValues: number[]      = [];
    sampleTimes: number[]       = [];
    loopTimer: number           = 1000;
    head: number                = 0;
    remain: number              = 0;
    lastTimer: any              = null;
    lastValue: number           = 0;
    varList: any                = {};
    varNames: any               = {};
    lastVars: any               = {};
    firstSeen: string           = new Date().toString();
    hideOutput: boolean         = false;
    logTimerseriesData: boolean = true;
    totalSamples: number        = 0;
    lastTimerStatus: any        = null;
    lastValueStatus: any        = null;
    statusUpdateClient: any     = null;
    status: any                 = null;
    timeSeriesData: any         = null;
    timeSeriesDatabase: any     = null;
    timeSeriesCounter: number   = 0;

    constructor(name: string) {
        this.name         = name;
        this.sampleValues = new Array(this.max).fill(-1);
        this.sampleTimes  = new Array(this.max).fill(-1);
        this.hideOutput   = !process.env.TOOL && process.env.TOOL === 'job';
        this.uuid         = `${ os.hostname() }${ name.slice(0, 9) }${ new Date().getTime() }`;

        setTimeout(() => this.statusUpdateTimer(), 5000);
    }

    onBeforeUpdate(varList: any, status: any) {
        // Placeholder for update
        return true
    }

    /**
     * Reset all the values we are tracking
     */
    reset() {
        this.sampleValues = new Array(this.max).fill(-1);
        this.sampleTimes  = new Array(this.max).fill(-1);
        this.lastTimer    = null;
        this.lastValue    = 0;

        return true;
    }

    /**
     * Stop tracking
     */
    stop() {
        if (this.statusUpdateClient) {
            this.status        = this.getStatusData();
            this.status.remain = -1;
            this.statusUpdateClient.sendStatus(this.status);
        }

        delete this.loopTimer;
    }

    /**
     * Increase the total units captured
     * @param totalSamples 
     * @param remain 
     */
    addSample(totalSamples: number, remain: number = 0) {
        if (totalSamples !== this.lastValue) {
            this.loopTimer = 1000;
        }

        if (this.lastTimer) {
            let timeDiff                 = process.hrtime(this.lastTimer);
            let proctime                 = timeDiff[0] * 1e9 + timeDiff[1];
            proctime /= 1000000;

            this.sampleValues[this.head] = totalSamples - this.lastValue;
            this.sampleTimes[this.head]  = proctime;
            this.head                    = ++this.head >= this.max ? 0 : this.head;
        }

        this.lastTimer    = process.hrtime();
        this.lastValue    = totalSamples;
        this.totalSamples = totalSamples;
    }

    incVar(name: string, amount: number = 1) {
        this.varList[name] = this.varList[name] || 0;
        this.varList[name] += amount;
        
        return this.varList[name];
    }

    setVar(name: string, amount: number = 1) {
        this.varList[name] = this.varList[name] || 0;
        this.varList[name] = amount;

        return this.varList[name];
    }

    addVar(name: string, title: string, value: number = 0) {
        this.varList[name]  = value;
        this.varNames[name] = title;

        return true;
    }

    /**
     * Raw data to send to the status server
     */
    private getStatusData() {
        let result: any  = {};
        result.avg       = this.getCurrentRate();
        result.total     = this.totalSamples;
        result.remain    = this.remain;
        result.name      = this.name;
        result.host      = os.hostname();
        result.memusage  = process.memoryUsage().heapUsed;
        result.type      = 'status';
        result.slot      = 0;
        result.uuid      = this.uuid;
        result.firstSeen = this.firstSeen;

        if (this.lastTimerStatus) {
            let timeDiff    = process.hrtime(this.lastTimerStatus);
            let proctime    = timeDiff[0] * 1e9 + timeDiff[1];
            proctime /= 1000000;
            result.proctime = proctime;
        }

        this.lastValueStatus = this.lastValueStatus || 0;
        result.numproc       = this.totalSamples - this.lastValueStatus;
        this.lastValueStatus = this.totalSamples;
        this.lastTimerStatus = process.hrtime();

        return result;
    }

    private statusUpdateTimer() {
        if (!this.statusUpdateClient) {
            this.statusUpdateClient = new StatusUpdateClient();
            this.statusUpdateClient.doOpenChangeMessageQueue()
            .then(() => {
                this.statusUpdateTimer();
            });

            return;
        }

        this.onBeforeUpdate(this.varList, this.status);
        this.status = this.getStatusData();
        this.showStatusTextDetailed();
        this.statusUpdateClient.sendStatus(this.status);
        
        if (this.logTimerseriesData) {
            if (!this.timeSeriesData) {
                this.timeSeriesCounter     = 0;
                let timeSeriesDatabaseName = 'rates';
                this.timeSeriesData        = EdgeTimeseriesData.getInstance();

                this.timeSeriesData.doCreateDatabase(timeSeriesDatabaseName)
                .then((db: any) => {
                    if (db && db.doSaveMeasurement) {
                        this.timeSeriesDatabase = db;
                    }
                });
            } else {
                let tagsToIndex: any = {
                    host: this.status.host,
                    slot: this.status.slot
                }

                let dataFields: any = {
                    num_proc    : this.status.total,
                    rate_sec    : this.status.numproc / (this.status.proctime / 1000),
                    remain      : (this.status.remain > 0) ? this.status.remain : 0
                }

                for (let key in this.varList) {
                    if (this.varList[key]) {
                        dataFields[key] = this.varList[key];
                    }
                }

                this.timeSeriesCounter ++;

                if (this.timeSeriesDatabase && this.timeSeriesCounter % 10 === 0) {
                    this.timeSeriesDatabase.doSaveMeasurement(this.name, tagsToIndex, dataFields);
                }
            }
        }
        
        // Restart the timer for the next update
        this.loopTimer *= 2
        this.loopTimer = this.loopTimer > 30000 ? 30000: this.loopTimer;
        setTimeout(() => this.statusUpdateTimer(), this.loopTimer);
    }
    
    /**
     * Display the status text
     */
    private showStatusTextDetailed() {
        try {
            this.status.remain   = this.status.remain || 0;
            this.status.total    = Number(this.status.total) || 0;
            this.status.avg      = Number(this.status.avg) || 0;
            this.status.numproc  = Number(this.status.numproc) || 0;
            this.status.proctime = Number(this.status.proctime) || 0;
            let eta              = ''

            if (this.status.remain > 0) {
                if (this.status.avg === 0) {
                    eta = ''
                } else {
                    let timeRemain = this.status.remain / this.status.avg;
                    if (timeRemain > 7200) {
                        eta = `eta = ${ numeral(timeRemain / (60 * 60)).format('#,###.##') } hrs.`;
                    } else if (timeRemain > 180){
                        eta = `eta = ${ Math.floor(timeRemain / 60) } min.`;
                    } else {
                        eta = `eta = ${ Math.floor(timeRemain) } sec.`;
                    }
                }
            }

            let strStatus = '';
            let strTime   = Ninja.pad(Math.floor(this.status.proctime), 8);
            let strNum    = this.status.numproc > 0 ?
                Ninja.pad(this.status.numproc, 8, chalk.red) :
                Ninja.pad(this.status.numproc, 8);

            strStatus += `${ strNum } in ${ strTime } ms`;
            strStatus += this.status.avg > 0 ?
                ` | ${ Ninja.pad(this.status.avg, 6, chalk.blueBright) }/sec` :
                ` |           `;
            strStatus += this.status.total > 0 ?
                ` | ${ Ninja.pad(this.status.total, 10, chalk.cyan) } Total` :
                ` |                 `;

            if (this.status.remain > 0) {
                let strPercent  = Ninja.pad(Ninja.dumpPercent(this.status.total / (this.status.remain+this.status.total)), 5, chalk.green);
                let strRemain   = Ninja.pad(this.status.remain, 20, chalk.magenta);
                strStatus += ` | ${ strRemain } ${ strPercent } ${ eta }`;
            }

            for (let name in this.varList) {
                this.varNames[name] = this.varNames[name] || name;
                
                if (this.varList[name] !== this.lastVars[name]) {
                    strStatus += ` | ${ Ninja.pad(this.varList[name], 6, chalk.green) } ${ this.varNames[name] }`;
                    this.lastVars[name] = this.varList[name];
                } else {
                    strStatus += ` | ${ Ninja.pad(this.varList[name], 6) } ${ this.varNames[name] }`;
                }
            }
            
            if (this.status.memusage > (1024000 * 200)) {
                let strHeap = ` | ${ Ninja.pad(this.status.memusage / 1024000, 5, chalk.gray, '#,###') } MB`;
                strStatus += strHeap;
            }

            strStatus += ` | ${ chalk.gray(this.name) }`;

            if (this.status.proctime < 5000 && this.status.numproc < 1) {
                return;
            }

            if (!this.hideOutput) {
                console.log(strStatus);
            }

        } catch (e) {
            console.log('Unknown exception during showStatusTextDetailed:', e);
        }
    }

    private getCurrentRate() {
        let total     = 0;
        let count     = 0;
        let totalTime = 0;

        for (let x = 0; x < this.max; x++) {
            if (this.sampleValues[x] === -1) {
                continue;
            }

            total += this.sampleValues[x];
            totalTime += this.sampleTimes[x];
            count ++;
        }

        return totalTime > 0 ? Math.floor(1000 * (total / totalTime)): 0;
    }
}


