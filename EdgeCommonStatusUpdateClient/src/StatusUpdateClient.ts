import * as winston       from 'winston';
import * as amqplib       from 'amqplib';
import * as os            from 'os';
import config             from 'edgecommonconfig';
import EdgeTimeseriesData from 'edgecommontimeseriesdata';

let globalStatusUpdateClientID = 0;

//
//  Global module variables that enable tracing to a file using winston
//  TODO: Check config or environment instead of hard coded
let globalStatusToFile: boolean = false;
let globalStatusTraceFile: any;

//
//  Global flag to use Rabbit MQ for status updates
//  TODO: Check config or environment instead of hard coded
const globalStatusToRabbit: boolean = false;

//
//  Global flag to use a Time Series database such as InfluxDB
//  TODO: Check config or environment instead of hard coded
let globalStatusToTimeSeries: boolean = true;


class StatusUpdateJob {
    client: StatusUpdateClient;
    lastWasZero: any;
    data: any;

    constructor(client: StatusUpdateClient, name: string, options?: any) {

        this.client = client;
        if (options == null) {
            options = {};
        }
        this.lastWasZero = {};

        this.data      = {};
        this.data.name = name;
        for (let varName in options) {
            const value        = options[varName];
            this.data[varName] = value;
        }

        this.data.first_seen = new Date();
        this.data.host       = os.hostname();
        this.data.uuid       = os.hostname() + name.slice(0, 9) + new Date().getTime();
    }

    stop() {

        this.data.type     = "jobend";
        this.data.remain   = -1;
        this.data.duration = new Date().getTime() - this.data.first_seen;
        return this.client.sendStatus(this.data);
    }
}

export default class StatusUpdateClient {
    name: string;
    defaultType: any;
    gid: number;
    openPromise: any;
    lastWasZero: any;
    timeseriesData: any;
    changemqConnection: any;
    messageExchange: any;
    useTimeSeriesData: boolean;

    //
    //  Enable logging of status messages to a file
    static setGlobalStatusToFile(val: boolean) {
        return globalStatusToFile = val;
    }

    constructor(name?: string, defaultType?: any) {
        this.name        = name;
        this.defaultType = defaultType;
        this.gid         = globalStatusUpdateClientID++;
    }

    //
    //  Message queue related API calls
    //  These calls only push information status exchange
    //

    doOpenChangeMessageQueue() {

        if (this.openPromise != null) {
            return this.openPromise;
        }
        return this.openPromise = (async () => {

            if (!globalStatusToRabbit) {
                return false;
            }

            const conn = await amqplib.connect(config.mqHost);

            //
            //  Channel outbound for changes to be logged, persistant MQ

            //
            //  outbound exchange for realtime change notifications
            //  inbound job queue
            this.changemqConnection = await conn.createChannel();
            this.messageExchange    = await this.changemqConnection.assertExchange(config.mqExchangeStatusUpdates, 'fanout', { durable: false });

            return true;
        })();
    }

    startJob(name: string, args: any, slot: any) {

        const job = new StatusUpdateJob(this, name);
        if (args != null) {
            job.data.args = args;
        }
        if (slot != null) {
            job.data.slot = slot;
        }
        job.data.type = "jobstart";
        this.sendStatus(job.data);
        return job;
    }

    logTimeseriesUpdate(data: any) {

        // globalStatusToFile = true
        if ((this.lastWasZero == null)) {
            this.lastWasZero = {};
        }

        if ((data != null) && (data.name != null) && (data.type === "workqueue")) {

            const fieldTags = {
                host: data.host,
                name: data.name
            };

            const dataTags = {
                total_in  : data.total_in,
                total_out : data.total_out,
                total_pend: data.total_pend,
                total_hits: data.total_hits
            };

            if (((data.total_in + data.total_out) === 0) && (this.lastWasZero[data.name] === true)) {
                return true;
            }

            //
            //  Don't send zero values more than once
            if ((data.total_in + data.total_out) === 0) {
                this.lastWasZero[data.name] = true;
            } else {
                this.lastWasZero[data.name] = false;
            }

            config.status("Sending timeSeries", fieldTags, dataTags);
            this.timeseriesData.doSaveMeasurement("workqueue", fieldTags, dataTags);

        } else {

            //
            //  TODO: What do we do with the other types of status update messages?
            //

            config.status("StatusUpdateClient logTimeseriesUpdate Unknown:", data);
        }
        // console.log "NON WORKQUUE:", data

        return true;
    }

    sendStatus(data: any) {

        if ((data.name == null)) {
            data.name = this.name;
        }
        if ((data.type == null)) {
            data.type = this.defaultType;
        }

        if ((data.pid == null)) {
            data.pid = process.pid;
        }

        //
        //  If logging to a file is enabled
        if (globalStatusToFile) {
            if ((globalStatusTraceFile == null)) {
                globalStatusTraceFile = new winston.Logger({
                    transports: [
                        new winston.transports.File({
                            level    : "info",
                            filename : config.getDataPath("statusUpdateClient.json"),
                            json     : true,
                            timestamp: true,
                            depth    : 6,
                            tailable : true,
                            showLevel: false
                        })
                    ]
                });
            }

            globalStatusTraceFile.info("StatusUpdate", { data });
        }

        //
        //  If logging to a Rabbit MQ message bus is enabled
        if (globalStatusToRabbit && (this.changemqConnection != null)) {
            this.changemqConnection.publish(config.mqExchangeStatusUpdates, 'status', Buffer.from(JSON.stringify(data)));
        }

        //
        //  If logging to a time series database (InfluxDB)
        if (globalStatusToTimeSeries) {

            if ((this.timeseriesData == null)) {

                const timeseriesData = EdgeTimeseriesData.getInstance();
                timeseriesData.doCreateDatabase("status_update")
                    .then((db: any) => {
                        if ((db != null) && (db !== false)) {
                            this.timeseriesData = db;
                            return this.logTimeseriesUpdate(data);
                        } else {
                            this.useTimeSeriesData = false;
                            config.status("StatusUpdateClient sendStatus: No time series database.  Can't initialize.");
                            return globalStatusToTimeSeries = false;
                        }
                    });

            } else {

                this.logTimeseriesUpdate(data);
            }
        }

        return true;
    }
}