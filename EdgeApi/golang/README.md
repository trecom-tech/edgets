## Description

This is a simple client that can connect to EdgeApiServer and see the available API calls.


You may want to set up the go lang environment. 
 ( I worked with go version 1.8 )


 This is client side program so it needs to run the server first of all.
 
 And then we use following package on go and should import them.

 * go get github.com/graarh/golang-socketio


 1. We should launch EdgeApiServer

* ...
* cd EdgeApiServer/
* coffee test/test.coffee

 2. Navigate to our golang directoy.

* ...
* cd golang/
* go run test_client.go

 	Then you can see the connect status and Api List after connecting to server.


 * I had edgeapiserver on my local so I supposed that socket server is localhost:8001

