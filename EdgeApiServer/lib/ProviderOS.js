"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const crypto = require("crypto");
const jsonfile = require('jsonfile');
const child_process_1 = require("child_process");
const edgecommonconfig_1 = require("edgecommonconfig");
const edgedatasetmanager_1 = require("edgedatasetmanager");
const ApiProvider_1 = require("./ApiProvider");
const EdgeError_1 = require("./EdgeError");
class ProviderOS extends ApiProvider_1.default {
    constructor(prototype) {
        super(prototype);
    }
    onInit() {
        let pack;
        this.serverDetail = {};
        this.serverDetail.host = os.hostname();
        this.serverDetail.total_mem = os.totalmem();
        let packageFile = edgecommonconfig_1.default.FindFileInPath('package.json', ['./', '../', '../../']);
        if (packageFile != null) {
            pack = jsonfile.readFileSync(packageFile);
        }
        else {
            pack = { version: 1.0, version_date: new Date(), version_text: 'OnDemand (no package.json)' };
        }
        this.serverDetail.version = pack.version;
        this.serverDetail.version_date = pack.version_date;
        this.serverDetail.version_text = pack.version_text;
    }
    getBaseName() {
        return 'os';
    }
    doGetServerInformation() {
        return __awaiter(this, void 0, void 0, function* () {
            this.serverDetail.free_mem = os.freemem();
            this.serverDetail.load_Avg = os.loadavg();
            return this.serverDetail;
        });
    }
    //  Execute a job and push the results back to the caller
    doExecuteJob(user, job_id) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.default.status('ProviderOS.doExecuteJob');
            return new Promise((resolve, reject) => {
                console.log('doExecuteJob:', job_id);
                return this.doInternalGetJobCollection().then(collection => {
                    return collection.doGetItem(job_id, false).then((job) => {
                        edgecommonconfig_1.default.status('Found job:', job);
                        if (job == null) {
                            return false;
                        }
                        let commandLine = `./${job.script}`;
                        if (/.coffee/.test(job.script)) {
                            commandLine = `/usr/local/bin/coffee ${commandLine}`;
                        }
                        //  Job was not found, output an error
                        if (job == null) {
                            user.sendPush('job-output', {
                                job_id,
                                output: `Invalid job ${job}`,
                            });
                            console.log(`Invalid Job: ${job}`);
                            reject(new EdgeError_1.default(`Invalid job: ${job}`, 'PROVIDER_OS'));
                            return;
                        }
                        //  Send initial job output when we start
                        let mid = __dirname.toString().split('/');
                        mid.pop();
                        mid.pop();
                        mid.push(job.folder);
                        let path = mid.join('/');
                        user.sendPush('job-output', {
                            job_id,
                            output: `Executing: ${commandLine} ${job.args} in ${path}\n\n`,
                        });
                        let args = job.args.split(' ');
                        // tslint:disable-next-line:one-variable-per-declaration
                        for (let n = 0, end = args.length, asc = 0 <= end; asc ? n < end : n > end; asc ? n++ : n--) {
                            args[n] = args[n].replace(/^'([^']+)'$/, '$1');
                        }
                        args.splice(0, 0, job.script);
                        try {
                            let fullOutput = '';
                            //  Spawn the job
                            let envCopy = {
                                TERM: 'HTML',
                                HOME: '/Users/innovation',
                                LANG: 'en_US.UTF-8',
                                LOGNAME: 'innovation',
                                PATH: '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
                                SHELL: '/bin/bash',
                                SHLVL: '1',
                                TMPDIR: '/tmp/',
                                USER: 'innovation',
                            };
                            let jobExec = child_process_1.spawn('/usr/local/bin/coffee', args, {
                                cwd: path,
                                env: envCopy,
                            });
                            jobExec.stdout.on('data', data => {
                                if (data != null) {
                                    fullOutput += data.toString();
                                    return user.sendPush('job-output', {
                                        job_id,
                                        output: data.toString(),
                                    });
                                }
                                return false;
                            });
                            jobExec.stderr.on('data', data => {
                                if (data != null) {
                                    fullOutput += data.toString();
                                    return user.sendPush('job-output', {
                                        job_id,
                                        output: data.toString(),
                                        is_err: true,
                                    });
                                }
                                return false;
                            });
                            return jobExec.on('close', code => {
                                job.result = fullOutput;
                                job.pending = false;
                                job.lastRun = new Date();
                                return collection.doUpsertOne(job_id, job).then(() => {
                                    return resolve({ status: 'Job Complete', exit_code: code });
                                });
                            });
                        }
                        catch (e) {
                            edgecommonconfig_1.default.reportError('Spawn issue:', e);
                            user.sendPush('job-output', {
                                job_id,
                                output: `Exception in code:${e.toString()}`,
                            });
                            reject(new EdgeError_1.default(`Exception in job`, 'PROVIDER_OS'));
                        }
                    });
                });
            });
        });
    }
    //  Shortcut to create a job that runs once a day
    doCreateDailyJob(title, workingFolder, scriptName, commandLineArgs) {
        return this.doCreateJob(title, 'hourly', 24, workingFolder, scriptName, commandLineArgs, 'system');
    }
    //  Shortcut to create a job that runs once an hour
    doCreateHourlyJob(title, workingFolder, scriptName, commandLineArgs) {
        return this.doCreateJob(title, 'hourly', 1, workingFolder, scriptName, commandLineArgs, 'system');
    }
    //  Shortcut to create a job that runs once and never again
    doCreateOnetimeJob(title, workingFolder, scriptName, commandLineArgs) {
        return this.doCreateJob(title, 'once', null, workingFolder, scriptName, commandLineArgs, 'system');
    }
    //  Create a job
    doCreateJob(title, freqType, freqRate, workingFolder, scriptName, commandLineArgs, jobOwner, npm = null) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            let c = yield this.doInternalGetJobCollection();
            let idHash = crypto.createHash('sha1');
            idHash.update(title);
            idHash.update(freqType);
            let job_id = idHash.digest('hex');
            if (freqType == null) {
                freqType = 'once';
            }
            if (freqRate == null || typeof freqRate !== 'number') {
                freqRate = 0;
            }
            if (workingFolder == null || typeof workingFolder !== 'string') {
                workingFolder = '';
            }
            if (scriptName == null || typeof scriptName !== 'string') {
                console.log('Can\'t create job, no script name');
                resolve();
            }
            if (commandLineArgs == null || typeof commandLineArgs !== 'string') {
                commandLineArgs = '';
            }
            let jobDoc = {
                id: job_id,
                title,
                freqType,
                freqRate,
                folder: workingFolder,
                script: scriptName,
                args: commandLineArgs,
                owner: jobOwner,
                enabled: true,
                pending: 1,
            };
            if (npm != null) {
                jobDoc.npm = npm;
            }
            let result = yield c.doUpdate(`/job/${job_id}`, jobDoc);
            resolve(result);
        }));
    }
    //  Internal function that connects to the database
    doInternalGetJobCollection() {
        if (this.internalJobPromise) {
            return this.internalJobPromise;
        }
        return this.internalJobPromise = new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            this.db = new edgedatasetmanager_1.default('os');
            yield this.db.doVerifySimpleIndex('job', 'lastRun', null, false);
            yield this.db.doVerifySimpleIndex('job', 'pending', null, false);
            yield this.db.doVerifySimpleIndex('job', 'owner', null, false);
            resolve(this.db);
        }));
    }
}
exports.default = ProviderOS;
;
//# sourceMappingURL=ProviderOS.js.map