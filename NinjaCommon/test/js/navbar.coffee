$ ->

    addTestButton "Simple Navbar", "Open", ()->
        div = addHolder().el
        navBar = new DynamicNav(div)
        navBar.render()

    addTestButton "Simple Navbar with button", "Open", ()->
        navBar = new DynamicNav(addHolder().el)
        navButton = new NavButton "Test","btn btn-success navbar-btn",
            "data-click": "sampleAttribute"
        navBar.addElement(navButton)
        navBar.render()

    addTestButton "NavButton Updated", "Open", ()->
        navBar = new DynamicNav(addHolder().el)
        navButton = new NavButton "Test", "btn btn-success navbar-btn",
            "data-click": "sampleAttribute"
        navBar.addElement(navButton)
        navBar.render()
        setTimeout () ->
            navButton.updateHTML "Updated", "fa fa-save", { "data-test" : "test-attr" }
        , 2000

    addTestButton "Simple Navbar with form", "Open", ()->
        navBar = new DynamicNav(addHolder().el)

        ## populate form first arg is action of form, second arg is alignment left or right
        navForm = new NavForm "#","left" # align second arg can be right also
        navForm.addElement new NavInput("username", null, placeholder:"Username") # default type will text to set radio or checkbox use 3rd argument attributes
        navForm.addElement new NavInput('password', null,
            placeholder: 'Password'
            type: 'password'
        )
        navForm.addElement new NavButton("Login", "navbar-btn btn-success btn")

        ## add populated form to navbar
        navBar.addElement(navForm)
        navBar.render()

    addTestButton "Simple Navbar with form and dropdown", "Open", ()->
        navBar = new DynamicNav(addHolder().el)

        ## populate dropdown
        dd = new NavDropDown("Brian P.","right") # constructor arg is title and alignment left|right
        dd.addItem
            type:"link"
            text:"Action"
            callback: (e) ->
                console.log e
                alert "Action is clicked"
        dd.addItem({type: "divider"}) # by setting type to divider it will add divider
        dd.addItem
            type:'link'
            text:"Sample"
            callback: (e) ->
                console.log e
                alert "sample is clicked"

        ## add populate dropdown to navbar
        navBar.addElement(dd)
        navBar.render()

    addTestButton "Toolbar with icons", "Open", (e)->

            popup = new PopupWindow "Test Window", 500, 400,
                scrollable: true

            navButton = new NavButton("<i class='fa fa-save'></i> Save", "flat-toolbar-btn", {
                "data-click": "sampleAttribute"
            })

            navButtonPrint = new NavButton("<i class='fa fa-print'></i> Print", "flat-toolbar-btn", {
                "data-click": "sampleAttribute"
            })

            navButton2 = new NavButton "Dropdown Test &#x25BC;", "flat-toolbar-btn"
            navButton2.onClick = (e)=>

                menu = new PopupMenu "Test Window", e
                menu.setMultiColumn 4, 200

                list = "Apple Banana Cherry Grapefruit Grape Lemon Lime Melon Orange Peach Pear
                    Pineapple Plum Strawberry Tangerine Watermelon Carrot Black Blue Brown
                    Gray Green Pink Purple Red White Yellow Bull Cow Calf Cat Chicken Duck Goat
                    Goose Horse Lamb Pig Shep Turkey Monkey Bird Ford Chevy Honda Circle".split ' '

                for name in list

                    menu.addItem name, (e, info)=>
                        console.log "Selected e=",e, "info=", info
                    , name


            popup.getBody().doSetViewWithNavbar "TestShowSize", (view, viewNavbar) ->
                viewNavbar.addToolbar [ navButton, navButtonPrint, navButton2 ]

            return 1

    addTestButton "Navbar Docked buttons with hotkey", "Open", ()->
        navBar = new DynamicNav(addHolder().el)
        navButton1 = new NavButton "Hotkey 'CTRL+A'"
        navButton1.addHotkey 'a', ()->
            alert "new hotkey ctrl+a"
        navBar.addElement(navButton1)

        navButton2 = new NavButton "Hotkey 'CTRL+C'"
        navButton2.addHotkey 'c', () ->
            alert "new hotkey ctrl+c"
        navBar.addElement(navButton2)

        navBar.render()

    addTestButton "ViewNavbar", "Open", () ->
        navButton = new NavButton("<i class='fa fa-save'></i> Save", "flat-toolbar-btn", {
            "data-click": "sampleAttribute"
        })

        navButtonPrint = new NavButton("<i class='fa fa-print'></i> Print", "flat-toolbar-btn", {
            "data-click": "sampleAttribute"
        })
        addHolder()
        .setView "NavBar", (view) ->
            view.addToolbar [navButton, navButtonPrint]

        true

    addTestButton "ViewNavbar Deserialize", "Open", () ->
        addHolder()
        .setView "NavBar", (view) ->
            view.deserialize 
                items: [
                    type: "button"
                    icon: "fa fa-save"
                    name: "nav-btn-save"
                    text: "Save"
                    callback: (e) ->
                        console.log "Save button is clicked: ", e
                        return true
                ,
                    type: "button"
                    icon: "fa fa-book"
                    name: "nav-btn-async"
                    text: "Async"
                    callback: (e) ->
                        console.log "Async button is clicked: ", e
                        new Promise (resolve, reject) ->
                            setTimeout () ->
                                resolve console.log("Async action is resolved here.")
                            , 2000
                ,
                    type: "toggle"
                    name: "nav-btn-toggle"
                    text: "Toggle"
                    align: "right"
                    callback: (e) ->
                        console.log "Toggle button is clicked: ", e
                        return true
                ]

            setTimeout () ->
                console.log "ViewNavbar buttonList = ", view.buttonList
                options = view.serialize()
                console.log "ViewNavbar serialized data: ", options
                doPopupView "NavBar", "NavBar Serialized", null, 1000, 600, (viewPopup) =>
                    viewPopup.deserialize options
            , 3000
        true

    go()
