export interface IContactInfo {
    type: string;
    priority: number;
    account: {
        deviceToken?: string;
        phoneNumber?: string;
        email?: string;
    };
}
