import DataFormatterType from './DataFormatterType';
export declare class DataFormatArray extends DataFormatterType {
    name: string;
    align: string;
    clickable: boolean;
    constructor();
    /**
     *
     * funtion to format the currently passed data
     * @param data [Object] data to be formatted
     * @param options [Object] additonal options defined for the datatype
     * @param path [String] path where the value is being edited
     * @return [Object] formatted data
     */
    format(data: any, options: any, path: any): string;
    /**
     * funtion to unformat the currently formatted data
     * @param data [Object] data to be unformatted
     * @param path [String] path where the value is being edited
     */
    unformat(data: any, path: any): any;
    /**
     *
     * @param row
     * @param value
     * @param tooltipWindow
     */
    renderTooltip(row: any, value: any, tooltipWindow: any): boolean;
    /**
     *
     * @param e
     * @param col
     * @param data
     */
    onFocus(e: any, col: any, data: any): any;
}
export default DataFormatArray;
