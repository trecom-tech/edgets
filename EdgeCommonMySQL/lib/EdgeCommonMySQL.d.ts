export declare class EdgeCommonMySQL {
    private configName;
    private creds;
    private pool;
    private logger;
    /**
     * Creates an instance
     * @param configName name of config as defined in the credential
     * @param options options for mysql connection
     */
    constructor(configName: string);
    /**
     * Execute a SQL query
     * @param sql Query string
     * @param callbackOnRow callback function
     */
    doRunQuery(sql: string, callbackOnRow: any): false | undefined;
    /**
     * Create a connection pool
     */
    private createPool;
}
export default EdgeCommonMySQL;
