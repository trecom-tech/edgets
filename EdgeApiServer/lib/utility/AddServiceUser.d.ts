export declare const DEFAULT_SERVICE_USER: {
    apiUser: string;
    apiPassword: string;
};
export declare function addServiceUser(args?: any): Promise<boolean>;
