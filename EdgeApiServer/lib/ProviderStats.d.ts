import { TimeSeriesDatabase } from 'edgecommontimeseriesdata';
import ApiProvider from './ApiProvider';
export default class ProviderStats extends ApiProvider {
    timeseriesData: TimeSeriesDatabase;
    statDatabases: {
        [dbName: string]: TimeSeriesDatabase;
    };
    constructor(prototype: ApiProvider);
    onInit(): boolean;
    doWriteStatisticalData(databaseName: string, categoryName: string, dataTags: any, dataFields: any): Promise<unknown>;
    doGetMessageQueueStats(): Promise<unknown>;
    getBaseName(): string;
    doGetStats(intervalName: string, categoryName: string): void;
    doAddStatsMany(intervalName: string, categoryName: string, keys: string[]): boolean;
    doAddStats(intervalName: string, categoryName: string, counterName: string, count?: number): Promise<boolean>;
}
