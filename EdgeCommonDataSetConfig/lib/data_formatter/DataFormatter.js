"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatText_1 = require("./DataFormatText");
const DataFormatInt_1 = require("./DataFormatInt");
const DataFormatNumber_1 = require("./DataFormatNumber");
const DataFormatCurrency_1 = require("./DataFormatCurrency");
const DataFormatDate_1 = require("./DataFormatDate");
const DataFormatDateTime_1 = require("./DataFormatDateTime");
const DataFormatFloat_1 = require("./DataFormatFloat");
const DataFormatDateAge_1 = require("./DataFormatDateAge");
const DataFormatEnum_1 = require("./DataFormatEnum");
const DataFormatDistance_1 = require("./DataFormatDistance");
const DataFormatBoolean_1 = require("./DataFormatBoolean");
const DataFormatPercent_1 = require("./DataFormatPercent");
const DataFormatTimeAgo_1 = require("./DataFormatTimeAgo");
const DataFormatSimpleObject_1 = require("./DataFormatSimpleObject");
const DataFormatSourceCode_1 = require("./DataFormatSourceCode");
const DataFormatTags_1 = require("./DataFormatTags");
const DataFormatMultiselect_1 = require("./DataFormatMultiselect");
const DataFormatDuration_1 = require("./DataFormatDuration");
const DataFormatLink_1 = require("./DataFormatLink");
const DataFormatImageList_1 = require("./DataFormatImageList");
const DataFormatMemo_1 = require("./DataFormatMemo");
const DataFormatNumberSystem_1 = require("./DataFormatNumberSystem");
const DataFormatArray_1 = require("./DataFormatArray");
const DataFormatPassword_1 = require("./DataFormatPassword");
const DataFormatIcon_1 = require("./DataFormatIcon");
const formatType = {
    DataFormatText: DataFormatText_1.DataFormatText,
    DataFormatInt: DataFormatInt_1.DataFormatInt,
    DataFormatNumber: DataFormatNumber_1.DataFormatNumber,
    DataFormatCurrency: DataFormatCurrency_1.DataFormatCurrency,
    DataFormatDate: DataFormatDate_1.DataFormatDate,
    DataFormatDateTime: DataFormatDateTime_1.DataFormatDateTime,
    DataFormatFloat: DataFormatFloat_1.DataFormatFloat,
    DataFormatDateAge: DataFormatDateAge_1.DataFormatDateAge,
    DataFormatEnum: DataFormatEnum_1.DataFormatEnum,
    DataFormatDistance: DataFormatDistance_1.DataFormatDistance,
    DataFormatBoolean: DataFormatBoolean_1.DataFormatBoolean,
    DataFormatPercent: DataFormatPercent_1.DataFormatPercent,
    DataFormatTimeAgo: DataFormatTimeAgo_1.DataFormatTimeAgo,
    DataFormatSimpleObject: DataFormatSimpleObject_1.DataFormatSimpleObject,
    DataFormatSourceCode: DataFormatSourceCode_1.DataFormatSourceCode,
    DataFormatTags: DataFormatTags_1.DataFormatTags,
    DataFormatMultiselect: DataFormatMultiselect_1.DataFormatMultiselect,
    DataFormatDuration: DataFormatDuration_1.DataFormatDuration,
    DataFormatLink: DataFormatLink_1.DataFormatLink,
    DataFormatImageList: DataFormatImageList_1.DataFormatImageList,
    DataFormatMemo: DataFormatMemo_1.DataFormatMemo,
    DataFormatNumberSystem: DataFormatNumberSystem_1.DataFormatNumberSystem,
    DataFormatArray: DataFormatArray_1.DataFormatArray,
    DataFormatPassword: DataFormatPassword_1.DataFormatPassword,
    DataFormatIcon: DataFormatIcon_1.DataFormatIcon
};
if (typeof window !== 'undefined' && window !== null) {
    // #|
    // #|  Only if loaded in a browser
    // @ts-ignore
    window['textWidth'] = (text, font) => {
        // @ts-ignore
        if (!textWidth.fakeEl) {
            // @ts-ignore
            textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            // @ts-ignore
            textWidth.fakeEl.css('font-size', '14px');
            // @ts-ignore
            textWidth.fakeEl.css('white-space', 'nowrap');
            // @ts-ignore
            textWidth.fakeEl.css('overflow-x', 'auto');
            // @ts-ignore
            textWidth.fakeEl.css('position', 'absolute');
        }
        // @ts-ignore
        textWidth.fakeEl.text(text);
        // @ts-ignore
        return textWidth.fakeEl.width();
    };
}
class DataFormatter {
    // ###
    // ### constructor
    // ###
    constructor() {
        this.formats = {};
        this.common = new DataFormatCommon_1.DataFormatCommon();
        for (const key in formatType) {
            const type = formatType[key];
            this.register((new type));
        }
    }
    getNumber(data) {
        return this.common.getNumber(data);
    }
    getMoment(data, format) {
        return this.common.getMoment(data, format);
    }
    // ###
    // ### function to register the data formatter class
    // ###
    // ### @param [DataFormatterType] formattingClass
    // ### @return null
    // ###
    register(formattingClass) {
        return this.formats[formattingClass.name] = formattingClass;
    }
    // ###
    // ### function to get the data formatter from registered formatter
    // ###
    // ### @param [String] dataTpe data type name of the desired formatter class
    // ### @note fires error if formatter not found
    // ### @return DataFormatterType
    // ###
    getFormatter(dataType) {
        if (!this.formats[dataType]) {
            console.log("Registered types:", this.formats);
            throw new Error(`Invalid type: ${dataType}`);
        }
        return this.formats[dataType];
    }
    // ###
    // ### Format some data based on the type and
    // ### return just the formatted value, not the style or other details.
    // ###
    // ### @param [String] dataType data type name for which formatter is running
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additional options to apply formatting
    // ### @param [String] path the current path at which the formatter is running
    // ### @return [Object] value formatted data using data type formatter
    // ###
    formatData(dataType, data, options, path) {
        let value;
        if ((this.formats[dataType] == null)) {
            console.log("Registered types:", this.formats);
            return `Invalid type [${dataType}]`;
        }
        return value = this.formats[dataType].format(data, options, path);
    }
    // ###
    // ### UnFormat some data based on the type and
    // ### return just the unformatted value, not the style or other details.
    // ###
    // ### @param [String] dataType data type name for which formatter is running
    // ### @param [Object] data data to be formatted
    // ### @param [Object] options additional options to apply formatting
    // ### @param [String] path the current path at which the formatter is running
    // ### @return [Object] value unformatted data using data type formatter
    // ###
    unformatData(dataType, data, options, path) {
        let value;
        if ((this.formats[dataType] == null)) {
            return `Invalid type [${dataType}]`;
        }
        return value = this.formats[dataType].unformat(data, options, path);
    }
}
exports.DataFormatter = DataFormatter;
exports.default = DataFormatter;
//# sourceMappingURL=DataFormatter.js.map