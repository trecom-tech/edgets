"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var edgecommonconfig_1 = require("edgecommonconfig");
var LIMIT = 10;
var RuleProcessor = /** @class */ (function () {
    function RuleProcessor(rule) {
        this.limit = LIMIT;
        this.rule = rule;
        this.test_condition = null;
        this.active = true;
        if (!rule.condition) {
            console.log('Invalid rule, missing condition:', this.rule);
            this.log.error('Invalid rule, missing condition', { rule: this.rule });
            return;
        }
        this.test_condition = new RegExp(this.rule.condition, 'i');
        // If the rule is not active in the database then it's not active here.
        this.active = this.rule.active === false ? false : this.active;
        this.active = this.rule.active === 0 ? false : this.active;
        this.onInit();
    }
    /**
     * Report an error and deactive the rule
     */
    RuleProcessor.prototype.setError = function (txt) {
        edgecommonconfig_1.config.status('Rule error', this.rule, txt);
        console.log("Rule error in " + this.rule.title + ":", txt);
        this.active = false;
    };
    /**
     * Check to see if a change matches any rules
     */
    RuleProcessor.prototype.check = function (job) {
        if (!this.active) {
            return false;
        }
        if (!this.test_condition) {
            return false;
        }
        if (!job) {
            return false;
        }
        if (!job.path) {
            return false;
        }
        if (this.test_condition.test(job.path)) {
            edgecommonconfig_1.config.status('RuleProcessor check passed:', {
                condition: this.test_condition,
                path: job.path,
                dataset: job.dataset
            });
            this.onExecute(job);
            return true;
        }
        return false;
    };
    RuleProcessor.prototype.onExecute = function (job) {
        // put this in the extended class
    };
    RuleProcessor.prototype.onInit = function () {
        // put this in the extended class
    };
    return RuleProcessor;
}());
exports.default = RuleProcessor;
//# sourceMappingURL=RuleProcessor.js.map