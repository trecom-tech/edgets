import { config } from '../src/Config';
import 'mocha';
import { encrypt, decrypt } from '../src/Encrypter';
import { expect }           from 'chai';

const path   = require('path');

const testKey = '0123456789c00350b293319e908f742a';

//
//  Display a simple object
config.dump('Simple object output:', { a: 10, b: 20 });

describe('Credential management', () => {

    it('Should be able to get the default test credentials', () => {

        //
        //  Get the credentials for a test host
        const mqHost = config.getCredentials('mqHost');
        expect(mqHost).to.equal('amqp://edge:Edge000199@dev1.protovate.com:5672');
    });

    it('Should read credential from environment variable first', () => {
        process.env.mqHost = 'amqp://localhost:5672';
        //
        //  Get the credentials for a test host
        const mqHost = config.getCredentials('mqHost');
        expect(mqHost).to.equal('amqp://localhost:5672');
    });

    it('Should be able to parse json from environment variable', () => {
        process.env.mysql = JSON.stringify({
            host: 'localhost',
            username: 'root',
            password: 'pass'
        });

        //
        //  Get the credentials for a test host
        const mysqlCreds = config.getCredentials('mysql');
        expect(mysqlCreds.host).to.equal('localhost');
    });

    it('Should read credential from individual file', () => {
        //  Get the credentials for a test host
        const mongodb = config.getCredentials('mongodbtest');
        expect(mongodb).to.equal('mongodb://localhost:27017/mongo');
    });
});

describe('set Credential test', () =>{
    it('Should be able to get info after set credential', () => {
        config.setCredentials('local', { url: 'http://localhost:3000' });

        const local = config.getCredentials('local');
        expect(local).to.have.property('url', 'http://localhost:3000');
    });
});

describe('getCredentialPath test', () =>{
    it('should return credential path', () => {
        const credentialPath = config.getCredentialPath();
        expect(credentialPath).to.equal('/tmp/');
    });
});

describe('Finding files', () =>

    describe('Finding files in a path list', () => {

        it('Should be able to find the README file', () => {
            const pathList = ['./', '../'];
            const filename = config.FindFileInPath('README.md', pathList);
            expect(filename).to.equal('./README.md');
        });

        it('Should be able to find the test file', () => {
            const pathList = ['./', '../', './test/', './dummy/'];
            const filename = config.FindFileInPath('test.ts', pathList);
            expect(filename).to.equal('./test/test.ts');
        });

        it('Should be Path without filename', () => {
            const pathList = ['./', '../'];
            const pathName = config.FindFileInPath('README.md', pathList, true);
            expect(pathName).to.equal(path.dirname(path.join(__dirname, '../README.md')) + '/');
        });
    })
);

describe('Logging test', () => {
    describe('reportException method', () => {
        it('Should log message and send it to edgecommonexceptionreport', () => {
            config.reportException('testError', new Error('Test Error'));
        });
    });

    describe('reportDatabaseError method', () => {
        it('Should log message and send it to edgecommonexceptionreport', () => {
            config.reportDatabaseError('dbError', 'update', { id: 'test' },  new Error('DB Error'));
        });
    });

    describe('status method', () => {
        it('Should log current status', () => {
            config.status('testStatus');
        });

        it('Should log current status without status name', () => {
            config.status(null);
        });
    });

    describe('info method', () => {
        it('Should log an information', () => {
            config.info('test Information');
        });
    });

    describe('error method', () => {
        it('Should log an error and record', () => {
            config.error('test Information');
        });
    });
});

describe('Encryptig/Decrypting credentiaals test', () => {
    let encryptedText: string;
    it('should encrypt a text', () => {
        encryptedText = encrypt('test', testKey);
    });

    it('should decrypt a text', () => {
        const decrypted = decrypt(encryptedText, testKey);
        expect(decrypted).to.equal('test');
    });
});