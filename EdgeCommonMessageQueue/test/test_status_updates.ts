
import { config } from 'edgecommonconfig';
import EdgeMessageQueue from '../src/EdgeMessageQueue';

//
//  Simple test that connects to the Status Update message queue which is
//  a "fanout" so this will allocate a one-time queue and connect it to the fan.

//

(async function () {

    console.log("Connecting...");
    await EdgeMessageQueue.doOpenSubscription(config.mqExchangeStatusUpdates, (message: any) => {
        console.log("STATUS:", message);
    });

    console.log("Connected");
}.bind(this)).then(() => console.log("Done."));