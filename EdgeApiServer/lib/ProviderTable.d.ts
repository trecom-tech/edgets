import DataSetManager from 'edgedatasetmanager';
import ApiProvider from './ApiProvider';
import ApiSocketUser from './ApiSocketUser';
import * as DataSetConfig from 'edgecommondatasetconfig';
export default class ProviderTable extends ApiProvider {
    dsLookup: DataSetManager;
    private cache;
    constructor(prototype: ApiProvider);
    /**
     * Get invoked when provider is added to gateway
     */
    onInit(): void;
    /**
     * Returns base name. {BaseName}_{MemberName}
     */
    getBaseName(): string;
    /**
     * returns cache key
     * @param tableName - [string] table Name
     */
    internalGetRecordKey(tableName: string): string;
    /**
     * returns cache key for table list
     * @param tableName - [string] table Name
     */
    internalGetListKey(prefix: string): string;
    /**
     * Save `newData` to `path`
     * @param path - [string] Path string to save newData
     * @param newData - [any] JSON object to save
     * @returns difference of new and old data or true when there's no reference.
     */
    internalDoSaveData(table: DataSetConfig.Table): Promise<boolean>;
    /**
     * Update / Create table
     * @param tableName - [string] table name
     * @param jsonData - [any] schema data in json for edgedatasetconfig's table
     */
    doSetTable(user: ApiSocketUser, tableName: string, jsonData: any): Promise<boolean>;
    /**
     * Get table
     * @param tableName - [string] table name
     */
    doGetTable(tableName: string): Promise<any>;
    /**
     * Change column field
     * @param tableName - [string] table name
     * @param columnName - [string] column name
     * @param changeType - [string] column field to change
     * @param newValue - [any] field data
     */
    doChangeTable(user: ApiSocketUser, tableName: string, columnName: string, changeType: string, newValue: any): Promise<boolean>;
    /**
     * Get tables starts with prefix
     * @param prefix - [string] prefix of table to search
     */
    doListTables(prefix?: string): Promise<any>;
}
