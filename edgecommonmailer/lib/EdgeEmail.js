"use strict";
/**
 * EdgeEmail Class
 * Includes mail validation and parsing markdown template
 */
Object.defineProperty(exports, "__esModule", { value: true });
const classes_1 = require("@sendgrid/helpers/classes");
const EdgeCommonMailerError_1 = require("./EdgeCommonMailerError");
const edgecommonconfig_1 = require("edgecommonconfig");
const MarkdownIt = require('markdown-it');
const md = new MarkdownIt();
const path = require('path');
const fs = require('fs');
const Datauri = require('datauri');
const datauri = new Datauri();
/**
 * Email wrapper of sendgrid email v3
 * https://sendgrid.com/docs/API_Reference/api_v3.html
 */
const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
class EdgeEmail extends classes_1.Mail {
    constructor(data) {
        super(data);
        if (data.mdTemplate) {
            this.parseTemplate(data.mdTemplate);
        }
    }
    parseTemplate(template) {
        const tokens = md.parse(template);
        const imageParser = md.renderer.rules['image'];
        const basePath = edgecommonconfig_1.default.getDataPath("images");
        md.renderer.rules['image'] = (tokens, idx, options, env, self) => {
            tokens.map((token) => {
                const srcIndex = token.attrs.map((attr) => attr[0]).indexOf('src');
                if (srcIndex !== -1) {
                    const externalLinkRegEx = /^http[s]*:\/\//i;
                    if (token.attrs[srcIndex][1] || !externalLinkRegEx.test(token.attrs[srcIndex][1])) {
                        const filePath = path.join(basePath, token.attrs[srcIndex][1]);
                        if (fs.existsSync(filePath)) {
                            token.attrs[srcIndex][1] = datauri.encodeSync(filePath);
                        }
                    }
                }
            });
            return imageParser(tokens, idx, options, env, self);
        };
        const result = md.renderer.render(tokens, {});
        this.setContent([{
                type: 'text/plain',
                value: template
            }, {
                type: 'text/html',
                value: result
            }]);
    }
    validate() {
        const jsonBody = this.toJSON();
        if (!jsonBody.personalizations || jsonBody.personalizations.length === 0) {
            throw new EdgeCommonMailerError_1.default('Email Validation: should have at least one recipient group.');
        }
        jsonBody.personalizations.forEach(group => {
            const recipients = group.to;
            if (!recipients || recipients.length === 0) {
                throw new EdgeCommonMailerError_1.default('Email Validation: should have at least one recipient.');
            }
            recipients.forEach((to) => {
                if (!emailRegex.test(to.email)) {
                    throw new EdgeCommonMailerError_1.default(`Email Validation: ${to.email} is not valid.`);
                }
            });
        });
        if (!jsonBody.content || jsonBody.content.length === 0) {
            throw new EdgeCommonMailerError_1.default('Email Validation: should have at least one content body.');
        }
        if (!emailRegex.test(jsonBody.from.email)) {
            throw new EdgeCommonMailerError_1.default(`Email Validation: ${jsonBody.from.email} is not valid.`);
        }
        if (!jsonBody.subject) {
            throw new EdgeCommonMailerError_1.default(`Email Validation: subject is required`);
        }
    }
}
exports.default = EdgeEmail;
//# sourceMappingURL=EdgeEmail.js.map