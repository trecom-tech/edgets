"use strict";
//
// class for age data type
//
// @extends [DataFormatterType]
//
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const moment = require('moment');
const common = new DataFormatCommon_1.DataFormatCommon();
const flatpickr = require('flatpickr');
function __guard__(value, transform) {
    return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}
class DataFormatDateAge extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'age';
        this.width = 135;
        this.input_width = 135;
        this.align = 'right';
    }
    //
    // funtion to open editor with flatpickr
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                type: "text",
                class: "dynamic_edit"
            });
            this.appendEditor();
            this.elEditor.on('keydown', (e) => {
                // if editor is closed then close datepicker
                if (!this.editorShowing) {
                    return this.datePicker.close();
                }
            });
        }
        this.attachEditor(this.elEditor, path);
        this.elEditor.css({
            position: "absolute",
            "z-index": 5001,
            top,
            left,
            width,
            height
        });
        this.elEditor.val(currentValue);
        this.elEditor.show();
        return this.elEditor.focus();
    }
    // #|
    // #|  Age editor picks a date only
    attachEditor(elSourceField, path) {
        super.attachEditor(elSourceField, path);
        // #|
        // #|  Create 3rd party date picker control
        if ((this.datePicker == null)) {
            this.datePicker = flatpickr(elSourceField[0], {
                maxDate: new Date(),
                allowInput: true,
                dateFormat: "Y-m-d",
                parseDate(dateStr) {
                    const mom = common.getMoment(dateStr, 'MM/DD/YYYY HH:mm:ss');
                    if (mom != null) {
                        return mom.toDate();
                    }
                    return null;
                },
                onChange: (dateObj, dateStr) => {
                    if ((dateObj != null) && dateObj.length) {
                        this.saveValue(dateObj);
                        elSourceField.val(this.format(dateObj[0], {}, path));
                        return elSourceField.change();
                    }
                },
                onOpen: (dateObj, dateStr, instance) => {
                    const time = __guard__(common.getMoment(this.unformat(elSourceField.val(), path)), (x) => x.format('MM/DD/YYYY HH:mm:ss'));
                    if (time != null) {
                        return instance.setDate(time);
                    }
                },
                onClose: (dateObj, dateStr, instance) => {
                    if ((dateObj != null) && dateObj.length) {
                        const date = dateObj[0];
                        const time = __guard__(common.getMoment(date), (x) => x.format('MM/DD/YYYY HH:mm:ss'));
                        if (time != null) {
                            instance.setDate(date);
                        }
                    }
                    this.editorShowing = false;
                    if (this.elEditor != null) {
                        return this.elEditor.hide();
                    }
                },
                enableTime: true,
                time_24hr: true
            });
            this.elDataEditor = this.datePicker.calendarContainer;
        }
        // #|
        // #|  Attach any keyboard event in the text editor to open the date picker
        elSourceField.on('keydown', (e) => {
            // if editor is closed then close datepicker
            if (!this.editorShowing) {
                return this.datePicker.close();
            }
        });
        return true;
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        const m = common.getMoment(data, "MM/DD/YYYY");
        if ((m == null)) {
            return "";
        }
        let html = `<span class='fdate'>${m.format("MM/DD/YYYY")}</span>`;
        let age = moment().diff(m);
        age = age / 86400000;
        if (age < 401) {
            age = numeral(age).format("#") + " d";
        }
        else if (age < (365 * 2)) {
            age = numeral(age / 30.5).format("#") + " mn";
        }
        else {
            age = numeral(age / 365).format("#.#") + " yrs";
        }
        html += `<span class='fage'>${age}</span>`;
        return html;
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        const format = "YYYY-MM-DD";
        const m = common.getMoment(data, format);
        if ((m == null)) {
            return "";
        }
        const dateStr = m.format(format);
        return new Date(dateStr);
    }
    getValueInTooltip(value) {
        const format = "YYYY-MM-DD";
        const m = common.getMoment(value, format);
        if (!m) {
            return "";
        }
        return m.format(format);
    }
}
exports.DataFormatDateAge = DataFormatDateAge;
exports.default = DataFormatDateAge;
//# sourceMappingURL=DataFormatDateAge.js.map