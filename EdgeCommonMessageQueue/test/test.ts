import EdgeMessageQueue from '../src/EdgeMessageQueue';
import config           from 'edgecommonconfig';

config.setCredentials('mqHost', process.env.AMQP_HOST || 'amqp://localhost:5672');
//
//  Assign the mq once connected
let amqp: any         = null;
let amqpExchange: any = null;

describe('AMQP Connecting and sending a message', () => {

    describe('Open a message queue', () => {
        it('Should be able to open a message queue', async () => {
            amqp = await EdgeMessageQueue.doOpenMessageQueue('testqueue');
            amqp.should.have.property('queueName', 'testqueue');
        });
    });

    describe('Submit messages to the message queue', () => {

        it('Should handle a string message', async () => {
            const result = await amqp.doSubmitBuffer(JSON.stringify({ test: 'testing' }));
            result.should.equal(true);

            await new Promise((resolve, reject) => {
                amqp.consume(1, (msg: any, result: any) => {
                    console.log('Data received in queue', result);
                    // successfully received message; resolving to make the test success
                    resolve(true);
                });
            });
        });

        it('Should handle JSON data', async () => {
            const result = await amqp.doSubmitBuffer({ age: 40, name: 'Brian' });
            result.should.equal(true);

            // await new Promise(async (resolve, reject) => {
            //     await EdgeMessageQueue.doOpenSubscription('testqueue', (result: any) => {
            //         console.log('Data received in queue', result);
            //         // successfully received message; resolving to make the test success
            //         resolve(true);
            //     });
            // });
        });
    });

    describe('Close a queue', () => {
        it('Should be able to open a message queue', async () => {
            const result = await amqp.doClose();
            result.should.equal(true);
        });
    });

    describe('Open an exchange', () => {
        it('Should be able to open an exchange', async () => {
            amqpExchange = await EdgeMessageQueue.doOpenExchangeForWrite('testqueue', 'testexchange');
            amqpExchange.should.have.property('queueName', 'testqueue');
        });
    });

    describe('Publish message to an exchange', () => {
        it('Should publish data', async () => {
            const result = await amqpExchange.doPublish('routing', { age: 40, name: 'Brian' });
            result.should.equal(true);

            await new Promise((resolve, reject) => {
                amqpExchange.consume(1, (msg: any, result: any) => {
                    console.log('Data received in queue', result);
                    // successfully received message; resolving to make the test success
                    resolve(true);
                });
            });
        });

        it('Should publish data and subscribe', async () => {
            const result = await amqpExchange.doPublish('routing', JSON.stringify({ age: 40, name: 'Brian' }));
            result.should.equal(true);
        });
    });
});
