export declare class DataFormatCommon {
    static getOptions(value: any): any;
    getNumber(data: any): any;
    getMoment(data: any, format?: any): any;
    constructor();
}
export default DataFormatCommon;
