class ViewNavBar extends View

    getDependencyList: ()=>
        ["/vendor/mousetrap.min.js"]

    onShowScreen: ()=>
        GlobalClassTools.addEventManager(this)
        @toolbarHeight = 40
        @toolbar = new DynamicNav(@el)

    getMinHeight: ()=>
        return @toolbarHeight

    getMaxHeight: ()=>
        return @toolbarHeight

    ##|
    ##|  Add a toolbar
    ##|
    addToolbar: (@buttonList) =>

        for button in @buttonList
            @toolbar.addElement button

        @toolbar.render()
        true

    ##|
    ##|  For use in getLayout, create buttons automatically
    ##|
    setEventCallback: (button, eventName)=>
        button.onClick = (e)=>
            button.toggleChecked()
            @emitEvent eventName, []
        true

    serialize: () =>
        items = @buttonList.map (button) ->
            return
                name: button.name
                text: button.value
                type: button.type
                icon: button.strIcon
                classes: button.classes
                isChecked: button.isChecked
                linkedTable: if @disableOnEmpty then null else button.linkedTable
                linkedTableWithDisable: if @disableOnEmpty then button.linkedTable else null
                keyboard: button.key
                onkeyhandler: button.onKeyHandler
                callback: button.callback
        return
            items: items

    deserialize: (options)=>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "object"
                reject "ViewNavBar: invalid data passed"

            if options? and options.items?
                buttonList = []
                for item in options.items
                    if !item.type? then item.type = "button"
                    if item.align?
                        item.classes = "toolbar-btn-#{item.align} #{item.classes or ''}"
                    button = new NavButton item.text, item.classes
                    button.setType item.type
                    if item.type is "button"
                        if item.icon? then button.setIcon item.icon
                        button.isChecked = null
                    else if item.type is "toggle"
                        if item.store_name?
                            button.storeName = item.store_name
                            storedVal = localStorage.getItem button.storeName 
                            if storedVal
                                item.checked = Boolean(Number(storedVal))
                        if !item.checked?
                            button.isChecked = false
                        else 
                            button.isChecked = item.checked
                        if button.isChecked is true
                            button.setIcon "far fa-checked-circle"
                        else
                            button.setIcon "far fa-circle"

                    if item.name?
                        button.name = item.name
                        this[item.name] = button

                        if !@_variableNames? then @_variableNames = []
                        @_variableNames.push item.name

                    # if item.align?
                    #     button.setAlign item.align

                    if typeof item.callback == "string"
                        @setEventCallback button, item.callback

                    else if typeof item.callback == "function"
                        button.setClickCallback item.callback

                    if item.keyboard?
                        button.addHotkey item.keyboard, item.onkeyhandler

                    if item.linkedTable?
                        button.setLinkedTable item.linkedTable, false

                    else if item.linkedTableWithDisable?
                        button.setLinkedTable item.linkedTableWithDisable, true

                    if item.initial_callback is true
                        button.onClick()

                    buttonList.push button

                @addToolbar(buttonList)

            resolve true
