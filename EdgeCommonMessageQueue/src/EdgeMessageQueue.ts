import { config }   from 'edgecommonconfig';
import * as zlib    from 'zlib';
import * as amqplib from 'amqplib';
//
//  Wrap the actual amqp class
//
class EdgeAQMPMessageQueue {
    mqChannel: any;
    messageQueue: any;
    queueName: string;
    drainRequired: boolean;
    messageExchange: any;
    drainWait: any[]        = [];
    defaultPriority: number = 5;

    constructor(mqChannel: any, messageQueue: any, queueName: string) {
        this.mqChannel    = mqChannel;
        this.messageQueue = messageQueue;
        this.queueName    = queueName;

        this.mqChannel.on('drain', () => {
            this.drainRequired = false;
            for (let callback of Array.from(this.drainWait)) {
                // console.log 'Drainwait called', callback
                callback();
            }

            this.drainWait = [];
        });
    }

    consume(prefetchNum: number, callback: any) {

        config.status(`EdgeMessageQueue consume Starting with ${prefetchNum}`);
        this.mqChannel.prefetch(prefetchNum);
        this.mqChannel.consume(this.messageQueue.queue, (msg: any) => {

            try {

                let job;
                if (msg.content[0] === 123) {

                    //
                    //  First letter is { so it's text JSON
                    config.status('EdgeMessageQueue consume message received in JSON', msg.content.toString());
                    job = JSON.parse(msg.content.toString());
                    callback(msg, job);

                } else {

                    //
                    //  Inflate the content
                    //
                    config.status('EdgeMessageQueue consume message received in ZLIB');
                    zlib.inflate(msg.content, (err: Error, str: Buffer) => {

                        if (err != null) {
                            console.log('Error during inflate:', err);

                        } else {

                            job = JSON.parse(str.toString());
                            callback(msg, job);
                        }
                    });
                }

            } catch (e) {

                console.log('Exception:', e);
                console.log('Error in onQueueMessage:', msg.content.toString());
                this.mqChannel.ack(msg);
                return false;
            }
        });
    }


    //
    //  Disconnect / disable the message queue
    async doClose() {
        config.status('EdgeMessageQueue::doClose Start');

        config.status('EdgeMessageQueue::doClose Complete');

        return true;
    }

    //
    //  Internal wait until the write queue is complete if buffer is full
    doWaitForDrain() {

        return new Promise((resolve, reject) => {
            this.drainWait.push(() => {
                resolve(true);
            });
        });
    }

    // publish string to a channel
    doPublish(routingKey: string, str: any) {

        const { exchange } = this.messageExchange;

        return new Promise((resolve, reject) => {

            if (typeof str === 'object') {
                str = JSON.stringify(str);
            }

            if (typeof str === 'string') {

                zlib.deflate(str, (err: Error, buffer: Buffer) => {
                    if (err != null) {
                        config.error('zlib.deflate error:', err);
                    } else {
                        config.status(`EdgeMessageQueue Publish ${buffer.length} bytes (from ${str.length})`);
                        const result = this.mqChannel.publish(exchange, routingKey, buffer);
                        if (!result) {
                            this.doWaitForDrain()
                                .then(() => {
                                    resolve(true);
                                })
                                .catch((err: Error) => {
                                    reject(err);
                                });
                        } else {
                            resolve(true);
                        }
                    }
                });

            } else {

                const result = this.mqChannel.publish(exchange, routingKey, str);
                if (!result) {
                    this.doWaitForDrain()
                        .then(() => {
                            resolve(true);
                        })
                        .catch((err: Error) => {
                            reject(err);
                        });
                } else {
                    resolve(true);
                }
            }
        });
    }

    //
    //  Write either JSON text or a binary buffer to the message queue
    //  returns a promise write the write is complete.
    //
    doSubmitBuffer(str: any, messagePriority: any) {

        return new Promise((resolve, reject) => {

            if (typeof str === 'object') {
                str = JSON.stringify(str);
            }

            if (typeof str === 'string') {

                //
                // Compress data and submit it
                zlib.deflate(str, (err: Error, buffer: Buffer) => {
                    if (err != null) {
                        config.error('zlib.deflate error:', err);
                    } else {
                        config.status(`EdgeMessageQueue [${this.messageQueue.queue}] doSubmitBuffer Sending Compressed ${str.length} => ${buffer.length}`);
                        // console.log 'HERE M=', @messageQueue
                        // result = @messageQueue.sendToQueue @queueName, buffer

                        if ((messagePriority == null)) {
                            messagePriority = this.defaultPriority;
                        }
                        const options = { priority: messagePriority };
                        const result  = this.mqChannel.sendToQueue(this.messageQueue.queue, buffer, options);
                        if (!result) {
                            this.doWaitForDrain()
                                .then(() => {
                                    resolve(true);
                                })
                                .catch((err: Error) => {
                                    reject(err);
                                });
                        } else {
                            resolve(true);
                        }
                    }
                });

            } else {

                const result = this.messageQueue.sendToQueue(this.queueName, str);
                if (!result) {
                    this.doWaitForDrain()
                        .then(() => {
                            resolve(true);
                        })
                        .catch((err: Error) => {
                            reject(err);
                        });
                } else {
                    resolve(true);
                }
            }
        });
    }
}


//
//  Main class that is returned when you require EdgeMessageQueue
//
class EdgeMessageQueueController {
    log: any;
    openPromise: any;
    conn: any;

    constructor() {
        this.log = config.getLogger('MessageQueue');
    }

    doConnect() {

        if (this.openPromise != null) {
            return this.openPromise;
        }
        return this.openPromise = (async () => {

            const server = config.getCredentials('mqHost');

            config.status(`EdgeMessageQueue doConnect ${server}`);

            // tslint:disable-next-line
            this.conn = await amqplib.connect(server);

            config.status('EdgeMessageQueue Connected');

            return true;
        })();
    }

    async doOpenSubscription(queueName: string, callback: any) {


        await this.doConnect();


        //
        // RabbitMQ / AQMP 0.9

        config.status('Creating subscription chan');

        const ch = await this.conn.createChannel();
        const mq = await ch.assertQueue('', { exclusive: true, durable: false });

        config.status('Creating bind chan to', queueName);

        ch.bindQueue(mq.queue, queueName);
        ch.consume(mq.queue, (msg: any) => {

            try {
                let json;
                ch.ack(msg);

                if (msg.content[0] === 123) {

                    //
                    //  First letter is { so it's text JSON
                    config.status('doOpenSubscription consume message received in JSON Subscription:', msg.content.toString());
                    json = JSON.parse(msg.content.toString());
                    if ((json.stamp == null)) {
                        json.stamp = new Date();
                    }
                    callback(json);

                } else {
                    //
                    //  Inflate the content
                    //
                    // config.status 'doOpenSubscription consume message received in ZLIB'
                    zlib.inflate(msg.content, (err: Error, str: Buffer) => {
                        if (err != null) {
                            console.log('Error during inflate:', err);
                        } else {
                            json = JSON.parse(str.toString());
                            if ((json.stamp == null)) {
                                json.stamp = new Date();
                            }
                            callback(json);
                        }
                    });
                }

            } catch (e) {

                console.log('JSON Exception:', e);
                if ((msg != null) && (msg.content != null)) {
                    console.log('JSON Content:', msg.content.length, ' = ', msg.content.toString());
                }

                const log = config.getLogger(`EdgeMessageQueue-${queueName}`);
                log.error('Exception during processing', { msg, e });
            }
        });
    }


    async doOpenMessageQueue(queueName: string, options: any = null) {

        await this.doConnect();

        config.status(`EdgeMessageQueue connecting to message queue ${queueName}`);


        if ((options == null)) {
            options = {};
        }
        options['x-max-priority'] = 10;
        options['durable']        = false;

        const mqChannel    = await this.conn.createChannel();
        const messageQueue = await mqChannel.assertQueue(queueName, options);
        const wrappedQueue = new EdgeAQMPMessageQueue(mqChannel, messageQueue, queueName);

        config.status(`EdgeMessageQueue connected to message queue ${queueName}`);
        return wrappedQueue;
    }

    async doOpenExchangeForWrite(queueName: string, exchangeName: string) {


        config.status(`doOpenExchangeForWrite queueName=${queueName}, exchangeName=${exchangeName}`);

        await this.doConnect();
        const mqChannel       = await this.conn.createChannel();
        const messageExchange = await mqChannel.assertExchange(exchangeName, 'fanout', { durable: false });
        const messageQueue    = await mqChannel.assertQueue(queueName, { durable: false });

        mqChannel.bindQueue(messageQueue.queue, exchangeName, '');
        const wrappedQueue           = new EdgeAQMPMessageQueue(mqChannel, messageQueue, queueName);
        wrappedQueue.messageExchange = messageExchange;

        return wrappedQueue;
    }
}

export const EdgeAMQPController = new EdgeMessageQueueController();
export default EdgeAMQPController;
