$ ->
    ##|
    ##|  This is just for diagnostics,  you don't need to verify the data map is
    ##|  loaded normally.  The data types should be loaded upon startup.
    addTest "Confirm Zipcodes datatype loaded", () ->
        dm = DataMap.getDataMap()
        if !dm? then return false

        zipcodes = dm.types["zipcode"]
        if !zipcodes? then return false
        if !zipcodes.col["code"]? then return false

        true

    ##|
    ##|  Load the zipcodes JSON file.
    ##|  This will insert the zipcodes into the global data map.
    addTest "Loading Zipcodes", () ->

        new Promise (resolve, reject) ->
            ds  = new DataSet "zipcode"
            ds.setAjaxSource "/js/test_data/zipcodes.json", "data", "code"
            ds.doLoadData()
            .then (dsObject)->
                resolve(true)
            .catch (e) ->
                console.log "Error loading zipcode data: ", e
                resolve(false)

    addTestButton "Render table - Simplest Example", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            table.on "click_row", (row, e) =>
                console.log "Click row event: ", row, e
                true

        true

    addTestButton "Set Title Simple case", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.setTitle("Zipcode")
            setTimeout () ->
                view.setTitle "New title 1"
            , 2000
            setTimeout () ->
                view.setTitle "New Title 2"
            , 4000
        true

    addTestButton "Set Title No Data", "Open", ()->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "invalid"
            table.setTitle("Title of Invalid Table")
        true  
    
    addTestButton "Set Title: Grouping Columns", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            table.groupBy("county")
            view.setTitle "Zipcode Table"
        true

    addTestButton "Table Messages - No data", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.startLoading "Zipcode data is being loaded...", () =>
                return new Promise (resolve, reject) ->
                    setTimeout () ->
                        console.log "Idle function in promise"
                        resolve true
                    , 5000
        true

    addTestButton "Table Messages - New data", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.startLoading "Zipcode data is being loaded...", () =>
                return new Promise (resolve, reject) ->
                    setTimeout () ->
                        console.log "New data is added to table in 2 seconds"
                        DataMap.addDataUpdateTable "zipcode", 12345,
                            "code"     : "12345"
                            "city"     : "New City"
                            "state"    : "New State"
                            "county"   : "New County"
                            "area_code": "12345"
                            "lat"      : "12.34567"
                            "lon"      : "-98.76543"
                    , 2000
                    setTimeout () ->
                        console.log "This Promise is now resolved in 5 seconds with idle value"
                        resolve false
                    , 5000
        true

    addTestButton "Table Update All Rows", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            table = view.addTable "zipcode"
            view.setEnableCheckboxes true
            table.forAll (row) =>
                row.city = "NewCity - #{row.city}"
                return row
        true        

    addTestButton "Table Update All Rows - Promises", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            view.addTable "zipcode"
            view.forAll (row) =>
                return new Promise (resolve, reject) =>
                    setTimeout () ->
                        row.city = "NewCity By Promise - #{row.city}"
                        resolve row
                    , 2000

        true

    addTestButton "Table Update All Checked Rows - Promises", "Open", () ->

        addHolder()
        .setView "Table", (view)->
            view.addTable "zipcode"
            view.setEnableCheckboxes true, 5
            setTimeout () ->
                view.forAllChecked true, (row) =>
                    return new Promise (resolve, reject) =>
                        setTimeout () ->
                            row.city = "NewCity Checked - #{row.city}"
                            resolve row
                        , 2000
            , 5000

        true

    addTestButton "Table Update All Visible Rows", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            setTimeout () ->
                view.setFilter "state", "comparator", "=~"
                view.setFilter "state", "value", "NH"
            , 2000

            setTimeout () ->
                view.forAllVisible false, (row) =>
                    return new Promise (resolve, reject) =>
                        setTimeout () ->
                            row.city = "New City Visible - #{row.city}"
                            resolve row
                        , 1000
            , 4000

            setTimeout () ->
                view.setFilter "state", "value", ""
            , 8000
        true

    addTestButton "Table Update All Fitlered Rows", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            setTimeout () ->
                view.setFilter "state", "comparator", "=~"
                view.setFilter "state", "value", "NH"
            , 2000

            setTimeout () ->
                view.forAllFiltered false, (row) =>
                    return new Promise (resolve, reject) =>
                        setTimeout () ->
                            row.city = "New City Visible - #{row.city}"
                            resolve row
                        , 1000
            , 2000

            setTimeout () ->
                view.setFilter "state", "value", ""
            , 6000
        true

    addTestButton "Table Custom Messages - No data", "Open", () ->

        addHolder()
        .setView "Table", (view)->        
            table = view.addTable "zipcode"
            view.setNoDataMessage "<i class='fa fa-low-vision'></i>&nbsp<span style='color:red'> Ooops! Nothing to show...</span>"
            setTimeout () ->
                DataMap.removeTableData "zipcode"
            , 3000
            setTimeout () ->
                DataMap.addDataUpdateTable "zipcode", "99999", 
                    newcol1: "New Col value1"
                    newcol2: "New Col value2"
                    newcol3: "New Col vallue3"
            , 5000                        
        true

    addTestButton "Table - Remove Rows", "Open", () ->

        addHolder()
        .setView "Table", (view)->        
            table = view.addTable "zipcode"
            setTimeout () ->
                view.removeRow "00501"
            , 1000
            setTimeout () ->
                view.removeRow "00544"
            , 2000
            setTimeout () ->
                view.removeRow "01001"
            , 3000
        true


    addTestButton "Table Set Focus by Value", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            
            setTimeout () ->
                view.setFilter "code", "comparator", ">"
                view.setFilter "code", "value", "1040"
            , 1000

            setTimeout () ->
                view.setFocusByValue "county", "WORCESTER"
            , 3000
            
            setTimeout () ->
                view.setFocusByValue "code", "01111"
            , 5000

            setTimeout () ->
                view.setFocusByValue "state", "MA"
            , 7000

            setTimeout () ->
                view.setFocusByValue "state", "RI"
            , 9000            
        true

    addTestButton "Table Set Row Class", "Open", () ->
        $("body").append '''
            <style type="text/css">
            .tableview .tableRow .cell.red {
                color  : red;
            }
            </style>
        '''        
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            view.addFlag 
                name: "Flag 1"
                icon: "fa fa-book"
            view.addFlag
                name: "Flag 2"
                icon: "fa fa-ban"
            view.setEnableFlags(true, 0)
            view.setEnableCheckboxes(true, 5)

            setTimeout () ->
                view.setRowClass "red", (row) ->
                    if row['row_flagged']?
                        return true
                    return false
            , 2000
            setTimeout () ->
                view.setRowClass "bg-info", (row) ->
                    true 
            , 4000

    addTestButton "Table Highlight Rows", "Open", () ->
        addHolder()
        .setView "Table", (view) ->
            table = view.addTable "zipcode"
            view.addFlag 
                name: "Flag 1"
                icon: "fa fa-book"
            view.addFlag
                name: "Flag 2"
                icon: "fa fa-ban"
            view.setEnableFlags(true, 0)
            view.setEnableCheckboxes(true, 5)

            table.addActionColumn
                name: "Run"
                width: 50
                callback: (row)=>
                    console.log "run action:", row
            view.highlightRows null, (row) ->
                return row.id == "00501"

    go()
