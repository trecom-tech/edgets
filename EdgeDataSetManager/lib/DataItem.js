"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A class that models a single record
 */
const edgecommonconfig_1 = require("edgecommonconfig");
const mongodb_1 = require("mongodb");
class DataItem {
    /**
     * Initialize the Item requires an item id and store id
     * @param path
     * @param db
     * @param data
     */
    constructor(path, db, data) {
        this.path = path;
        this.db = db;
        this.data = data;
        this.basePath = null;
        this.base = null;
        if (this.data == null) {
            this.data = { id: this.id };
        }
        // Set the base pointer based on the sub path if needed
        this.base = this.data;
        this.basePath = this.path.subPath.join('.');
        let counter = 0;
        while (this.path.subPath != null && this.path.subPath.length > counter) {
            let targetPath = this.path.subPath[counter];
            if (this.base[targetPath] != null) {
                this.base = this.base[targetPath];
            }
            else {
                this.base[targetPath] = {};
                this.base = this.base[targetPath];
            }
            counter++;
        }
    }
    /**
     * Internal virtual function that saves items or updates
     */
    doSaveUpdates() {
        return __awaiter(this, void 0, void 0, function* () {
            // Item has been saved, update the last modified and sub path
            const setCondition = {};
            if (this.basePath.length > 0) {
                setCondition[this.basePath] = this.base;
                setCondition._lastModified = new Date();
                try {
                    let result = yield this.db.doUpdateOne({ _id: this.data._id }, { '$set': setCondition });
                    return true;
                }
                catch (e) {
                    console.log("UpdateOne Error:", e);
                    throw e;
                }
            }
            else {
                if (typeof this.data._id === "string" && this.data._id.length === 24) {
                    this.data._id = mongodb_1.ObjectID.createFromHexString(this.data._id);
                }
                this.data._lastModified = new Date();
                try {
                    yield this.db.doUpdateOne({ _id: this.data._id }, { '$set': this.data });
                    return true;
                }
                catch (e) {
                    // config.reportError "UpdateOne Saving Error:", e
                    edgecommonconfig_1.config.inspect(setCondition);
                    console.log("UpdateOne Saving Error", e.toString());
                    throw e;
                }
            }
        });
    }
}
exports.DataItem = DataItem;
exports.default = DataItem;
//# sourceMappingURL=DataItem.js.map