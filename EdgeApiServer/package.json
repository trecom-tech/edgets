{
  "name": "edgeapiserver",
  "version": "1.116.0",
  "description": "The core API Server for the Edge Network",
  "engines": {},
  "main": "lib/EdgeApiServer.js",
  "devDependencies": {
    "@types/chai": "^4.2.4",
    "@types/inquirer": "^6.5.0",
    "@types/lodash": "^4.14.144",
    "@types/lru-cache": "^5.1.0",
    "@types/mocha": "^5.2.7",
    "@types/mongodb": "^3.3.8",
    "@types/node": "^10.17.4",
    "@types/redis": "^2.8.14",
    "@types/should": "^13.0.0",
    "@types/socket.io": "^2.1.4",
    "@types/socket.io-client": "^1.4.32",
    "@types/uuid": "^3.4.6",
    "@types/yargs": "^13.0.3",
    "chai": "^4.1.2",
    "inquirer": "^6.5.2",
    "mocha": "^6.2.1",
    "mocha-clean": "^1.0.0",
    "nyc": "^14.1.1",
    "prettier": "^1.18.2",
    "rimraf": "^2.7.1",
    "self-signed": "^1.3.1",
    "should": "^11.1.2",
    "swagger": "^0.7.5",
    "swagger-editor": "^3.7.0",
    "ts-jest": "~23.10.5",
    "ts-node": "^8.4.1",
    "tslint": "^5.20.1",
    "tslint-config-prettier": "^1.18.0",
    "tslint-config-standard": "^8.0.1",
    "tsutils": "~3.7.0",
    "typedoc": "^0.15.0",
    "typescript": "^3.7.2"
  },
  "scripts": {
    "clean": "rimraf coverage build tmp lib",
    "lint": "tslint --project tsconfig.json -t codeFrame 'src/**/*.ts' 'test/**/*.ts'",
    "build": "rm -rf lib && tsc -p tsconfig.release.json",
    "test-local": "NODE_ENV=test DB_HOST=mongodb://localhost/admin INFLUX_HOST=localhost REDIS_HOST=redis://localhost:6379 MQHOST=amqp://guest:guest@localhost:5672 API_SERVER=http://127.0.0.1:8001 EDGE_DEBUG=trace nyc mocha --timeout 60000 -r ts-node/register test/**/*.spec.ts --exit",
    "test": "NODE_ENV=test nyc mocha --timeout 90000 -r ts-node/register test/**/*.spec.ts --exit",
    "prettier": "prettier src/**/*.ts --write",
    "start": "ts-node server.ts",
    "doc": "typedoc --mode modules --out docs/class --tsconfig tdconfig.json",
    "swagger-edit": "swagger_swagger_fileName=docs/api/swagger/swagger.yaml swagger project edit",
    "test-server": "NODE_ENV=test DB_HOST=mongodb://localhost/admin INFLUX_HOST=localhost REDIS_HOST=redis://localhost:6379 MQHOST=amqp://guest:guest@localhost:5672 API_SERVER=http://127.0.0.1:8001 ts-node ./test/helper/server.ts",
    "addDefaultServiceUser": "ts-node scripts/AddServiceUser.ts"
  },
  "repository": {
    "type": "git",
    "url": "git@gitlab.protovate.com:EdgeTS/EdgeApiServer"
  },
  "author": "Roy D.",
  "license": "UNLICENSED",
  "dependencies": {
    "@types/koa": "^2.0.51",
    "NinjaCommon": "git+ssh://git@gitlab.protovate.com:EdgeTS/NinjaCommon",
    "coffeescript": "^1.12.3",
    "edgeapi": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeApi",
    "edgebulktablestorage": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeBulkTableStorage",
    "edgecommonconfig": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonConfig",
    "edgecommondatasetconfig": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonDataSetConfig",
    "edgecommonmessagequeue": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonMessageQueue",
    "edgecommonratecapture": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonRateCapture",
    "edgecommonrequest": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonRequest",
    "edgecommonstatusupdateclient": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonStatusUpdateClient",
    "edgecommontablebase": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonTableBase",
    "edgecommontimeseriesdata": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeCommonTimeseriesData",
    "edgedatasetmanager": "git+ssh://git@gitlab.protovate.com:EdgeTS/EdgeDataSetManager",
    "formidable": "*",
    "glob-all": "*",
    "jsonfile": "^5.0.0",
    "jsontoxml": "^1.0.1",
    "koa": "^2.11.0",
    "koa-compress": "^3.0.0",
    "koa-session": "^5.12.3",
    "less": "^3.10.3",
    "lodash": "^4.17.15",
    "lru-cache": "^5.1.1",
    "mime": "^2.4.4",
    "moment": "^2.24.0",
    "moment-timezone": "^0.5.27",
    "nib": "*",
    "ninjadebug": "git+ssh://git@gitlab.protovate.com:EdgeTS/ninjadebug",
    "project-version": "^1.0.0",
    "pug": "^2.0.4",
    "redis": "^2.8.0",
    "redislock": "^1.3.0",
    "socket.io": "^2.3.0",
    "socket.io-client": "^2.3.0",
    "stylus": "^0.54.7",
    "swagger2": "^1.0.5",
    "swagger2-koa": "^1.0.4",
    "uuid": "^3.3.3",
    "xml2js": "^0.4.22",
    "yargs": "^13.3.0"
  },
  "nyc": {
    "check-coverage": true,
    "per-file": true,
    "lines": 0,
    "statements": 0,
    "functions": 0,
    "branches": 0,
    "reporter": [
      "lcov",
      "text-summary"
    ],
    "extension": [
      ".ts"
    ],
    "include": [
      "src/**/*.ts"
    ],
    "require": [
      "ts-node/register"
    ],
    "cache": true,
    "all": true,
    "report-dir": "./coverage"
  }
}
