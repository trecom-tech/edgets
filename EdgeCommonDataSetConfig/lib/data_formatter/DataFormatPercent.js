"use strict";
//
// class for percent data type
//
// @extends [DataFormatterType]
//
Object.defineProperty(exports, "__esModule", { value: true });
const DataFormatCommon_1 = require("./DataFormatCommon");
const DataFormatterType_1 = require("./DataFormatterType");
const numeral = require('numeral');
const common = new DataFormatCommon_1.DataFormatCommon();
class DataFormatPercent extends DataFormatterType_1.DataFormatterType {
    constructor(...args) {
        super();
        this.name = 'percent';
        this.width = 80;
        this.input_width = 80;
        this.align = 'right';
    }
    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data, options, path) {
        return numeral(common.getNumber(data)).format('#,###.[##] %');
    }
    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data, path) {
        const num = common.getNumber(data);
        return num;
    }
}
exports.DataFormatPercent = DataFormatPercent;
exports.default = DataFormatPercent;
//# sourceMappingURL=DataFormatPercent.js.map