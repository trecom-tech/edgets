import { DataSetConfig } from '../src/DataSetConfig';
import DataSetManager    from '../src/DataSetManager';
import * as fs           from 'fs';
import * as path         from 'path';
import { expect }        from 'chai';

// let xmlText = `<?xml version="1.0"?> \
// <xml xmlns="http://www.w3.org/1999/xhtml"> \
// <block type="variables_set"> \
// <field name="VAR">blockly</field> \
// <value name="VALUE"> \
// <block type="text"> \
// <field name="TEXT">Hello Node.js!</field> \
// </block> \
// </value> \
// </block> \
// </xml>\
// `;

// xmlText = `<xml xmlns="http://www.w3.org/1999/xhtml"> \
// <custom scrollx="1774" scrolly="1399" ignore="Bedroom 1,Bedroom 2,Bedroom 3,Bedroom 4,Bedroom 5,Bedroom 6,Master Bed,Master Bath,Internet,Boat,Allow,Fax, URL,1st,2nd,3rd,4th,5th,Color,Additional,Master Bed,Other ,Virtual,Office,Internet,Kitchen,Laundry,Bonus,Co List,Co Owner,Co Selling,Co-List,Co-Sell,Permit"/> \
// <variables/> \
// </xml>\
// `;

const xmlText = fs.readFileSync(path.join(__dirname, '/data/example_xml1.xml')).toString();

describe("DataSetConfig", () => {
    let ds: DataSetManager;
    let datasetConfig: DataSetConfig;

    before(async () => {
        ds = new DataSetManager('rets_raw');
        datasetConfig = await ds.doInternalGetSettings('test');
    });

    // tslint:disable-next-line:no-emptyxmlText
    it("doSetTransformRule", async () => {
        const result = await datasetConfig.doSetTransformRule('test', xmlText);
        console.log(result);
    });

    it("doGetTransformRule", async () => {
        const result = await datasetConfig.doGetTransformRule('test');
        expect(result).to.equal(xmlText);
    });

});