import './helper/setup';
import EdgeApi       from 'edgeapi';
import { expect }    from 'chai';
import ApiSocketUser from '../src/ApiSocketUser';

describe('ApiSocketUser', () => {

    let api: EdgeApi;
    let apiSocketUser: ApiSocketUser;

    const testUser = {
        id: 'roleTest',
        username: 'roleTest',
        password: '7e0813c46bc1b9fb3616775ae9dbb7b8', // hash for 'pass'
        name: 'Steve Tester',
        first_modified: new Date(),
        last_modified: new Date(),
        active: true,
        email: 'admin@protovate.com',
        roles: {
            test: [
                'serviceUser',
                'Admin',
                'commsAdmin',
                'CompanyAdmin',
                'PhaseApproval',
            ],
            global: [
                'reporter'
            ]
        },
        attrib: {
            lunch: 'Pizza',
        },
    };

    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
        apiSocketUser = new ApiSocketUser(1, api.socket);

        await api.data_doUpdatePath('master', `/user/${testUser.id}`, testUser);
    });

    describe('hasRole method', () => {
        it('should return false', () => {
            const hasRole = apiSocketUser.hasRole('reporter');
            expect(hasRole).to.equal(false);
        });


        it('should test hasRole', async () => {
            apiSocketUser.rec = testUser;
            expect(apiSocketUser.hasRole('Admin', 'test')).to.equal(true);
            expect(apiSocketUser.hasRole('NonExistingRole', 'test')).to.equal(false);
        });
    });

});