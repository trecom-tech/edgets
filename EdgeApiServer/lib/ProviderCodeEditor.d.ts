import DataSetManager from 'edgedatasetmanager';
import ApiProvider from './ApiProvider';
export default class ProviderCodeEditor extends ApiProvider {
    dataStore: DataSetManager;
    modlist: any;
    constructor(prototype: ApiProvider);
    onInit(): boolean;
    getBaseName(): string;
    doRunCode(user: any, category: any, name: any, userObject: any, params: any, executeOptions: any): Promise<any>;
    internalGetKey(category: any, name: any, branch: any): any;
    doListAvailableModules(user: any, condition?: any): Promise<any[]>;
    doSaveModuleNotes(user: any, category: any, name: any, newNotes: any): Promise<boolean>;
    doSaveModuleLocal(user: any, category: any, name: any, code: any): Promise<any>;
    doTestCaseAdd(category: any, name: any, options: any, id: any): Promise<any>;
    doTestCaseRemove(category: any, name: any, id: any): Promise<any>;
    doGetModuleLocal(user: any, category: any, name: any): Promise<any>;
    doInternalGetModuleLocal(category: any, name: any): Promise<any>;
}
