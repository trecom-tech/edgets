"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jsonfile = require("jsonfile");
var edgecommonconfig_1 = require("edgecommonconfig");
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var reDateFormat1 = new RegExp('([0-9]{1,2}).([0-9]{1,2}).([0-9][0-9][0-9][0-9])');
var EdgeGoogleCell = /** @class */ (function () {
    function EdgeGoogleCell(sheet, row, col, cell) {
        this.sheet = sheet;
        this.row = row;
        this.col = col;
        this.cell = cell;
        this.update = null;
    }
    EdgeGoogleCell.prototype.value = function () {
        if (!this.cell.data) {
            return null;
        }
        if (!this.cell.data.userEnteredValue) {
            return null;
        }
        if (this.cell.data.userEnteredValue.stringValue) {
            return this.cell.data.userEnteredValue.stringValue;
        }
        if (this.cell.data.userEnteredValue.numberValue) {
            return this.cell.data.userEnteredValue.numberValue;
        }
        if (this.cell.data.effectiveValue) {
            if (this.cell.data.effectiveValue.numberValue) {
                return this.cell.data.effectiveValue.numberValue;
            }
            if (this.cell.data.effectiveValue.stringValue) {
                return this.cell.data.effectiveValue.stringValue;
            }
            if (this.cell.data.effectiveValue.errorValue) {
                return 'SpreadsheetError';
            }
            return this.cell.data.effectiveValue;
        }
        return null;
    };
    EdgeGoogleCell.prototype.isFormula = function () {
        if (this.cell.data &&
            this.cell.data.userEnteredValue &&
            this.cell.data.userEnteredValue.formulaValue) {
            return true;
        }
        return false;
    };
    /**
     * See https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets#RowData
     */
    EdgeGoogleCell.prototype.setValue = function (newValue) {
        var oldValue = this.value();
        if (oldValue === newValue) {
            return true;
        }
        this.initUpdate('userEnteredValue');
        this.cell.data = this.cell.data || {};
        if (typeof newValue === 'string') {
            this.update.rows[0].values[0].userEnteredValue = { stringValue: newValue };
            this.cell.data.userEnteredValue = { stringValue: newValue };
        }
        else if (typeof newValue === 'number') {
            this.update.rows[0].values[0].userEnteredValue = { numberValue: newValue };
            this.cell.data.userEnteredValue = { numberValue: newValue };
        }
        else if (typeof newValue === 'boolean') {
            var newValue2 = 'Yes';
            if (!newValue) {
                newValue2 = 'No';
            }
            this.update.rows[0].values[0].userEnteredValue = { stringValue: newValue2 };
            this.cell.data.userEnteredValue = { stringValue: newValue2 };
        }
        else {
            console.log("Warning, don't know how to setValue for type ", typeof newValue);
        }
    };
    EdgeGoogleCell.prototype.setFormula = function (newFormula) {
        this.cell.data = this.cell.data || {};
        this.cell.data.userEnteredValue = this.cell.data['userEnteredValue'] || {};
        if (this.cell.data && this.cell.data.userEnteredValue.formulaValue === newFormula) {
            return true;
        }
        this.initUpdate('userEnteredValue');
        this.update.rows[0].values[0].userEnteredValue = { formulaValue: newFormula };
        this.cell.data.userEnteredValue = { formulaValue: newFormula };
        return true;
    };
    EdgeGoogleCell.prototype.setHyperlink = function (newLink, newValue) {
        var formula = "=HYPERLINK('" + newLink + "','" + newValue + "')";
        this.setFormula(formula);
        this.cell.data.userEnteredValue.stringValue = newValue;
        return true;
    };
    EdgeGoogleCell.prototype.setBackgroundColor = function (newRed, newGreen, newBlue) {
        this.initUpdate('userEnteredFormat.backgroundColor');
        this.update.rows[0].values[0].userEnteredFormat =
            this.update.rows[0].values[0].userEnteredFormat || {};
        this.update.rows[0].values[0].userEnteredFormat.backgroundColor = {
            red: newRed,
            green: newGreen,
            blue: newBlue
        };
        return true;
    };
    EdgeGoogleCell.prototype.initUpdate = function (name) {
        if (!this.update) {
            this.update = {
                start: {
                    sheetId: this.sheet.properties.sheetId,
                    rowIndex: this.row,
                    columnIndex: this.col
                },
                rows: [{ values: [{}] }],
                fields: ''
            };
        }
        for (var _i = 0, _a = this.update.fields.split(','); _i < _a.length; _i++) {
            var n = _a[_i];
            if (n === name) {
                return true;
            }
        }
        if (this.update.fields.length > 0) {
            this.update.fields += ',';
        }
        this.update.fields += name;
        return true;
    };
    return EdgeGoogleCell;
}());
var EdgeGoogleSheet = /** @class */ (function () {
    function EdgeGoogleSheet(googleExport, spreadsheetId, docProperties, sheet) {
        this.googleExport = googleExport;
        this.spreadsheetId = spreadsheetId;
        this.docProperties = docProperties;
        this.properties = sheet.properties;
        // Data contains rowData, rowMetadata, columnMetadata
        this.data = sheet.data[0];
        this.cells = {};
        // Maximum values only, not that they all have data
        this.maxRowCount = sheet.properties.gridProperties.rowCount;
        this.maxColCount = sheet.properties.gridProperties.columnCount;
        this.rowCount = this.data.rowData.length;
    }
    EdgeGoogleSheet.prototype.doSaveChanges = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // See what changes are pending in each cell
            var requests = [];
            for (var rowNum in _this.cells) {
                var colList = _this.cells[rowNum];
                for (var colNum in colList) {
                    var cell = colList[colNum];
                    if (cell.update) {
                        requests.push({ updateCells: cell.update });
                    }
                }
            }
            if (requests.length > 0) {
                edgecommonconfig_1.config.status("EdgeGoogleSheet doSave " + requests.length + " changes");
                var batchUpdateRequest_1 = { requests: requests };
                var log_1 = edgecommonconfig_1.config.getLogger('sheets');
                log_1.info('Sending batch update', {
                    spreadsheetId: _this.spreadsheetId,
                    resource: batchUpdateRequest_1
                });
                var sheets = google.sheets('v4');
                sheets.spreadsheets.batchUpdate({
                    auth: _this.googleExport.getAuth(),
                    spreadsheetId: _this.spreadsheetId,
                    resource: batchUpdateRequest_1
                }, function (err, result) {
                    if (err) {
                        log_1.error('Sending batch update', {
                            spreadsheetId: _this.spreadsheetId,
                            resource: batchUpdateRequest_1,
                            err: err,
                            result: result
                        });
                        console.log('error in batch', {
                            spreadsheetId: _this.spreadsheetId,
                            resource: batchUpdateRequest_1,
                            err: err,
                            result: result
                        });
                        resolve(false);
                    }
                    else {
                        // Remove pending update
                        for (var rowNum in _this.cells) {
                            var colList = _this.cells[rowNum];
                            for (var colNum in colList) {
                                var cell = colList[colNum];
                                delete (cell.update);
                            }
                        }
                        resolve(result.replies);
                    }
                });
            }
            else {
                resolve([{}]);
            }
        });
    };
    EdgeGoogleSheet.prototype.get = function (rowNum, colNum) {
        if (this.cells[rowNum] && this.cells[rowNum][colNum]) {
            return this.cells[rowNum][colNum];
        }
        var result = {
            data: null
        };
        if (this.data.rowData[rowNum] && this.data.rowData[rowNum].values) {
            if (this.data.rowData[rowNum].values[colNum]) {
                result.data = this.data.rowData[rowNum].values[colNum];
            }
        }
        result.rowMeta = this.data.rowMetadata[rowNum] || null;
        result.colMeta = this.data.columnMetadata[colNum] || null;
        /*
        Example of what the result might look like at this point:
        { data:
            { userEnteredValue: { stringValue: 'Something Here that is text' },
              effectiveValue: { stringValue: 'Something Here that is text' },
              formattedValue: 'Something Here that is text',
              effectiveFormat:
                { backgroundColor: [Object],
                padding: [Object],
                horizontalAlignment: 'LEFT',
                verticalAlignment: 'BOTTOM',
                wrapStrategy: 'OVERFLOW_CELL',
                textFormat: [Object],
                hyperlinkDisplayType: 'PLAIN_TEXT' } },
        rowMeta: { pixelSize: 21 },
        colMeta: { pixelSize: 100 } }
        */
        this.cells[rowNum] = this.cells[rowNum] || {};
        this.cells[rowNum][colNum] = new EdgeGoogleCell(this, rowNum, colNum, result);
        return this.cells[rowNum][colNum];
    };
    return EdgeGoogleSheet;
}());
var EdgeImportExportGoogleDoc = /** @class */ (function () {
    function EdgeImportExportGoogleDoc(credentialKey, sheetId, cacheFile, maxCacheAge) {
        this.sheetId = sheetId;
        this.cacheFile = cacheFile;
        this.maxCacheAge = maxCacheAge;
        this.credentials = edgecommonconfig_1.config.getCredentials(credentialKey);
        if (!this.credentials) {
            edgecommonconfig_1.config.status("EdgeImportExportGoogleDoc Warning: Missing credentials for key '" + credentialKey);
            return;
        }
        if (!this.credentials.oauth2) {
            edgecommonconfig_1.config.status("EdgeImportExportGoogleDoc Warning: Missing credentials oauth2 for key '" + credentialKey + "'");
            return;
        }
        if (!this.credentials.oauth2.client_id) {
            edgecommonconfig_1.config.status("EdgeImportExportGoogleDoc Warning: Missing credentials oauth2 client_id for key '" + credentialKey + "'");
        }
    }
    /**
     * Batch updates document
     * https://developers.google.com/sheets/api/guides/batchupdate
     */
    EdgeImportExportGoogleDoc.prototype.doReadSheets = function (ranges) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Try the cache first
            if (_this.cacheFile) {
                try {
                    edgecommonconfig_1.config.status("doReadSheets, Reading cache file " + _this.cacheFile);
                    var rawData = jsonfile.readFileSync(_this.cacheFile);
                    if (rawData && rawData.cacheTime) {
                        var today = new Date();
                        _this.maxCacheAge = _this.maxCacheAge || 300 * 1000;
                        if (today.getTime() - rawData.cacheTime < _this.maxCacheAge) {
                            var sheets_1 = {};
                            for (var _i = 0, _a = rawData.data.sheets; _i < _a.length; _i++) {
                                var sheet = _a[_i];
                                var s = new EdgeGoogleSheet(_this, rawData.data.spreadsheetId, rawData.data.properties, sheet);
                                sheets_1[sheet.properties.title] = s;
                            }
                            resolve(sheets_1);
                            return;
                        }
                    }
                }
                catch (e) {
                    edgecommonconfig_1.config.status('doReadSheets, Cache file error', e);
                }
            }
            var options = {
                auth: _this.getAuth(),
                spreadsheetId: _this.sheetId,
                includeGridData: true
            };
            if (ranges) {
                options.ranges = ranges;
            }
            edgecommonconfig_1.config.status('doReadSheets, Reading sheets v4 with options:', options);
            var sheets = google.sheets('v4');
            sheets.spreadsheets.get(options, function (err, response) {
                if (err) {
                    reject(err);
                    return;
                }
                var sheets = {};
                for (var _i = 0, _a = response.sheets; _i < _a.length; _i++) {
                    var sheet = _a[_i];
                    edgecommonconfig_1.config.status('doReadSheets, processing sheet id:', response.spreadsheetId, ' as ', sheet.properties.title);
                    var s = new EdgeGoogleSheet(_this, response.spreadsheetId, response.properties, sheet);
                    sheets[sheet.properties.title] = s;
                }
                if (_this.cacheFile) {
                    edgecommonconfig_1.config.status('doReadSheets, writing cache file:', _this.cacheFile);
                    jsonfile.writeFileSync(_this.cacheFile, {
                        cacheTime: new Date().getTime(),
                        data: response
                    });
                }
                edgecommonconfig_1.config.status('doReadSheets, finished.');
                resolve(sheets);
            });
        });
    };
    EdgeImportExportGoogleDoc.prototype.doReadDoc = function (range, dataFormat) {
        var _this = this;
        if (dataFormat === void 0) { dataFormat = 'UNFORMATTED_VALUE'; }
        return new Promise(function (resolve, reject) {
            var sheets = google.sheets('v4');
            sheets.spreadsheets.values.get({
                auth: _this.getAuth(),
                spreadsheetId: _this.sheetId,
                range: range,
                valueRenderOption: dataFormat,
                dateTimeRenderOption: 'FORMATTED_STRING'
            }, function (err, response) {
                if (err) {
                    edgecommonconfig_1.config.status("EdgeImportExportGoogleDoc doReadDoc Range=" + range + " error", err);
                    reject(err);
                    return;
                }
                var allRows = [];
                for (var _i = 0, _a = response.values; _i < _a.length; _i++) {
                    var row = _a[_i];
                    var thisRow = [];
                    for (var _b = 0, row_1 = row; _b < row_1.length; _b++) {
                        var cell = row_1[_b];
                        if (typeof cell === 'string' && reDateFormat1.test(cell)) {
                            cell = new Date(cell);
                        }
                        thisRow.push(cell);
                    }
                    allRows.push(thisRow);
                }
                resolve(allRows);
            });
        });
        /*
        JSON When you request a single cell as a range:
        {"range":"Sheet1!A1","majorDimension":"ROWS","values":[["Something Here that is text"]]}
        
        JSON When you request a range of cells
        {"range":"Sheet1!A1:F6","majorDimension":"ROWS","values":[["Something Here that is text"],[],["Name","Magic Number","BirthDay","Formula","Another Formula","Formatted Column"],["Brian","23","10/2/1975","41.35","2300","Something Else"],["John","15","1/2/1989","28.08","1500"],["Mike","52.4","3/14/2012","4.87","5240","Another Thing"]]}
        */
    };
    /**
     * Read a part of the document using the range passed in
     * Range examples: "A1" - Default sheet, cell A1 only.
     */
    EdgeImportExportGoogleDoc.prototype.doReadDocFormulas = function (range) {
        return this.doReadDoc(range, 'FORMULA');
    };
    /**
     * Get the OAuth2 object based on the stored credentials
     * See the util/README file for information on how to generate the values.
     */
    EdgeImportExportGoogleDoc.prototype.getAuth = function () {
        var clientId = this.credentials.oauth2.client_id;
        var clientSecret = this.credentials.oauth2.client_secret;
        var redirectURL = '';
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectURL);
        oauth2Client.credentials = this.credentials.oauth2;
        return oauth2Client;
    };
    return EdgeImportExportGoogleDoc;
}());
exports.default = EdgeImportExportGoogleDoc;
//# sourceMappingURL=EdgeImportExportGoogleDoc.js.map