//  The worker class is one connection to the message queue
//  it handles importing the message, processing it with the database
//  and then sends process messages back the parent thread
import config           from 'edgecommonconfig';
import EdgeMessageQueue from 'edgecommonmessagequeue';
import DataSetManager   from 'edgedatasetmanager';
import ClusterWorker    from 'edgecommonclusterworker';
import EdgeApi          from 'edgeapi';
import * as path        from 'path';

const slug     = require('slug');
const jsonfile = require('jsonfile');
const { argv } = require('yargs');

//  During testing, set this to false and the invalid message will stop running
//  the worker allowing you to see the error and not burn the queue.  During
//  production, set to true so that a single invalid message doesn't cause the
//  worker to stop responding.

const ACK_INVALID_MESSAGES = true;
const ENABLE_DEBUG_SAVE    = false;

export class Worker extends ClusterWorker {

    reDoubleSlash: RegExp;
    changemqConnection: any;
    dataStore: any;
    invalidJobLog: any;
    sampleData: any[];
    nCounter: number;
    cache: any;

    constructor(...args: any[]) {
        super();
    }

    onInit() {
        this.reDoubleSlash = new RegExp("//", "g");
        return this.setup();
    }

    //  Incoming job type: message
    //  Log a message to the screen
    processJobMessage(job: any) {
        this.reportText(`Received Message: ${job.txt}`);
        return true;
    }

    //  Message queue related API calls
    //  These calls only push information or jobs to the server, there
    //  is no response.   The message queue we are pushing to is for monitoring changes
    async doOpenChangeMessageQueue() {
        this.changemqConnection = await EdgeMessageQueue.doOpenExchangeForWrite(config.mqItemChanges, config.mqExchangePathUpdates);
        return this.changemqConnection;
    }

    //  Incoming job type: save
    //  Saves changes to an item's json data
    processJobInsertData(job: any) {

        const dataSet   = slug(job.ds);
        const { path }  = job;
        const { agent } = job;
        const newData   = job.data;

        console.log("insert job");
        // process.exit(0);

        if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
            console.log("Invalid JobSaveData", job);
            return false;
        }

        if (!this.dataStore[dataSet]) {
            this.dataStore[dataSet] = new DataSetManager(dataSet);
        }

        return this.dataStore[dataSet].doInsert(path, newData, agent);
    }

    //
    //  Delete a path
    //
    async processJobDelete(job: any) {

        const dataSet  = slug(job.ds);
        const { path } = job;

        if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
            console.log("Invalid JobDelete", job);
            return false;
        }

        if (!this.dataStore[dataSet]) {
            this.dataStore[dataSet] = new DataSetManager(dataSet);
        }


        await this.dataStore[dataSet].doDeletePath(path);

        return true;
    }

    //  Append a record to an array or sub path
    async processJobAppendData(job: any) {

        const dataSet   = slug(job.ds);
        const { path }  = job;
        const { agent } = job;
        const { mode }  = job;
        const newData   = job.data;

        config.status(`processJobAppendData path=${path}`);

        if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
            this.invalidJobLog.error("Invalid Append, missing dataset", { job });
            config.status("processJobAppendData Invalid Job");
            return false;
        }


        if (!this.dataStore[dataSet]) {
            config.status("processJobAppendData Connect to DataSetManager");
            this.dataStore[dataSet] = new DataSetManager(dataSet);
        }

        config.status("processJobAppendData Starting doUpdate");
        await this.dataStore[dataSet].doAppend(path, newData, agent, mode);

        return true;
    }

    //  Incoming job type: save
    //  Saves changes to an item's json data
    async processJobSaveData(job: any) {

        const dataSet   = slug(job.ds);
        const { path }  = job;
        const { agent } = job;
        const { mode }  = job;
        const newData   = job.data;

        if ((dataSet == null) || (dataSet.length == null) || !dataSet.length) {
            config.status("ProcessJobSaveData Invalid Job");
            console.log("Invalid JobSaveData", job);
            return false;
        }

        // config.status "ProcessJobSaveData path=#{path}", newData
        // console.log "ProcessJobSaveData path=#{path}", newData

        if (!this.dataStore[dataSet]) {
            config.status("ProcessJobSaveData Connect to DataSetManager");
            this.dataStore[dataSet] = new DataSetManager(dataSet);
        }

        if (ENABLE_DEBUG_SAVE) {
            this.sampleData.push(job);
        }

        config.status("ProcessJobSaveData Starting doUpdate");
        await this.dataStore[dataSet].doUpdate(path, newData, agent, mode);

        return true;
    }

    //  General job router
    //  Job is an incoming object from the message queue
    //  it should have "job.type" which determines the
    //  rest of the contents and what should be done with it.
    processJob(job: any) {

        if (ENABLE_DEBUG_SAVE && (this.sampleData.length > 9)) {
            if (!this.nCounter) {
                this.nCounter = 0;
            }
            jsonfile.writeFileSync(`SampleData${this.nCounter}.json`, this.sampleData, { spaces: 4 });
            this.nCounter++;
            this.sampleData = [];
        }

        if (job.type === "savedata") {

            return this.processJobSaveData(job);

        } else if (job.type === "append") {

            return this.processJobAppendData(job);

        } else if (job.type === "insertdata") {

            return this.processJobInsertData(job);

        } else if (job.type === "deletepath") {

            return this.processJobDelete(job);

        } else if (job.type === "message") {

            return this.processJobMessage(job);

        } else {

            this.reportText(`Invalid job :${JSON.stringify(job)}`);
            return false;
        }
    }

    //  Initialize the worker
    async setup() {

        this.invalidJobLog = config.getLogger("InvalidJobs");

        //  An array of ItemStorage containers for each store number
        //  that we get updates for
        this.dataStore = {};

        //
        //  Cache connection
        this.cache = EdgeApi.getRedisConnection();


        config.status("Worker setup opening exchange for write changes");
        this.changemqConnection = await EdgeMessageQueue.doOpenExchangeForWrite(config.mqItemChanges, config.mqExchangePathUpdates);

        //
        //  --test <filename> to process a single job for testing.
        //
        if (argv.test != null) {
            const job = jsonfile.readFileSync(path.join(__dirname, `/../test/${argv.test}`));
            config.status("Worker setup testing job: ", argv.test);
            this.fixupJson(job);
            config.status("Worker setup test job data:", job);
            const result = this.processJob(job);
            config.status("Worker setup test job result:", result);
            return false;
        }

        config.status("Worker setup opening message queue read updates");
        // @mqItemUpdates     = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdates
        // @mqItemUpdatesHigh = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdatesHigh
        // @mqItemUpdatesLow  = yield EdgeMessageQueue.doOpenMessageQueue config.mqItemUpdatesLow

        this.sampleData = [];

        const statusHigh = this.processMessageQueue("UpdatesHigh", config.mqItemUpdatesHigh, 6, (job: any) => {
            this.fixupJson(job);
            return this.processJob(job);
        });

        const statusNormal = this.processMessageQueue("Updates", config.mqItemUpdates, 4, (job: any) => {
            this.fixupJson(job);
            // console.log "JOB:", job
            return this.processJob(job);
        });

        const statusLow = this.processMessageQueue("UpdatesLow", config.mqItemUpdatesLow, 1, (job: any) => {
            this.fixupJson(job);
            return this.processJob(job);
        });

        return true;
    }
}

export default Worker;

