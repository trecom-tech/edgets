import * as fs             from 'fs';
import * as path           from 'path';
import JobQueueSubmit      from '../src/JobQueueSubmit';
import config              from 'edgecommonconfig';
import EdgeMessageQueue    from 'edgecommonmessagequeue';
import * as chai           from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

chai.use(chaiAsPromised);
const expect = chai.expect;

process.env.mqHost = process.env.AMQP_HOST || 'amqp://localhost:5672';

const jobQueue = new JobQueueSubmit('High');

describe('JobQueueSubmit', () => {

    it('doSubmitAppend should throw error when not connected', async () => {
        try {
            const data: any = {data: 'test'};
            await jobQueue.doSubmitAppend("dataSet", "/collection/id/test", data.data);
        } catch (err) {
            // should throw error
        }
    });

    it('doSubmitInsert should throw error when not connected', async () => {
        try {
            const data: any = {data: 'test'};
            expect(jobQueue.doSubmitInsert("dataSet", "/collection/id/test", data.data)).to.throw();
        } catch (err) {
            // should throw error
        }

    });

    it('should submit data to queue', async () => {
        const queue     = await EdgeMessageQueue.doOpenMessageQueue(config.mqItemUpdatesHigh);
        const data: any = JSON.parse(fs.readFileSync(path.join(__dirname, "./zipcodes.json")).toString());
        await jobQueue.doSubmitData("dataSet", "/collection/id/test", data.data);

        await new Promise((resolve, reject) => {
            queue.consume(1, (msg: any, job: any) => {
                config.status('Data received in queue');
                // successfully received message; resolving to make the test success
                resolve(true);
            });
        });
    });

    it('doSubmitAppend', async () => {
        const data: any = {data: 'test'};
        await jobQueue.doSubmitAppend("dataSet", "/collection/id/test", data.data);
    });

    it('doSubmitInsert', async () => {
        const data: any = {data: 'test'};
        await jobQueue.doSubmitInsert("dataSet", "/collection/id/test", data.data);
    });

    it('doDeletePath', async () => {
        const queue     = await EdgeMessageQueue.doOpenMessageQueue(config.mqItemUpdatesHigh);
        const data: any = {data: 'test'};
        await jobQueue.doDeletePath("dataSet", "/collection/id/test");
    });
});
