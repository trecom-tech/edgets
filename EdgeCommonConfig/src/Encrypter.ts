import * as crypto from "crypto";

const iv = Buffer.alloc(16, 0);

export function encrypt(text: string, key: string): string {
    const cipher  = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted     = Buffer.concat([encrypted, cipher.final()]);
    return encrypted.toString('hex');
}

export function decrypt(text: string, key: string): string {
    const encryptedText = Buffer.from(text, 'hex');
    const decipher      = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted       = decipher.update(encryptedText);
    decrypted           = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}