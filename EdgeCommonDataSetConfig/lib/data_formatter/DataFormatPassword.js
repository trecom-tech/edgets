"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//
// class for Password datatype
//
// @extends DataFormatText
//
const DataFormatText_1 = require("./DataFormatText");
class DataFormatPassword extends DataFormatText_1.DataFormatText {
    constructor(...args) {
        super(...args);
        this.name = 'password';
    }
    //
    // funtion to open editor for password
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent, left, top, width, height, currentValue, path) {
        if ((currentValue == null)) {
            currentValue = "";
        }
        // @ts-ignore
        const m = new ModalViewDialog({
            showOnCreate: false,
            content: "You can change password by filling out this form.",
            title: "Change Password",
            ok: "Save"
        });
        m.setView("PasswordChange", (viewPasswordChange) => {
            this.viewPasswordChange = viewPasswordChange;
            return true;
        }, { hasOriginPassword: (this.options != null ? this.options.hasOriginPassword : undefined) });
        m.onButton2 = (e, fields) => {
            this.viewPasswordChange.onSubmitCallback(this.viewPasswordChange.getViewForm());
            m.hide();
            return true;
        };
        return m.show();
    }
    attachEditor(elSourceInput, path) {
        // #
        // # set type of this input as "password"
        elSourceInput.prop("type", "password");
        if (this.align != null) {
            elSourceInput.css("text-align", this.align);
        }
        return this;
    }
}
exports.DataFormatPassword = DataFormatPassword;
exports.default = DataFormatPassword;
//# sourceMappingURL=DataFormatPassword.js.map