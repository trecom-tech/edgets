# Getting Credentials

The script [get_oauth2_permissions](get_oauth2_permissions.js) will help get the permissions from the OAUTH2
[Url from Google](https://console.developers.google.com/apis/credentials).

Make sure to edit the file and put the correct Client Key.

    var CLIENT_ID     = '424022811922-591p70ohrl9480croshvuihltejjshtr.apps.googleusercontent.com';
    var CLIENT_SECRET = '1RvnvnYVpVcXgLqS-cvsta2Y';

Once you run the script you will visit url it tells you and get the key which looks something like his:

"oauth2": {
  "client_id": "424022811922-591p70ohrl9480croshvuihltejjshtr.apps.googleusercontent.com",
  "client_secret": "1RvnvnYVpVcXgLqS-cvsta2Y",
  "refresh_token": "1/n7VDhvM7bCTv3MINoLmKG_0boMKFCVX3S4d9S1bWKlI"
}

The entire structure needs to be setup as a credential in the local configuration file.   The person in charge
of the credential file would need to edit save_creds.coffee

    "GoogleDocs1" :
        oauth2:
            client_id:     "424022811922-591p70ohrl9480croshvuihltejjshtr.apps.googleusercontent.com"
            client_secret: "1RvnvnYVpVcXgLqS-cvsta2Y"
            refresh_token: "1/n7VDhvM7bCTv3MINoLmKG_0boMKFCVX3S4d9S1bWKlI"


