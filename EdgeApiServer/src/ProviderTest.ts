import ApiProvider    from './ApiProvider';
import IApiUser         from './interfaces/IApiUser';
import EdgeApi        from 'edgeapi';
import EdgeError      from './EdgeError';

export class ProviderTest extends ApiProvider 
{

    constructor(prototype: ApiProvider) 
    {
        super(prototype);
    }

    onInit() {
    }

    getBaseName() {
        return 'test';
    }

    /**
     * The most basic form of API call, /test/SimpleTest outputs the current system date.
     * http://localhost:8001/api/test/doSimpleTest.json
     */
    doSimpleTest() :string
    {
        return new Date().toString();
    };

    doShowUser(user :IApiUser)
    {
        let result = { 
            name          : user.myName,
            ip_address    : user.ipAddress,
            authAttempts  : user.authAttempts,
            timezoneOffset: user.timezoneOffset,
            browser       : user.userAgent,
            location      : user.location
        };
        return result;
    }

};

export default ProviderTest;
