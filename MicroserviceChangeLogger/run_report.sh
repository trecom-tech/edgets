#!/bin/sh
FILENAME=`ls -rt lib/isolate* | tail -1`
echo "Found: $FILENAME"
node --prof-process "$FILENAME" > processed.txt
rm $FILENAME