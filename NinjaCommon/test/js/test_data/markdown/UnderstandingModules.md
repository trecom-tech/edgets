# Understanding modules

Only module names are listed below, you can check further details of the modules in `UnderstandingVendors.md`.


Global modules
------------------------------------------

- EvEmitter
- Handlerbars
- Moment.js
- Numeral.js
- Draggabilly


View-level Modules
------------------------------------------

| Module Name         | Where it's being used in
| ------------------- | ----------------------------------------------------------------
| Ace                 | ViewCodeEditor, ViewShowTableEditor
| Full Calendar       | ViewCalendar
| Gridstack           | ViewGrid
| JointJS             | ViewDiagram
| MapboxGL            | ViewMapBox
| PDF.js              | ViewDocumentPDF
| SummernoteJS        | ViewForm, ViewTable(for data formatters) and ViewWysEditor
| FileSaver.js        | ViewTable
| Xlsx.js             | ViewTable
| Backbone.js         | ViewDiagram
| BootStrap Toggle    | ViewForm(for boolean data formatter)
| CanvasJS            | ViewDataChart and ViewGraphTime
| D3.js               | ViewChartEpoch
| Epoch.js            | ViewChartEpoch
| Flatpickr           | In date-related data formatters
| HTML Sortable       | ViewTableRearrange
| jQuery Multi Select | ViewTable and ViewForm
| jQuery Range        | In number data formatter
| Lodash              | ViewDiagram and ViewGrid
| Markdown-it         | ViewMarkDown
| Miller.js           | ViewMiller
| Mousetrap.js        | ViewDynamicTabs and ViewNavbar
| Selectize.js        | ViewComboBox, ViewTable, ViewForm and tags/enum data formatters
| Sha256.js           | ViewPasswordChange
| IScroll             | PopupWindow
| Prism.js            | ViewTestCode
