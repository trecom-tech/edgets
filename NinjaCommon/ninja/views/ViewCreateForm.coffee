class ViewCreateForm extends View 

    getLayout: ()=>
        view: "DockedToolbar"
        items: [
            type: "button"
            text: "Add Field"
            icon: "fa fa-plus"
            callback: @onAddField
        ,
            type: "button"
            text: "Remove Field"
            icon: "fa fa-trash-alt"
            callback: @onRemoveField
        ,
            type: "button"
            text: "Set Content Header"
            icon: "fa fa-comment"
            callback: @onSetContentHeader
        ,
            type: "button"
            text: "Set Title"
            icon: "fa fa-book"
            callback: @onSetTitle
        ]
        main:
            view: "Form"
            name: "viewForm"
            onSubmit: (form) =>
                validfields = @addedCols.map (col) ->
                    return col.getSource()
                if @columnsUpdated is true
                    # console.log "DataMap setFormInTable: ", @tableName, @formName, validfields    
                    DataMap.setFormInTable @tableName, @formName, 
                        validfields: validfields

                obj = {}
                for field in validfields 
                    val = form[field]?.getValue()
                    if val?
                        obj[field] = val
                if obj['id']?
                    DataMap.addData @tableName, obj['id'], obj
                else 
                    DataMap.addDataWithAutoID @tableName, obj

    onShowScreen: () =>
        GlobalClassTools.addEventManager(this)
        @gid = GlobalValueManager.NextGlobalID()
        true

    setTableName: (@tableName) =>
        @refreshColumns @tableName
        true

    setFormName: (@formName) =>
        true

    setValidColumns: (@colNames) =>
        if !colNames? or !colNames.length then return false
        result = []
        for cn in colNames
            cols = @columns.filter (column) -> column.getSource() == cn
            if cols? and cols.length
                col = cols[0]
                result.push col 
        if result.length > 0
            @addedCols = result
        @columnsUpdated = false
        true


    onAddField: (e) =>
        if !@addedCols then @addedCols = []
        p = new PopupMenu "Fields", e 
        for column in @columns
            if @addedCols.indexOf(column) >= 0 then continue
            p.addItem column.getName(), (event, col) =>
                # console.log "Column is added: ", col.getName()
                @addedCols.push col
                validFields = @addedCols.map (col) ->
                    return col.getSource()                
                @buildForm(@tableName, @formName, validFields)
                @columnsUpdated = true
                true
            , column
        true

    onRemoveField: (e) =>
        if !@addedCols or @addedCols.length is 0 then return false
        p = new PopupMenu "Fields", e 
        for column, index in @addedCols
            p.addItem column.getName(), (event, i) =>
                # console.log "Column is removed: ", @addedCols[i].getName()
                @addedCols.splice i, 1
                validFields = @addedCols.map (col) ->
                    return col.getSource()                
                @buildForm(@tableName, @formName, validFields)
                @columnsUpdated = true
                true
            , index

        true

    onSetContentHeader: (e) =>
        e.stopPropagation()
        btnObj = 
            title: "Save Note"
            tooltip: "Click to save the note"
            callback: (e) =>
                strNote  = @viewContentEditor.getContent()
                strNote  = strNote.replace /^<p>/, ""
                strNote  = strNote.replace /<.p>$/, ""
                strNote  = strNote.trim()
                @content = strNote
                @viewForm.setContentHeader @content 
                @viewContentEditor.setContent ""
                @viewPopupContentWindow.close()
                true
            icon: "<i class='fa fa-save'></i>"

        doPopupView "WysEditor", "Set the title", "createform-content-editor-#{@gid}", 800, 600, (view) =>
            @viewContentEditor = view
            @viewContentEditor.createButton btnObj
            @viewContentEditor.setContent(@content or "")

        .then (@viewPopupContentWindow) =>
            # @viewPopupContentWindow.showInFront()
            true
        true

    onSetTitle: (e) =>

        m = new ModalForm
            title:        "Set Title"
            content:      "Please write down title of the form."
            position:     "top"
            ok:           "Save"
            inputs: [
                name: "title"
                label: "Title"
            ]
            buttons: [
                type: "submit"
                text: "Save"
            ]
            onsubmit: (form) =>
                @title = form.title.getValue()
                m.hide()
                true

        true

    refreshColumns: (tableName) =>
        columns = DataMap.getColumnsFromTable tableName, null
        if !@columns? then @columns = []
        if columns? and columns.length 
            @columns = columns
        true

    buildForm: (tableName, formName, fields) =>
        @viewForm.renderFromTable tableName, fields, true
        @viewForm.setContentHeader @content
        true

    removeExtraThings: () =>
        if @tableName? and @formName? 
            DataMap.removeFormInTable @tableName, @formName

    serialize: () =>
        return
            tableName:     @tableName
            formName:      @formName
            validColumns:  @colNames

    deserialize: (options) =>
        new Promise (resolve, reject) =>
            if !options? or typeof options isnt "objectc"
                reject "ViewCreateForm: invalid data passed"

            if options.tableName?
                @setTableName options.tableName

            if options.formName?
                @setFormName options.formName

            if options.validColumns?
                @setValidColumns options.validColumns

            resolve true
