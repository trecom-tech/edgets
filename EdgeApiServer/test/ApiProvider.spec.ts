import EdgeApi            from 'edgeapi';
import { addServiceUser } from '../src/utility/AddServiceUser';

describe('ApiProvider', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();

    before(async () => {
        // add service user back
        await addServiceUser();

        api = await EdgeApi.doGetApi();
    });

    it('doCall should succeed', async () => {
        const params = {
            test: true
        };

        const result = await api.test_doTestdoCall(params);
        result.should.match(/as sent request with/);
    });
});

