import { DataFormatterType } from './DataFormatterType';

export class DataFormatMultiselect extends DataFormatterType {
    name: string   = 'multiselect';
    options: any[] = [];
    align: string  = 'align';

    constructor(...args: any[]) {
        super();
    }


    //
    // funtion to open multiselect editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        // @ts-ignore
        const m = new ModalDialog({
            showOnCreate: false,
            content: "Select the list of items",
            title: "Select options",
            ok: "Save"
        });

        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',').map(item => item.toString().trim());
        }

        m.getForm((form: any) => {
            form
                .addInput("select1", "Selection")
                .setOptions(this.options)
                .setDataFormatter("tags")
                .setValue(currentValue);

            return form.setSubmitFunction((f: any) => {
                this.saveValue(f.select1.getValue());
                return m.hide();
            });
        });

        return m.show();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(currentValue: any, options: any, path: any) {
        if (typeof options === "string") {
            options = options.split(',').map(option => option.toString().trim());
        }
        this.options = options;
        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',').map(item => item.toString().trim());
        }

        if (Array.isArray(currentValue)) {
            // #
            // # Make sure that all in current value belongs to options
            for (let val of Array.from(currentValue)) {
                if (this.options.indexOf(val) < 0) {
                    this.options.push(val);
                }
            }
            return currentValue.join(", ");
        }

        const values = [];
        for (let idx in currentValue) {
            // #
            // # Make sure that all in current value belongs to options
            const obj = currentValue[idx];
            if (this.options.indexOf(obj) < 0) {
                this.options.push(obj.toString().trim());
            }
            values.push(obj);
        }
        return values.join(", ");
    }

    //
    // funtion to unformat the currently unformatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(currentValue: any, path: any) {
        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',');
        }
        return currentValue;
    }
}

export default DataFormatMultiselect;