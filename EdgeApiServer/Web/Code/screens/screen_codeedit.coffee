class ScreenCodeEdit extends Screen

    windowTitle      : "View, Edit, Run Code Module"
    windowSubTitle   : "Current Module is ..."

    getLayout: ()=>
        view: "Splittable"
        percent: 70
        right:
            view: "DynamicTabs"
            minwidth: 300
            tabs: [
                title: "Test Cases"
                view: "DockedToolbar"
                items: [
                    type: "button"
                    text: "Create new"
                    icon: "fa fa-plus"
                    callback: @onButtonNewTestCase
                ,
                    type: "button"
                    text: "Run selected"
                    icon: "fa fa-plus"
                    keyboard: "r"
                    callback: @onButtonRun
                    linkedTableWithDisable : "codeedit"
                ]
                main:
                    view:       "Table"
                    name:       "viewTestCases"
                    tabletitle: "Test cases for this code"
                    collection: "codeedit"
                    autofill:   true
            ,
                view: "DockedToolbar"
                title: "README"
                items: [
                    text:     "Save"
                    icon:     "fa fa-save"
                    callback: @onButtonSaveNotes
                    keyboard: "s"
                    type:     "button"
                ]
                main:
                    view: "WysEditor"
                    name: "viewCodeNotes"
            ,
                view: "CodeEditProperties"
                name: "viewProperties"
                title: "Properties"
            ]

        left:
            view: "Splittable"
            percent: 70
            name: "viewCodeAreaSplitter"
            top:
                view: "DynamicTabs"
                tabs: [
                    view: "DockedToolbar"
                    title: "Dev Version"
                    items: [
                        type: "button"
                        text: "Save Local"
                        icon: "fa fa-save"
                        callback: @onButtonSaveDev
                    ,
                        type: "button"
                        text: "Add Parameter"
                        icon: "fa fa-plus"
                        callback: @onButtonNewParam
                    ,
                        type: "button"
                        text: "Add Database"
                        icon: "fa fa-plus"
                        callback: @onButtonNewDatabase
                    ]
                    main:
                        view: "CodeEditor"
                        name: "viewCodeLocal"
                ,
                    view: "FileManager"
                    name: "viewFileManager"
                    title: "Attachments"
                ]
            bottom:
                view: "DynamicTabs"
                name: "viewBottomTabs"
                tabs: [
                    view: "ConsoleOutput"
                    name: "viewCodeOutput"
                    title: "Output"
                ,
                    view: "CodeEditHelp"
                    name: "viewCodeEditHelp"
                    title: "Help"
                ]



    onShowScreen: ()=>

        #
        # Save changes made to test cases
        DataMap.setSaveCallback "codeedit", (id, field, oldValue, newValue)=>
            console.log "TEST SAVE id=#{id} field=#{field} old=#{oldValue} newValue=#{newValue}"
            true

        # id=5 field=neighborhood old=asdf newValue=apple

        #
        # @optionalArgs is the name of the code module to edit

        parts = @optionalArgs.split("_")
        @category = parts.shift()
        @name = parts.shift()

        # @viewTestCases.addTable "codeedit"

        @viewTestCases.setEnableCheckboxes(true, 100)

        $("#MainTitle").html "Edit module #{@category}/#{@name}"
        $("#SubTitle").html "Local code is not live, select from the options."

        if @alreadyInitialized
            #
            # Already been in the code editor once before
            @viewCodeLocal.setContent(" ")
            @viewCodeOutput.restart()

            #
            # Load the local copy of the code from the server
            globalBusyDialog.showBusy "Loading code from server"
            sock.code_doGetModuleLocal(@category, @name)
            .then (result)=>
                @onCodeLoaded(result)

            return


        #
        # Setup for server push output
        sock.on "runcodeLog", @onServerCodeOutput

        @setupCodeTabs()
        @setupOutputArea()
        @reloadCode()
        @alreadyInitialized = true

        true


    #
    # The main code area
    setupCodeTabs: ()=>

        @viewCodeLocal.showEditor()
        @viewCodeLocal.setTheme "tomorrow_night_eighties"
        @viewCodeLocal.setOptions
            enableBasicAutocompletion: true
            enableSnippets: true
            enableLiveAutocompletion: true
        @viewCodeLocal.setMode "coffee"

    reloadCode: ()=>
        #
        # Load the local copy of the code from the server
        globalBusyDialog.showBusy "Loading code from server"
        sock.code_doGetModuleLocal(@category, @name)
        .then (result)=>
            @onCodeLoaded(result)

    #
    # Under the code we have tabs for the output
    setupOutputArea: ()=>
        @viewCodeOutput.restart()
        true

    #
    # Execute the code on the server, parse the result
    doExecuteCode: (userObject, params, options)=>

        globalBusyDialog.showBusy "Executing code on server"
        new Promise (resolve, reject)=>

            try
                @viewCodeOutput.restart()
                @viewBottomTabs.show(@viewCodeOutput.id)

                if !params? then params = {}
                if !options? then options = {}
                if !userObject then userObject = {}
                sock.code_doRunCode @category, @name, userObject, params, options
                .then (result)=>

                    console.log "Found code result:", result
                    if result?

                        if result.error?
                            @viewCodeOutput.addOutput({ type: "error", values: result.error })

                        if result.total_ms?
                            @viewCodeOutput.endRun(result)

                    globalBusyDialog.finished()
                    resolve(true)


            catch e

                console.log "Exception trying to run:", e


    #
    # Convert the function parameters into a Datamap table
    createCodeEditTable: ()=>
        DataMap.removeTable("codeedit")
        if !@codeRecord.meta? then return
        if !@codeRecord.meta.params? then return

        for item in @codeRecord.meta.params

            itemType = "text"

            col = DataMap.addColumn "codeedit",
                name:     item.name
                source:   item.name
                type:     itemType
                options:  item.options
                editable: true

        DataMap.addColumn "codeedit",
            name: "Test case comments"
            source: "comments"
            type: "text"
            autosize: true
            editable: true

        DataMap.addColumn "codeedit",
            name: "Expected result"
            hint: "Leave blank to capture the result"
            source: "result"
            type: "text"
            editable: true

        true

    #
    # Callback when the server returns local module after loading
    onCodeLoaded: (result)=>

        globalBusyDialog.finished()
        console.log "onCodeLoaded: ", result
        if !result? then result = {}
        if !result.code? then result.code = ""

        if result.error?
            @viewCodeOutput.addOutput({ type: "error", values: result.error })
            # @viewCodeLocal.setContent("Internet server error:\n" + result.e)
            return

        @codeRecord = result
        document.title = "CodeEditor #{@codeRecord.category} / #{@codeRecord.name}"
        @viewCodeLocal.setContent(result.code)

        if result.meta?
            @createCodeEditTable()
            @viewProperties.updateFromMeta @codeRecord

            if result.testCases?
                for id, item of result.testCases
                    DataMap.addData "codeedit", id, item

        if result.notes?
            @viewCodeNotes.setContent result.notes



        true

    #
    # Called when the server sends a push message with some type of output
    onServerCodeOutput: (data)=>
        @viewCodeOutput.addOutput(data)

    #
    # Save local version
    onButtonSaveDev: (e)=>
        codeLocal = @viewCodeLocal.getContent()
        globalBusyDialog.waitFor "Saving to server", sock.code_doSaveModuleLocal @category, @name, codeLocal
        .then (result)=>
            console.log "saved:", result
            @onCodeLoaded(result)

    #
    # Save notes to a module
    onButtonSaveNotes: (e)=>
        content = @viewCodeNotes.getContent()
        globalBusyDialog.show "Saving notes to server"
        sock.code_doSaveModuleNotes @category, @name, content

    #
    # Run the test cases that are selected
    onButtonRun: (e)=>
        console.log "Run"

    #
    # Called by the viewForm when you create a new test case
    onAddTestCase: (form)=>

        options = {}
        for item in @codeRecord.meta.params
            options[item.name] = form[item.name].value

        options["comments"] = form["comments"].value
        options["result"] = form["result"].value

        p = sock.code_doTestCaseAdd(@category, @name, options, null)
        p.then @onCodeLoaded
        true


    #
    # Toolbar button "New test case"
    onButtonNewTestCase: (e)=>

        new ModalForm
            title: "New test case"
            create_from_table: "codeedit"
            onSubmit: @onAddTestCase

    #
    # Toolbar button: "Add Parameter"
    # Dialog box to add a new parameter to the current module
    onButtonNewParam: (e)=>

        options =
            title: "Add a parameter"
            buttons: [
                type:    "submit"
                text:    "Add parameter"
            ]
            inputs:  [
                name:    "name"
                label:   "Parameter name"
            ,
                name:    "type"
                label:   "Data type"
                type:    "enum"
                options: ["text","number","object"]
            ,
                name:    "description"
                label:   "Description"
            ]

            onSubmit: (form)=>

                globalBusyDialog.showBusy "Saving to server"

                str = "## @param {#{form.type.value}} #{form.name.value}"
                if form.description.value
                    str += " - " + form.description.value

                codeLocal = @viewCodeLocal.getContent()
                codeLocal = str + "\n" + codeLocal
                @viewCodeLocal.setContent(codeLocal)
                console.log "SAVE CODE:", codeLocal
                @onButtonSaveDev(e)
                return true


        console.log "Options=", options
        m = new ModalForm(options)

    #
    # Toolbar button: "Add Database"
    # Dialog box to add a new database to the current module
    onButtonNewDatabase: (e)=>

        options =
            title: "Add database"
            buttons: [
                type:    "submit"
                text:    "Add database"
            ]
            inputs:  [
                name:    "name"
                label:   "Variable name"
                type:    "text"
                value:   "sql"
            ,
                name:    "db"
                label:   "Optional config name"
                type:    "text"
            ,
                name:    "comment"
                label:   "Comments"
                hint:    "Optional comments for the field"
            ]

            onSubmit: (form)=>

                globalBusyDialog.showBusy "Saving to server"

                if form.db.value.length > 0
                    str = "## @sql {#{form.db.value}} #{form.name.value}"
                else
                    str = "## @sql #{form.name.value}"

                if form.comment.value
                    str += " - " + form.comment.value

                codeLocal = @viewCodeLocal.getContent()
                codeLocal = str + "\n" + codeLocal
                @viewCodeLocal.setContent(codeLocal)
                console.log "SAVE CODE:", codeLocal
                @onButtonSaveDev(e)
                return true

        console.log "Options=", options
        m = new ModalForm(options)

