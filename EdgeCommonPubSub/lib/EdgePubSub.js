"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const zlib = require("zlib");
const mqtt = require("async-mqtt");
// MessageQueue controller for MQTT
class EdgeMQTTPubSub {
    constructor(clientId = 'edgemqttclient') {
        this.clientId = clientId;
    }
    // method for subscribing to a topic
    consume(topicName, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue consume Starting`);
            yield this.doConnect();
            yield EdgeMQTTPubSub.mqttClient.subscribe(topicName, { qos: 1 });
            EdgeMQTTPubSub.mqttClient.on('message', (topic, message) => {
                if (topicName === topic) {
                    callback(message);
                }
            });
        });
    }
    // method for connecting
    doConnect() {
        return new Promise((resolve, reject) => {
            if (!EdgeMQTTPubSub.mqttClient || !EdgeMQTTPubSub.mqttClient.connected) {
                // config.getCredentials('mqHost');
                edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue connecting to client`);
                const server = edgecommonconfig_1.config.getCredentials('mqttHost');
                EdgeMQTTPubSub.mqttClient = mqtt.connect(server, {
                    clientId: this.clientId,
                    protocolId: 'MQIsdp',
                    protocolVersion: 3,
                    connectTimeout: 1000
                });
                EdgeMQTTPubSub.mqttClient.on('connect', () => {
                    edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue client connected`);
                    resolve();
                });
                EdgeMQTTPubSub.mqttClient.on('error', (err) => {
                    edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue client connection faileed`);
                    reject(err);
                });
            }
            else {
                resolve();
            }
        });
    }
    //
    //  Disconnect / disable the message queue
    doClose() {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue client disconnecting`);
            if (EdgeMQTTPubSub.mqttClient) {
                yield EdgeMQTTPubSub.mqttClient.end(true);
                edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue client disconnected`);
            }
        });
    }
    // Publish string to a topic
    doPublish(topicName, str) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doConnect();
            return new Promise((resolve, reject) => {
                if (typeof str === 'object') {
                    str = JSON.stringify(str);
                }
                zlib.deflate(str, (err, buffer) => __awaiter(this, void 0, void 0, function* () {
                    if (err != null) {
                        edgecommonconfig_1.config.error('zlib.deflate error:', err);
                        reject(err);
                    }
                    else {
                        edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue Publish ${buffer.length} bytes (from ${str.length})`);
                        try {
                            yield EdgeMQTTPubSub.mqttClient.publish(topicName, buffer);
                            resolve(true);
                        }
                        catch (err) {
                            reject(err);
                        }
                    }
                }));
            });
        });
    }
    //
    //  Write either JSON text or a binary buffer to the message queue
    //  returns a promise write the write is complete.
    //
    doSubmitBuffer(topicName, str) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doConnect();
            return new Promise((resolve, reject) => {
                // Compress data and submit it
                zlib.deflate(str, (err, buffer) => __awaiter(this, void 0, void 0, function* () {
                    if (err != null) {
                        edgecommonconfig_1.config.error('zlib.deflate error:', err);
                        reject(err);
                    }
                    else {
                        edgecommonconfig_1.config.status(`EdgeMQTTMessageQueue doSubmitBuffer Sending Compressed ${str.length} => ${buffer.length}`);
                        try {
                            yield EdgeMQTTPubSub.mqttClient.publish(topicName, buffer);
                            resolve(true);
                        }
                        catch (err) {
                            reject(err);
                        }
                    }
                }));
            });
        });
    }
}
exports.EdgeMQTTController = new EdgeMQTTPubSub();
exports.default = exports.EdgeMQTTController;
//# sourceMappingURL=EdgePubSub.js.map