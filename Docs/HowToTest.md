## Checking the status of a server:

Any server should be able to return the status with a simple command

    curl http://localhost:8001/os/GetServerInformation.json | jq