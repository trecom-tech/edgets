import EdgeCommonNotification from '../src/EdgeCommonNotification';
import {expect}               from 'chai';
import { IContactInfo }       from '../src/IContactInfo';
import './helper/setup';

const notification = new EdgeCommonNotification();

const smsValidContactDetail: IContactInfo = {
    type: 'sms',
    priority: 3,
    account: {
        phoneNumber: '13609407548',
    }
};

const smsInvalidContactDetail: IContactInfo = {
    type: 'sms',
    priority: 3,
    account: {
        phoneNumber: '136094075480',
    }
};

const message = {
    title: 'Testing Twilio Notification',
    alert: `\u2709 You have a new message(${new Date()})`,
    mediaUrl: 'https://emerald-coral-3661.twil.io/assets/2-OwlAir-Upcoming-Trip.PNG',
};

describe('Twilio', () => {
    it('doSendSMS() with valid number', async () => {
        const result = await notification.doSendSMS(smsValidContactDetail.account.phoneNumber, message);
        expect(result).to.equal(true);
    });

    it('doSendSMS() with invalid number', async () => {
        const result = await notification.doSendSMS(smsInvalidContactDetail.account.phoneNumber, message);
        expect(result).to.equal(false);
    });
});
