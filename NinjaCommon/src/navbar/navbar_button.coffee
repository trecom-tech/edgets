## -------------------------------------------------------------------------------------------------------------
## class to create button for the navbar
##
class NavButton

	## -------------------------------------------------------------------------------------------------------------
	## constructor to create new button
	##
	## @param [String] value to display
	## @param [String] classes to add in button default is btn btn-primary
	## @param [Object] attrs additional attributes
	##

	constructor: (@value, @classes = "", @attrs = {}) ->

		if !@attrs.type
			@attrs.type = "submit"

		@classes += " flat-toolbar-btn"

		@icon = ""
		@gid  = "b" + GlobalValueManager.NextGlobalID()

	## -------------------------------------------------------------------------------------------------------------
	## function to get the html of the current button
	##
	## @return [String]
	##
	getHtml: () ->
		$template = '''<button class="{{classes}}" id="{{gid}}"
				{{#each attrs}}
					{{@key}}="{{this}}"
				{{/each}}
				>{{{icon}}}{{{value}}}</button>'''

		Handlebars.compile($template)(this)

	setValue: (@value) ->

	setIcon: (@strIcon)->
		@icon = "<i class='#{@strIcon}'></i> "
		@renderHTML()
		true

	setType: (type = "button") =>
		@type = type 
		true

	setAttribute: (key, value) =>
		if !key or !value then return false
		@attrs[key] = value
		true

	onClick: (e)=>
		# console.log "Click this button: ", this
		@toggleChecked()
		true

	setClickCallback: (callback) =>
		if !callback? or typeof callback isnt "function" then return false
		@callback = callback
		if @type is "button"
			@onClick = (e) =>
				result = @callback(e)
				if !@disabled and result? and result.then? and typeof result.then is "function"
					@disableButton true
					result.then () =>
						@disableButton false
		else if @type is "toggle"
			@onClick = (e) =>
				@toggleChecked()
				result = @callback(e, @isChecked)
				if !@disabled and result? and result.then? and typeof result.then is "function"
					@disableButton true
					result.then () =>
						@disableButton false
		true

	disableButton: (bool = true) =>
		el = $("button##{@gid}")
		if bool
			el.attr("disabled", true)
		else
			el.removeAttr "disabled"
		true

	disable: (bool = true) =>
		@disableButton bool
		@disabled = bool
		true

	addHotkey: (key, callback)=>
		@key = key
		if !Mousetrap?
			console.log "Warning: Mousetrap is not loaded, no hotkeys available."
			return

		if callback?
			@onKeyHandler = callback
			Mousetrap.bind ['ctrl+'+key, 'command+'+key], @onKeyHandler
		else
			Mousetrap.bind ['ctrl+'+key, 'command+'+key], @onClick

		@setTitle "Run (control+" + key + ")"
		true

	setTitle: (text)=>
		@attrs.title = text
		true

	destroy: ()=>
		if @key?
			Mousetrap.unbind ['ctrl+'+@key, 'command+'+@key]
		true

	## -gao
	## function to link this button to table
	setLinkedTable: (tableName, disableOnEmpty) =>
		if disableOnEmpty? and typeof disableOnEmpty is "boolean"
			@disableOnEmpty = disableOnEmpty
		
		@linkedTable 	= tableName
		@linkedRowCount = 0
		GlobalClassTools.addEventManager(this)
		
		##
		## initialize button's content by current count of selected rows
		#@onChangeRowSelected(@linkedTable)

		##
		## attach a listener to "row_selected" event which will be fired from TableView
		globalTableEvents.on "row_selected", @onRowSelected

	setAlign: (align) =>
		el = $("button##{@gid}")
		if align is "right"
			el.addClass "toolbar-btn-right"
		else
			el.removeClass "toolbar-btn-right"
		true		

	##
	## function to handle toggling checkboxes
	onRowSelected: (tableName, id, status) =>
		if tableName is @linkedTable
			@linkedRowCount = DataMap.getSelectedList(@linkedTable).length
			# console.log "NavButton table #{@linkedTable}'s selected row count is : #{@linkedRowCount}"
			@renderHTML()
		else 
			return false
		## if no linked rows, then disable the button
		if @disableOnEmpty is true and @linkedRowCount is 0
			@disableButton true

		else
			@disableButton false
		true

	applyAttributes: () =>
		el = $("button##{@gid}")
		if el.length is 0 then return false
		el.attr @attrs
	
	renderHTML: () =>
		el = $("button##{@gid}")
		if el.length is 0 then return false
		if @linkedRowCount > 0
			el.html "#{@icon}#{@value} (#{@linkedRowCount})"
		else
			el.html "#{@icon}#{@value}"

	##
	## re-render html based on new value and icon
	updateHTML: (value, icon, attrs = {}) =>
		isUpdated     = false
		isAttrUpdated = false
		if value?
			@setValue value
			isUpdated = true

		if icon?
			@setIcon icon
			isUpdated = true

		for key, value of attrs
			@setAttribute key, value
			isAttrUpdated = true

		if isUpdated is true
			@renderHTML()

		if isAttrUpdated is true
			@applyAttributes()
		isUpdated or isAttrUpdated

	##
	## toggle checked status if button is "toggle" type
	toggleChecked: () =>
		if !@isChecked? then return false
		if @isChecked is true
			@isChecked = false
			@setIcon "far fa-circle"		
		else
			@isChecked = true
			@setIcon "far fa-check-circle"
		el = $("button##{@gid}")
		@renderHTML()

		if @storeName? then localStorage.setItem @storeName, Number(@isChecked)
		true						
