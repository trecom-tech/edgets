export interface IMessage {
    alert: string;
    title?: string;
    topic?: string;
    payload?: any;
    sound?: any;
    expiry?: any;
    badge?: any;
    priority?: any;
    urlArgs?: any[];
}
