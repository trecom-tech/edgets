import EdgeCommonMailer from '../src/EdgeCommonMailer';
import config           from 'edgecommonconfig';


const path = require('path');
const should = require('should');

const sampleMarkdown = `
# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***`;

const withImage = `
# With Image
![Minion](./tree.png)
<style>
    img {
        width: 100px;
        height: auto;
    }
</style>
`;

const withOnlineImage = `
# With Image
![Minion](https://image.shutterstock.com/image-photo/small-green-decorative-tree-growing-260nw-298056758.jpg)
`;

const tableMarkdown = `
# Table
| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |
<div style="font-size: 20px">
    <p> Example of table with inline html in markdown </p>
    <table>
        <thead>
            <tr>
                <th style="border: 1px solid green;">Tables</th>
                <th style="border: 1px solid green;">can be generated via html</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid green;">Style</td>
                <td style="border: 1px solid green;">it inline</td>
            </tr>
        </tbody>
    </table>
</div>
<style>
    table {
        boder-width: 1px;
    }
</style>
`;

describe('EdgeCommonMailer', () => {

    let mailer: EdgeCommonMailer;

    const message: any = {
        to          : 'edge.tester01@yopmail.com',
        subject     : 'EdgeCommonMailer Test',
        from        : 'noreply@edgecommonmailer.com',
        text        : 'This is test email',
    };

    before(() => {
    });

    describe('doSendMessage', () => {
        before(() => {
            mailer = EdgeCommonMailer.getMailer();
        });

        it('should return error without credential', async () => {
            try {
                await mailer.doSendMessage(message);
            } catch(err) {
                err.should.not.be.eql(null);
            }
        });
    });

    describe('doSendMessage', () => {
        let homeUrl = '';

        before(() => {
            config.setCredentials('sendgrid', {
                type: 'sendgrid',
                apikey: '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'
            });

            mailer = EdgeCommonMailer.getMailer(true);

            homeUrl = process.env.HOME;
            process.env.HOME = path.resolve(__dirname, 'data');
        });

        after(() => {
            process.env.HOME = homeUrl;
        })

        it('should return error with invalid data', async () => {
            try {
                await mailer.doSendMessage('random string');
            } catch(err) {
                err.should.not.be.eql(null);
            }

            try {
                await mailer.doSendMessage(5);
            } catch(err) {
                err.should.not.be.eql(null);
            }
        });

        it('invalid email', async () => {
            try {
                const invalidEmail = {
                    ...message,
                    from: 'random'
                };

                await mailer.doSendMessage(invalidEmail);
            } catch(err) {
                err.should.not.be.eql(null);
            }

            try {
                const invalidEmail = {
                    ...message,
                    content: null
                };

                await mailer.doSendMessage(invalidEmail);
            } catch(err) {
                err.should.not.be.eql(null);
            }

            try {
                const invalidEmail = {
                    from        : 'noreply@edgecommonmailer.com',
                    text        : 'This is test email',
                };

                await mailer.doSendMessage(invalidEmail);
            } catch(err) {
                err.should.not.be.eql(null);
            }

            try {
                const invalidEmail = {
                    to          : 'edge.tester01@yopmail.com',
                    subject     : 'EdgeCommonMailer Test',
                    text        : 'This is test email',
                };

                await mailer.doSendMessage(invalidEmail);
            } catch(err) {
                err.should.not.be.eql(null);
            }

            try {
                const invalidEmail = {
                    to          : 'edge.tester01@yopmail.com',
                    from        : 'noreply@edgecommonmailer.com',
                    text        : 'This is test email',
                };

                await mailer.doSendMessage(invalidEmail);
            } catch(err) {
                err.should.not.be.eql(null);
            }
        });


        it('should succeed', async () => {
            const result = await mailer.doSendMessage(message);
            result[0].should.have.property('statusCode', 202);
        });

        it('should succeed with template', async () => {
            const withTemplate = {
                ...message,
                mdTemplate: sampleMarkdown
            };

            const result = await mailer.doSendMessage(withTemplate);
            result[0].should.have.property('statusCode', 202);
        });

        it('should succeed with template with online image', async () => {
            const withTemplate = {
                ...message,
                mdTemplate: withOnlineImage
            };

            const result = await mailer.doSendMessage(withTemplate);
            result[0].should.have.property('statusCode', 202);
        });

        it('should succeed with template with image', async () => {
            const withTemplate = {
                ...message,
                mdTemplate: withImage
            };

            const result = await mailer.doSendMessage(withTemplate);
            result[0].should.have.property('statusCode', 202);
        });

        it('should succeed with template with table', async () => {
            const withTemplate = {
                ...message,
                mdTemplate: tableMarkdown
            };

            const result = await mailer.doSendMessage(withTemplate);
            result[0].should.have.property('statusCode', 202);
        });
    });

});
