import * as yargs         from 'yargs'
import {
    addServiceUser,
    DEFAULT_SERVICE_USER
}                         from '../src/utility/AddServiceUser';

const args = yargs
    .option('apiUser', {
        alias: 'u',
        describe: "Username to add",
        default: DEFAULT_SERVICE_USER.apiUser
    })
    .option('apiPassword', {
        alias: 'p',
        description: "Password for the username",
        default: DEFAULT_SERVICE_USER.apiPassword
    }).option('help', {
        alias: 'h',
        description: "Show help",
        epilog: 'copyright 2019'
    }).argv;

addServiceUser(args)
    .then((res) => {
        console.log('Finished adding service user!!!');
    })
    .catch((err) => {
        console.log(err);
    });
