/*

Create a simple web server that compiles the Ninja files into
distribution files and allows dynamic testing.

*/

//
// General configuration and error handlers
import NinjaWebServer from './NinjaWebServer';

const server = new NinjaWebServer();