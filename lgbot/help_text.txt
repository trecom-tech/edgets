lg currently supports the following

Display the items pending in work queues

    lg workqueues
    lg wq
    lg wqeta

Call any api

    lg call <provider> <api> <params...>
    lg call workqueue addrecord v2-import 123451

Listing files in Azure

    lg logs ls
    lg logs ls <path>
    lg logs ls <path>/<Regexp>
    lg logs get <path>/<Regexp>

    Examples:

    lg logs ls
    lg logs ls 'job_output_2018-04-25/Geo'
    lg logs get 'job_output_2018-04-25/lgv3node9_02_22_17_1_Hourly_Import_Offer'

Get data from the Database

    lg get <dataSet> <path>

    Examples:

    lg get rets_raw /raw/server_id:51
    lg get rets_raw /raw/server_id:51 --limit 5
    lg get rets_raw /raw/server_id:51 --start 5 --limit 5
    lg get rets_raw /raw/server_id:51 --start 5 --limit 5 --json

    Limit the fields downloaded

    lg get rets_raw /raw/server_id:51 --json --start 0 --limit 100 --field id --field processed

    Using jq, get just the list of ID values

    lg get rets_raw /raw/server_id:51 | jq "[.[]|{id}]"
    lg get rets_raw /raw/server_id:51 --json --start 0 --limit 100 --field id | jq ".[]|{id}" | jq -r '.id' | xargs -n 1 coffee test_export.coffee --id

    lg get rets_raw /raw/server_id:51 --json --start 0 --limit 1000000 --field id > all51.json

    cat idlist.txt | xargs -n 1 lg call workqueue addrecord export-needed

Load data to the Database

    lg load
    lg load <path>

    Examples: 

    lg load
    lg load os
    lg load os/jobs

    You can also specify dataset and collection with a command below.

    lg load test.json --dataset os --collection jobs

Export data from the Database

    lg export <dataset> <collection>

    Examples:
    
    lg export os jobs
    lg export os jobs --path ~/

Get data from REDIS Cache

    lg cacheget key

    Examples:
    lg cacheget TST1

Access other redis commands

    lg redis info 

Other examples of API Calls:

    lg call company doForceLoadPropertyImages TRI902453
