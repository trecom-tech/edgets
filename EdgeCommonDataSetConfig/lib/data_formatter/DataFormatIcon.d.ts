import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatIcon extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    format(data: any, options: any, path: any): any;
    unformat(data: any, path: any): any;
}
export default DataFormatIcon;
