"use strict";
/**
 * Handles configuration for a given DataSetManager
 * This classes is created as a member of a DataSetManager
 * to hold the available configurations for that specific dataset
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const edgeapi_1 = require("edgeapi");
// How long to use the same transform rules before reloading
// 1000 * 1000 = 1 second
let MAX_TRANSFORM_TIME = 1000 * 1000 * 60 * 5;
class DataSetTransformObject {
    constructor(data, isDebug) {
        this.data = data;
        this.isDebug = isDebug;
        this.basePointer = this.data;
        this.baseOutput = this.data;
        if (this.isDebug == null) {
            this.isDebug = false;
        }
        if (this.isDebug) {
            this.debug = [];
        }
    }
    set(name, val) {
        if (this.isDebug) {
            this.debug.push({ type: 'set', name, val });
        }
        if (val == null || val === '') {
            delete this.baseOutput[name];
        }
        else {
            this.baseOutput[name] = val;
        }
        return true;
    }
    get(name) {
        if (this.isDebug) {
            this.debug.push({ type: 'get', name, val: this.basePointer[name] });
        }
        if (this.basePointer[name] != null) {
            return this.basePointer[name];
        }
        return null;
    }
    resetBase() {
        // set all pointers back to the start
        this.basePointer = this.data;
        this.baseOutput = this.data;
        return true;
    }
    setWith(subPath) {
        let parts = subPath.replace('/', '.').split('.');
        for (let name of Array.from(parts)) {
            if (!this.basePointer[name]) {
                this.basePointer[name] = {};
            }
            this.basePointer = this.basePointer[name];
        }
        return true;
    }
    setOutputTo(subPath) {
        let parts = subPath.replace('/', '.').split('.');
        for (let name of Array.from(parts)) {
            if (!this.baseOutput[name]) {
                this.baseOutput[name] = {};
            }
            this.baseOutput = this.baseOutput[name];
        }
        return true;
    }
    parseCurrency(val) {
        console.log('PARSE CURRENCY: ', val);
        return process.exit(0);
    }
    searchReplace(patternText, replaceText, sourceValue) {
        if (sourceValue == null) {
            return null;
        }
        if (replaceText == null) {
            return null;
        }
        let re = new RegExp(patternText, 'i');
        let str = sourceValue.toString();
        let m = str.match(re);
        if (m != null) {
            for (let x = 1; x <= 10; x++) {
                if (m[x] != null) {
                    replaceText = replaceText.replace(`$${x}`, m[x]);
                }
            }
            if (/^[0-9\.]+$/.test(replaceText)) {
                return parseFloat(replaceText);
            }
            return replaceText;
        }
        return null;
    }
}
class DataSetConfig {
    constructor(ds, tableName) {
        this.ds = ds;
        this.tableName = tableName;
        this.settings = null;
        this.settings = null;
        this.configCollection = null;
        this.strDebug = '';
    }
    debug(line) {
        return (this.strDebug += line);
    }
    //
    // Returns a settings value or false if not set
    check(flagName) {
        if (this.settings != null && this.settings[flagName] != null) {
            return this.settings[flagName];
        }
        return false;
    }
    //
    // Transform an object by applying the rules to it
    doTransformObject(path, obj, isDebug) {
        return __awaiter(this, void 0, void 0, function* () {
            if (isDebug == null) {
                isDebug = false;
            }
            let e;
            edgecommonconfig_1.config.status('doTransformObject Transform Path     :', path);
            edgecommonconfig_1.config.status('doTransformObject Transform Incoming :', obj);
            let recordHashStart = edgeapi_1.default.getHash(obj);
            this.internalCheckTransformExpired();
            if (!this.transform) {
                yield this.doLoadTransforms();
            }
            if (this.transform.code == null) {
                edgecommonconfig_1.config.status('doTransformObject no transform code');
                return true;
            }
            if (this.transform.blocks == null) {
                try {
                    this.transform.blocks = Function('obj', this.transform.code);
                }
                catch (error) {
                    e = error;
                    console.log(`Error in Config code for dataset ${this.ds.databaseName}`, this.transform.code);
                    edgecommonconfig_1.config.reportError('DataSetConfig doTransformObject:', e);
                    process.exit(0);
                }
            }
            if (isDebug) {
                obj.rules = [];
            }
            let rule = this.transform.blocks;
            let tempObject = new DataSetTransformObject(obj, isDebug);
            try {
                rule(tempObject);
                if (isDebug) {
                    obj.rules.push(tempObject.debug);
                }
            }
            catch (error1) {
                e = error1;
                edgecommonconfig_1.config.reportError('Rule error', e);
                console.log('Rule error: ', e);
            }
            let recordHashEnd = edgeapi_1.default.getHash(obj);
            if (recordHashEnd !== recordHashStart) {
                //
                // Hash has changed due to rules
                return true;
            }
            // console.log "Transformed object path=", path, "obj=", obj
            // config.dump "Transformed", path, obj
            // process.exit(0)
            return false;
        });
    }
    //
    // Copy command
    // Takes a value from one name and moves it directly to another
    // requires:  sourceField, targetField
    //
    applyCommandCopy(obj, command) {
        let basePointer = obj;
        if (command.subPath != null && command.subPath.length > 0) {
            for (let name of Array.from(command.subPath)) {
                if (basePointer[name] != null &&
                    typeof basePointer[name] === 'object') {
                    basePointer = basePointer[name];
                }
                else {
                    console.log(`Did not find ${name} in object`);
                    return false;
                }
            }
        }
        if (basePointer[command.sourceField] != null) {
            // console.log "Copy #{command.targetField} value ", basePointer[command.sourceField]
            basePointer[command.targetField] = basePointer[command.sourceField];
            delete basePointer[command.sourceField];
        }
        return true;
    }
    doGetTransformRule(ruleName) {
        return __awaiter(this, void 0, void 0, function* () {
            this.internalCheckTransformExpired();
            if (!this.transform) {
                yield this.doLoadTransforms();
            }
            if (this.transform != null && this.transform[ruleName] != null) {
                return this.transform[ruleName].xml;
            }
            return null;
        });
    }
    //
    // Add/Change transform rules
    doSetTransformRule(ruleName, ruleXml) {
        return __awaiter(this, void 0, void 0, function* () {
            this.internalCheckTransformExpired();
            if (!this.transform) {
                yield this.doLoadTransforms();
            }
            this.transform[ruleName] = { xml: ruleXml };
            return this.transformCollection.doUpsertOne(this.tableName, this.transform);
            //
            // TODO:  Flag or something so we know it needs to be converted to code
            //
        });
    }
    /**
     * Load the settings and translations
     */
    doLoadSettings() {
        if (this.settingsPromise) {
            return this.settingsPromise;
        }
        return (this.settingsPromise = new Promise((resolve, reject) => {
            // @configCollection = @ds.db.collection "_config"
            // @settings = yield @configCollection.doFindByID @tableName, true
            // @internalVerifySettings()
            this.settings = {
                track_changes: true,
                active: true,
            };
            resolve(this);
        }));
    }
    //
    // Check to see if the transform rules are older than 10 minutes
    // if so, flush them from memory.
    // TODO:  Add a flag so that the tranform rules are reloaded
    // if a change notification comes from the status server.
    //
    internalCheckTransformExpired() {
        if (this.transformDateLoaded == null) {
            return false;
        }
        let now = new Date();
        if (now.getTime() - this.transformDateLoaded.getTime() >
            MAX_TRANSFORM_TIME) {
            //
            // Drop the existing values from doLoadTranform
            delete this.transformPromise;
            delete this.transformCollection;
            delete this.transform;
            delete this.transformDateLoaded;
        }
        return true;
    }
    doLoadTransforms() {
        if (this.transformPromise) {
            return this.transformPromise;
        }
        this.transformPromise = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            this.transformCollection = this.ds.db.collection('_transform');
            this.transform = yield this.transformCollection.doFindByID(this.tableName, true);
            this.transformDateLoaded = new Date();
            // console.log @tableName, "TRANSFORM LOADED:", @transform
            // process.exit()
            resolve(this);
        }));
        return this.transformPromise;
    }
    //
    // Look at the values and see if they are logical and initialized
    //
    internalVerifySettings() {
        let needsave = false;
        if (this.settings == null) {
            this.settings = {};
            needsave = true;
        }
        if (this.settings['track_changes'] == null) {
            this.settings['track_changes'] = true;
            needsave = true;
        }
        if (this.settings['active'] == null) {
            this.settings['active'] = true;
            needsave = true;
        }
        if (needsave) {
            return this.save();
        }
        return null;
    }
    //
    // Save back to server
    // This is not a promise function because we can allow this in the background
    save() {
        // @configCollection.doUpsertOne @tableName, @settings
        return true;
    }
}
exports.DataSetConfig = DataSetConfig;
exports.default = DataSetConfig;
//# sourceMappingURL=DataSetConfig.js.map