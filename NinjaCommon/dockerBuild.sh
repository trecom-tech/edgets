echo Building CoffeeNinja:latest

export DOCKER_ID_USER="$*"
docker build -t coffee-ninja:latest .
sudo docker tag coffee-ninja $DOCKER_ID_USER/coffee-ninja
sudo docker push $DOCKER_ID_USER/coffee-ninja
