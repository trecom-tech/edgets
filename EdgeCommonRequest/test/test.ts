import { EdgeRequest } from '../src/EdgeRequest';

const should = require('should');

// See https://httpbin.org/
// baseUrl = 'http://httpbin.org'
const baseUrl = 'https://httpbin.org';

describe("Simple GET request with Query String in URL", () =>
    it("should pull the query string out of the url", function () {
        const request = new EdgeRequest();
        return request.doGetLink(`${baseUrl}/get?show_env=1`, null)
            .then(function (data: any) {
                should.exist(data);
                return data.headers.should.have.property("Host", "httpbin.org");
            });
    })
);


describe("Test Digest Authentication in httpbin.org", () =>
    it('should handle Digest authentication', function () {
        const request  = new EdgeRequest();
        const qop      = "auth";
        const name     = "Chris";
        const password = "123456";
        request.setUsername(name, password);
        return request.doGetLink(`https://httpbin.org/digest-auth/${qop}/${name}/${password}`, {})
            .then((data: any) => should.exist(data));
    })
);

describe("Test Basic Authentication in httpbin.org", () =>
    it('should handle basic authentication', function () {
        const request  = new EdgeRequest();
        const name     = "Chris";
        const password = "123456";
        return request.doGetLink(`https://${name}:${password}@httpbin.org/basic-auth/${name}/${password}`, {})
            .then(function (data: any) {
                should.exist(data);
                data.should.have.property('authenticated', true);
                data.should.have.property('user', 'Chris');
            });
    })
);

describe("Simple POST request", () =>
    it('should handle a simple POST', function () {
        const request = new EdgeRequest();
        request.setMethod("POST");
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/post`, testObject)
            .then(function (data: any) {
                should.exist(data);
                should.exist(data.form);
                data.form.should.have.property('age', '40');
                data.form.should.have.property('name', 'Brian Pollack');
                data.headers.should.have.property('Content-Type', 'application/x-www-form-urlencoded');
                data.headers.should.have.property("Host", "httpbin.org");
            });
    })
);

describe("Simple JSON Request", () =>

    describe("Simple GET Test", function () {

        it('Should handle a simple GET', function () {
            const request    = new EdgeRequest();
            const testObject = { age: 40, name: "Brian Pollack" };
            return request.doGetLink(`${baseUrl}/get`, testObject)
                .then(function (data: any) {
                    should.exist(data);
                    should.exist(data.args);
                    should.exist(data.headers);
                    data.args.should.have.property("age", "40");
                    data.args.should.have.property("name", "Brian Pollack");
                    data.headers.should.have.property("Host", "httpbin.org");
                    data.headers.should.have.property("Accept", "*/*");
                });
        });

        it('Should handle a simple GET with GZip', function () {
            const request    = new EdgeRequest();
            const testObject = { age: 40, name: "Brian Pollack" };
            return request.doGetLink(`${baseUrl}/gzip`, testObject)
                .then(function (data: any) {
                    should.exist(data);
                    should.exist(data.headers);
                    data.should.have.property("gzipped", true);
                    data.headers.should.have.property("Host", "httpbin.org");
                    data.headers.should.have.property("Accept", "*/*");
                });
        });

        return it('Should handle a simple GET with Deflate', function () {
            const request    = new EdgeRequest();
            const testObject = { age: 40, name: "Brian Pollack" };
            return request.doGetLink(`${baseUrl}/deflate`, testObject)
                .then(function (data: any) {
                    should.exist(data);
                    should.exist(data.headers);
                    data.should.have.property("deflated", true);
                    data.headers.should.have.property("Host", "httpbin.org");
                    data.headers.should.have.property("Accept", "*/*");
                });
        });
    })
);


describe("Additional content types", function () {

    it('Should handle XML Output', function () {
        const request    = new EdgeRequest();
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/xml`, testObject)
            .then(function (data: any) {
                should.exist(data);
                should.exist(data.slideshow);
                should.exist(data.slideshow['$']);
            });
    });


    it('Should be buffer for image/png', function () {
        const request    = new EdgeRequest();
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/image/png`, testObject)
            .then(function (data: any) {
                should.exist(data);
                Buffer.isBuffer(data).should.equal(true);
            });
    });

    it('Should be buffer for image/webp', function () {
        const request    = new EdgeRequest();
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/image/webp`, testObject)
            .then(function (data: any) {
                should.exist(data);
                Buffer.isBuffer(data).should.equal(true);
            });
    });

    it('Should be buffer for image/jpeg', function () {
        const request    = new EdgeRequest();
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/image/jpeg`, testObject)
            .then(function (data: any) {
                should.exist(data);
                Buffer.isBuffer(data).should.equal(true);
            });
    });

    return it('Should be buffer for image/svg', function () {
        const request    = new EdgeRequest();
        const testObject = { age: 40, name: "Brian Pollack" };
        return request.doGetLink(`${baseUrl}/image/svg`, testObject)
            .then(function (data: any) {
                should.exist(data);
                Buffer.isBuffer(data).should.equal(true);
            });
    });
});