## -------------------------------------------------------------------------------------------------------------
##
class ModalForm

	# @property [String] content the content of the modal defualt is Default Content
	content:      ""

	# @property [String] title the title of the modal dialog default is Default Title
	title:        "Default title"

	# @property [String] position the position of the modal to display default is top
	position:     'top'

	setForm: (callbackWithForm)=>
		return @getViewContainer().setView "Form", (@viewForm)=>
			@viewForm.removeAbsolute()
			@viewForm.setNoResize()
			if callbackWithForm? then callbackWithForm(@viewForm)

	## -------------------------------------------------------------------------------------------------------------
	## constructor
	##
	## @param [Object] options any valid property can be used inside object
	##
	constructor:  (options) ->

		@gid = GlobalValueManager.NextGlobalID()

		##
		##  Possibly overwrite default options
		if options.position?
			@position = options.position

		if options.title?
			@title = options.title

		if options.content?
			@content = options.content

		@createModal()

		if !options.buttons?
			options.buttons = [
				type: "submit"
				text: options.ok || "OK"
			]

		@setForm (@viewForm)=>
			@viewForm.once "close", ()=>
				# console.log "Form Close"
				@hide()

			@viewForm.deserialize(options)

		@show()
		true


	##|
	##|  Create the modal window and set the view
	createModal: ()=>
		@modalContainer = new WidgetTag "div", "modal", "modal#{@gid}",
			"tabindex" 		: 	"-1"
			"role" 			: 	"dialog"
			"aria-hidden" 	: 	"true"
		@modalContainer.css "display", "none"
		@modalWrapper = @modalContainer.addDiv("modal-dialog").addDiv("modal-content")
		@header = @modalWrapper.addDiv "modal-header"
		# @buttonInHeader = @header.add "button", "close", "button-#{@gid}",
		# 	"data-dismiss" : 	"modal"
		# 	"aria-label"   :	"Close"
		# @spanInButton = @buttonInHeader.add("span", "", "", {"aria-hidden": "true"}).getTag().html '&times;'
		@header.add("h4", "modal-title").text("#{@title}")
		@body = @modalWrapper.addDiv "modal-body"
		@contentWrapper = @body.add("p")
		@contentWrapper.html "#{@content}"
		@viewContainer = @body.addDiv "modal-view", "modal-view#{@gid}"

	## -------------------------------------------------------------------------------------------------------------
	## function to execute on the close of the modal
	##
	## @event onClose
	## @return [Boolean]
	##
	onClose: () =>
		true

	## -------------------------------------------------------------------------------------------------------------
	## function to hide the modal
	##
	##
	hide: () =>
		@modal.modal('hide')

	## --gao
	## functioin to get container for view
	##
	getViewContainer: ()=>
		@viewContainer

	## -------------------------------------------------------------------------------------------------------------
	## function to show the modal
	##
	## @param [Object] options options to be used in showing the modal
	## @return [Boolean]
	##
	show: (options) =>

		$("body").append @modalContainer.getTag()

		@modal = $("#modal#{@gid}")
		@modal_body = @modal.find(".modal-body")
		@modal.modal(options)
		@modal.on "hidden.bs.modal", () =>
			##|
			##|  Remove HTML from body
			@modal.remove()

			##|
			##|  Call the close event
			@onClose()
			true

		##|
		##| -------------------------------- Position of the dialog ---------------------------

		if @position == "center"

			@modal.css
				'margin-top' : () =>
					Math.max(0, ($(window).scrollTop() + ($(window).height() - @modal.height()) / 2 ))
