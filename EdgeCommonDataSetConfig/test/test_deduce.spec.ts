import * as DataSetConfig from '../src';
const should = require('should');

const { Column } = DataSetConfig;

describe.skip("deduceInitialColumnType works fine", function() {
    it("should be deduced with date", function() {
        const testColumnData1 = { 
            name    : "Date",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };

        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();


        c.getType().should.equal('timeago');
    });

    it("should be deduced with Price", function() {
        const testColumnData1 = { 
            name    : "Good Price",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };

        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();


        c.getType().should.equal('money');
        c.getWidth().should.equal(90);
        c.getAlign().should.equal('right');
    });

    it("should be deduced with boolean", function() {
        const testColumnData1 = { 
            name    : "IsActive",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();

        c.getType().should.equal('boolean');
        c.getWidth().should.equal(60);
        c.getAlign().should.equal('left');
    });        

    it("should be deduced with distance", function() {
        const testColumnData1 = { 
            name    : "distance",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();

        c.getType().should.equal('distance');
        c.getWidth().should.equal(66);
        c.getAlign().should.equal('left');
    });

    it("should be deduced with id", function() {
        const testColumnData1 = { 
            name    : "id",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country_name",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();

        c.getType().should.equal('text');
        c.getAlign().should.equal('left');  
        c.getName().should.equal('ID');
        c.getVisible().should.equal(false);
    });

    it("should be deduced with lat or lon", function() {
        const testColumnData1 = { 
            name    : "lat-lon",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "lat",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();

        c.getType().should.equal('decimal');
        c.getAlign().should.equal('right');  
        return c.getOptions().should.equal('#.######');
    });

    it("should be deduced with memo", function() {
        const testColumnData1 = { 
            name    : "memo",
            type    : "text",
            width   : 80,
            editable: true,
            visible : true,
            source  : "country",
            autosize: true,
            cellColor  : `\
console.log("Dummy Function");
return("red");\
`
        };
        const c = new Column();

        c.deserialize(testColumnData1);

        c.deduceInitialColumnType();

        c.getType().should.equal('memo');
        c.getAlign().should.equal('left');
    });
});  
