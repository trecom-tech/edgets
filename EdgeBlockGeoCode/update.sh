#!/bin/bash

npm version minor
npm run build
git add lib/*js lib/*map
git commit -a -m "$*"
git push
