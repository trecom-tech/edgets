## Description

The **EdgeApi module allows access to caching and data from the Edge Microserver System.  The API can also allows
the caller to register a new microservice within the Edge API Server.

## Important Notes

The server that the EdgeApi connects to is currently specified in the EdgeCommonConfig module using the standard
credentials manager system.  This module was designed initially to seperate the connection from the usage of the
connection so that the same code in EdgeApiBase and EdgeApiProvider can be used in the browser and command line.

This version of the module is server side only and needs to be merged with the browser side module.

## Using the API

Create a new instance of EdgeApi() or use the static EdgeApi.doGetApi() to return a single shared instance.

Once you have an instance, call doConnect() to connect to the server.  This will also get a list of APIs supported
by the server and allocated functions for them in the local object automatically.

If the API server, for example, supports "data_doFind" then you would have api.data_doFind() defined as a
function automatically in the api instance variable.

### Accessing a remote server

You can request a connection to a sub node or a different server using doGetApiForServer which accepts a server name or an array of server names.

    testServer = "http://localhost:8001"
    EdgeApi.doGetApiForServer(testServer)
    .then((remoteApi)->{
    	// api.os_doGetStat();
    });

## EdgeAPI Functions

TODO: Document these API Calls
------------------------------

* cacheGet
* cacheSet
* cacheHGetAll
* cacheMSet
* cacheSetAdd

Helper functions with objects
-----------------------------

* @cleanupObject (obj) - Removes undefined and null values
* @getHash (obj) - Gets a crypto hash from an object members
* @flattenObject: (obj) - Converts an object into a "flat" object such as user.address becomes user_address
* @deepMergeObject (objTarget, objSrc, addAttributes, deleteAttributes) -> Merges values from source into target
* @cloneObject (obj) - Returns a clone from an object or value not a reference copy
* @dumpObject (obj, saveFile, theName) - Save an object to a filename for debugging
* @fixupJson (obj) - Convert text to numbers and fix dates

TODO: Broken functions remove or replace
----------------------------------------

These functions use DataSetManager to directly hit the database and
they really should be hitting the API Server and run running in this api.

* @doQuickFind: (dataSet, tableName, condition, sort, callback)
* @doAggregate : (dataSet, tableName, aggregateQuery, options) =>
* @doAggregateCount: (dataSet, tableName, fieldList, matchCondition, sortCondition, limit) =>

## Testing

You need to run EdgeApiServer locally to test EdgeApi. Please check how to run server from EdgeApiServer documentation.
Run `docker-compose up` for EdgeApiServer and `npm start` to start independent server.
And run `npm run test-local` to test EdgeApi.