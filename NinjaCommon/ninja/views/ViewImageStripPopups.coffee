##|
##|  To help with automated testing
globalImageCounter = 0

class ViewImageStripPopups extends View

	## ------------------------------------------------------------------------------------------
	## function to set data of image sources
	##
	setImgData: (@imageData)=>
		return @imageData
	
	##
	## default function of class View that is necessary
	##
	onSetupButtons: () =>

	##
	## default function of class View
	##    
	setTitle: (@title)=>

	##|
	##|  Loop through all thumbnails and give them a valid height
	resizeThumbnails: (w, h)=>
		# -gao
		# change direction of thumbnails

		if w > h
			ulWidth = 0
			@mainHolder.addClass "image-strip-popups-horizontal"
			@thumbsList.height h - @scrollbarSize
			imgHeight = h - @scrollbarSize - @numberBarSize - @vMargin

			for idx, data of @imageData
				imgWidth = imgHeight / data.h * data.w				
				data.li.addClass "horizontal"
				data.li.height h - @scrollbarSize - @vMargin
				data.li.outerWidth imgWidth + 2
				
				data.elImage.height imgHeight
				data.elImage.outerWidth imgWidth

				data.elNumber.outerWidth imgWidth
				ulWidth += (imgWidth + @hMargin + 2)
			@thumbsList.width ulWidth
		else
			ulHeight = 0
			@mainHolder.removeClass "image-strip-popups-horizontal"
			@thumbsList.width w - @scrollbarSize
			imgWidth = w - @scrollbarSize - @hMargin

			for idx, data of @imageData
				imgHeight = imgWidth / data.w * data.h				
				data.li.removeClass "horizontal"
				data.li.outerWidth imgWidth + 2
				data.li.height imgHeight + @numberBarSize

				data.elImage.outerWidth imgWidth
				data.elImage.height imgHeight

				data.elNumber.outerWidth imgWidth
				ulHeight += (imgHeight + @numberBarSize + @vMargin)
			@thumbsList.height ulHeight
		@resizeSelectedImage()
		true

	resizeSelectedImage: ()=>

		totalHeight = @height()
		currentTop  = @scrollTop()

		for idx, data of @imageData
			if "#{idx}" == "#{@selectedImgNumber}"
				data.li.addClass "selected"
				##|
				##|  scroll into view only if not already in view
				if (totalHeight - data.li.offsetTop() + currentTop < 0)
					data.li.element.scrollIntoView(true)
				else if (data.li.offsetTop() - currentTop < 0)
					data.li.element.scrollIntoView(false)
			else
				data.li.removeClass "selected"

			true

	##
	## function to reset image data list
	resetImageData: () =>
		if @imageData?
			@imageData = []
		@thumbsList?.el?.empty()
		true

	## ------------------------------------------------------------------------------------------
	## function to add an image source data
	##    
	addImage: (image)=>
		unless image? then return

		if !@thumbsList?
			setTimeout @addImage, 100, image
			return

		if !@imageData? then @imageData = []

		if typeof image == "object" and image.src?
			image = image.src

		##|
		##|  Load a new image
		img = new Image()
		img.onload = (o)=>
			fileNameArray = img.src.split('/').pop().split('.')
			if fileNameArray.length > 1 then fileNameArray.splice(-1)
			fileName = fileNameArray.join('.')

			imageDataOne =
				src 		: img.src
				fileName 	: fileName 
				w  			: img.naturalWidth
				h  			: img.naturalHeight

			##|
			##|  Create thumbnail holder
			##|
			imageDataOne.li = @thumbsList.add "li", "imagethumb"
			imageDataOne.elImage = imageDataOne.li.add "img"
			imageDataOne.elImage.el.attr("src", img.src)

			imageDataOne.li.css "cursor", "pointer"
			imageDataOne.li.el.attr "data-id", @imageData.length

			addTabToPopup = (view) =>
				tabName = parseInt(imageDataOne.li.el.attr("data-id")) + 1
				tab     = view.addTab tabName
				view.addCloseButton tabName
				tab.body.setView "ImageAnnotation", (view) =>
					view.setImage imageDataOne.src 
					view.setDataPath @tableName, "#{@path}_#{tab.name}"
					true

				view.show tab.id

			# -gao
			# bind click event that will open a PopupViewOnce with this image 
			imageDataOne.li.bind "click", (e)=>
				@setSelectedImgNumber parseInt($(e.currentTarget).attr("data-id"))
				popupName = "image-popup-#{@title}"
				popupView = PopupViews[@title]
				if !popupView
					for w in globalOpenWindowList 
						if w.configurations?.tableName is popupName
							popupView = w
				if !popupView
					popupViewWidth  = if imageDataOne.w < $(window).width() * 0.8 then imageDataOne.w else $(window).width() * 0.8
					popupViewHeight = popupViewWidth * imageDataOne.h / imageDataOne.w + 62
				else
					originalWidth  = popupView.popupWidth
					originalHeight = popupView.popupHeight - 62
					ratio1         = imageDataOne.h / originalHeight
					ratio2         = imageDataOne.w / originalWidth
					if ratio1 > ratio2
						popupViewWidth  = originalHeight * (imageDataOne.w / imageDataOne.h)
						popupViewHeight = originalHeight + 62
					else
						popupViewWidth  = originalWidth
						popupViewHeight = originalWidth * (imageDataOne.h / imageDataOne.w) + 62

				if !popupView
					doPopupView "DynamicTabs", "#{@title}", popupName, popupViewWidth, popupViewHeight, (view, tabText) ->
						addTabToPopup view 
				else 
					popupView.open()
					addTabToPopup popupView.view

			imageDataOne.elNumber = imageDataOne.li.addDiv "number_body"
			imageDataOne.elNumber.html @imageData.length+1

			@imageData.push imageDataOne
			@resizeThumbnails()

		img.onerror = (o)=>
			console.log "ViewImageStrip addImage error:", image, o

		img.src = image
		return true

	##
	## calculate new image size for new container size
	@calculateImgSize: (elImg, imgW, imgH, containerW, containerH) =>
		ratio1  = imgW / containerW 
		ratio2  = imgH / containerH 
		if ratio1 < ratio2 
			newWidth  = containerH * (imgW / imgH)
			newHeight = containerH
		else 
			newWidth  = containerW 
			newHeight = containerW * (imgH / imgW)
		elImg.attr 'width', newWidth
		elImg.attr 'height', newHeight
		true

	##|
	##|  Create the HTML Controls that make this work
	createControls: ()=>

		if @thumbsList? then return
		@mainHolder = @add "div", "image-strip-popups-main"
		@thumbsList = @mainHolder.add "ul", "scroll-list"
		@mainHolder.setScrollable()
		@mainHolder.onResize = @resizeThumbnails
		@selectedImgNumber = 0


	##
	## function to change width and height of the view when it is resized
	##
	onResize: (w, h) =>
		#super(w,h)
		@mainHolder.onResize w, h
		true

	onShowScreen: () =>
		globalTableEvents.on "note_added", @handleAnnotationUpdates

	setData: (data)=>
		##|
		##|  Data is called upon initialize of the View, data values can be added optionally is setView call
		@title = "ImageStripPopups"
		@createControls()
		# -gao
		# constants used to calculate elements' size
		# scrollbarsize depends on OS
		@scrollbarSize = 15
		@numberBarSize = 27
		@vMargin = 12
		@hMargin = 8		

		if data? and Array.isArray(data)
			@setImgData data
		else
			@setImgData []

		@selectedImgNumber = 0
		true
	##
	## Make changes per updates in image annotations linked to this view
	handleAnnotationUpdates: (tableName, idValue, fieldName, notes) =>
		# console.log "handleAnnocationUpdates(#{tableName}, #{idValue}, #{fieldName})"
		if !@tableName or !@path then return false

		parts = @path.split '/'
		if tableName isnt parts[1] or idValue isnt parts[2] or fieldName.indexOf(parts[3]) < 0
			return false

		try 
			imgNumber = parseInt(fieldName.split('_').pop()) - 1
			@imageData[imgNumber].elNumber.html "#{imgNumber + 1} <span class='number_body_annotation'><i class='si si-note'> #{Object.keys(notes).length}</span>"
		catch e
			console.log "ImageStripPopup: Error in updating title of image", e
		true

	##
	## function to set currently selected image's number
	##
	setSelectedImgNumber: (number)=>
		if number? and 0 <= number and number < @getImageCount()
			@selectedImgNumber = number
			@resizeSelectedImage()
		else
			return false
	
	##
	## function to get currently selected image number
	##
	getSelectedImgNumber: ()=>
		@selectedImgNumber

	##
	## function to get number(counted) of images
	##
	getImageCount: ()=>
		@imageData.length

	## set data path to where notes' data is saved
	setDataPath: (@tableName, @path) =>
		if !@tableName or !@path then return false
		@isSetDataPath = true
		true

	serialize: () =>
		if !@imageData? then return null
		images = @imageData.map (img) ->
			return img.src
		return
			title:  @title
			images: images
			table:  @tableName
			path:   @path

	deserialize: (options) =>
		new Promise (resolve, reject) =>
			if !options? or typeof options isnt "object"
				reject "ViewImageStripPopups: invalid data passed"

			if options.title?
				@setTitle options.title

			for item in options.images or []
				@addImage item

			if options.table? and options.path?
				@setDataPath options.table, options.path

			resolve true
