import { config } from 'edgecommonconfig';
const Influx = require('influx');

let globalIsDelayed: number = 0;

export class TimeSeriesDatabase {
    pendingPoints: Array<object>;
    pendingTimer: NodeJS.Timeout;

    constructor(
        public parent: EdgeTimeseriesData,
        public databaseName: string,
    ) {
        this.pendingPoints = [];
        this.pendingTimer = null;
    }

    /**
     * Send the pending data
     */

    sendTimer() {
        if (this.parent == null || this.parent.client == null) {
            this.pendingTimer = null;
            this.pendingPoints = [];
            return false;
        }

        this.parent.client
            .writePoints(this.pendingPoints, {
                database: this.databaseName,
                precision: 's',
            })
            .then(
                (): any => {
                    this.pendingTimer = null;
                    return (this.pendingPoints = []);
                },
            )
            .catch(
                (ex: Error): any => {
                    let log = config.getLogger('TimeSeriesData2');
                    log.error('ExcpetionInWritePoints2', {
                        database: this.databaseName,
                        points: this.pendingPoints,
                        ex,
                    });

                    this.pendingPoints = [];
                    return (this.pendingTimer = null);
                },
            );

        return true;
    }

    /**
     * Change to doSaveMeasurement for those that are approved during the testing phase.
     * @param measurement
     * @param tags
     * @param data
     */
    async doSaveMeasurement(measurement: string, tags: any, data: any) {
        if (this.parent.client == null) {
            return false;
        }

        // tslint:disable-next-line:no-control-regex
        let spaceReg = new RegExp('[ /\n\r]', 'g');
        measurement = measurement.replace(spaceReg, '');

        if (typeof tags !== 'object' || typeof data !== 'object') {
            data = {};
        }

        try {
            this.pendingPoints.push({
                measurement,
                tags,
                fields: data,
                timestamp: new Date(),
            });

            if (this.pendingTimer == null) {
                this.pendingTimer = setTimeout(this.sendTimer, 10000);
            }

            return true;
        } catch (e) {
            console.log('doSaveMeasurement write exception:', e);
            console.log(
                `[${globalIsDelayed}] TimeSeriesDatabase doSaveMeasurement delay enabled on ${
                    this.databaseName
                } for `,
                measurement,
                data,
            );

            return false;
        }
    }
}

export default class EdgeTimeseriesData {
    static singleInstance: EdgeTimeseriesData = null;
    private log: any;
    private base: string = '';
    public client: any;
    private dbCache: any = {};

    static getInstance() {
        if (EdgeTimeseriesData.singleInstance === null) {
            EdgeTimeseriesData.singleInstance = new EdgeTimeseriesData();
        }

        return EdgeTimeseriesData.singleInstance;
    }

    constructor() {
        const credentials = config.getCredentials('influxdb');

        if (
            credentials == null ||
            credentials.type == null ||
            credentials.type !== 'influxdb'
        ) {
            config.error('InfluxDB credentials not found');
            return;
        } else if (credentials.type === 'influxdb') {
            this.client = new Influx.InfluxDB(credentials);
        } else {
            config.error('TimeSeries config found but missing type = influxdb');
        }

        this.base = `http://${credentials.host}/`;
        this.log = config.getLogger('Influx');
    }

    /**
     * Creates database only if it doesn't exist, returns success
     * @param databaseName
     */
    async doCreateDatabase(databaseName: string) {
        if (this.dbCache[databaseName] != null) {
            return this.dbCache[databaseName];
        }

        if (this.client != null) {
            try {
                let databases: string[] = await this.client.getDatabaseNames();

                if (!Array.from(databases).includes(databaseName)) {
                    return await this.client.createDatabase(databaseName);
                }

                if (this.dbCache[databaseName] == null) {
                    this.dbCache[databaseName] = new TimeSeriesDatabase(
                        this,
                        databaseName,
                    );
                }

                return this.dbCache[databaseName];
            } catch (e) {
                config.error('InfluxDB createDatabase exception: ', e.message);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retrieves all database names
     */
    async doGetDatabases() {
        if (this.client == null) {
            return null;
        }

        try {
            return await this.client.getDatabaseNames();
        } catch (e) {
            return config.error('InfluxDB getDatabases exception: ', e.message);
        }
    }

    /**
     * Retrieves all measurements per database name
     * @param databaseName
     * @param measurement
     * @param tags
     * @param data
     */
    async doWriteMeasurement(
        databaseName: string,
        measurement: string,
        tags: string,
        data: any,
    ) {
        if (this.client == null) {
            throw config.error(
                'InfluxDB doWriteMeasurement exception: ',
                'client is not initilized',
            );
        }

        try {
            await this.client.writePoints(
                [
                    {
                        measurement: measurement,
                        tags: tags,
                        fields: data,
                    },
                ],
                {
                    database: databaseName,
                    precision: 's',
                },
            );

            return true;
        } catch (e) {
            return config.error(
                'InfluxDB getMeasurements exception: ',
                e.message,
            );
        }
    }

    /**
     * Retrieves all measurements per database name
     * @param databaseName
     */
    async doGetMeasurements(databaseName: string) {
        if (this.client == null) {
            return null;
        }

        try {
            return await this.client.getMeasurements(databaseName);
        } catch (e) {
            return config.error(
                'InfluxDB getMeasurements exception: ',
                e.message,
            );
        }
    }

    /**
     * Execute a query and get result, use fields same as '*' or 'cpu, mem, disk',
     * as usual select operation in database queries
     * @param databaseName
     * @param sql
     */
    async doQuery(databaseName: string, sql: string) {
        if (this.client == null) {
            return null;
        }

        const opts = { database: databaseName };

        try {
            let results: any = {
                data: [],
            };

            const all = await this.client.query(sql, opts);
            for (const varName in all) {
                const value = all[varName];
                if (value.time != null) {
                    const d = new Date(value.time);
                    value.time = d;
                    results.data.push(value);
                }
            }

            return results;
        } catch (e) {
            return config.error('InfluxDB query exception: ', e.message);
        }
    }

    async dropDatabase(databaseName: string) {
        return this.client.dropDatabase(databaseName);
    }
}
