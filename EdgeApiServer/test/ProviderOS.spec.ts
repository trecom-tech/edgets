import EdgeApi       from 'edgeapi';

describe('ProviderOS', () => {

    let api: EdgeApi;
    // jest.useFakeTimers();

    // Act before assertions
    before(async () => {
        api = await EdgeApi.doGetApi();
    });

    it('os_doGetServerInformation', async () => {
        let result = await api.os_doGetServerInformation();
        result.should.have.property('free_mem');
    });

    it('doCreateOnetimeJob', async () => {
        let result = await api.os_doCreateOnetimeJob('Test', './working', 'script.js', '');

    });
});