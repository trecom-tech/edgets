class ViewDynamicTabs extends View
	tabHeight: 32
	getDependencyList: () =>
		return [ "/vendor/mousetrap.min.js" ]

	##|
	##| setSize (see NinjaContainer) is called when the holder wants this dynamic tabs
	##| to take up exactly some amount of space.  By default it will resize the tabs and
	##| then onResize is called once the size has actually changed
	##|
	# setSize: (w, h)=>

	onResize: (w, h)=>
		super(w, h)

		##|
		##|  Update the visible tab
		@updateTabs()

		true

	##|
	##|  total the badge values from all children
	getBadgeText: ()=>

		total = 0
		for id, tag of @tags
			if tag.body? and tag.body.getBadgeText?
				num = tag.body.getBadgeText()
				if typeof num != "number" then num = parseInt(num)
				total += num

		if total > 0 then return total
		return null

	## -------------------------------------------------------------------------------------------------------------
	## Add a new tab data to array named "tabData"
	##
	## @param [String] tabName the name of the tab to be adding
	## @param [String] defaultHtml optional html for the tab content
	## @param [Integer] order (optional) order of the tab that should >= 0, if not specified, set it to -1
	## @return [Tab] the new tab Object which is created
	##
	addTabData: (tabName, defaultHtml, order, icon) =>
		if !order? then order = -1
		if !icon? then icon = ""
		tab = {
			tabName,
			defaultHtml,
			order,
			icon
		}
		@tabData.push tab
		return tab

	##|
	##|  Internal helper function that calls onTabShow for a tag.
	internalCallTabShow: (tag)=>
		if !tag? then return false
		if !tag.body? then return false
		if tag.body.onTabShow?
			tag.body.onTabShow(tag)
			return true
		if tag.body.childView? and tag.body.childView.onTabShow?
			tag.body.childView.onTabShow(tag)
			return true
		return false

	##|
	##|  Internal helper function that calls onTabShow for a tag.
	internalCallTabHide: (tag)=>
		if !tag? then return false
		if !tag.body? then return false
		if tag.body.onTabHide?
			tag.body.onTabHide(tag);
			return true
		if tag.body.childView? and tag.body.childView.onTabHide?
			tag.body.childView.onTabHide(tag)
			return true
		return false

	refreshTagPosition: (tag) =>
		if !tag? or !tag.body? or !tag.body.move? then return false
		tag.body.move 0, @height()-@getInsideHeight(), @getInsideWidth(), @getInsideHeight()
		true

	updateTabs: ()=>

		for id, tag of @tags

			if id == @activeTab
				tag.tab.addClass "active"
				tag.body.show()

				# console.log "Showing tab with ", tag.body, @getInsideWidth(), @getInsideHeight()

				if @getInsideHeight() == 0
					tag.body.hide()
				else if tag.body.move?
					@refreshTagPosition tag

				##|
				##|  If the tab has an activate function, call it.   Also, if the last tab has a deactivate function, call it.
				try
					if @lastActiveTab != tag
						##|
						##|  Tab has been changed
						@internalCallTabHide(@lastActiveTab)
						@internalCallTabShow(tag)

					@lastActiveTab = tag
				catch error
					console.log "Exception in DynamicTabs - onTabShow or onTabHide with Tab: ", error, tag

			else
				tag.tab.removeClass "active"
				tag.body.hide()

		@updateBadges()
		# @updateRightTab()
		true

	updateRightTab: () =>
		tabsWidth = 0
		for id, tag of @tags 
			if tag.tab? and tag.tab.getVisible() is true 
				tabsWidth += tag.tab.getTag().outerWidth()

		@rightTab.move tabsWidth, @height() - @getInsideHeight() - @tabHeight, @getInsideWidth() - tabsWidth, @tabHeight


	updateBadges: ()=>

		for id, tag of @tags

			baseText = tag.badge
			if !baseText? and tag.body? and tag.body.getBadgeText?
				baseText = tag.body.getBadgeText()

			if baseText? and (typeof(baseText)=="number" or baseText.length > 0)
				tag.badgeText.html baseText
				tag.badgeWrapper.show()
			else
				tag.badgeWrapper.hide()

		@updateRightTab()

		true

	##|
	##|  Add a view to a tab
	##|  Return (resolves) with the tab
	##|  Calls callbackWithView with the new view
	##|  The promise is only complete after the callback completes.
	##|
	doAddViewTab : (viewName, tabText, callbackWithView, sortOrder) =>

		new Promise (resolve, reject) =>
			try 
				gid = GlobalValueManager.NextGlobalID()
				tab = @addTab tabText, null, sortOrder
				tab.body.setView viewName, callbackWithView
				.then (view)=>
					view.elHolder = tab.body.el
					resolve(view)
			catch error
				console.log "Exception in adding View to DynamicTabs: ", error, viewName, tabText
			true

	##|
	##|  Add a table to a tab which is a common function so
	##|  we have included management for tabs with tables globally
	##|
	doAddTableTab : (tableName, tabText, sortOrder = null, callbackWithTableView) =>

		new Promise (resolve, reject)=>

			@doAddViewTab "Table", tabText, (view, viewText)=>

				if !@tables? then @tables = {}
				table = view.loadTable tableName
				@tables[tableName] = table
				@tables[tableName].tab = @tabs[tabText]
				@tabs[tabText].table = table

				if callbackWithTableView?
					callbackWithTableView(view)

				return true
			, sortOrder

			.then (tab)=>

				# total = @tabs[tabText].table.getTableTotalRows()
				# console.log "Setting Badge [#{tabText}] to #{total}:", @tabs[tabText].table
				# @tabs[tabText].setBadge(total)
				resolve(@tabs[tabText])

	onCheckTableUpdateRowcount: (tableName, newRowCount)=>

		if !@tables? then return
		# console.log "onCheckTableUpdateRowcount table=#{tableName} new=#{newRowCount}"

		if @tables[tableName]?
			@tables[tableName].tab.badgeText.html newRowCount
			@tables[tableName].tab.badgeWrapper.show()
			@tables[tableName].tab.badge = newRowCount

		true

	## -------------------------------------------------------------------------------------------------------------
	## Add a new tab to current instance
	##
	## @param [String] tabName the name of the tab to be adding
	## @param [String] defaultHtml optional html for the tab content
	## @return [Tab] the new tab Object which is created
	##
	addTab: (tabName, defaultHtml, sortOrder = null) =>

		##|
		##|  Return existing tab
		if @tabs[tabName]? then return @tabs[tabName]

		##|
		##|  Default sort order puts the tab at the end
		if !sortOrder? or typeof sortOrder != "number"
			sortOrder = Object.keys(@tags).length

		##|
		##|  Find where to insert
		insertPosition = 0
		for child in @tabList.getChildren()
			if child.classes?.indexOf("ninja-right-tab") >= 0 then continue
			if sortOrder > child.sortOrder then insertPosition++

		id = "tab#{@tabCount++}"
		elTab = @tabList.addAtPosition "li", "ninja-nav-tab", insertPosition
		elTab.outerWidth  @tabWidth
		elTab.outerHeight @tabHeight
		elTab.sortOrder = sortOrder
		elTab.setDataPath id
		elTab.on "click touchstart", @onClickTab

		elTabText = elTab.add "div", "ninja-tab-text"
		elTabText.html tabName

		elTabBadge     = elTab.addDiv "ninja-badge"
		elTabBadgeText = elTabBadge.addDiv "ninja-badge-text"

		elBody = @tabContent.add "div", "ninja-nav-body"
		if defaultHtml?		
			elBody.html defaultHtml

		if !@activeTab?
			@activeTab = id

		@tags[id] =
			name:         tabName
			id:           id
			html:         defaultHtml
			order:        sortOrder
			parent:       this
			tab:          elTab
			body:         elBody
			tabText:      elTabText
			badgeWrapper: elTabBadge
			badgeText:    elTabBadgeText
			setBadge:     @onSetBadge
			show: ()=>
				@activeTab = id
				@updateTabs()
			close: () =>
				elTab.destroy()
				if elBody.childView? and elBody.childView.removeView?
					elBody.childView.removeView()
				elBody.destroy()
				delete @tags[id]
				delete @tabs[tabName]
				@updateTabs()

		##|
		##|  A reference to the data by name
		@tabs[tabName] = @tags[id]
		@updateTabs()

		return @tags[id]

	onSetBadge: (num, classname)->
		id = this.id
		@parent.tags[id].badge = num

		if classname?
			@parent.tags[id].badgeText.addClass("badge-" + classname)

		return @parent.updateBadges()

	onClickTab: (e)=>
		# console.log "DynamicTabs onClickTab"
		if e? and e.path? then @show(e.path)
		return true

	getActiveTab: ()=>
		return @tags[@activeTab]

	show: (id)=>

		new Promise (resolve, reject)=>

			if !@viewsToLoad? then @viewsToLoad = {}

			##|
			##|  If the view isn't loaded, we need to load it fully before we can show it.
			##|  Otherwise the move and resize actions inside updateTabs won't be correct.
			##|  So only after the view is loaded and processed can we call the show method again.
			##|
			if typeof id == "object" and id.id? then id = id.id

			if @viewsToLoad[id]?
				@loadTabView(id)
				.then ()=>
					@show(id)
					.then (result)=>
						tab = @internalFindTab id
						if tab? and !tab.body.onTabShow?
							@internalCallTabShow(tab)
						resolve(result)

				return true

			if !id?
				resolve(false)
				return false

			if @tags[id]? and !@tags[id].disabled and @tags[id].tab.visible is true
				@emitEvent "showtab", [ id, @tags[id] ]
				@activeTab = id
				@updateTabs()
				##
				## save newly selected tab's name to localStorage
				@saveSettings()
			else
				resolve(false)
				return false

			resolve(true)
			true

	getTab: (tabName) =>

		##|
		##|  Return existing tab
		if @tabs[tabName]? then return @tabs[tabName]
		return null

	##|
	##|  Returns the size of the space available for the tab content
	getInsideHeight: ()=>
		h = @height()
		h -= (@tabHeight)
		return h

	##
	## Add global hot keys to tab so that the tab can be activated by the hotkey
	addHotkeyToTab: (tabName, keyboard) =>
		##
		## check if a required 3rd library is ready to use
		if !Mousetrap?
			console.log "Warning: Mousetrap is not loaded, no hotkeys available."
			return false

		##
		## validate inputs
		tab = @tabs[tabName]
		if !tab then return false
		if !keyboard? or keyboard is "" then return false

		tab.keyboard = keyboard
		Mousetrap.bind [ 'mod+'+ keyboard ], (e) =>
			@show tab.id
			false

		##
		## remove global hotkeys when this tab gets destroyed
		tab.tab.el.on "remove", (e) => 
			Mousetrap.unbind [ 'mod+' + keyboard ]
		
		true

	##|
	##|  Returns the size of the space available width
	getInsideWidth: ()=>
		return @getWidth()

	##
	## Add an icon to next to tab text
	addIconToTab: (tabName, icon) =>
		if !icon or icon is "" then return false

		tab = @internalFindTab(tabName)
		if !tab? then return false

		if !icon? or icon is "" then return false
		if !tab.icon?
			tab.elTabIcon   = tab.tab.addAtPosition "div", "ninja-tab-icon", 0
		tab.icon    = icon
		tab.elTabIcon.html "<i class='#{icon}'></i>"
		return true

	##
	## Add a "close" button at the right of the tab name
	addCloseButton: (tabName) =>
		tab = @internalFindTab tabName
		if !tab? then return false 
		if tab.closebtn is true then return false
		tab.closebtn = true 
		tab.elCloseBtn = tab.tab.add "span", "ninja-tab-close-btn"
		tab.elCloseBtn.html "<i class='fa fa-times'></i>"
		tab.elCloseBtn.on "click touchstart", (e) =>
			@showClosestTab tab
			#@show @lastActiveTab.id
			if tab.body.onTabHide? 
				tab.body.onTabHide tab
			tab.close()
			@emitEvent "closetab", [ tab.id, tab ] 
			true
		true

	createTabList: () =>
		@tabList    = @add "ul", "ninja-tab-list", "ninja-tab-list"
		@rightTab   = @tabList.add "li", "ninja-nav-tab ninja-right-tab"
		@tabList.height(@tabHeight)
		true

	onShowScreen: ()=>

		@tags       = {}
		@tabs       = {}
		@tabCount   = 0
		@activeTab  = null

		@el.addClass "ninja-tabs"
		@createTabList()
		@addDiv "clr"
		@tabContent = @add "div", "ninja-tab-content tab-content"

		@updateRightTab()
		##
		## Handle child views when they are trying to show/hide themselves
		@tabList.onChildVisibility = (child, visibility) =>
			## find tab which has child as body
			tab = null
			for key, tag of @tags
				if tag.tab is child then tab = tag
			if !tab then return false
			
			if visibility is "show"
				tab.tab.el.show()
				tab.tab.setVisible true
				# console.log "Tab is shown", tab.id
				@updateRightTab()

			else if visibility is "hide"
				tab.tab.el.hide()
				tab.tab.setVisible false
				# console.log "Tab is hidden", tab.id
				if tab.id is @activeTab
					@showClosestTab tab
				@updateRightTab()
			true

		@tabData  	= []

		##|
		##|  Allow this view to have events
		GlobalClassTools.addEventManager(this)

		##|
		##|  If someone updates the number for the badge, referesh the tabs
		globalTableEvents.on "row_count", ()=>
			@updateBadges()

		globalTableEvents.on "file_count", ()=>
			@updateBadges()

		true
	## 
	## Show the closest tab to parameter tab
	showClosestTab: (tab) =>

		position = @tabList.children.indexOf(tab.tab) + 1

		while(@tabList.children[position]?)
			if @tabList.children[position].visible is true
				for key, tag of @tags
					if tag.tab is @tabList.children[position]
						return @show(tag.id)
			position++

		position = @tabList.children.indexOf(tab.tab) - 1

		while(@tabList.children[position]?)
			if @tabList.children[position].visible is true
				for key, tag of @tags
					if tag.tab is @tabList.children[position]
						return @show(tag.id)
			position--

		@activeTab = null
		@updateTabs()
		return false

	## 
	## Enable/Disable a tab
	setTabEnabled: (tabName, isEnabled) =>
		if !tabName? then return false
		if !isEnabled? then isEnabled = true

		tab = @internalFindTab(tabName)
		if !tab? then return false

		tab.disabled = !isEnabled
		tab.tab.setClass "ninja-tab-disabled", tab.disabled

		## if active tab is disabled, then try to show other tab
		if tab.disabled is true and @activeTab is tab.id
			@showClosestTab tab
		true

	##|
	##|  Given a tab name or tab object, return the tab
	##|	 @param tabNameOrObject {string|object} Either text of a tab name, ID, or the tab object itself
	##|  @return null or a tab object
	##|
	internalFindTab: (tabNameOrObject)=>
		if !tabNameOrObject? then return null

		if typeof tabNameOrObject == "object" and tabNameOrObject.id?
			return tabNameOrObject

		if typeof tabNameOrObject == "string" and @tabs[tabNameOrObject]?
			return @tabs[tabNameOrObject]

		for tabName, tag of @tabs
			if tag.id == tabNameOrObject then return tag
			if tag.name? and tag.name == tabNameOrObject then return tag

			##|
			##|  Regex match attempt if tabNameOrObject is /something/
			if typeof tabNameOrObject == "object" and tabNameOrObject.test?
				if tabNameOrObject.test(tag.name) then return tag

		return null

	##|  Set the background color of the tab
	##|
	##|  @see (Theme style file)[theme.styl] for a reference to the colors available
	##|  @param color {number|string} The color either a number 0 through 9 or the text "tabcolor0" through "tabcolor9"
	setTabBGColor: (tabName, color) =>
		tab = @internalFindTab(tabName)
		if !tab? then return false
		tab.bgcolor = color
		if !color? then color = "tabcolor0"
		if !isNaN(color) then color = "tabcolor" + color
		tab.tab.setClass color, true
		true

	##|
	##|  Add a css class name to a tab
	##|  @param tabName {mixed} The title of the tab or the tab object, see internalFindTab
	##|  @param className {string} The css class to add or remove
	##|  @param addClass {boolean} Will add the class (true|default) or remove the class (false)
	##|
	setTabClass: (tabName, className, addClass = true)=>
		tab = @internalFindTab(tabName)
		if !tab? then return false
		tab.addclass = className
		tab.tab.setClass className, addClass

	##
	## Set name of setting that would be stored in localStorage to save the selected tab
	setSettingsName: (name) =>
		if !name? then return false
		@settingsName = name 
		exSettings = localStorage.getItem name 
		if exSettings? and @getTab(exSettings)?
			@show @getTab(exSettings).id
		true			

	##
	## Save active tab's name to localstorage
	saveSettings: () =>
		if !@settingsName? or @settingsName is "" then return false
		activeTab = @getActiveTab()
		if activeTab?
			localStorage.setItem @settingsName, activeTab.name 
		activeTab.name

	##
	## Add a serialized view that will be loaded when tab gets activated
	addTabViewToLoad: (id, options) =>
		if !@viewsToLoad? then @viewsToLoad = {}
		@viewsToLoad[id] = options
		@viewsToLoad

	addTabView: (id, options) =>
		if !@backedViewsToLoad? then @backedViewsToLoad = {}
		@backedViewsToLoad[id] = JSON.stringify options
		@backedViewsToLoad[id]

	##
	## Load a view in the tab's body
	loadTabView: (id) =>

		newPromise ()=>
			viewOptions = @viewsToLoad[id]
			tab         = @tags[id]
			yield tab.body.internalProcessLayout(viewOptions)
			delete @viewsToLoad[id]
			@internalCopyVariables(tab.body)
			return true

	##
	## Set which tab to activate after all tabs are created
	setActiveTab: (tabName) =>
		tab = @internalFindTab tabName
		if !tab or !tab.id? then return false 
		@show tab.id
		true

	##
	## Set html of the very right(overflowed) tab which is not clickable
	setOverflowHTML: (strHtml) =>
		children = @rightTab.getChildren() 
		if children and children.length > 0
			elText = children[0]
		else 
			elText = @rightTab.add "div", "ninja-tab-text"
		elText.html strHtml

	##
	## Set flag that indicates if it isn't allowed to auto-load one of delayed views
	setDisableAutoLoadDelayedView: (bool) =>
		@disableAutoLoadDelayedView = bool

	serialize: () =>
		tabs = []
		for id, tab of @tags
			if !tab? then continue
			view = @backedViewsToLoad?[id]
			if view?
				tabs.push JSON.parse(view)
			else
				tabs.push
					title:     tab.name
					html:      tab.html
					order:     tab.order
					icon:      tab.icon
					keyboard:  tab.keyboard
					disabled:  tab.disabled
					close:     tab.closebtn
					bgcolor:   tab.bgcolor
					addclass:  tab.addclass
					ontabshow: tab.body.onTabShow
					ontabhide: tab.body.onTabHide

		return 
			activetab   :               @activeTab
			settingsname:               @settingsName
			disableautoloaddelayedview: @disableAutoLoadDelayedView
			tabs:                       tabs

	##|
	##|  Autolayout helper class -- called after the view is created, before returning to the main caller.
	##|  See ViewTestLayout7
	##|
	deserialize: (options)=>

		newPromise ()=>

			allPromises = []
			if options.tabs?

				##|
				##|  See if any of the tabs have active: true
				##|
				tabNumber = 0
				activeTabNumber = 0
				for item in options.tabs
					if item.active == true
						activeTab = tabNumber
					tabNumber++

				##|
				##|  Create each tab
				tabNumber = 0
				for item in options.tabs

					title = "Unknown"
					order = null
					html  = null
					icon  = null

					if item.title? then title = item.title
					if item.html? then html = item.html
					if item.order? then order = item.order
					
					tab = @addTab title, html, order
					
					if item.icon?
					    icon = item.icon
					    @addIconToTab title, icon
					    
					if item.keyboard? 
						@addHotkeyToTab title, item.keyboard

					if item.disabled?
						@setTabEnabled title, !item.disabled
					
					if item.close? and item.close is true
						@addCloseButton title

					if item.bgcolor?
						@setTabBGColor title, item.bgcolor

					if item.addclass?
						@setTabClass title, item.addclass

					if item.ontabshow?
						tab.body.onTabShow = item.ontabshow

					if item.ontabhide?
						tab.body.onTabHide = item.ontabhide

					if item.view?
						@addTabView tab.id, item
						if item.delayed is true
							@addTabViewToLoad tab.id, item
							##|  delayed tab would call onTabShow when it loads
						else
							yield tab.body.internalProcessLayout(item)
							# allPromises.push tab.body.internalProcessLayout(item)
							if tabNumber == activeTabNumber
								@lastActiveTab = tab

					@internalCopyVariables(tab.body)

					tabNumber++

			if options.settingsname? or options.settingsName?
				@setSettingsName(options.settingsname or options.settingsName)

			if options.activetab? or options.activeTab?
				@setActiveTab(options.activetab or options.activeTab)

			if options.disableautoloaddelayedview?
				@setDisableAutoLoadDelayedView options.disableautoloaddelayedview

			# yield Promise.all(allPromises)

			##|
			##|  Call tab that was active
			if @lastActiveTab?
				@internalCallTabShow(@lastActiveTab)

			##
			## if all views have delay, then load the first view with delay
			unless @disableAutoLoadDelayedView is true
				viewIdsToLoad = Object.keys(@viewsToLoad or {})
				if viewIdsToLoad.length is @tabCount
					@show viewIdsToLoad[0]

			return true
