"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
const EdgeError_1 = require("./EdgeError");
const edgeapi_1 = require("edgeapi");
const ApiSocketServer_1 = require("./ApiSocketServer");
const crypto = require('crypto');
/**
 * This makes some class object into a provider of API calls.
 * It works by taking the prototype object and looking at all the function calls
 * and then making dynamic call references for them.
 *
 * All functions must start with "do"
 * All functions with Internal will be skipped
 */
class ApiProvider {
    constructor(thePrototype) {
        this._apiGatewayRef = null;
        this.dummyPromise = null;
        this.apiCallFunc = {};
        this.apiCallList = {};
        this.authLogger = edgecommonconfig_1.config.getLogger('AuthCheck');
        //  Return a dummy promise that does nothing if
        //  there is no real return value
        this.dummyPromise = new Promise((resolve, reject) => {
            return resolve(true);
        });
        this.redisPubsubQueue = edgeapi_1.EdgeApi.getRedisConnectionWQ();
        this.onInit();
        this.determineApiCalls(thePrototype);
    }
    onInit() {
    }
    getBaseName() {
        throw new Error('Error, getBaseName must be provided to return the API base call name');
    }
    /**
     * Given a function call, return the argunments required
     * @param thePrototype
     * @param functionCall
     */
    getParamNames(thePrototype, functionCall) {
        try {
            const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
            const ARGUMENT_NAMES = /([^\s,]+)/g;
            const fnStr = thePrototype[functionCall].toString().replace(STRIP_COMMENTS, '');
            const parts = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')'));
            const result = parts.match(ARGUMENT_NAMES);
            if (result === null) {
                return [];
            }
            return result;
        }
        catch (e) {
            edgecommonconfig_1.config.reportError(`Unable to getParamNames of ${functionCall} [${thePrototype}]`, e);
            throw e;
        }
    }
    sendToChannel(channelName, priority, data) {
        const pubsubData = {
            channel: channelName,
            stamp: new Date(),
            data,
        };
        this.redisPubsubQueue.publish(ApiSocketServer_1.REDIS_PUBSUB_CHANNEL, JSON.stringify(pubsubData));
    }
    /**
     * Look at the functions exposed by the API and create an
     * array to reference those functions so that the socket can
     * RPC call them.
     * @param thePrototype
     */
    determineApiCalls(thePrototype) {
        try {
            this.apiCallList = {};
            this.apiCallFunc = {};
            const reCallable = /^do/;
            const reInternal = /Internal/i;
            //  Find the available functions from the API
            const funcNames = Object.getOwnPropertyNames(thePrototype);
            for (let i = 0; i < funcNames.length; i++) {
                if (!reCallable.test(funcNames[i])) {
                    continue;
                }
                if (reInternal.test(funcNames[i])) {
                    continue;
                }
                const param = this.getParamNames(thePrototype, funcNames[i]);
                this.apiCallList[funcNames[i]] = param;
            }
            return true;
        }
        catch (e) {
            console.log('Error in determineApiCalls: ', e);
            return edgecommonconfig_1.config.reportError('Exception in determineApiCalls', e);
        }
    }
    /**
     * Handle a call to the API
     * Exactly same as `doProcessApiCall` of ApiGateway except this one is runnable
     * directly from providers
     * @param callName - [string] function name provider_method
     * @param callData - [any]
     * @param callerSession - [ApiSocketUser]
     */
    doCall(callName, callData, callerSession) {
        if (this._apiGatewayRef.apiCallList[callName] != null) {
            return this._apiGatewayRef.MakeDynamicCall(callName, callData, callerSession);
        }
        throw new Error(`Invalid api call: ${callName}`);
    }
    /**
     * Shortcut function to make it easy for any provider to update data.
     * @param user The calling user for authentication and role verification
     * @param dataSet - [string] The dataset to update
     * @param path - [string] The complete path to update
     * @param newData - [string] The object values to replace within the stored data.
     */
    internalUpdatePath(user, dataSet, path, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.doCall("data_doUpdatePath", { dataSet: dataSet, path: path, newData: newData }, user);
            return result;
        });
    }
    /**
     * Converts something into a hash such that an ID can be generated.  Will always create the
     * same id from the same source content.
     * @param sourceObject Any object or string value which is used to produce a hash.
     */
    fastHash(sourceObject) {
        if (sourceObject === null) {
            sourceObject = new Date().toString();
        }
        const ha1 = new crypto.createHash('sha1');
        ha1.update('SALT');
        ha1.update(JSON.stringify(sourceObject));
        let hashValue = ha1.digest("base64");
        hashValue = hashValue.replace("=", "Z");
        return hashValue;
    }
    /**
     * failedRoleCheck log failed role check error and normalize error response
     * @param user authenticating user
     * @param role required role
     * @param caller  caller method name
     */
    failedRoleCheck(user, role, caller) {
        const userId = user.rec ? user.rec.id : 'Non-logged in User';
        this.authLogger.error({
            userId: userId,
            role: role,
            ipAddress: user.ipAddress,
            caller,
        });
        return new EdgeError_1.default(`Invalid API Call, missing role ${role} for user ${userId}`, `INSUFFICENT_ROLE`, 403);
    }
}
exports.default = ApiProvider;
;
//# sourceMappingURL=ApiProvider.js.map