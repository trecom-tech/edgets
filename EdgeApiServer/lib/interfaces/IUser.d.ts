export interface IUser {
    id: string;
    name: string;
    first_modified: Date;
    last_modified: Date;
    active: boolean;
    password: string;
    last_login?: Date;
    last_login_ip?: string;
    partner_id?: string;
    sponsor_id?: string;
    authToken?: string;
    contact?: any;
    roles: any;
    attrib: any;
}
