"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const edgecommonconfig_1 = require("edgecommonconfig");
class MongoModel {
    /**
     *
     * @param name [string] collectionName
     * @param gateway MongoGateway object
     */
    constructor(name, gateway) {
        //
        // New model is created
        // name = the name of the collection
        // gateway = a reference to the MongoGateway object that issued the collection
        //
        // The collection is not yet connected until requested with doInternalGetCollection
        this.name = name;
        this.gateway = gateway;
        this.theCollection = null;
        this.internalCollectionPromise = null;
        // tslint:disable-next-line:no-floating-promises
        this.doInternalGetCollection();
    }
    /**
     * Connects to the database and gets a reference to the table
     * which is a collection in mongo terms
     *
     */
    doInternalGetCollection() {
        if (!this.internalCollectionPromise) {
            this.internalCollectionPromise = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    yield this.gateway.doConnectToDatabase();
                    this.theCollection = this.gateway.db.collection(this.name);
                    resolve(this.theCollection);
                }
                catch (err) {
                    reject(err);
                }
            }));
        }
        return this.internalCollectionPromise;
    }
    doAggregate(aggList) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            return this.theCollection.aggregate(aggList).toArray();
        });
    }
    /**
     * Given a specific field, return the unique values and counts.
     * @param fieldName
     * @param matchCondition
     */
    doGetUniqueValues(fieldName, matchCondition) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            const aggList = [];
            if (matchCondition != null) {
                aggList.push({
                    '$match': matchCondition,
                });
            }
            aggList.push({
                '$group': {
                    '_id': `$${fieldName}`,
                    'count': {
                        '$sum': 1,
                    },
                },
            });
            console.log('Using: ', aggList);
            return this.theCollection.aggregate(aggList).toArray();
        });
    }
    doUpdateOne(searchCondition, setCondition, isMulti = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            try {
                const result = yield this.theCollection.updateOne(searchCondition, setCondition);
                return result;
            }
            catch (e) {
                let log = edgecommonconfig_1.config.getLogger(`doUpdateOne-${this.name}`);
                log.error('doUpdateOne Exception', {
                    searchCondition,
                    setCondition,
                    isMulti,
                    exception: e,
                });
                throw e;
            }
        });
    }
    doInsertMany(newDocuments) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            //
            // Fast multiple document insert
            return this.theCollection.insertMany(newDocuments);
        });
    }
    doInsertOne(newDocument) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            try {
                const result = yield this.theCollection.insertOne(newDocument);
                return result;
            }
            catch (e) {
                console.log('doInsertOne Exception:', e);
                console.log('newDocument:', newDocument);
                return null;
            }
        });
    }
    doRemove(condition) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            // tslint:disable-next-line:deprecation
            const result = yield this.theCollection.remove(condition);
            return result;
        });
    }
    /**
     * Insert or update a document with a given ID
     * @param value
     * @param newDataObj
     */
    doUpsertOne(value, newDataObj) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            let newData = {};
            newDataObj.id = value;
            newData['$set'] = newDataObj;
            let options = {
                'new': true,
                'upsert': true,
            };
            let condition = { id: value };
            try {
                const r = yield this.theCollection.findAndModify(condition, {}, newData, options);
            }
            catch (e) {
                console.log('DatabaseError doUpsertOne', newDataObj);
                console.info(`DataSet   :${this.name}`);
                console.info(`Document  :${JSON.stringify(value)}`);
                console.info(`Exception :${e}`);
                return false;
            }
            return true;
        });
    }
    doAppend(condition, subPath, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                edgecommonconfig_1.config.status('doAppend condition=', condition);
                if (!this.theCollection) {
                    yield this.doInternalGetCollection();
                }
                const setData = {};
                setData['$set'] = { '_lastModified': new Date() };
                setData['$push'] = {};
                if (typeof newData === 'object' && Array.isArray(newData)) {
                    setData['$push'][subPath.join('.')] = { '$each': newData };
                }
                else {
                    setData['$push'][subPath.join('.')] = newData;
                }
                const options = { multi: true };
                try {
                    // tslint:disable-next-line:deprecation
                    const r = yield this.theCollection.update(condition, setData, options);
                    return r;
                    // r = await @theCollection.findAndModify condition, { }, newData , options
                }
                catch (e) {
                    console.log('DatabaseError doAppend', condition, subPath);
                    let log = edgecommonconfig_1.config.getLogger(`doAppend-${this.name}`);
                    log.error('Append Error', {
                        subPath,
                        condition,
                        newData,
                        options,
                        setData,
                        exception: e,
                    });
                    return false;
                }
            }
            catch (err) {
                throw err;
            }
        });
    }
    doFindAndInsert(condition, newData, attemptCount = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let value;
                edgecommonconfig_1.config.status('doFindAndInsert condition=', condition, 'newData=', newData);
                if (!this.theCollection) {
                    yield this.doInternalGetCollection();
                }
                let modifyOptions = {};
                modifyOptions['$setOnInsert'] = {};
                modifyOptions['$setOnInsert']['_lastModified'] = new Date();
                let topLevelKeys = {};
                topLevelKeys['_id'] = 1;
                for (const varName in newData) {
                    value = newData[varName];
                    topLevelKeys[varName] = 1;
                }
                for (let varName in condition) {
                    value = condition[varName];
                    topLevelKeys[varName] = 1;
                    newData[varName] = value;
                }
                modifyOptions['$setOnInsert'] = newData;
                const options = {
                    'new': true,
                    'upsert': true,
                    'fields': topLevelKeys,
                };
                if (edgecommonconfig_1.config.traceEnabled) {
                    edgecommonconfig_1.config.status('MongoModel doFindAndInsert Starting');
                }
                // config.dump "doFindAndInsert",
                //     condition     : condition
                //     newData       : newData
                //     modifyOptions : modifyOptions
                //     options       : options
                // TODO mainly because Collection does not have findAndModify
                let r = yield this.theCollection.findAndModify(condition, {}, modifyOptions, options);
                let result = {
                    isNew: r.lastErrorObject.updatedExisting !== true,
                    count: r.lastErrorObject.n,
                    value: r.value,
                };
                return result;
            }
            catch (e) {
                if (attemptCount === 0) {
                    edgecommonconfig_1.config.status('Attempting to reFindAndModify');
                    let val = yield this.doFindAndInsert(condition, newData, attemptCount + 1);
                    return val;
                }
                let log = edgecommonconfig_1.config.getLogger(`doFindAndInsert-${this.name}`);
                log.error('doFindAndInsert Exception', {
                    condition,
                    newData,
                    exception: e,
                });
                console.log('DatabaseError doFindAndInsert', newData);
                console.log('DatabaseException: ', e.toString());
                return null;
            }
        });
    }
    doInsertOrUpdate(condition, newData) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!this.theCollection) {
                    yield this.doInternalGetCollection();
                }
                const setData = { '$set': newData };
                // tslint:disable-next-line:deprecation
                const result = yield this.theCollection.update(condition, setData, { upsert: true });
                return result;
            }
            catch (e) {
                const log = edgecommonconfig_1.config.getLogger('doInsertOrUpdate');
                log.error('doInsertOrUpdate Exception', {
                    condition,
                    newData,
                    exception: e,
                });
                console.log('DatabaseError doInsertOrUpdate', newData);
                console.log('DatabaseException: ', e.toString());
                return null;
            }
        });
    }
    //
    // search for one or more items
    //
    // @param value [mixed] Value must be the object or value for "id"
    // @param createIfNeeded [bool] Create the record if it doesn't already exist
    // @param optionalPath [Array] Optional list of deeper paths within the object to return
    // @param returnFullDetail [bool] Normally just the object value is returned, otherwise the value and status
    //
    doFindByID(value, createIfNeeded = false, optionalPath = [], returnFullDetail = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let r;
                let result;
                if (!this.theCollection) {
                    yield this.doInternalGetCollection();
                }
                const condition = { id: value };
                if (createIfNeeded) {
                    //
                    // Find or Create record
                    const options = {
                        'new': true,
                        'upsert': true,
                    };
                    if (optionalPath) {
                        // TODO set function weird from original coffee
                        // ({ 'fields': optionalPath.join('.') });
                    }
                    r = yield this.theCollection.findAndModify(condition, {}, { '$setOnInsert': condition }, options);
                    if (returnFullDetail) {
                        result = {
                            isNew: r.lastErrorObject.updatedExisting !== true,
                            count: r.lastErrorObject.n,
                            value: r.value,
                        };
                        return result;
                    }
                    else if (r != null && r.value != null && r.value._id != null) {
                        return r.value;
                    }
                    else {
                        console.log('Internal error, should not be here with a null value:', r);
                        return null;
                    }
                }
                else {
                    // Only find
                    r = yield this.theCollection.findOne(condition);
                    if (returnFullDetail != null && returnFullDetail === true) {
                        result = {
                            isNew: false,
                            count: 1,
                            value: r,
                        };
                        return result;
                    }
                }
                return r;
            }
            catch (e) {
                console.log('DatabaseError doFindByID', value);
                console.info(`Exception :${e}`);
                return false;
            }
        });
    }
    /**
     * Count the results of a search before searching
     * @param condition
     */
    doCount(condition) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            edgecommonconfig_1.config.status(`MongoModel ${this.name} doCount condition=`, condition);
            const result = yield this.theCollection.find(condition).count();
            edgecommonconfig_1.config.status(`MongoModel ${this.name} doCount result=`, result);
            return result;
        });
    }
    doFindNearbyIds(conditions, location, miles, _offset, limit, fieldList) {
        return __awaiter(this, void 0, void 0, function* () {
            let coords;
            let cord = null;
            if (location != null && location.coordinates != null) {
                coords = location.coordinates;
            }
            else if (location != null && location[0] != null) {
                coords = location;
            }
            else if (location != null && location.lat != null) {
                coords = [location.lon, location.lat];
            }
            // console.log "doFindNearbyConditions:", conditions
            const aggList = [];
            const geoOptions = {
                '$geoNear': {
                    'near': {
                        'type': 'Point',
                        'coordinates': coords,
                    },
                    'distanceField': 'distance',
                    'maxDistance': 1609.34 * miles,
                    'spherical': true,
                },
            };
            // "query" : { "loc.type" : "Point" }
            if (limit != null && typeof limit === 'number') {
                geoOptions['$geoNear']['limit'] = limit;
            }
            aggList.push(geoOptions);
            if (Object.keys(conditions).length > 0) {
                aggList.push({
                    '$match': conditions,
                });
            }
            const requestFields = { id: 1, distance: 1 };
            if (fieldList != null) {
                let varName;
                if (Array.isArray(fieldList)) {
                    for (varName of Array.from(fieldList)) {
                        requestFields[varName] = 1;
                    }
                }
                else {
                    for (varName in fieldList) {
                        let value = fieldList[varName];
                        requestFields[varName] = value;
                    }
                }
            }
            aggList.push({ '$project': requestFields });
            aggList.push({ '$sort': { 'distance': 1 } });
            // if limit?
            //     aggList.push { '$limit' : limit }
            if (edgecommonconfig_1.config.traceEnabled) {
                edgecommonconfig_1.config.status('doFindNearbyIds Starting search');
                edgecommonconfig_1.config.dump('doFindNearbyIds doAggregate:', aggList);
            }
            let result = yield this.doAggregate(aggList);
            if (edgecommonconfig_1.config.traceEnabled) {
                edgecommonconfig_1.config.status(`doFindNearbyIds found ${result.length}`);
            }
            return result;
        });
    }
    /**
     * DoFindNearby - Returns records that are nearby assuming
     * that the destimation table has a "loc" field and that it
     * contains the 2dsphere index.
     * @param conditions [mixed] Should be an object with lat and lon defined or it can be a GeoJSON object with coordinates defined.
     * @param miles [number] the number of miles to search around
     * @param offset [number] Optional starting point in the result list (default 0)
     * @param limit [number] Optional the maximum number of results (default 1000)
     */
    doFindNearby(conditions, location, miles, offset, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            let cord = null;
            if (location == null) {
                edgecommonconfig_1.config.status('doFindNearby missing location for search');
                return null;
            }
            const condition = {
                loc: {
                    '$nearSphere': location,
                    '$maxDistance': 1609.34 * miles,
                },
            };
            //
            // Apply other conditions to the search if needed
            if (conditions != null && typeof conditions === 'object') {
                for (const keyName in conditions) {
                    const keyValue = conditions[keyName];
                    condition[keyName] = keyValue;
                }
            }
            if (!this.theCollection) {
                yield this.doInternalGetCollection();
            }
            let r = this.theCollection.find(condition);
            if (limit != null) {
                r = r.limit(limit);
            }
            if (offset != null) {
                r = r.skip(offset);
            }
            // r.sort { distance: 1 }
            try {
                const all = yield r.toArray();
                return all;
            }
            catch (ex) {
                let log = edgecommonconfig_1.config.getLogger(`doFindNearby-${this.name}`);
                log.error('doFindNearby', {
                    condition,
                    location,
                    miles,
                    offset,
                    limit,
                    ex,
                });
                console.log('Exception during doFindNearby: ', ex);
                console.log('Search condition:', condition);
                return {};
            }
        });
    }
    /**
     * Do find on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [object] condition to apply find
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doFind(condition, fieldList = null, sortCondition = null, offset = 0, limit = 100) {
        return __awaiter(this, void 0, void 0, function* () {
            edgecommonconfig_1.config.status(`doFind ${this.gateway.dbName}/${this.name} `, {
                'condition:': condition,
                'fieldList=': fieldList,
                'offset=': offset,
                'limit=': limit,
            });
            const r = yield this.doGetCursor(condition, fieldList, sortCondition, offset, limit);
            if (r == null) {
                return [];
            }
            return r.toArray();
        });
    }
    /**
     * Do search on `path` with MongoDb `offset` `limit` `sort` and fildlist`
     * @param path - [string] path to apply search
     * @param fieldList - [string|array] mongodb fieldlist to give as second param, array of fieldnames or string with comma-separted
     * @param sortCondition - [any] object { sortFieldName: 1 | -1 } or other mongo compatible
     * @param offset - [number] mongo offset
     * @param limit - [number] mongo limit
     */
    doGetCursor(condition, fieldList, sortCondition, offset, limit) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let r;
                if (!this.theCollection) {
                    yield this.doInternalGetCollection();
                }
                if (fieldList != null) {
                    // tslint:disable-next-line:deprecation
                    r = this.theCollection.find(condition, fieldList);
                }
                else {
                    // tslint:disable-next-line:deprecation
                    r = this.theCollection.find(condition);
                }
                if (limit != null) {
                    if (typeof limit !== 'number') {
                        console.log('Warning: invalid limit:', limit + '');
                    }
                    r = r.limit(limit);
                }
                if (offset != null) {
                    r = r.skip(offset);
                }
                if (sortCondition != null) {
                    r = r.sort(sortCondition);
                }
                return r;
            }
            catch (e) {
                let log = edgecommonconfig_1.config.getLogger(`doGetCursor-${this.name}`);
                log.error('doGetCursor Exception', {
                    condition,
                    fieldList,
                    sortCondition,
                    offset,
                    limit,
                    exception: e,
                });
                console.log('doGetCursor Exception', {
                    condition,
                    fieldList,
                    sortCondition,
                    offset,
                    limit,
                    exception: e,
                });
                return null;
            }
        });
    }
    //
    // Enable iteration through an entire collection
    //
    doStream(condition, fieldList, dateFieldName, _lastModified, limit, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            if (fieldList == null) {
                fieldList = null;
            }
            if (_lastModified == null) {
                _lastModified = new Date('2016-01-01');
            }
            if (typeof _lastModified === 'string') {
                _lastModified = new Date(_lastModified);
            }
            if (typeof _lastModified === 'number') {
                _lastModified = new Date(_lastModified);
            }
            if (_lastModified == null || isNaN(_lastModified.getTime())) {
                _lastModified = new Date('2015-01-01');
            }
            console.log('fieldLIst     : ', fieldList);
            console.log('_lastModified : ', _lastModified);
            console.log('Limit         : ', limit);
            console.log('Callback      : ', callback);
            console.log('dateFieldName : ', dateFieldName);
            if (typeof callback !== 'function') {
                console.log('Callback [', callback, '] is not a function');
                return null;
            }
            if (_lastModified != null) {
                condition[dateFieldName] = { '$gte': new Date(_lastModified) };
            }
            edgecommonconfig_1.config.status(`MongoModel ${this.name} doStream`, condition);
            let count = yield this.doCount(condition);
            edgecommonconfig_1.config.status('doStream available records:', count);
            console.log('doStream available records:', count);
            let sortCondition = {};
            if (dateFieldName != null) {
                sortCondition[dateFieldName] = 1;
            }
            return new Promise((resolve, reject) => {
                return this.doGetCursor(condition, fieldList, sortCondition, 0, limit).then(cursor => {
                    if (cursor == null) {
                        resolve(null);
                    }
                    let totalReceived = 0;
                    let stream = cursor.stream();
                    stream.on('data', doc => {
                        if (doc[dateFieldName] != null && typeof doc[dateFieldName] === 'string') {
                            doc[dateFieldName] = new Date(doc[dateFieldName]);
                        }
                        // console.log "Check [", doc[dateFieldName].getTime(), "][",_lastModified.getTime(), "]"
                        if (doc[dateFieldName] != null && doc[dateFieldName].getTime() > _lastModified.getTime()) {
                            // console.log "USING:", doc[dateFieldName]
                            _lastModified = doc[dateFieldName];
                        }
                        totalReceived++;
                        return callback(doc, totalReceived, count);
                    });
                    stream.on('error', err => {
                        console.log('doStream Stream error:', err);
                        console.log('doStream Stream condition:', condition);
                        resolve(_lastModified);
                    });
                    stream.on('end', (_err) => {
                        resolve(_lastModified);
                    });
                });
            });
        });
    }
    //
    // Enable iteration through an entire collection
    //
    doTail(condition, fieldList, offset, limit, callback, sortCondition) {
        return __awaiter(this, void 0, void 0, function* () {
            let shouldWait;
            if (sortCondition == null) {
                sortCondition = {};
            }
            if (fieldList == null) {
                fieldList = null;
            }
            if (offset == null) {
                offset = 0;
            }
            if (limit == null) {
                limit = null;
            }
            if (shouldWait == null) {
                let shouldWait = true;
            }
            // TODO roy: this one should be written this way
            /**
             * Make sure to resolve after it returns all result...
             * Commenting for now just becuase it affects other portion of code.
             * Come back when this caurses issue.
             * ```
             * return new Promise(resolve => {
             *  this.doGetCursor().then(coursor => {
             *    const stream = cursor.stream();
             *    stream.on('end', ()=>{resolve(totalReceived)});
             *  })
             * })
             * ```
             */
            return new Promise((resolve, reject) => {
                try {
                    return this.doGetCursor(condition, fieldList, sortCondition, offset, limit).then(cursor => {
                        if (cursor == null) {
                            return null;
                        }
                        let totalReceived = 0;
                        const stream = cursor.stream();
                        stream.on('data', doc => {
                            // console.log "doTail Data=", doc
                            totalReceived++;
                            return callback(doc, totalReceived);
                        });
                        stream.on('error', err => {
                            console.log('doTail Stream error:', err);
                            console.log('doTail Stream condition:', condition);
                            resolve(totalReceived);
                        });
                        stream.on('end', (_err) => {
                            resolve(totalReceived);
                        });
                    });
                }
                catch (e) {
                    console.log('doTail Exception:', e);
                    console.log('condition:', condition);
                    console.log('fieldList:', fieldList);
                    console.log('offset:', offset);
                    console.log('limit:', limit);
                    let log = edgecommonconfig_1.config.getLogger('doTail');
                    log.error('doTail', { condition, err: e });
                    return reject(null);
                }
            });
        });
    }
    doCreateIndex(indexName, indexOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doInternalGetCollection();
            return this.theCollection.createIndex(indexName, indexOptions);
        });
    }
}
exports.MongoModel = MongoModel;
exports.default = MongoModel;
//# sourceMappingURL=MongoModel.js.map