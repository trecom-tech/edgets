const stylus       = require('stylus');
const nib          = require('nib');
const pug          = require('pug');
const fs           = require('fs');
const less         = require('less');
const os           = require('os');
const chalk        = require('chalk');
const coffeeScript = require('coffeescript');

//
//  Helper function
export class WebServerHelper {

    //  Given a filename and path list, resolve with the full path that exists.
    static doFindFileInPath(filename: string, pathList: any): Promise<string> {

        return new Promise(function(resolve, reject) {

            if (typeof pathList === 'string') {
                pathList = [pathList];
            }
            for (let path of Array.from(pathList)) {

                try {
                    const filenameTest = path + filename;
                    const stat         = fs.statSync(filenameTest);
                    if ((stat != null) && stat.size) {
                        resolve(filenameTest);
                        return true;
                    }

                } catch (e) {
                    // console.log('Fail silently');
                }
            }

            // console.log(`File ${filename} not found in [`, pathList.join(','), ']');
            return resolve(null);
        });
    }


    static doCompileLessFiles(filenameList: string[]) {

        return new Promise((resolve, reject) => {

            try {

                let strContent = '';
                for (const filename of filenameList) {
                    strContent += fs.readFileSync(filename);
                }

                return less.render(strContent, { compress: false }, (err: Error, output: any) => {

                    if (err != null) {
                        console.log('LESS Compile Error:', err);
                    }

                    return resolve(output);
                });

            } catch (e) {

                console.log(`doCompileLessFile, file not found ${filenameList}`);
                return resolve('');
            }
        });
    }


    static doCompileStylusFile(filename: string): Promise<string> {

        return new Promise(function(resolve, reject) {

            if ((filename == null)) {
                resolve('');
                return;
            }

            return fs.readFile(filename, 'utf8', (err: Error, content: any) => {

                try {
                    if (err) {
                        return reject(err);
                    }

                    return stylus(content)
                        .set('filename', filename)
                        .set('compress', false)
                        .use(nib())
                        .render((err: Error, css: any) => {
                            if (err != null) {
                                console.log(chalk.yellow('Error in Stylus file: ') + filename);
                                console.log(err);
                                return resolve('');
                            } else {

                                return resolve(css);
                            }
                        });

                } catch (ex) {
                    if ((content == null)) {
                        console.log(`No such file: ${filename}`);
                    } else {
                        console.log(`Filename=${filename} Content=`, content);
                        console.log('Inner exception from Stylus: ', chalk.yellow(ex));
                    }

                    return resolve(null);
                }
            });
        });
    }


    static doCompilePugFile(filename: string): Promise<string> {

        return new Promise(function(resolve, reject) {

            if ((filename == null)) {
                resolve('');
                return;
            }

            try {

                return fs.readFile(filename, 'utf8', function(err: Error, content: any) {

                    if (err) {
                        return reject(err);
                    }

                    const html = pug.render(content, {
                            filename,
                            pretty  : false,
                            debug   : false,
                            buildnum: 1,
                            ioserver: os.hostname() + ':' + 9000,
                        },
                    );

                    return resolve(html);
                });

            } catch (e) {

                console.log('Unable to compile Pug: ', filename);
                return resolve('');
            }
        });
    }

    static doCompileCoffeeFile(filename: string, createMap: any = null) {

        return new Promise(function(resolve, reject) {

            console.log('Reading File:', filename, typeof filename);

            return fs.readFile(filename, 'utf8', function(err: Error, content: any) {

                try {

                    if (err) {
                        return reject(err);
                    }

                    let compiled;
                    if ((createMap != null) && createMap) {

                        compiled = coffeeScript.compile(content, {
                                bare     : true,
                                sourceMap: true,
                            },
                        );

                        return resolve(compiled);

                    } else {
                        compiled = coffeeScript.compile(content,
                            { bare: true });
                        return resolve(compiled);
                    }

                } catch (ex) {

                    console.log('Content=', content);
                    return console.log(`[filename=${filename}] Inner exception from CoffeeScript: `, ex.toString());
                }
            });
        });
    }
}

export default WebServerHelper;