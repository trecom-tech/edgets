export interface IMessage {
    alert       : string,
    title?      : string,
    topic?      : string,   // apple
    payload?    : any,      // apple
    sound?      : any,      // apple
    expiry?     : any,      // apple
    badge?      : any,      // apple
    priority?   : any,      // apple
    urlArgs?    : any[],
}