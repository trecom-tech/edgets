$ ->
	fileData1 =
		name: "file1.txt"
		date: "2017-09-09"
		size: "20KB"
		user: "@bpollack"
		description: "This is a text file"

	fileData2 =
		name: "file2.jpg"
		date: "2017-09-10"
		size: "100KB"
		user: "@gao"
		description: "This is an image file"

	fileData3 =
		name: "file3.mp3"
		date: "2017-09-11"
		size: "4.5MB"
		user: "@bpollack"
	
	fileData4 =
		name: "file4.mov"
		date: "2018-01-03"
		size: "123MB"

	fileData5 =
		name: "file5"
		date: "2018-01-05"
		size: "45.67MB"

	fileData6 =
		name: "file6"
		date: "2018-06-06"
		size: "12.23KB"
		subpath: "subfolder1"

	fileData7 =
		name: "file7"
		date: "2018-06-07"
		size: "11KB"
		subpath: "subfolder2"

	fileData8 =
		name: "file8"
		date: "2018-06-08"
		size: "22KB"
		subpath: "subfolder2"

	fileData9 =
		name: "file9"
		date: "2018-06-08"
		size: "22KB"
		subpath: "subfolder3"

	fileData10 =
		name: "file10"
		date: "2018-06-08"
		size: "22KB"
		subpath: "subfolder3/subfolder4"

	addTestButton "Simple File Manager", "Open", () ->
		addHolder().setView "FileManager", (view) =>
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3

			setTimeout () ->
				view.addFile fileData3
			, 1000
		true

	addTestButton "File Manager with various file types", "Open", () ->
		addHolder()
		.setView "FileManager", (view) =>
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3
			view.addFile fileData4
			view.addFile fileData5
		,
			tableName: "customTableName"
		true

	addTestButton "FileManager with Sub Folders", "Open", () ->
		addHolder()
		.setView "FileManager", (view) =>
			view.setPath "test/images"
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3
			view.addFile fileData4
			view.addFile fileData5
			view.addFile fileData6
			view.addFile fileData7
			view.addFile fileData8
			view.addFile fileData9
			view.addFile fileData10
			view.addSubFolder "subfolder3"
			view.addSubFolder "subfolder3/subfolder4"
		true

	addTestButton "File Manager on Popup", "Open", () ->
		doPopupView "FileManager", "Popup with a FileManager", null, 800, 600, (view)->
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3    
			
		true
	
	addTestButton "FileManager in tab", "Open", ()->
		addHolder()
		.setView "DynamicTabs", (viewTabs)->
			viewTabs.doAddViewTab "FileManager", "FileManagerTab", (view)->
				view.addFile fileData1
				view.addFile fileData2
				view.addFile fileData3
				view.addFile fileData1
				view.addFile fileData2
				view.addFile fileData3    
				
			viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
		true

	addTestButton "FileManager Badge Text", "Open", ()->
		addHolder()
		.setView "DynamicTabs", (viewTabs)->
			viewTabs.doAddViewTab "FileManager", "FileManagerTab", (view)->
				setTimeout () ->
					view.addFile fileData1
				, 1000
				setTimeout () ->
					view.addFile fileData2
				, 2000
				setTimeout () ->
					view.addFile fileData3
				, 3000
			
			viewTabs.addTab "EmptyTab", '<p style="font-size:xx-large;">--- Another tab ---</p>'
		true

	addTestButton "FileManager with Path", "Open", ()->
		addHolder().setView "FileManager", (view)->
			view.setPath "test/new"
			view.setUsername "@admin"

		true

	addTestButton "FileManager Serialize/Deserialize", "Open", () ->
		addHolder()
		.setView "FileManager", (view) =>
			view.setPath "test/images"
			view.addFile fileData1
			view.addFile fileData2
			view.addFile fileData3
			view.addFile fileData4
			view.addFile fileData5
			view.addFile fileData6
			view.addFile fileData7
			view.addFile fileData8
			view.addFile fileData9
			view.addFile fileData10
			view.addSubFolder "subfolder3"
			view.addSubFolder "subfolder3/subfolder4"
			setTimeout () =>
				doPopupView "FileManager", "Serialized FileManager", null, 800, 600, (viewPopup)->
					viewPopup.deserialize view.serialize()
			, 2000
		true

	go()
