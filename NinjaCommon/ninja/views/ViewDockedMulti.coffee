
class ViewDockedMulti extends View

    constructor   : () ->
        super()

        @optionData = {}
        @optionData.direction = "vertical"
        @optionData.sizes     = []
        @holders = []
        @sizes = []

    setDirection: (direction) =>
        if direction is "v" or direction is "V"
            direction = "vertical"
        else if direction is "h" or direction is "H"
            direction = "horizontal"
        @optionData.direction = direction
        @resetSize()
        true

    setupHolders: () =>
        if @children.length >= @optionData.sizes.length
            for child, index in @children
                if index <= @optionData.sizes.length
                    continue
                child.destroy()
            return true

        for size, index in @optionData.sizes
            if index < @children.length
                continue
            @holders.push @addDiv("dockedPart", "dockedPart#{index}")
        true

    getHolder: (index) =>
        @holders[index]

    ##
    ## @params: sizes - array of either number(like 100) or percent(like 0.5 or 50%)
    setSizes: (sizes) =>
        @optionData.sizes = sizes.map (size) =>
            return numeral(size).value()
        @calculateSizes()

    calculateSizes: () =>
        w = @width()
        h = @height()

        @setupHolders()
        @sizes = []

        ##|
        ##|  If not showing, do nothing yet
        if w == 0 or h == 0 then return

        totalFixedSize = @optionData.sizes.reduce (acc, size) =>
            acc = if size > 1 then acc + size else acc
            return acc
        , 0

        totalSize = 0

        if @optionData.direction is "horizontal"
            for size, index in @optionData.sizes
                if size <= 1
                    size = (w - totalFixedSize) * size

                minWidth = @holders[index].getMinWidth() || 0
                if minWidth > size then size = minWidth
                maxWidth = @holders[index].getMaxWidth() || 0
                if maxWidth > 0 and size > maxWidth then size = maxWidth
                if w - totalSize < size then size = Math.max(w - totalSize, 0)
                @sizes.push size
                totalSize += size

        else if @optionData.direction is "vertical"
            for size, index in @optionData.sizes
                if size <= 1
                    size = (h - totalFixedSize) * size

                minHeight = @holders[index].getMinHeight() || 0
                if minHeight > size then size = minHeight
                maxHeight = @holders[index].getMaxHeight() || 0
                if maxHeight > 0 and size > maxHeight then size = maxHeight
                if h - totalSize < size then size = Math.max(h - totalSize, 0)
                @sizes.push size
                totalSize += size

        true

    onResize: (w, h) =>
        super(w, h)
        @calculateSizes()

        if @optionData.direction is "horizontal"
            currentX = 0
            for holder, index in @holders
                holder.move currentX, 0, @sizes[index], h
                currentX += @sizes[index]

        else if @optionData.direction is "vertical"
            currentY = 0
            for holder, index in @holders
                holder.move 0, currentY, w, @sizes[index]
                currentY += @sizes[index]

        true

    serialize: () =>
        docks = []
        for holder, index in @holders
            console.log "Holder", index, holder
            docks.push Object.assign {}, holder?.childView?.serialize?(), 
                size: @sizes[index]

        return Object.assign {}, @optionData, { docks: docks }

    deserialize: (options) =>
        newPromise () =>

            if options.direction?
                @setDirection(options.direction)

            if options.docks? and Array.isArray(options.docks)
                @setSizes(options.docks.map (dock) -> dock.size)

                promises = []
                for dock, index in options.docks
                    if dock.view?
                        promises.push @getHolder(index).internalProcessLayout(dock)

                yield Promise.all promises

                for dock, index in options.docks
                    if dock.view?
                        @internalCopyVariables(@getHolder(index))

            return true
