import { IJob } from "./interfaces/IJob";
export declare class JobServer {
    private api;
    private basePath;
    private dbLogs;
    private homePath;
    private jobs;
    private jobDelay;
    private log;
    private refreshing;
    private serverDetail;
    private statusUpdateClient;
    private busyGetNextJob;
    constructor();
    refreshStatus(): Promise<boolean>;
    static getPendingCount(): Promise<any>;
    promiseDelayed(timeout?: any): Promise<unknown>;
    queueJob(job: IJob): Promise<any>;
    getJobsList(): Promise<IJob[]>;
    refreshJobList(): Promise<boolean>;
    static shouldJobRun(job: any): boolean | number;
    getNextJob(): Promise<IJob | null>;
    static doVerifyInstalled(job: IJob): Promise<boolean>;
    executeJobs(slot_id: number): Promise<any>;
    startSystemStatusLoop(): Promise<any>;
}
export default JobServer;
