import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatMemo extends DataFormatterType {
    name: string;
    input_width: number;
    align: string;
    options: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): boolean;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options: any, path: any): string;
    unformat(data: any, path: any): any;
    onFocus(e: any, col: any, data: any): boolean;
}
export default DataFormatMemo;
