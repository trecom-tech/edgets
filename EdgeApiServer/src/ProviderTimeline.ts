const slug            = require('slug');
const moment          = require('moment');

import EdgeError      from './EdgeError';
import DataSetManager from 'edgedatasetmanager';
import ApiProvider    from './ApiProvider';
import EdgeApi        from 'edgeapi';
import ApiSocketUser  from './ApiSocketUser';

const collectionName  = 'timeline';
const dbName          = 'comms';

export default class ProviderTimeline extends ApiProvider 
{
    dsComms: DataSetManager;

    constructor(prototype: ApiProvider) 
    {
        super(prototype);
    }

    onInit() 
    {
        const dataSet = slug(dbName);
        this.dsComms = new DataSetManager(dataSet);
    }

    getBaseName() {
        return 'timeline';
    }

    /**
     * Create/Upate a new record in comms/timeline db with
     * Hashed string of {participant_id, category, eventStamp}
     * @param participant_id - [string] participant_id
     * @param category - [string] category
     * @param subcategory - [string] subcategory
     * @param text - [string] text
     * @param eventStamp - [any] eventStamp
     * @param eventOffset - [any] eventOffset
     * @param options - [any] option
     * @returns id of added/updated timelined db id
     */
    async doAddTimeline(user: ApiSocketUser, participant_id: string, category: string, subcategory: string, text: string, eventStamp: any, eventOffset: any, options: any) {
        // Only whne user is same id to participant 
        // Or has serviceUser role
        if (!user.hasRole('adminManageUsers') && !user.hasRole('serviceUser') && user.rec.id !== participant_id) {
            throw new EdgeError('Authentication required', 'PROVIDER_TIMELINE');
        }

        if (!participant_id) {
            throw new EdgeError(`Validation Error: participant_id cannot be empty.`, 'PROVIDER_TIMELINE');
        }

        if (!text) {
            throw new EdgeError(`Validation Error: text cannot be empty.`, 'PROVIDER_TIMELINE');
        }

        if (eventStamp) {
            const timeStamp = moment(eventStamp);
            if (!timeStamp.isValid()) {
                throw new EdgeError(`Validation Error: eventStamp is invalid.`, 'PROVIDER_TIMELINE');
            }
            eventStamp = new Date(timeStamp.utc().toISOString());
        }
        if (eventOffset === null || eventOffset === undefined) {
            throw new EdgeError(`Validation Error: eventOffset can not be empty.`, 'PROVIDER_TIMELINE');
        }
        
        const hashData = {
            participant_id,
            category,
            eventStamp
        };

        const timelineId = EdgeApi.getHash(hashData);

        const timelineData = {
            participant_id,
            category, 
            subcategory,
            text,
            eventStamp,
            eventOffset,
            options
        };
        
        await this.dsComms.doUpdate(`/${collectionName}/${timelineId}`, timelineData);
        return timelineId;
    }

    /**
     * Get all timeline items of that specific participant.
     * @param user - [ApiSocketUser]
     * @param participant_id - [string]
     * @returns participant's timeline documents.
     */
    async doGetTimeline(user: ApiSocketUser, participant_id: string) {
        // Only whne user is same id to participant 
        // Or has serviceUser role
        if (!user.hasRole('serviceUser') && user.rec.id !== participant_id) {
            throw new EdgeError('Authentication required', 'PROVIDER_TIMELINE');
        }

        if (!participant_id) {
            throw new EdgeError(`Validation Error: participant_id cannot be empty.`, 'PROVIDER_TIMELINE');
        }
        
        const cond      = `/timeline/participant_id:${participant_id}`;
        const fieldList = {};
        const sortOrder = { _id: -1 };
        const offset    = 0;
        const limit     = 10000000;

        const result = await this.dsComms.doGetItems(cond, fieldList, sortOrder, offset, limit);
        return result;
    }
};  
