//
// class for int data type
//
// @extends [DataFormatterType]
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';

const numeral           = require('numeral');
const common            = new DataFormatCommon();

export class DataFormatInt extends DataFormatterType {
    name: string        = 'int';
    width: number       = 90;
    input_width: number = 90;
    align: string       = 'align';

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {
        if ((data == null)) {
            return "";
        }

        if ((data === null) || ((typeof data === "string") && (data.length === 0))) {
            return "";
        }

        if ((options != null) && (options !== null)) {
            return numeral(common.getNumber(data)).format(options);
        }
        return numeral(common.getNumber(data)).format("#,###");
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        const num = common.getNumber(data);
        if (isNaN(num)) {
            return "";
        }
        return Math.round(num);
    }

    attachEditor(elSourceInput: any, path: any) {
        // #
        // # set type of this input as "number"
        return elSourceInput.css("text-align", "right");
    }
}

export default DataFormatInt;