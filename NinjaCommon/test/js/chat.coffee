$ ->
    messages = [
        id: 1
        content:  "Test incoming text message 1"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-15 11:00')
        type:     "string"
    ,
        id: 2
        content:  "Test incoming text message 2"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-15 11:01')
        type:     "string"
    ,
        id: 3
        content:  "Test outgoing text message 1"
        incoming: false
        from:     "me"
        fromIcon: null
        stamp:    moment('2019-04-15 11:02')
        type:     "string"
    ,
        id: 4
        content:  "Test incoming text message 3"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-16 14:00')
        type:     "string"
    ,
        id: 5
        content:  "Test incoming text message 4"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-16 15:00')
        type:     "string"
    ,
        id: 6
        content:  "Test incoming text message 5"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 11:00')
        type:     "string"
    ,
        id: 7
        content:  "Test incoming text message 6"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 11:01')
        type:     "string"
    ,
        id: 8
        content:  "Test outgoing text message 3"
        incoming: false
        from:     "me"
        fromIcon: null
        stamp:    moment('2019-04-17 11:02')
        type:     "string"
    ,
        id: 9
        content:  "Test incoming text message 7"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 14:00')
        type:     "string"
    ,
        id: 10
        content:  "Test incoming text message 8"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 15:00')
        type:     "string"
    ,
        id: 11
        content:  "Test incoming text message 9"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 18:00')
        type:     "string"
    ,
        id: 12
        content:  "Test incoming text message 10"
        incoming: true
        from:     "sender1"
        fromIcon: null
        stamp:    moment('2019-04-17 18:01')
        type:     "string"
    ]

    addTestButton "Basic ChatView", "Open", ()->
        addHolder().setView "Chat", (view)->
            view.setMe
                id: 0
                name: "Me"
            for msg in messages
                view.addMessage msg
            setTimeout () =>
                view.addMessage null, "Test incoming text message now!", true, "sender1", null
            , 1000
            setTimeout () =>
                view.addMessage null, "Test outgoing text message now!", false, "me", null
            , 2000
        true

    addTestButton "ChatView on Popup", "Open", () ->
        doPopupView "Chat", "ChatView on Popup", null, 600, 800, (view)->
            view.setMe
                id: 0
                name: "Me"
            for msg in messages
                view.addMessage msg
            setTimeout () =>
                view.addMessage null, "Test incoming text message now!", true, "sender1", null
            , 1000
            setTimeout () =>
                view.addMessage null, "Test outgoing text message now!", false, "me", null
            , 2000
            true
        true

    go()