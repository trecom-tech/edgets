## Description

Continuous Integration (CI) is a development practice where developers integrate code into a shared repository frequently, preferably several times a day. Each integration can then be verified by an automated build and automated tests.

Continuous deployment (CD) is a software engineering approach in which software functionalities are delivered frequently through automated deployments

In EdgeTS codebase, we implement CI/CD pipelines using Gitlab CI and Gitlab test runner.

Currently, we already have Gitlab test runner in our dev server and it's connected with gitlab.protovate.com.


## Steps to implement Gitlab CI/CD

### Create `.gitlab-ci.yml` file in the repository.

You can find sample .gitlab-ci.yml file in EdgeCommonConfig and EdgeApiServer module.


```
image: node:latest

services:
  - redis:latest
  - mongo:latest
  - elasticsearch:5.6.15

variables:
  DB_HOST: 'mongodb://mongo:27017/admin'
  REDIS_HOST: 'redis://redis:6379'
  ELASTIC_HOST: 'http://elasticsearch:9200'
  API_SERVER: 'http://127.0.0.1:8001'

stages:
  - test

before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts

testing:
  stage: test
  tags:
    - protovate
  script:
    # Install and build
    - npm install
    - npm run lint
    - npm run test
  artifacts:
    paths:
      - coverage/
```

### After .gitlab-ci.yml file is created, we have to configure environment variables in repository settings.

We have to add 2 environment variables for the gitlab runner clones the private repositories successfully.
`SSH_PRIVATE_KEY`, `SSH_KNOWN_HOSTS` are 2 environment variables we need to add in repository settings.

Please go to `Settings -> CI/CD -> Environment Variables`, and add required variables.

After that, we have to assign test runner to the repository.
We can do it in `Settings -> CI/CD` page.

Only gitlab admins can access repository settings, so please contact gitlab admin for adding these 2 environment variables and assign test runner.