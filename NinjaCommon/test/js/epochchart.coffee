$ ->
	lineChartData = [
		# The first series
		{
		label: "Series 1",
		values: [ {x: 0, y: 100}, {x: 20, y: 300} ]
		},

		# The second series
		{
		label: "Series 2",
		values: [ {x: 20, y: 78}, {x: 30, y: 98} ]
		}
	]
	data = [
		# First series
		{
			label: "Series 1",
			values: [ {time: 1370044800, y: 100} ]
		},

		# The second series
		{
			label: "Series 2",
			values: [ {time: 1370044800, y: 78} ]
		}
	]

	addTestButton "Epoch Line Chart", "Open", ()->
		addHolder().setView "ChartEpoch", (view)=>
			view.setOptions
				# type: 'line'
				data: lineChartData

	addTestButton "Epoch TimeLine Chart - Basic 1 min", "Open", ()->
		initialTime = 1370044801
		addHolder().setView "ChartEpoch", (view)=>
			view.setOptions 
				type: 'time.line'
				data: data
				windowSize: 60
			# view.show()
			setInterval () => 
				newData = [
					{ time: initialTime++, y: Math.random() * 200 + 1},
					{ time: initialTime, y: Math.random() * 200 + 1}
				]
				view.pushData newData
			, 1000			

	addTestButton "Epoch TimeLine Chart - Constant Range Values 2 mins", "Open", ()->
		initialTime = 1370044801
		addHolder().setView "ChartEpoch", (view)=>
			view.setOptions
				type: 'time.line'
				data: data
				range: 
					left: [0, 300]
					right: [10, 500]
				windowSize: 120
			setInterval () => 
				newData = [
					{ time: initialTime++, y: Math.random() * 200 + 1},
					{ time: initialTime, y: Math.random() * 200 + 1}
				]
				view.pushData newData
			, 1000

	addTestButton "Epoch TimeLine Chart - Real time Values", "Open", ()->
		initialTime = 1370044801
		addHolder().setView "ChartEpoch", (view)=>
			view.setOptions
				type: 'time.line'
				data: data
				range: 
					left: [0, 1000]
					right: [0, 1000]
				windowSize: 120
			setInterval () => 
				newData = [
					{ time: initialTime++, y: Math.random() * 990 + 10},
					{ time: initialTime, y: Math.random() * 990 + 10}
				]
				view.pushData newData
			, 500


	addTestButton "Epoch TimeLine Chart - Entries in 10 mins", "Open", ()->
		initialTime = 1370044801
		addHolder().setView "ChartEpoch", (view)=>
			view.setOptions
				type: 'time.line'
				data: data
				range: 
					left: [0, 300]
					right: [10, 500]
				windowSize: 600
			setInterval () => 
				newData = [
					{ time: initialTime++, y: Math.random() * 200 + 1},
					{ time: initialTime, y: Math.random() * 200 + 1}
				]
				view.pushData newData
			, 1000		


	go()