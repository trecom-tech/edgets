import { DataFormatterType } from './DataFormatterType';

export class DataFormatIcon extends DataFormatterType {
    name: string        = 'icon';
    width: number       = 65;
    input_width: number = 65;
    align: string       = 'left';
    options: any;


    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        //
        //  Show typeahead

        const template = "<div id='iconMenu' style='width: 100%;height: 24px;border: none;cursor: pointer;'></div>";

        // @ts-ignore
        const div = addHolder().addDiv("iconField");
        div.html(template);
        elParent.html(div.currentValue);

        const options = {
            width: 500,
            showHeaders: true
        };

        // @ts-ignore
        const t = new TableDropdownMenu("#iconMenu", "icon", ["icon", "value"], options);
        return t.on("change", (val: any) =>
            // console.log "IconTableDropdownMenu:", val
            $('.floatingDropdownValue').html(val)
        );
    }

    //
    // In this case, the options is an array or a comma seperated list of values
    // data must be one of those values of a numeric index to those values.
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options: any, path: any) {

        let i;
        let o;
        this.options           = options;
        const htmlspecialchars = function (str: string) {
            if (typeof str === "string") {
                str = str.replace(/&/g, "&amp;");
                str = str.replace(/"/g, "&quot;");
                str = str.replace(/'/g, "&#039;");
                str = str.replace(/</g, "&lt;");
                str = str.replace(/>/g, "&gt;");
            }
            return str;
        };

        if ((data == null)) {
            return "";
        }

        for (i in this.options) {
            o = this.options[i];
            if (data === o) {
                return o;
            }
        }

        for (i in this.options) {
            o = this.options[i];
            if (`${data}` === `${i}`) {
                return o;
            }
        }

        data          = data + "";
        const matches = data.match(/,/g || []);
        if (matches && (matches.length > 2)) {
            const chars = data.split(",");
            data        = chars[0] + ", " + chars[1] + ", ...";
        }
        return htmlspecialchars(`[${data}]`);
    }

    //
    // funtion to unformat the currently formatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(data: any, path: any) {
        data          = data + "";
        const matches = data.match(/,/g || []);
        if (matches && (matches.length > 2)) {
            const chars = data.split(",");
            data        = chars[0] + ", " + chars[1] + ", ...";
        }
        return data;
    }
}

export default DataFormatIcon;