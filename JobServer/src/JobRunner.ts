import JobServer from "./JobServer";

if (process.env.NODE_ENV === 'local') {
    process.env.MongoDB = '{"url": "mongodb://localhost:27017/admin", "options": {"poolSize": 8,"autoReconnect": true, "noDelay": true,"forceServerObjectId": true,"ignoreUndefined": true,"authSource": "admin" } }';
    process.env.redisHostWQ = 'redis://127.0.0.1:6379';
    process.env.redisReadHost = 'redis://127.0.0.1:6379';
    process.env.redisHost = 'redis://127.0.0.1:6379';
    process.env.mqHost = 'amqp://127.0.0.1:5672';
    process.env.sendgrid  = '{"type": "sendgrid", "apikey": "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"}';
}

const runner = new JobServer();

export class JobRunner {

    static async index(): Promise<boolean> {
        try {
            await runner.startSystemStatusLoop();
            await runner.getJobsList();

            const all = [];
            all.push(runner.executeJobs(0));
            all.push(runner.executeJobs(1));
            all.push(runner.executeJobs(2));
            all.push(runner.executeJobs(3));

            await Promise.all(all);

            console.log("All jobs complete");

            return true;
        } catch (e) {
            return false;
        }
    }
}

JobRunner.index();

export default JobRunner;
