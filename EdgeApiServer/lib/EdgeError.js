"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EdgeError extends Error {
    constructor(message, code, status = 400, options = {}) {
        super(message);
        this.message = message;
        this.code = code;
        this.status = status;
        this.options = options;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.EdgeError = EdgeError;
exports.default = EdgeError;
//# sourceMappingURL=EdgeError.js.map