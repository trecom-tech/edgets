export declare class DataFormatter {
    formats: any;
    common: any;
    getNumber(data: any): any;
    getMoment(data: any, format: any): any;
    register(formattingClass: any): any;
    getFormatter(dataType: any): any;
    formatData(dataType: any, data: any, options?: any, path?: any): any;
    unformatData(dataType: any, data: any, options?: any, path?: any): any;
    constructor();
}
export default DataFormatter;
