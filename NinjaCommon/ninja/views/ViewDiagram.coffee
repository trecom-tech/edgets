###

This view is for a Diagram tool based on open source "JoinJS"

Reference links:
https://github.com/clientIO/joint
https://github.com/clientIO/joint/blob/master/tutorials/hello-world.html
http://resources.jointjs.com/demos/kitchensink
http://resources.jointjs.com/demos/umlcd

JointJS Online Documentation:
http://resources.jointjs.com/docs/jointjs/v2.0/joint.html#dia

###

class DiagramShape

    constructor: (@type, @text, @options, @parent)->

        if @text?
            if !@options.attrs? then @options.attrs = {}
            if !@options.attrs.text? then @options.attrs.text = {}
            if !@options.attrs.text.text? then @options.attrs.text.text = @text

        @shape = new @type(@options)
        @id = @shape.id

    width: ()=>
        return @shape.options.size.width

    height: ()=>
        return @shape.attributes.size.height

    left: ()=>
        return @shape.attributes.position.x

    top: ()=>
        return @shape.attributes.position.y


class DiagramShapeSimpleRect extends DiagramShape

    @defaultOptions =
        size:
            width: 100
            height: 20
        rect:
            fill: 'purple'
        text:
            text: ""
            fill: "yellow"

    constructor: (@text, @options, @parent)->
        if !@options? then @options = {}

        $.extend @options, DiagramShapeSimpleRect.defaultOptions, false
        console.log "OPTIONS:", @options
        super(joint.shapes.basic.Rect, @text, @options)


class ViewDiagram extends View

    getDependencyList: ()=>
        return [ "/vendor/lodash.min.js", "/vendor/backbone-min.js", "/vendor/jointjs/dist/joint.min.js", "/vendor/jointjs/dist/joint.min.css" ]

    ##|
    ##|  Automatically scale the content to fit after the view is resized
    ##|  @param enabled {boolean=true} Set to true will scale to fit automatically
    ##|
    setAutoFitAfterResize: (enabled = true) =>
        @viewOptions.autoFitAfterResize = enabled
        if enabled? then @scaleContentToFit()
        true

    ##|
    ##|  Create a rect shape
    createRect: (text, options = {})=>
        $.extend options, { position: x: @viewOptions.nextX, y: @viewOptions.nextY }, true
        rect = new DiagramShapeSimpleRect(text, options, this)
        @graph.addCells [rect.shape]
        @viewOptions.nextX = rect.left()
        @viewOptions.nextY = rect.top() + @viewOptions.defaultPadding + rect.height()
        return rect

    ##|
    ##|  Create a link between two shapes
    ##|  @param sourceShape {DiagramShape} The source where the link starts
    ##|  @param targetShape {DiagramShape} The target shape to link to
    ##|  @patam options {object} Not currently used
    ##|
    createLink: (sourceShape, targetShape, options)=>
        link = new joint.dia.Link
            source: { id: sourceShape.id }
            target: { id: targetShape.id }
        @graph.addCells [link]
        return link

    ##|
    ##|  Called automatically when the view is created
    ##|
    onShowScreen: ()=>

        ##|
        ##|  Default view options that can be extended.  Stored in an object
        ##|  so they can be easily saved and loaded.
        @viewOptions = {}
        @viewOptions.autoFitAfterResize = false
        @viewOptions.defaultPadding     = 10
        @viewOptions.nextX              = 10
        @viewOptions.nextY              = 10

        ##|
        ##|  View is created - Demo code from Tutorial "HelloWorld" above
        @graph = new joint.dia.Graph;

        @paper = new joint.dia.Paper
            el:       @el
            width:    @width(),
            height:   @height(),
            model:    @graph,
            gridSize: 1

        ##|
        ##| Tutorial content

        # rect = new joint.shapes.basic.Rect
        #     position: { x: 100, y: 30 },
        #     size: { width: 100, height: 30 },
        #     attrs: { rect: { fill: 'blue' }, text: { text: 'my box', fill: 'white' } }

        # rect2 = rect.clone();
        # rect2.translate(300);

        # link = new joint.dia.Link
        #     source: { id: rect.id },
        #     target: { id: rect2.id }

        # @graph.addCells([rect, rect2, link]);

    ##|
    ##|  View has been resized, resize the paper object
    onResize: (w, h)=>
        super(w, h)
        @paper.setDimensions(w, h)
        if @viewOptions.autoFitAfterResize then @scaleContentToFit()
        true

    ##|  Scale the content to fit
    ##|  @param options {object} See http://resources.jointjs.com/docs/jointjs/v2.0/joint.html#dia.Paper.prototype.scaleContentToFit for options
    ##|
    scaleContentToFit: (options)=>

        if !options? then options = {}
        if !options.padding? then options.padding = 10
        @paper.scaleContentToFit(options)
        true

