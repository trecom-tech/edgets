import { Column } from '../column/column';

export class Table {
    tableName: string;
    col: any;
    settings: any;

    //
    // Create a new table holder
    //
    constructor(tableName: string) {
        this.tableName = tableName;
        this.col       = {};

        //
        //  Store table specific columns
        this.settings = {};
    }

    //
    //  Change one of the table settingsz
    setOption(varName: string, value: any) {
        return this.settings[varName] = value;
    }

    //
    //  Get one of the table settings
    getOption(varName: string, defaultValue: any = null) {
        if (this.settings[varName] != null) {
            return this.settings[varName];
        }

        return defaultValue;
    }


    //
    // function to return a single column (of DataSetConfig.column type)
    //
    // @param [String] source to get value from the given string.
    //
    getColumn(source: string) {
        if (this.col[source] != null) {
            return this.col[source];
        }

        const s = source.toLowerCase().replace("_", " ");
        for (let id in this.col) {
            const col = this.col[id];
            if (col.getName().toLowerCase().replace("_", " ") === s) {
                return col;
            }
            if (col.getSource().toLowerCase().replace("_", " ") === s) {
                return col;
            }
        }

        return null;
    }

    //
    // Internal function to make sure that all the columns have an "order" value and that they are all unique.
    //
    verifyOrderIsUnique() {
        let col;
        let order;

        const seen: any = {};
        let max         = 0;

        //  For any columns with a known order
        for (const source in this.col) {
            col   = this.col[source];
            order = col.getOrder();
            if (order != null) {
                if (seen[order] != null) {
                    col.changeColumn("order", null);
                } else {
                    seen[order] = true;
                }
            }
        }

        //  Assign all unassigned
        for (const source in this.col) {
            col = this.col[source];
            if ((col.getOrder() == null)) {
                while (seen[max] != null) {
                    max = max + 1;
                }
                col.changeColumn("order", max);
                seen[max] = true;
            }
        }

        const names = Object.keys(this.col).sort((a, b) => {
            return this.col[a].getOrder() - this.col[b].getOrder();
        });

        max = 0;
        for (let name of Array.from(names)) {
            this.col[name].data.order = max++;
        }

        return true;
    }

    //
    // function to save all the current columns to a JSON object.
    //
    serialize() {
        const output: any = {};

        this.verifyOrderIsUnique();

        output.settings = this.settings;

        for (let source in this.col) {
            let functionText;
            const col      = this.col[source];
            output[source] = col.serialize();

            if ((output[source].render != null) && (typeof output[source].render === "function")) {
                //  Convert function to string
                functionText             = this.renderFunctionToString(output[source].render);
                output[source]["render"] = functionText;
            }

            if ((output[source].editFunction != null) && (typeof output[source].editFunction === "function")) {
                //  Convert function to string
                functionText                   = this.renderFunctionToString(output[source].editFunction);
                output[source]["editFunction"] = functionText;
            }
        }

        return output;
    }

    renderFunctionToString(functionObject: any) {
        return functionObject.toString();
    }

    //
    // function to restore from a saved configuration
    //
    unserialize(settingsObject: any) {
        for (let key in settingsObject) {
            const col = settingsObject[key];
            this.configureColumn(col, false);
        }

        return true;
    }

    //
    // function to add or update the configuration for a column. 
    // If skipDeduce is false then check the data in the table and try to figure out what the format should be.
    //
    configureColumn(col: any, skipDeduce: boolean) {
        if (skipDeduce == null) {
            skipDeduce = false;
        }
        if ((col == null) || (col.source == null)) {
            return;
        }

        if ((this.col[col.source] == null)) {
            this.col[col.source] = new Column(this.tableName);
        }

        if ((col.order == null)) {
            col.order = Object.keys(this.col).length;
        }

        this.col[col.source].deserialize(col);

        if ((skipDeduce == null) || (skipDeduce === false)) {
            this.col[col.source].deduceInitialColumnType();
        }

        return this.col[col.source];
    }
}

export default Table;