import * as EdgeApiServer from 'edgeapiserver';
import config             from 'edgecommonconfig';

config.setCredentials('redisReadHost', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHostWQ', `${process.env.REDIS_HOST}`);
config.setCredentials('redisHost', `${process.env.REDIS_HOST}`);
config.setCredentials('MongoDB', {
    url    : `${process.env.DB_HOST}`,
    options: {
        connectTimeoutMS: 600000,
        poolSize        : 16,
        socketTimeoutMS : 600000,
    },
});
config.setCredentials('elasticsearch', {
    type: 'es',
    host: process.env.ELASTIC_HOST,
});
config.setCredentials('mqHost', process.env.MQHOST);

config.ApiServers = [process.env.API_SERVER];


export const server = EdgeApiServer.startServer();
