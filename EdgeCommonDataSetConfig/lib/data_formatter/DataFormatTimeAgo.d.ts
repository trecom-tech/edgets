import { DataFormatterType } from './DataFormatterType';
export declare class DataFormatTimeAgo extends DataFormatterType {
    name: string;
    width: number;
    input_width: number;
    align: string;
    elEditor: any;
    editorShowing: boolean;
    elDataEditor: any;
    datePicker: any;
    constructor(...args: any[]);
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any): any;
    attachEditor(elSourceField: any, path: any): boolean;
    format(data: any, options?: any, path?: any): string;
    unformat(data: any, path: any): Date | "";
}
export default DataFormatTimeAgo;
