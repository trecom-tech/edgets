//
// class for datetime data type
//
// @extends [DataFormatterType]
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';


export class DataFormatTags extends DataFormatterType {
    name: string       = 'tags';
    align: string      = 'left';
    options: any;

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to open editor including ace code editor
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {

        let value;
        if ((currentValue == null)) {
            currentValue = "";
        }

        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',');
        }

        if (!this.options) {
            this.options = [];
        }

        // #
        // # extend options by current value to make sure that all current values
        // # are selectable
        for (value of Array.from(currentValue)) {
            this.options.push(value);
        }

        const options = {
            title: "Edit options",
            content: "",
            inputs: [{
                type: "tags",
                label: "Value",
                name: "input1",
                options: this.options,
                value: currentValue
            }
            ],
            buttons: [{
                type: "submit",
                text: "Save"
            }
            ],
            onSubmit: (form: any) => {
                this.saveValue(form.input1.value);
                return false;
            }
        };

        // @ts-ignore
        const m = new ModalForm(options);
        return true;
    }

    //
    //  Attach to an existing text input.
    //  Hide the text input and add the select statement
    //  and then use selectize https://github.com/selectize/selectize.js/blob/master/docs/api.md
    attachEditor(elSourceInput: any, path: any) {
        let name;
        let value;
        super.attachEditor(elSourceInput, path);

        const options = DataFormatCommon.getOptions(elSourceInput.data("options"));

        const config: any       = {};
        config.options     = {};
        config.valueField  = "id";
        config.labelField  = "title";
        config.searchField = "title";
        config.maxItems    = 999;  // # note:  easy to change in the options passed in
        config.options     = [];
        config.create      = true;
        config.items       = [];

        const currentValue = elSourceInput.val();
        if (typeof currentValue === "string" && (currentValue !== "")) {
            for (let item of Array.from(currentValue.split(","))) {
                config.items.push(item.toString().trim());
            }
        }

        if (Array.isArray(options.items)) {
            for (name of Array.from(options.items)) {
                config.options.push({id: name.toString().trim(), title: name});
            }
        } else {
            for (name in options.items) {
                value = options.items[name];
                config.options.push({id: value.toString().trim(), title: name});
            }
        }

        //
        //  Copy forward any config
        for (name in options) {
            value = options[name];
            if (name === "items") {
                continue;
            }
            config[name] = value;
        }

        // config.maxItems = config.options.length

        config.onChange = (newValue: any) => {
            elSourceInput.val(newValue);
            return true;
        };

        //
        elSourceInput.hide();
        elSourceInput.elSelect = $("<select />");
        elSourceInput.parent().prepend(elSourceInput.elSelect);
        const selectList = elSourceInput.elSelect.selectize(config);
        elSourceInput.elSelect.on("change", (e: any) => {
            // console.log "DataFormatTags elSelect changed: ", e
            return elSourceInput.change();
        });
        // console.log "Create with options:", config

        elSourceInput.val = (newValue: any) => {
            if ((newValue == null)) {
                return selectList[0].selectize.getValue();
            }
            // console.log "enum val(): ", newValue
            value = __guard__(DataFormatCommon.getOptions(newValue), (x: any) => x["items"]) || newValue;
            selectList[0].selectize.setValue(value, true);
            return newValue;
        };

        return true;
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(currentValue: any, options: any, path: any) {
        const obj = DataFormatCommon.getOptions(options);
        if (obj.items != null) {
            this.options = obj.items;
        } else {
            this.options = obj;
        }

        if (!Array.isArray(this.options)) {
            this.options = [];
        }

        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',');
        }

        const values = [];
        for (let idx in currentValue) {
            const val = currentValue[idx];
            if ((val == null) || (val === "")) {
                continue;
            }
            const value = String(val).trim();
            if (this.options.indexOf(value) < 0) {
                this.options.push(value);
            }
            values.push(value);
        }
        return values.sort().join(", ");
    }

    //
    // funtion to unformat the currently unformatted data
    //
    // @param [Object] data data to be unformatted
    // @param [String] path path where the value is being edited
    // @return [Object] data unformatted data
    //
    unformat(currentValue: any, path: any) {
        if (typeof currentValue === "string") {
            currentValue = currentValue.split(',');
        }
        return currentValue;
    }
}

export default DataFormatTags;

function __guard__(value: any, transform: any) {
    return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}