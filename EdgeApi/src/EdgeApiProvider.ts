/**
 * This class is a general interface that can hold a list of dynamic
 * API calls and then execute those calls as needed
 */
import { config }                              from 'edgecommonconfig';
import { EdgeApiDefinition, EdgeSocketClient } from './EdgeApi';

/**
 * A globally unique counter used to help assign a unique ID to each request
 */

let globalApiCounter: number = 10;

class EdgeApiProviderStream {
    private allData: any[] = [];
    private isFinished     = false;

    constructor(provider: EdgeApiProvider, public stream_id: any) {
        provider.on(this.stream_id, this.onDataCallback.bind(this));
    }

    onData(record: any) {
        this.allData.push(record);
        return false;
    }

    onFinished(): any {
        this.isFinished = true;
        return false;
    }

    /**
     * Create a promise that waits until all results are available
     */
    doWaitUntilFinished(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.isFinished) {
                resolve(this.allData);
                return true;
            }

            return (this.onFinished = () => {
                return resolve(this.allData);
            });
        });
    }

    onDataCallback(data: any): boolean {
        // fix because node server emit sends an array
        if (data != null && data[0] != null) {
            data = data[0];
        }

        console.log(`EdgeApiProviderStream ${this.stream_id}:`, data);
        if (data != null && typeof data === 'string' && data === 'END') {
            this.onFinished();
            return true;
        }

        this.onData(data);
        return true;
    }
}


export class EdgeApiProvider {
    [DynamicFunctionName: string]: any;

    isVerbose: boolean                                                 = true;
    dynamicResolveList: { [uui: string]: EdgeApiProviderStream | any } = {};
    socket: EdgeSocketClient                                           = null;
    registeredList: any[]                                              = [];
    commandCallback: { [callName: string]: EdgeApiDefinition }         = {};
    dynamicApiCallsList: { [index: string]: any }                      = {};
    dynamicCallPending: any[]                                          = [];
    dynamicRejectList: any[]                                           = [];

    constructor() {
        this.isVerbose                = true;
    }

    /**
     * @returns uuid for api call
     */
    fastUUID(): string {
        let uuid: string =
                new Date().getTime() +
                'E' +
                Math.ceil(Math.random() * 100000) +
                globalApiCounter++;
        return uuid;
    }

    /**
     * Callback when socket is connected again
     * @param socket socket to reconnect
     */
    onReconnect(socket?: SocketIOClient.Socket): boolean {
        // Resend registrations
        for (let cmd of Array.from(this.registeredList)) {
            this.socket.emit('register', cmd);
        }

        // console.log "onReconnect, resending ", Object.keys(@dynamicCallPending).length
        for (let i in this.dynamicCallPending) {
            let o = this.dynamicCallPending[i];
            config.status(`EdgeApiProvider onReconnect Resending ${i}:`, o);
            // console.log "API Call resending after reconnection, id=", i, "command=", o
            this.socket.emit('cmd', o);
        }

        return true;
    }

    /**
     * Default event when a command is done executing
     *
     */

    // tslint:disable-next-line:no-empty
    onCmdReplyComplete(data: any) {
    }

    /**
     * Setup the socket to listen for an incoming reply
     * @param socket socket to setup
     */
    onSetupSocket(socket: any): void {
        this.socket = socket;
        this.socket.on('cmd', this.onIncomingCommandRequest.bind(this));

        this.socket.on('api-list', this.onReceiveApiList.bind(this));

        // Server sends "cmd-reply" with the results of an API call
        this.socket.on('cmd-reply', this.onIncomingCommandReply.bind(this));
    }

    /**
     * This should be a reply to our command which must contain the UUID of the request
     * @param data result from the cmd reply
     */
    onIncomingCommandReply(data: any): boolean {
        if (!data.uuid) {
            console.log('Invalid cmd-reply, no uuid:', data);
            return false;
        }

        // Receive a response to a command
        // console.log "RECEIVED cmd-reply", data
        if (this.dynamicResolveList[data.uuid] != null) {
            if (
                data.result != null &&
                typeof data.result === 'string' &&
                /^STREAM:([0-9]+)/.test(data.result)
            ) {
                this.dynamicResolveList[data.uuid](
                    new EdgeApiProviderStream(this, data.result),
                );
            } else {
                this.dynamicResolveList[data.uuid](data.result);
            }

            delete this.dynamicResolveList[data.uuid];
            delete this.dynamicRejectList[data.uuid];
            delete this.dynamicCallPending[data.uuid];
            this.onCmdReplyComplete(data);
        } else {
            console.log('Received response to nothing:', data);
        }

        return true;
    }

    /**
     * @returns get pending call length
     */
    getPending(): number {
        return Object.keys(this.dynamicCallPending).length;
    }

    /**
     * Return when all pending calls are complete
     * allows a timeout
     * @param waitTimeout wait time span
     */
    doCompletePending(waitTimeout: number = null) {
        if (waitTimeout == null) {
            waitTimeout = 30000;
        }
        return new Promise((resolve, reject) => {
            let checkComplete: Function;
            let waitTimer = setTimeout(() => {
                config.status(
                    `EdgeApiProvider::doCompletePending Wait timeout with ${this.getPending()}`,
                );
                return resolve(true);
            }, waitTimeout);

            config.status(
                `EdgeApiProvider::doCompletePending Waiting on ${this.getPending()}`,
            );
            return (checkComplete = () => {
                if (this.getPending() === 0) {
                    clearTimeout(waitTimer);
                    resolve(true);
                }

                return setTimeout(checkComplete, 10);
            })();
        });
    }

    /**
     * Callback when it get api list from serer
     * @param data Api call list
     */
    onReceiveApiList(data: any): boolean {
        for (let name in data) {
            let params = data[name];
            this.addDynamicApiCall(name, params);
        }

        return true;
    }

    /**
     * Execute a dynamic call, returns a promise
     * the promise is resolve with the result of the API call
     * Param is an array of call values or an object with call values
     * @param callName api name
     * @param params array of call values or an object with call values
     * @returns success or failure
     */
    MakeDynamicCall(callName: string, params: any[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.dynamicApiCallsList[callName] == null) {
                reject(new Error(`Invalid API call: ${callName}`));
            }

            let pos       = 0;
            let data: any = {};

            // console.log "DY=", @dynamicApiCallsList[callName]
            for (let varName of this.dynamicApiCallsList[callName]) {
                if (varName === 'user') {
                    continue;
                }
                if (typeof params[pos] !== 'undefined') {
                    data[varName] = params[pos++];
                } else if (params[varName] != null) {
                    data[varName] = params[varName];
                } else {
                    data[varName] = null;
                    pos++;
                    // console.log "Invalid call to ", callName, "(", params, ")"
                    console.log(
                        'EdgeApi Warning, Parameter for ',
                        varName,
                        ` not passed in to call ${callName} with:`,
                        JSON.stringify(params),
                    );
                }
            }
            // reject new Error "Invalid call to #{callName}, missing #{varName}"

            data.callName = callName;
            data.uuid     = this.fastUUID();

            this.dynamicResolveList[data.uuid] = resolve;
            this.dynamicRejectList[data.uuid]  = reject;
            this.dynamicCallPending[data.uuid] = data;

            this.socket.emit('cmd', data);
        });
    }

    /** Register a single API command, create a dynamic function that calls it
     * This is an API call that the server knows how to answer, not a call
     * that we are capable of answering on this client
     * @param callName call name to add
     * @param callParams call params to add
     */

    addDynamicApiCall(callName: string, callParams: object): boolean {
        // Don't re-register the call
        if (this[callName] != null) {
            return true;
        }

        if (callParams == null) {
            callParams = [];
        }

        // console.log "dynamicApiCallsList[#{callName}] = ", callParams
        this.dynamicApiCallsList[callName] = callParams;
        let tmp                            = new Function(
            `return function ${callName}() { return this.MakeDynamicCall(arguments.callee.name, arguments); }`,
        );
        this[callName]                     = tmp();
        return true;
    }

    /**
     * Execute a local command that is a registered API call.
     * The callData comes from the caller and may or may not match
     * the requirements of the call.
     * @param callName
     * @param callData
     */
    onExecuteLocalCommand(callName: string, callData: any): any {
        if (callName == null || typeof callName !== 'string') {
            throw new Error('Invalid call, missing callName');
        }

        if (callData == null) {
            throw new Error('Invalid call, missing callData');
        }

        if (this.commandCallback[callName] == null) {
            console.log(
                `Invalid onExecuteLocalCommand(${callName});`,
                this.commandCallback,
            );
        }

        let args = [];
        for (let varName in this.commandCallback[callName].params) {
            let description = this.commandCallback[callName].params[varName];
            if (callData[varName] == null) {
                console.log(`Warning:  Missing '${varName}' in `, callData);
            }

            args.push(callData[varName]);
        }

        console.log('onExecuteLocalCommand args=', args);
        let result = this.commandCallback[callName].target.apply(this, args);
        console.log('onExecuteLocalCommand result=', result);

        return result;
    }

    /**
     * Callback when it got result from server
     * @param data data from server
     */
    onIncomingCommandRequest(data: any): boolean {
        if (this.commandCallback[data.callName] == null) {
            return false;
        }

        console.log('Incoming command for me:', data);
        let result = this.onExecuteLocalCommand(data.callName, data);

        if (
            result != null &&
            result.then != null &&
            typeof result.then === 'function'
        ) {
            // Handle the promise by waiting for the answer and then
            // sending that answer back to the client
            result.then((newResult: any) => {
                // console.log "Secondary response=", newResult
                return this.socket.emit('cmd-reply', {
                    uuid  : data.uuid,
                    result: newResult,
                });
            });
        } else {
            this.socket.emit('cmd-reply', {
                uuid: data.uuid,
                result,
            });
        }

        return true;
    }

    /**
     * Tell the server we are able to support a given command
     * When the command arrives from the socket, we'll execute it
     * and return the result.  The promise here returns after
     * the registration is sent to the server
     * @param apiDefinition
     */
    async doRegisterApiCommand(apiDefinition: EdgeApiDefinition): Promise<boolean> {
        let path = apiDefinition.base + '_' + apiDefinition.command;
        console.log(`doRegisterApiCommand: ${path}`);

        this.commandCallback[path] = apiDefinition;
        this.registeredList.push(apiDefinition);
        this.socket.emit('register', apiDefinition);

        // We send the register command to the central server
        // The central server converts this to an API Call
        // The central server sends the API call back out
        // which is received as "api-list" and added

        // When a client wants to trigger this call, a "cmd"
        // is received,

        return true;
    }
}

export default EdgeApiProvider;
