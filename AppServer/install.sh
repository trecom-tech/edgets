#!/bin/bash

CWD=`pwd`; 

echo "#!/bin/bash" > /usr/local/bin/jobserver; 
echo "cd '$CWD' && ./jobserver" >> /usr/local/bin/jobserver;
chmod +755 /usr/local/bin/jobserver

echo "#!/bin/bash" > /usr/local/bin/appserver;
echo "cd '$CWD' && ./appserver" >> /usr/local/bin/appserver;
chmod +755 /usr/local/bin/appserver

echo "#!/bin/bash" > /usr/local/bin/jobtool;
echo "cd '$CWD' && ./jobtool" >> /usr/local/bin/jobtool;
chmod +755 /usr/local/bin/jobtool