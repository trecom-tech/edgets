//
// class for timeago data type
//
// @extends [DataFormatterType]
//
import { DataFormatCommon }  from './DataFormatCommon';
import { DataFormatterType } from './DataFormatterType';
import { Moment }            from 'moment';

const numeral = require('numeral');
const common  = new DataFormatCommon();
const moment  = require('moment');

function __guard__(value: any, transform: any) {
    return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}

export class DataFormatTimeAgo extends DataFormatterType {
    name: string        = 'timeago';
    width: number       = 135;
    input_width: number = 135;
    align: string       = 'right';
    elEditor: any;
    editorShowing: boolean;
    elDataEditor: any;
    datePicker: any;

    constructor(...args: any[]) {
        super();
    }

    //
    // funtion to open editor with flatpickr
    //
    // @param [JqueryObject] elParent parent element
    // @param [Integer] left left position offset
    // @param [Integer] top top position offset
    // @param [Integer] width width of the editor
    // @param [Integer] height height of the editor
    // @param [Object] currentValue current value of the cell
    // @param [String] path path where the value is being edited
    // @return null
    //
    openEditor(elParent: any, left: any, top: any, width: any, height: any, currentValue: any, path: any) {
        if (!this.elEditor) {
            this.elEditor = $("<input />", {
                    type : "text",
                    class: "dynamic_edit"
                }
            );

            this.appendEditor();
        }

        this.elEditor.css({
            position : "absolute",
            "z-index": 5001,
            top,
            left,
            width,
            height
        });

        this.attachEditor(this.elEditor, path);

        // @elEditor.on "change", (val)=>
        //     # console.log "DATE CHANGE=", val
        //     @editorShowing = false
        //     @elEditor.hide()

        this.elEditor.val(this.format(currentValue, path));
        this.elEditor.show();
        return this.elEditor.focus();
    }

    attachEditor(elSourceField: any, path: any) {
        super.attachEditor(elSourceField, path);
        const options      = JSON.parse(elSourceField.data("options") || "{}");
        const df           = (options != null) && (options.dateonly != null) && (options.dateonly === true) ? "D, M J, Y" : "D, M J, Y h:i:S K";
        const et           = (options != null) && (options.dateonly != null) && (options.dateonly === true) ? false : true;
        const currentValue = this.unformat(elSourceField.val(), path);
        //
        //  Create 3rd party date picker control
        // @ts-ignore
        this.datePicker = new flatpickr(elSourceField[0], {
                allowInput: true,
                dateFormat: df,
                parseDate : (dateStr: string) => {
                    const mom = common.getMoment(dateStr, 'MM/DD/YYYY HH:mm:ss');
                    if (mom != null) {
                        return mom.toDate();
                    }
                    return null;
                },
                onChange  : (dateObj: any, dateStr: string) => {
                    if ((dateObj != null) && dateObj.length) {
                        this.saveValue(dateObj);
                        elSourceField.val(this.format(dateObj[0], options, path));
                        return elSourceField.change();
                    }
                },
                onOpen    : (dateObj: any, dateStr: string, instance: any) => {
                    const time = __guard__(common.getMoment(this.unformat(elSourceField.val(), path)), (x: Moment) => x.format('MM/DD/YYYY HH:mm:ss'));
                    if (time != null) {
                        return instance.setDate(time);
                    }
                },
                onClose   : (dateObj: any, dateStr: string, instance: any) => {
                    if ((dateObj != null) && dateObj.length) {
                        const date = dateObj[0];
                        const time = common.getMoment(date);
                        if (time != null) {
                            instance.setDate(date);
                        }
                        elSourceField.val(this.format(date), options, path);
                    }
                    this.editorShowing = false;
                    if (this.elEditor != null) {
                        return this.elEditor.hide();
                    }
                },
                enableTime: true,
                time_24hr : true
            }
        );

        this.elDataEditor = this.datePicker.calendarContainer;

        // #|
        // #|  Attach any keyboard event in the text editor to open the date picker
        elSourceField.on('keydown', (e: any) => {
            // if editor is closed then close datepicker
            if (!this.editorShowing) {
                return this.datePicker.close();
            }
        });
        elSourceField.val(this.format(currentValue));
        return true;
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    format(data: any, options?: any, path?: any) {

        let hrs;
        let stamp;
        let txt;

        if ((data == null)) {
            return "";
        }

        if (typeof data === "string") {
            stamp = new Date(data);
        } else if (typeof data === "number") {
            stamp = new Date(data);
        } else if (typeof data === "object") {
            if (data.getTime != null) {
                stamp = data;
            } else if (moment.isMoment(data)) {
                stamp = data.toDate();
            } else {
                stamp = new Date(common.getMoment(data));
            }
        } else {
            return "";
        }

        const originAge = new Date().getTime() - stamp.getTime();
        let age         = Math.abs(originAge);
        age /= 1000;

        if (age < 60) {
            txt = numeral(age).format("#") + " sec";
        } else if (age < (60 * 60)) {
            txt = numeral(age / 60).format("#") + " min";
        } else if (age > 86400) {
            let daysTxt;
            const days = Math.floor(age / 86400);
            hrs        = Math.floor((age - (days * 86400)) / (60 * 60));
            if (days !== 1) {
                daysTxt = "days";
            } else {
                daysTxt = "day";
            }
            if ((hrs > 0) && (days < 30)) {
                txt = `${days} ${daysTxt}, ${hrs} hr`;
                if (hrs !== 1) {
                    txt += "s";
                }
            } else {
                txt = `${days} ${daysTxt}`;
            }
        } else {
            let hrsText;
            hrs       = Math.floor(age / (60 * 60));
            const min = (age - (hrs * 60 * 60)) / 60;
            if (hrs > 1) {
                hrsText = "hrs";
            } else {
                hrsText = "hr";
            }
            txt = numeral(hrs).format("#") + ` ${hrsText}, ` + numeral(min).format("#") + " min";
        }
        if (originAge < 0) {
            txt = `-${txt}`;
        }
        return txt;
    }

    //
    // funtion to format the currently passed data
    //
    // @param [Object] data data to be formatted
    // @param [Object] options additonal options defined for the datatype
    // @param [String] path path where the value is being edited
    // @return [Object] data formatted data
    //
    unformat(data: any, path: any) {
        if ((data == null) || (typeof data !== "string") || (data === "")) {
            return "";
        }
        const format = "YYYY-MM-DD HH:mm:ss";

        const days   = data.split(" day").length > 1 ? parseInt(data.split(" days")[0], 10) : 0;
        const hrPos  = data.includes("min") ? 0 : 1;
        const hrs    = data.split(" hr").length > 1 ? parseInt(data.split(" hr")[0].split(", ")[hrPos], 10) : 0;
        const minPos = data.includes("hr") ? 1 : 0;
        const mins   = data.split(" min").length > 1 ? parseInt(data.split(" min")[0].split(", ")[minPos], 10) : 0;

        const diff = ((days * 86400) + (hrs * 3600) + (mins * 60)) * 1000;
        return new Date(new Date().getTime() - diff);
    }
}

export default DataFormatTimeAgo;